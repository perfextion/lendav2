import { Logpriority } from '../app/models/loanmodel';

export const environment = {
  production: true,
  // apiUrl: 'http://localhost:61002',
  // apiUrl: 'http://lendav2api.azurewebsites.net',
  apiUrl: 'http://lendav2vm.southcentralus.cloudapp.azure.com',
  loankey: 'currentselectedloan',
  loankey_copy: 'rawcurrentselectedloan',
  logpriority: 'logpriority',
  frontEndDebugLevel: 'frontenddebuglevel',
  backendDebugLevel: 'backenddebuglevel',
  loanCrossCollateral: 'loancrosscollateral',
  referencedatakey: 'refdata',
  uid: 'userid',
  officeid: 'officeid',
  loanidkey: 'selectedloanId',
  collateralTables: 'collateralTables',
  isDebugModeActive: true,
  localStorage: {
    userRole: 'userRole'
  },
  apiEndpoints: {
    adminEndpoints: {
      updateCrop: '/api/Loan/UpdateCropPrice',
      deleteCrop: '/api/Loan/DeleteCropPrice',
      allCropList: '/api/Loans/GetAllCropList'
    },
    userEndpoints: {
      subsidiaryDatabase: '/api/Loans/GetSubsidiaryLoanList'
    }
  },
  marginScale: 1000000,
  usersession: 'sessionid',
  errorbase: 'errors',
  exceptionStorageKey: 'exceptions',
  modifiedbase: 'changedvalues',
  verificationbase: 'verificationerrorvalues',
  syncRequiredItems: 'syncRequiredItems',
  myPreferences: 'myPreferences',
  currentpage: 'currentpage',
  schematicsSettings: 'schematicsSettings',
  loanGroup: 'loanGroup',
  completedPercentage: 'completedPercentage',
  modifiedoptimizerdata: 'modifiedoptimizerdata',
  loanofficerstats: 'loanofficerstats',
  favorites: 'favoristList',
  masterborrowerlist: 'masterborrowerlist',
  masterfarmerlist: 'masterfarmerlist',
  knowledgeLakeURL: 'https://agresmgmt.knowledgelake.io',
  knowledgeLakeDocsURL: 'https://armlend365.sharepoint.com/sites/loans/default.aspx',
  notridgeURL: 'https://community.nortridge.com/login',
  woltersKluwer: 'https://ui-login.accountingresearchmanager.com/?resume=/idp/HlSpD/resumeSAML20/idp/SSO.ping&loginPageMode=CLASSIC&PF_TargetResource=http%3A%2F%2Fwww.accountingresearchmanager.com%2F%23%2Fhome&loginPageType=STANDARD&currentCPID=WKUS-TAL-ARM#/login',
  unsavedChangesWarningMessage: 'You have unsaved changes on this page. Do you want to leave this page and discard your changes?',
  ghostUserDetails: 'ghostuser'
};
