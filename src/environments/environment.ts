import { Logpriority } from '../app/models/loanmodel';

export const environment = {
  production: true,
  // apiUrl: 'http://localhost:61002',
  // apiUrl: 'http://lendav2api.azurewebsites.net',
  apiUrl: 'http://lendav2vm.southcentralus.cloudapp.azure.com',
  loankey: 'currentselectedloan',
  loankey_copy: 'rawcurrentselectedloan',
  loanCrossCollateral: 'loancrosscollateral',
  logpriority: 'logpriority',
  referencedatakey: 'refdata',
  isDebugModeActive: true,
  collateralTables: 'collateralTables',
  localStorage: {
    userRole: 'userRole'
  },
  apiEndpoints: {
    adminEndpoints: {
      updateCrop: '/api/Loan/UpdateCropPrice',
      deleteCrop: '/api/Loan/DeleteCropPrice',
      allCropList: '/api/Loans/GetAllCropList'
    },
    userEndpoints: {
      subsidiaryDatabase: '/api/Loans/GetSubsidiaryLoanList'
    }
  },
  marginScale: 1000000,
  uid: 'userid',
  officeid: 'officeid',
  loanidkey: 'selectedloanId',
  usersession: 'sessionid',
  errorbase: 'errors',
  exceptionStorageKey: 'exceptions',
  verificationbase: 'verificationerrorvalues',
  syncRequiredItems: 'syncRequiredItems',
  myPreferences: 'myPreferences',
  schematicsSettings: 'schematicsSettings',
  currentpage: 'currentpage',
  loanGroup: 'loanGroup',
  loanofficerstats: 'loanofficerstats'
};
