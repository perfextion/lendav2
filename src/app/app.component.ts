import { Component, AfterViewInit, OnInit } from '@angular/core';
import {
  Router,
  NavigationStart,
  NavigationCancel,
  NavigationEnd
} from '@angular/router';

import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { LayoutService } from '@lenda/shared/layout/layout.service';
import { LoginService } from '@lenda/login/login.service';
import { PublishService, Sync } from '@lenda/services/publish.service';
import { SharedService } from '@lenda/services/shared.service';
import { PageInfoService } from '@lenda/services/page-info.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  public loadingBarColor = '#2cbb9c';
  public isSidebarExpanded: boolean = true;
  public isUserLoggedIn: boolean;
  public syncItems: Sync[];

  public loading: boolean;

  constructor(
    private router: Router,
    public loanstorage: LocalStorageService,
    public layoutService: LayoutService,
    public loginService: LoginService,
    public publishService: PublishService,
    public sharedService: SharedService,
    public pageInfoService: PageInfoService,
    private sessionStorage: SessionStorageService
  ) {
    this.loading = true;

    router.events.subscribe((res: any) => {
      let url: string = res.url;
      if (url != undefined) {
        if (url.indexOf('login') != -1) {
        } else {
          let on = loanstorage.retrieve(environment.uid);
          let user = sessionStorage.retrieve('UID');
          let refdata = loanstorage.retrieve(environment.referencedatakey);
          if (on == null || !refdata || !user) {
            router.navigateByUrl('login');
          }
        }
      }

      if (res instanceof NavigationEnd) {
        //window["Appcues"].page();

        window['userpilot'].anonymous();
      }
    });
  }

  ngOnInit() {
    this.layoutService.isSidebarExpanded().subscribe(value => {
      this.isSidebarExpanded = value;
    });

    // is user logged-in
    this.loginService.isLoggedIn().subscribe(value => {
      this.isUserLoggedIn = value;
    });
  }

  ngAfterViewInit() {
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationStart) {
        this.loading = true;
        if (event.url === '/home/loans') {
          this.sharedService.isLoanListActive(true);
        } else {
          this.sharedService.isLoanListActive(false);
        }
      } else if (
        event instanceof NavigationEnd ||
        event instanceof NavigationCancel
      ) {
        this.loading = false;
      }
    });
  }
}
