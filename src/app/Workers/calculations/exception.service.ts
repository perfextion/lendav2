// import { Injectable } from '@angular/core';
// import { LoanQResponse, ExceptionOperation, RefQuestions, RefHyperField } from '../../models/loan-response.model';
// import { LocalStorageService } from 'ngx-webstorage';
// import { environment } from '../../../environments/environment.prod';
// import { loan_model, LoanException, AssociationTypeCode, ExceptionSource, CalculatedVariables, LoanGroup, LoanStatus } from '../../models/loanmodel';
// import {  isArray } from 'util';
// import { LoggingService } from '@lenda/services/Logs/logging.service';
// import { DataService } from '@lenda/services/data.service';
// import { LoanMaster, RefPendingAction, RefDataModel } from '@lenda/models/ref-data-model';
// import { AddPendingActionModel, Remove_PA } from '@lenda/models/pending-action/add-pending-action.model';
// import { LoanApiService } from '@lenda/services/loan/loanapi.service';

// @Injectable()
// export class ExceptionService {
//   refData : RefDataModel;
//   localLoanObject : loan_model;

//   constructor(
//     private localStorageService : LocalStorageService,
//     private loggingservice:LoggingService,
//     private dataService : DataService,
//     private loanapi: LoanApiService
//   ) {
//     this.refData = this.localStorageService.retrieve(environment.referencedatakey);
//     this.localLoanObject= this.localStorageService.retrieve(environment.loankey);

//     //refdata get api is taking time, so for a safer side putting observer on refData
//     this.localStorageService.observe(environment.referencedatakey).subscribe(refdata=>{
//       if(refdata){
//         this.refData = refdata;
//       }
//     });

//     this.dataService.getLoanObject().subscribe((res: loan_model)=>{
//       if(res){
//         this.localLoanObject = res;
//       }
//     });
//   }


//   async logQuestionExceptionIfApplicable(response : LoanQResponse){

//     let respectiveQue = (this.refData.RefQuestions as Array<RefQuestions>).find(que=>response.Question_ID == que.Question_ID);
//     if(respectiveQue){
//       try{
//         this.resetQExceptionClasses(response.Question_ID);
//         this.logException(response.Response_Detail,respectiveQue,ExceptionSource.Question);
//         // this.storeLocalLoanObject(this.localLoanObject);

//       }catch(e){
//         console.error(`Error occurred while logging exception for qustion ${JSON.stringify(respectiveQue)}`);
//       }
//     }
//   }



//   async logHyperFieldExceptions(){

//     //remove all hyperfied exception and all are going to be recalculated
//     //this.localLoanObject.LoanExceptions = this.localLoanObject.LoanExceptions.filter(le =>le.Field_Ind == ExceptionSource.Question);

//     //calculated all hyperfield related vars
//     this.calculateHyperfieldVariables();

//     //check exceptions for all hyperfields
//     const hyperFields: Array<RefHyperField> = this.refData.RefHyperFields;

//     let hyperFieldsExc = hyperFields.filter(h => h.HyperField_Type == 1);

//     // Exception Routine
//     hyperFieldsExc.forEach(hypField => {
//       try{
//         this.processHyperFieldException(hypField);
//       }catch(e){
//         console.error(`Error occurred while processing hyperfield ${JSON.stringify(hypField)}`);
//       }
//     });

//     this.delete_Exceptions();
//     // Tolerance Level Routine
//     this.processToleranceLevelHyperFields(hyperFields);

//     this.storeLocalLoanObject(this.localLoanObject);
//     console.log('completes hyper field exception calc');

//   }

//   private delete_Exceptions() {
//     this.localLoanObject.LoanExceptions.filter(a => {
//       return (
//         a.Action_Status == 0
//       );
//     }).forEach(a => {
//       a.Action_Status = 3;
//     });
//   }

//   processToleranceLevelHyperFields(hyperFields: Array<RefHyperField>) {
//     let loanGroups: Array<LoanGroup> = this.localStorageService.retrieve(
//       environment.loanGroup
//     );

//     try {
//       if(loanGroups && loanGroups.length > 0) {
//         if(
//           this.localLoanObject.LoanMaster.Loan_Status == LoanStatus.Working ||
//           this.localLoanObject.LoanMaster.Loan_Status == LoanStatus.Recommended
//           ) {

//         let uploadedLoans = loanGroups.filter(a => a.IsCurrentYearLoan)
//           .filter(a => a.LoanJSON.LoanMaster.Loan_Status == LoanStatus.Uploaded);

//           if(uploadedLoans.length > 1) {
//             throw "Multiple Uploaded Versions of Loan";
//           } else {
//             if(uploadedLoans.length > 0) {

//               let uploadedLoanMaster = uploadedLoans[0].LoanJSON.LoanMaster as LoanMaster;
//               let currentLoanMaster = this.localLoanObject.LoanMaster as LoanMaster;

//               let toleraneLevelHyperFields = hyperFields.filter(h => h.HyperField_Type == 2);
//               let levels = [];

//               toleraneLevelHyperFields.forEach(hyperfield => {
//                 try {

//                   let uploadedLoanValue = uploadedLoanMaster[hyperfield.HyperField_Name] || 0;
//                   let currentLoanValue = currentLoanMaster[hyperfield.HyperField_Name] || 0;

//                   let diff = Math.abs(uploadedLoanValue - currentLoanValue);

//                   let level = this.getExceptionLevel(diff, hyperfield);
//                   levels.push(level);

//                 } catch (ex) {
//                   console.error(`Error occurred while processing hyperfield ${JSON.stringify(hyperfield)} - ${ex}`);
//                 }
//               });

//               const max_level = Math.max(...levels);
//               this.addPendingAction(max_level);
//             }
//           }
//         }
//       }
//     } catch (ex) {
//       console.error(`Error occurred while processing ToleranceLevelHyperFields ${ex}`);
//     }
//   }

//   private addPendingAction(max_level: number) {
//     try {

//       let PA_Code: string;
//       if(!max_level){
//         PA_Code = "WST";

//         const remove_pa = <Remove_PA> {
//           LoanFullId: this.localLoanObject.Loan_Full_ID,
//           User_Id: this.localLoanObject.LoanMaster.Loan_Officer_ID,
//           PA_Code: PA_Code
//         };

//         this.loanapi.removePendingAction(remove_pa)
//         .subscribe(res => {
//           this.localLoanObject.User_Pending_Actions = res.Data;
//           this.dataService.setLoanObject(this.localLoanObject);
//         });

//       } else {
//         if(max_level == 1) {
//           PA_Code = 'WST2';
//         } else {
//           PA_Code = 'WST3';
//         }

//         let workingPA = this.refData.RefPendingActions.find(a => a.PA_Code == PA_Code) as RefPendingAction;

//         const workingPAModel = <AddPendingActionModel>{
//           LoanFullId: this.localLoanObject.Loan_Full_ID,
//           FromUserId: 0,
//           ToUserIds: [ this.localLoanObject.LoanMaster.Loan_Officer_ID ],
//           Ref_PA_Id: workingPA.Ref_PA_ID,
//           PA_Group: workingPA.PA_Group
//         };

//         this.loanapi.addPendingAction(workingPAModel)
//         .subscribe(res => {
//           this.localLoanObject.User_Pending_Actions = res.Data;
//           this.dataService.setLoanObject(this.localLoanObject);
//         });
//       }
//     } catch (ex) {
//       console.error('Error while adding pending action.', ex);
//     }
//   }

//   private compareWithRawLoan(
//     current_loanobject: loan_model,
//     raw_loanobject: loan_model,
//     property_key: string
//     ) {
//     if(current_loanobject && raw_loanobject) {
//       return (
//         current_loanobject[property_key] ==
//         raw_loanobject[property_key]
//       );
//     }
//   }

//   calculateHyperfieldVariables(){
//     try{
//     if(environment.isDebugModeActive) console.time('Calculate Exception Variables');
//     if(this.localLoanObject ){
//       if(this.localLoanObject.Association){

//         if(!this.localLoanObject.CalculatedVariables) {
//           this.localLoanObject.CalculatedVariables = <CalculatedVariables>{};
//         }
//       //Total CreditRef
//       let creditRefs = this.localLoanObject.Association.filter(assoc=>assoc.Assoc_Type_Code == AssociationTypeCode.CreditRererrence && assoc.ActionStatus!=3);
//       this.localLoanObject.CalculatedVariables.FC_Total_CreditRef = creditRefs.length;

//       //Credit ref without Response
//       let creditRefsWithNOResp = creditRefs.filter(crf=>!crf.Response);
//       this.localLoanObject.CalculatedVariables.FC_Total_CRF_Response = creditRefsWithNOResp.length;

//       //Crefit Ref with Nuetral or Negetive respone
//       let creditRefWithNegResp = creditRefs.filter(crf=> ['Neutral','Negative'].includes(crf.Response));
//       this.localLoanObject.CalculatedVariables.FC_Total_CRF_Neg_Response = creditRefWithNegResp.length;

//       //AdCol Contract Type
//       let contractsWExc = this.refData.ContractType.filter(contract => contract.Exception_Ind == 'Y').map(contract => contract.Contract_Type_Code);
//       let countContractWExc = this.localLoanObject.LoanCollateral.filter(col => contractsWExc.includes(col.Contract_Type_Code));
//       this.localLoanObject.CalculatedVariables.FC_ADCOL_Total_Contract_Type_Exception_Ind = countContractWExc.length;

//       //AdCol Contract Measure Code Type
//       let measWExc = this.refData.MeasurementType.filter(meas => meas.Exception_Ind == 'Y').map(meas => meas.Measurement_Type_Code);
//       let countMeasWExc = this.localLoanObject.LoanCollateral.filter(meas => measWExc.includes(meas.Measure_Code));
//       this.localLoanObject.CalculatedVariables.FC_ADCOL_Total_Measure_Code_Exception_Ind = countMeasWExc.length;

//       //Mtk Contract Type
//       // LoanMarketingContracts.Crop_Type_Code
//       let countMktContractWExc = this.localLoanObject.LoanMarketingContracts.filter(mkt => contractsWExc.includes(mkt.Contract_Type_Code));
//       this.localLoanObject.CalculatedVariables.FC_MKT_Total_Contract_Type_Exception_Ind = countMktContractWExc.length;

//       //Highlight
//     }

//     // Crop yield : no of years of history provided
//     if(this.localLoanObject.CropYield){

//       let cropYear = this.localLoanObject.LoanMaster.Crop_Year;

//       let years = [];
//       for(let i=1; i<8;i++){
//         years.push(cropYear-i);
//       };

//       let FC_Year_History_Provided : number = 0;
//       this.localLoanObject.CropYield.forEach(crop => {
//         crop.FC_Year_History_Provided = 0;
//         years.forEach(year => {
//           if(crop[year]){
//             crop.FC_Year_History_Provided++;
//           }
//         });

//       });


//     }
//     if(environment.isDebugModeActive) console.timeEnd( 'Calculate Exception Variables');
//     }
//     // this.storeLocalLoanObject(this.localLoanObject);
//   }
//   catch{
//     //move one
//   }

//   }
//   processHyperFieldException(exceptionObj : RefHyperField){
//     let value = '';
//       if(exceptionObj.HyperField_TableName && isArray(this.evaluateExpression (exceptionObj.HyperField_TableName))){
//         let entities = this.evaluateExpression(exceptionObj.HyperField_TableName);
//         entities.forEach(entity => {
//           this.resetExceptionClasses(exceptionObj.Exception_ID);
//             let value = this.getHyperFieldValue(exceptionObj,entity);
//           let arrayItemId = this.getParsedText(exceptionObj.Array_Item_ID_Exp,value,exceptionObj.HyperField_TableName,entity);
//           this.logException(value,exceptionObj,ExceptionSource.HyperField, exceptionObj.HyperField_TableName, entity,arrayItemId);
//         });

//       }else{
//           this.resetExceptionClasses(exceptionObj.Exception_ID);
//           let value = this.getHyperFieldValue(exceptionObj);
//           this.logException(value,exceptionObj,ExceptionSource.HyperField);
//       }
//     //this.logException(value,exceptionObj);

//   }

//   private resetExceptionClasses(Exception_ID){
//     let items = document.querySelectorAll('#Exception_ID_' + Exception_ID);
//     Array.from(items).forEach((item) => {
//       item.classList.remove('cell-exception','exception-1','exception-2');
//     });
//   }

//   private resetQExceptionClasses(Question_ID){
//     if(Question_ID){
//       let items = document.querySelectorAll('#questions_' + Question_ID + ' .question-input-field');
//       Array.from(items).forEach((item) => {
//         item.classList.remove('cell-exception','exception-1','exception-2');
//       });
//     }
//   }

//   getHyperFieldValue(exceptionObj : RefHyperField, entity : any = undefined){
//     if(exceptionObj.HyperField_Expression){
//       let parsedExp;
//       if(entity){
//         parsedExp = this.getParsedText(exceptionObj.HyperField_Expression,'', exceptionObj.HyperField_TableName,entity); //TODO
//       }else{
//         parsedExp = this.getParsedText(exceptionObj.HyperField_Expression); //TODO
//       }
//       if(parsedExp){
//         let expValue = eval(parsedExp);
//         return this.isDecimal(expValue) ? expValue.toFixed(2) : expValue;
//       }else{
//         return undefined;
//       }
//     }else{
//       if(entity){
//         return this.evaluateExpression(exceptionObj.HyperField_Name,entity);
//       }else {
//         return this.evaluateExpression(exceptionObj.HyperField_TableName+"."+exceptionObj.HyperField_Name);
//       }
//     }
//   }

//   getExceptionLevel(value : any, exceptionObj : RefHyperField | RefQuestions | any){
//     let levels = exceptionObj.ExceptionLevels.sort((a,b)=>b.Exception_ID_Level - a.Exception_ID_Level);
//     let exceptionLevel = undefined;

//     //Excecute Warning Validations First Before Hard-Stop to override warning if Ever
//     levels = levels.sort((a,b) => (a.Exception_ID_Level > b.Exception_ID_Level) ? 1 : ((b.Exception_ID_Level > a.Exception_ID_Level) ? -1 : 0));

//     levels.some(level => {
//       if([ExceptionOperation.Greater,ExceptionOperation.GreaterEqual ,ExceptionOperation.Lesser,ExceptionOperation.LesserEqual].includes(level.Threshold_Operative)){
//         //numeric comparision
//         if(eval('parseFloat(value) '+level.Threshold_Operative+' parseFloat(level.Threshold_Value)')){
//           exceptionLevel = level.Exception_ID_Level;
//           // return true;
//         }
//       }else if(ExceptionOperation.Equal == level.Threshold_Operative){

//         if(value == level.Threshold_Value || (exceptionObj.HyperField_Name == "Borrower_Rating" && value.length == level.Threshold_Value)){
//           exceptionLevel = level.Exception_ID_Level;
//           // return true;
//         }
//       }

//     });

//     return exceptionLevel;

//   }

//   isNotNullUndefinedAndEmpty(value){
//     return (value !=null && value !=undefined && value!='')
//   }

//   logException(value : any,exceptionObj : RefQuestions | RefHyperField | any, exceptionSource : ExceptionSource, primaryEntityName :string = undefined,entity : any = undefined,arrayItemIdentifier : string = ''){
//     let exLevel = this.getExceptionLevel(value,exceptionObj);
//     if(exLevel){
//       let updatedMessage = this.getParsedText(exceptionObj.Exception_ID_Text,value,primaryEntityName, entity);
//       // this.resetQExceptionClasses(exceptionObj.Question_ID);
//       this.saveException(exceptionObj,exceptionSource, exLevel,updatedMessage,arrayItemIdentifier);
//       this.highlightQuestion(exceptionObj, exLevel);

//     }else{
//       // this.resetQExceptionClasses(exceptionObj.Question_ID);
//       this.deleteExceptionIfExist(exceptionObj,arrayItemIdentifier);
//     }
//   }

//   /**
//    * Higlight Question Radio Buttons / Inputs based on Exception.
//    * @param exceptionObj The Question Object.
//    * @param exLevel The level of exception.
//    */
//   private highlightQuestion(exceptionObj, exLevel){
//     //Highlight Question and Its Input Field
//     if(exceptionObj.Question_ID){
//       let classAnswer = exceptionObj.FC_Response_Detail == 'Yes' && exceptionObj.Choice1 == 'Yes' ? 'questions-container__answer--choice1' :
//       exceptionObj.FC_Response_Detail == 'Yes' && exceptionObj.Choice2 == 'Yes' ? 'questions-container__answer--choice2' :
//       exceptionObj.FC_Response_Detail == 'No' && exceptionObj.Choice1 == 'No' ? 'questions-container__answer--choice1' :
//       exceptionObj.FC_Response_Detail == 'No' && exceptionObj.Choice2 == 'No' ? 'questions-container__answer--choice2' : 'no-value';
//       let question = document.querySelectorAll('#questions_' + exceptionObj.Exception_ID + ' .' + classAnswer);

//       if(question.length < 1){
//         question = document.querySelectorAll('#questions_' + exceptionObj.Exception_ID + ' .question-input-field');
//       }

//       Array.from(question).forEach((ques) => {
//         let classes =  this.getQuestionClass(exLevel);
//           classes.forEach(classSCSS => {
//             ques.classList.add(classSCSS)
//         });
//       });
//     }else{

//       //Check if input Field
//       let items = document.querySelectorAll('#Exception_ID_' + exceptionObj.Exception_ID);

//       //Chech if

//       Array.from(items).forEach((item) => {
//         let classes =  this.getQuestionClass(exLevel);
//           classes.forEach(classSCSS => {
//             item.classList.add(classSCSS)
//         });
//       });

//     }



//   }

//   /**
//    * Provide Exception Classes.
//    * @param level The level of exception.
//    */
//   private getQuestionClass(level: number){
//     switch(level){
//       case 1:
//         return ['cell-exception','exception-1'];
//       case 2:
//         return ['cell-exception','exception-2'];
//       default:
//         return [];
//     }
//   }

//    /**
//    * Will evaluate the expression.
//    * if entity is passed, expression variable should only send the key to find in it
//    * if entity is NOT passed, it will try to evalulate expression in local loan object
//    * @param expression the look up key, (ex: FSN, LoanMaster[0].Crop_Year)
//    * @param entity [optional] if the expression should be looked up from entity object instead of local loan object
//    */
//   evaluateExpression(expression : string,entity : any = undefined ){
//     let evalValue;
//     if(entity){
//       evalValue = eval('entity.'+expression);
//     }else{
//       evalValue = eval('this.localLoanObject.'+expression);

//       if(expression == "LoanMaster.Borrower_Rating"){
//         evalValue = '*'.repeat(evalValue);
//       }
//     }

//     return this.isDecimal(evalValue) ? (evalValue as number).toFixed(2) : evalValue;
//   }

//   isDecimal(value : any){
//     return (!isNaN(value)) && (value % 1 !=0);
//   }
//   getParsedText(textToParse :string, value : any = '' ,primaryEntityName : string =undefined,entity: any = undefined){
//     const symbol = '##';
//     if(textToParse && textToParse.includes('##')){
//       let clonnedMessage = textToParse;
//       let variables = textToParse.split('##').filter((element, index)=> index%2 == 1);
//       variables.forEach(variable => {
//         if(variable == 'Response_Value'){
//           clonnedMessage = clonnedMessage.replace('##'+variable+'##',value);
//         }
//         else if(variable.includes('.') && variable.split('.')[0] == primaryEntityName){
//             clonnedMessage = clonnedMessage.replace('##'+variable+'##',this.evaluateExpression(variable.split('.')[1],entity));
//         }else{
//             clonnedMessage = clonnedMessage.replace('##'+variable+'##',this.evaluateExpression(variable));
//         }
//       });
//       return clonnedMessage;
//     }else{
//       return textToParse;
//     }

//   }



//   saveException(exceptionObj : RefQuestions | RefHyperField, exceptionSource :ExceptionSource,  exeLevel : number, exeMessage : string, arrayItemIdentifier : string = ''){
//     if(this.localLoanObject ){
//       let exception;
//       if(arrayItemIdentifier){
//         exception = this.localLoanObject.LoanExceptions.find(ex=>ex.Array_Item_ID == arrayItemIdentifier && ex.Exception_ID == exceptionObj.Exception_ID);
//       }else{
//         exception = this.localLoanObject.LoanExceptions.find(ex=>ex.Exception_ID == exceptionObj.Exception_ID);
//       }

//       if(!exception){
//         let newException = new LoanException();
//         newException = this.populateLoanExceptionObject(newException, exceptionObj, exeMessage, exeLevel, exceptionSource,arrayItemIdentifier);
//         newException.Action_Status = 1;
//         this.localLoanObject.LoanExceptions.push(newException);
//       }else{
//         // exception = this.populateLoanExceptionObject(exception, exceptionObj, exeMessage, exeLevel, exceptionSource,arrayItemIdentifier);
//         let index = this.localLoanObject.LoanExceptions.findIndex(ex=>ex.Exception_ID == exceptionObj.Exception_ID);
//         this.localLoanObject.LoanExceptions[index].Exception_Mitigation_Text = exeMessage;
//         this.localLoanObject.LoanExceptions[index].Exception_ID_Level = exeLevel;
//         this.localLoanObject.LoanExceptions[index].Sort_Order = exceptionObj.Sort_Order;
//         this.localLoanObject.LoanExceptions[index].Field_Ind = exceptionSource;
//         this.localLoanObject.LoanExceptions[index].Array_Item_ID = arrayItemIdentifier;
//         this.localLoanObject.LoanExceptions[index].Is_Justification_Required = exceptionObj.Is_Justification_Required;
//         this.localLoanObject.LoanExceptions[index].Tab_ID = exceptionObj.Tab_ID || this.getTabId(exceptionObj.Chevron_ID);
//         if(this.localLoanObject.LoanExceptions[index].Loan_Exception_ID){
//           this.localLoanObject.LoanExceptions[index].Action_Status = 2;
//         }else{
//           this.localLoanObject.LoanExceptions[index].Action_Status = 1;
//         }
//       }
//       // this.storeLocalLoanObject(this.localLoanObject);
//     }
//   }

//   private populateLoanExceptionObject(loanexception: LoanException, exceptionObj: RefQuestions | RefHyperField, exeMessage: string, exeLevel: number, exceptionSource: ExceptionSource,  arrayItemIdentifier : string = '') {
//     loanexception.Loan_Full_ID = this.localLoanObject.Loan_Full_ID;
//     loanexception.Loan_Seq_Num = this.localLoanObject.LoanMaster.Loan_Seq_num;
//     loanexception.Chevron_ID = exceptionObj.Chevron_ID;
//     loanexception.Exception_ID = exceptionObj.Exception_ID;
//     loanexception.Exception_Mitigation_Text = exeMessage;
//     loanexception.Exception_ID_Level = exeLevel;
//     loanexception.Sort_Order = exceptionObj.Sort_Order;
//     loanexception.Field_Ind = exceptionSource;
//     loanexception.Array_Item_ID = arrayItemIdentifier;
//     loanexception.Is_Justification_Required = exceptionObj.Is_Justification_Required;
//     loanexception.Tab_ID = exceptionObj.Tab_ID || this.getTabId(exceptionObj.Chevron_ID);
//     return loanexception;
//   }

//   private getTabId(Chevron_ID: number) {
//     try{
//       let chevron = this.refData.RefChevrons.find(a => a.Chevron_ID == Chevron_ID);
//       return chevron.Tab_ID;
//     } catch {
//       return null;
//     }
//   }

//   deleteExceptionIfExist(exceptionObj : RefQuestions | RefHyperField, arrayItemIdentifier : string = ''){
//     if(this.localLoanObject ){

//       let exception;

//       if(arrayItemIdentifier){
//         exception = this.localLoanObject.LoanExceptions.find(ex=>ex.Array_Item_ID == arrayItemIdentifier && ex.Exception_ID == exceptionObj.Exception_ID);
//       }else{
//         exception = this.localLoanObject.LoanExceptions.find(ex=>ex.Exception_ID == exceptionObj.Exception_ID);
//       }

//       if (exception){
//         if(exception.Loan_Exception_ID){
//           exception.Action_Status = 3;
//         }else{
//           let exceptionIndex = this.localLoanObject.LoanExceptions.findIndex(ex=>ex.Exception_ID == exceptionObj.Exception_ID);
//           this.localLoanObject.LoanExceptions.splice(exceptionIndex,1);
//         }
//         //this.storeLocalLoanObject(this.localLoanObject);
//       }
//     }
//   }

//   // this takes 4 seconds and is invoked 9 times
//   storeLocalLoanObject(localLoanObject : loan_model){
//     console.time("ExceptionStoringProcess - storeLocalLoanObject invoked");

//     // let starttime=new Date().getTime();
//     //localStorage.setItem(environment.loankey,JSON.stringify(localLoanObject))
//     this.localStorageService.store(environment.loankey, localLoanObject);
//     // let excendtime=new Date().getTime();
//     // this.loggingservice.checkandcreatelog(1, 'ExceptionStoringProcess', " timetaken :" + (excendtime - starttime).toString() + " ms");

//     console.timeEnd("ExceptionStoringProcess - storeLocalLoanObject invoked")
//   }
// }
