import { Injectable } from '@angular/core';
import { loan_model } from '../../models/loanmodel';
import * as _ from 'lodash'
@Injectable()
export class LoancroppracticeworkerService {

  constructor() { }
//This will Aggregate the FC Values on Loan Crop Practice
  performcalculations(input:loan_model){
    
    input.LoanCropPractices.forEach(cp => {
      //get respective cropunits
      let cunints=input.LoanCropUnits.filter(p=>p.Crop_Practice_ID==cp.Crop_Practice_ID);
      cp.FC_LCP_Ins_Value_Abc=_.sumBy(cunints,'FC_Abcvalue');
      cp.FC_LCP_Ins_Value_Crophail=_.sumBy(cunints,'FC_Crophailvalue');
      cp.FC_LCP_Ins_Value_Hmax=_.sumBy(cunints,'FC_Hmaxvalue');
      cp.FC_LCP_Ins_Value_Ice=_.sumBy(cunints,'FC_Icevalue');
      cp.FC_LCP_Ins_Value_Mpci=_.sumBy(cunints,'FC_MPCIvalue');
      cp.FC_LCP_Ins_Value_Pci=_.sumBy(cunints,'FC_Pcivalue');
      cp.FC_LCP_Ins_Value_Ramp=_.sumBy(cunints,'FC_Rampvalue');
      cp.FC_LCP_Ins_Value_Sco=_.sumBy(cunints,'FC_Scovalue');
      cp.FC_LCP_Ins_Value_Stax=_.sumBy(cunints,'FC_Staxvalue');

      //Discounted Values
      cp.FC_LCP_Disc_Ins_Value_Abc=_.sumBy(cunints,'FC_Disc_Abcvalue');
      cp.FC_LCP_Disc_Ins_Value_Crophail=_.sumBy(cunints,'FC_Disc_Crophailvalue');
      cp.FC_LCP_Disc_Ins_Value_Hmax=_.sumBy(cunints,'FC_Disc_Hmaxvalue');
      cp.FC_LCP_Disc_Ins_Value_Ice=_.sumBy(cunints,'FC_Disc_Icevalue');
      cp.FC_LCP_Disc_Ins_Value_Mpci=_.sumBy(cunints,'FC_Disc_MPCI_value');
      cp.FC_LCP_Disc_Ins_Value_Pci=_.sumBy(cunints,'FC_Disc_Pcivalue');
      cp.FC_LCP_Disc_Ins_Value_Ramp=_.sumBy(cunints,'FC_Disc_Rampvalue');
      cp.FC_LCP_Disc_Ins_Value_Sco=_.sumBy(cunints,'FC_Disc_Scovalue');
      cp.FC_LCP_Disc_Ins_Value_Stax=_.sumBy(cunints,'FC_Disc_Staxvalue');
     
    });

    return input;
    
  }

}
