import { Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { loan_model, LoanGroup, LoanStatus, Cross_Collateralized_Loans } from '@lenda/models/loanmodel';
import { errormodel } from '@lenda/models/commonmodels';
import { Loansettings } from '@lenda/models/loansettings';
import { precise_round } from '@lenda/services/common-utils';
import { daysDiff } from '@lenda/aggridformatters/valueformatters';
import { LoanMaster, MigratedLoanAdjustmentKeys } from '@lenda/models/ref-data-model';

import { Borrowerincomehistoryworker } from './borrowerincomehistoryworker.service';
import { LoggingService } from '../../services/Logs/logging.service';
import { LoancropunitcalculationworkerService } from './loancropunitcalculationworker.service';
import { LoancrophistoryService } from './loancrophistory.service';
import { FarmcalculationworkerService } from './farmcalculationworker.service';
import { AssociationcalculationworkerService } from './associationcalculationworker.service';
import { Collateralcalculationworker } from './collateralcalculationworker.service';
import { QuestionscalculationworkerService } from './questionscalculationworker.service';
import { LoanMasterCalculationWorkerService } from './loan-master-calculation-worker.service';
import { OverallCalculationServiceService } from './overall-calculation-service.service';
import { MarketingcontractcalculationService } from './marketingcontractcalculation.service';
import { OptimizercalculationService } from './optimizercalculationservice.service';
import { BudgetHelperService } from '../../components/budget/budget-helper.service';
import { DataService } from '@lenda/services/data.service';
import { wfrpworker } from './wfrpworker.service';
import { LoanInventoryService } from '@lenda/components/reports/loan-inventory/loan-inventory.service';
import { LoanTypeCalculationService } from '@lenda/services/loan-type.service';
import { BudgetcalculationworkerService } from './budgetcalculationworker.service';
import { TermsCalculationWorkerService } from './termscalculationworker.service';
import { ExceptionWorkerService } from './exceptionworker.service';
import { CropPriceWorkerService } from './croppriceworker.service';
import { RefVCAFieldsWorkerService } from './refvcafieldsworker.service';
import { RefDataService } from '@lenda/services/ref-data.service';
import { LoanToleranceCalculationService } from './loantolerancecalculatonworker.service';
import { ValidationService } from './validation.service';
import { MigratedLoanAdjustmentHelper } from '../utility/migrated-loan-adjustments-helper';

@Injectable()
export class LoancalculationWorker {
  constructor(
    private localst: LocalStorageService,
    private borrowerincomehistory: Borrowerincomehistoryworker,
    private loancropunitworker: LoancropunitcalculationworkerService,
    private loancrophistoryworker: LoancrophistoryService,
    private farmcalculation: FarmcalculationworkerService,
    private collateralcalculation: Collateralcalculationworker,
    private questionscalculations: QuestionscalculationworkerService,
    private loanMasterCalcualtions: LoanMasterCalculationWorkerService,
    private budgethelper: BudgetHelperService,
    private overallCalculationService: OverallCalculationServiceService,
    private associationcalculation: AssociationcalculationworkerService,
    public logging: LoggingService,
    private marketingContractService: MarketingcontractcalculationService,
    private optimizercaluclations: OptimizercalculationService,
    private dataService: DataService,
    private wfrpworkerobject: wfrpworker,
    private loanInventory: LoanInventoryService,
    private loanTypecalculation: LoanTypeCalculationService,
    private budgetCalculation: BudgetcalculationworkerService,
    private termcalculation: TermsCalculationWorkerService,
    private exceptionWorker: ExceptionWorkerService,
    private croppriceworker: CropPriceWorkerService,
    private vcaFieldsWorker: RefVCAFieldsWorkerService,
    private refDataService: RefDataService,
    private loanToleranceCalculationService: LoanToleranceCalculationService,
    private validationservice: ValidationService
  ) { }

  performcalculationonloanobject(
    localloanobj: loan_model,
    recalculate?: boolean,
    firstTimeCalculation = false
  ) {
    console.time('Loan Calculation');

    if (recalculate == undefined) {
      recalculate = true; // by default we are taking that it needs calculation
    }

    if (recalculate) {
      // this.logging.checkandcreatelog(1, 'Calculationforloan', "LoanCalculation Started");
      //STEP 1 -- BORROWER CALCULATIONS

      // if (localloanobj.Borrower != null) {
      //   localloanobj.Borrower = this.borrowerworker.prepareborrowermodel(
      //     localloanobj.Borrower
      //   );
      // }

      this.croppriceworker.generateCropPrice(localloanobj);

      if (localloanobj.BorrowerIncomeHistory !== null) {
        localloanobj = this.borrowerincomehistory.prepareborrowerincomehistorymodel(
          localloanobj
        );
      }

      if (localloanobj.Association) {
        let sumofthirdparty = _.sumBy(localloanobj.Association.filter(a => a.Assoc_Type_Code == 'THR' && a.ActionStatus != 3), a => parseFloat(a.Amount as any) || 0);
        localloanobj.LoanMaster.Third_Party_Credit = sumofthirdparty;
      }

          //STEP 2 -- CROP LEVEL CALCULATIONS

      if (localloanobj.LoanCropUnits) {
        localloanobj = this.loancropunitworker.prepareLCP_APHfromAPH(
          localloanobj
        );

        localloanobj = this.loancropunitworker.prepareRate_YieldfromYield(
          localloanobj
        );

        localloanobj = this.loancropunitworker.prepare_Rent_Percent(
          localloanobj
        );
      }

      if (localloanobj.CropYield != null) {
        localloanobj = this.loancrophistoryworker.prepareLoancrophistorymodel(
          localloanobj
        );
      }
      if (
        localloanobj.LoanCropUnits != null &&
        localloanobj.LoanCropPractices != null
      ) {
        localloanobj = this.optimizercaluclations.performcalculations(
          localloanobj
        );
      }


      if (localloanobj.LoanCropUnits) {
        localloanobj = this.loancropunitworker.prepareLoancropunitmodel(
          localloanobj
        );
        localloanobj = this.loancropunitworker.fillFCValuesforCropunits(
          localloanobj
        );

        this.loancropunitworker.preparePrimaryAIPandAgency(localloanobj);

      }

      //  WFRP BEfore Loan Insurance
      localloanobj = this.wfrpworkerobject.performWfrpCalculation(localloanobj);

      localloanobj.LoanMaster.Ins_Value_Crops = precise_round(localloanobj.LoanMaster.FC_Net_Market_Value_Insurance || 0, 0);
      localloanobj.LoanMaster.Net_Market_Value_Insurance = precise_round((localloanobj.LoanMaster.FC_Net_Market_Value_Insurance || 0) + localloanobj.Loanwfrp.Wfrp_Value, 0);
      // MigratedLoanAdjustmentHelper.Adjust_Value_By_Key(localloanobj, 'Net_Market_Value_Insurance', MigratedLoanAdjustmentKeys.Ins_Value_Crops);


      localloanobj.LoanMaster.Disc_value_Insurance = (localloanobj.LoanMaster.FC_Disc_value_Insurance || 0) + localloanobj.Loanwfrp.Wfrp_Disc_Value;
      // MigratedLoanAdjustmentHelper.Adjust_Value_By_Key(localloanobj, 'Disc_value_Insurance', MigratedLoanAdjustmentKeys.Disc_Ins_Value_Crops);

      //STEP 3 --- FARM CALCULATIONS
      if (localloanobj.Farms != null) {
        localloanobj = this.farmcalculation.prepareLoanfarmmodel(localloanobj);
      }

      //Populate Total Acres at LoanMaster level
      localloanobj.LoanMaster.Total_Acres = precise_round(_.sumBy(localloanobj.Farms, f => f.FC_Total_Acres || 0), 1);

      // fixed expense - cash rent calculation
      localloanobj = this.budgetCalculation.prepareCashrentBudgetModel(localloanobj);

      //STEP 4 --- INSURANCE POLICIES
      //******************************** no code in the file as if now --Tarjeet Singh */
      //localloanobj = this.insuranceworker.performcalculations(localloanobj);

      // Close date and Estimated days
      try {
        if (!localloanobj.LoanMaster.Close_Date) {
          localloanobj.LoanMaster.Close_Date = new Date();
        } else {
          let d = new Date(localloanobj.LoanMaster.Close_Date);
          if (d.getFullYear() == 1900) {
            localloanobj.LoanMaster.Close_Date = new Date();
          }
        }
      } catch {
        localloanobj.LoanMaster.Close_Date = new Date();
      }


      if (!localloanobj.LoanMaster.Maturity_Date) {
        localloanobj.LoanMaster.Maturity_Date = this.refDataService.getDefaultMaturityDateString(
          localloanobj.LoanMaster.Crop_Year
        );
      }

      localloanobj.LoanMaster.Estimated_Days = daysDiff(
        localloanobj.LoanMaster.Maturity_Date,
        localloanobj.LoanMaster.Close_Date
      );

      //STEP 5 --- BUDGET CALCULATIONS
      try {
        if (
          localloanobj.LoanBudget &&
          localloanobj.LoanBudget.length > 0 &&
          localloanobj.LoanCropPractices
        ) {
          localloanobj = this.budgethelper.getInsurancePremiums(localloanobj);

          localloanobj.LoanCropPractices.forEach(cp => {
            let cpLoanBudget = localloanobj.LoanBudget.filter(
              budget => budget.Crop_Practice_ID == cp.Crop_Practice_ID
            );

            cpLoanBudget
              .filter(p => p.Crop_Practice_ID != 0)
              .forEach(budget => {
                budget.ARM_Budget_Crop = budget.ARM_Budget_Acre * cp.LCP_Acres;

                budget.Distributor_Budget_Crop =
                  budget.Distributor_Budget_Acre * cp.LCP_Acres;

                budget.Third_Party_Budget_Crop =
                  budget.Third_Party_Budget_Acre * cp.LCP_Acres;

                if (budget.Expense_Type_ID != 10) {
                  budget.Total_Budget_Crop_ET =
                    budget.Total_Budget_Acre * cp.LCP_Acres;
                } else {
                  budget.Total_Budget_Crop_ET = budget.Third_Party_Budget_Crop;
                }
              });
          });

          let Arm_Budget_Totals = precise_round(_.sumBy(
            localloanobj.LoanBudget.filter(p => p.Crop_Practice_ID != 0 || (p.Budget_Type == 'F' && p.Expense_Type_ID != 14)),
            p => parseFloat(p.ARM_Budget_Crop.toString())
          ), 0);

          let Dist_Budget_Totals = precise_round(_.sumBy(
            localloanobj.LoanBudget.filter(p => p.Crop_Practice_ID != 0 || (p.Budget_Type == 'F' && p.Expense_Type_ID != 14)),
            p => parseFloat(p.Distributor_Budget_Crop.toString())
          ), 0);

          let ThirdParty_Budget_Totals = precise_round( _.sumBy(
            localloanobj.LoanBudget.filter(p => p.Crop_Practice_ID != 0 || (p.Budget_Type == 'F' && p.Expense_Type_ID != 14)),
            bgt => isNaN(bgt.Third_Party_Budget_Crop) ? 0 : bgt.Third_Party_Budget_Crop
          ), 0);

          //Interest Rate Calulations //
          localloanobj.LoanMaster.FC_Ins_Disc_Collateral_Value = 0;
          localloanobj.LoanMaster.FC_Total_Addt_Collateral_Value = 0;

          localloanobj.LoanMaster.Origination_Fee_Amount = Math.round(
            ((Arm_Budget_Totals + Dist_Budget_Totals) *
              localloanobj.LoanMaster.Origination_Fee_Percent) /
            100
          );

          // MigratedLoanAdjustmentHelper.Adjust_Value(localloanobj, MigratedLoanAdjustmentKeys.Origination_Fee_Amount);

          localloanobj.LoanMaster.Service_Fee_Amount = Math.round(
            ((Arm_Budget_Totals + Dist_Budget_Totals) *
              localloanobj.LoanMaster.Service_Fee_Percent) /
            100
          );

          // MigratedLoanAdjustmentHelper.Adjust_Value(localloanobj, MigratedLoanAdjustmentKeys.Service_Fee_Amount);

          let util_perc = this.optimizercaluclations.getUtilizationPercent() / 100;
          localloanobj.LoanMaster.ARM_Rate_Fee = precise_round((
            (localloanobj.LoanMaster.Interest_Percent / 100) *
            (localloanobj.LoanMaster.Estimated_Days / 365) *
            Arm_Budget_Totals * util_perc), 0);

          localloanobj.LoanMaster.Dist_Rate_Fee = precise_round(
            (localloanobj.LoanMaster.Interest_Percent / 100) *
            (localloanobj.LoanMaster.Estimated_Days / 365) *
            Dist_Budget_Totals * util_perc, 0);

          localloanobj.LoanMaster.Interest_Est_Amount = Math.round(
            localloanobj.LoanMaster.ARM_Rate_Fee +
            localloanobj.LoanMaster.Dist_Rate_Fee
          );

          this.compareWithUploadedVersion(localloanobj);

          localloanobj.LoanMaster.Total_ARM_Fees_And_Interest = precise_round(
            localloanobj.LoanMaster.Origination_Fee_Amount
            + localloanobj.LoanMaster.Service_Fee_Amount
            + (localloanobj.LoanMaster.ARM_Rate_Fee), 0);


          localloanobj.LoanMaster.Total_Return_Fee = localloanobj.LoanMaster.Origination_Fee_Amount +
            localloanobj.LoanMaster.Service_Fee_Amount +
            localloanobj.LoanMaster.Interest_Est_Amount; //Extension Fee should be added

          // update return scale
          this.termcalculation.updateReturnScale(localloanobj);

          localloanobj = this.budgetCalculation.prepareFeesBudget(localloanobj);

          // propogations to Loan master table
          localloanobj.LoanMaster.Arm_Total_Budget = Arm_Budget_Totals + localloanobj.LoanMaster.Origination_Fee_Amount + localloanobj.LoanMaster.Service_Fee_Amount;
          localloanobj.LoanMaster.Dist_Total_Budget = Dist_Budget_Totals;
          localloanobj.LoanMaster.Third_Party_Total_Budget = ThirdParty_Budget_Totals;

          localloanobj.LoanMaster.Loan_Total_Budget = Math.round(
            localloanobj.LoanMaster.Arm_Total_Budget + Dist_Budget_Totals + ThirdParty_Budget_Totals
          );

          localloanobj.LoanMaster.ARM_Commitment = Math.round(Arm_Budget_Totals + localloanobj.LoanMaster.Origination_Fee_Amount + localloanobj.LoanMaster.Service_Fee_Amount);
          localloanobj.LoanMaster.Dist_Commitment = Dist_Budget_Totals;

          // MigratedLoanAdjustmentHelper.Adjust_Value(localloanobj, MigratedLoanAdjustmentKeys.ARM_Commitment);
          // MigratedLoanAdjustmentHelper.Adjust_Value(localloanobj, MigratedLoanAdjustmentKeys.Dist_Commitment);

          localloanobj.LoanMaster.Total_Commitment = Math.round(
            localloanobj.LoanMaster.ARM_Commitment + localloanobj.LoanMaster.Dist_Commitment
          );

          localloanobj.LoanMaster.ActionStatus = 2;
        }

      } catch (e) {
        //level 1 log as it is error
        this.logging.checkandcreatelog(1, 'Calc_Budget', e);
        if (environment.isDebugModeActive) { console.error('ERROR IN BUDGET CALCULATION' + JSON.stringify(e)); }
      }

      //LOAN BUDGET TOTAL CALCULATIONS
      this.budgethelper.prepareTotalBudget(localloanobj);

      localloanobj = this.budgetCalculation.processExpenseTypeExceptions(localloanobj, firstTimeCalculation);
      //STEP 6 --- COLLATERAL CALCULATIONS

      if (localloanobj.LoanCollateral != null && localloanobj.LoanCollateral) {
        localloanobj = this.collateralcalculation.preparecollateralmodel(
          localloanobj
        );
        localloanobj = this.collateralcalculation.performMarketValueCalculations(
          localloanobj
        );
      }

      if (localloanobj.LoanOtherIncomes) {
        localloanobj.LoanMaster.Net_Other_Income = precise_round(_.sumBy(
          localloanobj.LoanOtherIncomes.filter(a => a.ActionStatus != 3),
          other => other.Amount || 0
        ), 2);
      }

           // STEP 7 --- QUESTIONS CALCULATIONS
      if (localloanobj.LoanQResponse != null) {
        localloanobj = this.questionscalculations.performcalculationforquestionsupdated(
          localloanobj
        );
      }
           // STEP 8 --- MASTER CALCULATIONS
      if (localloanobj.LoanMaster !== null) {
        localloanobj = this.loanMasterCalcualtions.performLoanMasterCalcualtions(
          localloanobj
        );
        localloanobj = this.loanMasterCalcualtions.performDashboardCaclulation(
          localloanobj
        );
        localloanobj = this.overallCalculationService.balancesheet_calc(
          localloanobj
        );
      }

         // STEP 9 --- OPTIMIZER CALCULATIONS
      localloanobj = this.optimizercaluclations.performLoanUnitCalculations(localloanobj);

      // STEP 10 --- Marketing Contract Calculations
      if (localloanobj.LoanMarketingContracts && localloanobj.LoanCrops) {
        this.marketingContractService.updateMarketingCalculation(localloanobj);
        this.marketingContractService.performPriceCalculation(localloanobj);
      }

      // calculate variables
      this.exceptionWorker.CalculateHyperfieldVariables(localloanobj);

      // Credit Agreement Fields
      //this.vcaFieldsWorker.Eval_VCA_Fields(localloanobj);
      this.loancropunitworker.prepareCAFields(localloanobj);

      //Step 12
      //Calculate Dashboard Stats

      // const allErrors = this.localst.retrieve(environment.errorbase);
      // for (let i = 0; i < localloanobj.DashboardStats.length; i++) {
      //   const currentStat = localloanobj.DashboardStats[i];
      //   switch (currentStat.title) {
      //     case 'icon-farm':
      //       currentStat.number = allErrors.filter(p => p.tab === 'Farm').length;
      //       currentStat.totalIRAcresText = _.sumBy(localloanobj.Farms, p => p.Irr_Acres);
      //       currentStat.totalNIRAcresText = _.sumBy(localloanobj.Farms, p => p.NI_Acres);
      //       break;
      //   }
      // }

      //STEP 12 SET LOAN TYPE
      //NEED CLARIFICATION ON FIRST LINE AND SECOND CONDITION
      localloanobj.LoanMaster.Loan_Type_Code = this.loanTypecalculation.calculateLoanType(localloanobj);

      // Loan Condition Calculation
      // const userId = this.localst.retrieve(environment.uid);
      // localloanobj.LoanConditions = this.loanConditionWorker.prepareCalculatedLoanConditions(localloanobj, userId);

      // validate 3rd party amount
      this.budgethelper.validateThirdPartyAmount(localloanobj);

      //Delta calculations
      if (environment.isDebugModeActive) { console.time('Dalta Calculations'); }
      localloanobj.LoanMaster.Loan_Tolerance_Level_Ind = 0;

      let loanGroups: Array<LoanGroup> = this.localst.retrieve(environment.loanGroup);
      if (loanGroups && loanGroups.length) {
        let currentLoanMasters: Array<LoanMaster> = [localloanobj.LoanMaster];

        loanGroups = loanGroups.filter(loan => loan.IsCurrentYearLoan && [LoanStatus.Working, LoanStatus.Uploaded].includes(loan.LoanJSON.LoanMaster.Loan_Status));
        loanGroups.forEach(loan => {
          currentLoanMasters.push(loan.LoanJSON.LoanMaster);
        });

        currentLoanMasters = _.orderBy(currentLoanMasters, 'Loan_Full_ID', ['desc']);

        if (this.getLoanStatus(currentLoanMasters[0].Loan_Status) === LoanStatus.Working) {
          //if latest isWorking - > get delta by subtractingg using from last uploaded

          //find last uploaded
          let remainigLoanMasters = [...currentLoanMasters];
          remainigLoanMasters.splice(0, 1);

          let lastUploaded = remainigLoanMasters.find(loan => loan.Loan_Status === LoanStatus.Uploaded);
          if (lastUploaded) {
            this.calculateDeltaValues(localloanobj, currentLoanMasters[0], lastUploaded);
            this.loanToleranceCalculationService.calculateLoanTolerance(localloanobj, lastUploaded);
          } else {
            this.calculateDeltaValues(localloanobj, currentLoanMasters[0], undefined);
          }

        } else if (this.getLoanStatus(currentLoanMasters[0].Loan_Status) === LoanStatus.Uploaded) {
          //if latest is uploaded -> reset deltas
          this.calculateDeltaValues(localloanobj, currentLoanMasters[0], undefined);
        } else {
          //need to test
          this.calculateDeltaValues(localloanobj, undefined, undefined);
        }
      } else {
        this.calculateDeltaValues(localloanobj, localloanobj.LoanMaster, undefined);
      }

      if (environment.isDebugModeActive) { console.timeEnd('Dalta Calculations'); }      //End Delta calculations
    }

    // WFRP Condition
    // const userId = this.localst.retrieve(environment.uid);
    // localloanobj = this.loanConditionWorker.prepare_WFRP_Condition(localloanobj, userId);

    if(localloanobj.LoanMaster.Loan_Tolerance_Level_Ind > 0) {
      this.exceptionWorker.Add_Tolerance_Level_PA(localloanobj);
    } else {
      this.exceptionWorker.Remove_Pending_Action(localloanobj, 'WST2');
      this.exceptionWorker.Remove_Pending_Action(localloanobj, 'WST3');
    }

    localloanobj = this.associationcalculation.prepareLoanassociationmodel(
      localloanobj
    );

    if (recalculate) {
      if (environment.isDebugModeActive) { console.log('object updated with calculations'); }
      //this.logging.checkandcreatelog(1, 'Calculationforloan', "Local Storage updated");
    } else {
      if (environment.isDebugModeActive) { console.log('object updated without calculations'); }
    }

    console.timeEnd('Loan Calculation');

    this.exceptionWorker.Process_Exceptions(localloanobj);

    if(environment.isDebugModeActive) {
      console.time('Validate_Loan_Documents');
    }

    this.validationservice.Validate_Loan_Documents(localloanobj);

    if(environment.isDebugModeActive) {
      console.timeEnd('Validate_Loan_Documents');
    }

    this.dataService.setLoanObject(localloanobj);

    // step 15: update progress chart
    const completedPerc = this.loanInventory.prepareInventoryInformation(localloanobj);
    this.localst.store(environment.completedPercentage, completedPerc);
  }

  private getLoanStatus(_loanStatus: string) {
    if (_loanStatus && _loanStatus.includes('_')) {
      return _loanStatus.substring(0, _loanStatus.indexOf('_'));
    }
    return _loanStatus;
  }

  private calculateDeltaValues(localLoan: loan_model, workingLoan: LoanMaster, prevLoan: LoanMaster) {
    if (workingLoan && prevLoan) {
      localLoan.LoanMaster.Delta_Crop_Acres = (workingLoan.Total_Crop_Acres || 0) - (prevLoan.Total_Crop_Acres || 0);
      localLoan.LoanMaster.Delta_ARM_Commitment = precise_round((workingLoan.ARM_Commitment || 0) - (prevLoan.ARM_Commitment || 0), 0);
      localLoan.LoanMaster.Delta_Dist_Commitment = precise_round((workingLoan.Dist_Commitment || 0) - (prevLoan.Dist_Commitment || 0), 0);
      localLoan.LoanMaster.Delta_Total_Commitment = precise_round((workingLoan.Total_Commitment || 0) - (prevLoan.Total_Commitment || 0), 0);
      localLoan.LoanMaster.Delta_Third_Party_Credit = precise_round((workingLoan.Third_Party_Credit || 0) - (prevLoan.Third_Party_Credit || 0), 0);

      localLoan.LoanMaster.Delta_Origination_Fee_Amount = precise_round((workingLoan.Origination_Fee_Amount || 0) - (prevLoan.Origination_Fee_Amount || 0), 0);
      localLoan.LoanMaster.Delta_Service_Fee_Amount = precise_round((workingLoan.Service_Fee_Amount || 0) - (prevLoan.Service_Fee_Amount || 0), 0);
      localLoan.LoanMaster.Delta_Extension_Fee_Amount = precise_round((workingLoan.ARM_Extension_Fee || 0) - (prevLoan.ARM_Extension_Fee || 0), 0);
      localLoan.LoanMaster.Delta_Total_Return_Fee = (workingLoan.Total_ARM_Fees_And_Interest || 0) - (prevLoan.Total_ARM_Fees_And_Interest || 0);
      localLoan.LoanMaster.Delta_Interest_Est_Amount = (workingLoan.Interest_Est_Amount || 0) - (prevLoan.Interest_Est_Amount || 0);

      localLoan.LoanMaster.Delta_Cash_Flow_Amount = precise_round((workingLoan.Cash_Flow_Amount || 0) - (prevLoan.Cash_Flow_Amount || 0), 1);
      localLoan.LoanMaster.Delta_Cash_Flow_Percent = precise_round((workingLoan.Cash_Flow_Percent || 0) - (prevLoan.Cash_Flow_Percent||0), 1);
      localLoan.LoanMaster.Delta_Risk_Cushion_Percent = precise_round((workingLoan.Risk_Cushion_Percent || 0) - (prevLoan.Risk_Cushion_Percent || 0), 1);
      localLoan.LoanMaster.Delta_Risk_Cushion_Amount = precise_round((workingLoan.Risk_Cushion_Amount || 0)- (prevLoan.Risk_Cushion_Amount || 0), 1);

      localLoan.LoanMaster.Delta_Return_Percent = precise_round((workingLoan.Return_Percent || 0) - (prevLoan.Return_Percent || 0), 1);
      localLoan.LoanMaster.Delta_Current_Addendum_Percent = precise_round((workingLoan.Current_Addendum_Percent || 0) - (prevLoan.Current_Addendum_Percent || 0), 1);

    } else if (workingLoan && !prevLoan) {
      localLoan.LoanMaster.Delta_Crop_Acres = workingLoan.Total_Crop_Acres || 0;
      localLoan.LoanMaster.Delta_ARM_Commitment = precise_round(workingLoan.ARM_Commitment || 0, 0);
      localLoan.LoanMaster.Delta_Dist_Commitment = precise_round(workingLoan.Dist_Commitment || 0, 0);
      localLoan.LoanMaster.Delta_Total_Commitment = precise_round(workingLoan.Total_Commitment || 0, 0);
      localLoan.LoanMaster.Delta_Third_Party_Credit = precise_round(workingLoan.Third_Party_Credit || 0, 1);

      localLoan.LoanMaster.Delta_Origination_Fee_Amount = precise_round(workingLoan.Origination_Fee_Amount || 0, 0);
      localLoan.LoanMaster.Delta_Service_Fee_Amount = precise_round(workingLoan.Service_Fee_Amount || 0, 0);
      localLoan.LoanMaster.Delta_Extension_Fee_Amount = precise_round(workingLoan.ARM_Extension_Fee || 0, 0); //add
      localLoan.LoanMaster.Delta_Total_Return_Fee = precise_round((workingLoan.Total_ARM_Fees_And_Interest) || 0, 0); //add
      localLoan.LoanMaster.Delta_Interest_Est_Amount = precise_round((workingLoan.Interest_Est_Amount) || 0, 0); //add

      localLoan.LoanMaster.Delta_Cash_Flow_Amount = precise_round( workingLoan.Cash_Flow_Amount || 0, 1);
      localLoan.LoanMaster.Delta_Cash_Flow_Percent = precise_round(workingLoan.Cash_Flow_Percent || 0, 1);
      localLoan.LoanMaster.Delta_Risk_Cushion_Percent = precise_round( workingLoan.Risk_Cushion_Percent || 0, 1);
      localLoan.LoanMaster.Delta_Risk_Cushion_Amount = precise_round(workingLoan.Risk_Cushion_Amount || 0, 1);

      localLoan.LoanMaster.Delta_Return_Percent = precise_round(workingLoan.Return_Percent || 0, 1);
      localLoan.LoanMaster.Delta_Current_Addendum_Percent = precise_round(workingLoan.Current_Addendum_Percent || 0, 1);

    } else if (!workingLoan && !prevLoan) {
      localLoan.LoanMaster.Delta_Crop_Acres = 0;
      localLoan.LoanMaster.Delta_ARM_Commitment = 0;
      localLoan.LoanMaster.Delta_Dist_Commitment = 0;
      localLoan.LoanMaster.Delta_Total_Commitment = 0;
      localLoan.LoanMaster.Delta_Third_Party_Credit = 0

      localLoan.LoanMaster.Delta_Origination_Fee_Amount = 0;
      localLoan.LoanMaster.Delta_Service_Fee_Amount = 0;
      localLoan.LoanMaster.Delta_Extension_Fee_Amount = 0;
      localLoan.LoanMaster.Delta_Total_Return_Fee = 0;
      localLoan.LoanMaster.Delta_Interest_Est_Amount = 0;

      localLoan.LoanMaster.Delta_Cash_Flow_Amount = 0;
      localLoan.LoanMaster.Delta_Cash_Flow_Percent = 0;
      localLoan.LoanMaster.Delta_Risk_Cushion_Percent = 0;
      localLoan.LoanMaster.Delta_Risk_Cushion_Amount = 0;

      localLoan.LoanMaster.Delta_Return_Percent = 0;
      localLoan.LoanMaster.Delta_Current_Addendum_Percent = 0;
    }
  }

  crossCollAffLoans() {

    let loans = this.localst.retrieve(environment.loanGroup);
    let affiliatedLoans = loans ? loans.filter(loan => {
      return loan.IsAffiliated;
    }) : [];

    //Include Affiliated Loans to Cross Collateral
    if (affiliatedLoans && affiliatedLoans.length > 0) {

      let loanObject: loan_model = this.localst.retrieve(environment.loankey);

      affiliatedLoans.forEach(af => af.Loan_ID = Number(af.Loan_Full_ID.split('-')[0]));
      let loanIDs = affiliatedLoans.filter(af => af.LoanJSON.LoanMaster.Crop_Year == loanObject.LoanMaster.Crop_Year).map(af => af.Loan_ID);
      loanIDs = loanIDs.filter(onlyUnique);

      let loanList = this.localst.retrieve(environment.loanCrossCollateral);

      let collateralizeItems = loanObject.Cross_Collateralized_Loans;
      let existingIDs = collateralizeItems.filter(loan => loan.ActionStatus != 3).map(loan => loan.Loan_ID);
      loanIDs.forEach(Loan_ID => {

        //Check if ID is already in the Cross Collateral
        if (existingIDs.includes(Loan_ID)) { return; }

        //Get collateral ID of Loan ID
        let newCollateral = loanList.filter(loan => loan.Loan_ID == Loan_ID);
        let Cross_Collateral_Group_ID = newCollateral && newCollateral.Cross_Collateral_Group_ID ? newCollateral.Cross_Collateral_Group_ID : 0;

        //Add Item to Cross Collateral List
        let newItem: Cross_Collateralized_Loans = new Cross_Collateralized_Loans();
        newItem.Loan_ID = Loan_ID;
        // newItem.Loan_Master_Cross_Ind = 0;
        newItem.ActionStatus = 1;
        newItem.Cross_Collateral_Group_ID = Cross_Collateral_Group_ID;
        newItem.Status = 1;
        newItem.Borrower = newCollateral[0].Borrower_First_Name + ' ' + newCollateral[0].Borrower_MI + ' ' + newCollateral[0].Borrower_Last_Name;
        newItem.Farmer = newCollateral[0].Farmer_First_Name + ' ' + newCollateral[0].Farmer_MI + ' ' + newCollateral[0].Farmer_Last_Name;
        collateralizeItems.push(newItem);

        //Get Related Loans Based on Cross_Collateral_Group_ID
        if (Cross_Collateral_Group_ID) {
          let relatedLoans = loanList.filter(loan =>
            loan.Cross_Collateral_Group_ID == Cross_Collateral_Group_ID &&
            loan.Loan_ID != Loan_ID && !existingIDs.includes(loan.Loan_ID));

          collateralizeItems = collateralizeItems.concat(relatedLoans);
        }

        //Save to local Storage
        loanObject.Cross_Collateralized_Loans = collateralizeItems;
        this.performcalculationonloanobject(loanObject, false);
      });
    }
  }

  setValidationErrorsBeforeSave(localloanobject: loan_model) {
    try {
      let loan_settings: Loansettings;
      try {
        loan_settings = JSON.parse(localloanobject.LoanMaster.Loan_Settings) as Loansettings;
      } catch {
        loan_settings = <Loansettings>{};
      }

      if (!loan_settings) {
        loan_settings = <Loansettings>{};
      }
      loan_settings.Validations = this.localst.retrieve(environment.errorbase);
      localloanobject.LoanMaster.Loan_Settings = JSON.stringify(loan_settings);
    } catch {
    }

    return localloanobject;
  }

  saveValidationsToLocalStorage(localloanobject: loan_model) {
    this.localst.store(environment.modifiedoptimizerdata, null);

    try {
      let loan_settings: Loansettings;
      try {
        loan_settings = JSON.parse(localloanobject.LoanMaster.Loan_Settings) as Loansettings;
      } catch {
        loan_settings = <Loansettings>{};
      }

      if (!loan_settings) {
        loan_settings = <Loansettings>{};
      }

      let data: Array<errormodel> = loan_settings.Validations || [];
      this.localst.store(environment.errorbase, data);
    } catch {
    }
  }

  WaiveofftempdisburseErrors(localloanobject: loan_model) {

    if (localloanobject.Disbursements.find(p => p.Loan_Disburse_ID == 'e1') != null) {
      localloanobject.Loan_Exceptions = localloanobject.Loan_Exceptions.filter(p => p.Chevron_ID != 69);
    }
    return localloanobject;
  }

  compareWithUploadedVersion(localloanobj: loan_model) {
    try {
      let loanGroups: Array<LoanGroup> = this.localst.retrieve(environment.loanGroup);

      if (loanGroups) {
        loanGroups.forEach(a => {
          let LoanFullID = a.Loan_Full_ID;
          a.Loan_ID = +LoanFullID.split('-')[0];
          a.Loan_Seq_Number = +LoanFullID.split('-')[1];
        });

        let uploadedLoanGroup = loanGroups.find(a => a.Loan_ID == localloanobj.LoanMaster.Loan_ID && a.LoanJSON.LoanMaster.Loan_Status == LoanStatus.Uploaded);
        if (uploadedLoanGroup) {
          let uploadeLoan = uploadedLoanGroup.LoanJSON;

          if (localloanobj.LoanMaster.Origination_Fee_Amount < uploadeLoan.LoanMaster.Origination_Fee_Amount) {
            localloanobj.LoanMaster.FC_Invalid_Origination = true;
          } else {
            localloanobj.LoanMaster.FC_Invalid_Origination = false;
          }

          if (localloanobj.LoanMaster.Service_Fee_Amount < uploadeLoan.LoanMaster.Service_Fee_Amount) {
            localloanobj.LoanMaster.FC_Invalid_Service = true;
          } else {
            localloanobj.LoanMaster.FC_Invalid_Service = false;
          }

          // if(localloanobj.LoanMaster.Extension_Fee_Amount < uploadeLoan.LoanMaster.Extension_Fee_Amount) {
          //   localloanobj.LoanMaster.FC_Invalid_Extension = true;
          // } else {
          //   localloanobj.LoanMaster.FC_Invalid_Extension = false;
          // }

          if (localloanobj.LoanMaster.ARM_Rate_Fee < uploadeLoan.LoanMaster.ARM_Rate_Fee) {
            localloanobj.LoanMaster.FC_Invalid_ARM_Fee = true;
          } else {
            localloanobj.LoanMaster.FC_Invalid_ARM_Fee = false;
          }

          if (localloanobj.LoanMaster.Dist_Rate_Fee < uploadeLoan.LoanMaster.Dist_Rate_Fee) {
            localloanobj.LoanMaster.FC_Invalid_Dist_Fee = true;
          } else {
            localloanobj.LoanMaster.FC_Invalid_Dist_Fee = false;
          }

          localloanobj.LoanMaster.Origination_Fee_Amount = Math.max(localloanobj.LoanMaster.Origination_Fee_Amount, uploadeLoan.LoanMaster.Origination_Fee_Amount);
          localloanobj.LoanMaster.Service_Fee_Amount = Math.max(localloanobj.LoanMaster.Service_Fee_Amount, uploadeLoan.LoanMaster.Service_Fee_Amount);
          // localloanobj.LoanMaster.Extension_Fee_Amount = Math.max(localloanobj.LoanMaster.Extension_Fee_Amount, uploadeLoan.LoanMaster.Origination_Fee_Amount);
          localloanobj.LoanMaster.ARM_Rate_Fee = Math.max(localloanobj.LoanMaster.ARM_Rate_Fee, uploadeLoan.LoanMaster.ARM_Rate_Fee);
          localloanobj.LoanMaster.Dist_Rate_Fee = Math.max(localloanobj.LoanMaster.Dist_Rate_Fee, uploadeLoan.LoanMaster.Dist_Rate_Fee);
        }
      }
    } catch {

    }
  }
}

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}
function settimeStart(text: string) {
  let d = new Date();
  console.log(text + " " + "Start:-" + d.getMinutes() + ":" + d.getSeconds() + ":" + d.getMilliseconds());
}
function settimeEnd(text: string) {
  let d = new Date();
  console.log(text + " " + "End:-" + d.getMinutes() + ":" + d.getSeconds() + ":" + d.getMilliseconds());
}
