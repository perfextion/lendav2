import { Injectable } from '@angular/core';
import { loan_model, Loan_Crop } from '../../models/loanmodel';
import * as _ from 'lodash';
import { LoggingService } from '../../services/Logs/logging.service';
import { LoanMaster, RefDataModel } from '@lenda/models/ref-data-model';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';
@Injectable()
export class OptimizercalculationService {
  refData: RefDataModel;

  constructor(public logging: LoggingService, private localstorage: LocalStorageService) { }

  performcalculations(input: loan_model) {
    let starttime = new Date().getTime();
    try {
      let distinctcrops = _.uniqBy(input.LoanCropUnits.filter(a => a.ActionStatus != 3), 'Crop_Practice_ID');

      distinctcrops.forEach(element => {
        let lcp = input.LoanCropPractices.filter(a => a.ActionStatus != 3).find(
          p => p.Crop_Practice_ID == element.Crop_Practice_ID
        );

        if(lcp) {
          lcp.LCP_Acres = _.sumBy(
            input.LoanCropUnits.filter(
              p => p.Crop_Practice_ID == element.Crop_Practice_ID
            ),
            o => o.CU_Acres || 0
          );

          lcp.ActionStatus = 2;
        }
      });
    } catch (ex) {
      console.log('Error in Optimizer Calculations');
    }

    let endtime = new Date().getTime();
    //level 2 log
    this.logging.checkandcreatelog(
      2,
      'Calc_Optimizer',
      'LoanCalculation timetaken :' + (endtime - starttime).toString() + ' ms'
    );
    return input;
  }

  public performLoanUnitCalculations(localloanobj: loan_model): loan_model {
    let loanMaster: LoanMaster = localloanobj.LoanMaster;

    for (let lcu of localloanobj.LoanCropUnits.filter(a => a.ActionStatus != 3)) {
      let cropYield = localloanobj.CropYield.find(
        f =>
          f.Loan_Full_ID == lcu.Loan_Full_ID &&
          f.Crop_Code == lcu.Crop_Code
      );

      let farm = localloanobj.Farms.filter(a => a.ActionStatus != 3).find(
        f => f.Farm_ID == lcu.Farm_ID
      );

      let loancroppractice = localloanobj.LoanCropPractices.find(
        x => x.Crop_Practice_ID == lcu.Crop_Practice_ID
      );

      if (loancroppractice && cropYield && farm) {
        let rentPerAcre = farm.Cash_Rent_Per_Acre;
        let rentWaivedPerAcre = farm.Cash_Rent_Waived;
        //LETS GET FRM BUDGET

        let lstloanBudget = localloanobj.LoanBudget.filter(
          budget =>
            budget.Crop_Practice_ID == loancroppractice.Crop_Practice_ID &&
            budget.ActionStatus != 3
        );

        let sumArmBudgetPerAcre = _.sumBy(lstloanBudget, 'ARM_Budget_Acre');
        lcu.Arm_Budget_Sum = sumArmBudgetPerAcre * lcu.CU_Acres;

        //////////////
        let sumTPudgetPerAcre = _.sumBy(
          lstloanBudget,
          bgt => bgt.Third_Party_Budget_Acre || 0
        );
        lcu.Third_Budget_Sum = sumTPudgetPerAcre * lcu.CU_Acres;

        //////////////
        let sumDistBudgetPerAcre = _.sumBy(
          lstloanBudget,
          'Distributor_Budget_Acre'
        );
        lcu.Dist_Budget_Sum = sumDistBudgetPerAcre * lcu.CU_Acres;
        ///////////////

        //BUDGET ENDS


        let estimatedDays = localloanobj.LoanMaster.Estimated_Days;
        if (estimatedDays === 0) {
          estimatedDays = 255;
        }

        lcu.Arm_finance_Cost = (farm.FC_Net_Rent_Acre + sumArmBudgetPerAcre + sumDistBudgetPerAcre)
          * ((loanMaster.Origination_Fee_Percent + loanMaster.Service_Fee_Percent + (loanMaster.Extension_Fee_Percent || 0)) / 100)
          + (farm.FC_Net_Rent_Acre + sumArmBudgetPerAcre) * loanMaster.Interest_Percent * loanMaster.Estimated_Days / 365 / 100 * this.getUtilizationPercent() / 100;

        lcu.Dist_Finance_cost = sumDistBudgetPerAcre * loanMaster.Interest_Percent * loanMaster.Estimated_Days / 365 / 100 * this.getUtilizationPercent() / 100;


        //Variables
        let Prod_Shr_Acres_Sum = lcu.CU_Acres * farm.Percent_Prod;
        let Crop_Yield_Sum = Prod_Shr_Acres_Sum * lcu.FC_CropYield;
        let Crop_Exp_Percent = 100; //Static
        let Crop_Exp_Qty = Crop_Exp_Percent * lcu.FC_CropYield;
        let Crop_Prod_Shr_Exp_Qty_Sum = Crop_Exp_Percent / 100 * Crop_Yield_Sum;
        let Mkt_Value = Crop_Exp_Qty * lcu.Z_Crop_Adj_Price;
        let Mkt_Shr_Value = Mkt_Value * farm.Percent_Prod;
        let Mkt_Shr_Value_Sum = Mkt_Shr_Value * lcu.CU_Acres;
        let Col_Map_Mkt_Adj = -2.5; // key mpt available so using static value

        let Mkt_Disc_Total = Mkt_Shr_Value_Sum * this.getDiscountbyKey('CRP') + Col_Map_Mkt_Adj;
        let Mkt_Disc_Shr_Value = Mkt_Shr_Value * (1 - Mkt_Disc_Total);
        let Mkt_Disc_Shr_Sum = Mkt_Disc_Shr_Value * lcu.CU_Acres;
        // let Ins_Shr=farm.Permission_To_Insure == 1?100:farm.Percent_Prod;
        // let Ins_Shr_Acres_Sum=
        let CEI = Math.max(Mkt_Disc_Shr_Value - lcu.Disc_Ins_value_Acre, 0);
        let TotalBudget_Acre = sumArmBudgetPerAcre + sumDistBudgetPerAcre + sumTPudgetPerAcre;


        //----------------



        let Cashflow_Acre = parseFloat(((lcu.Mkt_Value_Acre - rentPerAcre - TotalBudget_Acre - lcu.Arm_finance_Cost - lcu.Dist_Finance_cost)).toFixed(2));
        let CF_Sum = Cashflow_Acre * lcu.CU_Acres;

        let RiskCushion_Acre = parseFloat(((lcu.Disc_CEI_Value_Acre + lcu.Disc_Ins_value_Acre - rentPerAcre + rentWaivedPerAcre - sumArmBudgetPerAcre - lcu.Arm_finance_Cost)).toFixed(2));
        let RC_Sum = RiskCushion_Acre * lcu.CU_Acres;

        let CEI_Sum = CEI * lcu.CU_Acres;
        let ARM_Finance_Cost_Sum=lcu.Arm_finance_Cost * lcu.CU_Acres;
        let Dist_Finance_Cost_Sum=lcu.Dist_Finance_cost * lcu.CU_Acres;

        let ARM_Margin_Acre=lcu.Ins_Value_Acre - rentPerAcre + rentWaivedPerAcre - sumArmBudgetPerAcre - lcu.Arm_finance_Cost;
        let ARM_Margin_Sum=ARM_Margin_Acre * lcu.CU_Acres;

        let Total_Margin_Acre=ARM_Margin_Acre - sumDistBudgetPerAcre - lcu.Dist_Finance_cost;
        let Total_Margin_Sum=Total_Margin_Acre * lcu.CU_Acres;


        //value Setting to Loan Crop unit
        lcu.FC_Cashflow_Acre=Cashflow_Acre ? parseFloat(Number(Cashflow_Acre).toFixed(2)) : 0;
        lcu.FC_RiskCushion_Acre=RiskCushion_Acre ? parseFloat(Number(RiskCushion_Acre).toFixed(2)) : 0;
        lcu.FC_Margin_Acre=Total_Margin_Acre ? parseFloat(Number(Total_Margin_Acre).toFixed(2)) : 0;

        lcu.FC_Cashflow = CF_Sum ? parseFloat(Number(CF_Sum).toFixed(2)) : 0;
        lcu.FC_RiskCushion = RC_Sum ? parseFloat(Number(RC_Sum).toFixed(2)) : 0;
        lcu.Arm_Margin =  ARM_Margin_Sum ? parseFloat(Number(ARM_Margin_Sum).toFixed(2)) : 0;
        lcu.Total_Margin = Total_Margin_Sum ? parseFloat(Number(Total_Margin_Sum).toFixed(2)) : 0;
        lcu.FC_Margin =  lcu.Total_Margin;

      }
    }

    return localloanobj;
  }

  getUtilizationPercent() {
    try {
      if (!this.refData) {
        this.refData = this.localstorage.retrieve(environment.referencedatakey);
      }
      let util = this.refData.Discounts.find(d => d.Discount_Key == 'UTIL_RATE');
      return util.Discount_Value;
    } catch {
      return 62;
    }
  }

  getDiscountbyKey(key: string) {

    try {
      let value = (this.refData.Discounts as [any]).find(p => p.Discount_Key == key).Discount_Value
      if (value == undefined || value == null) {
        value = 0
      }
      return value;
    }
    catch{
      return 0;
    }

  }
}
