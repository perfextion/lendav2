import { Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { Loan_Policy, InsuranceValidationData } from '@lenda/models/insurancemodel';
import { environment } from '@env/environment.prod';
import { lookupStateValueFromPassedRefObject } from '@lenda/Workers/utility/aggrid/stateandcountyboxes';
import {
  errormodel,
  Chevronkeys,
  validationlevel
} from '@lenda/models/commonmodels';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { AgGridValidation, AgGridValidationResult, RequiredValidation } from '../utility/validation-functions';
import { Page } from '@lenda/models/page.enum';
import { FormGroup, FormControl } from '@angular/forms';
import { getcolumnkey } from '@lenda/components/insurance/policies/policiescolumnalias';
import { RefQuestions } from '@lenda/models/loan-response.model';
import { To_Friendly_Name, To_Friendly_Policy_Name } from '@lenda/services/common-utils';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { loan_model, LoanStatus } from '@lenda/models/loanmodel';

interface IValidationService {
  /**
   * Validated Insurance Policies
   */
  Validate_Insurance(params: any, insurancepolicies: Loan_Policy[], insuranceValidationData: InsuranceValidationData): void;

  /**
   * Validates AG Grid Table
   */
  validateTableFields<T>(rowData: Array<T>, coldefs: any[], page: Page, tableId: string, chevronKey: string, rowKey: string): void;

  /**
   * Validates Forms
   */
  validateFormField(formGroup: FormGroup, page: Page, component: string, formGroupName: string, chevronKey: string): void;

  /**
   * Validates Questions
   */
  validate_Questions(questions: RefQuestions[], page: string, policy: string): void;

  /**
   * Validates Loan Comment
   */
  ValidateLoanComment(comment: string): void;

  /**
   * Validates Loan Documents
   * @param localLoanObj Loan Object
   */
  Validate_Loan_Documents(localLoanObj: loan_model): void;

  /**
   * Highlight cells in AG Grid based on Validations
   */
  highlighErrorCells(errors: errormodel[], tableId?: string): void;

}

@Injectable({
  providedIn: 'root'
})
export class ValidationService implements IValidationService {
  private refdata: RefDataModel;

  constructor(
    private localstorage: LocalStorageService,
    public loggingservice: LoggingService
  ) {
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);
  }

  //#region [Insurance Ploicies table data]

   /**
   * Validated Insurance Policies
   * @param params Ag Grid Params
   * @param insurancepolicies Array Of Insurance Policy
   * @param insuranceValidationData Insurance Policy Validation Data - Reference Data ( Both Insurance Eligibilty and Validation Rules)
   */
  public Validate_Insurance(params: any, insurancepolicies: Loan_Policy[], insuranceValidationData: InsuranceValidationData) {
    if (environment.isDebugModeActive) console.time('Insurance Validation start Time');

    let currenterrors = this.localstorage.retrieve(
      environment.errorbase
    ) as Array<errormodel>;
    _.remove(currenterrors, e => e.chevron == Chevronkeys.InsurancePolices);

    const selectedPolicies = insurancepolicies.filter(p => p.Select_Ind);

    selectedPolicies.forEach(plan => {
      let mpciPlan = insurancepolicies.find(p => p.Policy_Key == plan.Policy_Key && p.Ins_Plan == 'MPCI');
      this.Validate_InsurancePolicyData(plan, currenterrors, mpciPlan, insuranceValidationData);
    });

    this.localstorage.store(environment.errorbase, currenterrors);
    if (environment.isDebugModeActive) console.timeEnd('Insurance Validation start Time');
  }

  private Validate_InsurancePolicyData(plan: Loan_Policy, errors: errormodel[], mpciPlan: Loan_Policy, insuranceValidationData: InsuranceValidationData) {
    if (environment.isDebugModeActive) console.time('Validate_InsurancePolicyData start Time');
    //Make sure insplantype dont produce errror by tarjeet
    plan.Ins_Plan_Type = plan.Ins_Plan_Type == undefined ? '' : plan.Ins_Plan_Type;
    //MPCI Plan
    if (plan.Ins_Plan.toUpperCase() == 'MPCI') {


      //CAT
      if (plan.Ins_Plan_Type.toUpperCase() == 'CAT') {

        //MPCI_CAT_UNIT
        if (plan.Ins_Unit_Type_Code && !insuranceValidationData.MPCI_CAT_UNIT.some(x => x == plan.Ins_Unit_Type_Code)) {
          let columnKey = 'Unit_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        //ToDo: MPCI_CAT_AIP

      }

      //YP
      if (plan.Ins_Plan_Type.toUpperCase() == 'YP') {

        //MPCI_YP_UNIT
        if (plan.Ins_Unit_Type_Code && !insuranceValidationData.MPCI_YP_UNIT.some(x => x == plan.Ins_Unit_Type_Code)) {
          let columnKey = 'Unit_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        //MPCI_YP_OPTION
        if (plan.Option && !insuranceValidationData.MPCI_YP_OPTION.some(x => x == plan.Option)) {
          let columnKey = 'Option_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        //MPCI_YP_UPPERPER
        if (plan.Upper_Level && !insuranceValidationData.MPCI_YP_UPPERPER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
          let columnKey = getcolumnkey('Upper_Level') + '_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        //MPC_YP_AIP
      }

      // RP-HPE
      if (plan.Ins_Plan_Type.toUpperCase() == 'RP-HPE') {

        //MPCI_RP_HPE_UNIT
        if (plan.Ins_Unit_Type_Code && !insuranceValidationData.MPCI_RP_HPE_UNIT.some(x => x == plan.Ins_Unit_Type_Code)) {
          let columnKey = 'Unit_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        //MPCI_RP_HPE_OPTION
        if (plan.Option && !insuranceValidationData.MPCI_RP_HPE_OPTION.some(x => x == plan.Option)) {
          let columnKey = 'Option_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        //MPCI_RP_HPE_UPPERPER
        if (plan.Upper_Level && !insuranceValidationData.MPCI_RP_HPE_UPPERPER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
          let columnKey = getcolumnkey('Upper_Level') + '_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        //MPC_RP_HPE_AIP
      }

      //RP
      if (plan.Ins_Plan_Type.toUpperCase() == 'RP') {
        //MPCI_RP_UNIT

        if (plan.Ins_Unit_Type_Code && !insuranceValidationData.MPCI_RP_UNIT.some(x => x == plan.Ins_Unit_Type_Code)) {
          let columnKey = 'Unit_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        //MPCI_RP_OPTION
        if (plan.Option && !insuranceValidationData.MPCI_RP_OPTION.some(x => x == plan.Option)) {
          let columnKey = 'Option_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        //MPCI_RP_UPPERPER
        if (plan.Upper_Level && !insuranceValidationData.MPCI_RP_UPPERPER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
          let columnKey = getcolumnkey('Upper_Level') + '_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        //MPC_RP_AIP

      }

      //ARH
      if (plan.Ins_Plan_Type.toUpperCase() == 'ARH') {

        //MPCI_ARH_UNIT
        if (plan.Ins_Unit_Type_Code && !insuranceValidationData.MPCI_ARH_UNIT.some(x => x == plan.Ins_Unit_Type_Code)) {
          let columnKey = 'Unit_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        // //MPCI_ARH_OPTION
        if (plan.Option && !insuranceValidationData.MPCI_ARH_OPTION.some(x => x == plan.Option)) {
          let columnKey = 'Option_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        //MPCI_ARH_UPPERPER
        if (plan.Upper_Level && !insuranceValidationData.MPCI_ARH_UPPERPER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
          let columnKey = getcolumnkey('Upper_Level') + '_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }

        //MPCI_ARH_YIELDPER
        if (plan.Yield_Percent && !insuranceValidationData.MPCI_ARH_YIELDPER.some(x => x == parseInt(plan.Yield_Percent.toString()))) {
          let columnKey = 'YieldProt_Pct_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }
        //MPCI_ARH_PRICEPER
        if (plan.Price_Percent && !insuranceValidationData.MPCI_ARH_PRICEPER.some(x => x == parseInt(plan.Price_Percent.toString()))) {
          let columnKey = 'PriceProt_Pct_MPCI';
          this.pushErrorsInsurancePolicies(plan, columnKey, errors);
        }
        //MPC_ARH_AIP

      }

      //Premium
      if (!plan.Premium || !parseInt(plan.Premium.toString())) {
        let columnKey = getcolumnkey('Premium') + '_MPCI';
        this.pushErrorsInsurancePolicies(plan, columnKey, errors);
      }

    }

    //WFRP Plan
    if (plan.Ins_Plan.toUpperCase() == 'WFRP') {

      //WFRP_AIP


      //WFRP_UPPERPER
      if (plan.Upper_Level && !insuranceValidationData.WFRP_UPPERPER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
        let columnKey = 'wfrp_level';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

      //Premium
      if (!plan.Premium || !parseInt(plan.Premium.toString())) {
        let columnKey = getcolumnkey('Premium') + '_' + plan.Ins_Plan;
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }
    }

    //STAX Plan
    if (plan.Ins_Plan.toUpperCase() == 'STAX') {

      //STAX_UPPERPER
      if (plan.Upper_Level && !insuranceValidationData.STAX_UPPERPER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
        let columnKey = 'Upper_Limit_STAX';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

      //STAX_LOWERPER
      if (plan.Lower_Level && !insuranceValidationData.STAX_LOWERPER.some(x => x == parseInt(plan.Lower_Level.toString()))) {
        let columnKey = 'Lower_Limit_STAX';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

      //lowerlevel should not be greater than upper level &&  STAX_RANGEMAX
      if (plan.Upper_Level && plan.Lower_Level && (parseInt(plan.Upper_Level.toString()) < parseInt(plan.Lower_Level.toString())
        || parseInt(plan.Upper_Level.toString()) - parseInt(plan.Lower_Level.toString()) > insuranceValidationData.STAX_RANGEMAX)) {
        let columnKey1 = 'Upper_Limit_STAX';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey1, errors);
        let columnKey2 = 'Lower_Limit_STAX';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey2, errors);
      }


      //STAX_YIELD
      if (plan.Yield_Percent && !insuranceValidationData.STAX_YIELD.some(x => x == parseInt(plan.Yield_Percent.toString()))) {
        let columnKey = 'Yield_Pct_STAX';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

      //Premium
      if (!plan.Premium || !parseInt(plan.Premium.toString())) {
        let columnKey = getcolumnkey('Premium') + '_' + plan.Ins_Plan;
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

      //STAX_UNIT
      if (plan.Ins_Unit_Type_Code && !insuranceValidationData.STAX_UNIT.some(x => x == plan.Ins_Unit_Type_Code.toString())) {
        let columnKey = 'Unit_STAX';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

      //STAX_AIP

    }

    //HMAX Plan
    if (plan.Ins_Plan.toUpperCase() == 'HMAX') {
      if (plan.Ins_Plan_Type.toUpperCase() == 'STANDARD') {
        // check validation for state and crop code
        if (!(insuranceValidationData.HMAX_STANDARD_ELLIGIBLESTATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.HMAX_STANDARD_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //HMAX_STANDARD_UPPERPER
        if (plan.Upper_Level && !insuranceValidationData.HMAX_STANDARD_UPPERPER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
          let columnKey = 'Upper_Limit_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //HMAX_STANDARD_LOWERPER
        if (plan.Lower_Level && !insuranceValidationData.HMAX_STANDARD_LOWERPER.some(x => x == parseInt(plan.Lower_Level.toString()))) {
          let columnKey = 'Lower_Limit_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //lowerlevel should not be greater than upper level
        if (plan.Upper_Level && plan.Lower_Level && parseInt(plan.Upper_Level.toString()) < parseInt(plan.Lower_Level.toString())) {
          let columnKey1 = 'Upper_Limit_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey1, errors);
          let columnKey2 = 'Lower_Limit_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey2, errors);
        }

        //HMAX_STANDARD_PRICE
        if (plan.Price_Percent && !insuranceValidationData.HMAX_STANDARD_PRICE.some(x => x == parseInt(plan.Price_Percent.toString()))) {
          let columnKey = 'Price_Pct_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

      }

      if (plan.Ins_Plan_Type.toUpperCase() == 'X1') {

        // check validation for state and crop code
        if (!(insuranceValidationData.HMAX_X1_ELLIGIBLESTATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.HMAX_X1_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //HMAX_X1_UPPERPER
        if (plan.Upper_Level && !insuranceValidationData.HMAX_X1_UPPERPER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
          let columnKey = 'Upper_Limit_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //HMAX_X1_LOWERPER
        if (plan.Lower_Level && !insuranceValidationData.HMAX_X1_LOWERPER.some(x => x == parseInt(plan.Lower_Level.toString()))) {
          let columnKey = 'Lower_Limit_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //lowerlevel should not be greater than upper level
        if (plan.Upper_Level && plan.Lower_Level && parseInt(plan.Upper_Level.toString()) < parseInt(plan.Lower_Level.toString())) {
          let columnKey1 = 'Upper_Limit_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey1, errors);
          let columnKey2 = 'Lower_Limit_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey2, errors);
        }

        //HMAX_X1_PRICE
        if (plan.Price_Percent && !insuranceValidationData.HMAX_X1_PRICE.some(x => x == parseInt(plan.Price_Percent.toString()))) {
          let columnKey = 'Price_Pct_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

      }

      if (plan.Ins_Plan_Type.toUpperCase() == 'MAXRP') {
        // check validation for state and crop code
        if (!(insuranceValidationData.HMAX_MAXRP_ELLIGIBLESTATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.HMAX_MAXRP_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
        //HMAX_MAXRP_UPPERPER
        if (plan.Upper_Level && !insuranceValidationData.HMAX_MAXRP_UPPERPER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
          let columnKey = 'Upper_Limit_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //HMAX_MAXRP_LOWERPER
        if (plan.Lower_Level && !insuranceValidationData.HMAX_MAXRP_LOWERPER.some(x => x == parseInt(plan.Lower_Level.toString()))) {
          let columnKey = 'Lower_Limit_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //lowerlevel should not be greater than upper level
        if (plan.Upper_Level && plan.Lower_Level && parseInt(plan.Upper_Level.toString()) < parseInt(plan.Lower_Level.toString())) {
          let columnKey1 = 'Upper_Limit_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey1, errors);
          let columnKey2 = 'Lower_Limit_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey2, errors);
        }

        //HMAX_MAXRP_PRICE
        if (plan.Price_Percent && !insuranceValidationData.HMAX_MAXRP_PRICE.some(x => x == parseInt(plan.Price_Percent.toString()))) {
          let columnKey = 'Price_Pct_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
      }

      //Plan Type
      if (!plan.Ins_Plan_Type && !(plan.Ins_Plan_Type == 'Standard' || plan.Ins_Plan_Type == 'X1' || plan.Ins_Plan_Type == 'MaxRP')) {
        let columnKey = 'Subtype_HMAX';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

      //Premium
      if (!plan.Premium || !parseInt(plan.Premium.toString())) {
        let columnKey = getcolumnkey('Premium') + '_' + plan.Ins_Plan;
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

      // //Late Deduct
      // if (plan.Late_Deductible && ((parseInt(plan.Late_Deductible.toString()) < insuranceValidationData.Late_Deduct_HMAX_Values) || plan.Late_Deductible.toString() == 'NaN')) {
      //   let columnKey = 'Late_Deduct_HMAX';
      //   this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      // }

    }

    //RAMP Plan
    if (plan.Ins_Plan.toUpperCase() == 'RAMP') {

      //Plan Type
      if (!plan.Ins_Plan_Type && !(plan.Ins_Plan_Type == 'RY' || plan.Ins_Plan_Type == 'RR')) {
        let columnKey = 'Subtype_RAMP';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

      if (plan.Ins_Plan_Type.toUpperCase() == 'RY') {
        // check validation for state and crop code
        if (!(insuranceValidationData.RAMP_RY_ELLIGIBLESTATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.RAMP_RY_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //RAMP_RY_UPPERPER
        if (plan.Upper_Level && !insuranceValidationData.RAMP_RY_UPPERPER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
          let columnKey = 'Upper_Limit_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //RAMP_RY_LOWERPER
        if (plan.Lower_Level && !insuranceValidationData.RAMP_RY_LOWERPER.some(x => x == parseInt(plan.Lower_Level.toString()))) {
          let columnKey = 'Lower_Limit_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //lowerlevel should not be greater than upper level
        if (plan.Upper_Level && plan.Lower_Level && parseInt(plan.Upper_Level.toString()) < parseInt(plan.Lower_Level.toString())) {
          let columnKey1 = 'Upper_Limit_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey1, errors);
          let columnKey2 = 'Lower_Limit_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey2, errors);
        }

        //RAMP_RY_PRICE
        if (plan.Price_Percent && !insuranceValidationData.RAMP_RY_PRICE.some(x => x == parseInt(plan.Price_Percent.toString()))) {
          let columnKey = 'Price_Pct_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //RAMP_RY_UNIT
        if (plan.Ins_Unit_Type_Code && !insuranceValidationData.RAMP_RY_UNIT.some(x => x == plan.Ins_Unit_Type_Code.toString())) {
          let columnKey = 'Unit_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }


        //Liability
        let Liability_RAMP_Values: number[];
        if (plan.Upper_Level && (parseInt(plan.Upper_Level.toString()) >= insuranceValidationData.RAMP_Liability_Max_Range)) {
          Liability_RAMP_Values = insuranceValidationData.RAMP_RY_LIABILITYMAX85;
        } else {
          Liability_RAMP_Values = insuranceValidationData.RAMP_RY_LIABILITYMAX;
        }
        //RAMP_RY_LIABILITYMAX  && //RAMP_RY_LIABILITYMAX85
        if (plan.Liability_Percent && !Liability_RAMP_Values.some(x => x == parseInt(plan.Liability_Percent.toString()))) {
          let columnKey = 'Liability_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

      }

      if (plan.Ins_Plan_Type.toUpperCase() == 'RR') {

        // check validation for state and crop code
        if (!(insuranceValidationData.RAMP_RR_ELLIGIBLESTATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.RAMP_RR_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //RAMP_RR_UPPERPER
        if (plan.Upper_Level && !insuranceValidationData.RAMP_RR_UPPERPER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
          let columnKey = 'Upper_Limit_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //RAMP_RR_LOWERPER
        if (plan.Lower_Level && !insuranceValidationData.RAMP_RR_LOWERPER.some(x => x == parseInt(plan.Lower_Level.toString()))) {
          let columnKey = 'Lower_Limit_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //lowerlevel should not be greater than upper level
        if (plan.Upper_Level && plan.Lower_Level && parseInt(plan.Upper_Level.toString()) < parseInt(plan.Lower_Level.toString())) {
          let columnKey1 = 'Upper_Limit_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey1, errors);
          let columnKey2 = 'Lower_Limit_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey2, errors);
        }

        //RAMP_RR_PRICE
        if (plan.Price_Percent && !insuranceValidationData.RAMP_RR_PRICE.some(x => x == parseInt(plan.Price_Percent.toString()))) {
          let columnKey = 'Price_Pct_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //RAMP_RR_UNIT
        if (plan.Ins_Unit_Type_Code && !insuranceValidationData.RAMP_RR_UNIT.some(x => x == plan.Ins_Unit_Type_Code.toString())) {
          let columnKey = 'Unit_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //Liability
        let Liability_RAMP_Values: number[];
        if (plan.Upper_Level && (parseInt(plan.Upper_Level.toString()) >= insuranceValidationData.RAMP_Liability_Max_Range)) {
          Liability_RAMP_Values = insuranceValidationData.RAMP_RR_LIABILITYMAX85;
        } else {
          Liability_RAMP_Values = insuranceValidationData.RAMP_RR_LIABILITYMAX;
        }
        //RAMP_RR_LIABILITYMAX  && //RAMP_RR_LIABILITYMAX85
        if (plan.Liability_Percent && !Liability_RAMP_Values.some(x => x == parseInt(plan.Liability_Percent.toString()))) {
          let columnKey = 'Liability_RAMP';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

      }

      //Premium
      if (!plan.Premium || !parseInt(plan.Premium.toString())) {
        let columnKey = getcolumnkey('Premium') + '_' + plan.Ins_Plan;
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

    }

    //SELECT Plan
    if (plan.Ins_Plan.toUpperCase() == 'SELECT') {

      //Plan Type
      if (!plan.Ins_Plan_Type && !(plan.Ins_Plan_Type == 'Yield' || plan.Ins_Plan_Type == 'Revenue')) {
        let columnKey = 'Subtype_SELECT';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

      if (plan.Ins_Plan_Type == 'Yield') {
        // check validation for state and crop code
        if (!(insuranceValidationData.SELECT_Yield_ELIGIBLE_STATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.SELECT_Yield_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_SELECT';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //SELECT_YIELD_UPPER_PER
        if (plan.Ins_Plan_Type == 'Yield' && plan.Upper_Level && !insuranceValidationData.SELECT_YIELD_UPPER_PER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
          let columnKey = 'Upper_Limit_SELECT';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //SELECT_YIELD_LOWER_PER
        if (plan.Ins_Plan_Type == 'Yield' && plan.Lower_Level && !insuranceValidationData.SELECT_YIELD_LOWER_PER.some(x => x == parseInt(plan.Lower_Level.toString()))) {
          let columnKey = 'Lower_Limit_SELECT';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //lowerlevel should not be greater than upper level-AY
        if (plan.Upper_Level && plan.Lower_Level && parseInt(plan.Upper_Level.toString()) < parseInt(plan.Lower_Level.toString())) {
          let columnKey1 = 'Upper_Limit_SELECT';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey1, errors);
          let columnKey2 = 'Lower_Limit_SELECT';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey2, errors);
        }

        //SELECT_YIELD_UNIT
        if (!plan.Ins_Unit_Type_Code && !insuranceValidationData.SELECT_YIELD_UNIT.some(x => x == plan.Ins_Unit_Type_Code.toString())) {
          let columnKey = 'Unit_SELECT';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

      }

      if (plan.Ins_Plan_Type == 'Revenue') {

        // check validation for state and crop code
        if (!(insuranceValidationData.SELECT_REVENUE_ELIGIBLE_STATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.SELECT_REVENUE_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_SELECT';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }


        //SELECT_REVENUE_UPPER_PER
        if (plan.Upper_Level && !insuranceValidationData.SELECT_REVENUE_UPPER_PER.some(x => x == parseInt(plan.Upper_Level.toString()))) {
          let columnKey = 'Upper_Limit_SELECT';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //SELECT_REVENUE_LOWER_PER
        if (plan.Lower_Level && !insuranceValidationData.SELECT_REVENUE_LOWER_PER.some(x => x == parseInt(plan.Lower_Level.toString()))) {
          let columnKey = 'Lower_Limit_SELECT';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //lowerlevel should not be greater than upper level -AR
        if (plan.Upper_Level && plan.Lower_Level && parseInt(plan.Upper_Level.toString()) < parseInt(plan.Lower_Level.toString())) {
          let columnKey1 = 'Upper_Limit_SELECT';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey1, errors);
          let columnKey2 = 'Lower_Limit_SELECT';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey2, errors);
        }

        //SELECT_REVENUE_UNIT
        if (!plan.Ins_Unit_Type_Code && !insuranceValidationData.SELECT_REVENUE_UNIT.some(x => x == plan.Ins_Unit_Type_Code.toString())) {
          let columnKey = 'Unit_SELECT';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
      }

      //Premium
      if (!plan.Premium || !parseInt(plan.Premium.toString())) {
        let columnKey = 'Premium_SELECT';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }


    }

    //SCO Plan
    if (plan.Ins_Plan.toUpperCase() == 'SCO') {

      //SCO_UNIT
      if (!plan.Ins_Unit_Type_Code && !insuranceValidationData.SCO_UNIT.some(x => x == (plan.Ins_Unit_Type_Code == null ? "" : plan.Ins_Unit_Type_Code.toString()))) {
        let columnKey = 'Unit_SCO';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }
      //SCO_AIP

      //Premium
      if (!plan.Premium || !parseInt(plan.Premium.toString())) {
        let columnKey = getcolumnkey('Premium') + '_' + plan.Ins_Plan;
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }


    }

    //ICE Plan
    if (plan.Ins_Plan.toUpperCase() == 'ICE') {

      if (plan.Ins_Plan_Type.toUpperCase() == 'BY') {
        // check validation for state and crop code
        if (!(insuranceValidationData.ICE_BY_ELLIGIBLESTATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.ICE_BY_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //ICE_BY_YIELD
        if (plan.Yield_Percent && !insuranceValidationData.ICE_BY_YIELD.some(x => x == parseInt(plan.Yield_Percent.toString()))) {
          let columnKey = 'Yield_Pct_ICE';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
        //ICE_BY_UNIT
        if (!plan.Ins_Unit_Type_Code && !insuranceValidationData.ICE_BY_UNIT.some(x => x == plan.Ins_Unit_Type_Code.toString())) {
          let columnKey = 'Unit_ICE';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
      }

      if (plan.Ins_Plan_Type.toUpperCase() == 'BR') {

        // check validation for state and crop code
        if (!(insuranceValidationData.ICE_BR_ELLIGIBLESTATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.ICE_BR_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //ICE_BR_YIELD
        if (plan.Yield_Percent && !insuranceValidationData.ICE_BR_YIELD.some(x => x == parseInt(plan.Yield_Percent.toString()))) {
          //if (plan.Yield_Percent && plan.Yield_Percent != Yield_Pct_ICE_Values) {
          let columnKey = 'Yield_Pct_ICE';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
        //ICE_BR_UNIT
        if (!plan.Ins_Unit_Type_Code && !insuranceValidationData.ICE_BR_UNIT.some(x => x == plan.Ins_Unit_Type_Code.toString())) {
          let columnKey = 'Unit_ICE';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
      }

      if (plan.Ins_Plan_Type.toUpperCase() == 'CY') {
        // check validation for state and crop code
        if (!(insuranceValidationData.ICE_CY_ELLIGIBLESTATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.ICE_CY_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
        //ICE_CY_YIELD
        if (plan.Yield_Percent && !insuranceValidationData.ICE_CY_YIELD.some(x => x == parseInt(plan.Yield_Percent.toString()))) {
          //if (plan.Yield_Percent && plan.Yield_Percent != Yield_Pct_ICE_Values) {
          let columnKey = 'Yield_Pct_ICE';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
        //ICE_CY_UNIT
        if (!plan.Ins_Unit_Type_Code && !insuranceValidationData.ICE_CY_UNIT.some(x => x == plan.Ins_Unit_Type_Code.toString())) {
          let columnKey = 'Unit_ICE';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
      }

      if (plan.Ins_Plan_Type.toUpperCase() == 'CR') {

        // check validation for state and crop code
        if (!(insuranceValidationData.ICE_CR_ELLIGIBLESTATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.ICE_CR_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //ICE_CR_YIELD
        if (plan.Yield_Percent && !insuranceValidationData.ICE_CR_YIELD.some(x => x == parseInt(plan.Yield_Percent.toString()))) {
          //if (plan.Yield_Percent && plan.Yield_Percent != Yield_Pct_ICE_Values) {
          let columnKey = 'Yield_Pct_ICE';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
        //ICE_CR_UNIT
        if (!plan.Ins_Unit_Type_Code && !insuranceValidationData.ICE_CR_UNIT.some(x => x == plan.Ins_Unit_Type_Code.toString())) {
          let columnKey = 'Unit_ICE';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
      }
      //Premium
      if (!plan.Premium || !parseInt(plan.Premium.toString())) {
        let columnKey = getcolumnkey('Premium') + '_' + plan.Ins_Plan;
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }
    }

    //PCI Plan
    if (plan.Ins_Plan.toUpperCase() == 'PCI') {

      // check validation for state and crop code
      if (!(insuranceValidationData.PCI_ELLIGIBLESTATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
        insuranceValidationData.PCI_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
      )) {
        let columnKey = 'Subtype_HMAX';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }
      //PCI_FCMC
      if (plan.Custom1 && !insuranceValidationData.PCI_FCMC.some(x => x == parseInt(plan.Custom1.toString()))) {
        let columnKey = 'FCMC_PCI';
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

      //PCI_AIP


      //Premium
      if (!plan.Premium || !parseInt(plan.Premium.toString())) {
        let columnKey = getcolumnkey('Premium') + '_' + plan.Ins_Plan;
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }

    }

    //Crop Hail Plan
    if (plan.Ins_Plan.toUpperCase() == 'CROPHAIL') {

      if (plan.Ins_Plan_Type.toUpperCase() == 'BASIC') {

        // check validation for state and crop code
        if (!(insuranceValidationData.CROPHAIL_BASIC_ELLIGIBLESTATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.CROPHAIL_BASIC_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //CROPHAIL_BASIC_DEDUCTMAX
        if (plan.Deductible_Percent && !insuranceValidationData.CROPHAIL_BASIC_DEDUCTMAX.some(x => x == parseInt(plan.Deductible_Percent.toString()))) {
          let columnKey = 'Deduct_Pct_CROPHAIL';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
        //CROPHAIL_BASIC_AIP
      }

      if (plan.Ins_Plan_Type.toUpperCase() == 'COMPANION') {

        // check validation for state and crop code
        if (!(insuranceValidationData.CROPHAIL_COMPANION_ELLIGIBLESTATES.some(x => x == lookupStateValueFromPassedRefObject(plan.State_ID, this.refdata)) &&
          insuranceValidationData.CROPHAIL_COMPANION_ELLIGIBLECROPS.some(x => x == plan.Crop_Code)
        )) {
          let columnKey = 'Subtype_HMAX';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //CROPHAIL_COMPANION_YIELD
        if (plan.Yield_Percent && !insuranceValidationData.CROPHAIL_COMPANION_YIELD.some(x => x == parseInt(plan.Yield_Percent.toString()))) {
          let columnKey = 'Yield_Pct_CROPHAIL';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }

        //CROPHAIL_COMPANION_PRICE
        if (plan.Ins_Plan_Type == 'Companion' && plan.Price_Percent && !insuranceValidationData.CROPHAIL_COMPANION_PRICE.some(x => x == parseInt(plan.Price_Percent.toString()))) {
          let columnKey = 'Price_Pct_CROPHAIL';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }


        //CROPHAIL_COMPANION_DEDUCTMAX
        if (plan.Deductible_Percent && !insuranceValidationData.CROPHAIL_COMPANION_DEDUCTMAX.some(x => x == parseInt(plan.Deductible_Percent.toString()))) {
          let columnKey = 'Deduct_Pct_CROPHAIL';
          this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
        }
        //CROPHAIL_COMPANION_AIP
      }

      //Premium
      if (!plan.Premium || !parseInt(plan.Premium.toString())) {
        let columnKey = getcolumnkey('Premium') + '_' + plan.Ins_Plan;
        this.pushErrorsInsurancePolicies(mpciPlan, columnKey, errors);
      }
    }
    if (environment.isDebugModeActive) console.timeEnd('Validate_InsurancePolicyData start Time');
    return;
  }

  private pushErrorsInsurancePolicies(plan: Loan_Policy, columnKey: string, errors: errormodel[], text?: string, validation_id?: number) {
    if (!columnKey.includes('SCO')) {
      let res = <AgGridValidationResult>{
        level: validationlevel.level2,
        Validation_ID_Text: text || `Invalid ${To_Friendly_Name(columnKey)} - ${this.getValidationText(plan)}`,
        Validation_ID: validation_id || 26
      };

      let errorMessage = this.getErrorMessage(
        plan,
        'Loan_Policy_ID',
        'Ins_',
        columnKey,
        'insurance',
        Chevronkeys.InsurancePolices,
        res
      );

      errors.push(errorMessage);
    }
  }

  /**
   * Validation Text for Policy
   * @param plan Loan Policy
   */
  private getValidationText(plan: Loan_Policy) {
    return `${plan.Ins_Plan} | ${plan.Ins_Plan_Type} | ${To_Friendly_Policy_Name(plan.Policy_Key)}`
  }




  //#endregion [Insurance Ploicies table data]

  /**
   * Use this function to validate table
   * @param rowData
   * @param coldefs
   * @param page
   * @param tableId
   * @param chevronKey
   * @param rowKey Other than Index for Row Id, i.e. - 'Assoc_Id'
   */
  public validateTableFields<T>(
    rowData: Array<T>,
    coldefs: any[],
    page: Page,
    tableId: string,
    chevronKey: string,
    rowKey: string
  ) {
    if (!rowData) {
      return;
    }

    let currenterrors = this.localstorage.retrieve(
      environment.errorbase
    ) as Array<errormodel>;

    _.remove(currenterrors, function (param) {
      return param.chevron == chevronKey;
    });

    rowData.forEach((obj: any) => {
      Object.keys(obj).forEach(key => {
        const col = coldefs.find(p => p.field == key);
        if (col != undefined) {
          if (Object.prototype.toString.call(col.validations) == "[object Function]") {
            let validation: AgGridValidation<T> = col.validations;
            const res = validation(obj, key);
            if (res.result) {
              currenterrors.push(this.getErrorMessage(obj, rowKey, tableId, key, page, chevronKey, res));
            }
          } else if (Object.prototype.toString.call(col.validations) == "[object Object]") {
            let result: AgGridValidationResult = Object.keys(col.validations).reduce((acc: AgGridValidationResult, current) => {

              let colValidation: AgGridValidation<T> = col.validations[current]
              let res: AgGridValidationResult = <AgGridValidationResult>{};

              if (Object.prototype.toString.call(colValidation) === "[object Function]") {
                res = colValidation(obj, key);
              } else {
                if (colValidation) {
                  res = RequiredValidation(obj, key);
                }
              }

              if (res.result) {
                acc.level = acc.level > res.level ? acc.level : res.level;
                acc.Validation_ID_Text = acc.level > res.level ? acc.Validation_ID_Text : res.Validation_ID_Text;
                acc.hoverText = acc.level > res.level ? acc.hoverText : res.hoverText;
                acc.Validation_ID = acc.level > res.level ? acc.Validation_ID : res.Validation_ID;
                acc.ignore = acc.level > res.level ? acc.ignore : res.ignore;
              }

              acc.result = acc.result || res.result;

              return acc;

            }, <AgGridValidationResult>{
              result: false,
              level: validationlevel.level1
            });

            if (result.result) {
              currenterrors.push(this.getErrorMessage(obj, rowKey, tableId, key, page, chevronKey, result));
            }

          } else if (col.validations && col.validations.required) {
            let res = RequiredValidation(obj, key);
            if (res.result) {
              currenterrors.push(this.getErrorMessage(obj, rowKey, tableId, key, page, chevronKey, res));
            }
          }
        }
      });
    });

    this.localstorage.store(environment.errorbase, _.uniq(currenterrors));
  }

  public validateFormField(formGroup: FormGroup, page: Page, component: string, formGroupName: string, chevronKey) {

    let currenterrors = this.localstorage.retrieve(
      environment.errorbase
    ) as Array<errormodel>;

    _.remove(currenterrors, function (param) {
      return param.chevron == chevronKey;
    });

    Object.keys(formGroup.controls).forEach(formControlName => {

      let hasError = formGroup.get(formControlName).invalid;

      if (hasError) {
        //check if required
        const validator = formGroup.controls[formControlName].validator(new FormControl());
        let isRequired = validator && validator.required ? true : false;
        let value = formGroup.controls[formControlName].value;
        currenterrors.push(this.formErrorMessage(page, component, formGroupName, formControlName, value, chevronKey, isRequired));
      }
    });

    this.localstorage.store(environment.errorbase, _.uniq(currenterrors));
  }

  public validate_Questions(questions: RefQuestions[], page: string, policy: string) {
    try {
      let currenterrors = this.localstorage.retrieve(
        environment.errorbase
      ) as Array<errormodel>;

      let chevron = `Q_${page}`;

      if (policy) {
        chevron = `${chevron}_${policy}`;
      }

      _.remove(currenterrors, function (param) {
        return param.chevron == chevron;
      });

      questions.forEach(q => {
        if (
          q.FC_Response_Detail == undefined ||
          q.FC_Response_Detail == null
        ) {
          if (q.Subsidiary_Question_ID_Ind == 1 && !q.Subsidiary_Q_Visible) {
            return;
          }

          let error = <errormodel>{
            tab: page,
            details: [],
            level: validationlevel.level2,
            chevron: chevron,
            quetionId: q.Question_ID,
            Validation_ID_Text: `Reponse required: ${q.Question_ID_Text}`,
            hoverText: `Reponse required: ${q.Question_ID_Text}`,
            Validation_ID: 25
          };

          currenterrors.push(error);
        }
      });

      currenterrors = currenterrors.filter(a => a.Validation_ID_Text);
      this.localstorage.store(environment.errorbase, _.uniq(currenterrors));
    } catch {

    }
  }

  private formErrorMessage(page: Page, component: string, formGroupName: string, formControlName: string, value: string, chevronKey, isRequired: boolean) {

    let level = isRequired && value ? 2 : isRequired && !value ? 4 : !isRequired && value ? 1 : !isRequired && !value ? 3 : 0;

    let controlName = '';

    if (formControlName == 'Borrower_SSN_Hash') {
      controlName = 'Borrower Entity ID';
    } else if (formControlName == 'Farmer_SSN_Hash') {
      controlName = 'Farmer Entity ID';
    } else if (formControlName == 'Spouse_SSN_Hash') {
      controlName = 'Spouse Entity ID';
    } else {
      controlName = To_Friendly_Name(formControlName);
    }

    return <errormodel>{
      tab: page,
      formDetails: {
        component: component,
        formGroup: formGroupName,
        formControlName: formControlName,
      },
      details: this.getClass(level),
      chevron: chevronKey,
      level: this.getLevel(level),
      Validation_ID_Text: `${controlName} required`,
      hoverText: `${controlName} required`,
      Validation_ID: 30
    }
  }

  public ValidateLoanComment(comment: string) {
    try {
      let currenterrors = this.localstorage.retrieve(
        environment.errorbase
      ) as Array<errormodel>;

      _.remove(currenterrors, function (param) {
        return param.chevron == `Loan_Comment`;
      });

      if (!comment) {
        let error = <errormodel>{
          tab: Page.committee,
          details: [],
          level: validationlevel.level2,
          chevron: `Loan_Comment`,
          Validation_ID_Text: 'Loan comment required.',
          hoverText: 'Loan comment required.',
          Validation_ID: 23
        };

        currenterrors.push(error);
      }

      this.localstorage.store(environment.errorbase, _.uniq(currenterrors));
    } catch {

    }
  }

  public Validate_Loan_Documents(localLoanObj: loan_model) {
    try {
      let currenterrors = this.localstorage.retrieve(
        environment.errorbase
      ) as Array<errormodel>;

      _.remove(currenterrors, function(param) {
        return param.chevron == `Loan_Document`;
      });

      if(localLoanObj.LoanMaster.Loan_Status == LoanStatus.Working) {
        let documents = localLoanObj.Loan_Documents.filter(
          d => d.ActionStatus != 3 && d.Document_Type_Level == 1
        );

        documents.forEach(doc => {
          if (!doc.Upload_Date_Time) {
            currenterrors.push({
              chevron: 'Loan_Document',
              tab: Page.checkout,
              level: 2,
              details: [],
              quetionId: doc.Document_Type_Level,
              Validation_ID_Text: `Documentation Required - ${doc.Document_Name}`,
              hoverText: `Documentation Required - ${doc.Document_Name}`,
              Validation_ID: 31
            });
          }
        });
      } else {
        let documents = localLoanObj.Loan_Documents.filter(
          d => d.ActionStatus != 3
        );

        documents.forEach(doc => {
          if (!doc.Upload_Date_Time) {
            currenterrors.push({
              chevron: 'Loan_Document',
              tab: Page.checkout,
              level: 2,
              details: [],
              quetionId: doc.Document_Type_Level,
              Validation_ID_Text: `Documentation Required - ${doc.Document_Name}`,
              hoverText: `Documentation Required - ${doc.Document_Name}`,
              Validation_ID: 31
            });
          }
        });
      }

      this.localstorage.store(
        environment.errorbase,
        _.uniq(currenterrors)
      );
    } catch {}
  }

  private getErrorMessage(obj, rowKey, tableId, key, tab, chevronKey, res: AgGridValidationResult) {
    return <errormodel>{
      cellid: tableId + obj[rowKey] + '_' + key,
      colId: colId(key),
      rowId: rowId(tableId, obj[rowKey]),
      tab: tab,
      details: this.getClass(res.level),
      chevron: chevronKey,
      level: this.getLevel(res.level),
      Validation_ID_Text: res.Validation_ID_Text,
      hoverText: res.hoverText || res.Validation_ID_Text,
      Validation_ID: res.Validation_ID,
      ignore: res.ignore
    }
  }

  private getClass(level: validationlevel) {
    switch (level) {
      case validationlevel.level1:
        return ['cell-validation', 'validation-1'];
      case validationlevel.level2:
        return ['cell-validation', 'validation-2'];
      case validationlevel.level1blank:
        return ['cell-validation', 'validation-1-blank'];
      case validationlevel.level2blank:
        return ['cell-validation', 'validation-2-blank'];
    }
  }

  private getLevel(level: validationlevel) {
    switch (level) {
      case validationlevel.level1:
      case validationlevel.level1blank:
        return validationlevel.level1;

      case validationlevel.level2:
      case validationlevel.level2blank:
        return validationlevel.level2;
    }
  }

  /**
   *
   * @param errors Error Array
   * @param tableId Table Id used in HTML
   */
  public highlighErrorCells(errors: errormodel[], tableId?: string) {
    let parent;

    if (tableId) {
      parent = document.getElementById(tableId);
    } else {
      parent = window.document;
    }

    if (parent) {
      const items = parent.querySelectorAll('.ag-cell');

      Array.prototype.forEach.call(items, item => {
        item.classList.remove('cell-validation');
        item.classList.remove('validation-1');
        item.classList.remove('validation-2');
        item.classList.remove('validation-1-blank');
        item.classList.remove('validation-2-blank');
      });

      errors.forEach(element => {
        let cell = parent.querySelector(`${element.rowId} ${element.colId}`);
        element.details.forEach(detailsClass => {
          try {
            cell.classList.add(detailsClass);
          } catch (ex) { }
        });
      });
    }
  }
}

/**
 * Use This Function to get Column Id for Error
 *
 *  @param key  Column Name
 */
export function colId(key: string) {
  return `[col-id="${key}"]`;
}

/**
 * Use this Function to get Row id for Error
 * @param tblId Table Id
 * @param key Row Index/Key
 */
export function rowId(tblId: string, key: any) {
  return `[row-id="${tblId}${key}"]`;
}

/**
 * Use this function to compare to array
 */
export function arraysEqual(_arr1, _arr2) {
  if (
    !Array.isArray(_arr1) ||
    !Array.isArray(_arr2) ||
    _arr1.length !== _arr2.length
  ) {
    return false;
  }

  let arr1 = _arr1.concat().sort();
  let arr2 = _arr2.concat().sort();

  for (let i = 0; i < arr1.length; i++) {
    if (arr1[i] !== arr2[i]) {
      return false;
    }
  }

  return true;
}
