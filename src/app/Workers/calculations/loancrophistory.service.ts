import { Injectable } from '@angular/core';
import { loan_model } from '../../models/loanmodel';
import { Loan_Crop_History_FC } from '../../models/cropmodel';
import { count } from 'rxjs/operators';
import { LoggingService } from '../../services/Logs/logging.service';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { Helper } from '@lenda/services/math.helper';

@Injectable()
export class LoancrophistoryService {
  public input: loan_model;

  public returnables = new Array<Loan_Crop_History_FC>();
  public years = [];

  constructor(
    private logging: LoggingService,
    private localStorageService: LocalStorageService
  ) {
    // for (let i = 1; i < 8; i++) {
    //   this.years.push(new Date().getFullYear() - i);
    // }
  }
  prepare_Crop_Yield() {
    let refData: RefDataModel = this.localStorageService.retrieve(
      environment.referencedatakey
    );

    for (let i = 0; i < this.input.CropYield.length; i++) {
      const cYield = this.input.CropYield[i];

      let refCropPractice = refData.CropList.find(
        cl => cl.Crop_And_Practice_ID == cYield.Crop_ID
      );

      if (refCropPractice) {
        let cropPractice = this.input.LoanCropPractices.find(
          cp => cp.Crop_Practice_ID == refCropPractice.Crop_And_Practice_ID
        );
        if (cropPractice) {
          cYield.APH = cropPractice.LCP_APH;
        } else {
          cYield.APH = 0;
        }
      } else {
        cYield.APH = 0;
      }

      if (refCropPractice) {
        let cropPractice = this.input.LoanCropPractices.find(
          cp => cp.Crop_Practice_ID == refCropPractice.Crop_And_Practice_ID
        );
        if (cropPractice) {
          cYield.Rate_Yield = cropPractice.FC_RateYieldAVG;
        } else {
          cYield.Rate_Yield = 0;
        }
      } else {
        cYield.Rate_Yield = 0;
      }

      let cropyielditems = [];
      this.years = [];
      cYield.CropYield = cYield.APH;
      for (let year = 1; year < 8; year++) {
        this.years.push(cYield.CropYear - year);
      }
      this.years.forEach(year => {
        if (cYield[year] != null && cYield[year] != 0) {
          cropyielditems.push(cYield[year]);
        }
      });

      if (cropyielditems.length <= 2) {
        cYield.CropYield = Math.round(cYield.APH);
      } else {
        let sum = cropyielditems.reduce((p, n) => {
          return p + n;
        });
        let max = Math.max.apply(null, cropyielditems);
        let min = Math.min.apply(null, cropyielditems);
        let coutie = cropyielditems.length - 2;
        cYield.CropYield = Math.round((sum - max - min) / coutie);
      }

      cYield.APH_Percent = Helper.divide(cYield.APH, cYield.CropYield) * 100;
    }
  }

  prepareLoancrophistorymodel(input: loan_model): loan_model {
    try {
      this.input = input;
      let starttime = new Date().getTime();
      this.prepare_Crop_Yield();
      let endtime = new Date().getTime();
      //level 2 log
      this.logging.checkandcreatelog(
        2,
        'Calc_CropYield',
        'LoanCalculation timetaken :' + (starttime - endtime).toString() + ' ms'
      );
      return this.input;
    } catch (e) {
      this.logging.checkandcreatelog(1, 'Calc_CropYield', e);
      return input;
    }
  }
}
