import { Injectable } from '@angular/core';

import { LoggingService } from '@lenda/services/Logs/logging.service';
import { loan_model } from '@lenda/models/loanmodel';

@Injectable()
export class QuestionscalculationworkerService {
  constructor(public logging: LoggingService) {}

  performcalculationforquestionsupdated(input: loan_model): loan_model {
    try {
      let starttime = new Date().getTime();
      const lm = input.LoanMaster;

      input.LoanQResponse.forEach(q => {
        // we are going to update according to question id
        switch (q.Question_ID) {
          case 7: // judgement question
            lm.Judgement_Ind = q.Response_Detail == 'Yes' ? 1 : 0;
            break;

          case 3: // Current bankruptcy question
            lm.Current_Bankruptcy_Status = q.Response_Detail == 'Yes' ? 1 : 0;
            break;

          case 5: // Previous bankruptcy question
            lm.Previous_Bankruptcy_Status = q.Response_Detail == 'Yes' ? 1 : 0;
            break;
          default:
            break;
        }
      });

      let endtime = new Date().getTime();
      //level 2 log
      this.logging.checkandcreatelog(
        2,
        'Calc_Question_1',
        'LoanCalculation timetaken :' + (endtime - starttime).toString() + ' ms'
      );
      return input;
    } catch (e) {
      //level 1 log as its error
      this.logging.checkandcreatelog(1, 'Calc_Question', e);
      return input;
    }
  }
}
