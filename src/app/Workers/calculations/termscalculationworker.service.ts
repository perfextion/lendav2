import { Injectable } from '@angular/core';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { loan_model } from '@lenda/models/loanmodel';
import { RefDataService } from '@lenda/services/ref-data.service';
import * as moment from 'moment';
import { daysDiff } from '@lenda/aggridformatters/valueformatters';

@Injectable()
export class TermsCalculationWorkerService {
  constructor(private refdataservice: RefDataService) {}

  updateReturnScale(input: loan_model) {
    try {
      let returnScaleUpdateFactor = 0;

      if (this.isCurrentMaturityPastDefaultMaturity(input)) {
        let daysdifference = daysDiff(
          this.formatMaturityDate(input.LoanMaster.Maturity_Date, input),
          this.refdataservice.getDefaultMaturityDate(input.LoanMaster.Crop_Year)
        );

        returnScaleUpdateFactor =
          (this.refdataservice.getCostToCarry() / 365) * daysdifference;
      } else {
        returnScaleUpdateFactor = 0;
      }

      returnScaleUpdateFactor = parseFloat(returnScaleUpdateFactor.toFixed(1));
      this.refdataservice.updateReturnScale(returnScaleUpdateFactor);
      input.LoanMaster.FC_Return_Scale = returnScaleUpdateFactor;

      return input;
    } catch {
      input.LoanMaster.FC_Return_Scale = 0;
      return input;
    }
  }

  private isCurrentMaturityPastDefaultMaturity(input: loan_model) {
    let defaultMaturityDate = this.refdataservice.getDefaultMaturityDate(input.LoanMaster.Crop_Year);

    let currentMaturityDate = new Date(
      this.formatMaturityDate(input.LoanMaster.Maturity_Date, input)
    );

    return +currentMaturityDate > +defaultMaturityDate;
  }

  private formatMaturityDate(strDate, input: loan_model) {
    let finalDate;
    if (strDate) {
      let date = new Date(strDate);
      if (isNaN(date.getTime()) || date < new Date()) {
        finalDate = this.refdataservice.getDefaultMaturityDate(input.LoanMaster.Crop_Year);
      } else {
        finalDate = date;
      }
    } else {
      finalDate = this.refdataservice.getDefaultMaturityDate(input.LoanMaster.Crop_Year);
    }

    return moment(finalDate).format('MM/DD/YYYY');
  }
}
