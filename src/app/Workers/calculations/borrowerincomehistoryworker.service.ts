import { Injectable } from '@angular/core';
import { borrower_model, borrower_income_history_model, loan_model } from '../../models/loanmodel';
import { LoggingService } from '../../services/Logs/logging.service';

@Injectable()
export class Borrowerincomehistoryworker {
  private input: loan_model
  constructor(public logging: LoggingService) { }


  prepareborrowerincomehistorymodel(input: loan_model): loan_model {
    try {
      this.input = input
      let hasborrowerinchistory=false;
      let starttime = new Date().getTime();
        for (let i = 0; i < this.input.BorrowerIncomeHistory.length; i++) {
            let value =this.prepare_fc_borrower_income(this.input.BorrowerIncomeHistory[i]);
            if(value==true && hasborrowerinchistory==false)
            {
              hasborrowerinchistory=true;
            }
        }
      let endtime = new Date().getTime();
        //level 2 log
      this.logging.checkandcreatelog(2, 'Calc_BorrowIncomeHistory', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
      this.logging.checkandcreatelog(2, 'Calc_BorrowIncomeHistory_1', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
      this.input.CalculatedVariables.FC_hasborrowerinc=hasborrowerinchistory;
      return this.input;
    } catch(e){
        //level 1 log as its error
      this.logging.checkandcreatelog(1, 'Calc_BorrowIncomeHistory', e);
      return this.input;
    }
  }

  prepare_fc_borrower_income(params){
    params.FC_Borrower_Income = params.Borrower_Revenue - params.Borrower_Expense;
    if(params.Borrower_Revenue!=0 || params.Borrower_Expense!=0)
    {
      return true;
    }
  }



}
