import { Injectable } from '@angular/core';

import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { loan_model, LoanStatus, AssociationTypeCode, Loan_Association } from '@lenda/models/loanmodel';
import { Loan_Crop_Unit, Crop_Unit_Policy } from '@lenda/models/cropmodel';
import { Loan_Policy } from '@lenda/models/insurancemodel';
import { Loan_Farm } from '@lenda/models/farmmodel.';
import {
  RKSFind,
  RKS_RMAFind,
  precise_round,
  IDictionary,
  Get_Policy_Key,
  Get_Policy_Adjustment_Key,
  RKS_RMAADJUSTMENTFind,
  RKS_RMAADJUSTMENTPercFind,
  Get_Collateral_Mkt_Disc,
  Get_Collateral_Action_Adj_Ins_Disc,
  Get_Ins_Disc
} from '@lenda/services/common-utils';
import { RefDataModel } from '@lenda/models/ref-data-model';

import { InsCalcHelper } from '@lenda/services/insurance/insurance-calculation-helper';
import { Helper } from '@lenda/services/math.helper';

import { LoanAuditTrailService } from '@lenda/services/audit-trail.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';

@Injectable()
export class LoancropunitcalculationworkerService {
  public input: loan_model;
  public refData: RefDataModel;

  private discounts: IDictionary<number> = {};

  constructor(
    private logging: LoggingService,
    public localstorage: LocalStorageService,
    private auditTrail: LoanAuditTrailService
  ) {
    this.refData = this.localstorage.retrieve(environment.referencedatakey);
    this.Get_Discounts();


    this.localstorage.observe(environment.referencedatakey).subscribe(res => {
      this.refData = res;
      this.Get_Discounts();
    });
  }

  private Get_Discounts() {
    if (this.refData != null) {
      this.refData.Discounts.forEach(disc => {
        this.discounts[disc.Discount_Key] = disc.Discount_Value;
      });
    }
  }

  prepare_crops_revenue() {
    for (let entry of this.input.LoanCropUnits) {
      entry.FC_Revenue = entry.CU_Acres * 200 * .95 * (entry.Z_Price + entry.Z_Basis_Adj + entry.Z_Marketing_Adj + entry.Z_Rebate_Adj);
    }
  }

  prepare_crops_subtotalacres() {
    this.input.LoanCropUnitFCvalues.FC_SubTotalAcres = this.input.LoanCropUnits.reduce(function (prev, cur) {
      return prev + cur.CU_Acres;
    }, 0);
  }

  prepare_crops_subtotalrevenue() {
    this.input.LoanCropUnitFCvalues.FC_SubtotalCropRevenue = this.input.LoanCropUnits.reduce(function (prev, cur) {
      return prev + cur.FC_Revenue;
    }, 0);
  }

  prepare_crops_totalrevenue() {
    this.input.LoanCropUnitFCvalues.FC_TotalRevenue = this.input.LoanCropUnitFCvalues.FC_SubtotalCropRevenue;
  }

  prepare_crops_totalbudget() {
    //still needs calculations
    this.input.LoanCropUnitFCvalues.FC_TotalBudget = 0;
  }

  prepare_crops_estimatedinterest() {
    //still needs calculations
    this.input.LoanCropUnitFCvalues.FC_EstimatedInterest = 0;
  }

  prepare_crops_totalcashflow() {
    this.input.LoanCropUnitFCvalues.FC_TotalCashFlow = this.input.LoanCropUnitFCvalues.FC_TotalRevenue - this.input.LoanCropUnitFCvalues.FC_TotalBudget - this.input.LoanCropUnitFCvalues.FC_EstimatedInterest;
  }

  prepareLoancropunitmodel(input: loan_model): loan_model {
    try {
      this.input = input;
      let starttime = new Date().getTime();
      this.prepare_crops_revenue();
      this.prepare_crops_subtotalacres();
      this.prepare_crops_subtotalrevenue();
      this.prepare_crops_totalrevenue();
      this.prepare_crops_totalbudget();
      this.prepare_crops_estimatedinterest();
      this.prepare_crops_totalcashflow();
      let endtime = new Date().getTime();
      //level 2 log
      this.logging.checkandcreatelog(2, 'Calc_CropUnit', "LoanCalculation timetaken :" + (starttime - endtime).toString() + " ms");
      return this.input;
    } catch (e) {
      //level 1 log as its an error
      this.logging.checkandcreatelog(1, 'Calc_CropUnit', e);
      return input;
    }
  }

  prepareLCP_APHfromAPH(localLoanObject: loan_model) {
    try {
      let starttime = new Date().getTime();
      let cropPractices = localLoanObject.LoanCropPractices;
      if (cropPractices) {
        cropPractices.forEach(cp => {
          let sumOfAcresIntoAPH = 0;
          let sumOfAcres = 0;
          let sumOfAPH = 0;

          let cropUnits = localLoanObject.LoanCropUnits.filter(cu => cu.Crop_Practice_ID == cp.Crop_Practice_ID);
          if (cropUnits) {

            cropUnits.forEach(cu => {

              let insaph = cu.Ins_APH == 0 ? cu.FC_CropYield : cu.Ins_APH;
              sumOfAcresIntoAPH += (cu.CU_Acres || 0) * (insaph || 0);
              sumOfAcres += (cu.CU_Acres || 0);
              sumOfAPH += (insaph || 0);
            });

            if (sumOfAcres && sumOfAcresIntoAPH) {
              cp.LCP_APH = sumOfAcresIntoAPH / sumOfAcres;
              cp.LCP_APH = parseFloat(cp.LCP_APH.toFixed(2));
            } else {
              cp.LCP_APH = 0;
            }

            if (sumOfAcres == 0) {
              cp.LCP_APH = Helper.divide(sumOfAPH, cropUnits.length);
              cp.LCP_APH = parseFloat(cp.LCP_APH.toFixed(2));
            }
          }
        });
      }

      let endtime = new Date().getTime();
      //level 2 log
      this.logging.checkandcreatelog(2, 'Calc_CropUnit_APH', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
      return localLoanObject;
    } catch (e) {
      //level 1 log
      this.logging.checkandcreatelog(1, 'Calc_CropUnit_APH', e);
      return localLoanObject;
    }
  }

  prepare_Rent_Percent(loanObj: loan_model) {
    try {
      if (loanObj.FarmCropOverride.length > 0) {
        loanObj.Farms.forEach(farm => {
          if (farm.Crop_share_Detail_Indicator == 1) {
            loanObj.FarmCropOverride.filter(a => a.Farm_ID == farm.Farm_ID)
            .forEach(override => {
              let cu = loanObj.LoanCropUnits.filter(a => a.ActionStatus != 3)
              .find(a =>
                a.Farm_ID == farm.Farm_ID &&
                `${a.Crop_Code}_${a.Crop_Type_Code == '-' ? 0 : a.Crop_Type_Code}_${a.Crop_Practice_Type_Code.toUpperCase()}_${a.Crop_Practice_Type_Code.toUpperCase()}` == override.Crop_Code
              );

              if (cu) {
                if (override.Share_Rent_Override) {
                  cu.FC_Rent_Percent = override.Share_Rent_Percent;
                  cu.FC_ProdPerc = 100 - cu.FC_Rent_Percent;
                  cu.FC_Disabled = false;
                } else {
                  cu.FC_Rent_Percent = 100 - farm.Percent_Prod;
                  cu.FC_ProdPerc = 100 - cu.FC_Rent_Percent;
                  cu.CU_Acres = 0;
                  cu.FC_Disabled = true;
                }
              }
            });
          } else {
            loanObj.LoanCropUnits.filter(a => a.ActionStatus != 3).filter(a => a.Farm_ID == farm.Farm_ID).forEach(a => {
              a.FC_Rent_Percent = 100 - farm.Percent_Prod;
              a.FC_ProdPerc = 100 - a.FC_Rent_Percent;
              a.FC_Disabled = false;
            });
          }
        });
      } else {
        loanObj.Farms.forEach(farm => {
          loanObj.LoanCropUnits.filter(a => a.ActionStatus != 3).filter(a => a.Farm_ID == farm.Farm_ID).forEach(a => {
            a.FC_Disabled = false;
            a.FC_Rent_Percent = 100 - farm.Percent_Prod;
            a.FC_ProdPerc = 100 - a.FC_Rent_Percent;
          });
        });

      }
      return loanObj;
    } catch {
      return loanObj;
    }
  }

  prepareRate_YieldfromYield(localLoanObject: loan_model) {
    try {
      let starttime = new Date().getTime();
      let cropPractices = localLoanObject.LoanCropPractices;
      if (cropPractices) {
        cropPractices.forEach(cp => {
          let sumOfAcresIntoRateyield = 0;
          let sumOfAcres = 0;
          let cropUnits = localLoanObject.LoanCropUnits.filter(cu => cu.Crop_Practice_ID == cp.Crop_Practice_ID);
          if (cropUnits) {

            cropUnits.forEach(cu => {

              let rateyield = cu.Rate_Yield;
              sumOfAcresIntoRateyield += cu.CU_Acres * rateyield;
              sumOfAcres += cu.CU_Acres;
            });
          }

          if (sumOfAcres && sumOfAcresIntoRateyield) {
            cp.FC_RateYieldAVG = sumOfAcresIntoRateyield / sumOfAcres;
            cp.FC_RateYieldAVG = parseFloat(cp.FC_RateYieldAVG.toFixed(2));
          } else {
            cp.FC_RateYieldAVG = 0;
          }

        });
      }

      let endtime = new Date().getTime();
      //level 2 log
      this.logging.checkandcreatelog(2, 'Calc_CropUnit_APH', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
      return localLoanObject;
    } catch (e) {
      //level 1 log
      this.logging.checkandcreatelog(1, 'Calc_CropUnit_APH', e);
      return localLoanObject;
    }
  }


  getDiscountbyKey(key: string) {
    try {
      let value = this.refData.Discounts.find(p => p.Discount_Key == key).Discount_Value;
      if (value == undefined || value == null) {
        value = 0;
      }
      return value;
    } catch {
      return 0;
    }
  }

  prepape_Insurance_Price(loanObj: loan_model) {
    try {
      if (loanObj.LoanMaster.Loan_Status == LoanStatus.Working) {
        const audits = [];

        loanObj.LoanPolicies.filter(a => a.ActionStatus == 1).forEach(item => {
          this.Set_Price(loanObj, item, audits);
        });

        this.Add_Audit_Insurance_Price(audits, loanObj);
      }
      return loanObj;
    } catch {
      return loanObj;
    }
  }

  private Set_Price(loanObj, item: Loan_Policy, audits: any[]) {
    let key = Get_Policy_Key(loanObj, item);
    let rma = RKS_RMAFind(key, item.State_ID, item.County_ID, this.refData.RMA_Insurance_Offers);
    let price = rma.MPCI_Base_Price + Math.max(0, rma.MPCI_Harvest_Price - rma.MPCI_Base_Price) * rma.Price_Established / 100;
    item.Price = price;

    const id = JSON.stringify(rma);
    const text = `Policy Price for - ${key}`;

    audits.push({
      id: id,
      text: text,
      type: 'RMA Price'
    });
  }

  private Add_Audit_Insurance_Price(audits: any[], loanObj: loan_model) {
    if (audits && audits.length > 0) {
      this.auditTrail.addAuditTrailTextsAsync(audits, loanObj);
    }
  }

  subpolicieskeys = ['stax', 'sco', 'pci', 'crophail', 'select', 'ramp', 'hmax', 'ice'];

  // tHis will Cater only MPCI
  prepareMPCIValues(MPCI_Plan: Loan_Policy, CropUnitRecord: Loan_Crop_Unit, input: loan_model, farm: Loan_Farm) {

    if (MPCI_Plan != undefined && MPCI_Plan != null) {
      CropUnitRecord.FC_Ins_Policy_ID = MPCI_Plan.Loan_Policy_ID;
      CropUnitRecord.FC_Ins_Unit = MPCI_Plan.Ins_Unit_Type_Code;
      CropUnitRecord.FC_Primary_limit = MPCI_Plan.Upper_Level;

      //Mkt Value
      try {
        //z price to be flipped to Adj_price
        // Ins_Aph flipped to Crop yield

        ///MPCI STARTS HERE
        //New Validation 12/12/2018
        let adjustmentkey = Get_Policy_Adjustment_Key(input, MPCI_Plan);
        let adjprice = RKS_RMAADJUSTMENTFind(adjustmentkey, this.refData.Ref_Option_Adj_Lookups);
        let adjperc = RKS_RMAADJUSTMENTPercFind(adjustmentkey, this.refData.Ref_Option_Adj_Lookups);
        if (MPCI_Plan.Ins_Plan_Type == "CAT") {
          MPCI_Plan.Upper_Level = this.getDiscountbyKey('MPCI_CAT_UL');
          MPCI_Plan.Price_Percent = this.getDiscountbyKey('MPCI_CAT_PRC_PCT');
          MPCI_Plan.Yield_Percent = this.getDiscountbyKey('MPCI_YLD_PCT');
          MPCI_Plan.Option = null;
        }

        if (['YP', 'RP-HPE', 'RP'].includes(MPCI_Plan.Ins_Plan_Type.trim())) {
          MPCI_Plan.Price_Percent = this.getDiscountbyKey('MPCI_PRC_PCT');
          MPCI_Plan.Yield_Percent = this.getDiscountbyKey('MPCI_YLD_PCT');
        }

        MPCI_Plan.Lower_Level = 0; // for all its zero
        CropUnitRecord.FC_MpciPremium = MPCI_Plan.Premium;
        if (MPCI_Plan.Ins_Plan_Type == "ARH") {
          //Then MPCI Leveli %= Loan_InsurancePolicy.Level_PCT* Loan_InsurancePolicy.Yield_PCT * Loan_InsurancePolicy.Price_PCT
          CropUnitRecord.FC_Level1Perc = MPCI_Plan.Yield_Percent / 100 * MPCI_Plan.Price_Percent / 100 * MPCI_Plan.Upper_Level;
        } else {
          CropUnitRecord.FC_Level1Perc = MPCI_Plan.Upper_Level;
          //
        }
        //APH Calculations for APH Modification
        if (MPCI_Plan.Ins_Plan_Type == "ARH") {
          //Change to CU_APH
          CropUnitRecord.FC_ModifiedAPH = CropUnitRecord.Ins_APH == 0 ? CropUnitRecord.FC_CropYield : CropUnitRecord.Ins_APH;
        } else {
          CropUnitRecord.FC_ModifiedAPH = (CropUnitRecord.Ins_APH == 0 ? CropUnitRecord.FC_CropYield : CropUnitRecord.Ins_APH) * (MPCI_Plan.Price + adjprice + (MPCI_Plan.Price * adjperc / 100));
        }
        ///MPCI VALUES  is ins value
        CropUnitRecord.FC_MPCIvalue_Acre = ((CropUnitRecord.FC_ModifiedAPH * CropUnitRecord.FC_Level1Perc  / 100) - MPCI_Plan.Premium ) * (CropUnitRecord.FC_Insurance_Share / 100 );
        CropUnitRecord.FC_MPCIvalue = Math.round(CropUnitRecord.FC_MPCIvalue_Acre * CropUnitRecord.CU_Acres );

        //MPCI Discount Selection
        let INS_DISC_KEY = "MPCI_" + MPCI_Plan.Ins_Plan_Type + '_' + MPCI_Plan.Crop_Code + '_0_' + MPCI_Plan.Irr_Practice_Type_Code + '_' + MPCI_Plan.Irr_Practice_Type_Code;
        let D_Col_Ins_Discount = RKSFind(INS_DISC_KEY, this.discounts);

        //let Col_INS_DISC = Get_Ins_Disc(this.refData, 'MPCI', MPCI_Plan.Ins_Plan_Type, MPCI_Plan.Crop_Code, MPCI_Plan.Crop_Type_Code, MPCI_Plan.Irr_Practice_Type_Code, MPCI_Plan.Crop_Practice_Type_Code );

        let ACRES_AIP_CERT_INS_DISC =  ( this.input.LoanMaster.AIP_Acres_Verified && this.input.LoanMaster.FSA_Acres_Verified ) ? Get_Collateral_Action_Adj_Ins_Disc('ACV', this.refData) : 0;
        let ACRES_MAP_INS_DISC  = this.input.LoanMaster.Acres_Mapped ? Get_Collateral_Action_Adj_Ins_Disc('MAP', this.refData) : 0;

        let TOTAL_Action_Ins_Disc_Adj = ACRES_AIP_CERT_INS_DISC + ACRES_MAP_INS_DISC;
        let Y_Col_Discount_Total = D_Col_Ins_Discount + TOTAL_Action_Ins_Disc_Adj;

        CropUnitRecord.FC_Disc_MPCI_value_Acre = CropUnitRecord.FC_MPCIvalue_Acre * (1 - Y_Col_Discount_Total / 100);
        CropUnitRecord.FC_Disc_MPCI_value = Math.round(CropUnitRecord.FC_Disc_MPCI_value_Acre * CropUnitRecord.CU_Acres);



        //Insurance value --sum of all insurance mpci values
        CropUnitRecord.Ins_Value_Acre = Math.max(CropUnitRecord.FC_MPCIvalue_Acre, 0);
        CropUnitRecord.Ins_Value = Math.max(CropUnitRecord.FC_MPCIvalue, 0);

        //MPCI type only as if now--We dont have secondary
        CropUnitRecord.Disc_Ins_value_Acre = CropUnitRecord.FC_Disc_MPCI_value_Acre;
        CropUnitRecord.Disc_Ins_value = CropUnitRecord.FC_Disc_MPCI_value;

        // Insurance Sub Policies Calculations
        let subpolicies = input.LoanPolicies.filter(p => p.Policy_Key == MPCI_Plan.Policy_Key && p.Ins_Plan != 'MPCI');
        //Input MPCI Here

        this.givevaluestocropunitpolicy(MPCI_Plan, CropUnitRecord.Loan_CU_ID, input, CropUnitRecord.Ins_Value, CropUnitRecord.Disc_Ins_value, MPCI_Plan.Premium, 0, CropUnitRecord.Disc_Ins_value_Acre, CropUnitRecord.Ins_Value_Acre);
        this.subpolicieskeys.forEach((Subpolicy: string) => {

          let subpolicy;
          if (subpolicies.find(p => p.HR_Exclusion_Ind == true && p.ActionStatus != 3 && p.Ins_Plan.toLowerCase() == Subpolicy.toLowerCase()) != undefined) {
            subpolicy = subpolicies.find(p => p.HR_Exclusion_Ind == true && p.ActionStatus != 3 && p.Ins_Plan.toLowerCase() == Subpolicy.toLowerCase());
          } else {
            subpolicy = subpolicies.find(p => p.HR_Exclusion_Ind == false && p.ActionStatus != 3 && p.Ins_Plan.toLowerCase() == Subpolicy.toLowerCase());
          }
          if (subpolicy != undefined || subpolicy != null) {

            if (subpolicy.Ins_Plan.toLowerCase() == "hmax") {
              //Hmax calculations
              CropUnitRecord = this.prepareHmax(subpolicy, MPCI_Plan, CropUnitRecord, this.input);
            } else if (subpolicy.Ins_Plan.toLowerCase() == "sco") {
              //Starts here

              CropUnitRecord = this.prepareSco(subpolicy, MPCI_Plan, CropUnitRecord, this.input);
            } else if (subpolicy.Ins_Plan.toLowerCase() == "stax") {
              //Starts here
              CropUnitRecord = this.prepareStax(subpolicy, MPCI_Plan, CropUnitRecord, this.input);
            } else if (subpolicy.Ins_Plan.toLowerCase() == "ramp") {
              CropUnitRecord = this.prepareRamp(subpolicy, MPCI_Plan, CropUnitRecord, this.input);
            } else if (subpolicy.Ins_Plan.toLowerCase() == "ice") {
              CropUnitRecord = this.prepareIce(subpolicy, CropUnitRecord, MPCI_Plan, this.input);
            } else if (subpolicy.Ins_Plan.toLowerCase() == "select") {
              CropUnitRecord = this.prepareSelect(subpolicy, MPCI_Plan, CropUnitRecord, this.input);
            } else if (subpolicy.Ins_Plan.toLowerCase() == "pci") {

              CropUnitRecord = this.preparePci(input, CropUnitRecord, subpolicy, MPCI_Plan);
            } else if (subpolicy.Ins_Plan.toLowerCase() == "crophail") {
              CropUnitRecord = this.prepareCropHail(subpolicy, MPCI_Plan, CropUnitRecord, this.input);
            }
          } else {
            //Delete if has a key
            if (input.CropUnitPolicies == undefined) {
              input.CropUnitPolicies = new Array<Crop_Unit_Policy>();
            }
            let investigatedObject = input.CropUnitPolicies.find(p => p.Loan_Crop_Unit_ID == CropUnitRecord.Loan_CU_ID && p.Ins_Plan_Code.toLowerCase() == Subpolicy.toLowerCase());
            if (investigatedObject != undefined) {
              investigatedObject.Status = 3;
            }
          }
        });

      } catch (ex) {

        console.error("Error in Cropunit Calculations");
        CropUnitRecord.Mkt_Value = 0;
        CropUnitRecord.Mkt_Value_Acre = 0;
      }

    }
  }

  private prepareCropHail(plan: Loan_Policy, MPCI_Plan: Loan_Policy, CropUnitRecord: Loan_Crop_Unit, input: loan_model) {
    try {

      if (plan.Ins_Plan_Type == "Basic") {
        plan.Yield_Percent = this.getDiscountbyKey('CROPHAIL_BASIC_YLD_PCT');
        plan.Price_Percent = this.getDiscountbyKey('CROPHAIL_BASIC_PRC_PCT');
        plan.Lower_Level = 0;
        plan.Upper_Level = 100;
        //plan.Deductible_Percent=0;
        plan.Ins_Unit_Type_Code = 'OU';
      } else if (plan.Ins_Plan_Type == "Companion") {
        plan.Upper_Level = this.getDiscountbyKey('CROPHAIL_COMP_UL');
        plan.Lower_Level = MPCI_Plan.Upper_Level;
        //plan.Liability_Percent = 99999999;
        plan.Ins_Unit_Type_Code = MPCI_Plan.Ins_Unit_Type_Code;

      } else {
        plan.Liability_Percent = 99999999;
        plan.Upper_Level = 0;
        plan.Lower_Level = 0;
        plan.Yield_Percent = 100;
        plan.Price_Percent = 100;
      }

      let Z_Unit = plan.Ins_Unit_Type_Code;
      let Z_Options = plan.Option;
      let D_Price = MPCI_Plan.Price; //To be Replaced with lookup Values
      let adjustmentkey = Get_Policy_Adjustment_Key(input, MPCI_Plan);
      let D_Plan_Option_Adj_Price = RKS_RMAADJUSTMENTFind(adjustmentkey, this.refData.Ref_Option_Adj_Lookups);
      let Y_Adj_Prc = D_Price + D_Plan_Option_Adj_Price; //To be Replaced with lookup Values from Plan and Option Adjustment
      let Z_Upper = plan.Upper_Level;
      let Z_Lower = plan.Lower_Level;
      let Z_Area_Yield = plan.Area_Yield;
      let Z_Yield_Pct = plan.Yield_Percent;
      let Z_Price_Pct = plan.Price_Percent;
      let Z_Liability = plan.Liability_Percent; // Mapped Input
      let Z_Deduct_perc = plan.Deductible_Percent; // - Overriding Discount Lookup
      let Z_Late_Deduct_Units = plan.Late_Deductible || 0;
      let Z_Deduct_Units = plan.Deductible_Units || 0; //
      // let D_ICC=plan.Custom1;  // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      // let Z_FCMC=plan.Custom2; // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      let Y_Actuarial_Factor = this.getinsuranceacturialvalue(plan.County_ID); // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let D_Plan_Option_Adj_Pct = 0; // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let Z_Premium = plan.Premium;  // Mapped Input
      let Z_MPCI_Upper = MPCI_Plan.Upper_Level;

    //CROPHAIL Discount Selection
    let INS_DISC_KEY = "CROPHAIL_" + (plan.Ins_Plan_Type || 0) + '_' + plan.Crop_Code + '_0_' + plan.Irr_Practice_Type_Code + '_' + plan.Irr_Practice_Type_Code;
    let D_Col_Ins_Discount = RKSFind(INS_DISC_KEY, this.discounts);

    //let Col_INS_DISC = Get_Ins_Disc(this.refData, 'CROP HAIL', plan.Ins_Plan_Type, plan.Crop_Code, plan.Crop_Type_Code, plan.Irr_Practice_Type_Code, plan.Crop_Practice_Type_Code );

    let ACRES_AIP_CERT_INS_DISC =  ( this.input.LoanMaster.AIP_Acres_Verified && this.input.LoanMaster.FSA_Acres_Verified ) ? Get_Collateral_Action_Adj_Ins_Disc('ACV', this.refData) : 0;
    let ACRES_MAP_INS_DISC  = this.input.LoanMaster.Acres_Mapped ? Get_Collateral_Action_Adj_Ins_Disc('MAP', this.refData) : 0;

    let TOTAL_Action_Ins_Disc_Adj = ACRES_AIP_CERT_INS_DISC + ACRES_MAP_INS_DISC;
    let Y_Col_Discount_Total = D_Col_Ins_Discount + TOTAL_Action_Ins_Disc_Adj;

      // Y====Computed
      // if (Z_Lower != undefined && Z_Lower <= Z_MPCI_Upper)
      // {
      let Y_band = Math.max(Z_Upper - Z_Lower, 0);
      let Y_Gap = Z_Lower > Z_MPCI_Upper ? true : false;
      let Y_CoveragetoMPCI = Y_Gap ? 0 : Math.max((Z_Upper - Z_MPCI_Upper), 0);
      let Y_OverlapFactor = 1; //Helper.devide(Y_CoveragetoMPCI , Y_band);
      let Y_CovPercent = Y_band / 100 * Y_OverlapFactor * Z_Yield_Pct / 100 * Z_Price_Pct / 100 * Y_Actuarial_Factor * (1 + D_Plan_Option_Adj_Pct / 100);
      let Y_Coverage_Add_Pct = Y_CovPercent;
      let Y_Coverage_Add_Amt = 0;

      if (plan.Ins_Plan_Type == "Basic") {
        Y_Coverage_Add_Pct = 0;
        Y_Coverage_Add_Amt = Y_CovPercent * Z_Liability * (1 - Z_Deduct_perc / 100);
      }

      let Y_Premium_Add = Z_Premium;

      let Y_APH = CropUnitRecord.Ins_APH == 0 ? CropUnitRecord.FC_CropYield : CropUnitRecord.Ins_APH;

      let Y_Max_Deduct_Total = Math.max(Z_Deduct_perc / 100, Helper.divide(Z_Deduct_Units, Y_APH)) + Helper.divide(Z_Late_Deduct_Units, Y_APH);
      Y_Max_Deduct_Total = Math.max(Y_Max_Deduct_Total, 0);

      if (plan.Ins_Plan_Type == "Basic") {
        Y_Max_Deduct_Total = 0;
      }

      // Optimizer Calculations Starts From Here
      let Y_Ins_Shr = CropUnitRecord.FC_Insurance_Share / 100;
      let Y_Ins_Shr_Acres_Sum = Y_Ins_Shr * CropUnitRecord.CU_Acres;
      // let Y_Rate_Sum=Y_Ins_Shr_Acres_Sum * Y_Rate_Yield;
      let Y_APH_Sum = Y_Ins_Shr_Acres_Sum * Y_APH;

      let Y_Exp_Qty_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);

      let Y_Exp_Qty_Sum = Y_Exp_Qty_Pct * Y_APH_Sum;
      let Y_Plan_Revenue_Value = plan.Ins_Plan_Type == 'ARH' ? Y_APH : (Y_APH * Y_Adj_Prc);
      let Y_Net_Coverage_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);
      let Y_Plan_Net_Revenue = (Y_Plan_Revenue_Value * Y_Net_Coverage_Pct) + (Y_Coverage_Add_Amt - Z_Premium);
      let Y_Ins_Value = Math.min(Y_Plan_Net_Revenue, Z_Liability);
      let Y_Ins_Shr_Value = ((Y_Ins_Value * Y_Ins_Shr));
      let Y_Ins_Shr_Sum = Math.round((Y_Ins_Shr_Value * CropUnitRecord.CU_Acres));
      let Y_Disc_Shr_Value = ((Y_Ins_Shr_Value * (1 - Y_Col_Discount_Total / 100)));
      let Y_Disc_Shr_Sum = Math.round((Y_Disc_Shr_Value * CropUnitRecord.CU_Acres));

      CropUnitRecord.FC_CrophailPremium = Z_Premium;
      CropUnitRecord.FC_Crophailvalue_Acre = Y_Ins_Shr_Value;
      CropUnitRecord.FC_Crophailvalue = Y_Ins_Shr_Sum;

      CropUnitRecord.FC_Disc_Crophailvalue_Acre = Y_Disc_Shr_Value;
      CropUnitRecord.FC_Disc_Crophailvalue = Y_Disc_Shr_Sum;

      try {
        this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, Y_Ins_Shr_Sum, Y_Disc_Shr_Sum, Z_Premium, 0, Y_Disc_Shr_Value, Y_Ins_Shr_Value);
      } catch {
        console.error("error");
      }
    } catch (ex) {
      CropUnitRecord.FC_Crophailvalue = -plan.Premium * CropUnitRecord.CU_Acres * (CropUnitRecord.FC_Insurance_Share / 100);
      CropUnitRecord.FC_Crophailvalue_Acre = -plan.Premium;
      this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, 0, 0, plan.Premium, 0, 0, 0);
    }
    return CropUnitRecord;
  }

  private preparePci(input: loan_model, CropUnitRecord: Loan_Crop_Unit, plan: Loan_Policy, MPCI_Plan: Loan_Policy) {

    try {
      plan.Upper_Level = 100;
      plan.Lower_Level = 0;
      plan.Yield_Percent = this.getDiscountbyKey('PCI_YLD_PCT');
      plan.Price_Percent = this.getDiscountbyKey('PCI_PRC_PCT');

      let array = [1, 2, 3, 4, 5];
      let ICC = _.sumBy(input.LoanBudget.filter(p => p.Crop_Practice_ID == CropUnitRecord.Crop_Practice_ID && array.includes(p.Expense_Type_ID)), "Total_Budget_Acre");
      plan.Custom2 = ICC;

      let Z_Unit = plan.Ins_Unit_Type_Code;
      let Z_Options = plan.Option;
      let D_Price = MPCI_Plan.Price; //To be Replaced with lookup Values
      let D_Plan_Option_Adj_Price = 0;
      let Y_Adj_Prc = D_Price + D_Plan_Option_Adj_Price; //To be Replaced with lookup Values from Plan and Option Adjustment
      let Z_Upper = plan.Upper_Level;
      let Z_Lower = plan.Lower_Level;
      let Z_Area_Yield = plan.Area_Yield;
      let Z_Yield_Pct = plan.Yield_Percent;
      let Z_Price_Pct = plan.Price_Percent;
      // let Z_Liability = Z_Area_Yield * Y_Adj_Prc; // Mapped Input
      let Z_Deduct_perc = 0; //  plan.Deductible_Percent; - Overriding Discount Lookup
      let Z_Late_Deduct_Units = plan.Late_Deductible;
      let Z_Deduct_Units = plan.Deductible_Units; //
      let D_ICC = plan.Custom2;  // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      let Z_FCMC = plan.Custom1; // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      let Z_Liability = D_ICC + Number(Z_FCMC); // Mapped Input
      let Y_Actuarial_Factor = this.getinsuranceacturialvalue(plan.County_ID); // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let D_Plan_Option_Adj_Pct = 0; // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let Z_Premium = plan.Premium;  // Mapped Input
      let Z_MPCI_Upper = MPCI_Plan.Upper_Level;

      //PCI Discount Selection
      let INS_DISC_KEY = "PCI_" + (plan.Ins_Plan_Type || 0) + '_' + plan.Crop_Code + '_0_' + plan.Irr_Practice_Type_Code + '_' + plan.Irr_Practice_Type_Code;
      let D_Col_Ins_Discount = RKSFind(INS_DISC_KEY, this.discounts);

      let ACRES_AIP_CERT_INS_DISC =  ( this.input.LoanMaster.AIP_Acres_Verified && this.input.LoanMaster.FSA_Acres_Verified ) ? Get_Collateral_Action_Adj_Ins_Disc('ACV', this.refData) : 0;
      let ACRES_MAP_INS_DISC  = this.input.LoanMaster.Acres_Mapped ? Get_Collateral_Action_Adj_Ins_Disc('MAP', this.refData) : 0;

      let TOTAL_Action_Ins_Disc_Adj = ACRES_AIP_CERT_INS_DISC + ACRES_MAP_INS_DISC;
      let Y_Col_Discount_Total = D_Col_Ins_Discount + TOTAL_Action_Ins_Disc_Adj;

      // Y====Computed
      let Y_band = Math.max(Z_Upper - Z_Lower, 0);
      let Y_Gap = Z_Lower > Z_MPCI_Upper ? true : false;
      let Y_CoveragetoMPCI = Y_Gap ? 0 : Math.max((Z_Upper - Z_MPCI_Upper), 0);
      let Y_OverlapFactor = 1; //Helper.devide(Y_CoveragetoMPCI , Y_band);
      let Y_CovPercent = Y_band / 100 * Y_OverlapFactor * Z_Yield_Pct / 100 * Z_Price_Pct / 100 * Y_Actuarial_Factor * (1 + D_Plan_Option_Adj_Pct / 100);
      let Y_Coverage_Add_Pct = InsCalcHelper.Coverage_Add_Percent(plan, Y_CovPercent);
      let Y_Coverage_Add_Amt = InsCalcHelper.Coverage_Add_Amount(plan, Y_CovPercent, Z_Liability);
      let Y_Premium_Add = Z_Premium;

      let Y_APH = CropUnitRecord.Ins_APH == 0 ? CropUnitRecord.FC_CropYield : CropUnitRecord.Ins_APH;

      let Y_Max_Deduct_Total = Math.max(Z_Deduct_perc / 100, Helper.divide(Z_Deduct_Units, Y_APH)) + Helper.divide(Z_Late_Deduct_Units, Y_APH);
      Y_Max_Deduct_Total = Math.max(Y_Max_Deduct_Total, 0);

      // Optimizer Calculations Starts From Here
      let Y_Ins_Shr = CropUnitRecord.FC_Insurance_Share / 100;
      let Y_Ins_Shr_Acres_Sum = Y_Ins_Shr * CropUnitRecord.CU_Acres;
      // let Y_Rate_Sum=Y_Ins_Shr_Acres_Sum * Y_Rate_Yield;
      let Y_APH_Sum = Y_Ins_Shr_Acres_Sum * Y_APH;

      let Y_Exp_Qty_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);

      let Y_Exp_Qty_Sum = Y_Exp_Qty_Pct * Y_APH_Sum;
      let Y_Plan_Revenue_Value = plan.Ins_Plan_Type == 'ARH' ? Y_APH : (Y_APH * Y_Adj_Prc);
      let Y_Net_Coverage_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);
      let Y_Plan_Net_Revenue = (Y_Plan_Revenue_Value * Y_Net_Coverage_Pct) + (Y_Coverage_Add_Amt - Z_Premium);
      let Y_Ins_Value = Math.min(Y_Plan_Net_Revenue, Z_Liability);
      let Y_Ins_Shr_Value = ((Y_Ins_Value * Y_Ins_Shr));
      let Y_Ins_Shr_Sum = Math.round((Y_Ins_Shr_Value * CropUnitRecord.CU_Acres));
      let Y_Disc_Shr_Value = ((Y_Ins_Shr_Value * (1 - Y_Col_Discount_Total / 100)));
      let Y_Disc_Shr_Sum = Math.round((Y_Disc_Shr_Value * CropUnitRecord.CU_Acres));

      CropUnitRecord.FC_PciPremium = Z_Premium;
      CropUnitRecord.FC_Pcivalue_Acre = Y_Ins_Shr_Value;
      CropUnitRecord.FC_Pcivalue = Y_Ins_Shr_Sum;

      CropUnitRecord.FC_Disc_Pcivalue_Acre = Y_Disc_Shr_Value;
      CropUnitRecord.FC_Disc_Pcivalue = Y_Disc_Shr_Sum;
      try {
        this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, Y_Ins_Shr_Sum, Y_Disc_Shr_Sum, Z_Premium, 0, Y_Disc_Shr_Value, Y_Ins_Shr_Value);
      } catch {
        console.error("error");
      }
    } catch (ex) {

      CropUnitRecord.FC_Pcivalue = 0;
      CropUnitRecord.FC_Pcivalue_Acre = 0;
      this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, 0, 0, plan.Premium, 0, 0, 0);
    }
    return CropUnitRecord;
  }

  private prepareSelect(plan: Loan_Policy, MPCI_Plan: Loan_Policy, CropUnitRecord: Loan_Crop_Unit, input: loan_model) {

    try {
      plan.Yield_Percent = this.getDiscountbyKey('ABC_YLD_PCT');
      plan.Price_Percent = this.getDiscountbyKey('ABC_PRC_PCT');

      plan.Lower_Level = 85;

      // Properties to use
      let Z_Unit = plan.Ins_Unit_Type_Code;
      let Z_Options = plan.Option;
      let D_Price = MPCI_Plan.Price; //To be Replaced with lookup Values
      let D_Plan_Option_Adj_Price = 0;
      let Y_Adj_Prc = D_Price + D_Plan_Option_Adj_Price; //To be Replaced with lookup Values from Plan and Option Adjustment
      let Z_Upper = plan.Upper_Level || 0;
      let Z_Lower = plan.Lower_Level;
      // let Z_Area_Yield=plan.Area_Yield;  //
      let Z_Yield_Pct = plan.Yield_Percent;
      let Z_Price_Pct = plan.Price_Percent;
      let Z_Liability = 99999999; // Mapped Input
      let Z_Deduct_perc = 0; //  plan.Deductible_Percent; - Overriding Discount Lookup
      let Z_Late_Deduct_Units = plan.Late_Deductible;
      let Z_Deduct_Units = plan.Deductible_Units; //
      // let D_ICC=plan.Custom1;  // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      // let Z_FCMC=plan.Custom2; // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      let Y_Actuarial_Factor = this.getinsuranceacturialvalue(plan.County_ID); // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let D_Plan_Option_Adj_Pct = 0; // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let Z_Premium = plan.Premium || 0;  // Mapped Input
      let Z_MPCI_Upper = MPCI_Plan.Upper_Level;

      //ABC Discount Selection
      let INS_DISC_KEY = "ABC_" + (plan.Ins_Plan_Type || 0) + '_' + plan.Crop_Code + '_0_' + plan.Irr_Practice_Type_Code + '_' + plan.Irr_Practice_Type_Code;
      let D_Col_Ins_Discount = RKSFind(INS_DISC_KEY, this.discounts);

      let ACRES_AIP_CERT_INS_DISC =  ( this.input.LoanMaster.AIP_Acres_Verified && this.input.LoanMaster.FSA_Acres_Verified ) ? Get_Collateral_Action_Adj_Ins_Disc('ACV', this.refData) : 0;
      let ACRES_MAP_INS_DISC  = this.input.LoanMaster.Acres_Mapped ? Get_Collateral_Action_Adj_Ins_Disc('MAP', this.refData) : 0;

      let TOTAL_Action_Ins_Disc_Adj = ACRES_AIP_CERT_INS_DISC + ACRES_MAP_INS_DISC;
      let Y_Col_Discount_Total = D_Col_Ins_Discount + TOTAL_Action_Ins_Disc_Adj;

      // Y====Computed
      let Y_band = Math.max(Z_Upper - Z_Lower, 0);
      let Y_Gap = Z_Lower > Z_MPCI_Upper ? true : false;
      let Y_CoveragetoMPCI = Y_Gap ? 0 : Math.max((Z_Upper - Z_MPCI_Upper), 0);
      let Y_OverlapFactor = (Y_band != 0 ? (Y_CoveragetoMPCI / Y_band) : 0);
      let Y_CovPercent = Y_band / 100 * Y_OverlapFactor * Z_Yield_Pct / 100 * Z_Price_Pct / 100 * Y_Actuarial_Factor * (1 + D_Plan_Option_Adj_Pct / 100);
      let Y_Coverage_Add_Pct = Y_CovPercent;
      let Y_Coverage_Add_Amt = 0;
      let Y_Premium_Add = Z_Premium;

      let Y_APH = CropUnitRecord.Ins_APH == 0 ? CropUnitRecord.FC_CropYield : CropUnitRecord.Ins_APH;

      let Y_Max_Deduct_Total = Math.max(Z_Deduct_perc / 100, Helper.divide(Z_Deduct_Units, Y_APH)) + Helper.divide(Z_Late_Deduct_Units, Y_APH);
      Y_Max_Deduct_Total = Math.max(Y_Max_Deduct_Total, 0);

      // Optimizer Calculations Starts From Here
      let Y_Ins_Shr = CropUnitRecord.FC_Insurance_Share / 100;
      let Y_Ins_Shr_Acres_Sum = Y_Ins_Shr * CropUnitRecord.CU_Acres;
      // let Y_Rate_Sum=Y_Ins_Shr_Acres_Sum * Y_Rate_Yield;
      let Y_APH_Sum = Y_Ins_Shr_Acres_Sum * Y_APH;

      let Y_Exp_Qty_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);

      let Y_Exp_Qty_Sum = Y_Exp_Qty_Pct * Y_APH_Sum;
      let Y_Plan_Revenue_Value = plan.Ins_Plan_Type == 'ARH' ? Y_APH : (Y_APH * Y_Adj_Prc);
      let Y_Net_Coverage_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);
      let Y_Plan_Net_Revenue = (Y_Plan_Revenue_Value * Y_Net_Coverage_Pct) + (Y_Coverage_Add_Amt - Z_Premium);
      let Y_Ins_Value = Math.min(Y_Plan_Net_Revenue, Z_Liability);
      let Y_Ins_Shr_Value = ((Y_Ins_Value * Y_Ins_Shr));
      let Y_Ins_Shr_Sum = Math.round((Y_Ins_Shr_Value * CropUnitRecord.CU_Acres));
      let Y_Disc_Shr_Value = ((Y_Ins_Shr_Value * (1 - Y_Col_Discount_Total / 100)));
      let Y_Disc_Shr_Sum = Math.round((Y_Disc_Shr_Value * CropUnitRecord.CU_Acres));

      CropUnitRecord.FC_AbcPremium = Z_Premium;
      CropUnitRecord.FC_Abcvalue_Acre = Y_Ins_Shr_Value;
      CropUnitRecord.FC_Abcvalue = Y_Ins_Shr_Sum;

      CropUnitRecord.FC_Disc_Abcvalue_Acre = Y_Disc_Shr_Value;
      CropUnitRecord.FC_Disc_Abcvalue = Y_Disc_Shr_Sum;
      try {
        this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, Y_Ins_Shr_Sum, Y_Disc_Shr_Sum, Z_Premium, 0, Y_Disc_Shr_Value, Y_Ins_Shr_Value);
      } catch {
        console.error("error");
      }
    } catch (ex) {
      CropUnitRecord.FC_Abcvalue = 0;
      CropUnitRecord.FC_Abcvalue_Acre = 0;
      this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, 0, 0, plan.Premium, 0, 0, 0);
    }
    return CropUnitRecord;
  }

  private prepareIce(plan: Loan_Policy, CropUnitRecord: Loan_Crop_Unit, MPCI_Plan: Loan_Policy, input: loan_model) {

    try {
      //FIRST LETS GET THE RIGHT VALUES ACCORDING TO CONDITION
      switch (plan.Ins_Plan_Type) {
        case "BY":
          plan.Deductible_Units = 0;
          plan.Deductible_Percent = this.getDiscountbyKey('ICE_BY_DP');
          if (CropUnitRecord.Crop_Code.toLowerCase() == "crn") { //hardcoded values for Soy and Corn
            plan.Deductible_Units = this.getDiscountbyKey('ICE_BY_DA_CORN');
          }
          if (CropUnitRecord.Crop_Code.toLowerCase() == "soy") { //hardcoded values for Soy and Corn
            plan.Deductible_Units = this.getDiscountbyKey('ICE_BY_DA_SOYBEANS');
          }
          //condtion
          plan.Upper_Level = this.getDiscountbyKey('ICE_BY_UL');
          plan.Lower_Level = this.getDiscountbyKey('ICE_BY_LL');
          break;
        case "BR":
          // subpolicy.Upper_Limit = 90;
          // subpolicy.Lower_Limit = 80;
          plan.Deductible_Units = 0;
          plan.Deductible_Percent = this.getDiscountbyKey('ICE_BR_DP');
          if (CropUnitRecord.Crop_Code.toLowerCase() == "crn") { //hardcoded values for Soy and Corn
            plan.Deductible_Units = this.getDiscountbyKey('ICE_BR_DA_CORN');
          }
          if (CropUnitRecord.Crop_Code.toLowerCase() == "soy") { //hardcoded values for Soy and Corn
            plan.Deductible_Units = this.getDiscountbyKey('ICE_BR_DA_SOYBEANS');
          }
          //condtion
          plan.Upper_Level = this.getDiscountbyKey('ICE_BR_UL');
          plan.Lower_Level = this.getDiscountbyKey('ICE_BR_LL');
          break;
        case "CY":
          plan.Deductible_Units = 0;
          plan.Deductible_Percent = this.getDiscountbyKey('ICE_CY_DP');
          if (CropUnitRecord.Crop_Code.toLowerCase() == "crn") { //hardcoded values for Soy and Corn
            plan.Deductible_Units = this.getDiscountbyKey('ICE_CY_DA_CORN');
          }
          if (CropUnitRecord.Crop_Code.toLowerCase() == "soy") { //hardcoded values for Soy and Corn
            plan.Deductible_Units = this.getDiscountbyKey('ICE_CY_DA_SOYBEANS');
          }
          //condtion
          plan.Upper_Level = this.getDiscountbyKey('ICE_CY_UL');
          plan.Lower_Level = this.getDiscountbyKey('ICE_CY_LL');
          break;
        case "CR":
          plan.Deductible_Units = 0;
          plan.Deductible_Percent = this.getDiscountbyKey('ICE_CR_DP');
          if (CropUnitRecord.Crop_Code.toLowerCase() == "crn") { //hardcoded values for Soy and Corn
            plan.Deductible_Units = this.getDiscountbyKey('ICE_CR_DA_CORN');
          }
          if (CropUnitRecord.Crop_Code.toLowerCase() == "soy") { //hardcoded values for Soy and Corn
            plan.Deductible_Units = this.getDiscountbyKey('ICE_CR_DA_SOYBEANS');
          }
          //condtion
          plan.Upper_Level = this.getDiscountbyKey('ICE_CR_UL');
          plan.Lower_Level = this.getDiscountbyKey('ICE_CR_LL');
          break;
        default:
          plan.Deductible_Percent = 100;
          plan.Deductible_Units = 0;
          plan.Upper_Level = 0;
          plan.Lower_Level = 0;
          break;
      }
      plan.Price_Percent = this.getDiscountbyKey('ICE_PRC_PCT');

      // Properties to use
      let Z_Unit = plan.Ins_Unit_Type_Code;
      let Z_Options = plan.Option;
      let D_Price = MPCI_Plan.Price; //To be Replaced with lookup Values
      let D_Plan_Option_Adj_Price = 0;
      let Y_Adj_Prc = D_Price + D_Plan_Option_Adj_Price; //To be Replaced with lookup Values from Plan and Option Adjustment
      let Z_Upper = plan.Upper_Level;
      let Z_Lower = plan.Lower_Level;
      // let Z_Area_Yield=plan.Area_Yield;  //
      let Z_Yield_Pct = plan.Yield_Percent || 100;
      let Z_Price_Pct = plan.Price_Percent;
      let Z_Liability = 99999999; // Mapped Input
      let Z_Deduct_perc = 0; //  plan.Deductible_Percent; - Overriding Discount Lookup
      let Z_Late_Deduct_Units = plan.Late_Deductible;
      let Z_Deduct_Units = plan.Deductible_Units; //
      // let D_ICC=plan.Custom1;  // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      // let Z_FCMC=plan.Custom2; // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      let Y_Actuarial_Factor = this.getinsuranceacturialvalue(plan.County_ID); // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let D_Plan_Option_Adj_Pct = 0; // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let Z_Premium = plan.Premium || 0;  // Mapped Input
      let Z_MPCI_Upper = MPCI_Plan.Upper_Level;

      //ICE Discount Selection
      let INS_DISC_KEY = "ICE_" + (plan.Ins_Plan_Type || 0) + '_' + plan.Crop_Code + '_0_' + plan.Irr_Practice_Type_Code + '_' + plan.Irr_Practice_Type_Code;
      let D_Col_Ins_Discount = RKSFind(INS_DISC_KEY, this.discounts);

      let ACRES_AIP_CERT_INS_DISC =  ( this.input.LoanMaster.AIP_Acres_Verified && this.input.LoanMaster.FSA_Acres_Verified ) ? Get_Collateral_Action_Adj_Ins_Disc('ACV', this.refData) : 0;
      let ACRES_MAP_INS_DISC  = this.input.LoanMaster.Acres_Mapped ? Get_Collateral_Action_Adj_Ins_Disc('MAP', this.refData) : 0;

      let TOTAL_Action_Ins_Disc_Adj = ACRES_AIP_CERT_INS_DISC + ACRES_MAP_INS_DISC;
      let Y_Col_Discount_Total = D_Col_Ins_Discount + TOTAL_Action_Ins_Disc_Adj;

      // Y === Computed

      let Y_band = Math.max(Z_Upper - Z_Lower, 0);
      let Y_Gap = Z_Lower > Z_MPCI_Upper ? true : false;
      let Y_CoveragetoMPCI = Y_Gap ? 0 : Math.max((Z_Upper - Z_MPCI_Upper), 0);
      let Y_OverlapFactor = (Y_band != 0 ? (Y_CoveragetoMPCI / Y_band) : 0);
      let Y_CovPercent = Y_band / 100 * Y_OverlapFactor * Z_Yield_Pct / 100 * Z_Price_Pct / 100 * Y_Actuarial_Factor * (1 + D_Plan_Option_Adj_Pct / 100);
      let Y_Coverage_Add_Pct = Y_CovPercent;
      let Y_Coverage_Add_Amt = 0;
      let Y_Premium_Add = Z_Premium;

      let Y_APH = CropUnitRecord.Ins_APH == 0 ? CropUnitRecord.FC_CropYield : CropUnitRecord.Ins_APH;

      let Y_Max_Deduct_Total = Math.max(Z_Deduct_perc / 100, Helper.divide(Z_Deduct_Units, Y_APH)) + Helper.divide(Z_Late_Deduct_Units, Y_APH);
      Y_Max_Deduct_Total = Math.max(Y_Max_Deduct_Total, 0);

      // Optimizer Calculations Starts From Here
      let Y_Ins_Shr = CropUnitRecord.FC_Insurance_Share / 100;
      let Y_Ins_Shr_Acres_Sum = Y_Ins_Shr * CropUnitRecord.CU_Acres;
      // let Y_Rate_Sum=Y_Ins_Shr_Acres_Sum * Y_Rate_Yield;
      let Y_APH_Sum = Y_Ins_Shr_Acres_Sum * Y_APH;

      let Y_Exp_Qty_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);

      let Y_Exp_Qty_Sum = Y_Exp_Qty_Pct * Y_APH_Sum;
      let Y_Plan_Revenue_Value = plan.Ins_Plan_Type == 'ARH' ? Y_APH : (Y_APH * Y_Adj_Prc);
      let Y_Net_Coverage_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);
      let Y_Plan_Net_Revenue = (Y_Plan_Revenue_Value * Y_Net_Coverage_Pct) + (Y_Coverage_Add_Amt - Z_Premium);
      let Y_Ins_Value = Math.min(Y_Plan_Net_Revenue, Z_Liability);
      let Y_Ins_Shr_Value = ((Y_Ins_Value * Y_Ins_Shr));
      let Y_Ins_Shr_Sum = Math.round((Y_Ins_Shr_Value * CropUnitRecord.CU_Acres));
      let Y_Disc_Shr_Value = ((Y_Ins_Shr_Value * (1 - Y_Col_Discount_Total / 100)));
      let Y_Disc_Shr_Sum = Math.round((Y_Disc_Shr_Value * CropUnitRecord.CU_Acres));

      //Feeding Values from here
      CropUnitRecord.FC_IcePremium = Z_Premium;
      CropUnitRecord.FC_Icevalue_Acre = Y_Ins_Shr_Value;
      CropUnitRecord.FC_Icevalue = Y_Ins_Shr_Sum;
      CropUnitRecord.FC_Disc_Icevalue_Acre = Y_Disc_Shr_Value;
      CropUnitRecord.FC_Disc_Icevalue = Y_Disc_Shr_Sum;
      try {
        this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, Y_Ins_Shr_Sum, Y_Disc_Shr_Sum, Z_Premium, 0, Y_Disc_Shr_Value, Y_Ins_Shr_Value);
      } catch {
        console.error("error");
      }
    } catch (ex) {
      CropUnitRecord.FC_Icevalue = 0;
      CropUnitRecord.FC_Icevalue_Acre = 0;
      this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, 0, 0, plan.Premium, 0, 0, 0);
    }
    return CropUnitRecord;
  }

  private prepareRamp(plan: Loan_Policy, MPCI_Plan: Loan_Policy, CropUnitRecord: Loan_Crop_Unit, input: loan_model) {

    try {
      //Yield Percent Set
      plan.Yield_Percent = this.getDiscountbyKey('RAMP_YLD_PCT');

      let Z_Unit = plan.Ins_Unit_Type_Code;
      let Z_Options = plan.Option;
      let D_Price = MPCI_Plan.Price; // To be Replaced with lookup Values
      let D_Plan_Option_Adj_Price = 0; // To be Replaced with lookup Values from Plan and Option Adjustment
      let Y_Adj_Prc = D_Price + D_Plan_Option_Adj_Price;
      let Z_Upper = plan.Upper_Level || 0;
      let Z_Lower = plan.Lower_Level || 0;
      // let Z_Area_Yield = plan.Area_Yield;
      let Z_Yield_Pct = plan.Yield_Percent;
      let Z_Price_Pct = plan.Price_Percent || 100;
      let Z_Liability = plan.Liability_Percent || 150; // Mapped Input
      let Z_Deduct_perc = 0; //  plan.Deductible_Percent; - Overriding Discount Lookup
      let Z_Late_Deduct_Units = plan.Late_Deductible;
      let Z_Deduct_Units = plan.Deductible_Units; //
      // let D_ICC=plan.Custom1;  // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      // let Z_FCMC=plan.Custom2; // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      let Y_Actuarial_Factor = this.getinsuranceacturialvalue(plan.County_ID); // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let D_Plan_Option_Adj_Pct = 0; // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let Z_Premium = plan.Premium || 0;  // Mapped Input
      let Z_MPCI_Upper = MPCI_Plan.Upper_Level;

      //RAMP Discount Selection
      let INS_DISC_KEY = "RAMP_" + (plan.Ins_Plan_Type || 0) + '_' + plan.Crop_Code + '_0_' + plan.Irr_Practice_Type_Code + '_' + plan.Irr_Practice_Type_Code;
      let D_Col_Ins_Discount = RKSFind(INS_DISC_KEY, this.discounts);

      let ACRES_AIP_CERT_INS_DISC =  ( this.input.LoanMaster.AIP_Acres_Verified && this.input.LoanMaster.FSA_Acres_Verified ) ? Get_Collateral_Action_Adj_Ins_Disc('ACV', this.refData) : 0;
      let ACRES_MAP_INS_DISC  = this.input.LoanMaster.Acres_Mapped ? Get_Collateral_Action_Adj_Ins_Disc('MAP', this.refData) : 0;

      let TOTAL_Action_Ins_Disc_Adj = ACRES_AIP_CERT_INS_DISC + ACRES_MAP_INS_DISC;
      let Y_Col_Discount_Total = D_Col_Ins_Discount + TOTAL_Action_Ins_Disc_Adj;

      // Insurance Calculations
      let Y_band = Math.max(Z_Upper - Z_Lower, 0);
      let Y_Gap = Z_Lower > Z_MPCI_Upper ? true : false;
      let Y_CoveragetoMPCI = Y_Gap ? 0 : Math.max((Z_Upper - Z_MPCI_Upper), 0);
      let Y_OverlapFactor = Helper.divide(Y_CoveragetoMPCI, Y_band);
      let Y_CovPercent = Y_band / 100 * Y_OverlapFactor * Z_Yield_Pct / 100 * Z_Price_Pct / 100 * Y_Actuarial_Factor * (1 + D_Plan_Option_Adj_Pct / 100);
      let Y_Coverage_Add_Pct = Y_CovPercent;
      let Y_Coverage_Add_Amt = 0;
      let Y_Premium_Add = Z_Premium;

      let Y_APH = CropUnitRecord.Ins_APH == 0 ? CropUnitRecord.FC_CropYield : CropUnitRecord.Ins_APH;

      let Y_Max_Deduct_Total = Math.max(Z_Deduct_perc / 100, Helper.divide(Z_Deduct_Units, Y_APH)) + Helper.divide(Z_Late_Deduct_Units, Y_APH);
      Y_Max_Deduct_Total = Math.max(Y_Max_Deduct_Total, 0);

      // Optimizer Calculations Starts From Here
      let Y_Ins_Shr = CropUnitRecord.FC_Insurance_Share / 100;
      let Y_Ins_Shr_Acres_Sum = Y_Ins_Shr * CropUnitRecord.CU_Acres;
      // let Y_Rate_Sum=Y_Ins_Shr_Acres_Sum * Y_Rate_Yield;
      let Y_APH_Sum = Y_Ins_Shr_Acres_Sum * Y_APH;

      let Y_Exp_Qty_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);

      let Y_Exp_Qty_Sum = Y_Exp_Qty_Pct * Y_APH_Sum;
      let Y_Plan_Revenue_Value = plan.Ins_Plan_Type == 'ARH' ? Y_APH : (Y_APH * Y_Adj_Prc);
      let Y_Net_Coverage_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);
      let Y_Plan_Net_Revenue = (Y_Plan_Revenue_Value * Y_Net_Coverage_Pct) + (Y_Coverage_Add_Amt - Z_Premium);
      let Y_Ins_Value = Math.min(Y_Plan_Net_Revenue, Z_Liability);
      let Y_Ins_Shr_Value = ((Y_Ins_Value * Y_Ins_Shr));
      let Y_Ins_Shr_Sum = Math.round((Y_Ins_Shr_Value * CropUnitRecord.CU_Acres));
      let Y_Disc_Shr_Value = ((Y_Ins_Shr_Value * (1 - Y_Col_Discount_Total / 100)));
      let Y_Disc_Shr_Sum = Math.round(Y_Disc_Shr_Value * CropUnitRecord.CU_Acres);

      CropUnitRecord.FC_RampPremium = Z_Premium;
      CropUnitRecord.FC_Rampvalue_Acre = Y_Ins_Shr_Value;
      CropUnitRecord.FC_Rampvalue = Y_Ins_Shr_Sum;

      CropUnitRecord.FC_Disc_Rampvalue_Acre = Y_Disc_Shr_Value;
      CropUnitRecord.FC_Disc_Rampvalue = Y_Disc_Shr_Sum;
      this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, Y_Ins_Shr_Sum, Y_Disc_Shr_Sum, Z_Premium, 0, Y_Disc_Shr_Value, Y_Ins_Shr_Value);
    } catch (ex) {
      CropUnitRecord.FC_Rampvalue = 0;
      CropUnitRecord.FC_Rampvalue_Acre = 0;
      this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, 0, 0, 0, 0, 0, 0);
    }

    return CropUnitRecord;
  }

  private prepareStax(plan: Loan_Policy, MPCI_Plan: Loan_Policy, CropUnitRecord: Loan_Crop_Unit, input: loan_model) {

    try {

      if (parseInt(plan.Upper_Level.toString()) == 0 || plan.Upper_Level == null || plan.Upper_Level == undefined) {
        plan.Upper_Level = 0;
      }

      plan.Price_Percent = this.getDiscountbyKey('STAX_PRC_PCT');
      plan.Yield_Percent = this.getDiscountbyKey('STAX_YLD_PCT');

      let Z_Unit = plan.Ins_Unit_Type_Code;
      let Z_Options = plan.Option;
      let D_Price = MPCI_Plan.Price; //To be Replaced with lookup Values
      let D_Plan_Option_Adj_Price = 0;
      let Y_Adj_Prc = D_Price + D_Plan_Option_Adj_Price; //To be Replaced with lookup Values from Plan and Option Adjustment
      let Z_Upper = plan.Upper_Level || 0;
      let Z_Lower = plan.Lower_Level || 0;
      let Z_Area_Yield = plan.Area_Yield || 100;
      let Z_Yield_Pct = plan.Yield_Percent;
      let Z_Price_Pct = plan.Price_Percent;
      let Z_Liability = Z_Area_Yield * (plan.Custom1 || 100) / 100 * Y_Adj_Prc; // Mapped Input
      let Z_Deduct_perc = 0; //  plan.Deductible_Percent; - Overriding Discount Lookup
      let Z_Late_Deduct_Units = plan.Late_Deductible;
      let Z_Deduct_Units = plan.Deductible_Units; //
      // let D_ICC=plan.Custom1;  // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      // let Z_FCMC=plan.Custom2; // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      let Y_Actuarial_Factor = this.getinsuranceacturialvalue(plan.County_ID); // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let D_Plan_Option_Adj_Pct = 0; // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let Z_Premium = plan.Premium || 0;  // Mapped Input
      let Z_MPCI_Upper = MPCI_Plan.Upper_Level;

      //SCO Discount Selection
      let INS_DISC_KEY = "STAX_" + (plan.Ins_Plan_Type || 0) + '_' + plan.Crop_Code + '_0_' + plan.Irr_Practice_Type_Code + '_' + plan.Irr_Practice_Type_Code;
      let D_Col_Ins_Discount = RKSFind(INS_DISC_KEY, this.discounts);

      let ACRES_AIP_CERT_INS_DISC =  ( this.input.LoanMaster.AIP_Acres_Verified && this.input.LoanMaster.FSA_Acres_Verified ) ? Get_Collateral_Action_Adj_Ins_Disc('ACV', this.refData) : 0;
      let ACRES_MAP_INS_DISC  = this.input.LoanMaster.Acres_Mapped ? Get_Collateral_Action_Adj_Ins_Disc('MAP', this.refData) : 0;

      let TOTAL_Action_Ins_Disc_Adj = ACRES_AIP_CERT_INS_DISC + ACRES_MAP_INS_DISC;
      let Y_Col_Discount_Total = D_Col_Ins_Discount + TOTAL_Action_Ins_Disc_Adj;

      // Y ==== Computed
      let Y_band = Math.max(Z_Upper - Z_Lower, 0);
      let Y_Gap = Z_Lower > Z_MPCI_Upper ? true : false;
      let Y_CoveragetoMPCI = Y_Gap ? 0 : Math.max((Z_Upper - Z_MPCI_Upper), 0);
      let Y_OverlapFactor = Helper.divide(Y_CoveragetoMPCI, Y_band);
      let Y_CovPercent = Y_band / 100 * Y_OverlapFactor * Z_Yield_Pct / 100 * Z_Price_Pct / 100 * Y_Actuarial_Factor * (1 + D_Plan_Option_Adj_Pct / 100); // multiplied with 0
      let Y_Coverage_Add_Pct = InsCalcHelper.Coverage_Add_Percent(plan, Y_CovPercent);
      let Y_Coverage_Add_Amt = InsCalcHelper.Coverage_Add_Amount(plan, Y_CovPercent, Z_Liability);
      let Y_Premium_Add = Z_Premium;

      let Y_APH = CropUnitRecord.Ins_APH == 0 ? CropUnitRecord.FC_CropYield : CropUnitRecord.Ins_APH;

      let Y_Max_Deduct_Total = Math.max(Z_Deduct_perc / 100, Helper.divide(Z_Deduct_Units, Y_APH)) + Helper.divide(Z_Late_Deduct_Units, Y_APH);
      Y_Max_Deduct_Total = Math.max(Y_Max_Deduct_Total, 0);

      // Optimizer Calculations Starts From Here
      let Y_Ins_Shr = CropUnitRecord.FC_Insurance_Share / 100;
      let Y_Ins_Shr_Acres_Sum = Y_Ins_Shr * CropUnitRecord.CU_Acres;
      // let Y_Rate_Sum=Y_Ins_Shr_Acres_Sum * Y_Rate_Yield;
      let Y_APH_Sum = Y_Ins_Shr_Acres_Sum * Y_APH;

      let Y_Exp_Qty_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);

      let Y_Exp_Qty_Sum = Y_Exp_Qty_Pct * Y_APH_Sum;
      let Y_Plan_Revenue_Value = plan.Ins_Plan_Type == 'ARH' ? Y_APH : (Y_APH * Y_Adj_Prc);
      let Y_Net_Coverage_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);
      let Y_Plan_Net_Revenue = (Y_Plan_Revenue_Value * Y_Net_Coverage_Pct) + (Y_Coverage_Add_Amt - Z_Premium);
      let Y_Ins_Value = Math.min(Y_Plan_Net_Revenue, Z_Liability);
      let Y_Ins_Shr_Value = ((Y_Ins_Value * Y_Ins_Shr));
      let Y_Ins_Shr_Sum = Math.round((Y_Ins_Shr_Value * CropUnitRecord.CU_Acres));
      let Y_Disc_Shr_Value = ((Y_Ins_Shr_Value * (1 - Y_Col_Discount_Total / 100)));
      let Y_Disc_Shr_Sum = Math.round((Y_Disc_Shr_Value * CropUnitRecord.CU_Acres));

      CropUnitRecord.FC_StaxPremium = Z_Premium;
      CropUnitRecord.FC_Staxvalue_Acre = Y_Ins_Shr_Value;
      CropUnitRecord.FC_Staxvalue = Y_Ins_Shr_Sum;

      CropUnitRecord.FC_Disc_Staxvalue_Acre = Y_Disc_Shr_Value;
      CropUnitRecord.FC_Disc_Staxvalue = Y_Disc_Shr_Sum;
      this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, Y_Ins_Shr_Sum, Y_Disc_Shr_Sum, Z_Premium, 0, Y_Disc_Shr_Value, Y_Ins_Shr_Value);
    } catch (ex) {
      CropUnitRecord.FC_Staxvalue = 0;
      CropUnitRecord.FC_Staxvalue_Acre = 0;
      this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, 0, 0, plan.Premium, 0, 0, 0);
    }
    return CropUnitRecord;
  }

  private prepareSco(plan: Loan_Policy, MPCI_Plan: Loan_Policy, CropUnitRecord: Loan_Crop_Unit, input: loan_model) {

    if (plan.Upper_Level == 0 || plan.Upper_Level == null || plan.Upper_Level == undefined) {
      plan.Upper_Level = 0;
    }

    plan.Price_Percent = this.getDiscountbyKey('SCO_PRC_PCT');
    plan.Yield_Percent = this.getDiscountbyKey('SCO_YLD_PCT');
    plan.Upper_Level = this.getDiscountbyKey('SCO_UL');
    plan.Lower_Level = MPCI_Plan.Upper_Level;

    try {

      let Z_Unit = plan.Ins_Unit_Type_Code;
      let Z_Options = plan.Option;
      let D_Price = MPCI_Plan.Price; //To be Replaced with lookup Values
      let D_Plan_Option_Adj_Price = 0;
      let Y_Adj_Prc = D_Price + D_Plan_Option_Adj_Price; //To be Replaced with lookup Values from Plan and Option Adjustment
      let Z_Upper = plan.Upper_Level;
      let Z_Lower = plan.Lower_Level;
      let Z_Area_Yield = plan.Area_Yield || 0;
      let Z_Yield_Pct = plan.Yield_Percent;
      let Z_Price_Pct = plan.Price_Percent;
      let Z_Liability = Z_Area_Yield * Y_Adj_Prc; // Mapped Input
      let Z_Deduct_perc = 0; //  plan.Deductible_Percent; - Overriding Discount Lookup
      let Z_Late_Deduct_Units = plan.Late_Deductible;
      let Z_Deduct_Units = plan.Deductible_Units; //
      // let D_ICC=plan.Custom1;  // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      // let Z_FCMC=plan.Custom2; // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      let Y_Actuarial_Factor = this.getinsuranceacturialvalue(plan.County_ID); // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let D_Plan_Option_Adj_Pct = 0; // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let Z_Premium = plan.Premium || 0;  // Mapped Input
      let Z_MPCI_Upper = MPCI_Plan.Upper_Level;

      //SCO Discount Selection
      let INS_DISC_KEY = "SCO_" + (plan.Ins_Plan_Type || 0) + '_' + plan.Crop_Code + '_0_' + plan.Irr_Practice_Type_Code + '_' + plan.Irr_Practice_Type_Code;
      let D_Col_Ins_Discount = RKSFind(INS_DISC_KEY, this.discounts);

      let ACRES_AIP_CERT_INS_DISC =  ( this.input.LoanMaster.AIP_Acres_Verified && this.input.LoanMaster.FSA_Acres_Verified ) ? Get_Collateral_Action_Adj_Ins_Disc('ACV', this.refData) : 0;
      let ACRES_MAP_INS_DISC  = this.input.LoanMaster.Acres_Mapped ? Get_Collateral_Action_Adj_Ins_Disc('MAP', this.refData) : 0;

      let TOTAL_Action_Ins_Disc_Adj = ACRES_AIP_CERT_INS_DISC + ACRES_MAP_INS_DISC;
      let Y_Col_Discount_Total = D_Col_Ins_Discount + TOTAL_Action_Ins_Disc_Adj;

      // Y====Computed
      let Y_band = Math.max(Z_Upper - Z_Lower, 0);
      let Y_Gap = Z_Lower > Z_MPCI_Upper ? true : false;
      let Y_CoveragetoMPCI = Y_Gap ? 0 : Math.max((Z_Upper - Z_MPCI_Upper), 0);
      let Y_OverlapFactor = Helper.divide(Y_CoveragetoMPCI, Y_band);
      let Y_CovPercent = Y_band / 100 * Y_OverlapFactor * Z_Yield_Pct / 100 * Z_Price_Pct / 100 * Y_Actuarial_Factor * (1 + D_Plan_Option_Adj_Pct / 100);
      let Y_Coverage_Add_Pct = InsCalcHelper.Coverage_Add_Percent(plan, Y_CovPercent);
      let Y_Coverage_Add_Amt = InsCalcHelper.Coverage_Add_Amount(plan, Y_CovPercent, Z_Liability);
      let Y_Premium_Add = Z_Premium;

      let Y_APH = CropUnitRecord.Ins_APH == 0 ? CropUnitRecord.FC_CropYield : CropUnitRecord.Ins_APH;

      let Y_Max_Deduct_Total = Math.max(Z_Deduct_perc / 100, Helper.divide(Z_Deduct_Units, Y_APH)) + Helper.divide(Z_Late_Deduct_Units, Y_APH);
      Y_Max_Deduct_Total = Math.max(Y_Max_Deduct_Total, 0);

      // Optimizer Calculations Starts From Here
      let Y_Ins_Shr = CropUnitRecord.FC_Insurance_Share / 100;
      let Y_Ins_Shr_Acres_Sum = Y_Ins_Shr * CropUnitRecord.CU_Acres;
      // let Y_Rate_Sum=Y_Ins_Shr_Acres_Sum * Y_Rate_Yield;
      let Y_APH_Sum = Y_Ins_Shr_Acres_Sum * Y_APH;

      let Y_Exp_Qty_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);

      let Y_Exp_Qty_Sum = Y_Exp_Qty_Pct * Y_APH_Sum;
      let Y_Plan_Revenue_Value = plan.Ins_Plan_Type == 'ARH' ? Y_APH : (Y_APH * Y_Adj_Prc);
      let Y_Net_Coverage_Pct = Math.max(Y_Coverage_Add_Pct - Y_Max_Deduct_Total, 0);
      let Y_Plan_Net_Revenue = (Y_Plan_Revenue_Value * Y_Net_Coverage_Pct) + (Y_Coverage_Add_Amt - Z_Premium);
      let Y_Ins_Value = Math.min(Y_Plan_Net_Revenue, Z_Liability);
      let Y_Ins_Shr_Value = ((Y_Ins_Value * Y_Ins_Shr));
      let Y_Ins_Shr_Sum = Math.round((Y_Ins_Shr_Value * CropUnitRecord.CU_Acres));
      let Y_Disc_Shr_Value = (Y_Ins_Shr_Value * (1 - Y_Col_Discount_Total / 100));
      let Y_Disc_Shr_Sum = Math.round((Y_Disc_Shr_Value * CropUnitRecord.CU_Acres));

      CropUnitRecord.FC_ScoPremium = Z_Premium;
      CropUnitRecord.FC_Scovalue_Acre = Y_Ins_Shr_Value;
      CropUnitRecord.FC_Scovalue = Y_Ins_Shr_Sum;

      CropUnitRecord.FC_Disc_Scovalue_Acre = Y_Disc_Shr_Value;
      CropUnitRecord.FC_Disc_Scovalue = Y_Disc_Shr_Sum;
      // }

      try {
        this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, Y_Ins_Shr_Sum, Y_Disc_Shr_Sum, Z_Premium, 0, Y_Disc_Shr_Value, Y_Ins_Shr_Value);
      } catch {
        console.error("error");
      }
    } catch (ex) {
      CropUnitRecord.FC_Scovalue = 0;
      CropUnitRecord.FC_Scovalue_Acre = 0;
      this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, 0, 0, plan.Premium, 0, 0, 0);
    }
    return CropUnitRecord;
  }

  private prepareHmax(plan: Loan_Policy, MPCI_Plan: Loan_Policy, CropUnitRecord: Loan_Crop_Unit, input: loan_model) {
    try {

      //Initial Settings
      plan.Yield_Percent = this.getDiscountbyKey('HMAX_YLD_PCT');

      //Deduct pct Settings
      plan.Deductible_Percent = this.getDiscountbyKey('HMAX_DP');

      //DeddctableUNits Logic
      if (CropUnitRecord.Crop_Practice_Type_Code == "IRR") {
        plan.Deductible_Units = this.getDiscountbyKey('HMAX_DA_IRR');
      } else {
        plan.Deductible_Units = this.getDiscountbyKey('HMAX_DA_NIR');
      }

      // if (plan.Lower_Level != undefined && plan.Lower_Level <= MPCI_Plan.Upper_Level) {
      // Properties to use
      let Z_Unit = plan.Ins_Unit_Type_Code;
      let Z_Options = plan.Option;
      let D_Price = MPCI_Plan.Price; //To be Replaced with lookup Values
      let D_Plan_Option_Adj_Price = 0;
      let Y_Adj_Prc = D_Price + D_Plan_Option_Adj_Price; //To be Replaced with lookup Values from Plan and Option Adjustment
      let Z_Upper = plan.Upper_Level || 0;
      let Z_Lower = plan.Lower_Level || 0;
      // let Z_Area_Yield=plan.Area_Yield;  //
      let Z_Yield_Pct = plan.Yield_Percent;
      let Z_Price_Pct = plan.Price_Percent || 100;
      let Z_Liability = 99999999; // Mapped Input
      let Z_Deduct_perc = this.getDiscountbyKey('HMAX_DP'); //  plan.Deductible_Percent; - Overriding Discount Lookup
      let Z_Late_Deduct_Units = plan.Late_Deductible;
      let Z_Deduct_Units = this.getDiscountbyKey('HMAX_DA'); //
      // let D_ICC=plan.Custom1;  // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      // let Z_FCMC=plan.Custom2; // Lookups of the Crop Table of Loan Budget Table using Crop Practice keys
      let Y_Actuarial_Factor = this.getinsuranceacturialvalue(plan.County_ID); // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let D_Plan_Option_Adj_Pct = 0; // Calculated For lookup Values based on Crop practice and County -- Based on ?
      let Z_Premium = plan.Premium || 0;  // Mapped Input
      let Z_MPCI_Upper = MPCI_Plan.Upper_Level;

      //SCO Discount Selection
      let INS_DISC_KEY = "HMAX_" + (plan.Ins_Plan_Type || 0) + '_' + plan.Crop_Code + '_0_' + plan.Irr_Practice_Type_Code + '_' + plan.Irr_Practice_Type_Code;
      let D_Col_Ins_Discount = RKSFind(INS_DISC_KEY, this.discounts);

      let ACRES_AIP_CERT_INS_DISC =  ( this.input.LoanMaster.AIP_Acres_Verified && this.input.LoanMaster.FSA_Acres_Verified ) ? Get_Collateral_Action_Adj_Ins_Disc('ACV', this.refData) : 0;
      let ACRES_MAP_INS_DISC  = this.input.LoanMaster.Acres_Mapped ? Get_Collateral_Action_Adj_Ins_Disc('MAP', this.refData) : 0;

      let TOTAL_Action_Ins_Disc_Adj = ACRES_AIP_CERT_INS_DISC + ACRES_MAP_INS_DISC;
      let Y_Col_Discount_Total = D_Col_Ins_Discount + TOTAL_Action_Ins_Disc_Adj;

      let Y_band = Math.max(Z_Upper - Z_Lower, 0);
      let Y_Gap = Z_Lower > Z_MPCI_Upper ? true : false;
      let Y_CoveragetoMPCI = Y_Gap ? 0 : Math.max((Z_Upper - Z_MPCI_Upper), 0);
      let Y_OverlapFactor = (Y_band != 0 ? (Y_CoveragetoMPCI / Y_band) : 0);
      let Y_CovPercent = Y_band / 100 * Y_OverlapFactor * Z_Yield_Pct / 100 * Z_Price_Pct / 100 * Y_Actuarial_Factor * (1 + D_Plan_Option_Adj_Pct / 100);
      let Y_Coverage_Add_Pct = Y_CovPercent;
      let Y_Coverage_Add_Amt = 0;
      let Y_Premium_Add = Z_Premium;

      let Y_APH = CropUnitRecord.Ins_APH == 0 ? CropUnitRecord.FC_CropYield : CropUnitRecord.Ins_APH;

      let Y_Max_Deduct_Total = (Math.max(Z_Deduct_perc / 100, Helper.divide(Z_Deduct_Units, Y_APH)) + Helper.divide(Z_Late_Deduct_Units, Y_APH));
      Y_Max_Deduct_Total = Math.max(Y_Max_Deduct_Total, 0);

      // Optimizer Calculations Starts From Here
      let Y_Ins_Shr = CropUnitRecord.FC_Insurance_Share / 100;
      let Y_Ins_Shr_Acres_Sum = Y_Ins_Shr * CropUnitRecord.CU_Acres;
      // let Y_Rate_Sum=Y_Ins_Shr_Acres_Sum * Y_Rate_Yield;
      let Y_APH_Sum = Y_Ins_Shr_Acres_Sum * Y_APH;

      let Y_Exp_Qty_Pct = ((Y_CovPercent - Y_Max_Deduct_Total));
      Y_Exp_Qty_Pct = (((Y_Exp_Qty_Pct > 0 ? Y_Exp_Qty_Pct : 0)));

      let Y_Exp_Qty_Sum = Y_Exp_Qty_Pct * Y_APH_Sum;
      let Y_Plan_Revenue_Value = plan.Ins_Plan_Type == 'ARH' ? Y_APH : (Y_APH * Y_Adj_Prc);
      let Y_Net_Coverage_Pct = Y_Coverage_Add_Pct - Y_Max_Deduct_Total;
       Y_Net_Coverage_Pct = Y_Net_Coverage_Pct > 0 ? Y_Net_Coverage_Pct : 0;
      let Y_Plan_Net_Revenue = (Y_Plan_Revenue_Value * Y_Net_Coverage_Pct) + (Y_Coverage_Add_Amt - Z_Premium);
      let Y_Ins_Value = Math.min(Y_Plan_Net_Revenue, Z_Liability);
      let Y_Ins_Shr_Value = ((Y_Ins_Value * Y_Ins_Shr));
      let Y_Ins_Shr_Sum = Math.round((Y_Ins_Shr_Value * CropUnitRecord.CU_Acres));
      let Y_Disc_Shr_Value = ((Y_Ins_Shr_Value * (1 - Y_Col_Discount_Total / 100)));
      let Y_Disc_Shr_Sum = Math.round((Y_Disc_Shr_Value * CropUnitRecord.CU_Acres));

      //Feeding Values from here
      CropUnitRecord.FC_HmaxPremium = Z_Premium;
      CropUnitRecord.FC_Hmaxvalue_Acre = Y_Ins_Shr_Value;
      CropUnitRecord.FC_Hmaxvalue = Y_Ins_Shr_Sum;

      CropUnitRecord.FC_Disc_Hmaxvalue_Acre = Y_Disc_Shr_Value;
      CropUnitRecord.FC_Disc_Hmaxvalue = Y_Disc_Shr_Sum;
      this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, Y_Ins_Shr_Sum, Y_Disc_Shr_Sum, Z_Premium, 0, Y_Disc_Shr_Value, Y_Ins_Shr_Value);
    } catch (ex) {
      CropUnitRecord.FC_Hmaxvalue = 0;
      CropUnitRecord.FC_Hmaxvalue_Acre = 0;
      this.givevaluestocropunitpolicy(plan, CropUnitRecord.Loan_CU_ID, this.input, 0, 0, plan.Premium, 0, 0, 0);
    }

    return CropUnitRecord;
  }

  fillFCValuesforCropunits(loanObj: loan_model) {
    //This Section has been added for AIP Verified
    try {
      // loanObj = this.prepape_Insurance_Price(loanObj);
      loanObj.LoanMaster.AIP_Verified = !(loanObj.LoanCropUnits.filter(p => p.Verf_Ins_APH_Status != true).length > 0);
    } catch {
       //No Cropunits or the field has been Eradicated
    }

    try {

      if (!this.refData) {
        this.refData = this.localstorage.retrieve(environment.referencedatakey);
      }

      let starttime = new Date().getTime();
      loanObj.LoanCropUnits.forEach(cropUnit => {
        cropUnit.Mkt_Value = 0;
        cropUnit.Mkt_Value_Acre = 0;

        cropUnit.Disc_Mkt_Value = 0;
        cropUnit.Disc_Mkt_Value_Acre = 0;

        let farm = loanObj.Farms.find(p => p.Farm_ID == cropUnit.Farm_ID);
        if (farm != undefined && farm != null) {
          cropUnit.FC_CountyID = farm.Farm_County_ID;
          cropUnit.FC_StateID = farm.Farm_State_ID;
          cropUnit.FC_FSN = farm.FSN;
          cropUnit.FC_Rating = farm.Rated;
          cropUnit.FC_Section = farm.Section;
          cropUnit.FC_Landlord = farm.Landowner;
          // cropUnit.FC_ProdPerc = farm.Percent_Prod;
        }

        if (farm.Permission_To_Insure == 1) {
          cropUnit.FC_Insurance_Share = 100;
        } else {
          cropUnit.FC_Insurance_Share = cropUnit.FC_ProdPerc || farm.Percent_Prod || 100;
        }

        let _cropyield = loanObj.CropYield.find(p => p.Crop_ID == cropUnit.Crop_Practice_ID);
        cropUnit.FC_CropYield = _cropyield ? _cropyield.CropYield : 0;

        let crop = loanObj.LoanCrops.find(p => p.Crop_Code == cropUnit.Crop_Code && p.Crop_Type_Code == cropUnit.Crop_Type_Code);
        if (!crop) {
          crop = loanObj.LoanCrops.find(p => p.Crop_Code == cropUnit.Crop_Code);
        }

        cropUnit.Z_Crop_Adj_Price = parseFloat(crop.Adj_Price.toFixed(4));

        cropUnit.Mkt_Value_Acre = cropUnit.FC_CropYield * cropUnit.Z_Crop_Adj_Price * farm.Percent_Prod / 100;
        cropUnit.Mkt_Value = Math.round(cropUnit.Mkt_Value_Acre * cropUnit.CU_Acres);

        let cropDiscount = Get_Collateral_Mkt_Disc('CRP', this.refData);
        let ACRES_AIP_CERT_INS_DISC = loanObj.LoanMaster.AIP_Verified ? Get_Collateral_Action_Adj_Ins_Disc('ACV', this.refData) : 0;
        let ACRES_MAP_INS_DISC  = loanObj.LoanMaster.Acres_Mapped ? Get_Collateral_Action_Adj_Ins_Disc('MAP', this.refData) : 0;

        let D_Col_Discount = cropDiscount + ACRES_AIP_CERT_INS_DISC + ACRES_MAP_INS_DISC;

        cropUnit.Disc_Mkt_Value_Acre = cropUnit.Mkt_Value_Acre * (1 - D_Col_Discount / 100);
        cropUnit.Disc_Mkt_Value = Math.round(cropUnit.Disc_Mkt_Value_Acre * cropUnit.CU_Acres);

        let insurancepolicies = loanObj.LoanPolicies.filter(p => p.Policy_Key.includes(cropUnit.Crop_Unit_Key));
        let mainpolicies = insurancepolicies.filter(p => p.Ins_Plan == 'MPCI' && p.ActionStatus != 3); //MPCI ONES

        let MPCIPolicy: Loan_Policy;
        if (mainpolicies.find(p => p.HR_Exclusion_Ind == true && p.Select_Ind == true) != undefined) {
          MPCIPolicy = mainpolicies.find(p => p.HR_Exclusion_Ind == true && p.Select_Ind == true);
        } else {
          MPCIPolicy = mainpolicies.find(p => p.HR_Exclusion_Ind != true && p.Select_Ind == true);
        }
        //Lets initialize the properties
        //----------------------
        cropUnit.FC_Icevalue = 0;
        cropUnit.FC_Icevalue_Acre = 0;

        cropUnit.FC_MPCIvalue = 0;
        cropUnit.FC_MPCIvalue_Acre = 0;

        cropUnit.FC_Staxvalue = 0;
        cropUnit.FC_Staxvalue_Acre = 0;

        cropUnit.FC_Scovalue = 0;
        cropUnit.FC_Scovalue_Acre = 0;

        cropUnit.FC_Hmaxvalue = 0;
        cropUnit.FC_Hmaxvalue_Acre = 0;

        cropUnit.FC_Abcvalue = 0;
        cropUnit.FC_Abcvalue_Acre = 0;

        cropUnit.FC_Pcivalue = 0;
        cropUnit.FC_Pcivalue_Acre = 0;

        cropUnit.FC_Rampvalue = 0;
        cropUnit.FC_Rampvalue_Acre = 0;

        cropUnit.FC_Crophailvalue = 0;
        cropUnit.FC_Crophailvalue_Acre = 0;

        if (MPCIPolicy != null || MPCIPolicy != undefined) {
          if (MPCIPolicy.Price == 0 || MPCIPolicy.Price == undefined) {
            MPCIPolicy.Price = this.refData.CropList.find(p => p.Crop_Code == cropUnit.Crop_Code && p.Irr_Prac_Code == cropUnit.Crop_Practice_Type_Code).Price;
          }
            this.prepareMPCIValues(MPCIPolicy, cropUnit, loanObj, farm);
          }

        try {

          // per acre calculation - added by ashwani
          cropUnit.Ins_Value_Acre = (cropUnit.Ins_Value_Acre || 0) + (cropUnit.FC_Icevalue_Acre || 0) + (cropUnit.FC_Hmaxvalue_Acre || 0) + (cropUnit.FC_Crophailvalue_Acre || 0) + (cropUnit.FC_Scovalue_Acre || 0) + (cropUnit.FC_Staxvalue_Acre || 0) + (cropUnit.FC_Abcvalue_Acre || 0) + (cropUnit.FC_Rampvalue_Acre || 0) + (cropUnit.FC_Pcivalue_Acre || 0);
          // cropUnit.Ins_Value = Math.max(0, cropUnit.Ins_Value);

          cropUnit.Disc_Ins_value_Acre = (cropUnit.Disc_Ins_value_Acre || 0) + (cropUnit.FC_Disc_Icevalue_Acre || 0) + (cropUnit.FC_Disc_Hmaxvalue_Acre || 0) + (cropUnit.FC_Disc_Crophailvalue_Acre || 0) + (cropUnit.FC_Disc_Scovalue_Acre || 0) + (cropUnit.FC_Disc_Staxvalue_Acre + cropUnit.FC_Disc_Abcvalue_Acre || 0) + (cropUnit.FC_Disc_Rampvalue_Acre || 0) + (cropUnit.FC_Disc_Pcivalue_Acre || 0);
          // cropUnit.Disc_Ins_value = Math.max(cropUnit.Disc_Ins_value_Acre, 0);

          cropUnit.Ins_Value = cropUnit.Ins_Value + cropUnit.FC_Icevalue + cropUnit.FC_Hmaxvalue + cropUnit.FC_Crophailvalue + cropUnit.FC_Scovalue + cropUnit.FC_Staxvalue + cropUnit.FC_Abcvalue + cropUnit.FC_Rampvalue + cropUnit.FC_Pcivalue;
          // cropUnit.Ins_Value = cropUnit.Ins_Value < 0 ? 0 : cropUnit.Ins_Value;

          cropUnit.Disc_Ins_value = (cropUnit.Disc_Ins_value || 0) + (cropUnit.FC_Disc_Icevalue || 0) + (cropUnit.FC_Disc_Hmaxvalue || 0) + (cropUnit.FC_Disc_Crophailvalue || 0) + (cropUnit.FC_Disc_Scovalue || 0) + (cropUnit.FC_Disc_Staxvalue + cropUnit.FC_Disc_Abcvalue || 0) + (cropUnit.FC_Disc_Rampvalue || 0) + (cropUnit.FC_Disc_Pcivalue || 0);
          // cropUnit.Disc_Ins_value = cropUnit.Disc_Ins_value < 0 ? 0 : cropUnit.Disc_Ins_value;


        } catch (ex) {
          cropUnit.Ins_Value = 0;
          cropUnit.Disc_Ins_value = 0;
          console.error("Error in Cropunit Calculations fro ins value");
        }

        //CEI Value
        try {
  /// CEI is Discounted CEI
          cropUnit.CEI_Value = Math.round(cropUnit.Disc_Mkt_Value - cropUnit.Disc_Ins_value);

          if (cropUnit.CEI_Value < 0) {
            cropUnit.CEI_Value = 0;
          }

          cropUnit.CEI_Value_Acre = Math.round(cropUnit.Disc_Mkt_Value_Acre - cropUnit.Disc_Ins_value_Acre);
          if (cropUnit.CEI_Value_Acre < 0) {
            cropUnit.CEI_Value_Acre = 0;
          }

          cropUnit.Disc_CEI_Value = Math.round(cropUnit.Disc_Mkt_Value - cropUnit.Disc_Ins_value);
          if (cropUnit.Disc_CEI_Value < 0) {
            cropUnit.Disc_CEI_Value = 0;
          }

          cropUnit.Disc_CEI_Value_Acre = Math.round(cropUnit.Disc_Mkt_Value_Acre - cropUnit.Disc_Ins_value_Acre);
          if (cropUnit.Disc_CEI_Value_Acre < 0) {
            cropUnit.Disc_CEI_Value_Acre = 0;
          }
        } catch (ex) {
          cropUnit.CEI_Value = 0;
          cropUnit.Disc_CEI_Value = 0;

          cropUnit.CEI_Value_Acre = 0;
          cropUnit.Disc_CEI_Value_Acre = 0;
          console.error("Error in Cropunit Calculations fro CEI_Value");
        }


      });


      //Loan Master
      if (loanObj.LoanMaster && loanObj.LoanMaster) {
        loanObj.LoanMaster.FC_Net_Market_Value_Insurance = precise_round(_.sumBy(loanObj.LoanCropUnits.filter(p => !isNaN(p.Ins_Value)), "Ins_Value"), 0);
        loanObj.LoanMaster.FC_Disc_value_Insurance = _.sumBy(loanObj.LoanCropUnits.filter(p => !isNaN(p.Ins_Value)), "Disc_Ins_value");
        loanObj.LoanMaster.CEI_Value = precise_round(_.sumBy(loanObj.LoanCropUnits.filter(p => !isNaN(p.Ins_Value)), "Disc_CEI_Value"), 0);

        loanObj.LoanMaster.Disc_CEI_Value_Acre = _.sumBy(loanObj.LoanCropUnits.filter(p => !isNaN(p.Ins_Value)), "Disc_CEI_Value_Acre");
      }

      loanObj.LoanCropPractices.forEach(cropPractice => {

        cropPractice.LCP_Acres = 0;

        let itemsincropunit = loanObj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropPractice.Crop_Practice_ID);

        itemsincropunit.forEach(crp => {
          let ppercent_Prod = loanObj.Farms.find(p => p.Farm_ID == crp.Farm_ID).Percent_Prod;
          // element.LCP_Acres=element.LCP_Acres + (crp.CU_Acres * ppercent_Prod/100);
          cropPractice.LCP_Acres = cropPractice.LCP_Acres + (crp.CU_Acres); //TODO: Removed ppercent_Prod, will have to add it somewhere else for collateral calculation
        });

        // element.FC_Agg_Mkt_Value=_.sumBy(input.LoanCropUnits,"Ins_Value");
        // element.FC_Agg_Disc_Mkt_Value=
        // element.FC_Agg_Disc_Ins_Value=
        // element.FC_Agg_Ins_Value=
        // element.FC_Agg_Disc_Cei_Value=

        cropPractice.FC_LCP_Ins_Value_Mpci_Acre = (_.sumBy(itemsincropunit, i => i.FC_MPCIvalue_Acre || 0) || 0);
        cropPractice.FC_LCP_Ins_Value_Mpci = (_.sumBy(itemsincropunit, "FC_MPCIvalue") || 0);

        cropPractice.FC_LCP_Disc_Ins_Value_Mpci_Acre = (_.sumBy(itemsincropunit, i => i.FC_Disc_MPCI_value_Acre || 0) || 0);
        cropPractice.FC_LCP_Disc_Ins_Value_Mpci = (_.sumBy(itemsincropunit, "FC_Disc_MPCI_value") || 0);

        cropPractice.FC_LCP_Ins_Value_Hmax_Acre = _.sumBy(itemsincropunit, i => i.FC_Hmaxvalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Ins_Value_Hmax = _.sumBy(itemsincropunit, "FC_Hmaxvalue") || 0;

        cropPractice.FC_LCP_Disc_Ins_Value_Hmax_Acre = _.sumBy(itemsincropunit, i => i.FC_Disc_Hmaxvalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Disc_Ins_Value_Hmax = _.sumBy(itemsincropunit, "FC_Disc_Hmaxvalue") || 0;

        cropPractice.FC_LCP_Ins_Value_Ramp_Acre = _.sumBy(itemsincropunit, i => i.FC_Rampvalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Ins_Value_Ramp = _.sumBy(itemsincropunit, "FC_Rampvalue") || 0;

        cropPractice.FC_LCP_Disc_Ins_Value_Ramp_Acre = (_.sumBy(itemsincropunit, i => i.FC_Disc_Rampvalue_Acre || 0)) || 0;
        cropPractice.FC_LCP_Disc_Ins_Value_Ramp = (_.sumBy(itemsincropunit, "FC_Disc_Rampvalue")) || 0;

        cropPractice.FC_LCP_Ins_Value_Ice_Acre = _.sumBy(itemsincropunit, i => i.FC_Icevalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Ins_Value_Ice = _.sumBy(itemsincropunit, "FC_Icevalue") || 0;

        cropPractice.FC_LCP_Disc_Ins_Value_Ice_Acre = _.sumBy(itemsincropunit, i => i.FC_Disc_Icevalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Disc_Ins_Value_Ice = _.sumBy(itemsincropunit, "FC_Disc_Icevalue") || 0;

        cropPractice.FC_LCP_Ins_Value_Abc_Acre = _.sumBy(itemsincropunit, i => i.FC_Abcvalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Ins_Value_Abc = _.sumBy(itemsincropunit, "FC_Abcvalue") || 0;

        cropPractice.FC_LCP_Disc_Ins_Value_Abc_Acre = _.sumBy(itemsincropunit, i => i.FC_Disc_Abcvalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Disc_Ins_Value_Abc = _.sumBy(itemsincropunit, "FC_Disc_Abcvalue") || 0;

        cropPractice.FC_LCP_Ins_Value_Pci_Acre = _.sumBy(itemsincropunit, i => i.FC_Pcivalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Ins_Value_Pci = _.sumBy(itemsincropunit, "FC_Pcivalue") || 0;

        cropPractice.FC_LCP_Disc_Ins_Value_Pci_Acre = _.sumBy(itemsincropunit, i => i.FC_Disc_Pcivalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Disc_Ins_Value_Pci = _.sumBy(itemsincropunit, "FC_Disc_Pcivalue") || 0;

        cropPractice.FC_LCP_Ins_Value_Crophail_Acre = _.sumBy(itemsincropunit, i => i.FC_Crophailvalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Ins_Value_Crophail = _.sumBy(itemsincropunit, "FC_Crophailvalue") || 0;

        cropPractice.FC_LCP_Disc_Ins_Value_Crophail_Acre = _.sumBy(itemsincropunit, i => i.FC_Disc_Crophailvalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Disc_Ins_Value_Crophail = _.sumBy(itemsincropunit, "FC_Disc_Crophailvalue") || 0;

        cropPractice.FC_LCP_Ins_Value_Stax_Acre = _.sumBy(itemsincropunit, i => i.FC_Staxvalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Ins_Value_Stax = _.sumBy(itemsincropunit, "FC_Staxvalue") || 0;

        cropPractice.FC_LCP_Disc_Ins_Value_Stax_Acre = _.sumBy(itemsincropunit, i => i.FC_Disc_Staxvalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Disc_Ins_Value_Stax = _.sumBy(itemsincropunit, "FC_Disc_Staxvalue") || 0;

        cropPractice.FC_LCP_Ins_Value_Sco_Acre = _.sumBy(itemsincropunit, i => i.FC_Scovalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Ins_Value_Sco = _.sumBy(itemsincropunit, "FC_Scovalue") || 0;

        cropPractice.FC_LCP_Disc_Ins_Value_Sco_Acre = _.sumBy(itemsincropunit, i => i.FC_Disc_Scovalue_Acre || 0) || 0;
        cropPractice.FC_LCP_Disc_Ins_Value_Sco = _.sumBy(itemsincropunit, "FC_Disc_Scovalue") || 0;

        cropPractice.FC_LCP_Ins_Value_Acre = cropPractice.FC_LCP_Ins_Value_Mpci_Acre + cropPractice.FC_LCP_Ins_Value_Ice_Acre + cropPractice.FC_LCP_Ins_Value_Hmax_Acre + cropPractice.FC_LCP_Ins_Value_Crophail_Acre + cropPractice.FC_LCP_Ins_Value_Sco_Acre + cropPractice.FC_LCP_Ins_Value_Stax_Acre + cropPractice.FC_LCP_Ins_Value_Abc_Acre + cropPractice.FC_LCP_Ins_Value_Ramp_Acre + cropPractice.FC_LCP_Ins_Value_Pci_Acre;
        cropPractice.FC_LCP_Ins_Value = cropPractice.FC_LCP_Ins_Value_Mpci + cropPractice.FC_LCP_Ins_Value_Ice + cropPractice.FC_LCP_Ins_Value_Hmax + cropPractice.FC_LCP_Ins_Value_Crophail + cropPractice.FC_LCP_Ins_Value_Sco + cropPractice.FC_LCP_Ins_Value_Stax + cropPractice.FC_LCP_Ins_Value_Abc + cropPractice.FC_LCP_Ins_Value_Ramp + cropPractice.FC_LCP_Ins_Value_Pci;

        cropPractice.FC_LCP_Disc_Ins_Value_Acre = cropPractice.FC_LCP_Disc_Ins_Value_Mpci_Acre + cropPractice.FC_LCP_Disc_Ins_Value_Ice_Acre + cropPractice.FC_LCP_Disc_Ins_Value_Hmax_Acre + cropPractice.FC_LCP_Disc_Ins_Value_Crophail_Acre + cropPractice.FC_LCP_Disc_Ins_Value_Sco_Acre + cropPractice.FC_LCP_Disc_Ins_Value_Stax_Acre + cropPractice.FC_LCP_Disc_Ins_Value_Abc_Acre + cropPractice.FC_LCP_Disc_Ins_Value_Ramp_Acre + cropPractice.FC_LCP_Disc_Ins_Value_Pci_Acre;
        cropPractice.FC_LCP_Disc_Ins_Value = cropPractice.FC_LCP_Disc_Ins_Value_Mpci + cropPractice.FC_LCP_Disc_Ins_Value_Ice + cropPractice.FC_LCP_Disc_Ins_Value_Hmax + cropPractice.FC_LCP_Disc_Ins_Value_Crophail + cropPractice.FC_LCP_Disc_Ins_Value_Sco + cropPractice.FC_LCP_Disc_Ins_Value_Stax + cropPractice.FC_LCP_Disc_Ins_Value_Abc + cropPractice.FC_LCP_Disc_Ins_Value_Ramp + cropPractice.FC_LCP_Disc_Ins_Value_Pci;

        cropPractice.FC_Market_Value_Acre = _.sumBy(itemsincropunit, i => i.Mkt_Value_Acre || 0) || 0;
        cropPractice.FC_Disc_Market_Value_Acre = _.sumBy(itemsincropunit, i => i.Disc_Mkt_Value_Acre || 0) || 0;
        //Not need to set these two variables as the values are already stored in Market_Value & Disc_Market_Value in crop practice
        // element.FC_Agg_Mkt_Value=_.sumBy(input.LoanCropUnits,"Ins_Value");
        // element.FC_Agg_Disc_Mkt_Value=
      });


      let endtime = new Date().getTime();
      //level 2 log
      this.logging.checkandcreatelog(2, 'Calc_CropUnit_FC', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
      return loanObj;
    } catch (e) {
      //level 1 log as it calculation
      this.logging.checkandcreatelog(1, 'Calc_CropUnit_FC', e);
      return loanObj;
    }

  }

  givevaluestocropunitpolicy(policy: Loan_Policy, CUid: number, input: loan_model, insvalue: number, discinsvalue: number, premium: number, Qty: number, discinsacre: number, insacre: number) {
    if (input.CropUnitPolicies == undefined) {
      input.CropUnitPolicies = new Array<Crop_Unit_Policy>();
    }
    let investigatedObject = input.CropUnitPolicies.find(p => p.Loan_Crop_Unit_ID == CUid && p.Ins_Plan_Code == policy.Ins_Plan);
    if (investigatedObject !== undefined) {

      input.CropUnitPolicies.find(p => p.Loan_Crop_Unit_ID == CUid && p.Ins_Plan_Code == policy.Ins_Plan).Ins_Value = insvalue;
      input.CropUnitPolicies.find(p => p.Loan_Crop_Unit_ID == CUid && p.Ins_Plan_Code == policy.Ins_Plan).Disc_Ins_Value = discinsvalue;
      input.CropUnitPolicies.find(p => p.Loan_Crop_Unit_ID == CUid && p.Ins_Plan_Code == policy.Ins_Plan).Ins_Value_Acre = insacre;
      input.CropUnitPolicies.find(p => p.Loan_Crop_Unit_ID == CUid && p.Ins_Plan_Code == policy.Ins_Plan).Disc_Ins_Value_Acre = discinsacre;
      input.CropUnitPolicies.find(p => p.Loan_Crop_Unit_ID == CUid && p.Ins_Plan_Code == policy.Ins_Plan).Premium = premium;
      input.CropUnitPolicies.find(p => p.Loan_Crop_Unit_ID == CUid && p.Ins_Plan_Code == policy.Ins_Plan).Ins_Qty = Qty;
      input.CropUnitPolicies.find(p => p.Loan_Crop_Unit_ID == CUid && p.Ins_Plan_Code == policy.Ins_Plan).Ins_Plan_Type_Code = policy.Ins_Plan_Type;
      input.CropUnitPolicies.find(p => p.Loan_Crop_Unit_ID == CUid && p.Ins_Plan_Code == policy.Ins_Plan).Status = 2;
    } else {
      let insrecord = new Crop_Unit_Policy();
      insrecord.Loan_Crop_Unit_ID = CUid;
      insrecord.Loan_Full_ID = input.Loan_Full_ID;
      insrecord.Loan_Plan_Detail_ID = 0;
      insrecord.Loan_Policy_ID = policy.Loan_Policy_ID;
      insrecord.Premium = premium;
      insrecord.Status = 1;
      insrecord.Disc_Ins_Value = discinsvalue;
      insrecord.Ins_Value = insvalue;
      insrecord.Disc_Ins_Value_Acre = discinsacre;
      insrecord.Ins_Value_Acre = insacre;
      insrecord.Ins_Qty = Qty;
      insrecord.Ins_Plan_Type_Code = policy.Ins_Plan_Type;
      insrecord.Ins_Plan_Code = policy.Ins_Plan;
      input.CropUnitPolicies.push(insrecord);
    }
  }

  getinsuranceacturialvalue(countyid) {
    // tslint:disable-next-line:no-unused-expression
    let item = this.refData.Ref_Ins_Plan_Actuarials.find(p => p.County_ID == countyid);
    // tslint:disable-next-line: no-non-null-assertion
    if (item! = null) {
      return item.Hail_Actuarial_Percent;
    } else {
      return 0;
    }
  }

  preparePrimaryAIPandAgency(input: loan_model) {
    try {
      if(input) {
        let AddedAIPs = input.Association.filter(a => a.Assoc_Type_Code == AssociationTypeCode.AIP && a.ActionStatus != 3);
        if (AddedAIPs.length > 0) {
          if (AddedAIPs.length == 1) {
            input.LoanMaster.Primary_AIP_ID = AddedAIPs[0].Assoc_ID;
            input.LoanMaster.Primary_AIP_Name = AddedAIPs[0].Assoc_Name;
          } else {
            let policyGroups = _.groupBy(input.LoanPolicies.filter(a => a.ActionStatus != 3 && a.Select_Ind), a => a.AIP_ID);
            let AIPAndInsValueGroup = this.getmaxinsvalue(input, policyGroups);
            let max_ins_value = _.maxBy(AIPAndInsValueGroup, a => a.value);

            if(max_ins_value) {
              let assoc = input.Association.find(a => a.Assoc_ID == max_ins_value.id);
              if(assoc) {
                input.LoanMaster.Primary_AIP_ID = assoc.Assoc_ID;
                input.LoanMaster.Primary_AIP_Name = assoc.Assoc_Name;
              }
            }
          }
        }

        let AddedAgencis = input.Association.filter(a => a.Assoc_Type_Code == AssociationTypeCode.Agency && a.ActionStatus != 3);
        if(AddedAgencis.length > 0) {
          if(AddedAgencis.length == 1) {
            input.LoanMaster.Primary_Agency_ID = AddedAgencis[0].Assoc_ID;
            input.LoanMaster.Primary_Agency_Name = AddedAgencis[0].Assoc_Name;
          } else {
            let policyGroups = _.groupBy(input.LoanPolicies.filter(a => a.ActionStatus != 3 && a.Select_Ind), a => a.Agency_ID);
            let AgencyAndInsValueGroups = this.getmaxinsvalue(input, policyGroups);
            let max_ins_value = _.maxBy(AgencyAndInsValueGroups, a => a.value);

            if(max_ins_value) {
              let assoc = input.Association.find(a => a.Assoc_ID == max_ins_value.id);
              if(assoc) {
                input.LoanMaster.Primary_Agency_ID = assoc.Assoc_ID;
                input.LoanMaster.Primary_Agency_Name = assoc.Assoc_Name;
              }
            }
          }
        }
      }
    } catch {}
  }

  private getmaxinsvalue(input: loan_model, policyGroups: _.Dictionary<Loan_Policy[]>) {
    let InsValueGroups: Array<{id, value}> = [];

    for(let group in policyGroups) {
      let policies = policyGroups[group];

      let crop_units: Array<Loan_Crop_Unit> = [];

      policies.forEach(p => {
        crop_units.push(...this.getCropUnits(input, p));
      });

      let total_ins_value = _.sumBy(crop_units, a => (a.Ins_Value || 0));

      InsValueGroups.push({
        id: group,
        value: total_ins_value
      });
    }

    return InsValueGroups;
  }

  private getCropUnits(input: loan_model, policy: Loan_Policy) {
    try {
      let crop_units = input.LoanCropUnits.filter(a => a.Crop_Unit_Key == policy.Policy_Key);
      return crop_units;
    } catch {
      return [];
    }
  }

  prepareCAFields(input: loan_model) {
    try {
      if(input) {
        // prepare primary collateral
        let groupByCrops = _.groupBy(input.LoanCropUnits, a => a.Crop_Code);

        let mktValueGroups: Array<{id, value}> = [];

        for(let group in groupByCrops) {
          let cropUnits = groupByCrops[group];
          mktValueGroups.push({
            id: group,
            value: _.sumBy(cropUnits, a => (a.Mkt_Value || 0 ))
          });
        }

        let maxCropValue = _.maxBy(mktValueGroups, a => a.value);

        input.LoanMaster.Primary_Col = maxCropValue.value;
        input.LoanMaster.CAF1 = maxCropValue.value;

        // prepare revenue ins
        let revenuePolicies = input.LoanPolicies.filter(
          a =>
            a.Select_Ind &&
            a.Ins_Plan == 'MPCI' &&
            (a.Ins_Plan_Type == 'RP-HPE' || a.Ins_Plan_Type == 'RP')
        );

        let revenueCropUnits : Array<Loan_Crop_Unit> = [];
        revenuePolicies.forEach(p => {
          revenueCropUnits.push(...this.getCropUnits(input, p));
        });

        let revenueIns = _.sumBy(revenueCropUnits, a => (a.Ins_Value || 0));
        input.LoanMaster.CAF2 = revenueIns;
        input.LoanMaster.Rev_Ins_Col = revenueIns;

        // prepare non revenue ins
        let nonRevenuePolicies = input.LoanPolicies.filter(
          a =>
            a.Select_Ind &&
            a.Ins_Plan == 'MPCI' &&
            (a.Ins_Plan_Type != 'RP-HPE' && a.Ins_Plan_Type != 'RP')
        );

        let nonRevenueCropUnits : Array<Loan_Crop_Unit> = [];
        nonRevenuePolicies.forEach(p => {
          nonRevenueCropUnits.push(...this.getCropUnits(input, p));
        });

        let nonRevenueIns = _.sumBy(nonRevenueCropUnits, a => (a.Ins_Value || 0));
        input.LoanMaster.CAF3 = nonRevenueIns;
        input.LoanMaster.Non_Rev_Ins_Col = nonRevenueIns;
      }
    } catch {

    }
  }

}
