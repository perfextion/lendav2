import { Injectable } from '@angular/core';
import { loan_model } from '@lenda/models/loanmodel';
import {
  CriticalItem,
  CriticalItemAction
} from '@lenda/models/schematics/base-entities/critical-item.model';
import { SchematicsSettings } from '@lenda/models/schematics/index.model';
import { FlowchartService } from '@lenda/components/flowchart/flowchart.service';
import { SchematicsItemSettings } from '@lenda/models/schematics/schematics-item-settings.model';
import { SchematicStatus } from '@lenda/models/schematics/base-entities/schematic-status.enum';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment';
import { LoanOfficerStats } from '@lenda/models/user';
import * as _ from 'lodash';
import { Loan_Document } from '@lenda/models/loan/loan_document';
import { DataService } from '@lenda/services/data.service';

/**
 * This is a helper file to update schematics once loan calculation is completed
 */
@Injectable()
export class LoanCalculationSchematicsStorageService {
  private schematicsSettings: SchematicsSettings;

  constructor(
    private flowchartService: FlowchartService,
    private localstorage: LocalStorageService,
    private dataservice: DataService
  ) {
    this.getSchematicSettings();
    this.dataservice.getLoanObject().subscribe(loan => {
      this.updateSchematics(loan);
    });
  }

  /**
   * Updates schematics data based upon loan calculation worker
   * @param localloanobj local loan object received from loancalculationworker
   */
  public updateSchematics(localloanobj: loan_model) {
    // TODO: Update the function to make use of localloanobj and update schematics with
    // live data rather than mock data
    this.schematicsSettings.borrower.isStepCompleted = true;
    this.schematicsSettings.borrower.infos = [];
    this.schematicsSettings.decide.infos = [];

    this.setLOStats();
    this.setConditionInfo(localloanobj);

    // After data is updated, save the settings
    this.flowchartService.saveSchematicSettings(this.schematicsSettings);
  }

  private setConditionInfo(localloanobj: loan_model) {
    // let documents = _.sortBy(
    //   localloanobj.Loan_Documents.filter(
    //     lc => lc.ActionStatus != 3 && !lc.Upload_User_ID
    //   ),
    //   'Sort_Order'
    // );

    let documents = [];

    // Underwriting Documents
    let loanUnderwritingDocuments = documents.filter(
      lc => lc.Document_Type_Level == 1
    );

    this.schematicsSettings.underwriting.errors = this.loanDocumentToCriticalItem(
      loanUnderwritingDocuments
    );

    // Pre Requisite Documents
    let loanPreReqDocuments = documents.filter(
      lc => lc.Document_Type_Level == 2
    );
    this.schematicsSettings.prerequisiteConditions.errors = this.loanDocumentToCriticalItem(
      loanPreReqDocuments
    );

    // Closing Documents
    let loanClosingDocuments = documents.filter(
      lc => lc.Document_Type_Level == 3
    );

    this.schematicsSettings.closing.errors = this.loanDocumentToCriticalItem(
      loanClosingDocuments
    );
  }

  private setLOStats() {
    if (!this.schematicsSettings.lostats) {
      this.schematicsSettings.lostats = new SchematicsItemSettings(
        SchematicStatus.success,
        false
      );
    }

    let lostats = this.localstorage.retrieve(
      environment.loanofficerstats
    ) as LoanOfficerStats;

    if (!lostats) {
      lostats = new LoanOfficerStats();
    }

    this.schematicsSettings.lostats.details = [
      `${lostats.Latest_Crop_Year}`,
      `${lostats.Total_Commitment}`,
      `${lostats.Average_Close_Days}`
    ];
  }

  /**
   * Gets latest copy of schematic settings from flowchart service
   */
  getSchematicSettings() {
    this.schematicsSettings = this.flowchartService.getSchematicSettings();

    this.flowchartService.schematicsSettingsChange.subscribe(settings => {
      this.schematicsSettings = settings;
    });
  }

  private loanDocumentToCriticalItem(loan_conditions: Loan_Document[]) {
    return [];
  }
}
