import { Injectable } from '@angular/core';
import { loan_model } from '../../models/loanmodel';
import { Loan_Crop_Unit } from '../../models/cropmodel';
import { forEach } from '@angular/router/src/utils/collection';
import { LoggingService } from '../../services/Logs/logging.service';
import * as _ from 'lodash';
import { FarmapiService } from '@lenda/services/farm/farmapi.service';
import { environment } from '@env/environment.prod';
import { Insurance_Subpolicy } from '@lenda/models/insurancemodel';
import { LocalStorageService } from 'ngx-webstorage';
import { precise_round } from '@lenda/services/common-utils';
@Injectable()
export class wfrpworker {
  public input: loan_model;
  public refData: any;
  constructor(private logging: LoggingService,
    public localstorage: LocalStorageService
  ) {
    this.refData = this.localstorage.retrieve(environment.referencedatakey);
  }

  performWfrpCalculation(input:loan_model) {
    this.input=input;
    let lcpcount=this.input.LoanCrops.filter(a => a.ActionStatus != 3).length;
    //WFRP Enabled Disabled
    if(lcpcount <2){
      this.input.Loanwfrp.FC_WfrpEnabled=false;
    }
    else
    {
     this.input.Loanwfrp.FC_WfrpEnabled=true;
    }
    //MAX PCT RESTRICTIONS
    if(lcpcount >=3){
      this.input.Loanwfrp.FC_Wfrp_MaxLevelPct=85;
    }
    else
    {
      this.input.Loanwfrp.FC_Wfrp_MaxLevelPct=75;
    }
    try{
    this.performAvgforAllowableRevenue();
    this.performWfrpinsuranceCalculation();
    this.performWfrpCEICalculation();
    }
    catch(ex){
      console.log(ex);
    }
    return this.input;
  }
  performWfrpCEICalculation(): number {
    try{
      let loantotalCEI=precise_round(_.sumBy(this.input.LoanCropUnits.filter(p => !isNaN(p.Ins_Value)), "CEI_Value"),0);
      this.input.Loanwfrp.FC_Wfrp_CEI=-Math.abs(loantotalCEI- this.input.Loanwfrp.Wfrp_Disc_Ins_Value);
    }
    catch{
      return 0;
    }
    return 0;

  }

  performAvgforAllowableRevenue(){
    if(this.input.BorrowerIncomeHistory.length==0)
    {
      this.input.Loanwfrp.FC_Wfrp_MaxAllowedRevenue= 0;
    }
    else
    {
    let borrowerrecords=_.orderBy(this.input.BorrowerIncomeHistory,p=>p.Borrower_Year,"desc");

    let simpleAvgrev=_.sumBy(borrowerrecords,p=>parseFloat(p.FC_Borrower_Income.toString()))/(borrowerrecords.length)
    let indexAgdefaulted=true;
    let indexAvg=1;
    borrowerrecords.forEach((brrec,index) => {
      if(index!=borrowerrecords.length-1)
      {
        try{
        brrec.FC_Index=parseFloat((Math.min(Math.max((brrec.FC_Borrower_Income/borrowerrecords[index+1].FC_Borrower_Income),0.8),1.2).toFixed(2)));
        if(brrec.FC_Index==undefined){
          brrec.FC_Index=undefined;
        }
        }
        catch{
          brrec.FC_Index=undefined;
        }
      }
    });
    //indexing done till here
    let simpleIndexAvg=_.sumBy(borrowerrecords,p=>p.FC_Index==undefined?0:p.FC_Index)/(borrowerrecords.length-1);
    let rec=_.maxBy(borrowerrecords.slice(0,2),p=>p.FC_Index);
    let maxavg=0;
    if(rec!=undefined)
    {
      maxavg=rec.FC_Index;;
    }
    if(maxavg<=simpleIndexAvg){
      indexAvg=1;
    }
    else
    {
      borrowerrecords.map(p=>p.FC_Index).forEach(element => {
        if(element!=undefined && element!=null && !isNaN(element))
        indexAvg=indexAvg*element;
      });
      indexAvg=parseFloat(indexAvg.toFixed(2));
    }
    let indexAvgrevenue=simpleAvgrev * indexAvg;
    let maxexpansionrevenue=simpleAvgrev * 135/100;
    this.input.Loanwfrp.FC_Wfrp_MaxAllowedRevenue=Math.round(Math.max(indexAvgrevenue,maxexpansionrevenue));
  }
  }
  performWfrpinsuranceCalculation(){

      //  this.input.Loanwfrp.Wfrp_Ins_Value= (this.input.Loanwfrp.Level/100 *  this.input.Loanwfrp.Approved_Revenue)- this.input.Loanwfrp.Premium;
      this.input.Loanwfrp.Wfrp_Ins_Value=Math.min( this.input.Loanwfrp.FC_Wfrp_MaxAllowedRevenue,this.input.Loanwfrp.Approved_Revenue) * Math.min(this.input.Loanwfrp.FC_Wfrp_MaxLevelPct,this.input.Loanwfrp.Level)/100
       if(!this.refData){
        this.refData = this.localstorage.retrieve(environment.referencedatakey);
       }
       let disc= this.refData.Discounts.find(p=>p.Discount_Key=="WFRP").Discount_Value;
       this.input.Loanwfrp.Wfrp_Disc_Ins_Value= Math.round(this.input.Loanwfrp.Wfrp_Ins_Value * (1-disc/100));

       this.input.Loanwfrp.Wfrp_Add_Coverage=this.input.Loanwfrp.Wfrp_Ins_Value-this.input.LoanMaster.FC_Net_Market_Value_Insurance;
       if(isNaN(this.input.Loanwfrp.Wfrp_Add_Coverage) || this.input.Loanwfrp.Wfrp_Add_Coverage<0){
        this.input.Loanwfrp.Wfrp_Add_Coverage=0;
        this.input.Loanwfrp.Wfrp_Value=0;
        this.input.Loanwfrp.Wfrp_Disc_Value=0;
       }
       else
       {
       this.input.Loanwfrp.Wfrp_Value=this.input.Loanwfrp.Wfrp_Add_Coverage-this.input.Loanwfrp.Premium;
       this.input.Loanwfrp.Wfrp_Disc_Value=Math.round(this.input.Loanwfrp.Wfrp_Value * (1-disc/100));
       }
  }
}
