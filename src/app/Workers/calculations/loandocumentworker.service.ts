import { Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import {
  Ref_Exception,
  RefDataModel,
  Ref_Document_Type,
  Ref_Document_Detail
} from '@lenda/models/ref-data-model';
import { loan_model } from '@lenda/models/loanmodel';
import {
  Loan_Document,
  Loan_Document_Action,
  Loan_Document_Detail
} from '@lenda/models/loan/loan_document';
import { exceptionlevel } from '@lenda/models/commonmodels';
import { Loan_Exception } from '@lenda/models/loan/loan_exceptions';

import { getRandomInt } from '../utility/randomgenerator';
import {
  Eval_Helper,
  Local_Table
} from '@lenda/services/exception-helper.service';

interface IDocumentWorkerService {
  /**
   * Process Document for an Exception
   */
  Process_Exception_Document(
    ex: Ref_Exception,
    loan: loan_model,
    exception_value: exceptionlevel,
    entity?: any
  ): void;

  /**
   * Remove Not Needed Document
   */
  Remove_Not_Needed_Document(Loan: loan_model);

  /**
   * Remove Not Needed Document for a Particular Exception
   */
  Remove_Not_Needed_Document_For_Exception(Loan: loan_model, refEx: Ref_Exception, LE: Loan_Exception);
}

@Injectable()
export class LoanDocumentWorkerService implements IDocumentWorkerService {
  refdata: RefDataModel;

  constructor(
    private localstorage: LocalStorageService
  ) {
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);

    this.localstorage
      .observe(environment.referencedatakey)
      .subscribe((d: RefDataModel) => {
        this.refdata = d;
      });
  }

  Process_Exception_Document(
    ex: Ref_Exception,
    loan: loan_model,
    exception_value: exceptionlevel,
    entity?: any
  ) {
    let Local_Table_Name = this.Get_Table_Name(ex.Level_1_Hyper_Code || ex.Level_2_Hyper_Code);

    let ref_doc = this.refdata.Ref_Document_Types.find(
      a => a.Document_Type_ID == ex.Document_Type_ID
    );

    if (ref_doc) {
      if (
        Local_Table_Name == Local_Table.LoanMaster &&
        ref_doc.Replication_Association_Type_Code
      ) {
        if(ref_doc.Assoc_Replication_Code == 'W') {
          this.Process_Assoc_Replication_Document(
            loan,
            ref_doc,
            ex,
            exception_value,
            Local_Table_Name
          );
        } else {
          this.Process_Loan_Master_Condition_Document(
            exception_value,
            loan,
            ref_doc,
            ex,
            Local_Table_Name
          );
        }
      } else if (Local_Table_Name == Local_Table.Association) {
        if (ref_doc.Assoc_Replication_Code == 'W') {
          this.Process_Assoc_Replication_Document(
            loan,
            ref_doc,
            ex,
            exception_value,
            Local_Table_Name
          );
        } else {
          this.Process_Association_Condition_Document(
            ref_doc,
            entity,
            ex,
            loan,
            exception_value,
            Local_Table_Name
          );
        }
      } else if (Local_Table_Name == Local_Table.Assoc_Replication) {
        if (ref_doc.Assoc_Replication_Code == 'W') {
          this.Process_Assoc_Replication_Document(
            loan,
            ref_doc,
            ex,
            exception_value,
            Local_Table_Name
          );
        }
      } else {
        this.Process_Exception_Document_Inner(
          ex,
          ref_doc,
          loan,
          exception_value,
          entity,
          Local_Table_Name
        );
      }
    }
  }

  private Process_Association_Condition_Document(
    ref_doc: Ref_Document_Type,
    entity: any,
    ex: Ref_Exception,
    loan: loan_model,
    exception_value: exceptionlevel,
    Local_Table_Name: string
  ) {
    if (ref_doc.Replication_Association_Type_Code) {
      if (entity.Assoc_Type_Code == ref_doc.Replication_Association_Type_Code) {
        this.Process_Exception_Document_Inner(
          ex,
          ref_doc,
          loan,
          exception_value,
          entity,
          Local_Table_Name
        );
      }
    } else {
      this.Process_Exception_Document_Inner(
        ex,
        ref_doc,
        loan,
        exception_value,
        entity,
        Local_Table_Name
      );
    }
  }

  private Process_Loan_Master_Condition_Document(
    exception_value: exceptionlevel,
    loan: loan_model,
    ref_doc: Ref_Document_Type,
    ex: Ref_Exception,
    Local_Table_Name: string
  ) {
    if (exception_value) {
      let Associations = loan.Association.filter(
        a => a.Assoc_Type_Code == ref_doc.Replication_Association_Type_Code && a.ActionStatus != 3
      );
      if (Associations) {
        Associations.forEach(assoc => {
          this.Process_Exception_Document_Inner(
            ex,
            ref_doc,
            loan,
            exception_value,
            assoc,
            Local_Table_Name
          );
        });
      }
    } else {
      loan.Loan_Documents.filter(
        a => a.Document_Type_ID == ex.Document_Type_ID
      ).forEach(a => {
        a.Doc_Needed_Ind = 0;
        a.ActionStatus = 3;
      });
    }
  }

  private Process_Assoc_Replication_Document(
    loan: loan_model,
    ref_doc: Ref_Document_Type,
    ex: Ref_Exception,
    exception_value: exceptionlevel,
    Local_Table_Name: string
  ) {
    const entities = loan.Association.filter(
      a =>
        a.ActionStatus != 3 &&
        a.Assoc_Type_Code == ref_doc.Replication_Association_Type_Code
    );

    if(entities.length > 0) {
      const name_list = entities.map(a => a.Assoc_Name).join(', ');

      const entity = {
        Assoc_Name: name_list,
        Assoc_Type_Code: ref_doc.Replication_Association_Type_Code,
        Assoc_ID: loan.LoanMaster.Borrower_ID
      };

      this.Process_Exception_Document_Inner(
        ex,
        ref_doc,
        loan,
        exception_value,
        entity,
        Local_Table_Name
      );
    }
  }

  private Process_Exception_Document_Inner(
    ex: Ref_Exception,
    ref_doc: Ref_Document_Type,
    loan: loan_model,
    ex_val: exceptionlevel,
    entity?: any,
    Local_Table_Name?: string
  ) {
    let doc = new Loan_Document();
    doc.Document_Name = ex.Exception_ID_Text;

    if (!this.Validate_Entity(ref_doc, entity)) {
      return;
    }

    if (!!ref_doc.Document_Name_Override) {
      doc.Document_Name = Eval_Helper.Get_Doc_Override_Name(
        ref_doc,
        ref_doc.Table_Name || Local_Table_Name,
        loan,
        entity
      );
    }

    doc.Exception_Level = ex_val;

    doc.ActionStatus = 1;
    doc.Document_Type_Level = ref_doc.Document_Type_Level;
    doc.Document_Type_ID = ref_doc.Document_Type_ID;
    doc.Loan_Full_ID = loan.Loan_Full_ID;
    doc.Loan_ID = loan.LoanMaster.Loan_ID;
    doc.Loan_Document_ID = getRandomInt(Math.pow(10, 3), Math.pow(10, 5));
    doc.Sort_Order = ref_doc.Sort_Order;

    doc.Document_Details = Eval_Helper.Get_Document_Details(
      ref_doc,
      ref_doc.Table_Name || Local_Table_Name,
      loan,
      entity,
      this.refdata
    );

    if (ref_doc.Replication_Association_Type_Code) {
      doc.Replication_Association_Type =
        ref_doc.Replication_Association_Type_Code;

      doc.Replication_Association_ID = Eval_Helper.Get_Replication_Association_ID(
        ref_doc,
        ref_doc.Table_Name || Local_Table_Name,
        loan,
        entity
      );
    }

    let key = Eval_Helper.Get_Document_Key(ref_doc, Local_Table_Name, loan, entity, this.refdata);
    let Temp_ID = Eval_Helper.Temp_ID(ref_doc, Local_Table_Name, loan, entity);

    doc.Document_Key = JSON.stringify({
      Key: key,
      ID: Temp_ID
    });

    let exDoc = loan.Loan_Documents.filter(
      a =>
        a.Document_Type_ID == ref_doc.Document_Type_ID &&
        a.Action_Ind != Loan_Document_Action.Archived
    ).find(a => {
      let id_key = Eval_Helper.Get_ID_And_Key(a, ref_doc, Local_Table_Name, loan, entity);

      if (a.ActionStatus == 0) {
        return id_key.Key == key || id_key.ID == Temp_ID;
      }

      if(ref_doc.Replication_Association_Type_Code == 'BOR') {
        return id_key.Key == key;
      }

      return id_key.ID == Temp_ID;
    });

    if (!!ex_val) {
      if (exDoc) {
        // Check if document has been already uploaded
        if (!!exDoc.Upload_Date_Time) {
          if (exDoc.Document_Details != doc.Document_Details) {
            exDoc.Action_Ind = Loan_Document_Action.Archived;
            doc.Action_Ind = Loan_Document_Action.RequireReupload;

            doc.ActionStatus = 1;
            exDoc.ActionStatus = 3;

            doc.Doc_Needed_Ind = 1;
            doc.Prev_Loan_Document_ID = exDoc.Loan_Document_ID;

            loan.Loan_Documents.push(doc);

            ex.Exception_Details.Documents.push(doc.Loan_Document_ID);
            ex.Exception_Details.Deleted_Documents.push(exDoc.Loan_Document_ID);
          } else {
            exDoc.Doc_Needed_Ind = 1;
            exDoc.Action_Ind = Loan_Document_Action.Uploaded;
            exDoc.Replication_Association_ID = doc.Replication_Association_ID;
            exDoc.Exception_Level = doc.Exception_Level;

            ex.Exception_Details.Exisiting_Documents.push(
              exDoc.Loan_Document_ID
            );
          }
        } else {
          if (exDoc.ActionStatus != 1) {
            exDoc.ActionStatus = 2;
          }

          exDoc.Document_Name = doc.Document_Name;
          exDoc.Document_Key = doc.Document_Key;
          exDoc.Document_Details = doc.Document_Details;
          exDoc.Doc_Needed_Ind = 1;
          exDoc.Action_Ind = Loan_Document_Action.RequireUpload;
          exDoc.Replication_Association_ID = doc.Replication_Association_ID;
          exDoc.Exception_Level = doc.Exception_Level;

          ex.Exception_Details.Exisiting_Documents.push(exDoc.Loan_Document_ID);
        }
      } else {
        ex.Exception_Details.Documents.push(doc.Loan_Document_ID);
        doc.Action_Ind = Loan_Document_Action.RequireUpload;
        doc.Doc_Needed_Ind = 1;
        loan.Loan_Documents.push(doc);
      }
    } else {
      if (exDoc) {
        ex.Exception_Details.Deleted_Documents.push(exDoc.Loan_Document_ID);

        if (exDoc.ActionStatus == 1) {
          let index = loan.Loan_Documents.indexOf(exDoc);
          loan.Loan_Documents.splice(index, 1);
        } else {
          exDoc.ActionStatus = 3;
          exDoc.Doc_Needed_Ind = 0;
          exDoc.Action_Ind = Loan_Document_Action.Archived;
        }
      }
    }
  }

  private Get_Table_Name(Hyper_Code: string) {
    let codes = Hyper_Code.split('.');
    let tableName = codes[0];

    let Local_Table_Name = Eval_Helper.Map_Table(tableName);
    return Local_Table_Name;
  }

  private Validate_Entity(ref_doc: Ref_Document_Type, entity: any) {
    if (entity) {
      if (ref_doc.Table_Name == Local_Table.Farms) {
        return entity.Farm_State_ID && entity.Farm_County_ID;
      }

      if (ref_doc.Table_Name == Local_Table.Association) {
        return entity.Assoc_Name;
      }

      return true;
    }

    return true;
  }

  private Assign_Document_Details(ref_doc: Ref_Document_Type, loan_doc: Loan_Document, entity: any, loan: loan_model) {
    let ref_doc_details = this.refdata.Ref_Document_Details.filter(a => a.Document_Type_ID == ref_doc.Document_Type_ID);

    let critical_field_value_changed = false;

    if(ref_doc_details && ref_doc_details.length > 0) {
      ref_doc_details.forEach(detail => {
        critical_field_value_changed = critical_field_value_changed || this.Add_Or_Update_Document_Details(detail, loan, entity, loan_doc);
      });
    }

    return critical_field_value_changed;
  }

  private Add_Or_Update_Document_Details(detail: Ref_Document_Detail, loan: loan_model, entity: any, loan_doc: Loan_Document) {
    let value = '';
    let critical_field_value_changed = false;

    if (detail.Table_Name == 'LoanMaster') {
      value = loan.LoanMaster[detail.Field_Name];
    } else {
      value = entity[detail.Field_Name];
    }

    let exisitingDetail = loan.Loan_Document_Details.find(
      a =>
        a.ActionStatus != 3 &&
        a.Field_ID == detail.Field_ID &&
        a.Loan_Document_ID == loan_doc.Loan_Document_ID
    );

    if (exisitingDetail) {
      if(detail.Critical_Ind == 1 && exisitingDetail.Field_ID_Value != value) {
        critical_field_value_changed = true;
      }

      exisitingDetail.Field_ID_Value = value;
      if (exisitingDetail.ActionStatus != 1) {
        exisitingDetail.ActionStatus = 2;
      }
    } else {
      let docDetail = new Loan_Document_Detail(value);
      docDetail.ActionStatus = 1;
      docDetail.Status = 1;
      docDetail.Critical_Change_Detail_Ind = detail.Critical_Ind;
      docDetail.Field_ID = detail.Field_ID;
      docDetail.Loan_Full_ID = loan.Loan_Full_ID;
      docDetail.Loan_Document_ID = loan_doc.Loan_Document_ID;

      if(loan_doc.Upload_Date_Time && detail.Critical_Ind == 1) {
        critical_field_value_changed = true;
      } else {
        critical_field_value_changed = false;
      }

      loan.Loan_Document_Details.push(docDetail);
    }

    return critical_field_value_changed;
  }

  Remove_Not_Needed_Document(Loan: loan_model) {
    _.remove(
      Loan.Loan_Documents,
      doc => !doc.Is_Custom && !doc.Doc_Needed_Ind && doc.ActionStatus == 1
    );

    Loan.Loan_Documents.filter(
      a => !a.Is_Custom && !a.Doc_Needed_Ind && a.ActionStatus != 1
    ).forEach(doc => {
      doc.ActionStatus = 3;
    });
  }

  Remove_Not_Needed_Document_For_Exception(Loan: loan_model, refEx: Ref_Exception, LE: Loan_Exception) {
    let docs = Loan.Loan_Documents.filter(A => A.Document_Type_ID == refEx.Document_Type_ID);
    if(LE.ActionStatus == 1) {
      docs.forEach(A =>  {
        Loan.Loan_Documents.splice(Loan.Loan_Documents.indexOf(A), 1);
      });
    } else {
      docs.forEach(a => {
        a.ActionStatus = 3;
        a.Doc_Needed_Ind = 0;
      })
    }
  }
}
