import { Injectable } from '@angular/core';

import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { Loan_Crop_Unit } from '@lenda/models/cropmodel';
import { Loan_Farm } from '@lenda/models/farmmodel.';
import { RefDataModel } from '@lenda/models/ref-data-model';

import { Helper } from '@lenda/services/math.helper';
import {
  lookupCountyValueFromPassedRefData,
  lookupStateNameFromPassedRefObject,
  lookupStateCodeFromPassedRefObject
} from '../utility/aggrid/stateandcountyboxes';

import { LoggingService } from '@lenda/services/Logs/logging.service';

@Injectable()
export class FarmcalculationworkerService {
  private refdata: RefDataModel;

  constructor(
    private logging:LoggingService,
    private localstorage: LocalStorageService
  ) {
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);

    this.localstorage.observe(environment.referencedatakey).subscribe(res => {
      this.refdata = res;
    });
  }


  prepareLoanfarmmodel(input:loan_model):loan_model{
    try{
      let starttime = new Date().getTime();

      let total_owned = 0;
      let total_leased = 0;

      for(let i = 0; i<input.Farms.length; i++){
        let farm = input.Farms[i];

        farm.FC_Total_Acres = farm.Irr_Acres + farm.NI_Acres;
        farm.FC_County_Name = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
        farm.FC_State_Name = lookupStateNameFromPassedRefObject(farm.Farm_State_ID, this.refdata);
        farm.FC_State_Code = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata);

        if(farm.Owned == 1) {
          total_owned += farm.FC_Total_Acres;
        } else {
          total_leased += farm.FC_Total_Acres;
        }

        if(farm.Waived_Rent == undefined || farm.Waived_Rent == null) {
          if(farm.Rent_UOM == 1) {
            farm.Waived_Rent = Helper.divide(farm.Cash_Rent_Waived_Amount, farm.FC_Total_Acres);
          } else {
            farm.Waived_Rent = farm.Cash_Rent_Waived_Amount || 0;
          }
        }

        if(farm.Display_Rent == undefined || farm.Display_Rent == null) {
          if(farm.Rent_UOM == 1) {
            farm.Display_Rent = Helper.divide(farm.Cash_Rent_Total, farm.FC_Total_Acres);
          } else {
            farm.Display_Rent = farm.Cash_Rent_Total || 0;
          }
        }

        if(farm.Percent_Prod) {
          farm.Share_Rent = 100 - farm.Percent_Prod;
        }

        if(!farm.Owned) {
          if(farm.Rent_UOM == 1) {
            farm.Cash_Rent_Total = farm.Display_Rent * farm.FC_Total_Acres;
            farm.Cash_Rent_Waived_Amount = farm.Waived_Rent * farm.FC_Total_Acres;
          } else {
            farm.Cash_Rent_Total = farm.Display_Rent;
            farm.Cash_Rent_Waived_Amount = farm.Waived_Rent;
          }
        } else {
          farm.Cash_Rent_Total = 0;
          farm.Cash_Rent_Waived_Amount = 0;
        }

        let cropUnits = input.LoanCropUnits.filter(a => a.Farm_ID == farm.Farm_ID);
        farm.FC_Total_Used_Acres = _.sumBy(cropUnits, (c: Loan_Crop_Unit) => c.CU_Acres || 0);
        farm.FC_Total_Used_Irr_Acres = _.sumBy(cropUnits.filter(a => a.Crop_Practice_Type_Code && a.Crop_Practice_Type_Code.toUpperCase() == 'IRR'), (c: Loan_Crop_Unit) => c.CU_Acres || 0);
        farm.FC_Total_Used_NI_Acres = _.sumBy(cropUnits.filter(a => a.Crop_Practice_Type_Code && a.Crop_Practice_Type_Code.toUpperCase() == 'NI'), (c: Loan_Crop_Unit) => c.CU_Acres || 0);

        farm.Cash_Rent_Per_Acre = Helper.divide(farm.Cash_Rent_Total, farm.FC_Total_Used_Acres);
        farm.Cash_Rent_Waived = Helper.divide(farm.Cash_Rent_Waived_Amount, farm.FC_Total_Used_Acres);

        farm.FC_Net_Rent_Acre = Helper.divide(farm.Cash_Rent_Total - farm.Cash_Rent_Waived_Amount, farm.FC_Total_Used_Acres);

        cropUnits.forEach(cu => {
          cu.FC_Total_Used_Acres = farm.FC_Total_Used_Acres;
          cu.FC_Total_Used_Irr_Acres = farm.FC_Total_Used_Irr_Acres;
          cu.FC_Total_Used_NI_Acres = farm.FC_Total_Used_NI_Acres;
        });

        // Farm Key Validation
        let isAlreadyExistingKey = input.Farms.some(
          x => x.Farm_ID != farm.Farm_ID && this.Farm_Key(farm) == this.Farm_Key(x)
        );

        if(isAlreadyExistingKey) {
          farm.Duplicate_Farm_Key = true;
        } else {
          farm.Duplicate_Farm_Key = false;
        }
      }

      if(total_owned > total_leased) {
        input.LoanMaster.FC_Owned = true;
      } else {
        input.LoanMaster.FC_Owned = false;
      }

      let endtime = new Date().getTime();
        //level 2 log
      this.logging.checkandcreatelog(2, 'Calc_LoanFarm', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
      return input;
    }
    catch(e){
        //level 1 log as its error
      this.logging.checkandcreatelog(1, 'Calc_LoanFarm', e);
      return input;
    }
  }

  Farm_Key (a: Loan_Farm) {
    return `${a.Farm_County_ID}_${a.Farm_County_ID}_${a.FSN}_${a.Section}_${a.Rated}`;
  }
}
