import { Injectable } from '@angular/core';

import * as _ from "lodash";
import { LocalStorageService } from 'ngx-webstorage';

import { Loan_Marketing_Contract, loan_model } from '@lenda/models/loanmodel';
import { precise_round } from '@lenda/services/common-utils';
import { lookupCropValueFromPassedRefData } from '../utility/aggrid/stateandcountyboxes';
import { RefDataModel } from '@lenda/models/ref-data-model';

import { LoggingService } from '@lenda/services/Logs/logging.service';
import { environment } from '@env/environment.prod';

@Injectable()
export class MarketingcontractcalculationService {
  private refdata: RefDataModel;

  constructor(
    public logging: LoggingService,
    private localstorage: LocalStorageService
  ) {
    this.getRefData();
  }

  getRefData() {
    if(!this.refdata) {
      this.refdata = this.localstorage.retrieve(environment.referencedatakey);
    }
  }

  updateMarketingCalculation(localloanobject : loan_model) {
    this.getRefData();

    let starttime = new Date().getTime();

    localloanobject.LoanMarketingContracts.forEach(contract => {
      contract.FC_Crop_Name = lookupCropValueFromPassedRefData(contract.Crop_Code, this.refdata);
      this.updateMktValueAndContractPer(localloanobject, contract);
    });

    let endtime = new Date().getTime();
    this.logging.checkandcreatelog(1, 'Calc_CrpContract MarketingCalculation', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
  }

  private updateMktValueAndContractPer(localloanobject : loan_model, contract: Loan_Marketing_Contract) {
    contract.Market_Value = contract.Price * contract.Quantity;
    let supplyQuantity = this.getCropContract(localloanobject,contract.Crop_Code, 'Irr') + this.getCropContract(localloanobject,contract.Crop_Code, 'NI');

    if(supplyQuantity > 0){
      contract.Contract_Per = precise_round((contract.Quantity / supplyQuantity)*100, 1);
    }else{
      contract.Contract_Per = 0;
    }
  }

  private getCropContract(localloanobject : loan_model, cropCode : string, type : string){
    // let starttime = new Date().getTime();
    if(localloanobject.LoanCropUnits && localloanobject.CropYield && localloanobject.Farms){

      let totalCUAcres= 0;
      let filteredCropUnits = localloanobject.LoanCropUnits.filter(lcu=>lcu.Crop_Code === cropCode && lcu.Crop_Practice_Type_Code === type);
      if(filteredCropUnits && filteredCropUnits.length > 0){
        filteredCropUnits.forEach(cu=>{
          let cuFarm = localloanobject.Farms.find(f=>f.Farm_ID == cu.Farm_ID);
          if(cuFarm){
            totalCUAcres += cu.CU_Acres * ((cuFarm.Percent_Prod || 100)/100);
          }
        })
      }

      let totalCropYield = 0
      let selectedCY = localloanobject.CropYield.find(cy=>cy.Crop_Code === cropCode  && cy.Practice === type);
      if(selectedCY){
        totalCropYield = selectedCY.CropYield;
      }
        // let endtime = new Date().getTime();
        // this.logging.checkandcreatelog(1, 'Calc_CrpContract_2', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
        return totalCUAcres * totalCropYield;
    }else{
      return 0;
    }

  }

  performPriceCalculation(localloanobject : loan_model){
    let starttime = new Date().getTime();
    localloanobject.LoanCrops.filter(a => a.ActionStatus != 3).forEach(crop =>{
      let matchingMC  = localloanobject.LoanMarketingContracts.filter(mc=>mc.Crop_Code == crop.Crop_Code && mc.ActionStatus != 3);
      if(matchingMC.length > 0){
        crop.Percent_booked =  _.sumBy(matchingMC,'Contract_Per');
        crop.Contract_Qty = _.sumBy(matchingMC,'Quantity');
        let totalContractPrice= _.sumBy(matchingMC,(mc=>mc.Quantity * mc.Price));
        crop.Contract_Price = totalContractPrice/crop.Contract_Qty;
        //the same caclulation is in price component, which should be shisted to common place
        crop.Marketing_Adj = (crop.Contract_Price - (crop.Basic_Adj + crop.Crop_Price))*(crop.Percent_booked/100);
        crop.Marketing_Adj = crop.Marketing_Adj ? parseFloat(crop.Marketing_Adj.toFixed(2)) : 0;
        crop.Adj_Price = (crop.Crop_Price || 0) + (crop.Basic_Adj || 0) + (crop.Marketing_Adj ||0) + (crop.Rebate_Adj || 0);
      }else{
        crop.Percent_booked = 0;
        crop.Contract_Price = 0;
        crop.Contract_Qty = 0;
        crop.Marketing_Adj =0;
        crop.Adj_Price = (crop.Crop_Price || 0) + (crop.Basic_Adj || 0) + (crop.Marketing_Adj ||0) + (crop.Rebate_Adj || 0);

      }
    });
    let endtime = new Date().getTime();
      //level 2 log
    this.logging.checkandcreatelog(2, 'Calc_Price', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
    // localloanobject.LoanMarketingContracts.forEach(mktContracts =>{
    //   let matchingCrop = localloanobject.LoanCrops.find(loanCrops=> loanCrops.Crop_Code === mktContracts.Crop_Code);
    //   if(matchingCrop){
    //     matchingCrop.Percent_booked = mktContracts.Contract_Per;
    //     matchingCrop.ActionStatus = 2;
    //   }
    // })

  }

}
