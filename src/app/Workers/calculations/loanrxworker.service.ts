import { Injectable } from '@angular/core';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { Borrowerincomehistoryworker } from './borrowerincomehistoryworker.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoancropunitcalculationworkerService } from './loancropunitcalculationworker.service';
import { LoancrophistoryService } from './loancrophistory.service';
import { FarmcalculationworkerService } from './farmcalculationworker.service';
import { Collateralcalculationworker } from './collateralcalculationworker.service';
import { QuestionscalculationworkerService } from './questionscalculationworker.service';
import { LoanMasterCalculationWorkerService } from './loan-master-calculation-worker.service';
import { OverallCalculationServiceService } from './overall-calculation-service.service';
import { MarketingcontractcalculationService } from './marketingcontractcalculation.service';
import { OptimizercalculationService } from './optimizercalculationservice.service';
import * as _ from 'lodash';
import { BudgetHelperService } from '@lenda/components/budget/budget-helper.service';
import { wfrpworker } from './wfrpworker.service';
import { LoanTypeCalculationService } from '@lenda/services/loan-type.service';
import { BudgetcalculationworkerService } from './budgetcalculationworker.service';
import { CropPriceWorkerService } from './croppriceworker.service';
import { precise_round } from '@lenda/services/common-utils';
import { daysDiff } from '@lenda/aggridformatters/valueformatters';
import { RefDataService } from '@lenda/services/ref-data.service';
import { Helper } from '@lenda/services/math.helper';

@Injectable()
export class LoanRxCalculationWorker {
  constructor(
    private borrowerincomehistory: Borrowerincomehistoryworker,
    private loancropunitworker: LoancropunitcalculationworkerService,
    private loancrophistoryworker: LoancrophistoryService,
    private farmcalculation: FarmcalculationworkerService,
    private collateralcalculation: Collateralcalculationworker,
    private questionscalculations: QuestionscalculationworkerService,
    private loanMasterCalcualtions: LoanMasterCalculationWorkerService,
    private budgethelper: BudgetHelperService,
    private overallCalculationService: OverallCalculationServiceService,
    private logging: LoggingService,
    private marketingContractService: MarketingcontractcalculationService,
    private optimizercaluclations: OptimizercalculationService,
    private wfrpworkerobject: wfrpworker,
    private loanTypecalculation: LoanTypeCalculationService,
    private budgetCalculation: BudgetcalculationworkerService,
    private croppriceworker: CropPriceWorkerService,
    private refDataService: RefDataService
  ) {}

  moveAllAcresToIrr(loan_obj) {
    let loan: loan_model = JSON.parse(loan_obj);

    try {
      loan.Farms.filter(a => a.ActionStatus != 3).forEach(farm => {
        farm.Irr_Acres = farm.FC_Total_Acres;
        farm.NI_Acres = 0;
      });

      let cropUnitGroups = _.groupBy(loan.LoanCropUnits, a => a.Farm_ID);

      for (let Farm_ID in cropUnitGroups) {
        let farm = loan.Farms.find(xa => Number(Farm_ID) == xa.Farm_ID);
        let cropunits = cropUnitGroups[Farm_ID];

        const cropunitsLength = cropunits.length;

        const acrePerCrop = parseFloat(
          Helper.divide(farm.Irr_Acres, cropunitsLength).toFixed(2)
        );

        cropunits.forEach(a => {
          if (a.Crop_Practice_Type_Code == 'Irr') {
            a.CU_Acres = acrePerCrop;
          } else {
            a.CU_Acres = 0;
          }
        });
      }

      loan = this.performLoanRxCalculation(loan);
    } catch (ex) {}

    return loan;
  }

  moveAllAcresToNI(loan_obj) {
    let loan: loan_model = JSON.parse(loan_obj);

    try {
      loan.Farms.filter(a => a.ActionStatus != 3).forEach(farm => {
        farm.NI_Acres = farm.FC_Total_Acres;
        farm.Irr_Acres = 0;
      });

      let cropUnitGroups = _.groupBy(loan.LoanCropUnits, a => a.Farm_ID);

      for (let Farm_ID in cropUnitGroups) {
        let farm = loan.Farms.find(xa => Number(Farm_ID) == xa.Farm_ID);
        let cropunits = cropUnitGroups[Farm_ID];

        const cropunitsLength = cropunits.length;

        const acrePerCrop = parseFloat(
          Helper.divide(farm.NI_Acres, cropunitsLength).toFixed(2)
        );

        cropunits.forEach(a => {
          if (a.Crop_Practice_Type_Code == 'NI') {
            a.CU_Acres = acrePerCrop;
          } else {
            a.CU_Acres = 0;
          }
        });
      }

      loan = this.performLoanRxCalculation(loan);
    } catch (ex) {}

    return loan;
  }

  maxCF(loan_obj: string, isIRR = true) {
    let loanmodel: loan_model = JSON.parse(loan_obj);

    let pCode = isIRR ? 'IRR' : 'NI';
    let property = 'FC_Cashflow_Acre';

    if(isIRR) {
      loanmodel = this.moveAllAcresToIrr(loan_obj);
    } else {
      loanmodel = this.moveAllAcresToNI(loan_obj);
    }

    this.maxOptimizer(loanmodel, isIRR, pCode, property);

    return loanmodel;
  }

  maxRC(loan_obj: string, isIRR = true) {
    let loanmodel: loan_model = JSON.parse(loan_obj);

    let pCode = isIRR ? 'IRR' : 'NI';
    let property = 'FC_RiskCushion_Acre';

    if(isIRR) {
      loanmodel = this.moveAllAcresToIrr(loan_obj);
    } else {
      loanmodel = this.moveAllAcresToNI(loan_obj);
    }

    this.maxOptimizer(loanmodel, isIRR, pCode, property);

    return loanmodel;
  }

  maxMargin(loan_obj: string, isIRR = true) {
    let loanmodel: loan_model = JSON.parse(loan_obj);

    let pCode = isIRR ? 'IRR' : 'NI';
    let property = 'FC_Margin_Acre';

    if(isIRR) {
      loanmodel = this.moveAllAcresToIrr(loan_obj);
    } else {
      loanmodel = this.moveAllAcresToNI(loan_obj);
    }

    this.maxOptimizer(loanmodel, isIRR, pCode, property);

    return loanmodel;
  }

  private maxOptimizer(
    loanmodel: loan_model,
    isIRR: boolean,
    pCode: string,
    property: string
  ) {
    loanmodel.Farms.filter(f => {
      if (isIRR) {
        return f.Irr_Acres > 0;
      }
      return f.NI_Acres > 0;
    }).forEach(farm => {
      const cropUnits = loanmodel.LoanCropUnits.filter(
        c =>
          c.Crop_Practice_Type_Code &&
          c.Crop_Practice_Type_Code.toUpperCase() == pCode &&
          c.Farm_ID == farm.Farm_ID
      );

      let maxValueCropUnit = _.maxBy(cropUnits, c => c[property]);

      if (!maxValueCropUnit) {
        return;
      }

      try {
        if (isIRR) {
          maxValueCropUnit.CU_Acres = farm.Irr_Acres;
        } else {
          maxValueCropUnit.CU_Acres = farm.NI_Acres;
        }

        cropUnits.forEach(lcu => {
          if (lcu.Crop_Practice_ID != maxValueCropUnit.Crop_Practice_ID) {
            lcu.CU_Acres = 0;
          }
        });
      } catch (ex) {
        console.error(ex.message);
      }
    });
  }

  maximizerMPCILevelPercent(loan_obj) {
    let loan: loan_model = JSON.parse(loan_obj);

    try {
      loan.LoanPolicies.filter(a => a.Select_Ind).forEach(policy => {
        if (policy.Ins_Plan == 'MPCI') {
          policy.Upper_Level = 90;
        }
      });

      loan = this.performLoanRxCalculation(loan);
    } catch (ex) {}

    return loan;
  }

  private performLoanRxCalculation(loan: loan_model) {
    console.time('Loan Calculation');

    let localloanobj: loan_model = JSON.parse(JSON.stringify(loan));

    this.croppriceworker.generateCropPrice(localloanobj);

    if (localloanobj.BorrowerIncomeHistory !== null) {
      localloanobj = this.borrowerincomehistory.prepareborrowerincomehistorymodel(
        localloanobj
      );
    }

    if (localloanobj.Association) {
      let sumofthirdparty = _.sumBy(
        localloanobj.Association.filter(
          a => a.Assoc_Type_Code == 'THR' && a.ActionStatus != 3
        ),
        a => parseFloat(a.Amount as any) || 0
      );

      localloanobj.LoanMaster.Third_Party_Credit = sumofthirdparty;
    }

    if (localloanobj.LoanCropUnits) {
      localloanobj = this.loancropunitworker.prepareLCP_APHfromAPH(
        localloanobj
      );
      localloanobj = this.loancropunitworker.prepareRate_YieldfromYield(
        localloanobj
      );
      localloanobj = this.loancropunitworker.prepare_Rent_Percent(localloanobj);
    }

    if (localloanobj.LoanOtherIncomes) {
      localloanobj.LoanMaster.Net_Other_Income = _.sumBy(
        localloanobj.LoanOtherIncomes.filter(a => a.ActionStatus != 3),
        other => other.Amount || 0
      );
    }

    if (localloanobj.CropYield != null) {
      localloanobj = this.loancrophistoryworker.prepareLoancrophistorymodel(
        localloanobj
      );
    }
    if (
      localloanobj.LoanCropUnits != null &&
      localloanobj.LoanCropPractices != null
    ) {
      localloanobj = this.optimizercaluclations.performcalculations(
        localloanobj
      );
    }

    if (localloanobj.LoanCropUnits) {
      localloanobj = this.loancropunitworker.prepareLoancropunitmodel(
        localloanobj
      );
      localloanobj = this.loancropunitworker.fillFCValuesforCropunits(
        localloanobj
      );
    }

    //  WFRP BEfore Loan Insurance
    localloanobj = this.wfrpworkerobject.performWfrpCalculation(localloanobj);

    localloanobj.LoanMaster.Net_Market_Value_Insurance = precise_round(
      (localloanobj.LoanMaster.FC_Net_Market_Value_Insurance || 0) +
        localloanobj.Loanwfrp.Wfrp_Value,
      0
    );
    localloanobj.LoanMaster.Disc_value_Insurance =
      (localloanobj.LoanMaster.FC_Disc_value_Insurance || 0) +
      localloanobj.Loanwfrp.Wfrp_Disc_Value;

    //FARM CALCULATIONS
    if (localloanobj.Farms != null) {
      localloanobj = this.farmcalculation.prepareLoanfarmmodel(localloanobj);
    }

    //Populate Total Acres at LoanMaster level
    localloanobj.LoanMaster.Total_Acres = precise_round(
      _.sumBy(localloanobj.Farms, f => f.FC_Total_Acres || 0),
      1
    );

    // fixed expense - cash rent calculation
    localloanobj = this.budgetCalculation.prepareCashrentBudgetModel(
      localloanobj
    );

    // Close date and Estimated days
    try {
      if (!localloanobj.LoanMaster.Close_Date) {
        localloanobj.LoanMaster.Close_Date = new Date();
      } else {
        let d = new Date(localloanobj.LoanMaster.Close_Date);
        if (d.getFullYear() == 1900) {
          localloanobj.LoanMaster.Close_Date = new Date();
        }
      }
    } catch {
      localloanobj.LoanMaster.Close_Date = new Date();
    }

    if (!localloanobj.LoanMaster.Maturity_Date) {
      localloanobj.LoanMaster.Maturity_Date = this.refDataService.getDefaultMaturityDateString(
        localloanobj.LoanMaster.Crop_Year
      );
    }

    localloanobj.LoanMaster.Estimated_Days = daysDiff(
      localloanobj.LoanMaster.Maturity_Date,
      localloanobj.LoanMaster.Close_Date
    );

    // BUDGET CALCULATIONS
    try {
      if (
        localloanobj.LoanBudget &&
        localloanobj.LoanBudget.length > 0 &&
        localloanobj.LoanCropPractices
      ) {
        localloanobj = this.budgethelper.getInsurancePremiums(localloanobj);

        localloanobj.LoanCropPractices.forEach(cp => {
          let cpLoanBudget = localloanobj.LoanBudget.filter(
            budget => budget.Crop_Practice_ID == cp.Crop_Practice_ID
          );

          cpLoanBudget
            .filter(p => p.Crop_Practice_ID != 0)
            .forEach(budget => {
              budget.ARM_Budget_Crop = budget.ARM_Budget_Acre * cp.LCP_Acres;

              budget.Distributor_Budget_Crop =
                budget.Distributor_Budget_Acre * cp.LCP_Acres;

              budget.Third_Party_Budget_Crop =
                budget.Third_Party_Budget_Acre * cp.LCP_Acres;

              if (budget.Expense_Type_ID != 10) {
                budget.Total_Budget_Crop_ET =
                  budget.Total_Budget_Acre * cp.LCP_Acres;
              } else {
                budget.Total_Budget_Crop_ET = budget.Third_Party_Budget_Crop;
              }
            });
        });

        let Arm_Budget_Totals = precise_round(
          _.sumBy(
            localloanobj.LoanBudget.filter(
              p => p.Crop_Practice_ID != 0 || p.Budget_Type == 'F'
            ),
            p => parseFloat(p.ARM_Budget_Crop.toString())
          ),
          0
        );

        let Dist_Budget_Totals = precise_round(
          _.sumBy(
            localloanobj.LoanBudget.filter(
              p => p.Crop_Practice_ID != 0 || p.Budget_Type == 'F'
            ),
            p => parseFloat(p.Distributor_Budget_Crop.toString())
          ),
          0
        );

        let ThirdParty_Budget_Totals = precise_round(
          _.sumBy(
            localloanobj.LoanBudget.filter(
              p => p.Crop_Practice_ID != 0 || p.Budget_Type == 'F'
            ),
            bgt =>
              isNaN(bgt.Third_Party_Budget_Crop)
                ? 0
                : bgt.Third_Party_Budget_Crop
          ),
          0
        );

        //Interest Rate Calulations //
        localloanobj.LoanMaster.FC_Ins_Disc_Collateral_Value = 0;
        localloanobj.LoanMaster.FC_Total_Addt_Collateral_Value = 0;

        localloanobj.LoanMaster.Origination_Fee_Amount = Math.round(
          ((Arm_Budget_Totals + Dist_Budget_Totals) *
            localloanobj.LoanMaster.Origination_Fee_Percent) /
            100
        );

        localloanobj.LoanMaster.Service_Fee_Amount = Math.round(
          ((Arm_Budget_Totals + Dist_Budget_Totals) *
            localloanobj.LoanMaster.Service_Fee_Percent) /
            100
        );

        let util_perc =
          this.optimizercaluclations.getUtilizationPercent() / 100;
        localloanobj.LoanMaster.ARM_Rate_Fee = precise_round(
          (localloanobj.LoanMaster.Interest_Percent / 100) *
            (localloanobj.LoanMaster.Estimated_Days / 365) *
            Arm_Budget_Totals *
            util_perc,
          0
        );

        localloanobj.LoanMaster.Dist_Rate_Fee = precise_round(
          (localloanobj.LoanMaster.Interest_Percent / 100) *
            (localloanobj.LoanMaster.Estimated_Days / 365) *
            Dist_Budget_Totals *
            util_perc,
          0
        );

        localloanobj.LoanMaster.Interest_Est_Amount = Math.round(
          localloanobj.LoanMaster.ARM_Rate_Fee +
            localloanobj.LoanMaster.Dist_Rate_Fee
        );

        localloanobj.LoanMaster.Total_ARM_Fees_And_Interest = precise_round(
          localloanobj.LoanMaster.Origination_Fee_Amount +
            localloanobj.LoanMaster.Service_Fee_Amount +
            localloanobj.LoanMaster.ARM_Rate_Fee,
          0
        );

        localloanobj.LoanMaster.Total_Return_Fee =
          localloanobj.LoanMaster.Origination_Fee_Amount +
          localloanobj.LoanMaster.Service_Fee_Amount +
          localloanobj.LoanMaster.Interest_Est_Amount; //Extension Fee should be added

        // update return scale
        // this.termcalculation.updateReturnScale(localloanobj);

        localloanobj = this.budgetCalculation.prepareFeesBudget(localloanobj);

        // propogations to Loan master table
        localloanobj.LoanMaster.Arm_Total_Budget = Arm_Budget_Totals;
        localloanobj.LoanMaster.Dist_Total_Budget = Dist_Budget_Totals;
        localloanobj.LoanMaster.Third_Party_Total_Budget = ThirdParty_Budget_Totals;

        localloanobj.LoanMaster.Loan_Total_Budget = Math.round(
          Arm_Budget_Totals + Dist_Budget_Totals + ThirdParty_Budget_Totals
        );

        localloanobj.LoanMaster.ARM_Commitment = Math.round(Arm_Budget_Totals);
        localloanobj.LoanMaster.Dist_Commitment = Dist_Budget_Totals;

        localloanobj.LoanMaster.Total_Commitment = Math.round(
          localloanobj.LoanMaster.ARM_Commitment +
            localloanobj.LoanMaster.Dist_Commitment
        );

        localloanobj.LoanMaster.ActionStatus = 2;
      }
    } catch (e) {
      //level 1 log as it is error
      this.logging.checkandcreatelog(1, 'Calc_Budget', e);
      if (environment.isDebugModeActive)
        console.error('ERROR IN BUDGET CALCULATION' + JSON.stringify(e));
    }

    //LOAN BUDGET TOTAL CALCULATIONS
    this.budgethelper.prepareTotalBudget(localloanobj);

    //localloanobj.LoanBudget = localloanobj.LoanBudget;
    // COLLATERAL CALCULATIONS

    if (localloanobj.LoanCollateral != null && localloanobj.LoanCollateral) {
      localloanobj = this.collateralcalculation.preparecollateralmodel(
        localloanobj
      );
      localloanobj = this.collateralcalculation.performMarketValueCalculations(
        localloanobj
      );
    }

    // QUESTIONS CALCULATIONS
    if (localloanobj.LoanQResponse != null) {
      localloanobj = this.questionscalculations.performcalculationforquestionsupdated(
        localloanobj
      );
    }

    // MASTER CALCULATIONS
    if (localloanobj.LoanMaster !== null) {
      let loanMaster = localloanobj.LoanMaster;

      let NetCropRevenue = loanMaster.Net_Market_Value_Crops
        ? parseInt(loanMaster.Net_Market_Value_Crops.toFixed(0))
        : 0;

      let Total_Additional_Revenue =
        loanMaster.Net_Market_Value_Livestock +
        loanMaster.Net_Market_Value_FSA +
        loanMaster.Net_Market_Value_Other +
        loanMaster.Net_Market_Value_Stored_Crops;

      loanMaster.Total_Revenue = precise_round(
        NetCropRevenue + Total_Additional_Revenue,
        0
      );

      localloanobj = this.loanMasterCalcualtions.performDashboardCaclulation(
        localloanobj
      );
      localloanobj = this.overallCalculationService.balancesheet_calc(
        localloanobj
      );
    }

    // OPTIMIZER CALCULATIONS
    localloanobj = this.optimizercaluclations.performLoanUnitCalculations(
      localloanobj
    );

    // Marketing Contract Calculations
    if (localloanobj.LoanMarketingContracts && localloanobj.LoanCrops) {
      this.marketingContractService.updateMarketingCalculation(localloanobj);
      this.marketingContractService.performPriceCalculation(localloanobj);
    }

    //SET LOAN TYPE
    localloanobj.LoanMaster.Loan_Type_Code = this.loanTypecalculation.calculateLoanType(
      localloanobj
    );

    console.timeEnd('Loan Calculation');

    return localloanobj;
  }
}
