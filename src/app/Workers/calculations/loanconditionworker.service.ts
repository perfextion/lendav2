import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import {
  Loan_Condition,
  Loan_Association,
  AssociationTypeCode,
  Loan_Collateral,
  loan_model,
  BorrowerEntityType,
  borrower_model
} from '@lenda/models/loanmodel';
import { Loan_Farm } from '@lenda/models/farmmodel.';
import {
  lookupCountyValueFromPassedRefData,
  lookupStateNameFromPassedRefObject
} from '../utility/aggrid/stateandcountyboxes';

import * as _ from 'lodash';
import { environment } from '@env/environment.prod';
import { Insurance_Policy } from '@lenda/models/insurancemodel';
import { ReferenceService } from '@lenda/services/reference/reference.service';
import { RefCondition } from '@lenda/models/loan-response.model';
import { LoanMaster } from '@lenda/models/ref-data-model';

@Injectable()
export class LoanConditionWorkerService {
  private refData: any;

  constructor(
    private localStorage: LocalStorageService,
    private refService: ReferenceService
  ) {
    this.refData = this.localStorage.retrieve(environment.referencedatakey);

    if (!this.refData) {
      this.refService.getreferencedata().subscribe(res => {
        this.refData = res.ResCode === 1 ? res.Data : {};
      });
    }
  }

  prepareCalculatedLoanConditions(loan_model: loan_model, userId: number) {
    if (environment.isDebugModeActive)
      console.time('prepareCalculatedLoanConditions');

    let loanConditionsWithKey = loan_model.LoanConditions.filter(
      c => !!c.Condition_Key
    );
    let loanConditionsWithoutKey = loan_model.LoanConditions.filter(
      c => !c.Condition_Key
    );

    let calculatedConditions = this.calculateConditions(loan_model, userId);

    loanConditionsWithKey.forEach(lc => {
      let condition = calculatedConditions.find(
        c => c.Condition_Key == lc.Condition_Key
      );

      if (!condition) {
        lc.Action_Status = 3;
      } else {
        condition.Action_Status = 0;
        condition.Suggestion_Only_Ind = lc.Suggestion_Only_Ind;
        condition.Loan_Condition_ID = lc.Loan_Condition_ID;

        condition.Requested_Date_Time = lc.Requested_Date_Time;
        condition.KL_Document_ID = lc.KL_Document_ID;
        condition.Upload_User_ID = lc.Upload_User_ID;
        condition.Uploaded_Date_Time = lc.Uploaded_Date_Time;

        if (condition.Loan_Condition_ID == 0) {
          condition.Action_Status = 1;
        }
      }
    });

    let finalLoanConditions = this.merge_array(
      loanConditionsWithKey,
      calculatedConditions
    );
    finalLoanConditions = [...loanConditionsWithoutKey, ...finalLoanConditions];

    finalLoanConditions = _.sortBy(finalLoanConditions, 'Sort_Order');

    if (environment.isDebugModeActive)
      console.timeEnd('prepareCalculatedLoanConditions');

    return finalLoanConditions;
  }

  preparePolicyVerificationConditions(loan_model: loan_model, userId: number) {
    if (environment.isDebugModeActive)
      console.time('preparePolicyVerificationConditions');

    let loanConditionsWithKey = loan_model.LoanConditions.filter(
      c => !!c.Condition_Key
    );

    let loanConditionsWithoutKey = loan_model.LoanConditions.filter(
      c => !c.Condition_Key
    );

    let calculatedConditions = this.prepareLoanConditionForInsurancePolicies(
      loan_model,
      loan_model.Loan_Full_ID,
      userId
    );

    loanConditionsWithKey.forEach(lc => {
      let condition = calculatedConditions.find(
        c => c.Condition_Key == lc.Condition_Key
      );

      if (condition) {
        condition.Action_Status = 0;
        condition.Suggestion_Only_Ind = lc.Suggestion_Only_Ind;
        condition.Loan_Condition_ID = lc.Loan_Condition_ID;

        condition.Requested_Date_Time = lc.Requested_Date_Time;
        condition.KL_Document_ID = lc.KL_Document_ID;
        condition.Upload_User_ID = lc.Upload_User_ID;
        condition.Uploaded_Date_Time = lc.Uploaded_Date_Time;

        if (condition.Loan_Condition_ID == 0) {
          condition.Action_Status = 1;
        }
      }
    });

    let finalLoanConditions = this.merge_array(
      loanConditionsWithKey,
      calculatedConditions
    );
    finalLoanConditions = [...loanConditionsWithoutKey, ...finalLoanConditions];

    if (environment.isDebugModeActive)
      console.timeEnd('preparePolicyVerificationConditions');

    return finalLoanConditions;
  }

  prepare_WFRP_Condition(loan_model: loan_model, userId: any) {
    try {
      let wfrpCondition = this.prepareWFRPCondition(loan_model, userId);
      let index = loan_model.LoanConditions.findIndex(
        a => a.Condition_Type_ID == 48
      );

      if (wfrpCondition) {
        if (index == -1) {
          loan_model.LoanConditions = [
            ...loan_model.LoanConditions,
            wfrpCondition
          ];
        }
      } else {
        if (index > -1) {
          let condition = loan_model.LoanConditions.find(
            a => a.Condition_Type_ID == 48
          );

          if (condition.Action_Status == 1) {
            loan_model.LoanConditions.splice(index, 1);
          } else {
            condition.Action_Status = 3;
          }
        }
      }

      return loan_model;
    } catch {
      return loan_model;
    }
  }

  private calculateConditions(loan_model: loan_model, userId: number) {
    return [
      ...this.prepareLevel2Conditions(
        loan_model,
        loan_model.Loan_Full_ID,
        userId
      ),
      ...this.prepareLevel3Conditions(
        loan_model,
        loan_model.Loan_Full_ID,
        userId
      )
    ];
  }

  private merge_array(
    array1: Array<Loan_Condition>,
    array2: Array<Loan_Condition>
  ) {
    const result_array: Array<Loan_Condition> = [];
    const arr = array1.concat(array2);

    let len = arr.length;
    const assoc = {};

    while (len--) {
      const item = arr[len];

      if (!assoc[item.Condition_Key]) {
        result_array.unshift(item);
        assoc[item.Condition_Key] = true;
      }
    }

    return result_array;
  }

  //#region prepareUnderwriting

  // Condition Id = 48
  private prepareWFRPCondition(loan: loan_model, userId: number) {
    try {
      let isWFRPEnabled = loan.Loanwfrp.Wfrp_Enabled;
      let approvedRevenue = loan.Loanwfrp.Approved_Revenue;

      if (isWFRPEnabled && approvedRevenue) {
        let ref_condition = this.getCondition(48);

        let condition = this.createLoanCondition(
          ref_condition.Condition_Name,
          ref_condition.Condition_ID,
          loan.Loan_Full_ID,
          userId,
          '',
          1
        );

        return condition;
      }
    } catch {
      return null;
    }
  }

  //#endregion prepareUnderwriting

  //#region  prepareClosingConditions

  private prepareLevel3Conditions(
    loan_model: loan_model,
    loanId: string,
    userId: number
  ) {
    let conditions: Array<Loan_Condition> = [];

    const policiesWithHMAX = loan_model.InsurancePolicies.filter(policy => {
      return (
        policy.ActionStatus != 3 &&
        policy.Subpolicies.some(p => p.Ins_Type === 'HMAX')
      );
    });

    // policiesWithHMAX.forEach(p => {
    //   conditions.push(this.insurancePolicyCondition(p, loanId, userId));
    // });

    const farms = loan_model.Farms.filter(f => f.ActionStatus != 3);
    const uniqueStates = _.uniqBy(farms, f => f.Farm_State_ID);

    uniqueStates.forEach(farm => {
      conditions.push(this.farmExistsStateCondition(farm, loanId, userId));
    });

    const uniqueCounties = _.uniqBy(farms, f => f.Farm_County_ID);

    uniqueCounties.forEach(farm => {
      conditions.push(this.farmExistsCountyCondition(farm, loanId, userId));
    });

    const rebators = loan_model.Association.filter(
      a =>
        a.ActionStatus != 3 && a.Assoc_Type_Code == AssociationTypeCode.Rebator
    );

    rebators.forEach(reb => {
      conditions.push(this.rebatorExistsCondition(reb, loanId, userId));
    });

    const buyers = loan_model.Association.filter(
      b => b.ActionStatus != 3 && b.Assoc_Type_Code == AssociationTypeCode.Buyer
    );

    buyers.forEach(buyer => {
      conditions.push(this.bookingExistsCondition(buyer, loanId, userId));
    });

    return conditions;
  }

  // Applicant is Buisness
  private applicantIsBusiness(
    borrower: LoanMaster,
    loanId: string,
    userId: number
  ) {
    if (
      borrower.Borrower_Entity_Type_Code == BorrowerEntityType.Corporation ||
      borrower.Borrower_Entity_Type_Code == BorrowerEntityType.LLC ||
      borrower.Borrower_Entity_Type_Code == BorrowerEntityType.Partner
    ) {
      let condition_name = this.getCondition(34).Condition_Name.replace(
        '{ Name}',
        borrower.Borrower_First_Name
      );

      let key = `34_${borrower.Borrower_ID}`;

      return this.createLoanCondition(
        condition_name,
        34,
        loanId,
        userId,
        key,
        Condition_Level.Level3
      );
    }
  }

  // InsurancePolicy with AIP
  // Insurance Policy with HMAX
  private insurancePolicyCondition(
    policy: Insurance_Policy,
    loanId: string,
    userId: number
  ) {
    return new Loan_Condition();
  }

  // Farms exist - County
  private farmExistsCountyCondition(
    farm: Loan_Farm,
    loanId: string,
    userId: number
  ) {
    let stateName = lookupStateNameFromPassedRefObject(
      farm.Farm_State_ID,
      this.refData
    );
    let countyName = lookupCountyValueFromPassedRefData(
      farm.Farm_County_ID,
      this.refData
    );

    const condition_name = this.getCondition(39)
      .Condition_Name.replace('{State}', stateName)
      .replace('{County}', countyName);

    let key = `39_${farm.Farm_State_ID}_${farm.Farm_County_ID}`;

    return this.createLoanCondition(
      condition_name,
      39,
      loanId,
      userId,
      key,
      Condition_Level.Level3
    );
  }

  // LoanType == Ag-Input and Applicant is Business

  // Rebate exists
  private rebatorExistsCondition(
    rebator: Loan_Association,
    loanId: string,
    userId: number
  ) {
    const condition_name = this.getCondition(45).Condition_Name.replace(
      '{Name}',
      rebator.Assoc_Name
    );

    let key = `45_${rebator.Assoc_ID}`;

    return this.createLoanCondition(
      condition_name,
      45,
      loanId,
      userId,
      key,
      Condition_Level.Level3
    );
  }

  // Booking exists
  private bookingExistsCondition(
    buyer: Loan_Association,
    loanId: string,
    userId: number
  ) {
    const condition_name = this.getCondition(46).Condition_Name.replace(
      '{Name}',
      buyer.Assoc_Name
    );

    let key = `46_${buyer.Assoc_ID}`;

    return this.createLoanCondition(
      condition_name,
      46,
      loanId,
      userId,
      key,
      Condition_Level.Level2
    );
  }

  // Farms exist - State
  private farmExistsStateCondition(
    farm: Loan_Farm,
    loanId: string,
    userId: number
  ) {
    let stateName = lookupStateNameFromPassedRefObject(
      farm.Farm_State_ID,
      this.refData
    );

    const condition_name = this.getCondition(47).Condition_Name.replace(
      '{State}',
      stateName
    );

    let key = `47_${farm.Farm_State_ID}`;

    return this.createLoanCondition(
      condition_name,
      47,
      loanId,
      userId,
      key,
      Condition_Level.Level2
    );
  }

  //#endregion prepareClosingConditions

  //#region preparePreReqConditions

  private prepareLevel2Conditions(
    loan_model: loan_model,
    loanId: string,
    userId: number
  ) {
    let conditions: Array<Loan_Condition> = [];

    conditions.push(
      ...this.prepareLoanConditionForInsurancePolicies(
        loan_model,
        loanId,
        userId
      )
    );

    conditions.push(
      ...this.prepareConditionsForBuyerAndHarvester(
        loan_model.Association,
        loanId,
        userId
      )
    );

    conditions.push(
      ...this.prepareLoanConditionForCollateral(
        loan_model.LoanCollateral,
        userId,
        loanId
      )
    );

    conditions.push(
      ...this.prepareConditionForFarm(loan_model.Farms, loanId, userId)
    );

    conditions.push(
      ...this.prepareConditionsForThirdParty(
        loan_model.Association,
        loanId,
        userId
      )
    );

    return conditions;
  }

  private prepareConditionsForBuyerAndHarvester(
    associations: Loan_Association[],
    loanId: string,
    userId: number
  ) {
    let conditions: Array<Loan_Condition> = [];

    associations.forEach(buyer => {
      if (
        buyer.Assoc_Type_Code === AssociationTypeCode.Buyer &&
        buyer.ActionStatus != 3
      ) {
        conditions.push(this.addLoanConditionForBuyer(buyer, userId, loanId));
      }
    });

    associations.forEach(harvester => {
      if (
        harvester.Assoc_Type_Code == AssociationTypeCode.Harvester &&
        harvester.ActionStatus != 3
      ) {
        conditions.push(
          this.addLoanConditionForHarvester(harvester, userId, loanId)
        );
      }
    });

    return conditions;
  }

  private prepareConditionsForThirdParty(
    associations: Loan_Association[],
    loanId: string,
    userId: number
  ) {
    let conditions: Array<Loan_Condition> = [];

    associations.forEach(thirdParty => {
      if (
        thirdParty.Assoc_Type_Code == AssociationTypeCode.ThirdParty &&
        thirdParty.ActionStatus != 3
      ) {
        conditions.push(
          this.addLoanConditionForThirdPartyCreditor(thirdParty, userId, loanId)
        );
      }
    });

    return conditions;
  }

  private prepareConditionForFarm(
    farms: Array<Loan_Farm>,
    loanId: string,
    userId: number
  ) {
    let conditions: Array<Loan_Condition> = [];

    farms
      .filter(a => a.ActionStatus != 3)
      .forEach(farm => {
        const countyName = lookupCountyValueFromPassedRefData(
          farm.Farm_County_ID,
          this.refData
        );
        const stateName = lookupStateNameFromPassedRefObject(
          farm.Farm_State_ID,
          this.refData
        );

      if (farm.Owned) {
          conditions.push(
            this.conditionForFarmOwned(
              farm,
              userId,
              loanId,
              countyName,
              stateName
            )
          );
      }

      if (!farm.Owned) {
          conditions.push(
            this.conditionForFarmOwned(
              farm,
              userId,
              loanId,
              countyName,
              stateName
            )
          );

        if (
          farm.Cash_Rent_Paid ==
          farm.Cash_Rent_Total - farm.Cash_Rent_Waived_Amount
        ) {
            conditions.push(
              this.conditionForFarmRentPaid(
                farm,
                userId,
                loanId,
                countyName,
                stateName
              )
            );
        }

          if (farm.Cash_Rent_Total == 0 || farm.Cash_Rent_Waived_Amount > 0) {
            conditions.push(
              this.conditionForFarmRent(
                farm,
                userId,
                loanId,
                countyName,
                stateName
              )
            );
        }
      }

      if (farm.Permission_To_Insure) {
        conditions.push(
            this.conditionForFarmPermissionToInsure(
              farm,
              userId,
              loanId,
              countyName,
              stateName
            )
        );
      }
    });

    return conditions;
  }

  // Insurance Policy with Assigned Agent
  public prepareLoanConditionForInsurancePolicies(
    loan_model: loan_model,
    loanId: string,
    userId: number
  ) {
    let conditions: Array<Loan_Condition> = [];

    const policies = loan_model.LoanPolicies.filter(
      a => a.ActionStatus != 3 && a.Select_Ind
    );

    loan_model.Association.filter(
      a =>
        a.ActionStatus != 3 && a.Assoc_Type_Code == AssociationTypeCode.Agency
    ).forEach(agency => {
      let AssignedPolicies = policies.filter(
        a => a.Agency_ID == agency.Assoc_ID
      );
      let uniquePolicies = _.uniqBy(AssignedPolicies, p => p.County_ID);

      uniquePolicies.forEach(policy => {
        const countyName: string = lookupCountyValueFromPassedRefData(
          policy.County_ID,
          this.refData
        );

          try {
            let condition = this.getCondition(11)
            .Condition_Name.replace('{Agent.Name}', agency.Assoc_Name)
              .replace('{County}', countyName);

          let key = `11_${agency.Assoc_ID}_${policy.County_ID}`;

            conditions.push(
              this.createLoanCondition(condition, 11, loanId, userId, key)
            );
          } catch (exception) { }

          try {
            const condition = this.getCondition(18)
            .Condition_Name.replace('{Agent.Name}', agency.Assoc_Name)
              .replace('{County}', countyName);

          const key = `18_${agency.Assoc_ID}_${policy.County_ID}`;

            conditions.push(
              this.createLoanCondition(condition, 18, loanId, userId, key)
            );
          } catch (e) { }
        });
      });

    return conditions;
  }

  private prepareLoanConditionForCollateral(
    loanCollaterals: Loan_Collateral[],
    userId: number,
    loanId: string
  ) {
    let conditions: Array<Loan_Condition> = [];

    loanCollaterals = loanCollaterals.filter(a => a.ActionStatus != 3);

    try {
      loanCollaterals
        .filter(c => c.Collateral_Category_Code === 'FSA')
        .forEach(col => {
          conditions.push(
            this.addLoanConditionForCollateral(col, userId, loanId)
          );
        });
    } catch (e) {
      console.log(e);
    }

    try {
      loanCollaterals
        .filter(col => col.Prior_Lien_Amount > 0)
        .forEach(col => {
        conditions.push(
          this.addLoanConditionForCollateralPriorLien(col, userId, loanId)
        );
      });
    } catch (e) {
      console.log(e);
    }

    return conditions;
  }

  // CollateralItem.Type == FSA
  private addLoanConditionForCollateral(
    col: Loan_Collateral,
    userId: number,
    loanId: string
  ) {
    let condition_name = this.getCondition(14).Condition_Name.replace(
      '{Name}',
      col.Collateral_Description
    );

    let key = `14_${col.Collateral_ID}`;

    return this.createLoanCondition(condition_name, 14, loanId, userId, key);
  }

  // CollateralItem.PriorLien > 0
  private addLoanConditionForCollateralPriorLien(
    col: Loan_Collateral,
    userId: number,
    loanId: string
  ) {
    let condition_name = this.getCondition(24).Condition_Name.replace(
      '{Name}',
      col.Collateral_Description
    );

    let key = `24_${col.Collateral_ID}`;

    return this.createLoanCondition(condition_name, 24, loanId, userId, key);
  }

  // ThirdPartyCreditor exists

  private addLoanConditionForThirdPartyCreditor(
    third_party: Loan_Association,
    userId: number,
    loanId: string
  ) {
    let condition_name = this.getCondition(27).Condition_Name.replace(
      '{Name}',
      third_party.Assoc_Name
    );

    let key = `27_${third_party.Assoc_ID}`;

    return this.createLoanCondition(condition_name, 27, loanId, userId, key);
  }

  // Booking exists
  private addLoanConditionForBuyer(
    buyer: Loan_Association,
    userId: number,
    loanId: string
  ) {
    let condition_name = this.getCondition(16).Condition_Name.replace(
      '{Name}',
      buyer.Assoc_Name
    );

    let key = `16_${buyer.Assoc_ID}`;

    return this.createLoanCondition(condition_name, 16, loanId, userId, key);
  }

  // Harvester exists
  private addLoanConditionForHarvester(
    harvester: Loan_Association,
    userId: number,
    loanId: string
  ) {
    let condition_name = this.getCondition(17).Condition_Name.replace(
      '{Name}',
      harvester.Assoc_Name
    );

    let key = `17_${harvester.Assoc_ID}`;

    return this.createLoanCondition(condition_name, 17, loanId, userId, key);
  }

  private conditionForFarmOwned(
    farm: Loan_Farm,
    userId: number,
    loanId: string,
    countyName: string,
    stateName: string
  ) {
    // Farm.Owned == true
    // Farm.Owned == false
    if (farm.Owned) {
      const condition_name = this.getCondition(19)
        .Condition_Name.replace('{State}', stateName)
        .replace('{County}', countyName)
        .replace('{FSN}', farm.FSN);

      const key = `19_${farm.Farm_State_ID}_${farm.Farm_County_ID}_${farm.FSN}`;

      return this.createLoanCondition(condition_name, 19, loanId, userId, key);
    } else {
      const condition_name = this.getCondition(20)
        .Condition_Name.replace('{Owner}', farm.Landowner)
        .replace('{FSN}', farm.FSN);

      const key = `20_${farm.Landowner}_${farm.FSN}`;

      return this.createLoanCondition(condition_name, 20, loanId, userId, key);
    }
  }

  // Farm.Paid == true
  private conditionForFarmRentPaid(
    farm: Loan_Farm,
    userId: number,
    loanId: string,
    countyName: string,
    stateName: string
  ) {
    const condition_name = this.getCondition(21)
      .Condition_Name.replace('{State}', stateName)
      .replace('{County}', countyName)
      .replace('{FSN}', farm.FSN);

    const key = `21_${farm.Farm_State_ID}_${farm.Farm_County_ID}_${farm.FSN}`;

    return this.createLoanCondition(condition_name, 21, loanId, userId, key);
  }

  // Farm.PermissionToInsure == true
  private conditionForFarmPermissionToInsure(
    farm: Loan_Farm,
    userId: number,
    loanId: string,
    countyName: string,
    stateName: string
  ) {
    const condition_name = this.getCondition(23)
      .Condition_Name.replace('{State}', stateName)
      .replace('{County}', countyName)
      .replace('{FSN}', farm.FSN);

    const key = `23_${farm.Farm_State_ID}_${farm.Farm_County_ID}_${farm.FSN}`;

    return this.createLoanCondition(condition_name, 23, loanId, userId, key);
  }

  // Farm.Rent == 0 || Farm.RentWaived > 0
  private conditionForFarmRent(
    farm: Loan_Farm,
    userId: number,
    loanId: string,
    countyName: string,
    stateName: string
  ) {
    const condition_name = this.getCondition(25)
      .Condition_Name.replace('{State}', stateName)
      .replace('{County}', countyName)
      .replace('{FSN}', farm.FSN);

    const key = `25_${farm.Farm_State_ID}_${farm.Farm_County_ID}_${farm.FSN}`;

    const c = this.createLoanCondition(condition_name, 25, loanId, userId, key);

    return c;
  }

  // Lienholder.Disposistion == Termination
  // Lienholder.Disposistion == Subordination
  private addLoanConditionForLienholderDisposistion(lieh: Loan_Association) {
    if (lieh.Assoc_Type_Code == AssociationTypeCode.LienHolder) {
      if (4 > 7) {
        // Lienholder.Disposistion == Termination
        return new Loan_Condition();
      }
      if (4 >= 7) {
        // Lienholder.Disposistion == Subordination
        return new Loan_Condition();
      }
    }
    return null;
  }

  //#endregion preparePreReqConditions

  /**
   * `Create new Loan Condition`
   */
  public createLoanCondition(
    condition_name: string,
    conditionId: number,
    loanId: string,
    userId: number,
    key?: string,
    level = Condition_Level.Level2
  ) {
    const seq = loanId.split('-')[1];

    return <Loan_Condition>{
      Action_Status: 1,
      Association_ID: null,
      Condition_Name: condition_name,
      Condition_Type_ID: conditionId,
      Loan_Condition_ID: 0,
      Document_ID: this.getDocId(conditionId),
      Loan_Full_ID: loanId,
      Loan_Seq_Num: Number(seq || 0),
      Other_Description: '',
      Status: 0,
      Suggestion_Only_Ind: 0,
      User_ID: userId,
      Condition_Level: level,
      Date_Time: new Date().toISOString(),
      Condition_Key: key,
      Sort_Order: this.getSortOrder(conditionId)
    };
  }

  private getCondition(conditionId: number): RefCondition {
    return this.refData.RefConditions.find(c => c.Condition_ID == conditionId);
  }

  private getDocId(conditionId: number): number {
    if (conditionId > 0) {
      const condition = this.getCondition(conditionId);

      if (!!condition) {
        return condition.Document_ID;
      }
      return 0;
    }
    return 0;
  }

  private getSortOrder(conditionId: number) {
    const condition = this.getCondition(conditionId);

    if (!!condition) {
      return condition.Sort_Order;
    }
    return conditionId;
  }
}

export enum Condition_Level {
  Level1 = 1, // Underwriting
  Level2 = 2, // Pre Requisite
  Level3 = 3, // Closing Document
  Level4 = 4 // Crop Monitoring
}
