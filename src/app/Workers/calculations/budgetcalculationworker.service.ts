import { Injectable } from '@angular/core';

import { loan_model, Loan_Budget } from '../../models/loanmodel';

import { LoggingService } from '../../services/Logs/logging.service';
import { ExceptionWorkerService } from './exceptionworker.service';
import { Local_Table } from '@lenda/services/exception-helper.service';
import { MigratedLoanAdjustmentHelper } from '../utility/migrated-loan-adjustments-helper';
import { MigratedLoanAdjustmentKeys } from '@lenda/models/ref-data-model';

@Injectable()
export class BudgetcalculationworkerService {
  constructor(
    private logging: LoggingService,
    private exceptionWorker: ExceptionWorkerService
  ) {}

  prepareCashrentBudgetModel(input: loan_model): loan_model {
    try {
      let starttime = new Date().getTime();

      let rentBgt = input.LoanBudget.find(
        bgt =>
          bgt.Budget_Type == 'F' &&
          bgt.Expense_Type_ID == 15 &&
          bgt.ActionStatus != 3
      );

      if (rentBgt) {
        rentBgt.ARM_Budget_Crop = 0;
        rentBgt.Third_Party_Budget_Crop = 0;

        for (let farm of input.Farms) {
          if (!farm.Owned) {
            if(farm.Cash_Rent_Paid == 1) {
              rentBgt.Third_Party_Budget_Crop +=  farm.Cash_Rent_Total;
            } else {
              rentBgt.ARM_Budget_Crop +=  farm.Cash_Rent_Total - farm.Cash_Rent_Waived_Amount;
              rentBgt.Third_Party_Budget_Crop += farm.Cash_Rent_Waived_Amount;
            }
          }
        }

        // let adj_value = MigratedLoanAdjustmentHelper.Get_Adjustment_Value(input, MigratedLoanAdjustmentKeys.Cash_Rent_Budget);
        // rentBgt.ARM_Budget_Crop += adj_value;
      }

      let endtime = new Date().getTime();
      //level 2 log
      this.logging.checkandcreatelog(
        2,
        'Calc_LoanFixedBudget',
        'LoanCalculation timetaken :' + (endtime - starttime).toString() + ' ms'
      );

      return input;
    } catch (e) {
      //level 1 log as its error
      this.logging.checkandcreatelog(1, 'Calc_LoanFixedBudget', e);
      return input;
    }
  }

  /**
   * Update Fees budget
   * @param input Loan Object
   */
  prepareFeesBudget(input: loan_model) {
    try {

      let Fees_Budget = input.LoanBudget.find(
        a =>
          a.ActionStatus != 3 &&
          a.Expense_Type_ID == 14 &&
          a.Budget_Type == 'F'
      );

      if(Fees_Budget) {
        Fees_Budget.ARM_Budget_Acre = 0;
        Fees_Budget.Distributor_Budget_Acre = 0;
        Fees_Budget.Third_Party_Budget_Acre = 0;
        Fees_Budget.Third_Party_Budget_Crop = 0;

        Fees_Budget.ARM_Budget_Crop = input.LoanMaster.Origination_Fee_Amount + input.LoanMaster.Service_Fee_Amount;
        Fees_Budget.Distributor_Budget_Crop = 0;
        Fees_Budget.Total_Budget_Crop_ET = input.LoanMaster.Origination_Fee_Amount + input.LoanMaster.Service_Fee_Amount;
      }

      return input;

    } catch(ex) {
      //level 1 log as its error
      this.logging.checkandcreatelog(1, 'prepareFeesBudget', JSON.stringify(ex));
      return input;
    }
  }

  processExpenseTypeExceptions(input: loan_model, firstTimeCalculation = false) {
    try {
      if(input && input.Farms && input.CropYield && input.LoanBudget) {
        const srccomponentedit = input.srccomponentedit || '';
        if(
          srccomponentedit.toLowerCase().includes('budget') ||
          srccomponentedit.toLowerCase().includes('optimizer') ||
          firstTimeCalculation
        ) {
          let total_budgets = input.LoanBudget.filter(
            a =>
              a.Budget_Type == 'V' &&
              a.Crop_Practice_ID == 0 &&
              a.ActionStatus != 3 &&
              a.Validation_Exception_Ind
          );

          if(total_budgets && total_budgets.length > 0) {
            total_budgets.forEach(budget => {
              if(budget.Exception_ID) {
                this.processException(budget, input);
              }

              if(budget.Validation_ID) {
                this.processValidation(budget, input);
              }
            });
          }
        }

        return input;
      }
    } catch (ex) {
      this.logging.checkandcreatelog(1, 'processExpenseTypeExceptions', JSON.stringify(ex));
      return input;
    }
  }

  private processException(budget: Loan_Budget, input: loan_model) {
    if (budget.Total_Budget_Crop_ET > 0) {
      this.exceptionWorker.Process_Exception_By_ID(input, budget.Exception_ID, budget, 1, Local_Table.LoanBudget);
    } else {
      this.exceptionWorker.Process_Exception_By_ID(input, budget.Exception_ID, budget, null, Local_Table.LoanBudget);
    }
  }

  private processValidation(budget: Loan_Budget, input: loan_model) {
    // TODO
  }
}
