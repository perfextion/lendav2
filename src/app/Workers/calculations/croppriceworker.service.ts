import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { DataService } from '@lenda/services/data.service';
import { RefDataModel, RefCrop, Ref_Crop_Basis } from '@lenda/models/ref-data-model';
import { environment } from '@env/environment';
import { loan_model, Loan_Crop, LoanStatus } from '@lenda/models/loanmodel';
import * as _ from 'lodash';
import { lookupUOM } from '../utility/aggrid/cropboxes';
import { Get_Crop_Basis, Get_Crop_Price } from '@lenda/services/common-utils';
import { Helper } from '@lenda/services/math.helper';
import { LoanAuditTrailService } from '@lenda/services/audit-trail.service';
import { Audit_Item } from '@lenda/models/audit-trail/audit-trail';

@Injectable()
export class CropPriceWorkerService {
  private _refdata: RefDataModel;

  constructor(
    private localstorage: LocalStorageService,
    private dataservice: DataService,
    private auditTrail: LoanAuditTrailService
  ) {
    this._refdata = this.dataservice.getRefData();

    if (!this._refdata) {
      this._refdata = this.localstorage.retrieve(environment.referencedatakey);
    }
  }

  generateCropPrice(loan: loan_model) {
    try {
      this._refdata = this.localstorage.retrieve(environment.referencedatakey);

      if(loan.LoanMaster.Loan_Status == LoanStatus.Working) {
        let yields = loan.CropYield.filter(
          (a: any) => !!a.Crop_Code && a.ActionStatus != 3
        );

        loan.LoanCrops.filter(a => a.ActionStatus != 3).forEach(a => {
          a.Needed_Ind = 0;
        });

        let uniquieCrops: any[] = _.uniqWith(
          yields,
          (x, y) =>
            y['Crop_Code'] == x['Crop_Code'] && y['cropType'] == x['cropType']
        );

        let neededRows = [];
        let auditTrails = [];

        for (let crop of uniquieCrops) {
          let ExistingLoanCropRow = loan.LoanCrops.filter(
            a => a.ActionStatus != 3
          ).find(
            a =>
              a.Crop_Code == crop.Crop_Code && a.Crop_Type_Code == crop.cropType
          );

          if (!ExistingLoanCropRow) {
            let ref_Crop = Get_Crop_Price(
              crop.Crop_Code,
              crop.cropType,
              loan.LoanMaster.Region_ID,
              this._refdata
            );

            let uom = lookupUOM(crop, this._refdata);

            let ref_Crop_basis = Get_Crop_Basis(
              ref_Crop.Crop_Code,
              ref_Crop.Crop_Type,
              loan.LoanMaster.Office_ID,
              loan.LoanMaster.Region_ID,
              this._refdata
            );

            if (ref_Crop && ref_Crop_basis) {
              let cropprice = <Loan_Crop>{
                Acres: 0,
                ActionStatus: 1,
                Adj_Price: 0,
                Basic_Adj: ref_Crop_basis.Basis_Adj,
                Contract_Price: 0,
                Contract_Qty: 0,
                Crop_Code: ref_Crop.Crop_Code,
                Crop_ID: ref_Crop.Crop_ID,
                Crop_Price: ref_Crop.Crop_Price,
                Crop_Type_Code: ref_Crop.Crop_Type,
                LC_Share: 0,
                Loan_Crop_ID: 0,
                Loan_Full_ID: loan.Loan_Full_ID,
                Marketing_Adj: 0,
                Percent_booked: 0,
                Rebate_Adj: 0,
                Revenue: 0,
                W_Crop_Yield: 0,
                InsUOM: uom,
                Needed_Ind: 1,
                Crop_Price_Percent: 1,
                Crop_Price_Percent_Value: 100
              };

              neededRows.push(cropprice);

              this.Add_Audit_Trail_Collection(ref_Crop, ref_Crop_basis, loan, auditTrails);
            }
          } else {
            ExistingLoanCropRow.Needed_Ind = 1;
            ExistingLoanCropRow.Crop_Price_Percent = parseFloat(Helper.divide(ExistingLoanCropRow.Adj_Price, ExistingLoanCropRow.Crop_Price).toFixed(2));
            ExistingLoanCropRow.Crop_Price_Percent_Value = Helper.percent(ExistingLoanCropRow.Adj_Price, ExistingLoanCropRow.Crop_Price);
          }
        }

        loan.LoanCrops.push(...neededRows);

        loan.LoanCrops.filter(c => c.Needed_Ind == 0).forEach(c => {
          let cropIndex = loan.LoanCrops.findIndex(
            a =>
              a.Crop_Code == c.Crop_Code && a.Crop_Type_Code == c.Crop_Type_Code
          );

          if (c.ActionStatus != 1) {
            if (cropIndex > -1) {
              loan.LoanCrops[cropIndex].ActionStatus = 3;
            }
          } else {
            loan.LoanCrops.splice(cropIndex, 1);
          }
        });

        this.Add_Audit_Trail(auditTrails, loan);
      }
    } catch (ex) {
      if (environment.isDebugModeActive) {
        console.error(ex);
      }
    }
  }

  private Add_Audit_Trail_Collection(ref_Crop: RefCrop, ref_Crop_basis: Ref_Crop_Basis, loan: loan_model, auditTrails: any[]) {
    const crop_price_audit_trail = `Crop Price picked from - Crop: ${ref_Crop.Crop_Code}, Crop Type: ${ref_Crop.Crop_Type}, Region Id: ${ref_Crop.Region_ID}`;
    const crop_basic_audit_trail = `Crop basis picked from - Crop: ${ref_Crop_basis.Crop_Code}, Crop Type: ${ref_Crop_basis.Crop_Type_Code}. Office Id: ${ref_Crop_basis.Z_Office_ID}, Region Id: ${ref_Crop_basis.Z_Region_ID}`;

    let audits: Array<Audit_Item> = [
      {
        id: JSON.stringify(ref_Crop),
        text: crop_price_audit_trail,
        type: 'Crop Price'
      },
      {
        id: JSON.stringify(ref_Crop_basis),
        text: crop_basic_audit_trail,
        type: 'Crop Basis'
      }
    ];

    auditTrails.push(...audits);
  }

  private Add_Audit_Trail(auditTrails: any[], loan: loan_model) {
    if (auditTrails && auditTrails.length > 0) {
      this.auditTrail.addAuditTrailTextsAsync(auditTrails, loan);
    }
  }
}
