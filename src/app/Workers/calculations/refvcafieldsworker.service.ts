import { Injectable } from '@angular/core';
import { loan_model } from '@lenda/models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { DataService } from '@lenda/services/data.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { environment } from '@env/environment.prod';

@Injectable()
export class RefVCAFieldsWorkerService {
  private refData: RefDataModel;

  constructor(
    private localStorageService: LocalStorageService,
    private loggingservice: LoggingService,
    private dataService: DataService,
    private loanapi: LoanApiService
  ) {
    this.refData = this.localStorageService.retrieve(
      environment.referencedatakey
    );

    //refdata get api is taking time, so for a safer side putting observer on refData
    this.localStorageService
      .observe(environment.referencedatakey)
      .subscribe(refdata => {
        if (refdata) {
          this.refData = refdata;
        }
      });
  }

  Get_VCA_Fields_Data(Loan: loan_model) {
    let allData: Array<VCA_Field> = [];

    try {
      this.refData.Ref_VCA_Fields.forEach(vca_field => {
        let data = new VCA_Field();

        let value = this.getValue(vca_field.CA_No, Loan) || '0';
        data.CAF_No = vca_field.CA_No;
        data.Name = vca_field.Name;
        data.Value = parseFloat(value.toString());

        allData.push(data);
      });
    } catch {}

    return allData;
  }

  getValue(cafField: string, Loan: loan_model) {
    switch (cafField) {
      case 'CA1':
        return Loan.LoanMaster.CAF1;
      case 'CA2':
        return Loan.LoanMaster.CAF2;
      case 'CA3':
        return Loan.LoanMaster.CAF3;
      case 'CA4':
        return Loan.LoanMaster.CAF4;
      default:
        return Loan.LoanMaster[cafField];
    }
  }

  Eval_VCA_Fields(Loan: loan_model) {
    try {
      this.refData.Ref_VCA_Fields.forEach(vca_field => {
        let value = this.Eval_VCA_Field(vca_field.Hyper_Code, Loan);
        switch (vca_field.CA_No) {
          case 'CA1':
            Loan.LoanMaster.CAF1 = parseFloat(value.toFixed(2));
            break;
          case 'CA2':
            Loan.LoanMaster.CAF2 = parseFloat(value.toFixed(2));
            break;
          case 'CA3':
            Loan.LoanMaster.CAF3 = parseFloat(value.toFixed(2));
            break;
          case 'CA4':
            Loan.LoanMaster.CAF4 = parseFloat(value.toFixed(2));
            break;

          default:
            Loan.LoanMaster[vca_field.CA_No] = parseFloat(value.toFixed(2));
            break;
        }
      });
    } catch {}
  }

  Eval_VCA_Field(code: string, Loan: loan_model) {
    try {
      if (code) {
        let hypercode = code;
        // Remove Paranthesis
        hypercode = hypercode.replace(/\)/g, '');
        hypercode = hypercode.replace(/\(/g, '');

        // Split
        let hypercodes = hypercode.split(new RegExp('[-+()*/]', 'g'));
        hypercodes = hypercodes.map(a => a.trim()).filter(a => !!a);

        hypercodes = Array.from(new Set(hypercodes));

        hypercodes.forEach(c => {
          if (c.includes('.')) {
            let v = eval(`Loan.` + c);

            let regEx = new RegExp(c, 'g');
            code = code.replace(regEx, v || 0);
          }
        });

        return eval(code) || 0;
      }
      return 0;
    } catch (ex) {
      this.loggingservice.checkandcreatelog(2, 'VCA Fields', ex.message);
    }
  }
}

export class VCA_Field {
  Name: string;
  CAF_No: string;
  Value: number;
}
