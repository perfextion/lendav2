import { Injectable } from '@angular/core';
import { loan_model } from '../../models/loanmodel';
import { LoggingService } from '../../services/Logs/logging.service';
import { precise_round } from '@lenda/services/common-utils';
@Injectable()
export class OverallCalculationServiceService {

  constructor(public logging: LoggingService) { }

  balancesheet_calc(loanObject: loan_model) {
    try{
      let starttime = new Date().getTime();
      if(loanObject.LoanMaster){
      let loanMaster = loanObject.LoanMaster;

      loanMaster.FC_Current_Adjvalue = precise_round(loanMaster.Current_Assets * (1 - loanMaster.Current_Assets_Disc_Percent / 100),0);
      loanMaster.Current_Net_Worth = precise_round(loanMaster.Current_Assets * (1 - loanMaster.Current_Assets_Disc_Percent / 100),0);

      loanMaster.FC_Inter_Adjvalue = precise_round(loanMaster.Inter_Assets * (1 - loanMaster.Inter_Assets_Disc_Percent / 100),0);
      loanMaster.Inter_Net_Worth = precise_round(loanMaster.Inter_Assets * (1 - loanMaster.Inter_Assets_Disc_Percent / 100),0);

      loanMaster.FC_Fixed_Adjvalue = precise_round(loanMaster.Fixed_Assets * (1 - loanMaster.Fixed_Assets_Disc_Percent / 100),0);
      loanMaster.Fixed_Net_Worth = precise_round(loanMaster.Fixed_Assets * (1 - loanMaster.Fixed_Assets_Disc_Percent / 100),0);

      loanMaster.Current_Disc_Net_Worth = precise_round((loanMaster.Current_Net_Worth - loanMaster.Current_Liabilities),0);
      loanMaster.Inter_Disc_Net_Worth = loanMaster.Inter_Net_Worth - loanMaster.Inter_Liabilities;
      loanMaster.Fixed_Disc_Net_Worth = loanMaster.Fixed_Net_Worth - loanMaster.Fixed_Liabilities;


      loanMaster.Total_Assets = precise_round(loanMaster.Current_Assets + loanMaster.Inter_Assets +  loanMaster.Fixed_Assets,0);
      loanMaster.Total_Net_Worth = precise_round(loanMaster.Current_Net_Worth + loanMaster.Inter_Net_Worth +  loanMaster.Fixed_Net_Worth, 0);
      loanMaster.Total_Liabilities = precise_round(loanMaster.Current_Liabilities + loanMaster.Inter_Liabilities +  loanMaster.Fixed_Liabilities,0);
      loanMaster.Total_Disc_Net_Worth = precise_round(loanMaster.Current_Disc_Net_Worth + loanMaster.Inter_Disc_Net_Worth +  loanMaster.Fixed_Disc_Net_Worth,0);

      loanMaster.FC_Total_AdjValue = loanMaster.Total_Net_Worth;
    }

      let endtime = new Date().getTime();
        //level 2 log
      this.logging.checkandcreatelog(2, 'Calc_BalanceSheet', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
      return loanObject;
    }catch(e){
        //level 1 log as its error
      this.logging.checkandcreatelog(1, 'Calc_BalanceSheet', e);
      return loanObject;
    }
  }

}
