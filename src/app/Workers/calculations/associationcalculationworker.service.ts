import { Injectable } from '@angular/core';
import {
  loan_model,
  BorrowerEntityType,
  Loan_Association,
  AssociationTypeCode,
  Borrower_Signer
} from '../../models/loanmodel';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoanMaster } from '@lenda/models/ref-data-model';
import * as _ from 'lodash';
import {
  Get_Borrower_Name,
  Get_Spouse_Name
} from '@lenda/services/common-utils';
import { getRandomInt } from '../utility/randomgenerator';
import { ExceptionWorkerService } from './exceptionworker.service';

@Injectable()
export class AssociationcalculationworkerService {
  constructor(private logging: LoggingService, private exceptionWorker: ExceptionWorkerService) {}

  prepareLoanassociationmodel(input: loan_model): loan_model {
    try {
      if (input) {
        let lm = input.LoanMaster as LoanMaster;

        _.remove(
          input.Association,
          a =>
            a.Assoc_Type_Code == AssociationTypeCode.Borrower &&
            a.ActionStatus == -1
        );

        this.Add_Borrower_As_Borrower_Assoc(lm, input);
        this.Add_Co_Borrowers_As_Borrower_Assoc(input);
      }
    } catch (ex) {
      this.logging.checkandcreatelog(
        2,
        'AssociationcalculationworkerService - prepareLoanassociationmodel',
        ex.message
      );
    }

    input = this.populateBorrowerSigners(input);
    input = this.processBorrowerSigners(input);
    input = this.Validate_Associations(input);

    input = this.performExceptions(input);

    return input;
  }

  private Add_Borrower_As_Borrower_Assoc(lm: LoanMaster, input: loan_model) {
    if (lm.Borrower_Entity_Type_Code == BorrowerEntityType.Individual) {
      this.Add_Borrower(lm, input);
    } else if (
      lm.Borrower_Entity_Type_Code == BorrowerEntityType.IndividualWithSpouse
    ) {
      this.Add_Borrower(lm, input);
      this.Add_Spouse(lm, input);
    } else {
      this.Add_Borrower(lm, input);
    }
  }

  private Add_Co_Borrowers_As_Borrower_Assoc(input: loan_model) {
    try {
      if (input.CoBorrower) {
        input.CoBorrower.forEach(borrower => {
          this.Add_Borrower_As_Borrower_Assoc(borrower as any, input);
        });
      }
    } catch (ex) {
      this.logging.checkandcreatelog(
        2,
        'AssociationcalculationworkerService - Add_Co_Borrowers_As_Borrower_Assoc',
        ex.message
      );
    }
  }

  private Add_Spouse(lm: LoanMaster, input: loan_model) {
    let pri = new Loan_Association();
    pri.Assoc_ID = lm.Borrower_ID;
    pri.Assoc_Type_Code = AssociationTypeCode.Borrower;
    pri.Loan_Full_ID = input.Loan_Full_ID;
    pri.Loan_ID = lm.Loan_ID;
    pri.Loan_Seq_Num = lm.Loan_Seq_num;
    pri.Phone = lm.Spouse_Phone;
    pri.Preferred_Contact_Ind = lm.Spouse_Preferred_Contact_Ind;
    pri.Email = lm.Spouse_Email;
    pri.Location = lm.Spouse_Address;
    pri.Assoc_Name = Get_Spouse_Name(lm);
    pri.ActionStatus = -1;
    pri.Is_CoBorrower = 1;
    input.Association.push(pri);
  }

  private Add_Borrower(lm: LoanMaster, input: loan_model) {
    let pri = new Loan_Association();
    pri.Assoc_ID = lm.Borrower_ID || getRandomInt(Math.pow(10, 4), Math.pow(10, 6));
    pri.Assoc_Type_Code = AssociationTypeCode.Borrower;
    pri.Loan_Full_ID = input.Loan_Full_ID;
    pri.Loan_ID = lm.Loan_ID;
    pri.Loan_Seq_Num = lm.Loan_Seq_num;
    pri.Phone = lm.Borrower_Phone;
    pri.Preferred_Contact_Ind = lm.Borrower_Preferred_Contact_Ind;
    pri.Email = lm.Borrower_Email;
    pri.Location = lm.Borrower_Address;
    pri.Assoc_Name = Get_Borrower_Name(lm);
    pri.ActionStatus = -1;
    pri.Is_CoBorrower = 1;
    input.Association.push(pri);
  }

  Validate_Associations(Loan: loan_model) {
    try {
      if (Loan.Association) {
        Loan.Association.forEach(assoc => {
          this.ValidateAssociation(assoc, Loan);
        });
      }
    } catch (ex) {
      this.logging.checkandcreatelog(
        2,
        'AssociationcalculationworkerService - Validate_Associations',
        ex.message
      );
    }
    return Loan;
  }

  ValidateAssociation(assoc: Loan_Association, Loan: loan_model) {
    let rows = Loan.Association.filter(
      a =>
        a.ActionStatus != 3 &&
        a.Assoc_Name == assoc.Assoc_Name &&
        (a.Contact || '') == (assoc.Contact || '') &&
        a.Assoc_Type_Code == assoc.Assoc_Type_Code
    );

    if (rows.length > 1) {
      assoc.Duplicate_Assoc = true;
    } else {
      assoc.Duplicate_Assoc = false;
    }
  }

  processBorrowerSigners(input: loan_model) {
    _.remove(
      input.Association,
      a =>
        (a.Assoc_Type_Code == AssociationTypeCode.SBI ||
          a.Assoc_Type_Code == AssociationTypeCode.Guarantor) &&
        a.ActionStatus == -1
    );

    try {
      if (input.Borrower_Signers) {
        input.Borrower_Signers.forEach(signer => {
          if (signer.SBI_Ind) {
            this.Add_Signer_As_Association(
              signer,
              input,
              AssociationTypeCode.SBI
            );
          }

          if (signer.Guarantor_Ind) {
            this.Add_Signer_As_Association(
              signer,
              input,
              AssociationTypeCode.Guarantor
            );
          }
        });
      }
      return input;
    } catch {
      return input;
    }
  }

  private Add_Signer_As_Association(
    signer: Borrower_Signer,
    input: loan_model,
    Assoc_Type_Code: AssociationTypeCode
  ) {
    let assoc = new Loan_Association();
    assoc.Assoc_ID = signer.Signer_ID;
    assoc.Assoc_Name = signer.Signer_Name;
    assoc.Email = signer.Email;
    assoc.Phone = signer.Phone;
    assoc.Location = signer.Location;
    assoc.Loan_Full_ID = signer.Loan_Full_ID;
    assoc.Loan_ID = input.LoanMaster.Loan_ID;
    assoc.Loan_Seq_Num = input.LoanMaster.Loan_Seq_num;
    assoc.Preferred_Contact_Ind = signer.Preferred_Contact_Ind;
    assoc.Assoc_Type_Code = Assoc_Type_Code;
    assoc.ActionStatus = -1;

    input.Association.push(assoc);
  }

  populateBorrowerSigners(input: loan_model) {
    try {
      if(!input.Borrower_Signers) {
        input.Borrower_Signers = [];
      }

      _.remove(
        input.Borrower_Signers,
        a => a.ActionStatus == -1
      );

      if (input.LoanMaster.Borrower_Entity_Type_Code == 'IND') {
        this.Add_Borrower_As_Signer(input);
      } else if (input.LoanMaster.Borrower_Entity_Type_Code == 'INS') {
        this.Add_Spouse_As_Signer(input);
      } else {
        this.Add_Borrower_As_Signer(input);
      }
      return input;
    } catch {
      return input;
    }
  }

  private Add_Spouse_As_Signer(input: loan_model) {
    let signer = new Borrower_Signer();
    signer.ActionStatus = -1;
    signer.Signer_ID = input.LoanMaster.Borrower_ID;
    signer.Signer_Name = Get_Spouse_Name(input.LoanMaster);
    signer.Location = input.LoanMaster.Spouse_Address;
    signer.Title = '';
    signer.Percent_Owned = 50;
    signer.Preferred_Contact_Ind = input.LoanMaster.Spouse_Preferred_Contact_Ind;
    signer.Signer_Ind = 1;
    signer.SBI_Ind = 1;
    signer.Email = input.LoanMaster.Spouse_Email;
    signer.Phone = input.LoanMaster.Spouse_Phone;
    input.Borrower_Signers.splice(0, 0, signer);
  }

  private Add_Borrower_As_Signer(input: loan_model) {
    let signer = new Borrower_Signer();
    signer.ActionStatus = -1;
    signer.Signer_ID = input.LoanMaster.Borrower_ID;
    signer.Signer_Name = Get_Borrower_Name(input.LoanMaster);
    signer.Location = input.LoanMaster.Borrower_Address;
    signer.Title = '';
    signer.Percent_Owned = 100;
    signer.Preferred_Contact_Ind = input.LoanMaster.Borrower_Preferred_Contact_Ind;
    signer.Signer_Ind = 1;
    signer.SBI_Ind = 1;
    signer.Email = input.LoanMaster.Borrower_Email;
    signer.Phone = input.LoanMaster.Borrower_Phone;
    input.Borrower_Signers.splice(0, 0, signer);
  }

  private performExceptions(input: loan_model) {
    try {
      if(input && input.Association) {
        input.Association.forEach(assoc => {
          if(assoc.Exception_ID > 0) {
            let Table_name = assoc.Assoc_Type_Code.toUpperCase();
            this.exceptionWorker.Process_Exception_By_ID(input, assoc.Exception_ID, assoc, 1, Table_name);
          }
        });

      }
    } catch (ex) {
      this.logging.checkandcreatelog(
        0,
        'AssociationcalculationworkerService - performExceptions',
        ex.message
      );
    }

    return input;
  }
}
