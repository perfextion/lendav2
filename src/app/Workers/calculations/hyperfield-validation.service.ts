import { Injectable } from '@angular/core';
import { Loan_Association } from '@lenda/models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';
import {
  RefDataModel,
  RefHyperfieldValidation
} from '@lenda/models/ref-data-model';
import { environment } from '@env/environment.prod';
import { validationlevel, errormodel, exceptionlevel } from '@lenda/models/commonmodels';
import { colId, rowId } from './validation.service';
import { RegEx } from '@lenda/shared/shared.constants';
import * as _ from 'lodash';

@Injectable()
export class HyperfieldValidationWorkerService {
  refData: RefDataModel;

  constructor(private localStorage: LocalStorageService) {
    this.refData = this.localStorage.retrieve(environment.referencedatakey);

    this.localStorage.observe(environment.referencedatakey).subscribe(res => {
      this.refData = res;
    });
  }

  processHyperfieldValidation(
    chevronId: number,
    rowData: Array<Loan_Association>,
    chevronKey: string
  ) {
    let currenterrors = this.localStorage.retrieve(
      environment.errorbase
    ) as Array<errormodel>;

    _.remove(currenterrors, function (param) {
      return param.chevron == chevronKey;
    });

    try {
      const hyperFieldValidations = this.refData.Ref_Hyperfield_Validations.filter(
        a => a.Chevron_Id == chevronId
      );

      const validationGroups = _.groupBy(
        hyperFieldValidations,
        (h: RefHyperfieldValidation) => h.Field
      );

      rowData.forEach(assoc => {
        for (let g in validationGroups) {
          let validationRes = [];

          validationGroups[g].forEach(hyperField => {
            if(hyperField.Field in assoc) {
              let val = assoc[g];
              let res = this.getResult(hyperField.Operator, hyperField.Compare_Fn, val);

              if(res) {
                validationRes.push({
                  result: res,
                  hyperField: hyperField
                });
              }
            }
          });

          let levelRes = this.getMaxValidationLevel(
            validationRes.map(a => a.hyperField.Action_Ind)
          );

          if(levelRes) {
            const validation = validationGroups[g][0];

            let error = this.getErrorMessage(
              assoc,
              'Assoc_ID',
              validation.Chevron_Code + '_',
              validation.Field,
              validation.Tab_Name.toLowerCase(),
              validation.Chevron_Code,
              levelRes.level,
              levelRes.isException
            );

            currenterrors.push(error);
          }
        }
      });
    } catch (ex) {
      console.error(ex);
    }

    this.localStorage.store(environment.errorbase, _.uniq(currenterrors));
  }

  getMaxValidationLevel(levels: Array<string>) {
    if (levels.some(a => a == 'V2Blank')) {
      return {
        level: validationlevel.level2blank,
        isException: false
      };
    }

    if (levels.some(a => a == 'V2')) {
      return {
        level: validationlevel.level2,
        isException: false
      };
    }

    if (levels.some(a => a == 'V1Blank')) {
      return {
        level: validationlevel.level1blank,
        isException: false
      };
    }

    if (levels.some(a => a == 'V1')) {
      return {
        level: validationlevel.level1,
        isException: false
      };
    }

    if(levels.some(a => a == 'E2')) {
      return {
        level: exceptionlevel.level2,
        isException: true
      };
    }

    if(levels.some(a => a == 'E1')) {
      return {
        level: exceptionlevel.level1,
        isException: true
      };
    }
  }

  private getErrorMessage(
    obj: any,
    rowKey: string,
    tableId: string,
    key: string,
    tab: string,
    chevronKey: string,
    level: validationlevel | exceptionlevel,
    isException = false
  ) {
    return <errormodel>{
      cellid: tableId + obj[rowKey] + '_' + key,
      colId: colId(key),
      rowId: rowId(tableId, obj[rowKey]),
      tab: tab,
      details: this.getClass(level, isException),
      chevron: chevronKey,
      level: this.getLevel(level, isException)
    };
  }

  public getClass(level: validationlevel | exceptionlevel, isException = false) {
    if(isException) {
      switch (level) {
        case exceptionlevel.level1:
          return ['cell-validation', 'exception-1'];
        case exceptionlevel.level2:
          return ['cell-validation', 'exception-2'];
      }
    }

    switch (level) {
      case validationlevel.level1:
        return ['cell-validation', 'validation-1'];
      case validationlevel.level2:
        return ['cell-validation', 'validation-2'];
      case validationlevel.level1blank:
        return ['cell-validation', 'validation-1-blank'];
      case validationlevel.level2blank:
        return ['cell-validation', 'validation-2-blank'];
    }
  }

  private getLevel(level: validationlevel | exceptionlevel, isException = false) {
    if(isException) {
      return level;
    }

    switch (level) {
      case validationlevel.level1:
      case validationlevel.level1blank:
        return validationlevel.level1;

      case validationlevel.level2:
      case validationlevel.level2blank:
        return validationlevel.level2;
    }
  }

  getResult(operator: string, action: string, value: any) {
    switch(operator) {
      case 'IS':
        switch (action) {
          case 'InvalidEmail':
            return !ValidEmail(value);

          case 'InvalidPhone':
            return !ValidPhone(value);

          case 'Blank':
            return value == '' || value == null || value == undefined;
        }
    }
  }
}

function ValidEmail(email: string) {
  return RegEx.Email.test(email);
}

function ValidPhone(Phone: string) {
  return RegEx.Phone.test(Phone);
}
