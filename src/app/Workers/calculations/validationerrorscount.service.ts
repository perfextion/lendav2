import { Injectable, EventEmitter } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';

import { loan_model, LoanException } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import { errormodel, validationlevel, exceptionlevel } from '@lenda/models/commonmodels';
import { Loansettings, Validation, ValidationError, ValidationModel } from '@lenda/models/loansettings';
import { Page } from '@lenda/models/page.enum';
import { DataService } from '@lenda/services/data.service';
import { Loan_Exception } from '@lenda/models/loan/loan_exceptions';
import { ValidationTabs } from '@lenda/components/flowchart/schematic-rules.values';

/**
 * =========================
 * Validation error count
 * =========================
 */
@Injectable()
export class ValidationErrorsCountService {

  private errors: errormodel[];
  private exceptions: Loan_Exception[] = [];
  private loanObj: loan_model;

  latestValidationCount$ = new EventEmitter(true);

  validation_errors = {};

  constructor(
    private localstorage: LocalStorageService,
    private dataService: DataService
  ) {
    this.errors = this.localstorage.retrieve(environment.errorbase) as errormodel[];

    this.loanObj = this.localstorage.retrieve(environment.loankey);
    this.exceptions = this.loanObj ? this.loanObj.Loan_Exceptions : [];
    this.validation_errors = this.getValidationErrors();

    this.localstorage.observe(environment.errorbase).subscribe((res: errormodel[]) => {
      this.errors = res;

      if(environment.isDebugModeActive) console.time('Validation Errors Count calculation - errorbase');
      this.calculateValidationErrorCounts(this.loanObj, true);
      if(environment.isDebugModeActive) console.timeEnd('Validation Errors Count calculation - errorbase');
    });

    this.dataService.getLoanObject().subscribe(loan_res => {
      this.loanObj = loan_res;
      this.exceptions = loan_res.Loan_Exceptions.filter(a => a.ActionStatus != 3);
      this.errors = this.localstorage.retrieve(environment.errorbase) as errormodel[];

      if(environment.isDebugModeActive) console.time('Validation Errors Count calculation - dataService');
      this.calculateValidationErrorCounts(this.loanObj, true);
      if(environment.isDebugModeActive) console.timeEnd('Validation Errors Count calculation - dataService');
    });
  }

  updateValidationErrorCounts(localobj: loan_model) {
    this.exceptions = localobj.Loan_Exceptions;
    this.calculateValidationErrorCounts(localobj, true);
  }

  private calculateValidationErrorCounts(localobj: loan_model, recalculate: boolean){
    this.loanObj = this.prepareValidationErrorObject(localobj, recalculate);
    this.localstorage.store(environment.loankey,  this.loanObj);
  }

  /**
   * Use this method to prepare validation_errors in Loan_Settings to store in local storage/API
   * @param input Loan Model
   */
  prepareValidationErrorObject(input: loan_model, recalculate = false) {

    try {
      let loan_settings: Loansettings;
      try {
        loan_settings = JSON.parse(input.LoanMaster.Loan_Settings) as Loansettings;
      }
      catch {
        loan_settings = <Loansettings>{};
      }

      if (!loan_settings) {
        loan_settings = <Loansettings>{};
      }

      if (!recalculate && loan_settings && loan_settings.validation_errors) {
        return input;
      }

      let summaryErrors = <Validation>{
        validations: {
          level1: 0,
          level2: 0
        },
        exceptions: {
          level1: this.countException(2, exceptionlevel.level1),
          level2: this.countException(2, exceptionlevel.level2)
        }
      };

      let borrowerErrors = <Validation>{
        validations: {
          level1: this.countValidation(validationlevel.level1, Page.borrower),
          level2: this.countValidation(validationlevel.level2, Page.borrower)
        },
        exceptions: {
          level1: this.countException(3, exceptionlevel.level1),
          level2: this.countException(3, exceptionlevel.level2)
        }
      };

      let cropErrors = <Validation>{
        validations: {
          level1: this.countValidation(validationlevel.level1, Page.crop),
          level2: this.countValidation(validationlevel.level2, Page.crop)
        },
        exceptions: {
          level1: this.countException(4, exceptionlevel.level1),
          level2: this.countException(4, exceptionlevel.level2)
        }
      };

      let farmErrors = <Validation>{
        validations: {
          level1: this.countValidation(validationlevel.level1, Page.farm),
          level2: this.countValidation(validationlevel.level2, Page.farm)
        },
        exceptions: {
          level1: this.countException(5, exceptionlevel.level1),
          level2: this.countException(5, exceptionlevel.level2)
        }
      };

      let insuranceErrors = <Validation>{
        validations: {
          level1: this.countValidation(validationlevel.level1, Page.insurance),
          level2: this.countValidation(validationlevel.level2, Page.insurance)
        },
        exceptions: {
          level1: this.countException(6, exceptionlevel.level1),
          level2: this.countException(6, exceptionlevel.level2)
        }
      };

      let budgetErrors = <Validation>{
        validations: {
          level1: this.countValidation(validationlevel.level1, Page.budget),
          level2: this.countValidation(validationlevel.level2, Page.budget)
        },
        exceptions: {
          level1: this.countException(7, exceptionlevel.level1),
          level2: this.countException(7, exceptionlevel.level2)
        }
      };

      let optimizerErrors = <Validation> {
        validations: {
          level1: this.countValidation(validationlevel.level1, Page.optimizer),
          level2: this.countValidation(validationlevel.level2, Page.optimizer)
        },
        exceptions: {
          level1: 0,
          level2: 0
        }
      };

      let collateralErrors = <Validation>{
        validations: {
          level1: this.countValidation(validationlevel.level1, Page.collateral),
          level2: this.countValidation(validationlevel.level2, Page.collateral)
        },
        exceptions: {
          level1: this.countException(14, exceptionlevel.level1),
          level2: this.countException(14, exceptionlevel.level2)
        }
      };

      let committeeErrors = <Validation>{
        validations: {
          level1: this.countValidation(validationlevel.level1, Page.committee),
          level2: this.countValidation(validationlevel.level2, Page.committee)
        },
        exceptions: {
          level1: 0,
          level2: 0
        }
      };

      let checkoutErrors = <Validation>{
        validations: {
          level1: this.countValidation(validationlevel.level1, Page.checkout),
          level2: this.countValidation(validationlevel.level2, Page.checkout)
        },
        exceptions: {
          level1: 0,
          level2: 0
        }
      };

      let disburseErrors = <Validation>{
        validations: {
          level1: this.countValidation(validationlevel.level1, Page.disburse),
          level2: this.countValidation(validationlevel.level2, Page.disburse)
        },
        exceptions: {
          level1: this.countException(20, exceptionlevel.level1),
          level2: this.countException(20, exceptionlevel.level2)
        }
      };

      let calculatedValidations = {};
      calculatedValidations[Page.summary] = summaryErrors;
      calculatedValidations[Page.borrower] = borrowerErrors;
      calculatedValidations[Page.crop] = cropErrors;
      calculatedValidations[Page.farm] = farmErrors;
      calculatedValidations[Page.insurance] = insuranceErrors;
      calculatedValidations[Page.budget] = budgetErrors;
      calculatedValidations[Page.collateral] = collateralErrors;
      calculatedValidations[Page.optimizer] = optimizerErrors;
      calculatedValidations[Page.committee] = committeeErrors;
      calculatedValidations[Page.checkout] = checkoutErrors;
      calculatedValidations[Page.disburse] = disburseErrors;

      if (loan_settings.validation_errors == undefined) {
        loan_settings.validation_errors = calculatedValidations;
      } else {
        loan_settings.validation_errors = this.compareCalculatedWithLocalStorage(calculatedValidations, loan_settings.validation_errors);
      }

      this.validation_errors = loan_settings.validation_errors;

      this.latestValidationCount$.emit(loan_settings.validation_errors);
      input.LoanMaster.Loan_Settings = JSON.stringify(loan_settings);

      return input;

    } catch (e) {
      return input;
    }
  }

  private compareCalculatedWithLocalStorage(calculatedErrors: ValidationError, storedErrors: ValidationError) {
    let currentPageName = this.localstorage.retrieve(environment.currentpage);

    Object.keys(calculatedErrors).forEach(errorKey => {
      if (storedErrors[errorKey]) {
        if (errorKey !== currentPageName && errorKey != Page.checkout && !!storedErrors[errorKey]) {
          calculatedErrors[errorKey].validations = storedErrors[errorKey].validations;
        }
      }
    });

    return calculatedErrors;
  }

  private countException(tabId: number, level: exceptionlevel) {
    let exceptions = this.exceptions.filter(e => {
      if (e.ActionStatus != 3) {
        return e.Exception_ID_Level == level && e.Tab_ID == tabId && e.Complete_Ind == 0;
      }
      return false;
    });

    return exceptions.length;
  }

  /**
   * Counts the Validation for Page
   * @param level Validation Level
   * @param page Page Name
   */
  private countValidation(level: validationlevel, page: Page) {
    return this.errors.filter(err => err.level == level && !err.ignore)
      .map(p => p.tab.toLowerCase())
      .filter(p => p == page.toLowerCase()).length;
  }

  private _getvalidationErrors() {
    try {
      let loan_settings: Loansettings;

      try {
        loan_settings = JSON.parse(this.loanObj.LoanMaster.Loan_Settings) as Loansettings;
      }
      catch {
        loan_settings = <Loansettings>{};
      }

      if (loan_settings.validation_errors) {
        return loan_settings.validation_errors;
      }
      return this.defaultValidationErrors;
    } catch {
      return this.defaultValidationErrors;
    }
  }

  /**
   * Gets Validation and Exception for a Page
   * @param page Name of the Page
   */
  getValidation(page: Page) {
    let errors = this.validation_errors || this._getvalidationErrors();
    let validation = new ValidationModel();

    if (errors && errors[page]) {
      validation = <ValidationModel>{
        level1: errors[page].exceptions ? errors[page].exceptions.level1 : 0,
        level2: errors[page].exceptions ? errors[page].exceptions.level2 : 0,
        level3: errors[page].validations ? errors[page].validations.level1 : 0,
        level4: errors[page].validations ? errors[page].validations.level2 : 0
      }
    }
    return validation;
  }

  getValidationErrors() {
    return this._getvalidationErrors();
  }

  hasLevel2Validations(page?: string) {
    let errors = this.validation_errors || this._getvalidationErrors();

    let total = 0;

    if(errors) {
      if(page) {
        total += ( errors[page].validations.level2 || 0 )
      } else {
        Object.keys(ValidationTabs).forEach(tab => {
          if(errors[tab] && errors[tab].validations) {
            total += ( errors[tab].validations.level2 || 0 )
          }
        });
      }

      return total > 0;
    }

    return false;
  }

  /**
   * Default Validation Errors
   */
  get defaultValidationErrors() {
    const error = <Validation>{
      exceptions: {
        level1: 0,
        level2: 0
      },
      validations: {
        level1: 0,
        level2: 0
      }
    };

    let defalut_error: ValidationError = {};

    defalut_error[Page.summary] = error;
    defalut_error[Page.borrower] = error;
    defalut_error[Page.crop] = error;
    defalut_error[Page.farm] = error;
    defalut_error[Page.insurance] = error;
    defalut_error[Page.budget] = error;
    defalut_error[Page.collateral] = error;
    defalut_error[Page.optimizer] = error;
    defalut_error[Page.committee] = error;
    defalut_error[Page.checkout] = error;
    defalut_error[Page.disburse] = error;

    return defalut_error;
  }
}

