import { Injectable } from '@angular/core';

import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { loan_model, LoanGroup, LoanStatus } from '@lenda/models/loanmodel';
import { LoanMaster } from '@lenda/models/ref-data-model';

export enum ToleranceLevel {
  NoTolerence = 0,
  WithinTolerance = 1,
  OutOfTolerance = 2
}

@Injectable()
export class LoanToleranceCalculationService {
  calculateLoanTolerance(
    localloanobj: loan_model,
    latestUpdatedLoanMaster: LoanMaster
  ) {
    if (environment.isDebugModeActive) {
      console.time('Loan Tolerance Calculation');
    }

    try {
      if (
        this.getLoanStatus(localloanobj.LoanMaster.Loan_Status) ===
        LoanStatus.Working
      ) {
        const currentLoanMaster: LoanMaster = localloanobj.LoanMaster;

        const allLevels = [];

        allLevels.push(
          this.getToleranceLevel(
            latestUpdatedLoanMaster.ARM_Commitment,
            currentLoanMaster.ARM_Commitment
          )
        );

        allLevels.push(
          this.getToleranceLevel(
            latestUpdatedLoanMaster.Total_Commitment,
            currentLoanMaster.Total_Commitment
          )
        );

        allLevels.push(
          this.getToleranceLevel(
            latestUpdatedLoanMaster.Risk_Cushion_Amount || 0,
            currentLoanMaster.Risk_Cushion_Amount || 0
          )
        );

        if (!latestUpdatedLoanMaster.Current_Bankruptcy_Status) {
          if (currentLoanMaster.Current_Bankruptcy_Status) {
            allLevels.push(ToleranceLevel.OutOfTolerance);
          } else {
            allLevels.push(ToleranceLevel.WithinTolerance);
          }
        }

        let loanToleranceLevel: ToleranceLevel;

        if (allLevels.some(l => l == ToleranceLevel.OutOfTolerance)) {
          loanToleranceLevel = ToleranceLevel.OutOfTolerance;
        } else if (allLevels.some(l => l == ToleranceLevel.WithinTolerance)) {
          loanToleranceLevel = ToleranceLevel.WithinTolerance;
        } else {
          loanToleranceLevel = ToleranceLevel.NoTolerence;
        }

        localloanobj.LoanMaster.Loan_Tolerance_Level_Ind = loanToleranceLevel;
      }
    } catch (ex) {
      console.log(ex);
    }

    if (environment.isDebugModeActive) {
      console.timeEnd('Loan Tolerance Calculation');
    }
  }

  private getLoanStatus(_loanStatus: string) {
    if (_loanStatus && _loanStatus.includes('_')) {
      return _loanStatus.substring(0, _loanStatus.indexOf('_'));
    }
    return _loanStatus;
  }

  /**
   * Calculate Tolerance Level based on values
   * @param val1 Current Value - Larger
   * @param val2 Previous Value - Smaller
   */
  private getToleranceLevel(val1: number, val2: number) {
    let diff = Math.abs(val1 - val2);

    let diffPerc = 0;
    if (val1 > 0) {
      diffPerc = (diff * 100) / val1;
    }

    if (diffPerc >= 20) {
      return ToleranceLevel.OutOfTolerance;
    } else if (diffPerc < 20 && diffPerc >= 10) {
      return ToleranceLevel.WithinTolerance;
    } else {
      return ToleranceLevel.NoTolerence;
    }
  }
}
