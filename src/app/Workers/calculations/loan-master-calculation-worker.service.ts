import { Injectable } from '@angular/core';

import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';

import { loan_model, AssociationTypeCode} from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import { calculatedNumberFormatter } from '@lenda/aggridformatters/valueformatters';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { Helper } from '@lenda/services/math.helper';
import { precise_round } from '@lenda/services/common-utils';
import { errormodel, validationlevel } from '@lenda/models/commonmodels';
import { Page } from '@lenda/models/page.enum';

import { LoggingService } from '@lenda/services/Logs/logging.service';

@Injectable()
export class LoanMasterCalculationWorkerService {
  private _farmFinancialStaticValues: DefaultFarmFinacialStaticValues;
  private _refdata: RefDataModel;

  private _borrowerRatingParams: BorrowerRatingParams;

  public get farmFinancialStaticValues() {
    return this._farmFinancialStaticValues;
  }

  //farm financial ends

  public invalidRevenueThreshold: boolean;
  public invalidInsuranceThreshold: boolean;
  public invalidMaxCrop: boolean;

  private percentFormatter = new Intl.NumberFormat('en-US', {
    style: 'percent',
    maximumFractionDigits: 1,
    minimumFractionDigits: 1
  });

  constructor(
    public logging: LoggingService,
    private localstorage: LocalStorageService
  ) {
    this._refdata = this.localstorage.retrieve(environment.referencedatakey);

    this.localstorage.observe(environment.referencedatakey).subscribe((res: RefDataModel) => {
      this._refdata = res;
    });
  }

  public getFarmFinancialStaticValues(isOwned: boolean) {
    if(!this._farmFinancialStaticValues) {
      this._getFarmFinancialStaticValues(isOwned);
    }
    return this._farmFinancialStaticValues;
  }

  public get borrowerRatingParams () {
    this._borrowerRatingParams = this.getBorrowerRatingValues();
    return this._borrowerRatingParams;
  }

  private getBorrowerRatingValues() {
    if(!this._borrowerRatingParams) {
      let borrowerRatingParams = new BorrowerRatingParams();
      let borrowerRatingstaticValues = new BorrowerRatingStaticValuesParams();
      borrowerRatingstaticValues.borrowerRating = [
        { star: ['*','*','*','*','*'] },
        { star: ['*','*','*','*', '-'] },
        { star: ['*','*','*', '-', '-'] },
        { star: ['*','*', '-', '-', '-'] },
        { star: ['*', '-', '-', '-', '-'] }
      ];

      borrowerRatingstaticValues.FICOScore = this._getBorrowerRatingValues('FICO', '', (v) => { if(parseFloat(v) > 0) return v; else return '';});
      borrowerRatingstaticValues.CPAFiancial = this._getBorrowerRatingValues('CPA', '');
      borrowerRatingstaticValues.threeYrsReturns = this._getBorrowerRatingValues('TAX', '');
      borrowerRatingstaticValues.bankruptcy = this._getBorrowerRatingValues('BNK', '');
      borrowerRatingstaticValues.judgement = this._getBorrowerRatingValues('JDG', '');
      borrowerRatingstaticValues.yearsFarming = this._getBorrowerRatingValues('YRF', '');
      borrowerRatingstaticValues.farmFinnacialRating = this._getBorrowerRatingValues('FFR', '');
      borrowerRatingstaticValues.farmFinnacialRatingDisplay = this._getBorrowerRatingValues('FFR', '', (v) => this.percentFormatter.format(parseFloat(v)/ 100));

      borrowerRatingParams.borrowerRatingstaticValues = borrowerRatingstaticValues;

      borrowerRatingParams.incomeConstant = this._getBorrowerRatingValues('INC', 0, (v) => Number(v));
      borrowerRatingParams.insuranceConstant = this._getBorrowerRatingValues('INS', 0, (v) => Number(v));
      borrowerRatingParams.discNetWorthConstant = this._getBorrowerRatingValues('DNW', 0, (v) => Number(v));
      borrowerRatingParams.maxAmountConstant = this._getBorrowerRatingValues('MAX', 0, (v) => Number(v));

      this._borrowerRatingParams = borrowerRatingParams;
    }

    return this._borrowerRatingParams;
  };

  private _getBorrowerRatingValues<T>(
    rating_Code: string,
    defaultVal: T,
    formatter: (v: any) => T = v => {
      return v;
    }
  ): RatingValues<T> {
    let rating = this._refdata.Ref_Borrower_Ratings.find(
      a => a.Borrower_Rating_Code == rating_Code
    );
    if (rating) {
      return [
        this._getValue<T>(rating.Value_1, defaultVal, formatter),
        this._getValue<T>(rating.Value_2, defaultVal, formatter),
        this._getValue<T>(rating.Value_3, defaultVal, formatter),
        this._getValue<T>(rating.Value_4, defaultVal, formatter),
        this._getValue<T>(rating.Value_5, defaultVal, formatter)
      ];
    } else {
      return [defaultVal, defaultVal, defaultVal, defaultVal, defaultVal];
    }
  }

  private _getValue<T>(
    val: any,
    defaultVal: T,
    formatter: (v: any) => T = v => {
      return v;
    }
  ) {
    if (val) {
      return formatter(val);
    } else {
      return defaultVal;
    }
  }

  private _getFarmFinancialStaticValues(isOwned: boolean) {
    this._farmFinancialStaticValues = {
      currentRatio: this.getFinancialRating('CR', isOwned, '>'),
      workingCapital: this.getFinancialRating('WC', isOwned, '>'),
      debtByAssets: this.getFinancialRating('DA', isOwned, '<'),
      debtByEquity: this.getFinancialRating('DE', isOwned, '<'),
      equityByAssets: this.getFinancialRating('EA', isOwned, '>'),
      ROA: this.getFinancialRating('ROA', isOwned, '>'),
      operatingProfit: this.getFinancialRating('OP', isOwned, '>'),
      operatingByExpRev: this.getFinancialRating('OE', isOwned, '>'),
      interestByCashFlow: this.getFinancialRating('IC', isOwned, '<'),
    }
  }

  private _setWeightValues(farm: FarmFinacialValueParams) {
    farm.currentRatio_weight = this._getRefWeight('CR');
    farm.workingCapital_weight = this._getRefWeight('WC');
    farm.debtByAssets_weight = this._getRefWeight('DA');
    farm.equityByAssets_weight = this._getRefWeight('EA');
    farm.debtByEquity_weight = this._getRefWeight('DE');
    farm.ROA_weight = this._getRefWeight('ROA');
    farm.operatingProfit_weight = this._getRefWeight('OP');
    farm.operatingByExpRev_weight = this._getRefWeight('OP');
    farm.interestByCashFlow_weight = this._getRefWeight('IC');

    farm.totalFarmFinacialRating_weight = farm.currentRatio_weight + farm.workingCapital_weight +
      farm.debtByAssets_weight + farm.ROA_weight + farm.equityByAssets_weight + farm.debtByEquity_weight +
      farm.operatingProfit_weight +  farm.operatingByExpRev_weight + farm.interestByCashFlow_weight;
  }

  private getFinancialRating(code: string, isOwned: boolean, operator: string): Array<any> {
    let rating = this._refdata.Ref_Farm_Financial_Ratings.find(a => a.Farm_Financial_Code == code);
    if(rating) {
      if(isOwned) {
        return [
          (rating.Owned_Strong / 100).toLocaleString('en-US', { minimumFractionDigits: 2 }),
          (rating.Owned_Stable / 100).toLocaleString('en-US', { minimumFractionDigits: 2 }),
          operator
        ];
      } else {
        return [
          (rating.Leased_Strong / 100).toLocaleString('en-US', { minimumFractionDigits: 2 }),
          (rating.Leased_Stable / 100).toLocaleString('en-US', { minimumFractionDigits: 2 }),
          operator
        ];
      }
    } else {
      return [
        '0.00',
        '0.00',
        operator
      ];
    }
  }

  private _getRefWeight(code: string) {
    try {
      let Ref_Farm_Financial_Rating = this._refdata.Ref_Farm_Financial_Ratings.find(a => a.Farm_Financial_Code.toUpperCase() == code.toUpperCase());
      return Ref_Farm_Financial_Rating.Weight;
    } catch {
      return 0;
    }
  }

  performLoanMasterCalcualtions(loanObject: loan_model) {
    try {
    let starttime = new Date().getTime();

    if(loanObject.LoanMaster) {

      this._getFarmFinancialStaticValues(loanObject.LoanMaster.FC_Owned);

      let loanMaster = loanObject.LoanMaster;
      //loanMaster.Borrower_Farm_Financial_Rating = loanMaster.Borrower_Farm_Financial_Rating || 145;
      loanObject.LoanMaster.Borrower_3yr_Tax_Returns = loanObject.LoanMaster.Borrower_3yr_Tax_Returns;
      // loanObject.Borrower.ActionStatus = 2;
      //loanObject.Borrower.Borrower_CPA_financials = !!loanObject.LoanMaster.CPA_Prepared_Financials;
      loanMaster.Credit_Score = loanMaster.Credit_Score || 0;
      loanMaster.Credit_Score_Date =  loanMaster.Credit_Score_Date || '';

      let FICOScore = loanMaster.Credit_Score;
      let CPAFiancial = loanObject.LoanMaster.CPA_Prepared_Financials ? 'Yes' : 'No';
      let threeYrsReturns = loanObject.LoanMaster.Borrower_3yr_Tax_Returns ? 'Yes' : 'No';
      let bankruptcy = loanMaster.Previous_Bankruptcy_Status ? 'Yes' : 'No';
      let judgement = loanMaster.Judgement_Ind ? 'Yes' : 'No';
      let yearsFarming = loanMaster.Year_Begin_Farming ? (new Date()).getFullYear() - loanMaster.Year_Begin_Farming : 0;
      let farmFinnacialRating = loanMaster.Borrower_Farm_Financial_Rating ? parseFloat(loanMaster.Borrower_Farm_Financial_Rating as any)  : 0;

      //Rating calculation

      // if(loanMaster.Current_Bankruptcy_Status && loanMaster.Current_Bankruptcy_Status === 1){
        //If currenlty bankrupt, no need to check anything. It should be rating 1
      //   loanMaster.Borrower_Rating = 1
      // }else{
      for (let rating = 5; rating >= 1; rating--) {
        let ratingRequirement = this.getRatingRequirement(rating);


        if (( isNaN(parseInt(FICOScore as any)) || FICOScore >= ratingRequirement.FICOScore)
          && (!ratingRequirement.CPAFiancial || CPAFiancial === ratingRequirement.CPAFiancial)
          && (!ratingRequirement.threeYrsReturns || threeYrsReturns === ratingRequirement.threeYrsReturns)
          && (!ratingRequirement.bankruptcy || bankruptcy === ratingRequirement.bankruptcy)
          && (!ratingRequirement.judgement || judgement === ratingRequirement.judgement)
          && yearsFarming >= ratingRequirement.yearsFarming
          && ((!ratingRequirement.farmFinnacialRating || (ratingRequirement.farmFinnacialRating && farmFinnacialRating >= ratingRequirement.farmFinnacialRating)))) {
          loanMaster.Borrower_Rating = rating;
          // loanMaster.Borrower_Rating = "*".repeat(rating);
        // this.exceptionService.logHyperFieldExceptions();
          break;
        }
        // }
      }

      let NetCropRevenue = loanMaster.Net_Market_Value_Crops ? parseInt(loanMaster.Net_Market_Value_Crops.toFixed(0)) : 0;
      let Total_Additional_Revenue = loanMaster.Net_Market_Value_Livestock + loanMaster.Net_Market_Value_FSA + loanMaster.Net_Market_Value_Other + loanMaster.Net_Market_Value_Stored_Crops;
      loanMaster.Total_Revenue = precise_round(NetCropRevenue + Total_Additional_Revenue,0);

      this.validateRevenueAndThresholdValues(loanObject);

      let endtime = new Date().getTime();
        //level 2 log
      this.logging.checkandcreatelog(2, 'Calc_LoanMaster', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
    }
    return loanObject;

    } catch(e) {
        //level 1 log as its error
      this.logging.checkandcreatelog(1, 'Calc_LoanMaster', e);
      return loanObject;
    }
  }

  performDashboardCaclulation(localLoanObject : loan_model): any {

    let starttime = new Date().getTime();
    if(localLoanObject.LoanMaster && localLoanObject.LoanMaster){
      let loanMaster = localLoanObject.LoanMaster;


      // Risk Cushion
      loanMaster.Risk_Cushion_Amount = ((loanMaster.FC_Ins_Disc_Collateral_Value || 0) + (loanMaster.Disc_value_Insurance || 0) +
                                        (loanMaster.CEI_Value || 0) + (loanMaster.Ag_Pro_Requested_Credit || 0)) -
                                       (loanMaster.ARM_Commitment  + loanMaster.ARM_Rate_Fee ) ;

      loanMaster.Risk_Cushion_Amount = parseFloat(loanMaster.Risk_Cushion_Amount.toFixed(2));
      loanMaster.Risk_Cushion_Percent = Helper.percent(loanMaster.Risk_Cushion_Amount, loanMaster.ARM_Commitment);

      // Return
      loanMaster.Total_ARM_Fees_And_Interest = loanMaster.Total_ARM_Fees_And_Interest || 0;
      loanMaster.Return_Amount = parseFloat((loanMaster.Total_ARM_Fees_And_Interest).toFixed(2));

      const Total_Fee = loanMaster.Origination_Fee_Amount + loanMaster.Service_Fee_Amount;
      loanMaster.Return_Percent = Helper.percent( loanMaster.Total_ARM_Fees_And_Interest, loanMaster.ARM_Commitment - Total_Fee);

      // Cashflow
      let total_revenue  = parseFloat((localLoanObject.LoanMaster.Net_Market_Value_Crops || 0) as any) + loanMaster.Net_Other_Income;
      // let total_revenue  = this.getRevanueThresholdValue(localLoanObject) + loanMaster.Net_Other_Income;
      let budgeted_Expenses = loanMaster.ARM_Commitment + loanMaster.Dist_Commitment + loanMaster.Third_Party_Total_Budget;

      loanMaster.Cash_Flow_Amount = precise_round(total_revenue -  (budgeted_Expenses + loanMaster.Interest_Est_Amount),0);
      loanMaster.Cash_Flow_Percent = Helper.percent(loanMaster.Cash_Flow_Amount, total_revenue);

      // Break Even Calculation
      loanMaster.Break_Even_Percent = Helper.percent(loanMaster.ARM_Commitment + loanMaster.Dist_Commitment,  total_revenue);

      // third party credit
      if(localLoanObject.Association) {
        loanMaster.Third_Party_Credit = _.sumBy(localLoanObject.Association.filter(
          a =>
            a.ActionStatus != 3 &&
            a.Assoc_Type_Code == AssociationTypeCode.ThirdParty
        ), a => a.Amount || 0);
      } else {
        loanMaster.Third_Party_Credit = 0;
      }
    }

    let endtime = new Date().getTime();
      //level 2 log
    this.logging.checkandcreatelog(2, 'Calc_LoanMaster_Dashboard', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
    return localLoanObject;
  }

  getRatingRequirement(rating: number) {
    let starttime = new Date().getTime();
    if (rating > 5 || rating < 1) {
      throw "Invalid rating passed";
    }
    let lookupIndex = 5 - rating;
    let endtime = new Date().getTime();
    // level 2 log
    this.logging.checkandcreatelog(2, 'Calc_LoanMaster_RatingReq', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
    return {
      borrowerRating: this.borrowerRatingParams.borrowerRatingstaticValues.borrowerRating[lookupIndex],
      FICOScore: this.borrowerRatingParams.borrowerRatingstaticValues.FICOScore[lookupIndex],
      CPAFiancial: this.borrowerRatingParams.borrowerRatingstaticValues.CPAFiancial[lookupIndex],
      threeYrsReturns: this.borrowerRatingParams.borrowerRatingstaticValues.threeYrsReturns[lookupIndex],
      bankruptcy: this.borrowerRatingParams.borrowerRatingstaticValues.bankruptcy[lookupIndex],
      judgement: this.borrowerRatingParams.borrowerRatingstaticValues.judgement[lookupIndex],
      yearsFarming: this.borrowerRatingParams.borrowerRatingstaticValues.yearsFarming[lookupIndex],
      farmFinnacialRating: this.borrowerRatingParams.borrowerRatingstaticValues.farmFinnacialRating[lookupIndex],
    }
  }

  /**
   * Validates Revenue Threshold Value and Insurance Threshold Value
   * @param loanObject Loan Object
   */
  validateRevenueAndThresholdValues(loanObject: loan_model) {
    this.validateRevanueThresholdValue(loanObject);
    this.validateInsuranceThresholdValue(loanObject);
    this.validateMaxCropValue(loanObject);

    this.addValidationBorrowerRating();
  }

  private addValidationBorrowerRating() {
    try {
      let currenterrors = this.localstorage.retrieve(
        environment.errorbase
      ) as Array<errormodel>;

      _.remove(currenterrors, function (param) {
        return param.chevron == `Borrower_Rating`;
      });

      if(this.invalidRevenueThreshold) {
        let error = <errormodel> {
          tab: Page.borrower,
          details: [],
          level: validationlevel.level2,
          chevron: `Borrower_Rating`,
          Validation_ID_Text: 'ARM Commitment is exceeding Revenue Threshold Value.',
          hoverText: 'ARM Commitment is exceeding Revenue Threshold Value.',
          Validation_ID: 1
        };

        currenterrors.push(error);
      }

      if(this.invalidInsuranceThreshold) {
        let error = <errormodel> {
          tab: Page.borrower,
          details: [],
          level: validationlevel.level2,
          chevron: `Borrower_Rating`,
          Validation_ID_Text: 'ARM Commitment is exceeding Insurance Threshold Value.',
          hoverText: 'ARM Commitment is exceeding Insurance Threshold Value.',
          Validation_ID: 2
        };

        currenterrors.push(error);
      }

      if(this.invalidMaxCrop) {
        let error = <errormodel> {
          tab: Page.borrower,
          details: [],
          level: validationlevel.level2,
          chevron: `Borrower_Rating`,
          Validation_ID_Text: 'ARM Commitment is exceeding Max Crop Loan Value.',
          hoverText: 'ARM Commitment is exceeding Max Crop Loan Value.',
          Validation_ID: 47
        };

        currenterrors.push(error);
      }

      this.localstorage.store(environment.errorbase, _.uniq(currenterrors));
    } catch {

    }
  }

  /**
   * Validates Revenue Threshold Value
   * @param loanObject Loan Object
   */
  private validateRevanueThresholdValue(loanObject: loan_model) {
    let revanueThresholdValue = this.getRevanueThresholdValue(loanObject);
    let ARM_Commitment = loanObject.LoanMaster.ARM_Commitment;

    this.invalidRevenueThreshold = ARM_Commitment > revanueThresholdValue;
  }

  /**
   * Validates Insurance Threshold Value
   * @param loanObject Loan Object
   */
  private validateInsuranceThresholdValue(loanObject: loan_model) {
    let insuranceThresholdValue = this.getInsuranceThresholdValue(loanObject);
    let ARM_Commitment = loanObject.LoanMaster.ARM_Commitment;

    this.invalidInsuranceThreshold = ARM_Commitment > insuranceThresholdValue;
  }

   /**
   * Validates Max Crop Value
   * @param loanObject Loan Object
   */
  private validateMaxCropValue(loanObject: loan_model) {
    let maxCropLoanValue = this.getMaxCropLoanValue(loanObject);
    let ARM_Commitment = loanObject.LoanMaster.ARM_Commitment;

    this.invalidMaxCrop = ARM_Commitment > maxCropLoanValue;
  }

  getRevanueThresholdValue(loanObject: loan_model) {
    let loanMaster = loanObject.LoanMaster;
    let temp = (loanMaster.Net_Market_Value_Crops || 0) + (loanMaster.Net_Market_Value_FSA || 0 ) + (loanMaster.Net_Market_Value_Other || 0);
    return Math.round(temp);
  }

  getRevanueThresholdStaticValues(loanObject: loan_model) {
    let revanueThresholdValue = this.getRevanueThresholdValue(loanObject);
    return this.borrowerRatingParams.incomeConstant.map((val, index) => Math.round(revanueThresholdValue * val / 100));
  }


  getInsuranceThresholdValue(loanObject: loan_model) {
    let loanMaster = loanObject.LoanMaster;
    let tValue = (loanObject.Loanwfrp.Approved_Revenue || 0) + (loanMaster.FC_Total_Addt_Collateral_Value || 0) + (_.sumBy(loanObject.LoanCropPractices, (cp)=>cp.FC_LCP_Ins_Value || 0));
    return Math.round(tValue);
  }

  getInsuranceThresholdStaticValue(loanObject: loan_model) {
    let maxCropLoanValue = this.getInsuranceThresholdValue(loanObject);
    return this.borrowerRatingParams.insuranceConstant.map((val, index) => Math.round(maxCropLoanValue * val / 100));
  }

  getMaxCropStaticValue(loanObject: loan_model){
    let rtStaticValue = this.getRevanueThresholdStaticValues(loanObject);
    let itStaticValue = this.getInsuranceThresholdStaticValue(loanObject);
    let values =  rtStaticValue.map((values,index)=>Math.min(rtStaticValue[index], itStaticValue[index]))
    return values;
  }

  getMaxCropLoanValue(loanObject: loan_model){
    let revanueThresholdValue = this.getRevanueThresholdValue(loanObject);
    let insuranceThresholdValue = this.getInsuranceThresholdValue(loanObject);

    return Math.min(revanueThresholdValue, insuranceThresholdValue);
  }

  getDiscNetWorthValue(loanObject: loan_model) {
    let loanMaster = loanObject.LoanMaster;
    return loanMaster.Total_Disc_Net_Worth;
  }

  getDiscWorthStaticValue(loanObject: loan_model) {
    let discWorthValue = this.getDiscNetWorthValue(loanObject);
    return this.borrowerRatingParams.discNetWorthConstant.map((val, index) => Math.round(discWorthValue * val / 100));
  }

  getAgProMaxAdditionStaticValue(loanObject: loan_model) {
    let discNetWorthStaticValue = this.getDiscWorthStaticValue(loanObject);
    return [Math.max(Math.min(this.borrowerRatingParams.maxAmountConstant[0], discNetWorthStaticValue[0]), 0),
            Math.max(Math.min(this.borrowerRatingParams.maxAmountConstant[1], discNetWorthStaticValue[1]), 0),
            Math.max(Math.min(this.borrowerRatingParams.maxAmountConstant[2], discNetWorthStaticValue[2]), 0),
            Math.max(Math.min(this.borrowerRatingParams.maxAmountConstant[3], discNetWorthStaticValue[3]), 0),
            Math.max(Math.min(this.borrowerRatingParams.maxAmountConstant[4], discNetWorthStaticValue[4]), 0)]
  }

  getFarmFinancialRating(loanObject: loan_model){
    let loanMaster = loanObject.LoanMaster;
    // let this.farmFinancialStaticValues = loanMaster.farmFinancialStaticValues;

    if(!this.farmFinancialStaticValues) {
      this._farmFinancialStaticValues = this.getFarmFinancialStaticValues(loanMaster.FC_Owned);
    }

    // let borrower = loanObject.Borrower;
    let farmFinancialRatingValues = new FarmFinacialValueParams();
    this._setWeightValues(farmFinancialRatingValues);

    let total_revenue = this.getRevanueThresholdValue(loanObject);

    farmFinancialRatingValues.currentRatio = Helper.divide(loanMaster.Current_Assets, loanMaster.Current_Liabilities);
    farmFinancialRatingValues.currentRatio_rating = this.getRating(farmFinancialRatingValues.currentRatio, this.farmFinancialStaticValues.currentRatio[0], this.farmFinancialStaticValues.currentRatio[1]);
    farmFinancialRatingValues.currentRatio_weight_sum = this.getWeightSum(farmFinancialRatingValues.currentRatio_weight, farmFinancialRatingValues.currentRatio_rating),

    farmFinancialRatingValues.workingCapital = Helper.divide(loanMaster.Current_Assets - loanMaster.Current_Liabilities, total_revenue);
    farmFinancialRatingValues.workingCapital_rating = this.getRating(farmFinancialRatingValues.workingCapital, this.farmFinancialStaticValues.workingCapital[0], this.farmFinancialStaticValues.workingCapital[1]);
    farmFinancialRatingValues.workingCapital_weight_sum = this.getWeightSum(farmFinancialRatingValues.workingCapital_weight, farmFinancialRatingValues.workingCapital_rating),

    farmFinancialRatingValues.debtByAssets = Helper.IFERROR(loanMaster.Total_Liabilities, loanMaster.Total_Assets);
    farmFinancialRatingValues.debtByAssets_rating = this.getRating(farmFinancialRatingValues.debtByAssets, this.farmFinancialStaticValues.debtByAssets[0], this.farmFinancialStaticValues.debtByAssets[1]);
    farmFinancialRatingValues.debtByAssets_weight_sum = this.getWeightSum(farmFinancialRatingValues.debtByAssets_weight, farmFinancialRatingValues.debtByAssets_rating),

    farmFinancialRatingValues.equityByAssets = Helper.divide(loanMaster.Total_Assets - loanMaster.Total_Liabilities, loanMaster.Total_Assets);
    farmFinancialRatingValues.equityByAssets_rating = this.getRating(farmFinancialRatingValues.equityByAssets, this.farmFinancialStaticValues.equityByAssets[0], this.farmFinancialStaticValues.equityByAssets[1]);
    farmFinancialRatingValues.equityByAssets_weight_sum = this.getWeightSum(farmFinancialRatingValues.equityByAssets_weight, farmFinancialRatingValues.equityByAssets_rating),

    farmFinancialRatingValues.debtByEquity = Helper.IFERROR(loanMaster.Total_Liabilities, loanMaster.Total_Assets - loanMaster.Total_Liabilities);
    farmFinancialRatingValues.debtByEquity_rating = this.getRating(farmFinancialRatingValues.debtByEquity, this.farmFinancialStaticValues.debtByEquity[0], this.farmFinancialStaticValues.debtByEquity[1]);
    farmFinancialRatingValues.debtByEquity_weight_sum = this.getWeightSum(farmFinancialRatingValues.debtByEquity_weight, farmFinancialRatingValues.debtByEquity_rating),

    farmFinancialRatingValues.ROA = Helper.divide(loanMaster.Cash_Flow_Amount, loanMaster.Total_Assets);
    farmFinancialRatingValues.ROA_rating = this.getRating(farmFinancialRatingValues.ROA, this.farmFinancialStaticValues.ROA[0], this.farmFinancialStaticValues.ROA[1]);
    farmFinancialRatingValues.ROA_weight_sum = this.getWeightSum(farmFinancialRatingValues.ROA_weight, farmFinancialRatingValues.ROA_rating),

    farmFinancialRatingValues.operatingProfit = Helper.divide(loanMaster.Cash_Flow_Amount, total_revenue);
    farmFinancialRatingValues.operatingProfit_rating = this.getRating(farmFinancialRatingValues.operatingProfit, this.farmFinancialStaticValues.operatingProfit[0], this.farmFinancialStaticValues.operatingProfit[1]);
    farmFinancialRatingValues.operatingProfit_weight_sum = this.getWeightSum(farmFinancialRatingValues.operatingProfit_weight, farmFinancialRatingValues.operatingProfit_rating);

    let budgeted_expenses = loanMaster.ARM_Commitment + loanMaster.Dist_Commitment + loanMaster.Third_Party_Total_Budget;
    farmFinancialRatingValues.operatingByExpRev = Helper.IFERROR(budgeted_expenses + loanMaster.Total_Return_Fee, total_revenue);
    farmFinancialRatingValues.operatingByExpRev_weight_sum =  this.getWeightSum(farmFinancialRatingValues.operatingByExpRev_weight, farmFinancialRatingValues.operatingByExpRev);

    farmFinancialRatingValues.interestByCashFlow = Helper.divide(loanMaster.Total_Return_Fee, loanMaster.Cash_Flow_Amount);
    farmFinancialRatingValues.interestByCashFlow_weight_sum = this.getWeightSum(farmFinancialRatingValues.interestByCashFlow_weight, farmFinancialRatingValues.interestByCashFlow);

    farmFinancialRatingValues.weightSum = farmFinancialRatingValues.currentRatio_weight_sum + farmFinancialRatingValues.workingCapital_weight_sum +
                                          farmFinancialRatingValues.debtByAssets_weight_sum + farmFinancialRatingValues.equityByAssets_weight_sum +
                                          farmFinancialRatingValues.debtByEquity_weight_sum + farmFinancialRatingValues.ROA_weight_sum +
                                          farmFinancialRatingValues.operatingProfit_weight_sum + farmFinancialRatingValues.operatingByExpRev_weight_sum +
                                          farmFinancialRatingValues.interestByCashFlow_weight_sum;

    let weight_sum: number = parseFloat(calculatedNumberFormatter(farmFinancialRatingValues.weightSum, 2));
    farmFinancialRatingValues.totalFarmFinacialRating = parseFloat((Helper.divide(weight_sum, farmFinancialRatingValues.totalFarmFinacialRating_weight) * 100).toFixed(2));
    loanMaster.Borrower_Farm_Financial_Rating = farmFinancialRatingValues.totalFarmFinacialRating;

    return farmFinancialRatingValues;
  }

  private getRating(input: number, strong: number, stable: number){
    if(!input) input = 0;
    return (100+(input - strong ) *(100 - 0 ) / (strong-stable))/100;
  }

  private getWeightSum(weight: number, rating: number){

    return weight * rating;
  }


  getPossible(ratio: number, params: Array<any>){
    // let operator = params[2];
     let stable = params[1];
     let strong = params[0];
    let state = this.getState(ratio,params);
    if (state === STATE.WEAK) {
      return 1;
    } else {
      return 1;
    }
  }

  getState(ratio: number, params: Array<any>){
    let operator = params[2];
    let stable = params[1];
    let strong = params[0];

    if (operator === '>') {
      if(ratio >= strong){
        return STATE.STRONG;
      }else if(ratio < strong && ratio >=stable){
        return STATE.STABLE
      }else{
        return STATE.WEAK;
      }
    } else {
      if(ratio <= strong){
        return STATE.STRONG;
      }else if(ratio > strong && ratio <=stable){
        return STATE.STABLE
      }else{
        return STATE.WEAK;
      }
    }
  }


}

export enum STATE{
  STRONG='strong',
  STABLE = 'stable',
  WEAK ='weak',

}

export class FarmFinacialValueParams {
  currentRatio: number = 0;
  workingCapital: number = 0;
  debtByAssets: number = 0;
  debtByEquity: number = 0;
  equityByAssets: number = 0;
  ROA: number = 0;
  operatingProfit: number = 0;
  operatingByExpRev: number = 0;
  interestByCashFlow: number = 0;
  totalFarmFinacialRating: number = 0;

  currentRatio_rating: number = 0;
  workingCapital_rating: number = 0;
  debtByAssets_rating: number = 0;
  debtByEquity_rating: number = 0;
  equityByAssets_rating: number = 0;
  ROA_rating: number = 0;
  operatingProfit_rating: number = 0;
  operatingByExpRev_rating: number = 0;
  interestByCashFlow_rating: number = 0;
  totalFarmFinacialRating_rating: number = 0;

  currentRatio_weight: number = 3;
  workingCapital_weight: number = 3;
  debtByAssets_weight: number = 3;
  debtByEquity_weight: number = 3;
  equityByAssets_weight: number = 3;
  ROA_weight: number = 1;
  operatingProfit_weight: number = 1;
  operatingByExpRev_weight: number = 0;
  interestByCashFlow_weight: number = 0;
  totalFarmFinacialRating_weight: number = 17;

  currentRatio_weight_sum: number = 0;
  workingCapital_weight_sum: number = 0;
  debtByAssets_weight_sum: number = 0;
  debtByEquity_weight_sum: number = 0;
  equityByAssets_weight_sum: number = 0;
  ROA_weight_sum: number = 0;
  operatingProfit_weight_sum: number = 0;
  operatingByExpRev_weight_sum: number = 0;
  interestByCashFlow_weight_sum: number = 0;
  totalFarmFinacialRating_weight_sum: number = 0;

  weightSum: number = 0;
}


export class DefaultFarmFinacialStaticValues {
  currentRatio: FinacialRating; // CR
  workingCapital: FinacialRating; // WC
  debtByAssets: FinacialRating; // DA
  debtByEquity: FinacialRating; // DE
  equityByAssets: FinacialRating; // EA
  ROA: FinacialRating; // ROA
  operatingProfit: FinacialRating; // OP
  operatingByExpRev: FinacialRating; // OPER
  interestByCashFlow: FinacialRating; // CF
}

export type FinacialRating = Array<any>;


export class BorrowerRatingParams {
  borrowerRatingstaticValues: BorrowerRatingStaticValuesParams;
  incomeConstant: RatingValues<number>;
  insuranceConstant: RatingValues<number>;
  discNetWorthConstant: RatingValues<number>;
  maxAmountConstant : RatingValues<number>;

  constructor() {
    this.borrowerRatingstaticValues = new BorrowerRatingStaticValuesParams();
    this.incomeConstant = [0, 0, 0, 0, 0];
    this.insuranceConstant = [0, 0, 0, 0, 0];
    this.discNetWorthConstant = [0, 0, 0, 0, 0];
    this.maxAmountConstant = [0, 0, 0, 0, 0];
  }
}

export class BorrowerRatingStaticValuesParams {
  borrowerRating: Array<BorrowerRatingValue>;
  FICOScore: RatingValues;
  CPAFiancial: RatingValues<string>;
  threeYrsReturns: RatingValues<string>;
  bankruptcy: RatingValues<string>;
  judgement: RatingValues<string>;
  yearsFarming: RatingValues;
  farmFinnacialRating: RatingValues;
  farmFinnacialRatingDisplay: RatingValues;

  constructor() {
    this.borrowerRating = [];
    this.FICOScore = [0, 0, 0, 0, 0];
    this.CPAFiancial = ['', '', '', '', ''];
    this.threeYrsReturns = ['', '', '', '', ''];
    this.bankruptcy = ['', '', '', '', ''];
    this.judgement = ['', '', '', '', ''];
    this.yearsFarming = [0, 0, 0, 0, 0];
    this.farmFinnacialRating = [0, 0, 0, 0, 0];
    this.farmFinnacialRatingDisplay = [0, 0, 0, 0, 0];
  }
}

export class BorrowerRatingValue {
  star: RatingValues<string>;

  constructor() {
    this.star = ['-', '-', '-', '-', '-'];
  }
}

export type RatingValues<T = string | number> = [T, T, T, T, T];
