import { Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment';

import {
  RefDataModel,
  Ref_Exception,
  Pending_Action_Required,
  RefValidation,
  RefBudgetExpenseType,
  RefCollateralActionDiscAdj,
  RefCollateralInsDisc,
  RefCollateralMktDisc,
  Exception_Detail,
  LoanMaster,
  RefPendingAction
} from '@lenda/models/ref-data-model';
import {
  loan_model,
  Loan_Budget,
  User_Pending_Action,
  CalculatedVariables,
  AssociationTypeCode
} from '@lenda/models/loanmodel';
import { exceptionlevel, validationlevel } from '@lenda/models/commonmodels';
import { Loan_Exception } from '@lenda/models/loan/loan_exceptions';
import { RefQuestions, LoanQResponse } from '@lenda/models/loan-response.model';
import { Loan_Validation } from '@lenda/models/loan/loan_validation';
import { getRandomInt } from '../utility/randomgenerator';

import { Helper } from '@lenda/services/math.helper';

import { LoanDocumentWorkerService } from './loandocumentworker.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import {
  Eval_Helper,
  Local_Table
} from '@lenda/services/exception-helper.service';

export type Validation_Exception_Type =
  | RefQuestions
  | RefBudgetExpenseType
  | RefCollateralActionDiscAdj
  | RefCollateralInsDisc
  | RefCollateralMktDisc
  | Loan_Budget;


/**
 * IExceptionWorkerService
 */
interface IExceptionWorkerService {
  /**
   * Process All Exception
   */
  Process_Exceptions(Loan: loan_model): void;

  /**
   * Trigger an Exception from Outside with an Exception ID and Level
   * @param Loan Loan Object
   * @param Exception_ID Ref Exception Id
   * @param entity Entity (i.e. Disburse Details)
   * @param Level Exception Level
   */
  Process_Exception_By_ID(
    Loan: loan_model,
    Exception_ID: number,
    entity: any,
    Level: exceptionlevel,
    Local_Table_Name: string
  ): void;

  GetExceptionByID(Exception_ID: number, Loan: loan_model, entity: any): Loan_Exception;

  CalculateHyperfieldVariables(localLoanObject: loan_model): void;

  LogQuestionExceptionIfApplicable(response : LoanQResponse, Loan: loan_model): void;
}

@Injectable()
export class ExceptionWorkerService implements IExceptionWorkerService {
  private refdata: RefDataModel;

  constructor(
    private localstorage: LocalStorageService,
    private logger: LoggingService,
    private loandocworker: LoanDocumentWorkerService
  ) {
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);

    this.localstorage
      .observe(environment.referencedatakey)
      .subscribe((d: RefDataModel) => {
        this.refdata = d;
      });
  }

  //#region Process All Exceptions

  /**
   * Process All Exception
   */
  public Process_Exceptions(Loan: loan_model) {

    if (!Loan.Loan_Exceptions) {
      Loan.Loan_Exceptions = [];
    }

    console.time('Total Exception Calculation');

    this.Process_Ref_Exceptions(Loan);

    console.timeEnd('Total Exception Calculation');
  }


  private Process_Ref_Exceptions(Loan: loan_model) {
    try {
      Loan.Loan_Documents.forEach(a => {
        a.Doc_Needed_Ind = 0;
      });

      Loan.Loan_Documents.filter(a => a.Standard_Document_Ind == 1 || a.Document_Type_ID == 85).forEach(a => {
        a.Doc_Needed_Ind = 1;
      });

      Loan.Loan_Exceptions.filter(a => a.Custom_Ind == 1).forEach(e => {
        e.Needed_Ind = 1;
      });

      const Exceptions = Loan.Loan_Exceptions.filter(a => !a.Custom_Ind);

      Exceptions.filter(a => !String(a.Exception_Key).includes('Question')).forEach(a => {
        a.Needed_Ind = 0;
      });

      Exceptions.filter(a => String(a.Exception_Key).includes('Question')).forEach(a => {
        a.Needed_Ind = 1;
        this.Update_Exception_Ind_Question_Exception(a, Loan);
      });

      this.refdata.RefExceptions.filter(a => !a.Custom_Ind).forEach(refException => {
        try {
          refException.Exception_Details = new Exception_Detail();
          this.Process_Exception(refException, Loan);
        } catch (ex) {
          this.logger.checkandcreatelog(
            0,
            'Process_Exceptions - RefException',
            `${ex.message} - ${JSON.stringify(refException)}`
          );
        }
      });

      this.loandocworker.Remove_Not_Needed_Document(Loan);
      this.Remove_Not_Needed_Loan_Exceptions(Loan);

      this.localstorage.store(environment.loankey, Loan);
      this.localstorage.store(environment.referencedatakey, this.refdata);
    } catch (ex) {
      this.logger.checkandcreatelog(
        2,
        'Process_Exceptions - RefExceptions',
        ex.message
      );
    }
  }

  private Process_Exception(refException: Ref_Exception, loan: loan_model, VE?: Validation_Exception_Type) {
    if (!refException.Level_1_Hyper_Code && !refException.Level_2_Hyper_Code) {
      return;
    }

    let codes: Array<string>;

    if (refException.Level_1_Hyper_Code) {
      codes = refException.Level_1_Hyper_Code.split('.');
    }

    if (refException.Level_2_Hyper_Code) {
      codes = refException.Level_2_Hyper_Code.split('.');
    }

    let tableName = codes[0];

    let Local_Table_Name = Eval_Helper.Map_Table(tableName);

    if (Local_Table_Name == Local_Table.LoanMaster) {
      this.Log_Ref_Exception(refException, loan, Local_Table_Name);
    } else if (Local_Table_Name.includes('Association_')) {
      let entities = loan.Association;

      if (entities) {
        let Assoc_Type_Code = Local_Table_Name.substring(
          Local_Table_Name.indexOf('_') + 1
        );

        entities = entities.filter(a => a.Assoc_Type_Code == Assoc_Type_Code && a.ActionStatus != 3);
        entities.forEach(entity => {
          this.Log_Ref_Exception(refException, loan, Local_Table_Name, entity);
        });
      }
    } else if(Local_Table_Name == Local_Table.Questions) {
      if(VE) {
        this.Log_Ref_Exception(refException, loan, Local_Table_Name, VE);
      }
    } else if(Local_Table_Name == Local_Table.CalculatedVariables) {
      this.Log_Ref_Exception(refException, loan, Local_Table_Name, loan.CalculatedVariables);
    } else if(Local_Table_Name == Local_Table.Assoc_Replication) {
      this.Log_Ref_Exception(refException, loan, Local_Table_Name, null, 1);
    } else if(Local_Table_Name == Local_Table.COLLATERAL) {
      let entities = loan.LoanCollateral || [];

      entities = entities.filter(a => a.ActionStatus != 3 && a.Exception_Ind == 'Y');

      entities.forEach(entity => {
        this.Log_Ref_Exception(refException, loan, Local_Table_Name, entity);
      });
    } else {
      let entities = eval(`loan.` + Local_Table_Name) as Array<any>;
      if (entities) {
        entities.filter(a => {
          let actionStatus = a.ActionStatus || a.Action_Status;
          return actionStatus != 3;
        }).forEach(entity => {
          this.Log_Ref_Exception(refException, loan, Local_Table_Name, entity);
        });
      }
    }
  }

  //#endregion Process All Exceptions

  /**
   * Trigger an Exception from Outside with an Exception ID and Level
   * @param Loan Loan Object
   * @param Exception_ID Ref Exception Id
   * @param entity Entity (i.e. Disburse Details)
   * @param Level Exception Level
   */
  public Process_Exception_By_ID(
    Loan: loan_model,
    Exception_ID: number,
    entity: any,
    Level: exceptionlevel,
    Local_Table_Name: string = undefined
  ) {
    try {
      let exception = this.refdata.RefExceptions.find(a => a.Exception_ID == Exception_ID);
      if(exception) {
        if(!Local_Table_Name) {
          let codes: Array<string>;

          if (exception.Level_1_Hyper_Code) {
            codes = exception.Level_1_Hyper_Code.split('.');
          }

          if (exception.Level_2_Hyper_Code) {
            codes = exception.Level_2_Hyper_Code.split('.');
          }

          let tableName = codes[0];

          Local_Table_Name = Eval_Helper.Map_Table(tableName);
        }

        exception.Exception_Details = new Exception_Detail();
        this.Log_Ref_Exception(exception, Loan, Local_Table_Name, entity, Level, false);
      }
    } catch (ex) {
      this.logger.checkandcreatelog(
        2,
        'Exception_Service - Process_Exception_By_ID',
        `${ex.message} - Exception_ID : ${Exception_ID}`
      );
    }
  }

  private Log_Ref_Exception(
    refEx: Ref_Exception,
    loan: loan_model,
    Local_Table_Name: string,
    entity?: any,
    level: exceptionlevel = undefined,
    calcLevel = true
  ) {
    let Exception_Level: exceptionlevel;
    let Level1: boolean;
    let Level2: boolean;
    let Custom_Ind = calcLevel ? 0 : 1;

    if(calcLevel) {
      if (refEx.Level_1_Hyper_Code) {
        let Level_Hyper_Code = refEx.Level_1_Hyper_Code;

        if(Level_Hyper_Code.includes('OR') || Level_Hyper_Code.includes('AND')) {
          try {
            Level1 = this.evaluateHypercode(Level_Hyper_Code, loan, entity);
            if (Level1) {
              Exception_Level = exceptionlevel.level1;
              refEx.Exception_Details.Level_1_Value = true;
            }
          } catch (ex) {
            let logMessage = `${JSON.stringify(ex)} :: ${refEx.Level_1_Hyper_Code}`;
            this.logger.checkandcreatelog(2, 'Process_Exception', logMessage);
          }
        } else {
          let hypercode = refEx.Level_1_Hyper_Code.substring(refEx.Level_1_Hyper_Code.indexOf('.') + 1, refEx.Level_1_Hyper_Code.length);

          try {
            Level1 = this.Evaluate_Expression(hypercode, loan, entity);
            if (Level1) {
              Exception_Level = exceptionlevel.level1;
              refEx.Exception_Details.Level_1_Value = true;
            }
          } catch (ex) {
            let logMessage = `${JSON.stringify(ex)} :: ${refEx.Level_1_Hyper_Code}`;
            this.logger.checkandcreatelog(2, 'Process_Exception', logMessage);
          }
        }
      }

      if (refEx.Level_2_Hyper_Code) {
        let Level_Hyper_Code = refEx.Level_2_Hyper_Code;

        if(Level_Hyper_Code.includes('OR') || Level_Hyper_Code.includes('AND')) {
          try {
            Level2 = this.evaluateHypercode(Level_Hyper_Code, loan, entity);
            if (Level2) {
              Exception_Level = exceptionlevel.level2;
              refEx.Exception_Details.Level_2_Value = true;
            }
          } catch (ex) {
            let logMessage = `${JSON.stringify(ex)} :: ${refEx.Level_2_Hyper_Code}`;
            this.logger.checkandcreatelog(2, 'Process_Exception', logMessage);
          }
        } else {
          let hypercode = refEx.Level_2_Hyper_Code.substring(refEx.Level_2_Hyper_Code.indexOf('.') + 1, refEx.Level_2_Hyper_Code.length);

          try {
            Level2 = this.Evaluate_Expression(hypercode, loan, entity);
            if (Level2) {
              Exception_Level = exceptionlevel.level2;
              refEx.Exception_Details.Level_2_Value = true;
            }
          } catch (ex) {
            let logMessage = `${JSON.stringify(ex)} :: ${refEx.Level_2_Hyper_Code}`;
            this.logger.checkandcreatelog(2, 'Process_Exception', logMessage);
          }
        }
      }
    } else {
      Exception_Level = level;
    }

    // Log an Exception into Loan_Exception
    let LE = this.Log_Exception(
      refEx,
      loan,
      Exception_Level,
      Local_Table_Name,
      entity,
      Custom_Ind
    );

    let exisitingLE = loan.Loan_Exceptions.find(a => {
      return (
        a.Exception_ID == LE.Exception_ID && a.Exception_Key == LE.Exception_Key && a.ActionStatus != 3
      );
    });

    if (Level1 || Level2 || Exception_Level) {
      if (refEx.Mitigation_Text_Required_Ind == 1) {
        if (!exisitingLE) {
          LE.Needed_Ind = 1;
          loan.Loan_Exceptions.push(LE);

          // Support Ind
          if(refEx.Support_Required_Ind) {
            this.Add_Support_Pending_Action(refEx, LE, loan);
          }
        } else {
          exisitingLE.Exception_ID_Level = LE.Exception_ID_Level;
          exisitingLE.Exception_ID_Text = LE.Exception_ID_Text;
          exisitingLE.Custom_Ind = LE.Custom_Ind;
          exisitingLE.Needed_Ind = 1;
          if (exisitingLE.ActionStatus != 1) {
            exisitingLE.ActionStatus = 2;
          }
        }
      }

      // Support Ind
      if(refEx.Support_Required_Ind) {
        this.Add_Support_Pending_Action(refEx, exisitingLE, loan);
      }

      // Highlight Question
      if(Local_Table_Name == Local_Table.Questions) {
        this.Highlight_Question(entity, Exception_Level);
      }

      // Pending Action
      if (refEx.Pending_Action_Required_Ind) {
        this.Process_Pending_Action(refEx, loan, Level1, Level2);
      }
    } else {
      if (!!exisitingLE) {
         // Delete Question Exception
        if(Local_Table_Name == Local_Table.Questions) {
          this.Remove_Question_Exception(exisitingLE, loan);
        } else {
          if(exisitingLE.ActionStatus == 1) {
            exisitingLE.Needed_Ind = 0;
          } else {
            exisitingLE.Needed_Ind = 0;
          }
        }
      }

      this.Remove_Existing_Pending_Action(refEx, loan);
      this.Remove_Support_Pending_Action(refEx, loan);

      if(!calcLevel) {
        this.Remove_Exception(exisitingLE, loan);
        this.loandocworker.Remove_Not_Needed_Document_For_Exception(loan, refEx, exisitingLE);
      }
    }

    // Document
    if (refEx.Document_Required_Ind) {
      this.loandocworker.Process_Exception_Document(
        refEx,
        loan,
        Exception_Level,
        entity
      );
    }

    if(!exisitingLE && Exception_Level) {
      this.Update_Exception_Ind(LE, Local_Table_Name, loan, entity);
    } else if(exisitingLE) {
      this.Update_Exception_Ind(exisitingLE, Local_Table_Name, loan, entity);
    }
  }

  private Remove_Exception(exisitingLE: Loan_Exception, loan: loan_model) {
    if (exisitingLE.ActionStatus == 1) {
      loan.Loan_Exceptions.splice(loan.Loan_Exceptions.indexOf(exisitingLE), 1);
    } else {
      exisitingLE.ActionStatus = 3;
      exisitingLE.Needed_Ind = 0;
    }
  }

  private Add_Support_Pending_Action(ex: Ref_Exception, LE: Loan_Exception, loan: loan_model) {
    let refPA = this.refdata.RefPendingActions.find(a => a.PA_Group == 'SUP');

    if(refPA) {
      let exisitingPA = loan.User_Pending_Actions.filter(A => A.Trigger_ID == ex.Exception_ID && A.Ref_PA_ID == refPA.Ref_PA_ID);

      if(exisitingPA.length == 0) {
        let PA = this.Get_New_Pending_Action(loan, refPA, 'SUP', ex.Exception_ID, ex.Exception_ID_Text);

        if(ex.Support_Role_Type_Code_1 && !LE.Support_User_ID_1) {
          PA.Support_Roles.push(ex.Support_Role_Type_Code_1);
        }

        if(ex.Support_Role_Type_Code_2 && !LE.Support_User_ID_2) {
          PA.Support_Roles.push(ex.Support_Role_Type_Code_2);
        }

        if(ex.Support_Role_Type_Code_3 && !LE.Support_User_ID_3) {
          PA.Support_Roles.push(ex.Support_Role_Type_Code_3);
        }

        if(ex.Support_Role_Type_Code_4 && !LE.Support_User_ID_4) {
          PA.Support_Roles.push(ex.Support_Role_Type_Code_4);
        }

        if(PA.Support_Roles.length > 0) {
          loan.User_Pending_Actions.push(PA);
          if(LE.ActionStatus == 0) {
            LE.ActionStatus = 2;
          }
        }
      }
    }
  }

  private Remove_Support_Pending_Action(ex: Ref_Exception, loan: loan_model) {
   try {
    const PAs = loan.User_Pending_Actions.filter(a => a.Trigger_ID == ex.Exception_ID && a.PA_Group_Code == 'SUP');
    PAs.forEach(pa => {
      if(pa.ActionStatus == 1) {
        loan.User_Pending_Actions.splice(loan.User_Pending_Actions.indexOf(pa), 1);
      } else {
        pa.ActionStatus = 3;
      }
    });
   } catch (Ex) {}
  }

  private Remove_Question_Exception(exisitingLE: Loan_Exception, loan: loan_model) {
    if (exisitingLE.ActionStatus == 1) {
      exisitingLE.Needed_Ind = 0;
      let index = loan.Loan_Exceptions.indexOf(exisitingLE);
      if (index > -1) {
        loan.Loan_Exceptions.splice(index, 1);
      }
    } else {
      exisitingLE.Needed_Ind = 0;
      exisitingLE.ActionStatus = 3;
    }
  }

  private Log_Exception(
    exception: Ref_Exception,
    loan: loan_model,
    level: exceptionlevel,
    Local_Table_Name: string,
    entity: any,
    Custom_Ind = 0
  ) {
    let ex = new Loan_Exception();

    ex.Loan_Full_ID = loan.Loan_Full_ID;
    ex.Exception_ID = exception.Exception_ID;
    ex.Exception_ID_Text = exception.Exception_ID_Text;

    if (exception.Exception_ID_Text.includes('##')) {
      ex.Exception_ID_Text = Eval_Helper.ReplaceHash_Ex(
        exception.Exception_ID_Text,
        loan,
        Local_Table_Name,
        entity
      );
    }

    ex.Exception_Key = Eval_Helper.Get_Exception_Key(
      Local_Table_Name,
      loan,
      entity,
      this.refdata
    );

    ex.Custom_Ind = Custom_Ind || exception.Custom_Ind;

    ex.Exception_ID_Level = level || -1;
    ex.Tab_ID = exception.Tab_ID;
    ex.Chevron_ID = exception.Chevron_ID;

    ex.Mitigation_Ind = exception.Mitigation_Text_Required_Ind;
    ex.Sort_Order = exception.Sort_Order;

    ex.Support_Ind = exception.Support_Required_Ind;
    ex.Support_Role_Type_Code_1 = exception.Support_Role_Type_Code_1;
    ex.Support_Role_Type_Code_2 = exception.Support_Role_Type_Code_2;
    ex.Support_Role_Type_Code_3 = exception.Support_Role_Type_Code_3;
    ex.Support_Role_Type_Code_4 = exception.Support_Role_Type_Code_4;

    ex.Document_Type_ID = exception.Document_Type_ID || 0;
    ex.Complete_Ind = 0;

    ex.ActionStatus = 1;
    return ex;
  }

  private Process_Pending_Action(
    EV: Ref_Exception | RefValidation,
    loan: loan_model,
    Level1: boolean,
    Level2: boolean
  ) {
    let Add_Pending_Action = false;

    switch (EV.Pending_Action_Required_Ind) {
      case Pending_Action_Required.Level1AndLevel2:
        if (Level1 && Level2) {
          Add_Pending_Action = true;
        }
        break;

      case Pending_Action_Required.Level2Only:
        if (Level2) {
          Add_Pending_Action = true;
        }
        break;
    }

    if (Add_Pending_Action) {
      if (EV instanceof Ref_Exception) {
        this.Add_Pending_Action_By_Code(
          EV.Exception_ID,
          EV.Exception_ID_Text,
          EV.Pending_Action_Code,
          loan
        );
      } else {
        this.Add_Pending_Action_By_Code(
          EV.Validation_ID,
          EV.Validation_ID_Text,
          EV.Pending_Action_Code,
          loan
        );
      }
    }
  }

  private Add_Pending_Action_By_Code(
    ID: number,
    Text: string,
    PA_Code: string,
    Loan: loan_model
  ) {
    let Ref_PA = this.refdata.RefPendingActions.find(a => a.PA_Code == PA_Code);

    if (Ref_PA) {
      let PA = this.Get_New_Pending_Action(Loan, Ref_PA, PA_Code, ID, Text);
      Loan.User_Pending_Actions.push(PA);
    }
  }

  private Get_New_Pending_Action(
    Loan: loan_model,
    Ref_PA: RefPendingAction,
    PA_Code: string,
    ID: number,
    Text: string
  ) {
    let PA = new User_Pending_Action();
    PA.User_PA_ID = getRandomInt(Math.pow(10, 5), Math.pow(10, 6));
    PA.User_ID = Loan.LoanMaster.Loan_Officer_ID;
    PA.PA_Level = Ref_PA.PA_Level;
    PA.PA_Name = Ref_PA.PA_Name;
    PA.PA_Group_Code = Ref_PA.PA_Group;
    PA.PA_Code = PA_Code;
    PA.Loan_Full_ID = Loan.Loan_Full_ID;
    PA.Ref_PA_ID = Ref_PA.Ref_PA_ID;
    PA.Status = 1;
    PA.From_User_ID = 0;
    PA.Trigger_ID = ID;
    PA.Custom_Txt = Text;
    let currentTime = new Date().toISOString();
    PA.Added_On = currentTime;
    PA.Modified_On = currentTime;
    PA.ActionStatus = 1;
    return PA;
  }

  public Add_Tolerance_Level_PA(Loan: loan_model) {
    if(Loan && Loan.LoanMaster.Loan_Tolerance_Level_Ind) {
      let PA_Code = Loan.LoanMaster.Loan_Tolerance_Level_Ind == 1 ? 'WST2' : 'WST3';
      let Ref_PA = this.refdata.RefPendingActions.find(a => a.PA_Code == PA_Code);

      let existingPA = Loan.User_Pending_Actions.find(
        a =>
          a.PA_Code == PA_Code &&
          a.ActionStatus != 3
      );

      if(Ref_PA && !existingPA) {
        let PA = this.Get_New_Pending_Action(
          Loan,
          Ref_PA,
          PA_Code,
          0,
          'Tolerance Level Pending Action'
        );
        Loan.User_Pending_Actions.push(PA);

        if(PA_Code == 'WST3') {
          this.Remove_Pending_Action(Loan, 'WST2');
        } else {
          this.Remove_Pending_Action(Loan, 'WST3');
        }
      }
    }
  }

  public Remove_Pending_Action(loan: loan_model, PA_Code: string) {
    let existingPA = loan.User_Pending_Actions.find(
      a =>
        a.PA_Code == PA_Code &&
        a.ActionStatus != 3
    );

    if(existingPA) {
      if(existingPA.ActionStatus == 1) {
        loan.User_Pending_Actions.splice(
          loan.User_Pending_Actions.indexOf(existingPA), 1
        );
      } else {
        existingPA.ActionStatus = 3;
      }
    }
  }

  private Remove_Existing_Pending_Action(
    ex: Ref_Exception | RefValidation,
    loan: loan_model
  ) {
    let Existing_Pending_Actions = loan.User_Pending_Actions.filter(a => {
      if (ex instanceof Ref_Exception) {
        return (
          a.Trigger_ID == ex.Exception_ID &&
          a.PA_Code == ex.Pending_Action_Code
        );
      }

      return (
        a.Trigger_ID == ex.Validation_ID &&
        a.PA_Code == ex.Pending_Action_Code
      );
    });

    if (Existing_Pending_Actions.length > 0) {
      Existing_Pending_Actions.forEach(a => {
        a.ActionStatus = 3;
      });
    }
  }

  private Process_Validation_Exception(
    VE: Validation_Exception_Type,
    Loan: loan_model
  ) {
    if (VE.Validation_Exception_Ind) {
      if (VE.Exception_ID) {
        try {
          let refException = this.refdata.RefExceptions.find(
            a => a.Exception_ID == VE.Exception_ID
          );

          if (refException) {
            refException.Exception_Details = new Exception_Detail();
            this.Process_Exception(refException, Loan, VE);
          }
        } catch (ex) {
          this.logger.checkandcreatelog(
            2,
            'Process_Validation_Exception',
            ex.message
          );
        }
      }

      if (VE.Validation_ID) {
        try {
          let refValidation = this.refdata.RefValidations.find(
            a => a.Validation_ID == VE.Validation_ID
          );

          if (refValidation) {
            this.Process_Validation(VE, refValidation, Loan);
          }
        } catch (ex) {
          this.logger.checkandcreatelog(
            2,
            'Process_Validation_Exception',
            ex.message
          );
        }
      }
    }
  }

  private Process_Validation(
    VE: Validation_Exception_Type,
    refValidation: RefValidation,
    Loan: loan_model
  ) {
    let Validation_Level: validationlevel;

    let Level1: boolean;
    let Level2: boolean;

    if (refValidation.Level_1_Hyper_Code) {
      let exp = `loan.${refValidation.Level_1_Hyper_Code}`;
      try {
        Level1 = eval(exp);
        if (Level1) {
          Validation_Level = validationlevel.level1;
        }
      } catch (ex) {
        let logMessage = `${JSON.stringify(ex.message)} :: ${exp}`;
        this.logger.checkandcreatelog(2, 'Process_Validation', logMessage);
      }
    }

    if (refValidation.Level_2_Hyper_Code) {
      let exp = `loan.${refValidation.Level_2_Hyper_Code}`;
      try {
        Level2 = eval(exp);
        if (Level2) {
          Validation_Level = validationlevel.level2;
        }
      } catch (ex) {
        let logMessage = `${JSON.stringify(ex.message)} :: ${exp}`;
        this.logger.checkandcreatelog(2, 'Process_Validation', logMessage);
      }
    }

    if (Level1 || Level2 || Validation_Level) {
      let Validation = this.Log_Validation(
        VE,
        refValidation,
        Loan,
        Validation_Level
      );

      if (!Loan.Loan_Validations) {
        Loan.Loan_Validations = [];
      }

      Loan.Loan_Validations.push(Validation);

      if (refValidation.Pending_Action_Required_Ind) {
        this.Process_Pending_Action(refValidation, Loan, Level1, Level2);
      }
    } else {
      this.Remove_Existing_Validation(VE, Loan);
    }
  }

  private Log_Validation(
    VE: Validation_Exception_Type,
    refValidation: RefValidation,
    Loan: loan_model,
    Level: validationlevel
  ) {
    let v = new Loan_Validation();
    v.Loan_Full_ID = Loan.Loan_Full_ID;
    v.ActionStatus = 1;
    v.Tab_ID = refValidation.Tab_ID.toString();
    v.Validation_ID = refValidation.Validation_ID;

    v.Validation_ID_Level = Level;
    v.Validation_ID_Text = refValidation.Validation_ID_Text;

    return v;
  }

  private Remove_Existing_Validation(
    VE: Validation_Exception_Type,
    Loan: loan_model
  ) {
    let v = Loan.Loan_Validations.find(
      a => a.Validation_ID == (VE.Exception_ID || VE.Validation_ID)
    );
  }

  private Remove_Not_Needed_Loan_Exceptions(Loan: loan_model) {
    _.remove(Loan.Loan_Exceptions, a => a.ActionStatus == 1 && !a.Needed_Ind);

    Loan.Loan_Exceptions.filter(a => a.ActionStatus != 1 && !a.Needed_Ind)
      .forEach(a => {
        a.ActionStatus = 3;
      });
  }

  private Update_Exception_Ind(ex: Loan_Exception, Local_Table_Name: string, Loan: loan_model, entity: any) {
    let indicators = [];
    if(ex.Mitigation_Text) {
      indicators.push(1);
    } else {
      indicators.push(0);
    }

    if(ex.Support_Ind) {
      if(ex.Support_Role_Type_Code_1) {
        if(ex.Support_User_ID_1) {
          indicators.push(1);
        } else {
          indicators.push(0);
        }
      }

      if(ex.Support_Role_Type_Code_2) {
        if(ex.Support_User_ID_2) {
          indicators.push(1);
        } else {
          indicators.push(0);
        }
      }

      if(ex.Support_Role_Type_Code_3) {
        if(ex.Support_User_ID_3) {
          indicators.push(1);
        } else {
          indicators.push(0);
        }
      }

      if(ex.Support_Role_Type_Code_4) {
        if(ex.Support_User_ID_4) {
          indicators.push(1);
        } else {
          indicators.push(0);
        }
      }
    }

    if(ex.Document_Type_ID) {
      let ref_doc = this.refdata.Ref_Document_Types.find(a => a.Document_Type_ID == ex.Document_Type_ID);
      if(ref_doc) {
        let key = Eval_Helper.Get_Document_Key(ref_doc, Local_Table_Name, Loan, entity, this.refdata);
        let Temp_ID = Eval_Helper.Temp_ID(ref_doc, Local_Table_Name, Loan, entity);

        let doc_key = JSON.stringify({
          Key: key,
          ID: Temp_ID
        });

        let doc = Loan.Loan_Documents.find(a => a.Document_Type_ID == ex.Document_Type_ID && a.Document_Key == doc_key);
        if(doc) {
          if(doc.Upload_User_ID) {
            indicators.push(1);
          } else {
            indicators.push(0);
          }
        }
      }

    }

    if(indicators.some(a => a == 0)) {
      ex.Complete_Ind = 0;
    } else {
      ex.Complete_Ind = 1;
    }
  }

  //#region [Question Exception]

  public LogQuestionExceptionIfApplicable(response : LoanQResponse, Loan: loan_model) {
    let respectiveQue = (this.refdata.RefQuestions as Array<RefQuestions>).find(que=>response.Question_ID == que.Question_ID);

    if(respectiveQue) {
      try {
        if(respectiveQue.Validation_Exception_Ind) {
          this.Reset_Question_CSS_Classes(response.Question_ID);
          this.Process_Validation_Exception(respectiveQue, Loan);
        }
      } catch(e) {
        console.error(`Error occurred while logging exception for qustion ${JSON.stringify(respectiveQue)}`);
      }
    }
  }

  private Update_Exception_Ind_Question_Exception(Loan_Exception: Loan_Exception, Loan: loan_model) {
    try {
      if(Loan_Exception && Loan_Exception.Exception_Key) {
        let question_id = +Loan_Exception.Exception_Key.split('_')[1];
        let qn = this.refdata.RefQuestions.find(a => a.Question_ID == question_id);
        if(qn) {
          this.Update_Exception_Ind(Loan_Exception, 'Questions', Loan, qn);
        }
      }
    } catch (ex) {
      this.logger.checkandcreatelog(
        0,
        'Exception Worker - Update_Exception_Ind_Question_Exception',
        JSON.stringify(ex)
      );
    }
  }

  /**
   * Reset CSS Class on Question Response
   * @param Question_ID Question ID
   */
  private Reset_Question_CSS_Classes(Question_ID){
    if(Question_ID){
      let items = document.querySelectorAll('#questions_' + Question_ID + ' .question-input-field');
      Array.from(items).forEach((item) => {
        item.classList.remove('cell-exception','exception-1','exception-2');
      });
    }
  }

  /**
   * Highlight Question Radio Buttons / Inputs based on Exception.
   * @param q The Question Object.
   * @param exLevel The level of exception.
   */
  private Highlight_Question(q: RefQuestions, exLevel: number){
    if(q.Question_ID){

      let questions: NodeListOf<HTMLElement>;

      if(q.Questions_Cat_Code == 1) {
        let classAnswer = q.FC_Response_Detail == 'Yes' && q.Choice1 == 'Yes' ? 'questions-container__answer--choice1' :
        q.FC_Response_Detail == 'Yes' && q.Choice2 == 'Yes' ? 'questions-container__answer--choice2' :
        q.FC_Response_Detail == 'No' && q.Choice1 == 'No' ? 'questions-container__answer--choice1' :
        q.FC_Response_Detail == 'No' && q.Choice2 == 'No' ? 'questions-container__answer--choice2' : 'no-value';

        questions = document.querySelectorAll('#questions_' + q.Question_ID + ' .' + classAnswer);
      } else {
        questions = document.querySelectorAll('#questions_' + q.Question_ID + ' .question-input-field');
      }

      Array.from(questions).forEach((ques) => {
        let classes =  this.getQuestionClass(exLevel);
          classes.forEach(classSCSS => {
            ques.classList.add(classSCSS)
        });
      });
    }
  }

  /**
   * Provide Exception Classes.
   * @param level The level of exception.
   */
  private getQuestionClass(level: number){
    switch(level){
      case 1:
        return ['cell-exception','exception-1'];
      case 2:
        return ['cell-exception','exception-2'];
      default:
        return [];
    }
  }

  //#endregion [Question Exception]

  /**
   * Will evaluate the expression.
   * if entity is passed, expression variable should only send the key to find in it
   * if entity is NOT passed, it will try to evalulate expression in local loan object
   * @param expression the look up key, (ex: FSN, LoanMaster[0].Crop_Year)
   * @param entity [optional] if the expression should be looked up from entity object instead of local loan object
   */
  private Evaluate_Expression(
    expression: string,
    Loan: loan_model,
    entity: any = undefined
  ) {
    let exp: string;

    if (entity) {
      exp = 'entity.' + expression;
    } else {
      exp = 'Loan.LoanMaster.' + expression;
    }

    let evalValue = eval(exp);

    return Helper.isDecimal(evalValue)
      ? (evalValue as number).toFixed(2)
      : evalValue;
  }

  public CalculateHyperfieldVariables(localLoanObject: loan_model) {
    try{
      if(environment.isDebugModeActive) console.time('Calculate Exception Variables');

      if(localLoanObject ){
        let loanMaster  = localLoanObject.LoanMaster as LoanMaster;

        if(!localLoanObject.CalculatedVariables) {
          localLoanObject.CalculatedVariables = <CalculatedVariables>{};
        }

        if(localLoanObject.Association) {
          //Total CreditRef
          let creditRefs = localLoanObject.Association.filter(assoc=>assoc.Assoc_Type_Code == AssociationTypeCode.CreditRererrence && assoc.ActionStatus!=3);
          localLoanObject.CalculatedVariables.FC_Total_CreditRef = creditRefs.length;

          //Credit ref without Response
          let creditRefsWithNOResp = creditRefs.filter(crf=>!crf.Response);
          localLoanObject.CalculatedVariables.FC_Total_CRF_Response = creditRefsWithNOResp.length;

          //Crefit Ref with Nuetral or Negetive respone
          let creditRefWithNegResp = creditRefs.filter(crf=> ['Neutral','Negative'].includes(crf.Response));
          localLoanObject.CalculatedVariables.FC_Total_CRF_Neg_Response = creditRefWithNegResp.length;
        }

        //AdCol Contract Type
        let contractsWExc = this.refdata.ContractType.filter(contract => contract.Exception_Ind == 'Y').map(contract => contract.Contract_Type_Code);
        let countContractWExc = localLoanObject.LoanCollateral.filter(col => contractsWExc.includes(col.Contract_Type_Code));
        localLoanObject.CalculatedVariables.FC_ADCOL_Total_Contract_Type_Exception_Ind = countContractWExc.length;

        //AdCol Contract Measure Code Type
        let measWExc = this.refdata.MeasurementType.filter(meas => meas.Exception_Ind == 'Y').map(meas => meas.Measurement_Type_Code);
        let countMeasWExc = localLoanObject.LoanCollateral.filter(meas => measWExc.includes(meas.Measure_Code));
        localLoanObject.CalculatedVariables.FC_ADCOL_Total_Measure_Code_Exception_Ind = countMeasWExc.length;

        //Mtk Contract Type
        // LoanMarketingContracts.Crop_Type_Code
        let countMktContractWExc = localLoanObject.LoanMarketingContracts.filter(mkt => contractsWExc.includes(mkt.Contract_Type_Code));
        localLoanObject.CalculatedVariables.FC_MKT_Total_Contract_Type_Exception_Ind = countMktContractWExc.length;

        //Highlight

        // Crop yield : no of years of history provided
        if(localLoanObject.CropYield){

          let cropYear = loanMaster.Crop_Year;

          let years = [];

          for(let i=1; i<8;i++){
            years.push(cropYear-i);
          };

          localLoanObject.CropYield.forEach(crop => {
            crop.FC_Year_History_Provided = 0;
            years.forEach(year => {
              if(crop[year]){
                crop.FC_Year_History_Provided++;
              }
            });
          });
        }

        localLoanObject.CalculatedVariables.Total_Farmed_Acres = 0;
        localLoanObject.CalculatedVariables.Total_Acres = 0;

        if(localLoanObject.Farms) {
          localLoanObject.Farms.forEach(f => {
            localLoanObject.CalculatedVariables.Total_Acres += f.FC_Total_Acres;
            localLoanObject.CalculatedVariables.Total_Farmed_Acres += f.FC_Total_Used_Acres;
          });
        }

        let yearsFarming = loanMaster.Year_Begin_Farming ? new Date().getFullYear() - (loanMaster.Year_Begin_Farming ||  new Date().getFullYear()): 0;
        localLoanObject.CalculatedVariables.FC_Farming_Years = yearsFarming;

        localLoanObject.CalculatedVariables.FC_Total_Farm_Without_Rent = localLoanObject.Farms.filter(
          a =>
            a.ActionStatus != 3 &&
            !a.Owned &&
            !a.Cash_Rent_Total &&
            !a.Share_Rent
        ).length;

        localLoanObject.CalculatedVariables.FC_Total_Farm_With_Rent_Waiver = localLoanObject.Farms.filter(
          a =>
           a.ActionStatus != 3 &&
           !a.Owned &&
           a.Waived_Rent > 0
        ).length;

        localLoanObject.CalculatedVariables.FC_Total_Farm_With_Permission_To_Insure = localLoanObject.Farms.filter(
          a =>
            a.ActionStatus != 3 &&
            a.Permission_To_Insure == 1
        ).length;

        localLoanObject.CalculatedVariables.FC_Valid_LO_Role = localLoanObject.LoanMaster.Loan_Officer_Role == 'LO' || localLoanObject.LoanMaster.Loan_Officer_Role == 'ML';
        localLoanObject.CalculatedVariables.Loan_Officer_Role = localLoanObject.LoanMaster.Loan_Officer_Role;

        if(localLoanObject.Farms) {
          localLoanObject.Farms.forEach(farm => {
            if(farm.Owned == 0 && farm.Permission_To_Insure == 1) {
              farm.FC_Perm_To_Ins = true;
            }
          });
        }

      if(environment.isDebugModeActive) console.timeEnd( 'Calculate Exception Variables');
      }
    } catch {
    //move one
    }
  }


  public GetExceptionByID(Exception_ID: number, Loan: loan_model, entity: any) {
    try {
      let refEx = this.refdata.RefExceptions.find(a => a.Exception_ID == Exception_ID);
      if(refEx) {
        let codes: Array<string>;

        if (refEx.Level_1_Hyper_Code) {
          codes = refEx.Level_1_Hyper_Code.split('.');
        }

        if (refEx.Level_2_Hyper_Code) {
          codes = refEx.Level_2_Hyper_Code.split('.');
        }

        let tableName = codes[0];

        let Local_Table_Name = Eval_Helper.Map_Table(tableName);
        let key = Eval_Helper.Get_Exception_Key(Local_Table_Name, Loan, entity, this.refdata);

        let exception = Loan.Loan_Exceptions.find(
          a => a.Exception_Key == key && a.Exception_ID == Exception_ID
        );

        if(exception) {
          return exception;
        }
        return null;
      }
      return null;
    } catch {
      return null;
    }
  }

  // ONLY OR SEPERATED OR ONLY AND SEPERATED
  private evaluateHypercode(
    hypercode: string,
    loan: loan_model,
    entity: any
  ) {
    let hypercodes: Array<string> = [];
    let operator: string;

    if(hypercode.includes('OR')) {
      operator = 'OR';
      hypercodes = hypercode.split(/OR/);
    } else if (hypercode.includes('AND')) {
      operator = 'AND';
      hypercodes = hypercode.split(/AND/);
    } else {
      operator = null;
      hypercodes.push(hypercode);
    }

    let result = operator == 'AND';

    hypercodes.map(a => a.trim()).filter(a => a.length > 0).forEach(code => {
      let hypercode = code.substring(code.indexOf('.') + 1, code.length);
      let level = this.Evaluate_Expression(hypercode, loan, entity);
      if(level) {
        if(operator == 'OR') {
          result = result || level;
        } else if (operator == 'AND') {
          result = result && level;
        } else {
          result = level;
        }
      }
    });

    return result;
  }
}
