import { Injectable } from '@angular/core';
import { loan_model, Loan_Collateral, CollateralCategoryMapping, Loan_Other_Income, Collateral_Category_Code } from '../../models/loanmodel';
import { LoggingService } from '../../services/Logs/logging.service';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../environments/environment.prod';
import { Loan_Crop_Unit } from '../../models/cropmodel';
import * as _ from "lodash";
import { Loan_Farm } from '../../models/farmmodel.';
import { precise_round } from '@lenda/services/common-utils';
import {
  LoanMaster,
  RefDataModel,
  RefMeasurementType,
  RefCollateralMktDisc,
  RefCollateralInsDisc,
  RefCollateralCategory,
  RefContractType,
  MigratedLoanAdjustmentKeys
} from '@lenda/models/ref-data-model';
import { getRandomInt } from '../utility/randomgenerator';
import { MigratedLoanAdjustmentHelper } from '../utility/migrated-loan-adjustments-helper';


@Injectable()
export class Collateralcalculationworker {

    private refdata: RefDataModel = <RefDataModel>{};
    private components;

    private measureItems: Array<RefMeasurementType>;
    private collateralMktDisc: Array<RefCollateralMktDisc>;
    private collateralCategory: Array<RefCollateralCategory>;
    private contractType: Array<RefContractType>;
    private collateralInsDisc: Array<RefCollateralInsDisc>;

    constructor(public logging: LoggingService, public localStorageService : LocalStorageService) {
        this.getValuesFromDomain();
    }

    preparemktvalue(params) {
        params.Market_Value = Number(params.Qty) * Number(params.Price);
    }

    preparecollateralmodel(input: loan_model): loan_model {

        this.getValuesFromDomain();

        try {
          let LoanMaster: LoanMaster = input.LoanMaster;
          this.Set_Initial_Values(LoanMaster);

          input = this.populateOtherIncomeFromCollateral(input);
          input = this.populateCollateralFromOtherIncome(input);

          input.LoanCollateral.forEach(lc => {
              //Don't recalculate deleted Item
              if(lc.ActionStatus == 3) return;

              this.setCropName(lc);

              this.prepareNetMarketValue(lc);
              this.prepareMarketDiscValue(lc);
              this.prepareInsuranceValues(lc, input);

              if(lc.Collateral_Category_Code == Collateral_Category_Code.FSA) {
                this.computeTotalFSA(lc, input);
              }

              if(lc.Collateral_Category_Code === Collateral_Category_Code.Equipemt) {
                this.computeTotalEquip(lc, input);
              }

              if(lc.Collateral_Category_Code === Collateral_Category_Code.Livestock) {
                this.computeTotallivestock(lc, input);
              }

              if(lc.Collateral_Category_Code === Collateral_Category_Code.RealEState) {
                this.computerealstateTotal(lc, input);
              }

              if(lc.Collateral_Category_Code === Collateral_Category_Code.StoredCrop) {
                this.computestoredcropTotal(lc, input);
              }

              if(lc.Collateral_Category_Code === Collateral_Category_Code.Other) {
                this.computeotherTotal(lc, input);
              }

              if(lc.ActionStatus != 1) {
                lc.ActionStatus = 2;
              }
          });

          input.LoanMaster.FC_Total_Disc_Collateral_Value = input.LoanMaster.Disc_value_FSA +
            input.LoanMaster.Disc_value_Equipment + input.LoanMaster.Disc_value_Livestock +
            input.LoanMaster.Disc_value_Other + input.LoanMaster.Disc_value_Real_Estate +
            input.LoanMaster.Disc_value_Stored_Crops;


          // Total Disc Market Value
          input.LoanMaster.Disc_value_Total = input.LoanMaster.FC_Total_Disc_Collateral_Value;

          // Total Market Value
          input.LoanMaster.Net_Market_Value_Total = input.LoanMaster.Net_Market_Value_FSA +
            input.LoanMaster.Net_Market_Value_Equipment + input.LoanMaster.Net_Market_Value_Livestock +
            input.LoanMaster.Net_Market_Value_Other  + input.LoanMaster.Net_Market_Value_Real_Estate + input.LoanMaster.Net_Market_Value_Stored_Crops;

          // Total Ins Value
          input.LoanMaster.Ins_Value_Total = input.LoanMaster.Ins_Value_FSA +
            input.LoanMaster.Ins_Value_Livestock + input.LoanMaster.Ins_Value_Stored_Crops +
            input.LoanMaster.Ins_Value_Equipment  + input.LoanMaster.Ins_Value_Real_Estate + input.LoanMaster.Ins_Value_Other;

          // Total Disc Ins Value
          input.LoanMaster.Disc_Ins_Value_Total = input.LoanMaster.Disc_Ins_Value_FSA +
            input.LoanMaster.Disc_Ins_Value_Livestock + input.LoanMaster.Disc_Ins_Value_Stored_Crops +
            input.LoanMaster.Disc_Ins_Value_Equipment  + input.LoanMaster.Disc_Ins_Value_Real_Estate + input.LoanMaster.Disc_Ins_Value_Other;


          let starttime = new Date().getTime();
          let endtime = new Date().getTime();

          //level 2 log
          this.logging.checkandcreatelog(2, 'Calc_Collateral', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
          return input;
        } catch(e){
          //level 1 log as its error
          this.logging.checkandcreatelog(1, 'Calc_Collateral', e);
          return input;
        }
    }

  private Set_Initial_Values(LoanMaster: LoanMaster) {
    LoanMaster.Net_Market_Value_FSA = 0;
    LoanMaster.Net_Market_Value_Livestock = 0;
    LoanMaster.Net_Market_Value_Stored_Crops = 0;
    LoanMaster.Net_Market_Value_Equipment = 0;
    LoanMaster.Net_Market_Value_Real_Estate = 0;
    LoanMaster.Net_Market_Value_Other = 0;

    LoanMaster.Disc_value_FSA = 0;
    LoanMaster.Disc_value_Livestock = 0;
    LoanMaster.Disc_value_Stored_Crops = 0;
    LoanMaster.Disc_value_Equipment = 0;
    LoanMaster.Disc_value_Real_Estate = 0;
    LoanMaster.Disc_value_Other = 0;

    LoanMaster.Ins_Value_FSA = 0;
    LoanMaster.Ins_Value_Livestock = 0;
    LoanMaster.Ins_Value_Stored_Crops = 0;
    LoanMaster.Ins_Value_Equipment = 0;
    LoanMaster.Ins_Value_Real_Estate = 0;
    LoanMaster.Ins_Value_Other = 0;

    LoanMaster.Disc_Ins_Value_FSA = 0;
    LoanMaster.Disc_Ins_Value_Livestock = 0;
    LoanMaster.Disc_Ins_Value_Stored_Crops = 0;
    LoanMaster.Disc_Ins_Value_Equipment = 0;
    LoanMaster.Disc_Ins_Value_Real_Estate = 0;
    LoanMaster.Disc_Ins_Value_Other = 0;

    LoanMaster.FC_FSA_Prior_Lien_Amount = 0;
    LoanMaster.FC_Market_Value_FSA = 0;
    LoanMaster.FC_Equip_Prior_Lien_Amount = 0;
    LoanMaster.FC_Market_Value_Equip = 0;
    LoanMaster.FC_Market_Value_lst = 0;
    LoanMaster.FC_Lst_Prior_Lien_Amount = 0;
    LoanMaster.FC_total_Qty_lst = 0;
    LoanMaster.FC_total_Price_lst = 0;
    LoanMaster.FC_Market_Value_other = 0;
    LoanMaster.FC_other_Prior_Lien_Amount = 0;
    LoanMaster.FC_Market_Value_realstate = 0;
    LoanMaster.FC_realstate_Prior_Lien_Amount = 0;
    LoanMaster.FC_total_Qty_Real_Estate = 0;
    LoanMaster.FC_Market_Value_storedcrop = 0;
    LoanMaster.FC_storedcrop_Prior_Lien_Amount = 0;
    LoanMaster.FC_total_Qty_storedcrop = 0;
    LoanMaster.FC_total_Price_storedcrop = 0;
  }

    private prepareNetMarketValue(params: Loan_Collateral) {

        //Other Market Values is user Input
        if (params.Collateral_Category_Code == Collateral_Category_Code.StoredCrop) {
            params.Market_Value = params.Price * params.Qty;
        }

        //Prior Lien is Deducted from Net Market Value Based on Indicator
        params.Net_Market_Value = parseFloat(Math.max((Number(params.Market_Value) - Number(params.Prior_Lien_Amount)), 0).toFixed(2));
        //Prior_Lien_Max_Disc_Ind ? Net_Market_Value == Market_Value : Market_Value - Prior_Lien_Amount;
        // params.Net_Market_Value = (Number(params.Market_Value) - Number(params.Prior_Lien_Amount)).toFixed(2);
    }

    private prepareMarketDiscValue(params: Loan_Collateral) {
      let dict = params.Disc_Pct || 0;
      params.Disc_Pct = dict;
      params.Disc_Value = Math.max(Math.max(Number(params.Market_Value)*(1-(Number(dict)/100)), 0) - Number(params.Prior_Lien_Amount), 0);
      params.Disc_Value = parseFloat(params.Disc_Value.toFixed(2));
      params.Disc_Mkt_Value = params.Disc_Value;
    }

    private prepareInsuranceValues(params: Loan_Collateral, input?: loan_model) {
        //Get Insurance Discount
        let insureDiscItem = this.collateralInsDisc.find(item => item.Collateral_Category_Code == params.Collateral_Category_Code);
        let ins_disc_percent = insureDiscItem && insureDiscItem.Disc_Percent ? insureDiscItem.Disc_Percent : 100;

        //TODO:
        //Collateral_Action_Disc_Adj :: How to apply discount? Which identifier
        params.Net_Market_Value = params.Net_Market_Value || 0;
        params.Insurance_Value = params.Insurance_Value || 0;

        if (params.Insured_Flag == 1) {
          params.Ins_Value = parseFloat(params.Net_Market_Value.toFixed(2));
          params.Insurance_Value = parseFloat(params.Net_Market_Value.toFixed(2));

          if(params.Measure_Code) {
            params.Disc_Ins_Value = precise_round(params.Disc_Mkt_Value, 2);
            params.Insurance_Disc_Value = precise_round(params.Disc_Mkt_Value, 2);
          } else {
            params.Disc_Ins_Value = precise_round(Math.max(params.Insurance_Value * ( 1 - (ins_disc_percent / 100)), 0), 2);
            params.Insurance_Disc_Value = precise_round(Math.max(params.Insurance_Value * ( 1 - (ins_disc_percent / 100)), 0), 2);
          }

          input.LoanMaster.FC_Ins_Disc_Collateral_Value += params.Disc_Ins_Value;
          input.LoanMaster.FC_Total_Addt_Collateral_Value += params.Ins_Value;
        } else {
          params.Ins_Value = 0;
          params.Insurance_Value = 0;
          params.Disc_Ins_Value = 0;
          params.Insurance_Disc_Value = 0;
        }
    }

    // this is for footer row of FSA
    private computeTotalFSA(params, input: loan_model) {
      input.LoanMaster.Net_Market_Value_FSA += precise_round(parseFloat(params.Net_Market_Value) || 0,0);
      input.LoanMaster.FC_FSA_Prior_Lien_Amount += precise_round(parseFloat(params.Prior_Lien_Amount) || 0, 0) ;
      input.LoanMaster.FC_Market_Value_FSA += precise_round(params.Market_Value || 0,0);
      input.LoanMaster.Disc_value_FSA += parseFloat(params.Disc_Value) || 0;

      input.LoanMaster.Ins_Value_FSA += precise_round(parseFloat(params.Insurance_Value) || 0,0);
      input.LoanMaster.Disc_Ins_Value_FSA += parseFloat(params.Insurance_Disc_Value) || 0;
    }

    // this is for footer row of Equip
    private computeTotalEquip(params, input:loan_model) {
      input.LoanMaster.Net_Market_Value_Equipment += precise_round(parseFloat(params.Net_Market_Value) || 0,0);
      input.LoanMaster.FC_Equip_Prior_Lien_Amount += precise_round(parseFloat(params.Prior_Lien_Amount) || 0,0);
      input.LoanMaster.FC_Market_Value_Equip += precise_round(parseFloat(params.Market_Value) || 0,0);
      input.LoanMaster.Disc_value_Equipment += parseFloat(params.Disc_Value) || 0;

      input.LoanMaster.Ins_Value_Equipment += precise_round(parseFloat(params.Insurance_Value) || 0,0);
      input.LoanMaster.Disc_Ins_Value_Equipment += parseFloat(params.Insurance_Disc_Value) || 0;
    }

       // this is for footer row of livestock
    private computeTotallivestock(params, input: loan_model) {
      input.LoanMaster.FC_Market_Value_lst +=  precise_round(Number(params.Market_Value) || 0 ,0);
      input.LoanMaster.FC_Lst_Prior_Lien_Amount += precise_round(Number(params.Prior_Lien_Amount) || 0 ,0);
      input.LoanMaster.Net_Market_Value_Livestock += precise_round(Number(params.Net_Market_Value) || 0 ,0);
      input.LoanMaster.Disc_value_Livestock += Number(params.Disc_Value) || 0;
      input.LoanMaster.FC_total_Qty_lst = precise_round(Number(params.Qty) || 0 ,0);
      input.LoanMaster.FC_total_Price_lst = precise_round(Number(params.Price) || 0 ,0);

      input.LoanMaster.Ins_Value_Livestock += precise_round(parseFloat(params.Insurance_Value) || 0,0);
      input.LoanMaster.Disc_Ins_Value_Livestock += parseFloat(params.Insurance_Disc_Value) || 0;
    }

      // this is for footer row of Other
      private computeotherTotal(params, input: loan_model) {
        //
        input.LoanMaster.FC_Market_Value_other += precise_round(Number(params.Market_Value) || 0 ,0);
        input.LoanMaster.FC_other_Prior_Lien_Amount += precise_round(Number(params.Prior_Lien_Amount) || 0 ,0);
        input.LoanMaster.Net_Market_Value_Other += Number(params.Net_Market_Value) || 0;
        input.LoanMaster.Disc_value_Other += Number(params.Disc_Value) || 0;

        input.LoanMaster.Ins_Value_Other += precise_round(parseFloat(params.Insurance_Value) || 0,0);
        input.LoanMaster.Disc_Ins_Value_Other += parseFloat(params.Insurance_Disc_Value) || 0;
      }

      // this is for footer row of realstate
      private computerealstateTotal(params, input: loan_model) {

        input.LoanMaster.FC_Market_Value_realstate += precise_round(Number(params.Market_Value) || 0,0);
        input.LoanMaster.FC_realstate_Prior_Lien_Amount += precise_round(Number(params.Prior_Lien_Amount) || 0,0);
        input.LoanMaster.Net_Market_Value_Real_Estate += precise_round(Number(params.Net_Market_Value) || 0,0);
        input.LoanMaster.Disc_value_Real_Estate += Number(params.Disc_Value) || 0;
        input.LoanMaster.FC_total_Qty_Real_Estate += precise_round(Number(params.Qty) || 0,0);

        input.LoanMaster.Ins_Value_Real_Estate += precise_round(parseFloat(params.Insurance_Value) || 0,0);
        input.LoanMaster.Disc_Ins_Value_Real_Estate += parseFloat(params.Insurance_Disc_Value) || 0;
      }

      // this is for footer row of stored crop
      private computestoredcropTotal(params, input: loan_model) {

        input.LoanMaster.FC_Market_Value_storedcrop += precise_round(Number(params.Market_Value) || 0 ,0);
        input.LoanMaster.FC_storedcrop_Prior_Lien_Amount += precise_round(Number(params.Prior_Lien_Amount) || 0 ,0);
        input.LoanMaster.Net_Market_Value_Stored_Crops += precise_round(Number(params.Net_Market_Value) || 0 ,0);
        input.LoanMaster.Disc_value_Stored_Crops += Number(params.Disc_Value) ||0;
        input.LoanMaster.FC_total_Qty_storedcrop += precise_round(Number(params.Qty) || 0 ,0);
        input.LoanMaster.FC_total_Price_storedcrop += precise_round( Number(params.Price) || 0 ,0);

        input.LoanMaster.Ins_Value_Stored_Crops += precise_round(parseFloat(params.Insurance_Value) || 0,0);
        input.LoanMaster.Disc_Ins_Value_Stored_Crops += parseFloat(params.Insurance_Disc_Value) || 0;
    }

    populateOtherIncomeFromCollateral(input: loan_model) {
      try {
        if(input.LoanCollateral) {
          const collaterals = input.LoanCollateral.filter(a => a.ActionStatus != 3);

          collaterals.forEach(collateral => {
            let Other_Income_Name = CollateralCategoryMapping[collateral.Collateral_Category_Code];
            let existingIncome = input.LoanOtherIncomes.find(a => a.ActionStatus != 3 && a.Loan_Collateral_ID == collateral.Collateral_ID);
            if(collateral.Income_Ind == 1 && collateral.Loan_Other_Income_ID == 0) {
              if(!existingIncome) {
                let newItem = new Loan_Other_Income();
                newItem.Loan_Full_ID = input.Loan_Full_ID;
                newItem.Other_Income_ID = 0;
                newItem.Loan_Collateral_ID = collateral.Collateral_ID;
                newItem.Loan_Other_Income_ID = getRandomInt(
                  Math.pow(10, 5),
                  Math.pow(10, 8)
                );
                newItem.Amount = parseFloat(parseFloat(collateral.Net_Market_Value as any).toFixed(0));
                newItem.Other_Description_Text = collateral.Collateral_Description;
                newItem.Other_Income_Name = Other_Income_Name;
                let ref_other_income = this.refdata.Ref_Other_Incomes.find(a => a.Other_Income_Name == Other_Income_Name);
                if(ref_other_income) {
                  newItem.Other_Income_ID = ref_other_income.Other_Income_ID;
                }
                newItem.ActionStatus = 1;
                newItem.Collateral_Ind = 1;
                if(collateral.Collateral_Category_Code == Collateral_Category_Code.StoredCrop) {
                  newItem.Crop_Code = collateral.Crop_Detail;
                }
                input.LoanOtherIncomes.push(newItem);
              }
            } else {
              if(existingIncome && existingIncome.Loan_Collateral_ID > 0) {
                if(existingIncome.ActionStatus != 1) {
                  existingIncome.ActionStatus = 3;
                } else {
                  let index = input.LoanOtherIncomes.indexOf(existingIncome);
                  input.LoanOtherIncomes.splice(index, 1);
                }
              }
            }

            if(collateral.Income_Ind == 0 && collateral.Loan_Other_Income_ID > 0) {
              let income = input.LoanOtherIncomes.find(a => a.ActionStatus != 3 && a.Loan_Other_Income_ID == collateral.Loan_Other_Income_ID);
              if(income) {
                if(income.ActionStatus != 1) {
                  income.ActionStatus = 3;
                } else {
                  input.LoanOtherIncomes.splice(input.LoanOtherIncomes.indexOf(income), 1);
                }
              }
              collateral.Loan_Other_Income_ID = 0;
            }
          });
        }
        return input;
      } catch {
        return input;
      }
    }

    populateCollateralFromOtherIncome(input: loan_model) {
      try {
        if(input.LoanOtherIncomes) {
          const other_incomes = input.LoanOtherIncomes.filter(a => a.ActionStatus != 3);

          other_incomes.forEach(income => {
            let existingCollateral = input.LoanCollateral.find(a => a.Loan_Other_Income_ID == income.Loan_Other_Income_ID && a.ActionStatus != 3);
            if(income.Collateral_Ind == 1 && income.Loan_Collateral_ID == 0) {
              if(!existingCollateral) {
                let newCollateralItem = new Loan_Collateral();
                newCollateralItem.ActionStatus = 1;
                newCollateralItem.Collateral_ID = getRandomInt(Math.pow(10, 4), Math.pow(10, 6));
                newCollateralItem.Collateral_Category_Code = this.OtherIncomeNameToCollateralCategory(income.Other_Income_Name);
                newCollateralItem.Collateral_Description = income.Other_Description_Text;
                newCollateralItem.Loan_Full_ID = input.Loan_Full_ID;
                newCollateralItem.Loan_ID = input.LoanMaster.Loan_ID;
                newCollateralItem.Loan_Seq_Num = input.LoanMaster.Loan_Seq_num;
                newCollateralItem.Market_Value = income.Amount;
                newCollateralItem.Prior_Lien_Amount = 0;
                newCollateralItem.Net_Market_Value = income.Amount;
                newCollateralItem.Income_Ind = 1;
                newCollateralItem.Qty = 0;
                newCollateralItem.Price = 0;
                newCollateralItem.Disc_Pct = 100;

                if(newCollateralItem.Collateral_Category_Code == Collateral_Category_Code.StoredCrop) {
                  newCollateralItem.Price = income.Amount;
                  newCollateralItem.Qty = 1;
                }

                newCollateralItem.Loan_Other_Income_ID = income.Loan_Other_Income_ID;
                newCollateralItem.Crop_Detail = income.Crop_Code;
                input.LoanCollateral.push(newCollateralItem);
              }
            } else {
              if(existingCollateral && existingCollateral.Loan_Other_Income_ID > 0) {
                if(existingCollateral.ActionStatus == 1) {
                  let index = input.LoanCollateral.indexOf(existingCollateral);
                  input.LoanCollateral.splice(index, 1);
                } else {
                  existingCollateral.ActionStatus = 3;
                }
              }
            }

            if(income.Collateral_Ind == 0 && income.Loan_Collateral_ID > 0) {
              let collateral = input.LoanCollateral.find(a => a.ActionStatus != 3 && a.Collateral_ID == income.Loan_Collateral_ID);
              if(collateral) {
                if(collateral.ActionStatus != 1) {
                  collateral.ActionStatus = 3;
                } else {
                  input.LoanCollateral.splice(input.LoanCollateral.indexOf(collateral), 1);
                }
              }
              income.Loan_Collateral_ID = 0;
            }
          });
        }
        return input;
      } catch {
        return input;
      }
    }

    private OtherIncomeNameToCollateralCategory(income_name) {
      if(income_name == 'FSA') {
        return Collateral_Category_Code.FSA;
      }

      if(income_name == 'Livestock') {
        return Collateral_Category_Code.Livestock;
      }

      if(income_name == 'Equipment') {
        return Collateral_Category_Code.Equipemt;
      }

      if(income_name == 'Stored Crop') {
        return Collateral_Category_Code.StoredCrop;
      }

      if(income_name == 'Real Estate') {
        return Collateral_Category_Code.RealEState;
      }

      return Collateral_Category_Code.Other;
    }

    performMarketValueCalculations(loanObject : loan_model){

        try{
            let starttime = new Date().getTime();
            let cropPractices = loanObject.LoanCropPractices;
            cropPractices.filter(a => a.ActionStatus != 3).forEach(cp => {
                cp.FC_CropYield = this.getCropYieldByPracticeId(loanObject, cp.Crop_Practice_ID);
                cp.FC_Share = this.getShareByPracticeId(loanObject, cp.Crop_Practice_ID);
                //cp.Market_Value = (acres * cp.FC_CropYield * cp.FC_Share/100)*(crop.Crop_Price+crop.Basic_Adj+crop.Marketing_Adj+crop.Rebate_Adj);
                cp.Market_Value = _.sumBy(loanObject.LoanCropUnits.filter(p=>p.Crop_Practice_ID==cp.Crop_Practice_ID),p=>p.Mkt_Value||0);
                cp.Disc_Market_Value = _.sumBy(loanObject.LoanCropUnits.filter(p=>p.Crop_Practice_ID==cp.Crop_Practice_ID),p=>p.Disc_Mkt_Value||0);;
                //cp.FC_LCP_Cei_Value = cp.Market_Value - cp.FC_LCP_Ins_Value; //FC_Agg_Ins_Value calculated in cropunit calculations
                cp.FC_LCP_Cei_Value_Acre = _.sumBy(loanObject.LoanCropUnits.filter(p=>p.Crop_Practice_ID==cp.Crop_Practice_ID), p=>p.CEI_Value_Acre || 0 );
                cp.FC_LCP_Cei_Value_Acre = Math.max(cp.FC_LCP_Cei_Value_Acre, 0);

                cp.FC_LCP_Cei_Value=_.sumBy(loanObject.LoanCropUnits.filter(p=>p.Crop_Practice_ID==cp.Crop_Practice_ID),p=>p.CEI_Value||0);
                // set to zero if negative
                if(cp.FC_LCP_Cei_Value<0)
                cp.FC_LCP_Cei_Value =0;

                cp.FC_LCP_Disc_Cei_Value_Acre = _.sumBy(loanObject.LoanCropUnits.filter(p=>p.Crop_Practice_ID == cp.Crop_Practice_ID), p => p.Disc_CEI_Value_Acre || 0 );
                cp.FC_LCP_Disc_Cei_Value_Acre = Math.max( cp.FC_LCP_Disc_Cei_Value_Acre , 0);

                cp.FC_LCP_Disc_Cei_Value = _.sumBy(loanObject.LoanCropUnits.filter(p=>p.Crop_Practice_ID==cp.Crop_Practice_ID),p=>p.Disc_CEI_Value||0);
                // set to zero if negative
                if(cp.FC_LCP_Disc_Cei_Value<0)
                cp.FC_LCP_Disc_Cei_Value =0; // FC_Agg_Disc_Ins_Value is calculated in cropunit calculation
                //tell Db he needs to update
                cp.ActionStatus =2;
            });

            if(loanObject.LoanMaster && loanObject.LoanMaster){
                loanObject.LoanMaster.Net_Market_Value_Crops = precise_round(_.sumBy(cropPractices,(cp)=> cp.Market_Value),0);
                //MigratedLoanAdjustmentHelper.Adjust_Value(loanObject, MigratedLoanAdjustmentKeys.Net_Market_Value_Crops);

                loanObject.LoanMaster.Disc_value_Crops = _.sumBy(cropPractices,(cp)=> cp.Disc_Market_Value);
                //MigratedLoanAdjustmentHelper.Adjust_Value(loanObject, MigratedLoanAdjustmentKeys.Disc_value_Crops);
            }

            let crops = loanObject.LoanCrops;
            crops.filter(a => a.ActionStatus != 3).forEach(crop => {
                crop.Acres = this.getAcresForCropByCropType(loanObject, crop.Crop_Code, crop.Crop_Type_Code);

                crop.Irr_Acres = this.getAcresForCropByCropType(loanObject, crop.Crop_Code, crop.Crop_Type_Code, 'Irr');
                crop.NI_Acres = this.getAcresForCropByCropType(loanObject, crop.Crop_Code, crop.Crop_Type_Code, 'NI');

                crop.W_Crop_Yield = this.getCropYieldForCropByCropType(loanObject,crop.Crop_Code, crop.Crop_Type_Code);
                crop.LC_Share = this.getShareByCropType(loanObject,crop.Crop_Code, crop.Crop_Type_Code);

                let practices = cropPractices.filter(a => a.Fc_Crop_Code == crop.Crop_Code && a.FC_Crop_Type == crop.Crop_Type_Code);
                crop.Revenue = _.sumBy(practices, p => p.Market_Value || 0);

                if(crop.ActionStatus != 1) {
                  crop.ActionStatus =2;
                }
            });

            let toVerify = _.sumBy(crops,(c)=> c.Revenue);
            if(loanObject.LoanMaster && loanObject.LoanMaster){
                loanObject.LoanMaster.Total_Crop_Acres = precise_round(_.sumBy(crops,(cp)=> cp.Acres),1);

            }
            let endtime = new Date().getTime();
              //level 2 log
            this.logging.checkandcreatelog(2, 'Calc_MarketValue', "LoanCalculation timetaken :" + (endtime - starttime).toString() + " ms");
            return loanObject;
        }catch(e){
              //level 1 log as itts error
            this.logging.checkandcreatelog(1, 'Calc_MarketValue', e);
            return loanObject;
        }

    }

    getCrop(localObject : loan_model,cropCode){
        if(localObject.LoanCrops && localObject.LoanCrops.length > 0)
        {
            return localObject.LoanCrops.find(c=>c.Crop_Code == cropCode);
        }
        return undefined;
    }

    private setCropName(lc: Loan_Collateral) {
      let crop = this.refdata.CropList.find(a => a.Crop_Code == lc.Crop_Detail);
      if(crop) {
        lc.FC_Crop_Name = crop.Crop_Name;
      } else {
        lc.FC_Crop_Name = '-';
      }
    }

    getCropAndPracticeType(cropPracticeID){
        let refdata = this.localStorageService.retrieve(environment.referencedatakey);
        if(refdata.CropList){
            let cropPractice = refdata.CropList.find(cl=>cl.Crop_And_Practice_ID == cropPracticeID);
            if(cropPractice){
                return {
                    cropCode : cropPractice.Crop_Code,
                    practiceTypeCode : cropPractice.Irr_Prac_Code
                }
            }else{
                return undefined;
            }
        }
    }
    getCropPracticeID(cropCode,practiceType){
        let refdata = this.localStorageService.retrieve(environment.referencedatakey);
        if(refdata.CropList){
            let cropPractice = refdata.CropList.find(cl=>cl.Crop_Code == cropCode && cl.Irr_Prac_Code == practiceType);
            if(cropPractice){
                return cropPractice.Crop_And_Practice_ID;

            }else{
                return undefined;
            }
        }
    }

    performMarketValueCalculationsAtCropLevel(loanObject : loan_model){

    }


  getAcresForCrop(loanObject : loan_model,cropCode, practiceType = undefined){
    let totalAcres :number= 0;
    if(loanObject.LoanCropUnits && loanObject.LoanCropUnits.length > 0 ){
      let unitsForCrop : Array<Loan_Crop_Unit>;
      if(practiceType){
        unitsForCrop = loanObject.LoanCropUnits.filter(cu=>cu.Crop_Code == cropCode &&  cu.Crop_Practice_Type_Code == practiceType);
      }else{
        unitsForCrop = loanObject.LoanCropUnits.filter(cu=>cu.Crop_Code == cropCode);
      }

      if(unitsForCrop && unitsForCrop.length > 0){
       totalAcres  = _.sumBy(unitsForCrop, (cu)=> cu.CU_Acres);
      }
    }
    return totalAcres;

  }

  getAcresForCropByCropType(loanObject : loan_model,cropCode, CropType: string, practiceType?: string){
    let totalAcres :number= 0;
    if(loanObject.LoanCropUnits && loanObject.LoanCropUnits.length > 0 ){
      let unitsForCrop = loanObject.LoanCropUnits.filter(cu=> {
        if(CropType == '-') {
          return cu.Crop_Code == cropCode && ( cu.Crop_Type_Code == 'NA' || cu.Crop_Type_Code == CropType);
        }
        return cu.Crop_Code == cropCode &&  cu.Crop_Type_Code == CropType;
      });

      if(practiceType){
        unitsForCrop = unitsForCrop.filter(cu => cu.Crop_Practice_Type_Code == practiceType);
      }

      if(unitsForCrop && unitsForCrop.length > 0){
       totalAcres  = _.sumBy(unitsForCrop, (cu)=> cu.CU_Acres);
      }
    }
    return totalAcres;

  }

  getCropYieldForCropPractice(loanObject : loan_model,cropCode, practice){

    if(cropCode && practice){
      if(loanObject.CropYield && loanObject.CropYield.length > 0){
        let cropPracticeYield = loanObject.CropYield.find(cy=>cy.Crop_Code == cropCode && cy.Practice == practice);
        return cropPracticeYield ? cropPracticeYield.CropYield : 0;
      }else{
        return 0;
      }
    }
  }

  getCropYieldByPracticeId(loanObject: loan_model, cropPracticeID: number) {
    if(cropPracticeID){
      if(loanObject.CropYield && loanObject.CropYield.length > 0){
        let cropPracticeYield = loanObject.CropYield.find(cy=>cy.Crop_ID == cropPracticeID);
        return cropPracticeYield ? cropPracticeYield.CropYield : 0;
      }else{
        return 0;
      }
    }
  }

  getCropYieldForCrop(loanObject : loan_model,cropCode,cropPractice=undefined){
    let IRRAcres = this.getAcresForCrop(loanObject,cropCode,'Irr');
    let NIRAcres = this.getAcresForCrop(loanObject,cropCode,'NI');
    let IRRYield = this.getCropYieldForCropPractice(loanObject,cropCode, 'Irr');
    let NIRYield = this.getCropYieldForCropPractice(loanObject,cropCode,'NI');

    if(IRRAcres+NIRAcres >0){
        return ((IRRAcres*IRRYield) + (NIRAcres*NIRYield))/(IRRAcres+NIRAcres);
    } else {
      return 0;
    }
  }

  private getCropYieldForCropByCropType(loanObject : loan_model, cropCode: string, CropType: string){
    let IRRAcres = this.getAcresForCropByCropType(loanObject, cropCode, CropType,'Irr');
    let NIRAcres = this.getAcresForCropByCropType(loanObject,cropCode, CropType, 'NI');
    let IRRYield = this.getCropYieldForCropPracticeByCropType(loanObject,cropCode, CropType, 'Irr');
    let NIRYield = this.getCropYieldForCropPracticeByCropType(loanObject,cropCode, CropType, 'NI');

    if(IRRAcres+NIRAcres >0){
        return ((IRRAcres*IRRYield) + (NIRAcres*NIRYield))/(IRRAcres+NIRAcres);
    } else {
      return 0;
    }
  }

  private getCropYieldForCropPracticeByCropType(loanObject : loan_model, cropCode: string, CropType: string, practice: string){
    if(cropCode && practice && CropType){
      if(loanObject.CropYield && loanObject.CropYield.length > 0){
        let cropPracticeYield = loanObject.CropYield.find(cy=>cy.Crop_Code == cropCode && cy.cropType == CropType && cy.Practice == practice);
        return cropPracticeYield ? cropPracticeYield.CropYield : 0;
      }else{
        return 0;
      }
    }
  }

  getShareByPracticeId(loanObject : loan_model, Crop_And_Practice_ID: number) {
    let totalAcres = 0;
    let shareAcres= 0;
    let cropUnits = [];

    if(loanObject.LoanCropUnits && loanObject.LoanCropUnits.length > 0 ){
      cropUnits = loanObject.LoanCropUnits.filter(cu=>cu.Crop_Practice_ID == Crop_And_Practice_ID);

      cropUnits.forEach(cu => {
        let perProd = this.getPerProdForFarm(loanObject, cu.Farm_ID);
        totalAcres += (cu.CU_Acres || 0);
        shareAcres += (cu.CU_Acres * perProd)/100;
      });
    }

    if(totalAcres > 0){
      return (shareAcres/totalAcres)*100;
    }else{
      return 0;
    }
  }

  getShareByCropType(loanObject : loan_model,cropCode,CropType =undefined) {
    let totalAcres = 0;
    let shareAcres= 0;
    let cropUnits = [];
    if(loanObject.LoanCropUnits && loanObject.LoanCropUnits.length > 0 ){
      if(CropType){
        cropUnits = loanObject.LoanCropUnits.filter(cu => {
          if(CropType == '-') {
            return cu.Crop_Code == cropCode && (cu.Crop_Type_Code == CropType || cu.Crop_Type_Code == 'NA');
          }
          return cu.Crop_Code == cropCode && cu.Crop_Type_Code == CropType;
        });
      }else{
          cropUnits = loanObject.LoanCropUnits.filter(cu=>cu.Crop_Code == cropCode);
      }

      cropUnits.forEach(cu => {
        let perProd = this.getPerProdForFarm(loanObject,cu.Farm_ID);
        totalAcres += cu.CU_Acres;
        shareAcres += (cu.CU_Acres*perProd)/100;

      });
    }
    if(totalAcres > 0){
      return (shareAcres/totalAcres)*100;
    }else{
      return 0;
    }
  }

  getPerProdForFarm(loanObject : loan_model,farmID){
    let farm : Loan_Farm;
    if(loanObject.Farms && loanObject.Farms.length > 0 ){
      farm = loanObject.Farms.find(f=>f.Farm_ID == farmID);
      return farm ? farm.Percent_Prod : 0;
    }else{
      return 0;
    }
  }

  private getValuesFromDomain() {
    this.refdata = this.localStorageService.retrieve(environment.referencedatakey);

    if(this.refdata){
      this.measureItems = this.refdata.MeasurementType || [];
      this.collateralMktDisc = this.refdata.CollateralMktDisc || [];
      this.collateralCategory = this.refdata.CollateralCategory || [];
      this.contractType = this.refdata.ContractType || [];
      this.collateralInsDisc = this.refdata.CollateralInsDisc || [];
    }
  }
}
