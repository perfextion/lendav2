import { AsYouType, formatNumber } from 'libphonenumber-js';
import { IValueGetterParams } from '@lenda/aggriddefinations/aggridoptions';

//Numeric cell editor config
export function getNumericCellEditor(allowNegative?: boolean) {
  function isCharNumeric(charStr) {
    // return !!/\d/.test(charStr);
    //for decimals
    if (allowNegative) {
      return /^-*\d*\.?\d*$/.test(charStr);
    } else {
      return /^\d*\.?\d*$/.test(charStr);
    }
  }

  function isCharDot(charStr) {
    return charStr === '.';
  }

  function isKeyPressedNumeric(event) {
    let charCode = getCharCodeFromEvent(event);
    let charStr = String.fromCharCode(charCode);
    return isCharNumeric(charStr);
  }

  function isKeyPressedDot(event) {
    let charCode = getCharCodeFromEvent(event);
    let charStr = String.fromCharCode(charCode);
    return isCharDot(charStr);
  }

  function getCharCodeFromEvent(event) {
    event = event || window.event;
    return typeof event.which === 'undefined' ? event.keyCode : event.which;
  }

  function NumericCellEditor() {}

  NumericCellEditor.prototype.init = function(params) {
    this.focusAfterAttached = params.cellStartedEdit;
    this.eInput = document.createElement('input');
    (this.eInput as HTMLInputElement).classList.add('expandedcell');

    this.eInput.addEventListener('change', function(event) {});

    this.eInput.value =
      (isCharNumeric(params.charPress) ? params.charPress : params.value) ==
      undefined
        ? 0
        : isCharNumeric(params.charPress)
        ? params.charPress
        : params.value;
    let that = this;
    this.eInput.addEventListener('keypress', function(event) {
      //this.eInput
      //this.eInput.addClass('lenda-cellEdit-color')
      if (!isKeyPressedNumeric(event) && !isKeyPressedDot(event)) {
        that.eInput.focus();
        if (event.preventDefault) event.preventDefault();
      }
    });

    this.eInput.addEventListener('blur', function(event) {
      if (
        params.newValue == undefined ||
        params.newValue == null ||
        params.newValue == ''
      ) {
        // return this.classList.add('error');
      }
      //this.eInput
      //this.eInput.addClass('lenda-cellEdit-color')
    });
  };

  NumericCellEditor.prototype.getGui = function() {
    return this.eInput;
  };

  NumericCellEditor.prototype.afterGuiAttached = function() {
    if (this.focusAfterAttached) {
      this.eInput.focus();
      this.eInput.select();
    }
  };

  NumericCellEditor.prototype.isCancelBeforeStart = function() {
    return this.cancelBeforeStart;
  };

  NumericCellEditor.prototype.isCancelAfterEnd = function() {};

  NumericCellEditor.prototype.getValue = function() {
    return this.eInput.value == '' ? 0 : this.eInput.value;
  };

  NumericCellEditor.prototype.focusIn = function() {
    let eInput = this.getGui();
    eInput.focus();
    eInput.select();
    console.log('NumericCellEditor.focusIn()');
  };

  NumericCellEditor.prototype.focusOut = function() {
    console.log('NumericCellEditor.focusOut()');
  };

  return NumericCellEditor;
}

export function getIntegerCellEditor() {
  function isCharNumeric(charStr) {
    return !!/\d/.test(charStr);
  }

  function isKeyPressedNumeric(event) {
    let charCode = getCharCodeFromEvent(event);
    let charStr = String.fromCharCode(charCode);
    return isCharNumeric(charStr);
  }

  function getCharCodeFromEvent(event) {
    event = event || window.event;
    return typeof event.which === 'undefined' ? event.keyCode : event.which;
  }

  function IntegerCellEditor() {}

  IntegerCellEditor.prototype.init = function(params) {
    this.focusAfterAttached = params.cellStartedEdit;
    this.eInput = document.createElement('input');
    this.eInput.style.width = '100%';
    this.eInput.style.height = '100%';
    this.eInput.addEventListener('change', function(event) {
      //event.srcElement.parentElement.className=event.srcElement.parentElement.className.replace("editable-color","edited-color")
    });

    this.eInput.value =
      (isCharNumeric(params.charPress) ? params.charPress : params.value) ==
      undefined
        ? null
        : isCharNumeric(params.charPress)
        ? params.charPress
        : params.value;
    let that = this;
    this.eInput.addEventListener('keypress', function(event) {
      if (!isKeyPressedNumeric(event)) {
        that.eInput.focus();
        if (event.preventDefault) event.preventDefault();
      }
    });

    this.eInput.addEventListener('blur', function(event) {
      if (
        params.newValue == undefined ||
        params.newValue == null ||
        params.newValue == ''
      ) {
        return this.classList.add('error');
      }
    });
  };

  IntegerCellEditor.prototype.getGui = function() {
    return this.eInput;
  };

  IntegerCellEditor.prototype.afterGuiAttached = function() {
    if (this.focusAfterAttached) {
      this.eInput.focus();
      this.eInput.select();
    }
  };

  IntegerCellEditor.prototype.isCancelBeforeStart = function() {
    return this.cancelBeforeStart;
  };

  IntegerCellEditor.prototype.isCancelAfterEnd = function() {};

  IntegerCellEditor.prototype.getValue = function() {
    return this.eInput.value == '' ? 0 : this.eInput.value;
  };

  IntegerCellEditor.prototype.focusIn = function() {
    let eInput = this.getGui();
    eInput.focus();
    eInput.select();
  };

  IntegerCellEditor.prototype.focusOut = function() {
  };

  return IntegerCellEditor;
}

export function numberValueSetter(params) {
  if (
    params.newValue == undefined ||
    params.newValue == null ||
    params.newValue == '' ||
    isNaN(params.newValue)
  ) {
    params.newValue = 0;
    params.data[params.colDef.field] = 0;
  } else {
    let data = parseFloat(params.newValue);
    params.data[params.colDef.field] = data;
  }

  return true;
}

export function priceValueSetter(params) {
  if (
    params.newValue == undefined ||
    params.newValue == null ||
    params.newValue == '' ||
    isNaN(params.newValue)
  ) {
    params.newValue = 0;
    params.data[params.colDef.field] = 0;
  } else {
    let data = parseFloat(params.newValue);
    data = parseFloat(data.toFixed(4));
    params.data[params.colDef.field] = data;
  }

  return true;
}

export function numberValueSetterwithdash(params) {
  if (
    params.newValue == undefined ||
    params.newValue == null ||
    params.newValue == '' ||
    isNaN(params.newValue)
  ) {
    params.newValue = null;
    params.data[params.colDef.field] = null;
  } else {
    let data = parseFloat(params.newValue);
    params.data[params.colDef.field] = data;
  }

  return true;
}

export function numberWithOneDecPrecValueSetter(params, decimals = 1) {
  if (
    params.newValue == undefined ||
    params.newValue == null ||
    params.newValue == ''
  ) {
    params.newValue = 0;
  }

  let data = parseFloat(params.newValue);
  data = parseFloat(data.toFixed(decimals));
  params.data[params.colDef.field] = data;
  return true;
}

export function yieldValueSetter(params: IValueGetterParams) {
  if (!params.newValue || params.newValue == 0) {
    params.newValue = null;
    params.data[params.colDef.field] = null;
  } else {
    let data = parseFloat(params.newValue);
    params.data[params.colDef.field] = data;
  }

  return true;
}

export function formatPhoneNumber(params) {
  let val = ('' + params.value).replace(/\D/g, '');
  let num = val.match(/^(\d{3})(\d{3})(\d{4})$/);
  return !num ? null : '(' + num[1] + ') ' + num[2] + '-' + num[3];
}

export function formatPhoneNumberAsYouType(params) {
  let val = ('' + params.value).replace(/\D/g, '');
  let num = val.match(/^(\d{3})(\d{3})(\d{4})$/);
  const phoneNumber = formatNumber(val, 'US', 'National');
  return phoneNumber;
}

export function formatPhoneNumberAsYouTypeValue(params) {
  let val = ('' + params).replace(/\D/g, '');
  let num = val.match(/^(\d{3})(\d{3})(\d{4})$/);
  return new AsYouType('US').input(val);
}

export function getPhoneCellEditor() {
  function isCharNumeric(charStr) {
    if (!charStr && charStr >= 0) return false;
    return !!/\d/.test(charStr);
  }

  function isKeyPressedNumeric(event) {
    let charCode = getCharCodeFromEvent(event);
    let charStr = String.fromCharCode(charCode);
    return isCharNumeric(charStr) && event.srcElement.value.length <= 9;
  }

  function getCharCodeFromEvent(event) {
    event = event || window.event;
    return typeof event.which === 'undefined' ? event.keyCode : event.which;
  }

  function NumericCellEditor() {}

  NumericCellEditor.prototype.init = function(params) {
    this.focusAfterAttached = params.cellStartedEdit;
    this.eInput = document.createElement('input');

    this.eInput.classList.add('expandedcell');

    this.eInput.addEventListener('change', function(event) {
      event.srcElement.parentElement.classList.add('edited-color');
    });

    this.eInput.value =
      (isCharNumeric(params.charPress) ? params.charPress : params.value) ==
      undefined
        ? null
        : isCharNumeric(params.charPress)
        ? params.charPress
        : params.value;
    let that = this;

    this.eInput.addEventListener('keypress', function(event) {
      if (!isKeyPressedNumeric(event) && !that.firstTimeInput) {
        that.eInput.focus();
        if (event.preventDefault) event.preventDefault();
      } else {
        that.firstTimeInput = false;
      }
    });

    this.eInput.addEventListener('blur', function(event) {
      if (
        params.newValue == undefined ||
        params.newValue == null ||
        params.newValue == ''
      ) {
        return this.classList.add('error');
      }
    });

    this.eInput.addEventListener('click', function(event) {
      that.firstTimeInput = event.srcElement.value.length != 10;
    });

  };

  NumericCellEditor.prototype.getGui = function() {
    return this.eInput;
  };

  NumericCellEditor.prototype.afterGuiAttached = function() {
    if (this.focusAfterAttached) {
      this.eInput.focus();
      this.eInput.select();
      this.firstTimeInput = true;
    }
  };

  NumericCellEditor.prototype.isCancelBeforeStart = function() {
    return this.cancelBeforeStart;
  };

  NumericCellEditor.prototype.isCancelAfterEnd = function() {};

  NumericCellEditor.prototype.getValue = function() {
    return this.eInput.value == '' ? null : this.eInput.value;
  };

  NumericCellEditor.prototype.focusIn = function() {
    let eInput = this.getGui();
    eInput.focus();
    eInput.select();
    // console.log("NumericCellEditor.focusIn()");
  };

  NumericCellEditor.prototype.focusOut = function() {
    // console.log("NumericCellEditor.focusOut()");
  };
  // console.log('Numeric',NumericCellEditor);
  return NumericCellEditor;
}

export function numberEmptyDefault(params) {
  if (
    params.newValue == undefined ||
    params.newValue == null ||
    params.newValue == '' ||
    isNaN(params.newValue)
  ) {
    params.newValue = null;
    params.data[params.colDef.field] = null;
  } else {
    let data = parseFloat(params.newValue);
    params.data[params.colDef.field] = data;
  }

  return true;
}
//Numeric cell editor config Ends

//Numeric cell editor config
export function getNumberEmptyDefault() {
  function isCharNumeric(charStr) {
    // return !!/\d/.test(charStr);
    //for decimals
    return /^\d*\.?\d*$/.test(charStr);
  }
  function isCharDot(charStr) {
    return charStr === '.';
  }

  function isKeyPressedNumeric(event) {
    let charCode = getCharCodeFromEvent(event);
    let charStr = String.fromCharCode(charCode);
    return isCharNumeric(charStr);
  }

  function isKeyPressedDot(event) {
    let charCode = getCharCodeFromEvent(event);
    let charStr = String.fromCharCode(charCode);
    return isCharDot(charStr);
  }

  function getCharCodeFromEvent(event) {
    event = event || window.event;
    return typeof event.which === 'undefined' ? event.keyCode : event.which;
  }

  function NumberEmptyDefault() {}

  NumberEmptyDefault.prototype.init = function(params) {
    this.focusAfterAttached = params.cellStartedEdit;
    this.eInput = document.createElement('input');

    (this.eInput as HTMLInputElement).classList.add('expandedcell');

    this.eInput.addEventListener('change', function(event) {});

    this.eInput.value =
      (isCharNumeric(params.charPress) ? params.charPress : params.value) ==
      undefined
        ? 0
        : isCharNumeric(params.charPress)
        ? params.charPress
        : params.value;
    let that = this;

    this.eInput.addEventListener('keypress', function(event) {
      //this.eInput
      //this.eInput.addClass('lenda-cellEdit-color')
      if (!isKeyPressedNumeric(event) && !isKeyPressedDot(event)) {
        that.eInput.focus();
        if (event.preventDefault) event.preventDefault();
      }
    });

    this.eInput.addEventListener('blur', function(event) {
      if (
        params.newValue == undefined ||
        params.newValue == null ||
        params.newValue == ''
      ) {
        return this.classList.add('error');
      }
      //this.eInput
      //this.eInput.addClass('lenda-cellEdit-color')
    });
  };

  NumberEmptyDefault.prototype.getGui = function() {
    return this.eInput;
  };

  NumberEmptyDefault.prototype.afterGuiAttached = function() {
    if (this.focusAfterAttached) {
      this.eInput.focus();
      this.eInput.select();
    }
  };
  NumberEmptyDefault.prototype.isCancelBeforeStart = function() {
    return this.cancelBeforeStart;
  };

  NumberEmptyDefault.prototype.isCancelAfterEnd = function() {};

  NumberEmptyDefault.prototype.getValue = function() {
    // return this.eInput.value==""?0:this.eInput.value;
    return this.eInput.value;
  };

  NumberEmptyDefault.prototype.focusIn = function() {
    let eInput = this.getGui();
    eInput.focus();
    eInput.select();
    // console.log("NumberEmptyDefault.focusIn()");
  };

  NumberEmptyDefault.prototype.focusOut = function() {
    // console.log("NumberEmptyDefault.focusOut()");
  };
  return NumberEmptyDefault;
}
