import { RefDataModel } from '@lenda/models/ref-data-model';

// States

export function extractCropValues(mappings) {
  let obj = [];
  mappings.forEach((element, index) => {
    if (obj.findIndex(p => p.key == element.Crop_Code) == -1)
      obj[obj.length] = { key: element.Crop_Code, value: element.Crop_Name };
  });
  return obj;
}

export function lookupCropValue(mappings, key) {
  if (key != null && key != undefined) {
    let crop = mappings.find(p => p.key.toLowerCase() == key.toLowerCase());
    if (crop) {
      return crop.value;
    } else {
      return '';
    }
  } else return '';
}

export function lookupCropValuewithoutmapping(key) {
  try {
    let refdata = JSON.parse(
      '[' + window.localStorage.getItem('ng2-webstorage|refdata') + ']'
    )[0];
    let mappings = extractCropValues(refdata.CropList);
    return mappings.find(p => p.key.toLowerCase() == key.toLowerCase()).value;
  } catch (ex) {
    return '';
  }
}

export function Cropvaluesetter(params) {
  let crop = params.newValue;
  let values = params.colDef.cellEditorParams.values;
  for (let object of values) {
    let value = object.key;
    if (value == params.newValue) {
      params.data[params.colDef.field] = crop;
    }
  }
  return true;
}

export function cropNameValueSetter(params) {
  let crop = params.newValue;
  let values = params.colDef.cellEditorParams.values;
  for (let object of values) {
    let value = object.key;
    if (value == params.newValue) {
      params.data[params.colDef.field] = object.value;
    }
  }
  return true;
}
// Ends Here

// County

export function extractCropTypeValues(mappings) {
  let obj = [];
  mappings.forEach((element, index) => {
    if (obj.findIndex(p => p.key == element.Crop_Type_Code) == -1)
      obj[obj.length] = {
        key: element.Crop_Type_Code,
        value: element.Crop_Type_Name
      };
  });
  return obj;
}

export function lookupCropTypeValue(key) {
  try {
    key = key.trim();
    let refdata = JSON.parse(
      '[' + window.localStorage.getItem('ng2-webstorage|refdata') + ']'
    )[0];
    if (key != undefined && key != '') {
      return refdata.CropList.find(p => p.Crop_Type_Code == key).Crop_Type_Name;
    }
  } catch (ex) {
    return '';
  }
}

export function CropTypevaluesetter(params) {
  let refdata = JSON.parse(
    '[' + window.localStorage.getItem('ng2-webstorage|refdata') + ']'
  )[0];
  let county = params.newValue;
  let values = refdata.CropList;
  for (let object of values) {
    let value = object.Crop_Type_Code;
    if (value == params.newValue) {
      params.data[params.colDef.field] = object.Crop_Type_Code;
    }
  }
  return true;
}

export function getfilteredCropType(params) {
  let refdata = JSON.parse(
    '[' + window.localStorage.getItem('ng2-webstorage|refdata') + ']'
  )[0];
  let selectedcrop = params.data.Crop_Code;
  let allowedcroptypes = refdata.CropList.filter(
    p => p.Crop_Code == selectedcrop
  );
  return { values: extractCropTypeValues(allowedcroptypes) };
}

export function lookupCropType(mappings, key) {
  try {
    let test = mappings.find(p => p.value.toLowerCase() == key.toLowerCase())
      .key;
    return test;
  } catch (ex) {
    return '';
  }
}

export function APHRoundValueSetter(params) {
  return Math.round(params.value);
}

export function CropPracticeValueSetter(params) {
  if (params.value === 'NIR') {
    return 'NI';
  } else {
    return params.value;
  }
}

export function lookupUOM(Crop_Code, refdata) {
  if (refdata) {
    let crop = refdata.CropList.find(c => c.Crop_Code == Crop_Code);
    if (crop) {
      return crop.MPCI_UOM;
    } else {
      return 'Bu';
    }
  } else {
    return 'Bu';
  }
}

export function lookupCropTypeWithRefData(
  crop_id: number,
  refdata: RefDataModel
) {
  try {
    let crop = refdata.CropList.find(a => a.Crop_And_Practice_ID == crop_id);
    if (crop) {
      return crop.Crop_Type_Code;
    } else {
      return '';
    }
  } catch {
    return '';
  }
}

export function lookupCropPracticeTypeWithRefData(
  crop_id: number,
  refdata: RefDataModel
) {
  try {
    let crop = refdata.CropList.find(a => a.Crop_And_Practice_ID == crop_id);
    if (crop) {
      return crop.Crop_Prac_Code;
    } else {
      return '';
    }
  } catch {
    return '';
  }
}

export function lookupIrrPracticeTypeWithRefData(
  crop_id: number,
  refdata: RefDataModel
) {
  try {
    let crop = refdata.CropList.find(a => a.Crop_And_Practice_ID == crop_id);
    if (crop) {
      return crop.Irr_Prac_Code;
    } else {
      return '';
    }
  } catch {
    return '';
  }
}
