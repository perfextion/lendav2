//Alpha Numeric cell editor config
export function getAlphaNumericCellEditor() {
  function isCharAlphaNumeric(charStr) {
    //for alphaNumeric
    return /^[a-z0-9 @.']+$/i.test(charStr);
  }

  function isKeyPressedAlphaNumeric(event) {
    let charCode = getCharCodeFromEvent(event);
    let charStr = String.fromCharCode(charCode);
    return isCharAlphaNumeric(charStr);
  }

  function getCharCodeFromEvent(event) {
    event = event || window.event;
    return typeof event.which === 'undefined' ? event.keyCode : event.which;
  }

  function AlphaNumericCellEditor() {}

  AlphaNumericCellEditor.prototype.init = function(params) {
    this.focusAfterAttached = params.cellStartedEdit;
    this.eInput = document.createElement('input');
    // this.eInput.style.width = "100% !important";
    // this.eInput.style.height = "100%";
    (this.eInput as HTMLInputElement).classList.add('expandedcell');
    // this.eInput.Width=params.eGridCell.clientWidth;
    // this.eInput.Height=params.eGridCell.clientHeight;
    this.eInput.addEventListener('change', function(event) {
      event.srcElement.parentElement.className = event.srcElement.parentElement.className.replace(
        'editable-color',
        'edited-color'
      );
    });

    this.eInput.value =
      params.charPress && isCharAlphaNumeric(params.charPress)
        ? params.charPress
        : params.value == undefined
        ? ''
        : params.value;
    let that = this;

    this.eInput.addEventListener('keypress', function(event) {
      if (!isKeyPressedAlphaNumeric(event)) {
        that.eInput.focus();
        if (event.preventDefault) event.preventDefault();
      }
    });

    this.eInput.addEventListener('blur', function(event) {
      if (
        params.newValue == undefined ||
        params.newValue == null ||
        params.newValue == ''
      ) {
        //  return this.classList.add("error");
      }
    });
  };

  AlphaNumericCellEditor.prototype.getGui = function() {
    return this.eInput;
  };

  AlphaNumericCellEditor.prototype.afterGuiAttached = function() {
    if (this.focusAfterAttached) {
      this.eInput.focus();
      this.eInput.select();
    }
  };

  AlphaNumericCellEditor.prototype.isCancelBeforeStart = function() {
    return this.cancelBeforeStart;
  };

  AlphaNumericCellEditor.prototype.isCancelAfterEnd = function() {};

  AlphaNumericCellEditor.prototype.getValue = function() {
    return this.eInput.value;
  };

  AlphaNumericCellEditor.prototype.focusIn = function() {
    let eInput = this.getGui();
    eInput.focus();
    eInput.select();
    // console.log("AlphaNumericCellEditor.focusIn()");
  };

  AlphaNumericCellEditor.prototype.focusOut = function() {
    // console.log("AlphaNumericCellEditor.focusOut()");
  };
  return AlphaNumericCellEditor;
}
