import { isNumber } from "util";
import { RefDataModel } from "@lenda/models/ref-data-model";
export function lookupCountyValue(key) {
    let refdata = JSON.parse('[' + window.localStorage.getItem("ng2-webstorage|refdata") + ']')[0];
    if (key) {
        return refdata.CountyList.find(p => p.County_ID == _parseIntlocal(key)).County_Name;
    }
}

function _parseIntlocal(key: any) {
  return parseInt(key, 10);
}

export function lookupInsValidationValue(key: string, refdata: RefDataModel) {
    if (key) {
        if (refdata.InsValidation.find(p => p.Ins_V_Key == key) != undefined) {
            if (refdata.InsValidation.find(p => p.Ins_V_Key == key).Ins_V_Value == null) {
                return "";
            }
            return refdata.InsValidation.find(p => p.Ins_V_Key == key).Ins_V_Value;
        }
        return "";
    }
}
export function lookupCountyValueFromPassedRefData(key, refdata: RefDataModel) {
    try {
        if (key) {
            return refdata.CountyList.find(p => p.County_ID == _parseIntlocal(key)).County_Name;
        }
    } catch (ex) {
    }
}
export function lookupCropValueFromPassedRefData(key, refdata: RefDataModel) {
    if (key) {
        return refdata.CropList.find(p => p.Crop_Code == key).Crop_Name;
    }
}

export function lookupStateRefValue(key) {

    let refdata: RefDataModel = JSON.parse('[' + window.localStorage.getItem("ng2-webstorage|refdata") + ']')[0];
    if (key) {
        return refdata.StateList.find(p => p.State_ID == _parseIntlocal(key)).State_Name;
    }
}

export function lookupStateAbvRefValue(key, refData: RefDataModel) {

    if (key) {
        let state = refData.StateList.find(p => p.State_ID == _parseIntlocal(key));
        return state ? state.State_Abbrev : '';
    }
}
// States
//

export function extractStateValues(mappings) {
    let obj = [];
    obj.push({ key: '', value: '' });
    mappings.forEach(element => {
        //obj[element.State_ID] = element.State_Abbrev;
        obj.push({ key: element.State_ID, value: element.State_Abbrev });
    });
    //console.log(obj);
    return obj;
}

export function lookupStateValue(mappings, key) {
    if (key) {
        let test = mappings.find(p => p.key === key).value;
        return test;
    } else {
        return mappings[0].value;
    }

}

export function lookupStateValueinRefobj(key) {
    let refdata: RefDataModel = JSON.parse('[' + window.localStorage.getItem("ng2-webstorage|refdata") + ']')[0];
    if (!isNaN(key) && _parseIntlocal(key)) {
        return refdata.StateList.find(p => p.State_ID == _parseIntlocal(key)).State_Abbrev;
    } else {
        return '';
    }

}

export function lookupStateValueFromPassedRefObject(key, refdata: RefDataModel) {
    if (!isNaN(key) && _parseIntlocal(key)) {
        return refdata.StateList.find(p => p.State_ID == _parseIntlocal(key)).State_Abbrev;
    } else {
        return '';
    }

}

export function lookupStateNameFromPassedRefObject(key, refdata: RefDataModel) {
    if (!isNaN(key) && _parseIntlocal(key) && refdata) {
        return refdata.StateList.find(p => p.State_ID == _parseIntlocal(key)).State_Name;
    } else {
        return '';
    }

}
export function lookupStateCodeFromPassedRefObject(key, refdata: RefDataModel) {
    if (!isNaN(key) && _parseIntlocal(key) && refdata) {
        return refdata.StateList.find(p => p.State_ID == _parseIntlocal(key)).State_Abbrev;
    } else {
        return '';
    }

}
export function Statevaluesetter(params) {
    params.data[params.colDef.field] = _parseIntlocal(params.newValue);
    return true;
}
// Ends Here

// County

export function extractCountyValues(mappings) {
    let obj = [];
    mappings.forEach(element => {
        //obj[element.County_ID] = element.County_Name;
        obj.push({ key: element.County_ID, value: element.County_Name });
    });
    return obj;
}

export function lookupCountyRefValue(key, refData: RefDataModel) {
    if (key) {
        let county = refData.CountyList.find(p => p.County_ID == _parseIntlocal(key));
        return county ? county.County_Name : '';
    }
}




export function Countyvaluesetter(params) {
    params.data[params.colDef.field] = params.newValue;
    return true;
}

export function getfilteredcounties(params) {
    let refdata: RefDataModel = JSON.parse('[' + window.localStorage.getItem("ng2-webstorage|refdata") + ']')[0];
    let selectedstate = params.data.Farm_State_ID === 0 ? null : params.data.Farm_State_ID;
    let allowedCounties = refdata.CountyList.filter(p => p.State_ID == selectedstate);
    return { values: extractCountyValues(allowedCounties) };
}

export function getfilteredcountiesAPH(params) {

    let refdata: RefDataModel = JSON.parse('[' + window.localStorage.getItem("ng2-webstorage|refdata") + ']')[0];
    let selectedstate = params.data.State_ID === 0 ? null : params.data.State_ID;
    let allowedCounties = refdata.CountyList.filter(p => p.State_ID == selectedstate);
    return { values: extractCountyValues(allowedCounties) };
}
 // Ends Here
