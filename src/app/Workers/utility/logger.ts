import { environment } from '@env/environment.prod';

/**
 * Custom Logger
 * ===
 */

export class ILogger {
  /**
   * Equivalent to console.log
   */
  static log(message?: any, ...optionalParams: any[]) {
    if (environment.isDebugModeActive) {
      console.log(message, optionalParams);
    }
  }

  /**
   * Equivalent to console.info
   */
  static info(message?: any, ...optionalParams: any[]) {
    if (environment.isDebugModeActive) {
      console.info(message, optionalParams);
    }
  }

  /**
   * Equivalent to console.error
   */
  static error(message?: any, ...optionalParams: any[]) {
    if (environment.isDebugModeActive) {
      console.error(message, optionalParams);
    }
  }

  /**
   * Equivalent to console.warn
   */
  static warn(message?: any, ...optionalParams: any[]) {
    if (environment.isDebugModeActive) {
      console.warn(message, optionalParams);
    }
  }

  /**
   * Equivalent to console.time
   */
  static time(label: string) {
    if (environment.isDebugModeActive) {
      console.time(label);
    }
  }

  /**
   * Equivalent to console.timeEnd
   */
  static timeEnd(label: string) {
    if (environment.isDebugModeActive) {
      console.timeEnd(label);
    }
  }
}
