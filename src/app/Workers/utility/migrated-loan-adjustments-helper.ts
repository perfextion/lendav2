import { loan_model } from '@lenda/models/loanmodel';
import {
  MigratedLoanAdjustmentKeys,
  Migrated_Loan_Adjustments
} from '@lenda/models/ref-data-model';

export class MigratedLoanAdjustmentHelper {
  public static Adjust_Value(
    loan: loan_model,
    key_to_adjust: MigratedLoanAdjustmentKeys
  ) {
    try {
      let loanMaster = loan.LoanMaster;

      //If Loan is Migrated Loan
      if (loanMaster.Legacy_Loan_ID > 0) {
        let adjustments: Migrated_Loan_Adjustments = JSON.parse(
          loanMaster.Migrated_Loan_Adjustments
        );

        if (key_to_adjust && key_to_adjust in loanMaster) {
          let value_before_adj = loanMaster[key_to_adjust];
          let adj_value = adjustments[key_to_adjust];
          let value_after_adj = (value_before_adj || 0) + (adj_value || 0);
          loanMaster[key_to_adjust] = value_after_adj;
        }
      } else {
        //No Need to Adjust
      }
    } catch {}
  }

  public static Adjust_Value_By_Key(
    loan: loan_model,
    key_to_adjust: string,
    value_key: MigratedLoanAdjustmentKeys
  ) {
    try {
      let loanMaster = loan.LoanMaster;

      //If Loan is Migrated Loan
      if (loanMaster.Legacy_Loan_ID > 0) {
        let adjustments: Migrated_Loan_Adjustments = JSON.parse(
          loanMaster.Migrated_Loan_Adjustments
        );

        if (key_to_adjust && key_to_adjust in loanMaster) {
          let value_before_adj = loanMaster[key_to_adjust];
          let adj_value = adjustments[value_key];
          let value_after_adj = (value_before_adj || 0) + (adj_value || 0);
          loanMaster[key_to_adjust] = value_after_adj;
        }
      } else {
        //No Need to Adjust
      }
    } catch {}
  }

  public static Get_Adjustment_Value(loan: loan_model, key_to_adjust: string) {
    try {
      let loanMaster = loan.LoanMaster;
      //If Loan is Migrated Loan
      if (loanMaster.Legacy_Loan_ID > 0) {
        let adjustments: Migrated_Loan_Adjustments = JSON.parse(
          loanMaster.Migrated_Loan_Adjustments
        );
        if (key_to_adjust && key_to_adjust in adjustments) {
          let adj_value = adjustments[key_to_adjust];
          return adj_value;
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    } catch {
      return 0;
    }
  }
}
