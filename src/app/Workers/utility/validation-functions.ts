import { Loan_Farm } from "@lenda/models/farmmodel.";
import { Loan_Association, Borrower_Income_History, Loan_Collateral, AssociationTypeCode, AssociationCodeNameMapping, Loan_Budget, Loan_Other_Income, CollateralCategoryMapping, Loan_Marketing_Contract } from "@lenda/models/loanmodel";
import { RegEx } from "@lenda/shared/shared.constants";
import { validationlevel } from "@lenda/models/commonmodels";
import { Loan_Crop_Unit } from "@lenda/models/cropmodel";
import { To_Friendly_Name } from "@lenda/services/common-utils";

/**
 * Use this Function to create a Custom Validation for ag-grid cell,
 * @param row T
 * @param key Column Name
 * @return AgGridValidationResult
 */
export interface AgGridValidation<T> {
  (row: T, key?: string) : AgGridValidationResult;
}

/**
 * @property result  boolean
 * @property level  Validation Level
 */
export interface AgGridValidationResult {
  /**
   * boolean result
   */
  result: boolean;

  /**
   * Validation Level
   */
  level: validationlevel;

  /**
   * Validation Text
   */
  Validation_ID_Text?: string;

  /**
   * Hover Text in Schematics
   */
  hoverText?: string;

  Validation_ID: number;

  /**
   * Do not Consider while counting errors if ignore = true
   */
  ignore?: boolean;
}

/**
 * Use this function for Required Validation for ag-grid cell
 * @returns AgGridValidationResult
 */
export const RequiredValidation: AgGridValidation<any> = (row , key) => {
  let res = row[key] == undefined || row[key] == null || row[key].toString() == '';


  let text = `${To_Friendly_Name(key)} required`;

  if(row.Assoc_Type_Code) {
    text += `- ${row.Assoc_Name} | ${ AssociationCodeNameMapping[row.Assoc_Type_Code]}`;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2blank,
    Validation_ID_Text: text,
    hoverText: text,
    Validation_ID: 30
  }
}

/**
 * Duplicate Farm Key Validation `State_County_FSN_Section_Rated`
 */
export const DuplicateFarmKeyValidation: AgGridValidation<Loan_Farm> = (farm, _) => {

  return <AgGridValidationResult>{
    result: farm.Duplicate_Farm_Key,
    level: validationlevel.level2,
    Validation_ID_Text:  `Duplicate Farm Key - ${farm.FC_State_Code || ''} | ${farm.FC_County_Name || ''} | ${farm.FSN || ''} | ${farm.Rated || ''}`,
    hoverText:  `Duplicate Farm Key - ${farm.FC_State_Code || ''} | ${farm.FC_County_Name || ''} | ${farm.FSN || ''} | ${farm.Rated || ''}`,
    Validation_ID: 27
  }
}

/**
 * Farm State Validation
 */
export const StateValidation: AgGridValidation<Loan_Farm> = (farm, _) => {
  let res =  !farm.Farm_State_ID;

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2blank,
    Validation_ID_Text:  `State required`,
    hoverText:  `State required`,
    Validation_ID: 6
  }
}

/**
 * Farm County Validation
 */
export const CountyValidation: AgGridValidation<Loan_Farm> = (farm, _) => {
  let res =  !farm.Farm_County_ID;

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2blank,
    Validation_ID_Text: `County required - ${farm.FC_State_Code || ''}`,
    hoverText: `County required - ${farm.FC_State_Code || ''}`,
    Validation_ID: 7
  }
}

/**
 * Landowner Validation ( Owner = No )
 * @returns AgGridValidationResult
 */
export const LandownerValidation: AgGridValidation<Loan_Farm> = (farm, _ ) => {
  let res =  farm.Owned == 0 && !farm.Landowner ;

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2blank,
    Validation_ID_Text: `Landlord required - ${farm.FC_State_Code || ''} | ${farm.FC_County_Name || ''} | ${farm.FSN || ''}`,
    hoverText: `Landlord required - ${farm.FC_State_Code || ''} | ${farm.FC_County_Name || ''} | ${farm.FSN || ''}`,
    Validation_ID: 8
  }
}

/**
 * Rend Due Date Validation ( Owned = No )
 * @returns AgGridValidationResult
 */
export const RentDueDateValidation: AgGridValidation<Loan_Farm> = (farm , _ ) => {
  let res =  farm.Owned == 0 && (!farm.Cash_Rent_Due_Date ||  new Date(farm.Cash_Rent_Due_Date).toLocaleDateString() == '01/01/1900' || new Date(farm.Cash_Rent_Due_Date).toLocaleDateString() == "1/1/1900");

  return <AgGridValidationResult>{
    result: !!farm.Display_Rent && res,
    level: validationlevel.level1blank,
    Validation_ID_Text: `Rent Due date required.- ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    hoverText: `Rent Due date required.- ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    Validation_ID: 9
  }
}

/**
 * Rend UOM Validation ( Owned = No )
 * @returns AgGridValidationResult
 */
export const RentUOMValidation: AgGridValidation<Loan_Farm> = (farm , _ ) => {
  let res = false;

  if(farm.Owned == 0) {
    if(!farm.Rent_UOM) {
      res = true;
    }
  }

  return <AgGridValidationResult>{
    result: !!farm.Display_Rent && res,
    level: validationlevel.level2blank,
    Validation_ID_Text: `Rent UOM required. - ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    hoverText: `Rent UOM required. - ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    Validation_ID: 10
  }
}

/**
 * Rent Validation ( Owned = No )
 * @returns AgGridValidationResult
 */
export const RentValidation: AgGridValidation<Loan_Farm> = (farm , _ ) => {
  let res = false;

  if(farm.Owned == 0) {
    if(!farm.Display_Rent && !farm.Share_Rent) {
      res = true;
    }
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2blank,
    Validation_ID_Text: `Rent $ required - ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    hoverText: `Rent $ required - ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    Validation_ID: 11
  }
}

/**
 * Rent % Validation ( Owned = No )
 * @returns AgGridValidationResult
 */
export const RentPercentValidation: AgGridValidation<Loan_Farm> = (farm , _ ) => {
  let res = false;

  if(farm.Owned == 0) {
    if(!farm.Display_Rent && !farm.Share_Rent) {
      res = true;
    }
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2blank,
    Validation_ID_Text: `Rent % required - ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    hoverText: `Rent % required - ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    Validation_ID: 50
  }
}

/**
 * Rend $ > 300 $ Validation ( Owned = No )
 * @returns AgGridValidationResult
 */
export const RentExceedsValidation: AgGridValidation<Loan_Farm> = (farm , _ ) => {
  let res = false;

  if(farm.Owned == 0) {
    if(farm.Display_Rent && farm.Cash_Rent_Per_Acre > 300) {
      res = true;
    }
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level1blank,
    Validation_ID_Text: `Rent $ exceeds $300 per ac - ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    hoverText: `Rent $ exceeds $300 per ac - ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    Validation_ID: 35
  }
}


/**
 * Rend Validation ( Owned = No )
 * @returns AgGridValidationResult
 */
export const WaivedRentValidation: AgGridValidation<Loan_Farm> = (farm , _ ) => {
  let res = false;

  if(farm.Owned == 0) {
    if(farm.Display_Rent < farm.Waived_Rent) {
      res = true;
    }
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2,
    Validation_ID_Text: `Waived Rent $ exceeds than Rent $. - ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    hoverText: `Waived Rent $ exceeds than Rent $. - ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    Validation_ID: 13
  }
}


/**
 * Non Approved Risk Other Queue Association Validation
 */
export const ROQ_Association_Validation: AgGridValidation<Loan_Association> = (assoc, _ ) => {
  let res = false;
  let level: validationlevel;

  const ROQ = ['AIP', 'THR', 'DIS'];

  let isROQ = ROQ.includes(assoc.Assoc_Type_Code);

  if(isROQ) {
    if(!assoc.Ref_Assoc_ID) {
      res = true;
      level = validationlevel.level2;
    }
  }

  return <AgGridValidationResult>{
    result: res,
    level: level,
    Validation_ID_Text: `Non Approved Association Other queue item - ${assoc.Assoc_Name} | ${ AssociationCodeNameMapping[assoc.Assoc_Type_Code]}`,
    hoverText: `Non Approved Association Other queue item - ${assoc.Assoc_Name} | ${ AssociationCodeNameMapping[assoc.Assoc_Type_Code]}`,
    Validation_ID: 3
  }
}

/**
 * Mandatory Crop - Other Income
 */
export const CropValidation: AgGridValidation<Loan_Other_Income> = (income, _ ) => {
  let res = false;

  if(income.Other_Income_Name == 'Stored Crop' && !income.Crop_Code) {
    res = true;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2,
    Validation_ID_Text: `Crop Name required - Stored Crop - Other Income`,
    hoverText: `Crop Name required - Stored Crop - Other Income`,
    Validation_ID: 30
  }
}

/**
 * Mandatory Association Name
 */
export const AssoicationNameValidation: AgGridValidation<Loan_Association> = (assoc, _ ) => {
  let res = false;

  if(!assoc.Assoc_Name) {
    res = true;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level1blank,
    Validation_ID_Text: `Association Name required - ${ AssociationCodeNameMapping[assoc.Assoc_Type_Code]}`,
    hoverText: `Association Name required - ${ AssociationCodeNameMapping[assoc.Assoc_Type_Code]}`,
    Validation_ID: 29
  }
}

/**
 * Liendholder has not subordinated
 */
export const LienholderSubordinationValidation: AgGridValidation<Loan_Association> = (assoc, _ ) => {
  let res = false;

  if(assoc.Assoc_Type_Code == AssociationTypeCode.LienHolder && assoc.Documentation == 'Lienholder has not subordinated') {
    res = true;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2,
    Validation_ID_Text: `Lienholder has not subordinated. - ${assoc.Assoc_Name}`,
    hoverText: `Lienholder has not subordinated. - ${assoc.Assoc_Name}`,
    Validation_ID: 33
  }
}

/**
 * Liendholder Documentation Required
 */
export const LienholderDocumentationValidation: AgGridValidation<Loan_Association> = (assoc, _ ) => {
  let res = false;

  if(assoc.Assoc_Type_Code == AssociationTypeCode.LienHolder && !assoc.Documentation) {
    res = true;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2,
    Validation_ID_Text: `Documentation Type required. - Lienholder | ${assoc.Assoc_Name}`,
    hoverText: `Documentation Type required. - Lienholder | ${assoc.Assoc_Name}`,
    Validation_ID: 36
  }
}

/**
 * Duplicate Association Name/Contact
 */
export const DuplicateAssoicationNameContactValidation: AgGridValidation<Loan_Association> = (assoc, _ ) => {
  let res = false;

  if(assoc.Duplicate_Assoc) {
    res = true;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2,
    Validation_ID_Text: `Duplicate Assocication Type/Association Name/Contact -  ${ AssociationCodeNameMapping[assoc.Assoc_Type_Code]}/${assoc.Assoc_Name}/${assoc.Contact}`,
    hoverText: `Duplicate Assocication Type/Association Name/Contact -  ${ AssociationCodeNameMapping[assoc.Assoc_Type_Code]}/${assoc.Assoc_Name}/${assoc.Contact}`,
    Validation_ID: 32
  }
}


/**
 * Email Validation Function for ag-grid cell ( Association Table )
 * @returns AgGridValidationResult
 */
export const EmailValidation: AgGridValidation<Loan_Association> = (assoc, _ ) => {
  let res = true;
  let level: validationlevel;

  if(assoc.Email) {
    if(RegEx.Email.test(assoc.Email)){
      res =  false;
    } else {
      level = validationlevel.level1;
    }
  } else {
    res = false;
  }

  return <AgGridValidationResult>{
    result: res,
    level: level,
    Validation_ID_Text: `Invalid Email - ${assoc.Assoc_Name} | ${ AssociationCodeNameMapping[assoc.Assoc_Type_Code]}`,
    hoverText: `Invalid Email - ${assoc.Assoc_Name} | ${ AssociationCodeNameMapping[assoc.Assoc_Type_Code]}`,
    Validation_ID: 4
  }
}

function isEmailMandatory(assoc: Loan_Association) {
  return (
    assoc.Assoc_Type_Code === AssociationTypeCode.Distributor
  );
}

/**
 * Phone Number Validation for ag-grid cell ( Association Table )
 * @returns AgGridValidationResult
 */
export const PhoneNumberValidation: AgGridValidation<Loan_Association> = (assoc, _ ) => {

  let res = false;
  let level: validationlevel;

  if(!!assoc.Phone){
    if(RegEx.Phone.test(assoc.Phone)){
      res =  false;
    } else {
      res = true;
      level= validationlevel.level2;
    }
  } else {
    //Validation is only triggered when A value is entered.
    //No Value should be triggered via required
    res = false;
    level = validationlevel.level2blank;
  }

  return <AgGridValidationResult>{
    result: res,
    level: level,
    Validation_ID_Text: `Phone number required -  ${assoc.Assoc_Name} | ${ AssociationCodeNameMapping[assoc.Assoc_Type_Code]}`,
    hoverText: `Phone number required -  ${assoc.Assoc_Name} | ${ AssociationCodeNameMapping[assoc.Assoc_Type_Code]}`,
    Validation_ID: 5
  }
}


/**
 * Borrower Income History table Validation
 * @returns AgGridValidationResult
 */
export const BorrowerIncomeHistoryValidation: AgGridValidation<Borrower_Income_History> = (income, _ ) => {
  let res =  income.Borrower_Revenue < income.Borrower_Expense;

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level1
  }
}

export const InsuredFlagValidation: AgGridValidation<Loan_Collateral> = (insured, _ ) => {

  // let collateralInsDisc = insured.collateralInsDisc;
  // let collateralInsDiscItem = collateralInsDisc.find(col => col.Collateral_Category_Code == insured.Collateral_Category_Code);

  let res =  insured.Insured_Flag && Number(insured.Insured_Flag) >= 0 ? false : true;

  let validationResult = insured.Insured_Flag ? validationlevel.level2 : validationlevel.level2blank;

  return <AgGridValidationResult>{
    result: res,
    level: validationResult
  }
}

export const MeasureValidation: AgGridValidation<any> = (measure, _ ) => {

  let res = false;

  let measureItems = measure.measureItems;
  let measureItem = measureItems.find( mea => mea.Measurement_Type_Code == measure.Measure_Code);

  res = measureItem && measureItem.Exception_Ind == 'Y' ? true : false;

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2
  }
}

/**
 * Collateral Detail Validation.
 * @returns AgGridValidationResult
 */
export const BorrowerAndContractValidation: AgGridValidation<Loan_Collateral> = (params, current) => {

  let res = true;
  let text = `${To_Friendly_Name(current)} required`;
  let validation_id = 26;

  if((params.Assoc_ID && params.Contract && params.Contract_Type_Code && params.Qty && params.Price) ||
    (!params.Assoc_ID && !params.Contract && !params.Contract_Type_Code && !params.Qty && !params.Price) ||
    params[current]){
      res=  false;
  }

  if((current == 'Qty'|| current == 'Price') && params[current]){
    res = Number(params[current]) <= 0 ? true : res;
  }

  let validationResult = params[current] ? validationlevel.level2 : validationlevel.level2blank;

  if(current == 'Lien_Holder') {
    text = 'Lienholder name required';
  }

  if(current == 'Assoc_ID') {
    text = 'Buyer required';
  }

  if(current == 'Qty') {
    text = 'Quantity required';
    validation_id = 16;
  }

  if(current == 'Price') {
    text = 'Price required';
    validation_id = 17;
  }

  text = `${text} - Additional Collateral | ${ CollateralCategoryMapping[params.Collateral_Category_Code]}`;

  text = `${text} | ${params.FC_Crop_Name} | ${params.Crop_Type || '-'} | ${params.Collateral_Description}`;

  return <AgGridValidationResult>{
    result: res,
    level: validationResult,
    Validation_ID_Text: text,
    hoverText: text,
    Validation_ID: validation_id
  }
}


/**
 * Marketing Contracts table Validation.
 * @returns AgGridValidationResult
 */
export const MarketingContractValidation: AgGridValidation<Loan_Marketing_Contract> = (params, current) => {

  let res = true;
  let text = `${To_Friendly_Name(current)} required`;
  let validation_id = 26;

  let validationResult = params[current] ? validationlevel.level2 : validationlevel.level2blank;

  if(current == 'Assoc_ID') {
    res = params.Assoc_ID > 0 ? false : true;
    text = 'Buyer required';
  }

  if(current == 'Contract') {
    res = params.Contract ? false : true;
    text = 'Contract ID required';
  }

  if(current == 'Contract_Type_Code') {
    res = params.Contract_Type_Code ? false : true;
    text = 'Contract Type required';
  }

  if(current == 'Quantity') {
    res = params.Quantity > 0 ? false : true;
    text = 'Quantity required';
  }

  if(current == 'Price') {
    res = params.Price > 0 ? false : true;
    text = 'Price required';
  }

  text = `${text} - Marketing Contract | ${params.FC_Crop_Name} | ${params.Crop_Type_Code || '-'}`;

  return <AgGridValidationResult>{
    result: res,
    level: validationResult,
    Validation_ID_Text: text,
    hoverText: text,
    Validation_ID: validation_id
  }
}

/**
 * Collateral Detail table Lien Validation.
 * @returns AgGridValidationResult
 */
export const LienValidation: AgGridValidation<Loan_Collateral> = (params, current) => {

  let res = true;
  let text = '';

  if((!params.Prior_Lien_Amount && !params.Lien_Holder) || (params.Prior_Lien_Amount && params.Lien_Holder) ||
     (current == 'Prior_Lien_Amount' && ((params.Prior_Lien_Amount && Number(params.Prior_Lien_Amount) > 0) && params.Prior_Lien_Amount && !params.Lien_Holder)) ||
     (current == 'Lien_Holder' && (!params.Prior_Lien_Amount || (params.Prior_Lien_Amount &&  Number(params.Prior_Lien_Amount) <= 0))  && params.Lien_Holder) ||
     params[current]){
    res=  false;
  }

  let validationResult = params[current] ? validationlevel.level2 : validationlevel.level2blank;

  if(current == 'Lien_Holder') {
    text = 'Lienholder required';
  }

  if(current == 'Prior_Lien_Amount') {
    text = 'Prior Lien Amount required';
  }

  text = `${text} - Additional Collateral | ${CollateralCategoryMapping[params.Collateral_Category_Code]} | ${params.FC_Crop_Name} | ${params.Crop_Type || '-'} | ${params.Collateral_Description}`;

  return <AgGridValidationResult>{
    result: res,
    level: validationResult,
    Validation_ID_Text: text,
    hoverText: text,
    Validation_ID: 18
  }
}


/**
 * Rent % and Prod % Validation
 * @param farm Loan Farm
 * @param key Column Key ( Rentperc or Percent_Prod )
 */
export const RentAndProdPercValidation: AgGridValidation<Loan_Farm> = (farm, key) => {
  let res = true;

  if(parseFloat(farm[key]) < 0 || parseFloat(farm[key]) > 100 ) {
    res = true;
  } else {
    res = false;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2,
    Validation_ID_Text: `Invalid Prod % / Rent % - ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    hoverText: `Invalid Prod % / Rent % - ${farm.FC_State_Code} | ${farm.FC_County_Name} | ${farm.FSN}`,
    Validation_ID: 12
  }
}


/**
 * Third Party Amount
 */
export const ThirdPartyAmountValidation: AgGridValidation<Loan_Association> = (assoc, _) => {
  let res = true;

  if(assoc.FC_INVALID_THR) {
    res = true;
  } else {
    res = false;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2,
    Validation_ID_Text: `Thid Party Credit Amount must be greater than Third Party Budget for Crop Inputs`,
    hoverText: `Thid Party Credit Amount must be greater than Third Party Budget for Crop Inputs`,
    Validation_ID: 15
  }
}


/**
 * Total Used Acres = 0
 */
export const OptimizerTotalUsedAcres: AgGridValidation<Loan_Crop_Unit> = (cu, _) => {
  let res = true;

  if(!cu.FC_Total_Used_Acres && !cu['istotal']) {
    res = true;
  } else {
    res = false;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2,
    Validation_ID_Text: `No Optimizer acres given for Farm - ${cu.FC_State_Name} | ${cu.FC_County_Name} | ${cu.FC_FSN} | ${cu['Crop']} | ${cu['Crop_Type']} | ${cu['Practice']} | ${cu['Crop_Practice']}`,
    hoverText: `No Optimizer acres given for Farm - ${cu.FC_State_Name} | ${cu.FC_County_Name} | ${cu.FC_FSN} | ${cu['Crop']} | ${cu['Crop_Type']} | ${cu['Practice']} | ${cu['Crop_Practice']}`,
    Validation_ID: 19
  }
}

/**
 * Optimizer Total Used Irr Acres > Farm Irr Acres
 */
export const OptimizerTotalUsedAcresExceedingFarmIrrAcres: AgGridValidation<Loan_Crop_Unit> = (cu, _) => {
  let res = true;

  if(cu.FC_Total_Used_Irr_Acres && cu.FC_Irr_Acres && cu.FC_Total_Used_Irr_Acres > cu.FC_Irr_Acres) {
    res = true;
  } else {
    res = false;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level1,
    Validation_ID_Text: `Total Optimizer Irr acres cannot be more than Farm Irr acres. - ${cu.FC_State_Name} | ${cu.FC_County_Name} | ${cu.FC_FSN} | ${cu['Crop']} | ${cu['Crop_Type']} | ${cu['Practice']} | ${cu['Crop_Practice']}`,
    hoverText: `Total Optimizer Irr acres cannot be more than Farm Irr acres. - ${cu.FC_State_Name} | ${cu.FC_County_Name} | ${cu.FC_FSN} | ${cu['Crop']} | ${cu['Crop_Type']} | ${cu['Practice']} | ${cu['Crop_Practice']}`,
    Validation_ID: 20,
    ignore: true
  }
}

/**
 * Optimizer Total Used Irr Acres > Farm Irr Acres
 */
export const OptimizerTotalUsedAcresExceedingTotalIRR: AgGridValidation<Loan_Crop_Unit> = (cu, _) => {
  let res = true;

  if(cu['istotal'] && cu['Is_Invalid']) {
    res = true;
  } else {
    res = false;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level1,
    Validation_ID_Text: `Total Optimizer Irr acres cannot be more than Farm Irr acres. - ${cu.FC_State_Name} | ${cu.FC_County_Name} | ${cu.FC_FSN}`,
    hoverText: `Total Optimizer Irr acres cannot be more than Farm Irr acres. - ${cu.FC_State_Name} | ${cu.FC_County_Name} | ${cu.FC_FSN}`,
    Validation_ID: 20
  }
}

/**
 * Optimizer Total Used NI Acres > Farm NI Acres
 */
export const OptimizerTotalUsedAcresExceedingFarmNIAcres: AgGridValidation<Loan_Crop_Unit> = (cu, _) => {
  let res = true;

  if(cu.FC_Total_Used_NI_Acres && cu.FC_NI_Acres && cu.FC_Total_Used_NI_Acres > cu.FC_NI_Acres) {
    res = true;
  } else {
    res = false;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level1,
    Validation_ID_Text: `Total Optimizer NI acres cannot be more than Farm NI acres. - ${cu.FC_State_Name} | ${cu.FC_County_Name} | ${cu.FC_FSN} | ${cu['Crop']} | ${cu['Crop_Type']} | ${cu['Practice']} | ${cu['Crop_Practice']}`,
    hoverText: `Total Optimizer NI acres cannot be more than Farm NI acres. - ${cu.FC_State_Name} | ${cu.FC_County_Name} | ${cu.FC_FSN} | ${cu['Crop']} | ${cu['Crop_Type']} | ${cu['Practice']} | ${cu['Crop_Practice']}`,
    Validation_ID: 21,
    ignore: true
  }
}

/**
 * Optimizer Total Used NI Acres > Farm NI Acres
 */
export const OptimizerTotalUsedAcresExceedingTotalNI: AgGridValidation<Loan_Crop_Unit> = (cu, _) => {
  let res = true;

  if(cu['istotal'] && cu['Is_Invalid']) {
    res = true;
  } else {
    res = false;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level1,
    Validation_ID_Text: `Total Optimizer NI acres cannot be more than Farm NI acres. - ${cu.FC_State_Name} | ${cu.FC_County_Name} | ${cu.FC_FSN}`,
    hoverText: `Total Optimizer NI acres cannot be more than Farm NI acres. - ${cu.FC_State_Name} | ${cu.FC_County_Name} | ${cu.FC_FSN}`,
    Validation_ID: 21
  }
}

/**
 * Total Acres in Farm = 0
 */
export const FarmTotalAcres: AgGridValidation<Loan_Farm> = (farm, _) => {
  let res = true;

  if((!farm.Irr_Acres && !farm.NI_Acres) || (!farm.FC_Total_Acres || farm.FC_Total_Acres == 0)) {
    res = true;
  } else {
    res = false;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2,
    Validation_ID_Text: `Both Irr Acres and NI acres cannot be 0 - ${farm.FC_State_Code || ''} | ${farm.FC_County_Name || ''} | ${farm.FSN || ''}`,
    hoverText: `Both Irr Acres and NI acres cannot be 0 - ${farm.FC_State_Code || ''} | ${farm.FC_County_Name || ''} | ${farm.FSN || ''}`,
    Validation_ID: 14
  }
}

/**
 * Non Approved Budget Expense
 */
export const BudgetExpenseTypeNameValidation: AgGridValidation<Loan_Budget> = (bgt, _) => {
  let res = true;

  if(!bgt.Expense_Type_ID) {
    res = true;
  } else {
    res = false;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2,
    Validation_ID_Text: `Expense Type Invalid - ${bgt.Expense_Type_Name}`,
    hoverText: `Expense Type Invalid - ${bgt.Expense_Type_Name}`,
    Validation_ID: 22
  }
}

/**
 * Non Approved Other Income
 */
export const OtherIncomeNameValidation: AgGridValidation<Loan_Other_Income> = (income, _) => {
  let res = true;

  if(!income.Other_Income_ID) {
    res = true;
  } else {
    res = false;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level2,
    Validation_ID_Text: `Other Income Invalid - ${income.Other_Income_Name}`,
    hoverText: `Other Income Invalid - ${income.Other_Income_Name}`,
    Validation_ID: 28
  }
}

/**
 * APH Validation
 */
export const APHValidation: AgGridValidation<Loan_Crop_Unit> = (aph, _) => {
  let res = true;

  if(aph.CU_Acres > 0 && !aph.Ins_APH) {
    res = true;
  } else {
    res = false;
  }

  return <AgGridValidationResult>{
    result: res,
    level: validationlevel.level1,
    Validation_ID_Text: `APH Required - ${aph.Crop_Code} | ${aph['Croptype']} | ${aph.Crop_Practice_Type_Code} | ${aph['Crop_Practice_Type']}`,
    hoverText: `APH Required - ${aph.Crop_Code} | ${aph['Croptype']} | ${aph.Crop_Practice_Type_Code} | ${aph['Crop_Practice_Type']}`,
    Validation_ID: 28
  }
}
