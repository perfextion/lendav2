import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderIconsPanelComponent } from './header-icons-panel.component';

describe('HeaderIconsPanelComponent', () => {
  let component: HeaderIconsPanelComponent;
  let fixture: ComponentFixture<HeaderIconsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderIconsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderIconsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
