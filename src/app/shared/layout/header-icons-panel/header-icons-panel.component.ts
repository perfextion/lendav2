import { Component, OnInit, Input, OnChanges, ViewChild, Renderer2, OnDestroy } from '@angular/core';
import { MatMenuTrigger } from '@angular/material';

import { LocalStorageService } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';
import { JsonConvert } from 'json2typescript';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';
import { loan_model } from '@lenda/models/loanmodel';
import { PendingAction, EmailTemplateType } from '@lenda/models/pending-action/add-pending-action.model';
import { RefDataModel } from '@lenda/models/ref-data-model';

import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-header-icons-panel',
  templateUrl: './header-icons-panel.component.html',
  styleUrls: ['./header-icons-panel.component.scss']
})
export class HeaderIconsPanelComponent implements OnInit, OnChanges, OnDestroy {
  @Input()
  public loanId: string;

  public currentLoan: loan_model;
  public refdata: RefDataModel;
  public loggedInUserId: number;

  public model: boolean = false;

  @ViewChild(MatMenuTrigger) public trigger: MatMenuTrigger;

  public PA: typeof PendingAction = PendingAction;
  public pa_code: string;

  private subscription: ISubscription;
  private syncSubscription: ISubscription;

  constructor(
    private dataService: DataService,
    private localStorage: LocalStorageService,
    private renderer: Renderer2,
    private loanApi: LoanApiService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.currentLoan = res;
      this.loggedInUserId = this.localStorage.retrieve(environment.uid);
    });
  }

  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.syncSubscription) {
      this.syncSubscription.unsubscribe();
    }
  }

  ngOnChanges() {
    this.currentLoan = this.localStorage.retrieve(environment.loankey);
    this.refdata = this.localStorage.retrieve(environment.referencedatakey);
    this.loggedInUserId = this.localStorage.retrieve(environment.uid);
  }

  hasPendingAction(pa_code: string) {
    try {
      return this.currentLoan.User_Pending_Actions.some(
        a => a.ActionStatus != 3 && a.PA_Group_Code == pa_code && a.Status == 1
      );
    } catch {
      return false;
    }
  }

  onClick($event) {
    $event.stopPropagation();
    $event.preventDefault();
    this.trigger.closeMenu();
  }

  onRightClick(pa_code: string, $event) {
    $event.stopPropagation();
    $event.preventDefault();
    if(this.hasPendingAction(pa_code)) {
      let pa = this.currentLoan.User_Pending_Actions.find(
        a => a.ActionStatus != 3 && a.PA_Group_Code == pa_code && a.Status == 1
      );

      this.model = pa && pa.Reminder_Ind == 1;
      this.trigger.openMenu();

      let overlayElement = (this.trigger as any)._overlayRef.overlayElement as HTMLDivElement;
      overlayElement.className.split(' ').forEach(cls => {
        this.renderer.removeClass(overlayElement, cls);
      });
      this.renderer.addClass(overlayElement, 'cdk-overlay-pane');
      this.renderer.addClass(overlayElement, pa_code);
      this.pa_code = pa_code;
    }
  }

  onSliderChange($event: any) {
    let syncRequired = this.currentLoan.User_Pending_Actions.some(a => a.PA_Group_Code == this.pa_code && a.ActionStatus == 1);
    if(syncRequired) {
      this.UpdateReminderIndAfterSync($event);
    } else {
      this.UpdateReminderInd($event);
    }
  }

  private UpdateReminderIndAfterSync($event: any) {
    this.syncSubscription = this.loanApi.syncloanobject(this.currentLoan).subscribe(res => {
      if (res.ResCode == 1) {
        this.loanApi.getLoanById(this.currentLoan.Loan_Full_ID).subscribe(res => {
          if (res.ResCode == 1) {
            let jsonConvert: JsonConvert = new JsonConvert();
            this.currentLoan = jsonConvert.deserialize(res.Data, loan_model);
            this.dataService.setLoanObjectwithoutsubscription(this.currentLoan);
            this.UpdateReminderInd($event);
          }
        });
      }
      else {
        this.toastr.error('Reminder Update Failed');
      }
    }, _ => {
      this.toastr.error('Reminder Update Failed');
    });
  }

  UpdateReminderInd($event) {
    const model = {
      Loan_Full_ID: this.currentLoan.Loan_Full_ID,
      Pending_Action: {
        Reminder_Ind: $event.checked ? 1 : 0,
        PA_Group_Code: this.pa_code
      },
      From_Loan_List: false
    };

    let EmailBody = {
      LoanFullID: this.currentLoan.Loan_Full_ID,
      EmailTemplateType: EmailTemplateType.PendingActionReminder,
      EmailData: {
        PA_Code: this.pa_code,
        User_ID: this.localStorage.retrieve(environment.uid)
      }
    };

    this.loanApi.Update_Reminder_Ind(model).subscribe(res => {
      if(res.ResCode == 1) {
        this.currentLoan.User_Pending_Actions = res.Data;
        this.dataService.setLoanObjectwithoutsubscription(this.currentLoan);
        this.loanApi.sendEmailAndNotification(EmailBody).subscribe(res => {});
        this.toastr.success('Reminder Updated');
      } else {
        this.toastr.error('Reminder Update Failed');
      }
    }, _ => {
      this.toastr.error('Reminder Update Failed');
    });
  }
}
