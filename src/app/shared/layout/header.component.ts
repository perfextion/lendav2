import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';

import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { JsonConvert } from 'json2typescript';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';
import { Page } from '@lenda/models/page.enum';

import { ReferenceService } from '@lenda/services/reference/reference.service';
import { LayoutService } from './layout.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { PublishService } from '@lenda/services/publish.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { ApiService } from '@lenda/services';
import { RefDataService } from '@lenda/services/ref-data.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoanvalidatorService } from '@lenda/services/loanvalidator.service';
import { MasterService } from '@lenda/master/master.service';
import { DataService } from '@lenda/services/data.service';

import { loan_model, LoanStatus, LoanGroup, Loantype_dsctext, Loan_Type_Codes } from '@lenda/models/loanmodel';
import { versions } from '@lenda/versions';
import { Preferences } from '@lenda/preferences/models/index.model';
import { ResponseModel } from '@lenda/models/resmodels/reponse.model';
import { ChangeStatusModel, LoanStatusChange } from '@lenda/models/copy-loan/copyLoanModel';
import { EditLoanParamsModel } from '@lenda/models/edit-loan-params/edit-loan-params.model';
import { RefDataModel, LoanMaster, Ref_Loan_Type } from '@lenda/models/ref-data-model';
import { LoggedInUserModel } from '@lenda/models/commonmodels';
import { GetAllUsers } from '@lenda/components/committee/members/member.model';
import { PendingActionGhostingService } from '@lenda/alertify/components/pending-action-ghosting/pending-action-ghosting.service';

import { FavouriteLoan } from '@lenda/pipes/favourite-loan.pipe';
import { Get_Distributor_Name, IsCurrentYearLoan, GetCurrentCropYear } from '@lenda/services/common-utils';

@Component({
  selector: 'layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  public loanid: string = ""
  public apiurl = environment.apiUrl;
  public Git = versions.revision;
  public Databasename: string = "";
  public value: number = 1;
  public isExpanded: boolean = true;
  public isRightSidebarExpanded: boolean;
  public loggedinusername: String;
  public isUserAdmin: boolean = false;
  public isDevAdmin: boolean = false;
  public favoriteList: Array<FavouriteLoan> = [];

  private localloanobject: loan_model;
  private refData: RefDataModel;
  private currentPage: string;
  private isSyncRequired: boolean = false;

  public isGhostingEnabled: boolean;
  private ghostUserDetails: GetAllUsers;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    public localst: LocalStorageService,
    public referencedataapi: ReferenceService,
    public toaster: ToastrService,
    public loancalculation: LoancalculationWorker,
    private loanService: LoanApiService,
    public layoutService: LayoutService,
    private dataService: DataService,
    private sessionservice: SessionStorageService,
    private alertify: AlertifyService,
    private publishService: PublishService,
    private settingsService: SettingsService,
    private logging: LoggingService,
    private loanvalidator: LoanvalidatorService,
    private apiService: ApiService,
    public masterService: MasterService,
    private refDataService: RefDataService,
    private pendingActionGhosting: PendingActionGhostingService
  ) {

    this.localloanobject = this.localst.retrieve(environment.loankey);
    this.refData = this.localst.retrieve(environment.referencedatakey);

    this.ghostUserDetails = this.localst.retrieve(environment.ghostUserDetails);
    this.isGhostingEnabled = !!this.ghostUserDetails;

    this.publishService.listenToSyncRequired().subscribe(res => {
      this.isSyncRequired = res && res.length > 0;
      if(document.location.href.includes('loanoverview')) {
        if(this.localloanobject) {
          this.updateFavouriteLoanList(this.localloanobject, !this.isSyncRequired);
        }
      }
    });

    this.subscription = this.dataService.getLoanObject().subscribe((res: loan_model) => {
      if (res != undefined && res != null && res.Loan_Full_ID) {
        this.loanid = res.Loan_Full_ID.replace("-", "/");
      }
      this.localloanobject = res;

      if(!document.location.href.includes('debug') && !document.location.href.includes('loans')) {
        this.updateFavouriteLoanList(res, false);
      }

      //this.checkAndOpenCopyLoanDialog();
    });

    let user = this.sessionservice.retrieve("UID");
    if (!_.isEmpty(user)) {
      this.isUserAdmin = user.IsAdmin == 1;
      this.isDevAdmin = user.DevAdmin == 1;
    }

    this.getloanid();
  }

  ngOnInit() {
    this.getCurrentPage();

    //default open
    this.value = this.localst.retrieve(environment.logpriority);
    this.layoutService.isSidebarExpanded().subscribe((value) => {
      this.isExpanded = value;
    });

    this.layoutService.isRightSidebarExpanded().subscribe((value) => {
      this.isRightSidebarExpanded = value;
    });

    this.loggedinusername = this.localst.retrieve("loggedinuser");
    let refDataFavList = this.localst.retrieve(environment.referencedatakey);
    if(refDataFavList) {
      this.favoriteList = refDataFavList[environment.favorites + this.localst.retrieve(environment.uid)];
    } else {
      this.favoriteList = [];
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private getCurrentPage(){
    this.currentPage = this.localst.retrieve(
      environment.currentpage
    );

    if(this.currentPage == 'summary'){
      this.currentPage = Page.reports;
    }
  }

  private updateFavouriteLoanList(res: loan_model, syncFavList = false) {
    let isLoanidOnFavList = _.find(this.favoriteList, (el) => {
      return el.loanFullID.replace("-", "/") == this.loanid;
    });

    let dist_name = Get_Distributor_Name(res, false);

    let loanDetails = {
      loanFullID: this.loanid,
      loanCropYear: res.LoanMaster.Crop_Year,
      borrowerName: `${res.LoanMaster.Borrower_First_Name} ${res.LoanMaster.Borrower_Last_Name}`,
      loanTypeAndDistCode: `${res.LoanMaster.Loan_Type_Name == 'Ag-Input' ? (res.LoanMaster.Loan_Type_Name + ' ( ' + dist_name + ' )' ) : res.LoanMaster.Loan_Type_Name}`
    };

    if(!loanDetails.loanFullID) {
      return;
    }

    if (_.isEmpty(isLoanidOnFavList)) {
      if (this.favoriteList.length < 10) {
        this.favoriteList.unshift(loanDetails);
      } else {
        this.favoriteList.unshift(loanDetails);
        this.favoriteList.pop();
      }

      this.settingsService.preferences.favouriteLoanList = this.favoriteList;
      this.settingsService.savePreferencesAsync(this.settingsService.preferences);
    } else {
      if(this.favoriteList) {
        let index = this.favoriteList.findIndex(a => a.loanFullID == this.loanid);
        if(index > 0 || syncFavList) {
          this.updateFavLoanListInPreferences(index, loanDetails);
        }
      }
    }

    _.remove(this.favoriteList, a => !a.loanFullID);

    let refDataFavList = this.localst.retrieve(environment.referencedatakey);
    refDataFavList[environment.favorites + this.localst.retrieve(environment.uid)] = this.favoriteList;
    this.localst.store(environment.referencedatakey, refDataFavList);
  }

  private updateFavLoanListInPreferences(index: number, loanDetails: FavouriteLoan) {
    this.favoriteList.splice(index, 1);
    this.favoriteList.unshift(loanDetails);
    this.settingsService.preferences.favouriteLoanList = this.favoriteList;
    this.settingsService.savePreferencesAsync(this.settingsService.preferences);
  }

  createLoan() {
    if(location.href.includes('loans')) {
      this.Create_New_Loan();
    } else {
      this.Check_Sync_And_Perform(() => {
        this.Create_New_Loan();
      });
    }
  }

  private Create_New_Loan() {
    // Get default landing page from my preferences
    let preferences: Preferences = this.settingsService.preferences;

    let loanObj: LoanMaster = <LoanMaster>{};

    this.Set_Defaults(loanObj);
    this.Set_Office_And_Region_ID(loanObj);

    this.loanService.Create_New_Loan(loanObj).subscribe(res => {
      this.logging.checkandcreatelog(3, 'Overview', "APi LOAN GET with Response " + res.ResCode);
      if (res.ResCode == 1) {

        this.loanvalidator.validateloanobject(res.Data).then(errors => {
          if (errors && errors.length > 0) {

            this.toaster.error("Loan Data is invalid ... Rolling Back to list")
            this.router.navigateByUrl("/home/loans");


          } else {
            let jsonConvert: JsonConvert = new JsonConvert();

            this.loancalculation.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model));
            this.localst.store(environment.errorbase, []);
            this.localst.store(environment.modifiedbase, []);
            this.localst.store(environment.modifiedoptimizerdata, []);
            this.publishService.syncCompleted();

            this.loancalculation.saveValidationsToLocalStorage(res.Data);

            //we are making a copy of it also
            this.localst.store(environment.loankey_copy, res.Data);
            this.localst.store(environment.currentpage, preferences.userSettings.landingPage);
            this.localst.store(environment.loanGroup, []);
            this.localst.store(environment.loanidkey, (res.Data as loan_model).Loan_Full_ID);
            this.router.navigateByUrl("/home/loanoverview/" + this.loanid.replace("-", "/") + '/borrower');

            //loan affiliated loan, currently static
            const route = '/api/Loans/GetLoanGroups?LoanFullID=' + (res.Data as loan_model).Loan_Full_ID;
            this.apiService.get(route).subscribe((res: ResponseModel) => {
              if (res) {
                let loanGrpups: Array<LoanGroup> = res.Data;
                this.localst.store(environment.loanGroup, res.Data);
              }
            });

            this.localst.store(environment.loanCrossCollateral, []);
            this.loanService.getDistinctLoanIDList(this.loanid).subscribe(res => {
              if(res && res.ResCode == 1) {
                this.localst.store(environment.loanCrossCollateral, res.Data);
              }
            });
          }
          this.localst.store(environment.exceptionStorageKey, []);
          this.localst.store(environment.modifiedbase, []);
          this.localst.store(environment.modifiedoptimizerdata, null);
        });
      }
      else {
        this.toaster.error("Could not fetch Loan Object from API")
      }
    });
  }

  public get canCopyAndCreateNewLoan() {
    return !IsCurrentYearLoan(this.localloanobject.LoanMaster.Crop_Year);
  }

  Copy_And_Create_New_Loan() {
    this.Check_Sync_And_Perform(() => {
      this.alertify.confirm('Confirm', 'Are you sure you want to leave the loan?').subscribe(res => {
        if (res) {
          this.Copy_And_Create_New_Loan_To_Current_Year();
        }
      });
    });
  }

  private Copy_And_Create_New_Loan_To_Current_Year() {
    let loanObj = this.localloanobject.LoanMaster;

    this.Set_Defaults(loanObj);
    this.Set_Office_And_Region_ID(loanObj);

    this.loanService.Copy_And_Create_New_Loan_To_Current_Year(loanObj).subscribe(res => {
      if (res.ResCode == 1) {

        this.loanvalidator.validateloanobject(res.Data).then(errors => {
          if (errors && errors.length > 0) {
            this.toaster.error("Loan Data is invalid ... Rolling Back to list")
            this.router.navigateByUrl("/home/loans");
          } else {
            let jsonConvert: JsonConvert = new JsonConvert();

            this.localst.store('CopyLoan', 1);
            this.loancalculation.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model));
            this.localst.store(environment.errorbase, []);
            this.localst.store(environment.modifiedbase, []);
            this.localst.store(environment.modifiedoptimizerdata, []);
            this.publishService.syncCompleted();

            this.loancalculation.saveValidationsToLocalStorage(res.Data);

            //we are making a copy of it also
            this.localst.store(environment.loankey_copy, res.Data);
            this.localst.store(environment.currentpage, 'borrower');
            this.localst.store(environment.loanGroup, []);
            this.localst.store(environment.loanidkey, (res.Data as loan_model).Loan_Full_ID);
            this.router.navigateByUrl("/home/loanoverview/" + this.loanid.replace("-", "/") + '/borrower');
            //loan affiliated loan, currently static
            const route = '/api/Loans/GetLoanGroups?LoanFullID=' + (res.Data as loan_model).Loan_Full_ID;
            this.apiService.get(route).subscribe((res: ResponseModel) => {
              if (res) {
                let loanGrpups: Array<LoanGroup> = res.Data;
                this.localst.store(environment.loanGroup, res.Data);
              }
            });

            this.localst.store(environment.loanCrossCollateral, []);
            this.loanService.getDistinctLoanIDList(this.loanid).subscribe(res => {
              if(res && res.ResCode == 1) {
                this.localst.store(environment.loanCrossCollateral, res.Data);
              }
            });
          }
          this.localst.store(environment.exceptionStorageKey, []);
          this.localst.store(environment.modifiedbase, []);
          this.localst.store(environment.modifiedoptimizerdata, null);
        });
      } else {
        this.toaster.error(res.Message || "Some error occured while creating new loan.");
      }
    },
    () => {
      this.toaster.error("Some error occured while creating new loan.");
    });
  }

  private Set_Defaults(loanObj: LoanMaster) {
    // Loan_Type_Code
    loanObj.Loan_Type_Code = Loan_Type_Codes.AllIn;
    loanObj.Loan_Type_Name = Loantype_dsctext(Loan_Type_Codes.AllIn);

    // terms
    let loanTypeDetails = this.getLoanTypeDetails(Loan_Type_Codes.AllIn);

    loanObj.Origination_Fee_Percent = loanTypeDetails.Origination_Fee_Percent;
    loanObj.Service_Fee_Percent = loanTypeDetails.Service_Fee_Percent;
    loanObj.Extension_Fee_Percent = loanTypeDetails.Extension_Fee_Percent;
    loanObj.Interest_Percent = loanTypeDetails.Rate_Percent;

    // balance sheet
    loanObj.Current_Assets_Disc_Percent = this.getDiscount('CURRENT_DISC');
    loanObj.Inter_Assets_Disc_Percent = this.getDiscount('INTER_DISC');
    loanObj.Fixed_Assets_Disc_Percent = this.getDiscount('FIXED_DISC');

    // Maturity Date
    let crop_year = GetCurrentCropYear();
    loanObj.Maturity_Date = this.refDataService.getDefaultMaturityDateString(crop_year);

    let x = new Date();
    loanObj.Close_Date = new Date(crop_year, x.getMonth(), x.getDate());
  }

  private Set_Office_And_Region_ID(loanObj: LoanMaster) {
    let user: LoggedInUserModel = this.sessionservice.retrieve('UID');

    if(user) {
      loanObj.Loan_Officer_ID = user.UserID;
      loanObj.Loan_Officer_Name = (user.LastName || '');

      if(user.LastName) {
        loanObj.Loan_Officer_Name += ', ';
      }

      loanObj.Loan_Officer_Name += user.FirstName;

      loanObj.Office_ID = user.Office_ID;
      loanObj.Region_ID = user.Region_ID;

      try {
        let office = this.refData.OfficeList.find(a => a.Office_ID == user.Office_ID);
        let region = this.refData.RegionList.find(a => a.Region_ID == user.Region_ID);

        loanObj.Office_Name = office ? office.Office_Name : '';
        loanObj.Region_Name = region ? region.Region_Name : '';
      } catch { }
    }
  }

  private getLoanTypeDetails(loan_type_code: Loan_Type_Codes) {
    try {
      let loanType = this.refData.Ref_Loan_Types.find(a => a.Loan_Type_Code == loan_type_code);

      if(loanType) {
        return loanType;
      } else {
        return new Ref_Loan_Type();
      }
    } catch {
      return new Ref_Loan_Type();
    }
  }

  /**
   * Get Default data
   * @param key Discount Key
   */
  private getDiscount(key: string) {
    if(!this.refData) {
      this.refData = this.localst.retrieve(environment.referencedatakey);
    }

    try {
      let disc = this.refData.Discounts.find(a => a.Discount_Key == key);
      return disc.Discount_Value;
    } catch {
      return 0;
    }
  }

  navigatetoloan(loanID: string) {
    this.Check_Sync_And_Perform(() => {
      this.getDataForLoan(loanID);
    });
  }

  getDataForLoan(loanID: string) {
    let id = loanID.replace("/", "-");
    this.loanid = id
    this.localst.store(environment.loanidkey, id);
    // Get default landing page from my preferences
    let preferences: Preferences = this.settingsService.preferences;
    //get loan here now
    this.localst.store(environment.loanGroup, []);//reset loan group if loan to be opened is not exist in local storage
    this.getLoanBasicDetailsFromFavList(preferences);
  }

  getLoanBasicDetailsFromFavList(preferences: Preferences) {
    if (this.loanid != null) {
      let loaded = false;
      try {
        this.localst.clear(environment.loankey);
        this.localst.clear(environment.loanGroup);
      } catch (e) {
        console.log(`trying to clear a non existing local storage key : ${environment.loankey}`);
      }
      this.loanService.getLoanById(this.loanid.replace("/", "-")).subscribe(res => {
        this.masterService.isMinimize = false;
        this.logging.checkandcreatelog(3, 'Overview', "APi LOAN GET with Response " + res.ResCode);
        if (res.ResCode == 1) {

          this.loanvalidator.validateloanobject(res.Data).then(errors => {
            if (errors && errors.length > 0) {

              this.toaster.error("Loan Data is invalid ... Rolling Back to list")
              this.router.navigateByUrl("/home/loans");


            } else {
              let jsonConvert: JsonConvert = new JsonConvert();
              this.localst.store(environment.errorbase, []);
              this.loancalculation.saveValidationsToLocalStorage(res.Data);

              //we are making a copy of it also
              this.localst.store(environment.loankey_copy, res.Data);

              let currentPage = this.localst.retrieve(environment.currentpage);
              if(currentPage == "loanlist" || currentPage == 'debug') {
                currentPage = preferences.userSettings.landingPage;
              }

              //loan affiliated loan, currently static
              const route = '/api/Loans/GetLoanGroups?LoanFullID=' + (res.Data as loan_model).Loan_Full_ID;
              this.apiService.get(route).subscribe((resLoanGroup: ResponseModel) => {
                if (resLoanGroup) {
                  let loanGrpups: Array<LoanGroup> = resLoanGroup.Data;
                  this.localst.store(environment.loanGroup, resLoanGroup.Data);
                  this.publishService.syncCompletedevent.next(res.Data);
                  this.loancalculation.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model));
                  this.router.navigateByUrl("/home/loanoverview/" + this.loanid.replace("-", "/") + '/' + currentPage);
                } else {
                  this.publishService.syncCompletedevent.next(res.Data);
                  this.loancalculation.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model));
                  this.router.navigateByUrl("/home/loanoverview/" + this.loanid.replace("-", "/") + '/' + currentPage);
                }
              },_ => {
                this.publishService.syncCompletedevent.next(res.Data);
                this.loancalculation.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model));
                this.router.navigateByUrl("/home/loanoverview/" + this.loanid.replace("-", "/") + '/' + currentPage);
              });

              this.localst.store(environment.loanCrossCollateral, []);
              this.loanService.getDistinctLoanIDList(this.loanid).subscribe(resDistinctLoans => {
                if(resDistinctLoans && resDistinctLoans.ResCode == 1) {
                  this.localst.store(environment.loanCrossCollateral, resDistinctLoans.Data);
                }
              });
            }
            this.localst.store(environment.exceptionStorageKey, []);
            this.localst.store(environment.modifiedbase, []);
            this.localst.store(environment.modifiedoptimizerdata, null);
          });
        }
        else {
          this.toaster.error("Could not fetch Loan Object from API")
        }
        loaded = true;
      });

    }
    else {
      this.toaster.error("Something went wrong");
    }
  }

  logout() {
    let currentPage = this.localst.retrieve(environment.currentpage);

    if(currentPage == "loanlist") {
      this.Log_Out();
    } else {
      this.Check_Sync_And_Perform(() => {
        this.Log_Out();
      });
    }
  }

  Log_Out() {
    let obj = {
      Username: this.loggedinusername
    };

    this.loanService.Log_Out(obj).subscribe(res => {
      const keyName = "jwtToken";
      localStorage.removeItem(keyName);
      this.localst.clear('isloggedin');
      this.localst.clear('loggedinuser');
      this.localst.clear('loggedinusername');
      this.localst.clear('sessionid');
      this.localst.clear('userid');
      this.localst.clear('userrole');
      this.localst.clear('logpriority');
      this.localst.clear('isadmin');
      this.localst.clear(environment.uid);
      this.router.navigateByUrl('login');
    });
  }

  changepriority(event: any) {
    this.localst.store(environment.logpriority, parseInt(event.value));

    let user = this.sessionservice.retrieve("UID");
    this.isUserAdmin = user.IsAdmin == 1;
    this.isDevAdmin = user.DevAdmin == 1;
  }

  toggleSideBar() {
    this.layoutService.toggleSidebar(!this.isExpanded);
  }

  toggleRightSidenav() {
    this.layoutService.toggleRightSidebar(!this.isRightSidebarExpanded);
  }

  hideRightSideNav() {
    this.layoutService.toggleRightSidebar(false);
  }

  isReports(){
    this.getCurrentPage();
    return this.currentPage == Page.reports;
  }

  isInsurance(){
    this.getCurrentPage();
    return this.currentPage == Page.insurance;
  }

  get canRescind() {
    if (!this.localloanobject) {
      return false;
    }
    return (
      this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Recommended ||
      this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Approved
    );
  }

  rescindLoan() {
    this.Check_Sync_And_Perform(() => {
      if (this.localloanobject && this.canRescind) {
        this.alertify.confirm('Confirm', 'Are you sure you want to rescind the Loan?').subscribe(res => {
          if(res) {
            const body = <ChangeStatusModel>{
              FromLoanFullID: this.localloanobject.Loan_Full_ID,
              ToStatus: LoanStatusChange.Rescind
            };

            this.loanService.changeStatus(body).subscribe(
              res => {
                if(res.ResCode == 1) {
                  this.toaster.success('Rescind Loan Successful.');
                  this.publishService.syncToDb();
                } else {
                  this.toaster.error('Rescind Loan Failed.');
                }
              },
              error => {
                this.toaster.error('Rescind Loan Failed.');
              }
            );
          }
        });
      }
    });
  }

  get canWithdraw() {
    if (!this.localloanobject) {
      return false;
    }
    return (
      this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Working ||
      this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Recommended ||
      this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Approved
    );
  }

  public isWorking() {
    if(this.localloanobject) {
      return this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Working;
    }
    return false;
  }

  public isUsedByOtherUser() {
    if(this.localloanobject) {
      return this.localloanobject.LoanMaster.Loan_Status.includes('_R');
    }
    return false;
  }

  public isProtectedActive() {
    if (this.localloanobject) {
      return (
        this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Uploaded ||
        this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Recommended ||
        this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Approved
      );
    }

    return false;
  }

  public isProtectedInActive() {
    if (this.localloanobject) {
      return (
        this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Historical ||
        this.localloanobject.LoanMaster.Loan_Status == LoanStatus.PaidOff ||
        this.localloanobject.LoanMaster.Loan_Status == LoanStatus.WrittenOff ||
        this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Void ||
        this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Withdrawn ||
        this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Declined
      );
    }
    return false;
  }

  public showLoanStatus(){
    return this.localloanobject && this.localloanobject.LoanMaster.Loan_Status ? true : false;
  }

  public withdrawLoan() {
    this.Check_Sync_And_Perform(() => {
      if (this.localloanobject && this.canWithdraw) {
        this.alertify
          .confirm('Confirm', 'Are you sure you want to withdraw this Loan?')
          .subscribe(res => {
            if(res) {
              const body = <ChangeStatusModel>{
                FromLoanFullID: this.localloanobject.Loan_Full_ID,
                ToStatus: LoanStatusChange.Withdrawn
              };

              this.loanService.changeStatus(body).subscribe(
                res => {
                  if(res.ResCode == 1) {
                    this.toaster.success('Withdraw Loan Successful.');
                    this.publishService.syncToDb();
                  } else {
                    this.toaster.error('Withdraw Loan Failed.');
                  }
                },
                error => {
                  this.toaster.error('Withdraw Loan Failed.');
                }
              );
            }
          });
      }
    });
  }

  public reviveCurrentLoan() {
    this.Check_Sync_And_Perform(() => {
      if (this.localloanobject) {
        this.alertify
        .confirm('Confirm', 'Are you sure you want to Revive/Copy this Loan?')
        .subscribe(res => {
          if(res) {
            const body = <ChangeStatusModel>{
              FromLoanFullID: this.localloanobject.Loan_Full_ID,
              ToStatus: LoanStatusChange.ReviveOrCopy
            };

            this.loanService.changeStatus(body).subscribe(
              res => {
                if(res.ResCode == 1) {
                  this.toaster.success('Revive/Copy Loan Successful.');
                  this.navigatetoloan(res.Data);
                } else {
                  this.toaster.error('Revive/Copy Loan Failed.');
                }
              },
              error => {
                this.toaster.error('Revive/Copy Loan Failed.');
              }
            );
          }
        });
      }
    });
  }

  public resetCurrentLoan() {
    this.Check_Sync_And_Perform(() => {
      this.getloanid();
        this.localst.store(environment.errorbase, []);
        this.localst.store(environment.exceptionStorageKey, []);
        this.localst.store(environment.modifiedbase, []);
        this.getLoanBasicDetails(() => {
          this.localst.store(environment.errorbase, []);
          this.localst.store(environment.modifiedbase, []);
          this.localst.store(environment.modifiedoptimizerdata, null);
          this.publishService.syncCompleted();
        });
    });
  }

  public getLoanBasicDetails(onSubscribe: () => void = () => {} ) {
    this.loanService.getLoanById(this.loanid.replace("/", "-")).subscribe(res => {
      if (res.ResCode == 1) {
        onSubscribe();
        let jsonConvert: JsonConvert = new JsonConvert();
        this.loancalculation.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model));

        this.loancalculation.saveValidationsToLocalStorage(res.Data);

        //we are making a copy of it also
        this.localst.store(environment.loankey_copy, res.Data);
        this.toaster.success("Reset current changes Complete")
        this.router.navigateByUrl(this.router.url);
      } else {
        this.toaster.error("Could not fetch Loan Object from API");
      }
    }, _ => {
      this.toaster.error("Could not fetch Loan Object from API");
    });
  }

  public getloanid() {
    try {
      this.loanid = this.localloanobject.Loan_Full_ID.replace("-", "/");
      this.Databasename = this.localst.retrieve(environment.referencedatakey).Databasename;
    } catch (ex) {}
  }

  public copyPreviousLoan(){
    this.alertify.copyLoan(this.loanid.replace("/", "-"));
  }

  public copyCrossCollateralLoan(){
    this.alertify.copyLoan('', true);
  }

  public get isWorkingLoan() {
    try {
      if (!this.localloanobject) {
        return false;
      }
      return this.localloanobject.LoanMaster.Loan_Status == LoanStatus.Working;
    } catch {
      return false;
    }
  }

  public editLoanParamsDialog(){
    this.Check_Sync_And_Perform(() => {
      if(this.localloanobject && this.isWorkingLoan) {
        this.alertify.editLoanParams('Edit Loan Parameters', this.localloanobject).subscribe(res => {
          if(res) {
            let model = <EditLoanParamsModel>{
              Crop_Year: this.localloanobject.LoanMaster.Crop_Year,
              LoanFullID: this.localloanobject.Loan_Full_ID,
              Office_ID: this.localloanobject.LoanMaster.Office_ID,
              Office_Name: this.localloanobject.LoanMaster.Office_Name,
              Loan_Officer_ID: this.localloanobject.LoanMaster.Loan_Officer_ID,
              Loan_Officer_Name: this.localloanobject.LoanMaster.Loan_Officer_Name,
              Region_ID: this.localloanobject.LoanMaster.Region_ID,
              Region_Name: this.localloanobject.LoanMaster.Region_Name,
              Update_Crop_Year_Ind: this.localloanobject.LoanMaster['Update_Crop_Year_Ind']
            };

            this.loanService.editLoanParams(model).subscribe(res => {
             if(res.ResCode == 1) {
               if(model.Update_Crop_Year_Ind == 1) {
                this.loanService.getLoanById(this.localloanobject.Loan_Full_ID).subscribe(res => {
                  if(res.ResCode == 1) {
                    this.toaster.success('Loan Parameters Updated.', 'Success');
                    let jsonConvert = new JsonConvert();

                    let loan = jsonConvert.deserialize(res.Data, loan_model);
                    loan.srccomponentedit = 'EditLoanParameters';

                    this.loancalculation.performcalculationonloanobject(loan, true, true);
                  } else {
                    this.toaster.error('Error in getting loan object.');
                  }
                },
                () => {
                 this.toaster.error('Error in getting loan object.');
                });
               } else {
                this.toaster.success('Loan Parameters Updated.', 'Success');
                this.dataService.setLoanObject(this.localloanobject);
               }
             } else {
              this.toaster.error('Error in Editing Loan Params');
             }
            },
            () => {
              this.toaster.error('Error in Editing Loan Params');
            });
          }
        });
      }
    });
  }

  public toggleChevrons() {
    this.masterService.toggleChevron();
  }

  private Check_Sync_And_Perform(onSuccess: () => void) {
    if(this.isSyncRequired) {
      this.alertify.confirm(
        'Confirm',
        environment.unsavedChangesWarningMessage
      ).subscribe(res => {
        if(res) {
          this.publishService.syncCompleted();
          onSuccess();
        }
      });
    } else {
      onSuccess();
    }
  }

  public getTitle() {
    if(this.ghostUserDetails) {
      let name = '';
      if (this.ghostUserDetails.LastName) {
        name = this.ghostUserDetails.LastName + ', ';
      }
      name += this.ghostUserDetails.FirstName;
      return name;
    }
    return '';
  }

  public openPendingActionGhostingDialog() {
    if(!this.isGhostingEnabled) {
      this.ghostUserDetails = null;
        this.alertify.openPendingActionGhosting().subscribe(res => {
          if(res.res) {
            this.isGhostingEnabled = true;
            this.ghostUserDetails = res.user;
            this.localst.store(environment.ghostUserDetails, this.ghostUserDetails);
            this.pendingActionGhosting.ghostUser$.next(res.user.UserID);
          }
        });
    } else {
      this.isGhostingEnabled = false;
      this.ghostUserDetails = null;
      this.localst.store(environment.ghostUserDetails, this.ghostUserDetails);
      this.pendingActionGhosting.ghostUser$.next(0);
    }
  }
}
