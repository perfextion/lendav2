import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';

import { LayoutModel } from '@lenda/models/layout.model';
/**
 * Shared service for layout items - header, footer, sidebar
 */
@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  private isExpanded: BehaviorSubject<boolean> = new BehaviorSubject(true);
  private isRightbarExpanded: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private behaviorSubject: BehaviorSubject<LayoutModel> = new BehaviorSubject(null);

  public isSidebarExpanded(): Observable<boolean> {
    return this.isExpanded.asObservable();
  }

  public isRightSidebarExpanded(): Observable<boolean> {
    return this.isRightbarExpanded.asObservable();
  }

  public toggleSidebar(newValue: boolean): void {
    this.isExpanded.next(newValue);
  }

  public toggleRightSidebar(newValue: boolean): void {
    this.isRightbarExpanded.next(newValue);
  }

  public receive(): Observable<LayoutModel> {
    return this.behaviorSubject.asObservable();
  }

  public notify(layout: LayoutModel): void {
    this.behaviorSubject.next(layout);
  }
}
