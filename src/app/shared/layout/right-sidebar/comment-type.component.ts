import { RefOfficeList } from './../../../models/ref-data-model';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment';
import { Loantype_dsctext, Loan_List_Comment } from '@lenda/models/loanmodel';
import { Page } from '@lenda/models/page.enum';

@Component({
  selector: 'comment-type',
  templateUrl: './comment-type.component.html'
})
export class CommentTypeComponent implements OnInit {
  @Input() comment;
  @Input() loginUserId;
  @Input() isCommitteeMember: boolean;

  loanObject: any;
  // commentAuthor: string = '';
  loanFullID: string = '';
  borrowerName: string = '';
  loanTypeAndDistCode: string = '';
  loanCropYear: string = '';
  loanOffice: any;
  loanOfficeName: string = '';

  @Output()
  onReadCommentTypeClick: EventEmitter<any> = new EventEmitter();

  constructor(private localstorageservice: LocalStorageService, private sessionservice: SessionStorageService,) {}

  ngOnInit() {
    let currentPage = this.localstorageservice.retrieve(
      environment.currentpage
    );

    if (currentPage == Page.loanlist) {
      let comment = this.comment as Loan_List_Comment;
      this.loanFullID = comment.Loan_Full_ID;
      this.loanCropYear = comment.Crop_Year.toString();
      this.borrowerName = comment.Borrower_Name.trim();
      this.loanTypeAndDistCode = this.getTypeAndDistCode(
        comment.Loan_Type_Code
      );

    } else {
      this.loanObject = this.localstorageservice.retrieve(environment.loankey);
      this.loanFullID = this.loanObject.LoanMaster.Loan_Full_ID;
      this.loanCropYear = this.loanObject.LoanMaster.Crop_Year;
      this.borrowerName = `${this.loanObject.LoanMaster.Borrower_First_Name} ${
        this.loanObject.LoanMaster.Borrower_Last_Name
      }`.trim();
      this.loanTypeAndDistCode = this.getTypeAndDistCode(
        this.loanObject.LoanMaster.Loan_Type_Code
      );
      this.loanOffice = _.find(this.localstorageservice.retrieve(environment.referencedatakey).OfficeList, (el) => {
        return el.Office_ID == this.loanObject.LoanMaster.Office_ID; });
      if (this.loanOffice) {
        this.loanOfficeName = `${this.loanOffice.City}, ${this.loanOffice.State}`;
      }
    }
  }

  getTypeAndDistCode(loanTypeCode: number) {
    return `${
      Loantype_dsctext(loanTypeCode) == 'Ag-Input'
        ? Loantype_dsctext(loanTypeCode) + ' (CPS)'
        : Loantype_dsctext(loanTypeCode)
    }`;
  }

  onRead() {
    this.onReadCommentTypeClick.emit({
      Loan_Comment_ID: this.comment.Loan_Comment_ID,
      User_ID: this.loginUserId,
      Loan_Full_ID: this.comment.Loan_Full_ID
    });
  }
}
