import { ListSettings } from '@lenda/preferences/models/loan-list/list/list.model';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../layout.service';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';

import { loan_model, Loan_Comments } from '@lenda/models/loanmodel';
import { Page } from '@lenda/models/page.enum';
import { DataService } from '@lenda/services/data.service';
import { CommitteStatusIconPipe } from '@lenda/pipes/committe-status-icon.pipe';

@Component({
  selector: 'app-right-sidebar',
  templateUrl: './right-sidebar.component.html',
  styleUrls: ['./right-sidebar.component.scss'],
})
export class RightSidebarComponent implements OnInit {
  public isRightbarExpanded: boolean = false;
  subscription: Subscription;
  private localloanobject: loan_model = new loan_model();

  public comments: Array<Loan_Comments> = [];
  public dbComments: Array<Loan_Comments> = [];
  public loginUserId: number;
  public listSettings: ListSettings;

  isCommitteeMember: boolean;

  constructor(
    private layoutService: LayoutService,
    public localstorageservice: LocalStorageService,
    private committeeStatusIcon: CommitteStatusIconPipe,
    private loanservice: LoanApiService,
    public loanserviceworker: LoancalculationWorker,
    private dataservice: DataService
  ) {}

  ngOnInit() {
    this.loginUserId = this.localstorageservice.retrieve(environment.uid);

    this.layoutService.receive().subscribe(layoutModel => {
      if (layoutModel == null) {
        this.comments = [];
        this.dbComments = [];
        return;
      }
      if (layoutModel.isFilter) {
        if (layoutModel.watchListInd != undefined) {
          this.filterByWatchList(layoutModel.watchListInd);
        } else {
          this.filterComments(layoutModel.id, layoutModel.commentReadType);
        }
      } else {
        this.initComments();
      }
    });

    this.layoutService.isRightSidebarExpanded().subscribe(value => {
      this.isRightbarExpanded = value;
      if (this.isRightbarExpanded) {
        this.loginUserId = this.localstorageservice.retrieve(environment.uid);
        this.initComments();
      }
    });
  }

  getComments() {
    this.loginUserId = this.localstorageservice.retrieve(environment.uid);
    this.localloanobject = this.localstorageservice.retrieve(
      environment.loankey
    );
    if (this.localloanobject != null) {
      if (this.localloanobject.LoanCommittees) {
        this.dbComments = this.localloanobject.LoanComments;
        this.comments = this.localloanobject.LoanComments;
      } else {
        this.comments = [];
        this.dbComments = [];
      }
    }
  }

  private initComments() {
    if (this.currentPage != Page.loanlist) {
      this.localloanobject = this.localstorageservice.retrieve(
        environment.loankey
      );
      this.initLoanComments();

      this.dataservice.getLoanObject().subscribe(res => {
        this.localloanobject = res;
        this.initLoanComments();
      });
    } else {
      this.initLoanListComments();
    }
  }

  private initLoanComments() {
    if (this.localloanobject != null) {
      if (this.localloanobject.LoanCommittees) {
        this.isCommitteeMember = this.localloanobject.LoanCommittees.some(
          a =>
            a.User_ID == this.loginUserId &&
            a.CM_Role == 1 &&
            a.Status == 1
        );

        this.dbComments = this.localloanobject.LoanComments;
        this.comments = this.localloanobject.LoanComments;
      } else {
        this.comments = [];
        this.dbComments = [];
      }
    } else {
      this.comments = [];
      this.dbComments = [];
    }
  }

  private initLoanListComments() {
    this.loanservice.loanListComments(this.loginUserId).subscribe(res => {
      if (res.ResCode == 1) {
        this.dbComments = res.Data;
        this.comments = res.Data;
      }
    });
  }

  filterByWatchList(watchListind: number) {
    this.comments = this.dbComments.filter((c: any) => {
      return c.Watch_List_Ind == watchListind;
    });
  }

  filterComments(commentTypeLevelCode, commentReadType) {
    if (commentTypeLevelCode == -1 && commentReadType == -1) {
      this.comments = this.dbComments;
    } else if (commentTypeLevelCode != -1 && commentReadType != -1) {
      let isRead = 1;
      if (commentReadType <= 0) {
        isRead = 0;
      }
      this.comments = this.dbComments.filter(
        c => c.Comment_Type_Code == commentTypeLevelCode && c.IsRead == isRead
      );
    } else if (commentTypeLevelCode == -1 && commentReadType != -1) {
      let isRead = 1;
      if (commentReadType <= 0) {
        isRead = 0;
      }
      this.comments = this.dbComments.filter(c => c.IsRead == isRead);
    } else if (commentTypeLevelCode != -1 && commentReadType == -1) {
      this.comments = this.dbComments.filter(
        c => c.Comment_Type_Code == commentTypeLevelCode
      );
    }
  }

  onRead($event) {
    if (this.currentPage != Page.loanlist) {
      this.loanservice.readComment($event).subscribe(res => {
        if (res.ResCode == 1) {
          this.localloanobject.LoanComments = res.Data.LoanComments;
          this.localloanobject.User_Pending_Actions = res.Data.User_Pending_Actions;

          // Save to Local Storage
          this.dataservice.setLoanObject(this.localloanobject);

          this.dbComments = this.localloanobject.LoanComments;
          this.comments = this.localloanobject.LoanComments;
        } else {
        }
      });
    }
  }

  private get currentPage(): Page {
    return this.localstorageservice.retrieve(environment.currentpage);
  }
}
