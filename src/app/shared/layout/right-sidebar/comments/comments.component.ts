import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import {
  loan_model,
  Loan_Comments,
  LoanGroup,
  Loan_Committee,
  Add_Loan_Comments,
  LoanStatus
} from '@lenda/models/loanmodel';
import { Page } from '@lenda/models/page.enum';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { environment } from '@env/environment.prod';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import * as moment from 'moment';
import { LayoutService } from '@lenda/shared/layout/layout.service';
import * as _ from 'lodash';

import {
  CommentTypeMock,
  CommentReadTypeMock
} from '@lenda/models/comment-type/mock-comment-type';
import { LayoutModel } from '@lenda/models/layout.model';
import { DataService } from '@lenda/services/data.service';
import { MembersService } from '@lenda/components/committee/members/members.service';
import { CommentTypeEnum, CommentType } from '@lenda/models/comment-type/comment-type.model';
import { JsonConvert } from 'json2typescript';
import { CommitteeStatusMock } from '@lenda/models/committee-status/mock-committee-status';
import { forkJoin } from 'rxjs';
import { RefDataModel, LoanMaster, Ref_Comment_Type } from '@lenda/models/ref-data-model';
import { MatCheckboxChange } from '@angular/material';
import { UpdateWatchListIndModel } from '@lenda/models/resmodels/loan-list.model';
import { AddPendingActionModel, EmailAndNotificationModel, EmailTemplateType } from '@lenda/models/pending-action/add-pending-action.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  private localloanobject: loan_model = new loan_model();

  public comments: Array<Loan_Comments> = [];

  userForm: FormGroup;

  // btnComment: string = 'add';
  commentTypeList: CommentType[];

  commentTypeIndex: number;
  commentType: CommentType;

  filterCommentTypeIndex: any;
  filterCommentType: any;

  commentReadIndex: any;
  commentReadType: any;

  isWatchListOne: boolean = false;
  isHidden: boolean = true;

  isOnLoanList: boolean = false;
  isCMRoleValid: boolean = false;
  hasUserVoted: boolean = false;
  isVoteApproved: number = 0;
  isUserOnCommitteeList: boolean = false;

  loanGroups: Array<LoanGroup>;
  isCrossCollaterized: boolean;
  isCrossCollaterizedLoan: boolean;

  loggedInUserId: number;

  selected: string;

  // add comment
  crossColLoans: string[] = [];

  actionType: string = '';
  selectedType: any;
  selectedInnerType: any;
  currenUserID: any;
  isOnWatchList: number = 0;

  isLoanStatusValid: boolean = false;

  refData: RefDataModel;

  currentPage: Page;

  constructor(
    private layoutService: LayoutService,
    private formBuilder: FormBuilder,
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public loanAPI: LoanApiService,
    private toastar: ToastrService,
    private dataService: DataService,
    private location: Location,
    private memberService: MembersService,
    public alertify: AlertifyService,
    private sessionservice: SessionStorageService
  ) {
    this.userForm = this.formBuilder.group({
      Comment: [''],
      CrossCol: [false]
    });

    this.currenUserID = this.localstorageservice.retrieve(environment.uid);
    this.refData = this.localstorageservice.retrieve(environment.referencedatakey);
  }

  checkUserIfVoted() {
    // let currentUserID = this.localstorageservice.retrieve(environment.uid);
    let currentSelectedLoan = this.localstorageservice.retrieve(
      environment.loankey
    );

    if (!_.isEmpty(currentSelectedLoan)) {
      this.isUserOnCommitteeList = this.localloanobject.LoanCommittees.some(
        c => c.CM_Role == 1 && c.User_ID == this.loggedInUserId
      );
      let loanStatus = currentSelectedLoan.LoanMaster.Loan_Status;
      this.isOnWatchList = currentSelectedLoan.LoanMaster.Watch_List_ind;

      if (loanStatus == 'R' || loanStatus == 'A' || loanStatus == 'Z') {
        this.isLoanStatusValid = true;
      }

      let committeeUser: Loan_Committee = _.find(
        this.localstorageservice.retrieve(environment.loankey)[
          'LoanCommittees'
        ],
        el => {
          return el.User_ID == this.currenUserID;
        }
      );

      // this.isUserOnCommitteeList = committeeUser ? true : false;
      if (committeeUser) {
        this.isCMRoleValid = committeeUser.CM_Role == 1;
        this.hasUserVoted = !!committeeUser.Voted_Date;
        this.isVoteApproved = committeeUser.Vote;
      }
    }
  }

  allowedToVote() {
    return (
      this.isCMRoleValid && this.isUserOnCommitteeList && this.isLoanStatusValid
    );
  }

  ngOnInit() {
    this.currentPage = this.localstorageservice.retrieve(
      environment.currentpage
    );

    this.localstorageservice
      .observe(environment.currentpage)
      .subscribe(page => {
        this.currentPage = page;
      });

    this.loggedInUserId = this.localstorageservice.retrieve(environment.uid);

    if (this.currentPage != Page.loanlist) {
      this.localloanobject = this.localstorageservice.retrieve(
        environment.loankey
      );

      this.setInitialDataForLoan();
    }

    this.dataService.getLoanObject().subscribe(res => {
      this.localloanobject = res;
      this.setInitialDataForLoan();
      this.checkUserIfVoted();
    });

    this.commentTypeList = CommentTypeMock.filter(t => t.id != -1);
    this.commentType = this.commentTypeList.find(c => c.order == 0);
    this.commentTypeIndex = this.commentType.order;

    this.filterCommentType = CommentTypeMock[0];
    this.filterCommentTypeIndex = 0;

    this.commentReadType = CommentReadTypeMock[0];
    this.commentReadIndex = 0;

    this.layoutService.isRightSidebarExpanded().subscribe(value => {
      if (!value) {
        this.resetCommentBox();
        this.checkUserIfVoted();
      } else {
        const orderIndex = 0;
        this.commentTypeIndex = orderIndex;
        this.commentType = this.commentTypeList.find(a => a.order == orderIndex);
      }
    });

    this.setCrossCollateralStatus();
    this.setCommitteeCommentType();
  }

  private setInitialDataForLoan() {
    if (this.localloanobject != null) {
      if (this.localloanobject.LoanComments != null) {
        this.comments = this.localloanobject.LoanComments;
      } else {
        this.comments = [];
      }
    } else {
      this.comments = [];
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onAddComment(type) {
    this.checkUserIfVoted();
    this.isOnLoanList = _.isEqual(this.location.path(), '/home/loans');
    if (type == 'add') {
      this.isHidden = false;
    } else {
      this.resetCommentBox();
    }
  }

  get isLoanInVotingCondition() {
    try {
      if (this.localloanobject) {
        let master: LoanMaster = this.localloanobject.LoanMaster;
        return (
          master.Loan_Status == LoanStatus.Working ||
          master.Loan_Status == LoanStatus.Recommended ||
          master.Loan_Status == LoanStatus.Approved ||
          master.Loan_Status == LoanStatus.Uploaded
        );
      }
      return false;
    } catch {
      return false;
    }
  }

  resetCommentBox() {
    this.isOnLoanList = _.isEqual(this.location.path(), '/home/loans');
    this.isHidden = true;
    this.userForm.reset();
  }

  getFromNow(date) {
    return moment(date).fromNow();
  }

  sendComment() {
    this.doAddComment();
  }

  doAddComment() {
    let comment = this.userForm.value.Comment;

    if (!comment || comment.replace(/\s/g, '') == '') {
      this.toastar.error('Please enter a comment');
      return;
    }

    let refCommentType = this.refData.Ref_Comment_Types.find(
      a => a.Comment_Type_Code == this.commentType.code
    );

    if(refCommentType && this.commentType) {
      this.validateAndAddComment(comment, refCommentType);
    } else {
      this.toastar.warning('Invalid Comment Type');
    }
  }

  private validateAndAddComment(comment: string, refCommentType: Ref_Comment_Type) {
    const lc = new Add_Loan_Comments();
    lc.Comment_Text = comment;
    lc.Parent_Emoji_ID = 0;
    lc.Comment_Emoji_ID = 0;
    lc.Comment_Read_Ind = refCommentType.Read_Required_Ind;
    lc.Comment_Type_Code = this.commentType.id;
    lc.Loan_Full_ID = this.localloanobject.Loan_Full_ID;
    lc.User_ID = this.loggedInUserId;

    let ToUserIds = [];

    switch (lc.Comment_Type_Code) {
      case CommentTypeEnum.WatchList:
        if (this.localloanobject.LoanMaster.Watch_List_Ind == 1) {
          lc.Parent_Emoji_ID = 0;
        } else {
          lc.Parent_Emoji_ID = 1;
        }

        this.updateWatchListInd(lc);

        break;

      default:
        try {
          ToUserIds = _.orderBy(
            this.localloanobject.LoanCommittees,
            (c: Loan_Committee) => c.CM_ID
          ).map(a => a.User_ID);

          //
        } catch {
          ToUserIds = [];
        }

        try {
          if(this.localloanobject.LoanCommittees.length == 0) {
            ToUserIds = [
              this.localloanobject.LoanMaster.Loan_Officer_ID
            ];
          }
        } catch {
          ToUserIds = [];
        }

        this.tryAddComment(lc, refCommentType, ToUserIds);

        break;
    }
  }

  private tryAddComment(
    lc: Add_Loan_Comments,
    refCommentType: Ref_Comment_Type,
    ToUserIds: Array<any>
  ) {
    const getLatestLoanAndSentNotification = (loanCommentId: number) => {
      this.getLatestLoan();

      if(refCommentType.Notification_Ind) {
        this.sendEmail(loanCommentId, refCommentType);
      }
    };

    const addLoanCommentToDBSuccess = (loanCommentId: number) => {
      if(refCommentType.Pending_Action_Code) {
        this.addPendingAction(ToUserIds, refCommentType.Pending_Action_Code, () => {
          getLatestLoanAndSentNotification(loanCommentId);
        });
      } else {
        getLatestLoanAndSentNotification(loanCommentId);
      }
    }

    this.addLoanCommentToDB(lc, (loanCommentId: number) => {
      addLoanCommentToDBSuccess(loanCommentId);
    });
  }

  private addPendingAction(ToUserIds: Array<any>, PA_Code: string, onSuccess: () => void) {
    let refPA = this.refData.RefPendingActions.find(a => a.PA_Group == PA_Code);

    if(refPA) {
      let conf_PA = <AddPendingActionModel>{
        FromUserId: this.loggedInUserId,
        ToUserIds: ToUserIds,
        LoanFullId: this.localloanobject.Loan_Full_ID,
        PA_Group: refPA.PA_Group,
        Ref_PA_Id: refPA.Ref_PA_ID
      };

      this.loanAPI.addPendingAction(conf_PA).subscribe(res => {
        if (res.ResCode == 1) {
          onSuccess();
        }
      });
    } else {
      this.toastar.warning('Invalid Pending Action Code');
    }
  }

  private sendEmail(loanCommentId: number, refCommentType: Ref_Comment_Type) {
    const body = <EmailAndNotificationModel>{
      LoanFullID: this.localloanobject.Loan_Full_ID,
      EmailTemplateType: EmailTemplateType.CommitteeComment,
      EmailData: {
        Loan_Comment_ID: loanCommentId,
        Comment_Type_Code: refCommentType.Comment_Type_Code,
        Comment_Type_Name: refCommentType.Comment_Type_Name
      }
    };

    this.loanAPI.sendEmailAndNotification(body).subscribe(res => {
      if(res.ResCode == 1) {
        this.toastar.success('Notification sent.');
      } else {
        this.toastar.error('Error while sending notification');
      }
    },
    () => {
      this.toastar.error('Error while sending notification');
    });
  }

  private addLoanCommentToDB(
    lc: Add_Loan_Comments,
    onSuccess: (loanCommentId: number) => void
  ) {
    this.loanAPI.addComment(lc).subscribe(res => {
      if (res.ResCode == 1) {
        this.isHidden = true;
        this.toastar.success('Successfully recorded comment');

        let layoutModel = new LayoutModel();
        layoutModel.isFilter = false;
        this.layoutService.notify(layoutModel);
        onSuccess(res.Data);
        this.userForm.reset();
        this.setCommitteeCommentType();
      } else {
        this.toastar.error('Error while adding comment');
      }
    },
    () => {
      this.toastar.error('Error while adding comment');
    });
  }

  private updateWatchListInd(lc: Add_Loan_Comments) {
    let isUserRiskAdmin = false;

      let user = this.sessionservice.retrieve('UID');
      if (!_.isEmpty(user)) {
        isUserRiskAdmin = user.RiskAdmin == 1 ? true : false;
      }

      this.isOnWatchList = this.localloanobject.LoanMaster.Watch_List_Ind;

      let watchListModel = new UpdateWatchListIndModel();
      watchListModel.LoanFullId = lc.Loan_Full_ID;
      watchListModel.WatchListInd = this.isOnWatchList == 1 && isUserRiskAdmin ? 0 : 1;
      watchListModel.Comment = lc.Comment_Text;
      watchListModel.System_Comment = this.isOnWatchList == 1 ? 'On Watchlist' : 'Not on Watchlist';

      this.loanAPI.updateWatchListInd(watchListModel)
        .subscribe(res => {
          if (res.ResCode != 0) {
            this.isHidden = true;
            if (watchListModel.WatchListInd == 1) {
              this.toastar.success('Successfully added to watch list');
            } else {
              this.toastar.success('Successfully removed to watch list');
            }

            this.getLatestLoan();

            let layoutModel = new LayoutModel();
            layoutModel.isFilter = false;
            this.layoutService.notify(layoutModel);

            this.userForm.reset();
            this.setCommitteeCommentType();
          } else {
            this.toastar.error(
              res.Message || 'Error while adding to watch list'
            );
          }
        },
        () => {
          this.toastar.error('Error while adding/removing to/from watch list');
        });
  }

  onFilterByWatchList() {
    let currentPage = this.localstorageservice.retrieve(
      environment.currentpage
    );

    this.isWatchListOne = !this.isWatchListOne;
    let layoutModel = new LayoutModel();

    if (currentPage == Page.loanlist) {
      layoutModel.isFilter = true;
      layoutModel.watchListInd = this.isWatchListOne ? 1 : 0;

      this.layoutService.notify(layoutModel);
    } else {
      layoutModel.isFilter = true;
      layoutModel.id = this.isWatchListOne ? 5 : -1;
      layoutModel.commentReadType = -1;

      this.layoutService.notify(layoutModel);
    }
  }

  onCommentTypeChange() {
    let orderIndex = this.commentTypeIndex;
    if (orderIndex >= 4) {
      orderIndex = 0;
    } else {
      orderIndex = orderIndex + 1;
    }
    this.commentTypeIndex = orderIndex;
    this.commentType = this.commentTypeList.find(a => a.order == orderIndex);
  }

  onFilterCommentTypeChange() {
    this.isWatchListOne = false;
    let id = this.filterCommentTypeIndex;
    if (id >= 4) {
      id = 0;
    } else {
      id = id + 1;
    }
    this.filterCommentTypeIndex = id;
    this.filterCommentType = CommentTypeMock[id];
    let layoutModel = new LayoutModel();
    layoutModel.isFilter = true;
    layoutModel.id = this.filterCommentType.id;
    layoutModel.commentReadType = this.commentReadType.id;
    this.layoutService.notify(layoutModel);
  }

  onCommentReadTypeChange() {
    let id = this.commentReadIndex;
    if (id >= 2) {
      id = 0;
    } else {
      id = id + 1;
    }
    this.commentReadIndex = id;
    this.commentReadType = CommentReadTypeMock[id];

    let layoutModel = new LayoutModel();
    layoutModel.isFilter = true;
    layoutModel.id = this.filterCommentType.id;
    layoutModel.commentReadType = this.commentReadType.id;
    this.layoutService.notify(layoutModel);
  }

  crossColChange($event: MatCheckboxChange) {
    this.isCrossCollaterized = $event.source.checked;
  }

  setCommitteeCommentType() {
    this.commentType = CommentTypeMock.find(
      c => c.id == CommentTypeEnum.CommitteeComment
    );

    this.commentTypeIndex = this.commentType.order;
  }

  public openEmoji() {
    if (this.isCMRoleValid && !this.hasUserVoted) {
      this.alertify.openEmojiDialog('Vote by Emoji').subscribe(res => {
        if (res) {
          this.recordVoteAPI(res.committeeStatusId, res.innerCommitteeStatusId);
        }
      });
    }
  }

  public rescindVoteAPI() {
    try {
      this.setCommitteeCommentType();

      let cmId = this.localloanobject.LoanCommittees.find(
        c => c.CM_Role == 1 && c.User_ID == this.loggedInUserId
      ).CM_ID;

      let crossColLoans: Array<string> = this.getCrossColLoanIds(
        this.isCrossCollaterized
      );
      let committeeStatus = CommitteeStatusMock.find(s => s.id == 5);

      let innerCommitteeStatus = committeeStatus.innerStatus.find(
        i => i.id == 0
      );

      let recindVotes = crossColLoans.map(loan_Id => {
        return this.memberService.rescindVote(
          loan_Id,
          cmId,
          this.currenUserID,
          this.loggedInUserId,
          '',
          innerCommitteeStatus.title
        );
      });

      forkJoin(recindVotes).subscribe(
        res => {
          if (res[res.length - 1].ResCode == 1) {
            this.userForm.reset();
            // this.isHidden = true;
            this.toastar.success('Successfully Rescinded Vote.');
            this.getLatestLoan();
          } else {
            this.toastar.error('Error occured while rescinding voting');
          }
        },
        error => {
          this.toastar.error('Error occured while rescinding voting');
        }
      );
    } catch {
      this.toastar.error('Error occured while rescinding voting');
    }
  }

  public recordVoteAPI(parentEmoji: number, commentEmoji: number) {
    if (!this.isCMRoleValid || this.hasUserVoted) {
      return;
    }

    try {
      this.setCommitteeCommentType();

      let committeeStatus = CommitteeStatusMock.find(s => s.id == parentEmoji);

      let innerCommitteeStatus = committeeStatus.innerStatus.find(
        i => i.id == commentEmoji
      );

      let crossColLoans: Array<string> = this.getCrossColLoanIds(
        this.isCrossCollaterized
      );

      let voteLoans = crossColLoans.map(loan_Id => {
        return this.memberService.voteLoan(
          loan_Id,
          this.loggedInUserId,
          parentEmoji,
          commentEmoji,
          '',
          innerCommitteeStatus.title
        );
      });

      forkJoin(voteLoans).subscribe(
        res => {
          if (res[res.length - 1].ResCode == 1) {
            this.userForm.reset();
            // this.isHidden = true;
            this.toastar.success('Successfully Recorded Vote');
            this.getLatestLoan();
          } else {
            this.toastar.error('Error occured while recording voting');
          }
        },
        error => {
          this.toastar.error('Error occured while recording voting');
        }
      );
    } catch {
      this.toastar.error('Error occured while recording voting');
    }
  }

  private getLatestLoan() {
    this.loanAPI
      .getLoanById(this.localloanobject.Loan_Full_ID)
      .subscribe(res => {
        if (res.ResCode == 1) {
          this.toastar.success('Loan synced successfully');
          let jsonConvert: JsonConvert = new JsonConvert();
          this.loanserviceworker.performcalculationonloanobject(
            jsonConvert.deserialize(res.Data, loan_model)
          );
          this.loanserviceworker.saveValidationsToLocalStorage(res.Data);
        } else {
          this.toastar.error('Could not fetch latest Loan from API');
        }
      });
  }

  private getCrossColLoanIds(isCross: boolean) {
    let crossColLoans: Array<string> = [];

    if (isCross && this.loanGroups) {
      crossColLoans = this.loanGroups
        .filter(a => a.IsCrossCollateralized)
        .map(a => a.Loan_Full_ID);
    }

    crossColLoans.push(this.localloanobject.Loan_Full_ID);

    return crossColLoans;
  }

  private setCrossCollateralStatus() {
    this.loanGroups = this.localstorageservice.retrieve(environment.loanGroup);
    // Need to observe as otherwise sometimes loangroup is not available
    this.localstorageservice.observe(environment.loanGroup).subscribe(res => {
      this.loanGroups = res;
      if (this.loanGroups) {
        this.isCrossCollaterizedLoan = this.loanGroups.some(
          lg => lg.IsCrossCollateralized
        );
      } else {
        this.isCrossCollaterizedLoan = false;
      }
    });

    if (this.loanGroups) {
      this.isCrossCollaterizedLoan = this.loanGroups.some(
        lg => lg.IsCrossCollateralized
      );
    } else {
      this.isCrossCollaterizedLoan = false;
    }
  }

  lineBreak(event) {
    if ((event.ctrlKey || event.metaKey) && (event.keyCode == 13 || event.keyCode == 10)) {
      let cursorPosition = event.currentTarget.selectionStart;
    event.currentTarget.value =
      event.currentTarget.value.substr(0, cursorPosition) +
      '\n--' +
      event.currentTarget.value.substr(cursorPosition);

      this.userForm.controls['Comment'].setValue(event.currentTarget.value);
  }

  }
}
