import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { CommitteeStatusMock } from '@lenda/models/committee-status/mock-committee-status';
@Component({
  selector: 'app-media-article',
  templateUrl: './media-article.component.html',
  styleUrls: ['./media-article.component.scss']
})
export class MediaArticleComponent implements OnInit {
  @Input() title;
  @Input() text;
  @Input() systemText: string;
  @Input() date;
  @Input() isRead;
  @Input() icon = '';
  @Input() pIcon;
  @Input() cIcon;
  @Input() loanFD: string = '';
  @Input() bName: string = '';
  @Input() loanTDC: string = '';
  @Input() lcYear: string = '';
  @Input() loffice: string = '';
  @Input() commentType: any;

  isMaterial: boolean;
  isEmoji: boolean;
  isFlaticon: boolean;
  emojiIcon: string;

  onReadClick = new EventEmitter();

  constructor() {}

  ngOnInit() {
    this.getParentEmoji();
  }

  public onRead() {
    this.onReadClick.emit();
  }

  getEmojiText() {
    try {
      if (this.pIcon == 1 && this.cIcon != 0) {
        return 'approved;';
      }

      if (this.pIcon == 2 && this.cIcon != 0) {
        return 'declined;';
      }

      if (this.pIcon == 3) {
        return 'unsure;';
      }

      if (this.pIcon == 4) {
        return 'recommended;';
      }
    } catch {
      return '';
    }
  }

  getParentEmoji() {
    try {
      let emoji = CommitteeStatusMock.find(s => s.id == +this.pIcon);
      if (emoji.parentIcon == 'material') {
        this.isMaterial = true;
      }

      if (emoji.parentIcon == 'em') {
        this.isEmoji = true;
      }

      if (emoji.parentIcon == 'flaticon') {
        this.isFlaticon = true;
      }
      this.emojiIcon = emoji.icon;
    } catch (error) {
      this.emojiIcon = '';
    }
  }
}
