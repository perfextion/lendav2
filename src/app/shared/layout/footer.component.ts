import { Component, OnInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';
import { versions } from '@lenda/versions';

import { ReferenceService } from '@lenda/services/reference/reference.service';

@Component({
  selector: 'layout-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public apiurl = environment.apiUrl;
  public git = versions.revision;
  public databaseName: string = '';

  constructor(
    public localst: LocalStorageService,
    public referenceService: ReferenceService
  ) {}

  ngOnInit() {
    //this.getReferenceData(); why when we already have on header
  }

  getReferenceData() {
    this.referenceService.getreferencedata().subscribe(res => {
      this.localst.store(environment.referencedatakey, res.Data);
      this.databaseName = res.Data.Databasename;
    });
  }
}
