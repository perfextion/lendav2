import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material';

import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';
import { loan_model } from '@lenda/models/loanmodel';
import { Page } from '@lenda/models/page.enum';
import { Preferences } from '@lenda/preferences/models/index.model';
import { ShowTotalsSettings } from '@lenda/preferences/models/user-settings/show-totals.model';

import { ValidationErrorsCountService } from '@lenda/Workers/calculations/validationerrorscount.service';
import { PageInfoService } from '@lenda/services/page-info.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { PublishService, Sync } from '@lenda/services/publish.service';
import { LayoutService } from './layout.service';
import { DataService } from '@lenda/services/data.service';

/**
 * @title Autosize sidenav
 */
@Component({
  selector: 'layout-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
  public isExpanded = true;
  public isAdmin = false;

  @ViewChild('leftSidenav')
  public sideNav: MatSidenav;

  public loanid: string = '';
  public syncItems: Sync[];
  public showTotalsSettings: ShowTotalsSettings;

  public isARM = true;
  private localloanobject: loan_model;

  private subscription: ISubscription;
  private isSidebarExpandedSub: ISubscription;
  private listenToSyncRequiredSub: ISubscription;
  private loanKeySub: ISubscription;
  private preferencesChangeSub: ISubscription;

  constructor(
    private router: Router,
    private layoutService: LayoutService,
    private localstorage: LocalStorageService,
    private sessionservice: SessionStorageService,
    private publishService: PublishService,
    private dataService: DataService,
    private validationErrorCountService: ValidationErrorsCountService,
    private pageInfoService: PageInfoService,
    private alertify: AlertifyService,
    private settingsService: SettingsService,
    private logging: LoggingService
  ) {
    this.localloanobject = this.localstorage.retrieve(environment.loankey);

    this.subscription = this.dataService.getLoanObject().subscribe((res: loan_model) => {
      this.localloanobject = res;
      if (res != undefined && res != null)
        this.loanid = this.localstorage.retrieve(environment.loanidkey).replace("-", "/");
    })

    if (!_.isEmpty(this.sessionservice.retrieve("UID"))) {
      this.isAdmin = this.sessionservice.retrieve("UID").IsAdmin == 1 ? true : false;
    }

    this.getloanid();
  }

  ngOnInit() {
    this.isSidebarExpandedSub = this.layoutService.isSidebarExpanded().subscribe((value) => {
      this.isExpanded = value;
    });

    // Start subscribing if sync is required
    this.listenToSyncRequiredSub = this.publishService.listenToSyncRequired().subscribe((syncItems) => {
      this.syncItems = syncItems;
    });

    this.loanKeySub = this.localstorage.observe(environment.loanidkey).subscribe(_ => {
      this.getloanid();
    });

    // Check for changes in preferences
    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe((preferences: Preferences) => {

      this.showTotalsSettings = preferences.userSettings.showTotalsSettings;
    });
    // Get initial settings
    this.showTotalsSettings = this.settingsService.preferences.userSettings.showTotalsSettings;
  }

  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.loanKeySub) {
      this.loanKeySub.unsubscribe();
    }

    if(this.isSidebarExpandedSub) {
      this.isSidebarExpandedSub.unsubscribe();
    }

    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }

    if(this.listenToSyncRequiredSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  public isSyncRequired(pageName: string) {
    let item: Sync;
    for (item of this.syncItems) {
      if (item.page === pageName) {
        return true;
      }
    }
    return false;
  }

  public checkLoanSync() {
    let hasValidations = this.validationErrorCountService.hasLevel2Validations();

    if (hasValidations) {
      this.alertify.alert(
        'Alert',
        'Please resolve all Level 2 Validations before Sync'
      );
      return false;
    } else {
      return true;
    }
  }

  public onPublish() {
    this.checkUpdateBudgetInd();
  }

  private checkUpdateBudgetInd() {
    if(this.localloanobject.Update_Budget_Ind) {
      this.alertify.confirm(
        'Confirm',
        'Warning: You have changed default budget data. Do you want to update your defaults?'
      ).subscribe(res => {
        if(res) {
          this.localloanobject.Update_Budget_Ind = 1;
        } else {
          this.localloanobject.Update_Budget_Ind = 0;
        }
        this.publishService.syncCompleted();
        this.publishService.syncToDb();
      });
    } else {
      this.localloanobject.Update_Budget_Ind = 0;
      this.publishService.syncCompleted();
      this.publishService.syncToDb();
    }
  }

  gotoborrower(event) {
    this.router.navigateByUrl('/home/loanoverview/1/borrower');
  }

  getloanid() {
    try {
      this.loanid = this.localstorage.retrieve(environment.loanidkey).replace('-', '/');
    } catch (ex) { }
  }

  public get showIcons() {
    return this.isARM && this.isExpanded;
  }

  public get summaryErrors() {
    return this.validationErrorCountService.getValidation(Page.summary);
  }

  public get borrowerErrors() {
    return this.validationErrorCountService.getValidation(Page.borrower);
  }

  public get cropErrors() {
    return this.validationErrorCountService.getValidation(Page.crop);
  }

  public get farmErrors() {
    return this.validationErrorCountService.getValidation(Page.farm);
  }

  public get insuranceErrors() {
    return this.validationErrorCountService.getValidation(Page.insurance);
  }

  public get budgetErrors() {
    return this.validationErrorCountService.getValidation(Page.budget);
  }

  public get collateralErrors() {
    return this.validationErrorCountService.getValidation(Page.collateral);
  }

  public get optimizerErrors() {
    return this.validationErrorCountService.getValidation(Page.optimizer);
  }

  public get committeeErrors() {
    return this.validationErrorCountService.getValidation(Page.committee);
  }

  public get checkoutErrors() {
    return this.validationErrorCountService.getValidation(Page.checkout);
  }

  public get disburseErrors() {
    return this.validationErrorCountService.getValidation(Page.disburse);
  }

  public setCurrentPage(page: any) {
    let prevPage = this.localstorage.retrieve(environment.currentpage);
    this.localstorage.store(environment.currentpage, page);
    this.pageInfoService.setCurrentPage(page);
    this.logging.checkandcreatelog(0, 'Sidenav', `Change tab from - ${prevPage} to - ${page}`);
  }
}

