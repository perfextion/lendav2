import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

import { ValidationModel } from '@lenda/models/loansettings';

@Component({
  selector: 'app-exception-icons',
  templateUrl: './exception-icons.component.html',
  styleUrls: ['./exception-icons.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ExceptionIconsComponent implements OnInit {
  @Input()
  error: ValidationModel;

  @Input() isValidationsDisplayed: boolean;
  @Input() isErrorsDisplayed: boolean;

  constructor() {}

  ngOnInit() {}

  public get hasOneException() {
    return (
      (this.error.level1 == 0 && this.error.level2 > 0) ||
      (this.error.level1 > 0 && this.error.level2 == 0)
    );
  }

  public get hasNoExceptions() {
    return this.error.level1 == 0 && this.error.level2 == 0;
  }
}
