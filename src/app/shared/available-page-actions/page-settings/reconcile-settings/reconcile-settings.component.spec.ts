import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReconcileSettingsComponent } from './reconcile-settings.component';

describe('ReconcileSettingsComponent', () => {
  let component: ReconcileSettingsComponent;
  let fixture: ComponentFixture<ReconcileSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReconcileSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconcileSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
