import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

import { BorrowerSettings } from '@lenda/preferences/models/user-settings/borrower-settings.model';
import { Preferences } from '@lenda/preferences/models/index.model';

import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-borrower-settings',
  templateUrl: './borrower-settings.component.html',
  styleUrls: ['./borrower-settings.component.scss']
})
export class BorrowerSettingsComponent implements OnInit, AfterViewInit, OnDestroy {
  public borrowerSettings: BorrowerSettings = new BorrowerSettings();
  private preferencesChangeSub: ISubscription;

  constructor(private settingsService: SettingsService) {}

  ngOnInit() {
    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe(
      (preferences: Preferences) => {
        this.borrowerSettings = preferences.userSettings.borrowerSettings;
      }
    );
  }

  ngOnDestroy() {
    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  lableChange(event) {
    this.settingsService.labelChange.emit(event.checked);
  }

  ngAfterViewInit() {
    this.borrowerSettings = this.settingsService.preferences.userSettings.borrowerSettings;
  }

  onChange() {
    this.settingsService.preferencesChange.next(this.settingsService.preferences);
  }
}
