import { Component, OnInit, OnDestroy } from '@angular/core';

import { Preferences } from '@lenda/preferences/models/index.model';
import { ListSettings } from '@lenda/preferences/models/loan-list/list/list.model';
import { LoanListSettings } from '@lenda/preferences/models/loan-list/index.model';

import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { PubSubService } from '@lenda/services/pubsub.service';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-loan-list-settings',
  templateUrl: './loan-list-settings.component.html',
  styleUrls: ['./loan-list-settings.component.scss']
})
export class LoanListSettingsComponent implements OnInit, OnDestroy {
  public listSettings: ListSettings = new ListSettings();
  public loanListSettings: LoanListSettings = new LoanListSettings();

  private preferencesChangeSub: ISubscription;

  constructor(
    private settingsService: SettingsService,
    public pubsubService: PubSubService
  ) {}

  ngOnInit() {
    // Update loan list of preferences are changed
    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe(
      (preference: Preferences) => {
        this.loanListSettings = preference.loanListSettings;
        this.listSettings = preference.loanListSettings.listSettings;
      }
    );
  }

  ngOnDestroy() {
    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  /**
   * Is include reports
   * @param ev event
   */
  toggleIsIncludeMyReportsEnabledChange(ev) {
    this.pubsubService.sendEvent('event.loanlist.includereports.updated', {
      checkboxEvent: ev
    });
  }

  /**
   * Latest loan version
   */
  onIncludeAllLoanVersionChange(ev) {
    this.pubsubService.sendEvent('event.loanlist.IncludeAllVersions.updated', {
      checkboxEvent: ev
    });
  }

  /**
   * portfolio details
   * @param ev  event
   */
  togglePortfolioAnalysisSettingsChange(ev) {
    this.pubsubService.sendEvent('event.loanlist.portfolioAnalysis.updated', {
      checkboxEvent: ev
    });
  }

  /**
   * Include Active only toggled
   * @param ev event
   */
  onIncludeInactiveChanged(ev) {
    this.pubsubService.sendEvent('event.loanlist.includeInactive.updated', {
      checkboxEvent: ev
    });
  }
}
