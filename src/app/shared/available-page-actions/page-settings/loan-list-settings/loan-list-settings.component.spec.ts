import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanListSettingsComponent } from './loan-list-settings.component';

describe('LoanListSettingsComponent', () => {
  let component: LoanListSettingsComponent;
  let fixture: ComponentFixture<LoanListSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanListSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanListSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
