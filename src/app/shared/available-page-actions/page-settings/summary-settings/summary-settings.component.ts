import { Component, OnInit } from '@angular/core';

import { Loansettings } from '@lenda/models/loansettings';

import { PubSubService } from '@lenda/services/pubsub.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';

@Component({
  selector: 'app-summary-settings',
  templateUrl: './summary-settings.component.html',
  styleUrls: ['./summary-settings.component.scss']
})
export class SummarySettingsComponent implements OnInit {
  loanSetting: Loansettings = new Loansettings();
  constructor(
    private pubsubService: PubSubService,
    private settingsService: SettingsService
  ) {
    this.loanSetting = this.settingsService.userLoanSettings;
  }

  ngOnInit() {
    this.pubsubService.sendEvent('event.summary.standard.selected', {});
  }

  /**
   * Report settings value is toggled
   */
  onReportsSettingsToggled(ev) {
    ev.stopPropagation();
  }
}
