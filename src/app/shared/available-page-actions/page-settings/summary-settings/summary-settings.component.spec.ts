import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummarySettingsComponent } from './summary-settings.component';

describe('SummarySettingsComponent', () => {
  let component: SummarySettingsComponent;
  let fixture: ComponentFixture<SummarySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummarySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummarySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
