import { Component, OnInit, AfterViewInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment';

import { InsurancePlanSettings } from '@lenda/preferences/models/loan-settings/insurance-plan.model';
import {
  Loansettings,
  Insurance_Policy_Settings,
  Insurance_Policy_Tabs
} from '@lenda/models/loansettings';
import { loan_model } from '@lenda/models/loanmodel';

import { PubSubService } from '@lenda/services/pubsub.service';
import { DataService } from '@lenda/services/data.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';

@Component({
  selector: 'app-insurance-settings',
  templateUrl: './insurance-settings.component.html',
  styleUrls: ['./insurance-settings.component.scss']
})
export class InsuranceSettingsComponent implements OnInit, AfterViewInit {
  public insuranceToggles = {
    MPCI: true,
    STAX: false,
    SCO: false,
    HMAX: false,
    RAMP: false,
    ICE: false,
    SELECT: false,
    PCI: false,
    CROPHAIL: false,
    WFRP: false
  };
  public HRexclusion: boolean = false;
  public subscription: ISubscription;
  public loanmodel: loan_model = null;
  public insurancePlanSettings: InsurancePlanSettings;

  constructor(
    private pubsubService: PubSubService,
    private dataService: DataService,
    private settingsService: SettingsService,
    private localstorage: LocalStorageService
  ) {}

  ngOnInit() {
    let preferences = this.settingsService.preferences;
    // Intially set from preferences
    this.insurancePlanSettings = preferences.loanSettings.insurancePlanSettings;
    this.mapInsurancePlanSettings();

    // If loan model is availalable then set from there
    this.loanmodel = this.localstorage.retrieve(environment.loankey);
    this.mapLoanSettings();
  }

  ngAfterViewInit() {
    this.getInitialSettings();
  }

  /**
   *
   * @param ev Checked event
   * @param value value that got toggled
   */
  onInsuranceToggleUpdated(ev, value) {
    this.pubsubService.sendEvent('event.insurance.setting.updated', {
      checkboxEvent: ev,
      value: value
    });
  }

  onHRToggleUpdated(ev, value) {
    this.pubsubService.sendEvent('event.insurance.HR.updated', {
      checkboxEvent: ev,
      value: value
    });
  }
  /**
   * Gets intial settings either from loan object or from preferences
   */
  getInitialSettings() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res != null) {
        this.loanmodel = res;
        this.mapLoanSettings();
      }
    });
  }

  /**
   * Maps preferences object to insurance plan settings on insurance page
   */
  mapInsurancePlanSettings() {
    this.insuranceToggles.SELECT = this.insurancePlanSettings.isAbcEnabled;
    this.insuranceToggles.CROPHAIL = this.insurancePlanSettings.isCropHailEnabled;
    this.insuranceToggles.HMAX = this.insurancePlanSettings.isHmaxEnabled;
    this.insuranceToggles.ICE = this.insurancePlanSettings.isIceEnabled;
    this.insuranceToggles.PCI = this.insurancePlanSettings.isPciEnabled;
    this.insuranceToggles.RAMP = this.insurancePlanSettings.isRampEnabled;
    this.insuranceToggles.SCO = this.insurancePlanSettings.isScoEnabled;
    this.insuranceToggles.STAX = this.insurancePlanSettings.isStaxEnabled;
    this.HRexclusion = this.insurancePlanSettings.isHrEnabled;
  }

  /**
   * Maps loan settings to insurance plan
   */
  mapLoanSettings() {
    let objsettings = new Loansettings();
    let loanSettings = this.loanmodel.LoanMaster.Loan_Settings;

    if (loanSettings != null && loanSettings != '') {
      // if settings exist then use these
      objsettings = JSON.parse(loanSettings) as Loansettings;
    }

    if (objsettings.insurance_policy_Settings == undefined) {
      objsettings.insurance_policy_Settings = new Insurance_Policy_Settings();
    }

    if (
      Object.keys(objsettings.insurance_policy_Settings.policy_visibility_state)
        .length == 0
    ) {
      objsettings.insurance_policy_Settings.policy_visibility_state = new Insurance_Policy_Tabs();
    }

    Object.keys(
      objsettings.insurance_policy_Settings.policy_visibility_state
    ).forEach(element => {
      this.insuranceToggles[element] =
        objsettings.insurance_policy_Settings.policy_visibility_state[element];
    });
  }
}
