import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CropSettingsComponent } from './crop-settings.component';

describe('CropSettingsComponent', () => {
  let component: CropSettingsComponent;
  let fixture: ComponentFixture<CropSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CropSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CropSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
