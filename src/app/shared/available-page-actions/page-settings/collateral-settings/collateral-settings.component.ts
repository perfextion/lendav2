import { Component, OnInit } from '@angular/core';

import { SharedService } from '@lenda/services/shared.service';

@Component({
  selector: 'app-collateral-settings',
  templateUrl: './collateral-settings.component.html',
  styleUrls: ['./collateral-settings.component.scss']
})
export class CollateralSettingsComponent implements OnInit {
  constructor(private sharedService: SharedService) {}

  ngOnInit() {}

  /**
   * Loan detail extra - checked or unchecked
   * @param ev toggle event
   */
  onLoanDetailExtraChanged(ev) {
    this.sharedService.isLoanDetailExtraExpanded(ev.checked);
  }
}
