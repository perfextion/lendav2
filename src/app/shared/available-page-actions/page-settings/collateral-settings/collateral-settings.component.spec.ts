import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollateralSettingsComponent } from './collateral-settings.component';

describe('CollateralSettingsComponent', () => {
  let component: CollateralSettingsComponent;
  let fixture: ComponentFixture<CollateralSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollateralSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollateralSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
