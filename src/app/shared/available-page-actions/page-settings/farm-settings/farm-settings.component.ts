import { Component, OnInit } from '@angular/core';

import { PubSubService } from '@lenda/services/pubsub.service';

@Component({
  selector: 'app-farm-settings',
  templateUrl: './farm-settings.component.html',
  styleUrls: ['./farm-settings.component.scss']
})
export class FarmSettingsComponent implements OnInit {
  public rentdetailchecked: boolean = true;

  constructor(private pubsubService: PubSubService) {}

  ngOnInit() {}

  onrentdetailToggleUpdated(event) {
    this.pubsubService.sendEvent('event.farm.rentdetail.updated', {
      checkboxEvent: event,
      value: event.checked
    });
  }
}
