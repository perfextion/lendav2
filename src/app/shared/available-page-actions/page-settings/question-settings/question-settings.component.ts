import { Component, OnInit, Input } from '@angular/core';

import { PubSubService } from '@lenda/services/pubsub.service';

@Component({
  selector: 'app-question-settings',
  templateUrl: './question-settings.component.html'
})
export class QuestionSettingsComponent implements OnInit {
  public questionSlider: boolean = true;

  @Input() pageName: string;

  constructor(private pubsubService: PubSubService) {}

  ngOnInit() {}

  onToggleUpdated(event) {
    this.pubsubService.sendEvent('event.question.slider.updated', {
      checkboxEvent: event,
      value: event.checked,
      page: this.pageName
    });
  }
}
