import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommitteeSettingsComponent } from './committee-settings.component';

describe('CommitteeSettingsComponent', () => {
  let component: CommitteeSettingsComponent;
  let fixture: ComponentFixture<CommitteeSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommitteeSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommitteeSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
