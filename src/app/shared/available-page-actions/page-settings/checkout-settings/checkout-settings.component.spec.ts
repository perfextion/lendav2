import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutSettingsComponent } from './checkout-settings.component';

describe('CheckoutSettingsComponent', () => {
  let component: CheckoutSettingsComponent;
  let fixture: ComponentFixture<CheckoutSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckoutSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
