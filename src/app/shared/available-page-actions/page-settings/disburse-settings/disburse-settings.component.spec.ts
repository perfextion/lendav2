import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisburseSettingsComponent } from './disburse-settings.component';

describe('DisburseSettingsComponent', () => {
  let component: DisburseSettingsComponent;
  let fixture: ComponentFixture<DisburseSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisburseSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisburseSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
