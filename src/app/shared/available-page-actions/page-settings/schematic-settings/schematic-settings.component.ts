import { Component, OnInit, OnDestroy } from '@angular/core';

import { SchematicSettings } from '@lenda/preferences/models/user-settings/schematic.model';

import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-schematic-settings',
  templateUrl: './schematic-settings.component.html',
  styleUrls: ['./schematic-settings.component.scss']
})
export class SchematicSettingsComponent implements OnInit, OnDestroy {
  public schematicPreferences: SchematicSettings;

  private preferencesChangeSub: ISubscription;

  constructor(private settingsService: SettingsService) {}

  ngOnInit() {
    this.getUserPreferences();
  }

  getUserPreferences() {
    this.schematicPreferences = this.settingsService.preferences.userSettings.schematicSettings;

    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe(preferences => {
      this.schematicPreferences = preferences.userSettings.schematicSettings;
    });
  }

  ngOnDestroy() {
    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  toggleChange($event) {
    this.schematicPreferences.isEnhanceDetailActive = $event.checked;
    this.settingsService.enhancedDetailChange.next(
      this.schematicPreferences.isEnhanceDetailActive
    );
  }
}
