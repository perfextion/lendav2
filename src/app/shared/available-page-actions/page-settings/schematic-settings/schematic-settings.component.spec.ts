import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchematicSettingsComponent } from './schematic-settings.component';

describe('SchematicSettingsComponent', () => {
  let component: SchematicSettingsComponent;
  let fixture: ComponentFixture<SchematicSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchematicSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchematicSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
