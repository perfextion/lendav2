import { Component, OnInit } from '@angular/core';

import { PubSubService } from '@lenda/services/pubsub.service';

@Component({
  selector: 'app-optimizer-settings',
  templateUrl: './optimizer-settings.component.html',
  styleUrls: ['./optimizer-settings.component.scss']
})
export class OptimizerSettingsComponent implements OnInit {
  public rentdetailchecked: boolean = false;

  constructor(private pubsubService: PubSubService) {}

  ngOnInit() {}

  onrentdetailToggleUpdated(event) {
    this.pubsubService.sendEvent('event.optimizer.rentdetail.updated', {
      checkboxEvent: event,
      value: event.checked
    });
  }
}
