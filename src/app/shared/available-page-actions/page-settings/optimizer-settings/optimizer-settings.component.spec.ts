import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptimizerSettingsComponent } from './optimizer-settings.component';

describe('OptimizerSettingsComponent', () => {
  let component: OptimizerSettingsComponent;
  let fixture: ComponentFixture<OptimizerSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptimizerSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptimizerSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
