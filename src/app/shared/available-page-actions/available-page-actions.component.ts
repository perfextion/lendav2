import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { PageInfoService } from '@lenda/services/page-info.service';

@Component({
  selector: 'app-available-page-actions',
  templateUrl: './available-page-actions.component.html',
  styleUrls: ['./available-page-actions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AvailablePageActionsComponent implements OnInit {
  currentPage: string;

  constructor(public pageInfoService: PageInfoService) {}

  ngOnInit() {
    this.pageInfoService.currentPage.subscribe(value => {
      this.currentPage = value;
    });
  }
}
