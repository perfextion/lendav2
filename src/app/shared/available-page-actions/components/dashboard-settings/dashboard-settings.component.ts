import { Component, OnInit, OnDestroy } from '@angular/core';

import { ViewMode } from '@lenda/preferences/models/base-entities/view-modes.enum';
import { Loansettings } from '@lenda/models/loansettings';

import { PubSubService } from '@lenda/services/pubsub.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { ISubscription } from 'rxjs-compat/Subscription';

@Component({
  selector: 'app-dashboard-settings',
  templateUrl: './dashboard-settings.component.html',
  styleUrls: ['./dashboard-settings.component.scss']
})
export class DashboardSettingsComponent implements OnInit, OnDestroy {
  isStandard: boolean = false;
  isCondensed: boolean = false;
  isSuperCondensed: boolean = false;
  loanSetting: Loansettings = new Loansettings();

  private preferencesChangeSub: ISubscription;

  constructor(
    private pubsubService: PubSubService,
    private settingsService: SettingsService
  ) {
    this.loanSetting = this.settingsService.userLoanSettings;
  }

  ngOnInit() {
    this.setView(this.loanSetting.dashboardSettings.viewMode);
    this.getLoanSetting();
  }

  private getLoanSetting() {
    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe(preferences => {
      this.loanSetting = this.settingsService.userLoanSettings;
      this.setView(this.loanSetting.dashboardSettings.viewMode);
    });
  }

  ngOnDestroy() {
    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  /**
   *
   * @param viewMode viewMode
   */
  public setView(viewMode) {
    this.pubsubService.sendEvent('event.dashboard.viewmode.updated', {
      viewMode: viewMode
    });

    // Logic to switch off the other mode
    switch (viewMode) {
      case ViewMode.condensed:
        this.isCondensed = true;
        this.isSuperCondensed = false;
        this.isStandard = false;
        break;
      case ViewMode.supercondensed:
        this.isCondensed = false;
        this.isSuperCondensed = true;
        this.isStandard = false;
        break;
      default:
        this.isCondensed = false;
        this.isSuperCondensed = false;
        this.isStandard = true;
        break;
    }
  }
}
