import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailablePageActionsComponent } from './available-page-actions.component';

describe('AvailablePageActionsComponent', () => {
  let component: AvailablePageActionsComponent;
  let fixture: ComponentFixture<AvailablePageActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailablePageActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailablePageActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
