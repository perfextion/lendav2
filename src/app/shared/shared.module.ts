import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { ChartsModule } from 'ng2-charts';
import { MomentModule } from 'angular2-moment';

import { MaterialModule } from './material.module';
import { UiComponentsModule } from './../ui-components/ui-components.module';

import { GetInitialPipe } from '@lenda/pipes/get-initial.pipe';
import { UsdCurrencyPipe } from '@lenda/pipes/usd-currenct.pipe';
import { UsdPricePipe } from '@lenda/pipes/usd-price.pipe';
import { LendaNumberPipe } from '@lenda/pipes/number.pipe';
import { MoneyPipe } from '@lenda/pipes/money.pipe';
import { PendingActionIconPipe } from '@lenda/pipes/pending-action-icon.pipe';

import { SidebarComponent } from './layout/sidebar.component';
import { ChartsVisualizationComponent } from '@lenda/shared/charts-visualization/charts-visualization.component';
import { CashFlowComponent } from '@lenda/shared/charts-visualization/cash-flow/cash-flow.component';
import { RiskAndReturnComponent } from '@lenda/shared/charts-visualization/risk-and-return/risk-and-return.component';
import { CompanyInfoComponent } from '@lenda/shared/charts-visualization/company-info/company-info.component';
import { RiskChartComponent } from '@lenda/shared/charts-visualization/risk-and-return/risk-chart/risk-chart.component';
import { CommitmentChartComponent } from '@lenda/shared/charts-visualization/risk-and-return/commitment-chart/commitment-chart.component';
import { BottomIconsComponent } from '@lenda/shared/charts-visualization/company-info/bottom-icons/bottom-icons.component';
import { ProgressChartComponent } from '@lenda/shared/charts-visualization/company-info/progress-chart/progress-chart.component';
import { HeaderIconsPanelComponent } from './layout/header-icons-panel/header-icons-panel.component';
import { ExceptionIconsComponent } from './layout/exception-icons/exception-icons.component';
import { SvgComponent } from './svg/svg.component';
import { AvailablePageActionsComponent } from './available-page-actions/available-page-actions.component';
import { SummarySettingsComponent } from './available-page-actions/page-settings/summary-settings/summary-settings.component';
import { SchematicSettingsComponent } from './available-page-actions/page-settings/schematic-settings/schematic-settings.component';
import { BorrowerSettingsComponent } from './available-page-actions/page-settings/borrower-settings/borrower-settings.component';
import { CropSettingsComponent } from './available-page-actions/page-settings/crop-settings/crop-settings.component';
import { FarmSettingsComponent } from './available-page-actions/page-settings/farm-settings/farm-settings.component';
import { InsuranceSettingsComponent } from './available-page-actions/page-settings/insurance-settings/insurance-settings.component';
import { BudgetSettingsComponent } from './available-page-actions/page-settings/budget-settings/budget-settings.component';
import { OptimizerSettingsComponent } from './available-page-actions/page-settings/optimizer-settings/optimizer-settings.component';
import { CollateralSettingsComponent } from './available-page-actions/page-settings/collateral-settings/collateral-settings.component';
import { CommitteeSettingsComponent } from './available-page-actions/page-settings/committee-settings/committee-settings.component';
import { CheckoutSettingsComponent } from './available-page-actions/page-settings/checkout-settings/checkout-settings.component';
import { DisburseSettingsComponent } from './available-page-actions/page-settings/disburse-settings/disburse-settings.component';
import { ReconcileSettingsComponent } from './available-page-actions/page-settings/reconcile-settings/reconcile-settings.component';
import { DashboardSettingsComponent } from './available-page-actions/components/dashboard-settings/dashboard-settings.component';
import { LoanListSettingsComponent } from './available-page-actions/page-settings/loan-list-settings/loan-list-settings.component';
import { BtnCounterComponent } from '@lenda/preferences/ui-components/btn-counter/btn-counter.component';
import { RiskOtherQueueFilterPipe } from '@lenda/pipes/roq.pipe';
import { QuestionSettingsComponent } from './available-page-actions/page-settings/question-settings/question-settings.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
    ChartsModule,
    UiComponentsModule,
    MomentModule,
    MaterialModule
  ],
  declarations: [
    SidebarComponent,
    ChartsVisualizationComponent,
    CashFlowComponent,
    RiskAndReturnComponent,
    CompanyInfoComponent,
    RiskChartComponent,
    CommitmentChartComponent,
    BottomIconsComponent,
    ProgressChartComponent,
    HeaderIconsPanelComponent,
    ExceptionIconsComponent,
    SvgComponent,
    PendingActionIconPipe,
    AvailablePageActionsComponent,
    SummarySettingsComponent,
    SchematicSettingsComponent,
    BorrowerSettingsComponent,
    CropSettingsComponent,
    FarmSettingsComponent,
    InsuranceSettingsComponent,
    BudgetSettingsComponent,
    OptimizerSettingsComponent,
    CollateralSettingsComponent,
    CommitteeSettingsComponent,
    CheckoutSettingsComponent,
    DisburseSettingsComponent,
    ReconcileSettingsComponent,
    DashboardSettingsComponent,
    LoanListSettingsComponent,
    BtnCounterComponent,
    GetInitialPipe,
    UsdCurrencyPipe,
    LendaNumberPipe,
    UsdPricePipe,
    RiskOtherQueueFilterPipe,
    MoneyPipe,
    QuestionSettingsComponent
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    SidebarComponent,
    ChartsVisualizationComponent,
    CashFlowComponent,
    RiskAndReturnComponent,
    CompanyInfoComponent,
    RiskChartComponent,
    CommitmentChartComponent,
    BottomIconsComponent,
    ProgressChartComponent,
    HeaderIconsPanelComponent,
    MomentModule,
    ExceptionIconsComponent,
    PendingActionIconPipe,
    AvailablePageActionsComponent,
    BtnCounterComponent,
    GetInitialPipe,
    UsdCurrencyPipe,
    LendaNumberPipe,
    UsdPricePipe,
    MoneyPipe,
    RiskOtherQueueFilterPipe
  ]
})
export class SharedModule {}
