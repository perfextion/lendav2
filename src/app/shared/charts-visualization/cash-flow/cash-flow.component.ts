import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import 'chart.piecelabel.js';
import { ISubscription } from 'rxjs/Subscription';
import { DataService } from '@lenda/services/data.service';
import { loan_model, Loan_Budget } from "@lenda/models/loanmodel";
import { chartSettings } from "@lenda/chart-settings";
import { environment } from "@env/environment";
import { ViewMode } from '@lenda/preferences/models/base-entities/view-modes.enum';
import { RefDataModel, LoanMaster } from '@lenda/models/ref-data-model';
import { Substring } from '@lenda/services/common-utils';

@Component({
  selector: 'app-cash-flow',
  templateUrl: './cash-flow.component.html',
  styleUrls: ['./cash-flow.component.scss']
})
export class CashFlowComponent implements OnInit, OnDestroy, OnChanges {

  private subscription : ISubscription;
  @Input() viewMode: ViewMode;
  @Input() viewClass: string;

  @Input() isARM: boolean;

  public info = {
    budget: 0,
    cashFlow: 0,
    breakEven: '0',
    excessIns: 0,
    excessInsPercent: '',
    cashFlowPercentage: '0'
  };

  localLoanObject: loan_model;
  // Doughnut
  public doughnutChartLabels: string[] = [];
  public doughnutChartData: any[] = [];
  public fullNameLables: string[];
  // public generatedColors: string[] = [];

  public doughnutChartType: string = 'doughnut';
  public chartColors: any[] = [
    {
      backgroundColor:
      // this.generatedColors
      chartSettings.doughnut.backgroundColors
    }];

  public chartOptions: any = {
    legend: {
      position: 'right',
      labels: {
        fontColor: chartSettings.doughnut.legendColor,
        fontSize: 11,
        usePointStyle: false,
        boxWidth: 10,
        padding: 5,
        strokeStyle: '#fff',
        lineWidth: 2
      }
    },
    pieceLabel: {
      render: 'percentage',
      fontColor: chartSettings.doughnut.textColor,
      showActualPercentages: true
    },
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          let label = this.fullNameLables[tooltipItem.index];
          let value = parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]).toLocaleString();
          return `${label}: ${value}`;
        }
      }
    }
  };

  constructor(
    private localStorageService: LocalStorageService,
    private dataService : DataService
  ) { }

  ngOnInit() {
    this.localLoanObject = this.localStorageService.retrieve(environment.loankey);
    if (this.localLoanObject && this.localLoanObject.LoanMaster && this.localLoanObject.LoanBudget) {
      this.getLoanBudgetFromLocalStorage(this.localLoanObject.LoanBudget);
      this.getLoanSummary(this.localLoanObject.LoanMaster);
    }

    this.subscription =  this.dataService.getLoanObject().subscribe((res: loan_model)=>{
      if(res){
        this.localLoanObject = res;
        if (this.localLoanObject && this.localLoanObject.LoanMaster && this.localLoanObject.LoanBudget) {
          this.getLoanBudgetFromLocalStorage(this.localLoanObject.LoanBudget);
          this.getLoanSummary(this.localLoanObject.LoanMaster);
        }
      }
    })
  }

  ngOnChanges(){
    if(!this.isARM){
      if(this.viewMode == ViewMode.standard || this.viewMode == ViewMode.condensed){
        this.viewMode = ViewMode.condensed;
      }
    }
  }

  ngOnDestroy(){
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }

  getLoanSummary(loanMaster: LoanMaster) {
    this.info.budget = loanMaster.Total_Commitment ? loanMaster.Total_Commitment : 0;
    this.info.cashFlow = loanMaster.Cash_Flow_Amount ? loanMaster.Cash_Flow_Amount : 0;
    if(loanMaster.Break_Even_Percent > 0) {
      this.info.breakEven = loanMaster.Break_Even_Percent ? loanMaster.Break_Even_Percent.toFixed(1) : '0';
    } else {
      this.info.breakEven = '0';
    }

    this.info.cashFlowPercentage = loanMaster.Cash_Flow_Percent ? loanMaster.Cash_Flow_Percent.toFixed(1) : '0';

    let excessIns = ( loanMaster.Net_Market_Value_Insurance + loanMaster.FC_Total_Addt_Collateral_Value) - (loanMaster.ARM_Commitment + loanMaster.Dist_Commitment + loanMaster.Interest_Est_Amount);
    this.info.excessIns = excessIns ? excessIns : 0;

    let totalCmt = loanMaster.Total_Commitment;
    if(totalCmt > 0) {
      this.info.excessInsPercent = excessIns ? ((excessIns) * 100 / parseFloat(totalCmt as any)).toFixed(1) + '%' || '0.0%' : '0.0%';
    } else {
      this.info.excessInsPercent = '0.0%';
    }
  }

  getLoanBudgetFromLocalStorage(loanBudgets: Loan_Budget[]) {
    this.doughnutChartLabels = [];
    this.doughnutChartData = [];
    this.fullNameLables = [];

    let refData: RefDataModel = this.localStorageService.retrieve(environment.referencedatakey);
    this.chartColors[0].backgroundColor = chartSettings.doughnut.backgroundColors;

    const data = loanBudgets.filter(p=>p.Crop_Practice_ID==0 && p.Budget_Type.trim()=='V' && p.ActionStatus !=3)
    .filter(a => a.Total_Budget_Crop_ET > 0)
    .sort((a,b) => a.Sort_order - b.Sort_order);

    let isMoreThan20 = data.length > 10;

    data.slice(0, 20)
    .forEach(b => {
      let Budget_Expense_Name = b.Expense_Type_Name;
      if(!Budget_Expense_Name) {
        Budget_Expense_Name =  this.getBudgetExpenseType(b.Expense_Type_ID, refData);
      }

      this.fullNameLables.push(Budget_Expense_Name);

      if(isMoreThan20) {
        Budget_Expense_Name = Substring(Budget_Expense_Name, 10);
      } else {
        Budget_Expense_Name = Substring(Budget_Expense_Name, 20);
      }

      this.doughnutChartLabels.push(Budget_Expense_Name);
      this.doughnutChartData.push(Math.round(b.Total_Budget_Crop_ET || 0));

      if(this.doughnutChartLabels.length > this.chartColors[0].backgroundColor.length){
        let randomColor = this.dynamicColors();
        this.chartColors[0].backgroundColor.push(randomColor);
      }
    });
  }

  private getBudgetExpenseType(expenseType: number, refData: RefDataModel) {
    for (let type of refData.BudgetExpenseType) {
      if (type.Budget_Expense_Type_ID === expenseType) {
        return type.Budget_Expense_Name;
      }
    }
    return 'UNDEFINED';
  }

  dynamicColors() {
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);
    return "rgba(" + r + "," + g + "," + b + ", 0.85)";
  }
}
