import { Component, OnInit, Input, AfterViewInit, OnDestroy } from '@angular/core';
import { SettingsService } from "@lenda/preferences/settings/settings.service";
import { Preferences } from "@lenda/preferences/models/index.model";
import { ViewMode } from "@lenda/preferences/models/base-entities/view-modes.enum";
import { Page } from '@lenda/models/page.enum';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';
import { LoanGroup, loan_model } from '@lenda/models/loanmodel';
import { Loansettings } from '@lenda/models/loansettings';
import { DataService } from '@lenda/services/data.service';
import { ISubscription } from 'rxjs/Subscription';
import { SharedService } from '@lenda/services/shared.service';
import { PubSubService } from '@lenda/services/pubsub.service';

@Component({
  selector: 'app-charts-visualization',
  templateUrl: './charts-visualization.component.html',
  styleUrls: ['./charts-visualization.component.scss']
})
export class ChartsVisualizationComponent implements OnInit, AfterViewInit, OnDestroy {
  pubsubSubscriber: ISubscription;
  subscription: ISubscription;

  private localLoanObject: loan_model;
  loanSetting: Loansettings = new Loansettings();

  isStandard: boolean = true;
  isCondensed: boolean = false;
  isSuperCondensed: boolean = false;

  @Input() viewMode: string = ViewMode.standard;
  @Input() page: Page = Page.reports;
  public isFixedOnTop: boolean = true;

  public viewClass: string;

  isARM = true;

  isCrossCollaterized: boolean;

  private preferencesChangeSub: ISubscription;

  constructor(
    private settingsService: SettingsService,
    private localStorage: LocalStorageService,
    private dataService: DataService,
    private sharedservice:SharedService,
    private pubsubService: PubSubService
  ) {
    this.localLoanObject = this.localStorage.retrieve(environment.loankey);
    this.loanSetting = this.settingsService.userLoanSettings;
  }

  ngOnInit() {
    this.pubsubSubscriber = this.pubsubService.subscribeToEvent().subscribe((event) => {
      if (event.name === 'event.dashboard.viewmode.updated') {
        this.setView(event.data.viewMode);
      }
    });

    this.getViewMode();

    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe((preferences) => {
      this.loanSetting = this.settingsService.userLoanSettings;
      this.getViewMode();
    });

    this.setCrossCollateralStatus();
  }


  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.pubsubService) {
      this.pubsubSubscriber.unsubscribe();
    }

    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  ngAfterViewInit(){
    this.getViewMode();
  }

  private getViewMode(){
    this.viewMode = this.loanSetting.dashboardSettings.viewMode;
    //
  }

  /**
   * To set cross collateral status
   * TODO: Call this function later (when needed)
   */
  setCrossCollateralStatus() {
   let loanGroups: Array<LoanGroup> = this.localStorage.retrieve(environment.loanGroup);
    // Need to observe as otherwise sometimes loangroup is not available
    this.subscription = this.localStorage.observe(environment.loanGroup).subscribe(res=>{
      loanGroups = res;
      if(loanGroups) {
        this.isCrossCollaterized = loanGroups.some(lg => lg.IsCrossCollateralized);
      } else {
        this.isCrossCollaterized = false;
      }
    });

    if(loanGroups) {
      this.isCrossCollaterized = loanGroups.some(lg => lg.IsCrossCollateralized);
    } else {
      this.isCrossCollaterized = false;
    }
  }

  private viewClassMode() {
    // if(this.isARM){
    //   return this.loanSetting.dashboardSettings.viewMode;
    // } else {
    //   if(this.loanSetting.dashboardSettings.viewMode == ViewMode.standard || this.loanSetting.dashboardSettings.viewMode == ViewMode.condensed){
    //     return ViewMode.condensed;
    //   } else {
    //     return this.loanSetting.dashboardSettings.viewMode;
    //   }
    // }
    return this.viewMode;
  }

  get viewModeByPage() {
    return `${this.page}Dashboard`;
  }

  getViewClass() {
    return this.isFixedOnTop ? `${this.viewClassMode()} fixed`: this.viewClassMode();
  }

  setView(viewMode) {
    this.loanSetting.dashboardSettings.viewMode = viewMode;
    switch (viewMode) {
      case ViewMode.condensed:
        this.isCondensed = true;
        this.isSuperCondensed = false;
        this.isStandard = false;
        break;
      case ViewMode.supercondensed:
        this.isCondensed = false;
        this.isSuperCondensed = true;
        this.isStandard = false;
        break;
      default:
        this.isCondensed = false;
        this.isSuperCondensed = false;
        this.isStandard = true;
        break;
    }

    this.viewMode = viewMode;
  }
}
