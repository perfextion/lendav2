import { Component, OnInit, Input, OnDestroy, OnChanges } from '@angular/core';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { ViewMode } from '@lenda/preferences/models/base-entities/view-modes.enum';
import { DataService } from '@lenda/services/data.service';
import {
  loan_model,
  Loantype_dsctext,
  borrower_model
} from '@lenda/models/loanmodel';
import { environment } from '@env/environment';
import { LoanMaster } from '@lenda/models/ref-data-model';
import { Get_Borrower_Name } from '@lenda/services/common-utils';
import { PublishService } from '@lenda/services/publish.service';

@Component({
  selector: 'app-company-info',
  templateUrl: './company-info.component.html',
  styleUrls: ['./company-info.component.scss']
})
export class CompanyInfoComponent implements OnInit, OnDestroy, OnChanges {
  private subscription: ISubscription;
  public _ViewMode: typeof ViewMode = ViewMode;

  @Input()
  viewMode: ViewMode;

  @Input()
  isARM: boolean;

  @Input()
  isCrossCollaterized: boolean;

  public info = {
    borrwerFirstName: '',
    borrowerLastName: '',
    farmerFirstName: '',
    farmerLastName: '',
    farmerMI: '',
    loanFullId: '',
    cropYear: new Date().getFullYear(),
    FARMing: null,
    ARMing: null,
    Local_Ind: false,
    Controlled_Disbursement_Ind: false
  };

  public starsCount: number = 1;
  public localLoanObject: loan_model;
  public isDevAdmin: boolean;

  constructor(
    private localStorageService: LocalStorageService,
    private dataService: DataService,
    private publishservice:PublishService,
    private sessionservice: SessionStorageService
  ) {}

  get loantype() {
    return Loantype_dsctext(this.localLoanObject.LoanMaster.Loan_Type_Code);
  }

  ngOnInit() {
    this.localLoanObject = this.localStorageService.retrieve(environment.loankey);
    let user = this.sessionservice.retrieve("UID");
    if(user) {
      this.isDevAdmin = user.DevAdmin == 1;
    }

    if (this.localLoanObject && this.localLoanObject.LoanMaster) {
      this.getCompanyInfo(this.localLoanObject.LoanMaster);
    }

    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res) {
          this.localLoanObject = res;
          if (this.localLoanObject && this.localLoanObject.LoanMaster) {
            this.getCompanyInfo(this.localLoanObject.LoanMaster);
          }
        }
      });
  }

  ngOnChanges(){
    if(!this.isARM){
      if(this.viewMode == ViewMode.standard || this.viewMode == ViewMode.condensed){
        this.viewMode = ViewMode.condensed;
      }
    }
  }

  syncloanforce() {
    if(this.isDevAdmin) {
      this.publishservice.syncToDb();
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getCompanyInfo(loanMaster: LoanMaster) {
    this.info.borrwerFirstName = loanMaster.Borrower_First_Name;
    this.info.borrowerLastName = loanMaster.Borrower_Last_Name;
    this.info.farmerFirstName = loanMaster.Farmer_First_Name;
    this.info.farmerLastName = loanMaster.Farmer_Last_Name;
    this.info.farmerMI = loanMaster.Farmer_MI;
    this.info.loanFullId = loanMaster.Loan_Full_ID;
    this.starsCount = loanMaster.Borrower_Rating || 0;
    this.info.FARMing = loanMaster.Year_Begin_Farming ?  (new Date()).getFullYear() - (loanMaster.Year_Begin_Farming) : 0;
    this.info.ARMing = loanMaster.Year_Begin_Client ? (new Date()).getFullYear() - (loanMaster.Year_Begin_Client) : 0;
    this.info.cropYear = loanMaster.Crop_Year;
    this.info.Local_Ind =  loanMaster.Local_Ind == 1;
    this.info.Controlled_Disbursement_Ind = loanMaster.Controlled_Disbursement_Ind == 1;
  }

  get address() {
    let address = this.localLoanObject.LoanMaster.Borrower_City || '';

    if(address) {
      address += ', ';
    }

    address += this.localLoanObject.LoanMaster.Borrower_State_Abbrev || '';

    if(address) {
      address += ' - ';
    }

    address += this.localLoanObject.LoanMaster.Borrower_Zip || '';

    return address;
  }

  get BorrowerName() {
    return Get_Borrower_Name(this.localLoanObject.LoanMaster as any);
  }

  get FarmerName() {
    let name = this.info.farmerLastName || '';

    if(name) {
      name += ', ';
    }

    name += (this.info.farmerFirstName || '') + ' ' + ( this.info.farmerMI || '');

    return name;
  }
}
