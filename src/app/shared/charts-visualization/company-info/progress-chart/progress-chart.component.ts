import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';

import { chartSettings } from "@lenda/chart-settings";
import { LoanInventoryService } from '@lenda/components/reports/loan-inventory/loan-inventory.service';
declare var Chart;

@Component({
  selector: 'app-progress-chart,[app-progress-chart]',
  templateUrl: './progress-chart.component.html',
  styleUrls: ['./progress-chart.component.scss']
})
export class ProgressChartComponent implements OnInit, OnDestroy {
  @Input() viewMode;
  @Input() isSmallSize: boolean = false;

  public chartAreaRight: number = 100;

  // TODO: Replace this data with live API
  public doughnutChartLabels: string[] = ['Progress', 'Remaining'];
  public doughnutChartData: number[] = [65, 35];

  // Doughnut chart settings
  public doughnutChartType: string = 'doughnut';
  public chartColors: any[] = [
    {
      backgroundColor: [
        chartSettings.doughnut.theme.primary,
        chartSettings.doughnut.theme.secondary
      ]
    }];

  public chartOptions: any = {
    cutoutPercentage: 75,
    devicePixelRatio: 1.5,
    legend: {
      display: false
    },
    elements: {
      center: {
        text: this.doughnutChartData[0],
        color: chartSettings.doughnut.theme.primary,
        textStyle: chartSettings.doughnut.typography.textStyle,
        iconStyle: chartSettings.doughnut.typography.iconStyle,
        textSuffixStyle: chartSettings.doughnut.typography.textSuffixStyle
      }
    },
    layout: {
      padding: {
        left: 100,
        right: 0,
        top: 0,
        bottom: 0
      }
    }
  };

  private subscription : ISubscription;

  constructor(private loanInventory: LoanInventoryService) { }

  ngOnInit() {
    // Adjustment for schematics when progress chart is small
    if (this.isSmallSize) {
      this.chartOptions.elements.center.textStyle = chartSettings.doughnut.typography.textStyleSm;
      this.chartOptions.elements.center.iconStyle = chartSettings.doughnut.typography.iconStyleSm;
      this.chartOptions.elements.center.textSuffixStyle = chartSettings.doughnut.typography.textSuffixStyleSm;
    }

    this.setChart(this.loanInventory.completedPerc);

    // this.applyCustomStyleChartJs();

    this.subscription = this.loanInventory.loanInventoryStatusChange.subscribe(res => {
      this.setChart(res);
    });
  }

  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  setChart(res) {
    let completedPerc = parseFloat(res.toFixed(0));
    this.doughnutChartData = [ completedPerc, 100 - completedPerc ];
    this.chartOptions.elements.center.text = completedPerc;
    this.applyCustomStyleChartJs();
  }

  // Custom code for adding icon and text inside chartjs doughnut chart
  applyCustomStyleChartJs() {
    Chart.pluginService.register({
      beforeDraw: (chart) => {
        if (chart.config.options.elements.center) {
          chart.config.options.elements.center.text = this.chartOptions.elements.center.text;
          // Get ctx from string
          let ctx = chart.chart.ctx;
          let centerConfig = chart.config.options.elements.center;
          let color = centerConfig.color;
          ctx.clearRect(0, 0, chart.width, chart.height);

          // Set properties
          ctx.fillStyle = color;
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';

          // Set font settings to draw it correctly.
          let centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          let centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);

          // Set icon - shopping cart
          ctx.font = centerConfig.iconStyle;
          if (this.isSmallSize) {
            ctx.fillText('\uf100', centerX, centerY + 10);
          } else {
            ctx.fillText('\uF100', centerX, centerY + 10);
          }

          // Set text - percentage
          ctx.font = centerConfig.textStyle;
          if (this.isSmallSize) {
            ctx.fillText(centerConfig.text, centerX - 5, centerY - 10);
          } else {
            ctx.fillText(centerConfig.text, centerX - 5, centerY - 15);
          }

          // Smaller font for % text
          ctx.font = centerConfig.textSuffixStyle;
          if(chart.config.options.elements.center.text == 100) {
            if (this.isSmallSize) {
              ctx.fillText('%', centerX + 20, centerY - 15);
            } else {
              ctx.fillText('%', centerX + 20, centerY - 20);
            }
          } else {
            if (this.isSmallSize) {
              ctx.fillText('%', centerX + 15, centerY - 15);
            } else {
              ctx.fillText('%', centerX + 15, centerY - 20);
            }
          }
        }
      }
    });
  }
}
