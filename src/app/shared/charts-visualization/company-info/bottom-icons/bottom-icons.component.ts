import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment';
import { ViewMode } from '@lenda/preferences/models/base-entities/view-modes.enum';
import { loan_model } from '@lenda/models/loanmodel';
import { ISubscription } from 'rxjs/Subscription';
import { ValidationErrorsCountService } from '@lenda/Workers/calculations/validationerrorscount.service';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-bottom-icons',
  templateUrl: './bottom-icons.component.html',
  styleUrls: ['./bottom-icons.component.scss']
})
export class BottomIconsComponent implements OnInit, OnDestroy {
  @Input()
  viewMode: ViewMode;

  @Input()
  isARM: boolean;

  public loanStatus;
  public isWatchListEnabled: boolean = false;
  public isCrossCollateralEnabled: boolean = false;

  private subscription : ISubscription;
  private datasubscritpion: ISubscription;

  private loanobject: loan_model;

  constructor(
    private localStorageService: LocalStorageService,
    private validationCount: ValidationErrorsCountService,
    private dataservice: DataService
  ) {}

  ngOnInit() {
    this.loanobject = this.localStorageService.retrieve(environment.loankey) as loan_model;

    this.setData();

    this.setExceptionCount(this.validationCount.getValidationErrors());

    this.subscription = this.validationCount.latestValidationCount$.subscribe(valiationerrors => {
      this.setExceptionCount(valiationerrors);
    });

    this.datasubscritpion = this.dataservice.getLoanObject().subscribe(res => {
      this.loanobject = res;
      this.setData();
    });
  }

  private setData() {
    let loanMaster = this.loanobject.LoanMaster;
    this.loanStatus = loanMaster.Loan_Status;
    this.isWatchListEnabled = loanMaster.Watch_List_Ind == 1;
    this.isCrossCollateralEnabled = loanMaster.Cross_Col_ID !== 0;
  }

  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.datasubscritpion) {
      this.datasubscritpion.unsubscribe();
    }
  }

  errors = {};

  setExceptionCount(valiationerrors){
    let warningCount = 0;
    let errorCount = 0;
    let Level1Count = 0;
    let Level2Count = 0;


    Object.keys(valiationerrors).forEach(tab => {
      warningCount += valiationerrors[tab].exceptions.level1;
      errorCount += valiationerrors[tab].exceptions.level2;
      Level1Count += valiationerrors[tab].validations.level1;
      Level2Count += valiationerrors[tab].validations.level2;
    });

    this.errors['level1'] = warningCount;
    this.errors['level2'] = errorCount;
    this.errors['level3'] = Level1Count;
    this.errors['level4'] = Level2Count;
  }
}
