import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ViewMode } from '@lenda/preferences/models/base-entities/view-modes.enum';

@Component({
  selector: 'app-risk-and-return',
  templateUrl: './risk-and-return.component.html',
  styleUrls: ['./risk-and-return.component.scss']
})
export class RiskAndReturnComponent implements OnInit, OnChanges {
  @Input()
  viewMode: ViewMode;

  @Input() isARM: boolean;

  constructor() {}

  ngOnInit() {}

  ngOnChanges(){
    if(!this.isARM){
      if(this.viewMode == ViewMode.standard || this.viewMode == ViewMode.condensed){
        this.viewMode = ViewMode.condensed;
      }
    }
  }
}