import { Component, OnInit, OnDestroy, Input, ViewChild, ElementRef, AfterViewInit, HostListener } from '@angular/core';
import * as d3 from 'd3';
import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import { DataService } from '@lenda/services/data.service';
import { formatcurrency } from '@lenda/aggridformatters/valueformatters';
import { environment } from "@env/environment";
import { loan_model } from "@lenda/models/loanmodel";
import { chartSettings } from "@lenda/chart-settings";
import { ViewMode } from '@lenda/preferences/models/base-entities/view-modes.enum';
import { LoanMaster } from '@lenda/models/ref-data-model';
import { Helper } from '@lenda/services/math.helper';
import { UsdCurrencyPipe } from '@lenda/pipes/usd-currenct.pipe';
import { PubSubService } from '@lenda/services/pubsub.service';
import { fromEvent } from 'rxjs';
import { LayoutService } from '@lenda/shared/layout/layout.service';

@Component({
  selector: 'app-commitment-chart',
  templateUrl: './commitment-chart.component.html',
  styleUrls: ['./commitment-chart.component.scss']
})
export class CommitmentChartComponent implements OnInit, OnDestroy {

  @Input()
  viewMode: ViewMode;

  @Input()
  isARM = true;

  @ViewChild('commitChart') commitChart: ElementRef<HTMLDivElement>;

  private subscription : ISubscription;
  public info = {
    armCommitment: 0,
    distCommitment: 0,
    totalCommitment: 0,
    excessIns: 0,
    excessInsPercent: 0,
    armX: 0,
    distX: 0,
    distWidth: 0,
    armWidth: 0,
    armTextWidth: 0,
    distTextWidth: 0,
    marginWidth: 0,
    marginTextWidth: 0,
    marginX: 0,
    scaleTextPosition: 425
  };

  localLoanObject : loan_model;
  armCommit = chartSettings.commitmentExcessIns.armCommit;
  distCommit = chartSettings.commitmentExcessIns.distCommit;
  excessIns = chartSettings.commitmentExcessIns.excessIns;

  totalWidth: number = 400;
  maxWidth: number = (document.body.clientWidth - 215 ) / 3;

  private pubSubSubscritpion: ISubscription;
  private resizeSubscription: ISubscription;
  private layoutSubscription: ISubscription;

  marginScale: number;

  private sideBarWidth: number;

  constructor(
    private localStorageService: LocalStorageService,
    private dataService : DataService,
    private pubsubService: PubSubService,
    private layout: LayoutService
  ) { }

  ngOnInit() {

    this.localLoanObject = this.localStorageService.retrieve(environment.loankey);
    if(this.localLoanObject && this.localLoanObject.LoanMaster){
      this.resizeChart();
    }

    this.subscription =  this.dataService.getLoanObject().subscribe((res: loan_model)=>{
      if(res){
        this.localLoanObject = res;
        if(this.localLoanObject && this.localLoanObject.LoanMaster){
          this.resizeChart();
        }
      }
    });

    this.pubSubSubscritpion = this.pubsubService.subscribeToEvent().subscribe(event => {
      if(event.name == 'event.dashboard.viewmode.updated') {
        this.resizeChart();
      }
    });

    this.resizeSubscription = fromEvent(window, 'resize').subscribe(a => {
      this.resizeChart();
    });

    this.layoutSubscription = this.layout.isSidebarExpanded().subscribe(res => {
      if(res) {
        this.sideBarWidth = 220;
      } else {
        this.sideBarWidth = 80;
      }
      this.resizeChart();
    });
  }

  resizeChart() {
    const self = this;
    self.maxWidth = ((document.body.clientWidth - self.sideBarWidth ) / 3) - 20;

    setTimeout(() => {
      if(self.commitChart) {
        self.totalWidth = Math.min(self.commitChart.nativeElement.offsetWidth, self.maxWidth);
        self.totalWidth = Math.max(self.totalWidth, 330);
        self.processInfo(self.localLoanObject.LoanMaster);
      }
    }, 500);
  }

  ngOnDestroy(){
    if(this.subscription){
      this.subscription.unsubscribe();
    }

    if(this.pubSubSubscritpion) {
      this.pubSubSubscritpion.unsubscribe();
    }

    if(this.resizeSubscription) {
      this.resizeSubscription.unsubscribe();
    }

    if(this.layoutSubscription) {
      this.layoutSubscription.unsubscribe();
    }
  }

  processInfo(loanMaster: LoanMaster){
    this.getCommitment(loanMaster);
    this.calculateScale(this.info.armCommitment, this.info.distCommitment, this.info.excessIns);
    // Set bars

    const armPosition = Helper.divide(this.info.armCommitment * this.totalWidth, this.marginScale);
    const distPosition = Helper.divide(this.info.distCommitment * this.totalWidth, this.marginScale);
    const marginPosition = Helper.divide(this.info.excessIns * this.totalWidth, this.marginScale);

    this.info.armWidth = armPosition;
    this.info.distWidth = distPosition;

    let cp = new UsdCurrencyPipe();

    this.info.armTextWidth = this.getWidth(cp.transform(this.info.armCommitment));

    if(this.info.distCommitment > 0) {
      this.info.distTextWidth = this.getWidth(cp.transform(this.info.distCommitment));
    } else {
      this.info.distTextWidth = 0;
    }

    this.info.armX = (armPosition - this.info.armTextWidth) / 2;
    this.info.distX = armPosition + ((this.info.distWidth - this.info.distTextWidth) / 2);

    this.setChart('#arm', 0, armPosition, chartSettings.commitmentExcessIns.armCommit, 10, cp.transform(this.info.armCommitment));

    if(this.info.distCommitment > 0) {
      this.setChart('#dist', armPosition - 1, armPosition + distPosition, chartSettings.commitmentExcessIns.distCommit, 10, cp.transform(this.info.distCommitment));
    } else {
      this.setChart('#dist', armPosition - 1, armPosition - 1, chartSettings.commitmentExcessIns.distCommit, 10, cp.transform(this.info.distCommitment));
    }

    let marginWidth = marginPosition;
    this.info.marginWidth = Math.abs(marginWidth);
    this.info.marginTextWidth = this.getWidth(cp.transform(this.info.excessIns));
    this.info.marginX = armPosition + distPosition + ((marginWidth - this.info.marginTextWidth) / 2);

    if(this.info.excessIns > 0) {
      this.setChart('#excess', armPosition + distPosition, armPosition + distPosition + marginWidth, chartSettings.commitmentExcessIns.excessIns, 25, cp.transform(this.info.excessIns));
    } else {
      this.setChart('#excess',armPosition + distPosition + marginWidth, armPosition + distPosition, chartSettings.commitmentExcessIns.excessIns, 25, cp.transform(this.info.excessIns));
    }

    let scaleTextWidth = this.getWidth(this.getScale());

    this.info.scaleTextPosition = Math.min(this.totalWidth - scaleTextWidth - 5, 425);

    // Set gridlines
    for (let i = 0; i < 9; i++) {
      this.setGrid('#grid-' + i, 'rgba(0, 0, 0, 0.04)', i * this.totalWidth / 8);
    }
  }

  setChart(
    item: string,
    rangeStart: number,
    rangeEnd: number,
    color: string,
    translateY: number,
    value: string
  ) {
    let linearScale = d3.scaleLinear()
      .domain([0, 1])
      .range([rangeStart, rangeEnd]);

    let myData = d3.range(0, 1);

    d3.select(item).selectAll('*').remove();

    d3.select(item)
      .selectAll('rect')
      .data(myData)
      .enter()
      .append('rect')
      .attr('x', function (d) {
        return linearScale(d);
      })
      .attr('transform', function (d, i) {
        return 'translate(0, ' + translateY + ')';
      })
      .attr('width', Math.abs(rangeEnd - rangeStart))
      .attr('height', 15)
      .attr('fill', color)
      .append('svg:title').text((d, i) => value);
  }

  getCommitment(loanMaster: LoanMaster) {
    this.info.armCommitment = loanMaster.ARM_Commitment ? loanMaster.ARM_Commitment : 0;
    this.info.distCommitment = loanMaster.Dist_Commitment ? loanMaster.Dist_Commitment : 0;

    let totalCmt = loanMaster.Total_Commitment;
    this.info.totalCommitment = totalCmt ? totalCmt : 0;

    // TODO: Get excessin value from local storage
    let excessIns = ( loanMaster.Net_Market_Value_Insurance + loanMaster.FC_Total_Addt_Collateral_Value) - (loanMaster.ARM_Commitment + loanMaster.Dist_Commitment + loanMaster.Interest_Est_Amount);
    this.info.excessIns = excessIns ? excessIns : 0;

    this.info.excessInsPercent = Helper.percent(this.info.excessIns, this.info.totalCommitment);

  }
  setGrid(item: string, color: string, translateX: number) {
    let linearScale = d3.scaleLinear()
      .domain([0, 1])
      .range([0, 100]);

    let myData = d3.range(0, 1);

    d3.select(item).selectAll('*').remove();
    d3.select(item)
      .selectAll('rect')
      .data(myData)
      .enter()
      .append('rect')
      .attr('x', function (d) {
        return linearScale(d);
      })
      .attr('transform', function (d, i) {
        return 'translate(' + translateX + ', 0)';
      })
      .attr('width', 1)
      .attr('height', 50)
      .attr('fill', color);
  }


  getWidth(text: string) {
    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext("2d");
    ctx.font = "10px Roboto";
    let width = ctx.measureText(text).width;
    return width;
  }

  calculateScale(arm: number, dist: number, margin: number) {
    let total = Math.abs(arm) + Math.abs(dist) + Math.abs(margin);
    if(total  <= environment.marginScale * 0.75) {
      this.marginScale = environment.marginScale;
    } else {
      this.marginScale = environment.marginScale + environment.marginScale * Math.round(Helper.divide(total, environment.marginScale));
    }
  }

  getScale() {
    let scale = Math.round(Helper.divide(this.marginScale, environment.marginScale));
    return `Scale: ${scale}M`;
  }
}
