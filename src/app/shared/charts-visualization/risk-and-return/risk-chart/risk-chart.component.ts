import { Component, OnInit, Input, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import * as d3 from 'd3';
import { ISubscription } from 'rxjs/Subscription';
import { DataService } from '@lenda/services/data.service';
import { loan_model } from '@lenda/models/loanmodel';
import { environment } from '@env/environment';
import { chartSettings } from '@lenda/chart-settings';
import { ViewMode } from '@lenda/preferences/models/base-entities/view-modes.enum';
import { ScaleHelper } from '@lenda/services/scale.helper';
import { RefDataService } from '@lenda/services/ref-data.service';
import { LoanMaster, RefDataModel } from '@lenda/models/ref-data-model';
import { fromEvent } from 'rxjs';
import { ILogger } from '@lenda/Workers/utility/logger';
import { LayoutService } from '@lenda/shared/layout/layout.service';

@Component({
  selector: 'app-risk-chart',
  templateUrl: './risk-chart.component.html',
  styleUrls: ['./risk-chart.component.scss']
})
export class RiskChartComponent implements OnInit, AfterViewInit, OnDestroy {
  private subscription: ISubscription;

  @Input()
  viewMode: ViewMode;

  @Input()
  isARM: boolean;

  @ViewChild('riskChart') riskChart: ElementRef<HTMLDivElement>;

  totalWidth: number = 473;
  sideBarWidth: number = 220;
  maxWidth: number = (document.body.offsetWidth - this.sideBarWidth ) / 3;

  localLoanObject: loan_model;

  public info = {
    riskCushionAmount: 0,
    riskCushionPercent: '',
    returnPercent: '',
    // TODO: Replace with real value for black and red diamond
    blackDiamond: 0,
    redDiamond: 0,
    armCommitment: 0,
    armInfo: '',
    startBlack: -200,
    startRed: -200
  };

  currencyFormatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 1
  });

  riskScale = [0, 2.5, 5.0, 7.5, 10.0];
  returnScale = [5.0, 6.4, 7.8, 10.8, 13.7];
  actualScale = [5, 95, 185, 275, 365];

  private _refData: RefDataModel;

  private refSub: ISubscription;
  private scaleSub: ISubscription;
  private resizeSubscription: ISubscription;
  private layoutSubscription: ISubscription;

  constructor(
    private localStorageService: LocalStorageService,
    private dataService: DataService,
    private refDataService: RefDataService,
    private layout: LayoutService
  ) {}

  ngOnInit() {
    this.localLoanObject = this.localStorageService.retrieve(
      environment.loankey
    );

    this._refData = this.localStorageService.retrieve(environment.referencedatakey);
    this.refSub = this.localStorageService.observe(environment.referencedatakey).subscribe(res => {
      this._refData = res;
    });

    this.setScale();

    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res) {
          this.localLoanObject = res;
          if (this.localLoanObject && this.localLoanObject.LoanMaster) {
            this.adjustScale();
          }
        }
      });

    this.resizeSubscription = fromEvent(window, 'resize').subscribe(a => {
      this.adjustScale();
    });

    this.layoutSubscription = this.layout.isSidebarExpanded().subscribe(res => {
      if(res) {
        this.sideBarWidth = 220;
      } else {
        this.sideBarWidth = 80;
      }
      this.adjustScale();
    });
  }

  private setScale() {
    this.returnScale = this.refDataService.getReturnScale(this.localLoanObject.LoanMaster.FC_Return_Scale || 0);
    this.riskScale = this.refDataService.getRiskScale();

    this.scaleSub = this.refDataService.returnScaleUpdated.subscribe(newscale => {
      this.returnScale = newscale;
      this.adjustScale();
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.scaleSub) {
      this.scaleSub.unsubscribe();
    }

    if(this.refSub){
      this.refSub.unsubscribe();
    }

    if(this.resizeSubscription) {
      this.resizeSubscription.unsubscribe();
    }

    if(this.layoutSubscription) {
      this.layoutSubscription.unsubscribe();
    }
  }

  ngAfterViewInit() {
    if (this.localLoanObject && this.localLoanObject.LoanMaster) {
      this.adjustScale();
    }
  }

  private setBars() {
    let step = (this.totalWidth - 10) / 4;
    this.setChart('#risk', this.actualScale[0], this.actualScale[1], chartSettings.riskAndReturns.riskBg, step);
    this.setChart('#cushion', this.actualScale[1], this.actualScale[2], chartSettings.riskAndReturns.cushionBg, step);
    this.setChart('#returnFirst', this.actualScale[2], this.actualScale[3], chartSettings.riskAndReturns.returnLightGreen, step);
    this.setChart('#returnSecond', this.actualScale[3], this.actualScale[4], chartSettings.riskAndReturns.returnDarkGreen, step);
  }

  getRiskReturnValuesFromLocalStorage(loanMaster: LoanMaster) {
    this.info.riskCushionAmount =
      loanMaster.Risk_Cushion_Amount && loanMaster.Risk_Cushion_Amount !== 0
        ? loanMaster.Risk_Cushion_Amount
        : 0;

    this.info.riskCushionPercent = loanMaster.Risk_Cushion_Percent
      ? loanMaster.Risk_Cushion_Percent.toFixed(1) + '%'
      : '0.0%';

    this.info.returnPercent = loanMaster.Return_Percent
      ? loanMaster.Return_Percent.toFixed(1) + '%'
      : '0.0%';

    this.info.armCommitment = loanMaster.ARM_Commitment
      ? loanMaster.ARM_Commitment
      : 0;

    this.info.armInfo = `${loanMaster.Loan_Officer_Name} ${loanMaster.Office_Name}; ARM Cleveland, MS`;

    if(this._refData) {
      let office = this._refData.OfficeList.find(a => a.Office_ID == loanMaster.Office_ID);
      if(office) {
        this.info.armInfo = `${loanMaster.Loan_Officer_Name || ''}; ${loanMaster.Office_Name}, ${office.State}`;
      } else {
        this.info.armInfo = `${loanMaster.Loan_Officer_Name || ''}; ${loanMaster.Office_Name}`;
      }
    }

    this.info.blackDiamond = ScaleHelper.getPosition(
      this.returnScale,
      this.actualScale,
      parseFloat(this.info.returnPercent)
    );

    // this.info.blackDiamond = Math.min(this.info.blackDiamond, this.totalWidth, this.maxWidth);

    this.info.redDiamond = ScaleHelper.getPosition(
      this.riskScale,
      this.actualScale,
      parseFloat(this.info.riskCushionPercent)
    );

    // this.info.redDiamond = Math.min(this.info.redDiamond, this.totalWidth, this.maxWidth);

    // ILogger.log({
    //   black: this.info.blackDiamond,
    //   red: this.info.redDiamond,
    //   totalWidth: this.totalWidth,
    //   max: this.maxWidth,
    //   chartWidth: this.riskChart.nativeElement.clientWidth
    // });
  }

  setChart(item, rangeStart, rangeEnd, color, step) {
    let linearScale = d3
      .scaleLinear()
      .domain([0, 1])
      .range([rangeStart, rangeEnd]);

    let myData = d3.range(0, 1);

    d3.select(item).selectAll('*').remove();

    d3.select(item)
      .selectAll('rect')
      .data(myData)
      .enter()
      .append('rect')
      .attr('x', function(d) {
        return linearScale(d);
      })
      .attr('transform', function(d, i) {
        return 'translate(0, 10)';
      })
      .attr('width', step)
      .attr('height', 15)
      .attr('fill', color);
  }

  setRiskAndReturnScale() {
    let symbolGenerator = d3.symbol().size(30);

    let symbolTypes = ['symbolDiamond'];

    let xScale = d3.scaleLinear().range([0, 400]);

    d3.select('#blackDiamond').selectAll('*').remove();

    d3.select('#blackDiamond')
      .html(null)
      .selectAll('path')
      .data(symbolTypes)
      .enter()
      .append('path')
      .attr('transform', (d, i) => {
        return 'translate(' + this.info.startBlack + ', 30)';
      })
      .transition()
      .duration(1500)
      .ease(d3.easeLinear)
      .attr('transform', (d, i) => {
        return 'translate(' + this.info.blackDiamond + ', 30)';
      })
      .attr('d', function(d) {
        symbolGenerator.type(d3[d]);

        return symbolGenerator();
      })
      .attr('fill', chartSettings.riskAndReturns.blackDiamond);

    this.info.startBlack = this.info.blackDiamond;

    d3.select('#redDiamond').selectAll('*').remove();

    d3.select('#redDiamond')
      .html(null)
      .selectAll('path')
      .data(symbolTypes)
      .enter()
      .append('path')
      .attr('transform', (d, i) => {
        return 'translate(' + this.info.startRed + ', 5)';
      })
      .transition()
      .duration(1750)
      .attr('transform', (d, i) => {
        return 'translate(' + this.info.redDiamond + ', 5)';
      })
      .attr('d', function(d) {
        symbolGenerator.type(d3[d]);

        return symbolGenerator();
      })
      .attr('fill', chartSettings.riskAndReturns.redDiamond);

    this.info.startRed = this.info.redDiamond;
  }

  adjustScale() {
    const self = this;
    self.maxWidth = ((document.body.clientWidth - this.sideBarWidth ) / 3) - 10;

    setTimeout(() => {
      if(self.riskChart) {
        // ILogger.time('Risk chart - adjust scale');

        self.totalWidth = Math.min(self.riskChart.nativeElement.offsetWidth, self.maxWidth);
        self.totalWidth = Math.max(self.totalWidth, 334);

        self.setActualScale();
        self.getRiskReturnValuesFromLocalStorage(self.localLoanObject.LoanMaster);
        self.setBars();
        self.setRiskAndReturnScale();

        // ILogger.timeEnd('Risk chart - adjust scale');
      }
    }, 500);
  }

  setActualScale() {
    let step = (this.totalWidth - 10) / 4;
    this.actualScale = [
      5,
      1 * step + 5,
      2 * step + 5,
      3 * step + 5,
      4 * step + 5
    ];
  }
}
