import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from "ngx-toastr";
import { environment } from './../../environments/environment.prod';
import { deserialize, serialize } from 'serializer.ts/Serializer';
import { JsonConvert } from 'json2typescript';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor(
  	private toaster: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private localstorageservice:LocalStorageService,
    ) { }

  ngOnInit() {
    let obj=this.localstorageservice.retrieve(environment.loankey);
    // this.router.navigateByUrl("/home/admin/namingconvention");
  }

  gotonamingconvention(){
    // this.router.navigateByUrl("/home/admin/namingconvention");
  }

}
