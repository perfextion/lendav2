import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { CropListComponent } from './crop-list/crop-list.component';

// import { NamingConventionComponent } from './naming-convention/naming-convention.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'crop-list',
        component: CropListComponent
      },
      {
        path: '',
        redirectTo: 'crop-list'
      }
    ]
  },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }