import { Component, OnInit, Inject } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from './../../../environments/environment';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { CropsService } from './../../services/admin/crops.service';

@Component({
  selector: 'app-crop-list',
  templateUrl: './crop-list.component.html',
  styleUrls: ['./crop-list.component.scss']
})
export class CropListComponent implements OnInit {
  public p: number = 1;
  public cropList: any;
  private selectedCrop: any;

  constructor(private localStorageService: LocalStorageService,
          private cropsService: CropsService,
          private dialog: MatDialog) { }

  ngOnInit() {
    this.cropsService.getAllCropList().subscribe((response) => {
      this.cropList = response.Data;
    });
  }

  editCrop(crop) {
  	this.selectedCrop = JSON.parse(JSON.stringify(crop)); // copy of crop object

    const dialogRef = this.dialog.open(CropListEditDialog, {
      width: '250px',
      data: {Price: this.selectedCrop.Price, Final_Planting_Date: this.selectedCrop.Final_Planting_Date}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.selectedCrop.Price = result.Price;
        this.selectedCrop.Final_Planting_Date = result.Final_Planting_Date;
        this.updateCrop();
      }
    });
  }

  updateCrop() {

    const data = {
      Crop_And_Practice_ID: this.selectedCrop.Crop_And_Practice_ID,
      Price: this.selectedCrop.Price,
      Final_Planting_Date: this.selectedCrop.Final_Planting_Date
    }

    this.cropsService.updateCrop(data).subscribe((response) => {
      if(response.ResCode === 0) {
        const crop = this.cropList.find(c => c.Crop_And_Practice_ID === this.selectedCrop.Crop_And_Practice_ID);
        crop.Price = response.Data.Price;
        crop.Final_Planting_Date = response.Data.Final_Planting_Date;
      }
    });
  }

  deleteCrop(crop) {
    this.cropsService.deleteCrop(crop.Crop_And_Practice_ID).subscribe((response) => {
      const index = this.cropList.indexOf(crop);
      this.cropList.splice(index, 1);
    });
  }
}

@Component({
  selector: 'crop-list-edit-dialog',
  templateUrl: 'crop-list-edit-dialog.component.html',
  styleUrls: ['./crop-list.component.scss']
})
export class CropListEditDialog {

  constructor(
    public dialogRef: MatDialogRef<CropListEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  cancel(): void {
    this.dialogRef.close();
  }

}
