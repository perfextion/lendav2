import { Injectable } from '@angular/core';

import { SettingsService } from '@lenda/preferences/settings/settings.service';

@Injectable()
export class MasterService {
  isMinimize: boolean = false;
  isChevronOpen: boolean = false;

  constructor(private settings: SettingsService) {
    this.setChevronStatus(settings.preferences);

    this.settings.preferencesChange.subscribe(preferences => {
      this.setChevronStatus(preferences);
    });
  }

  setChevronStatus(preferences) {
    try {
      this.isChevronOpen = preferences.userSettings.showTotalsSettings.isChevronOpenState;
    } catch {
      this.isChevronOpen = false;
    }
  }

  minimizeBanner() {
    this.isMinimize = !this.isMinimize;
  }

  toggleChevron() {
    this.isChevronOpen = !this.isChevronOpen;
    this.settings.preferences.userSettings.showTotalsSettings.isChevronOpenState = this.isChevronOpen;
  }
}
