import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { JsonConvert } from 'json2typescript';
import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';

import { environment } from '@env/environment.prod';
import { loan_model, LoanGroup, LoanStatusMapping } from '@lenda/models/loanmodel';
import { Preferences } from '@lenda/preferences/models/index.model';
import { ResponseModel } from '@lenda/models/resmodels/reponse.model';

import { ApiService } from '@lenda/services/api.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoanvalidatorService } from './../services/loanvalidator.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { DataService } from '@lenda/services/data.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { LayoutService } from '../shared/layout/layout.service';
import { SharedService } from "@lenda/services/shared.service";
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { MasterService } from './master.service';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss']
})
export class MasterComponent implements OnInit {
  public isSidebarExpanded: boolean = true;
  private localloanobject: loan_model = new loan_model();

  public bannerMessage: string = '';
  public inactiveMessage: string = 'Loan Status is Inactive and protected.';
  public recomendedMessage: string = 'Loan Status is awaiting Checkout and protected.';
  public uploadedMessage: string = 'Loan is uploaded to Notridge';

  public showDetailMessage: boolean = false;
  public detailMessage: string = '';

  public loanStatus: string = '';
  public lockColor: string = '';
  public validToShowBanner: boolean;

  public loanid: string = "";

  constructor(
    private router: Router,
    public layoutService: LayoutService,
    public sharedService: SharedService,
    private loanService: LoanApiService,
    public localstorageservice: LocalStorageService,
    public toaster: ToastrService,
    private dataService: DataService,
    private settingsService: SettingsService,
    private logging: LoggingService,
    private loanvalidator: LoanvalidatorService,
    public loancalculation: LoancalculationWorker,
    private apiService: ApiService,
    public masterSvc: MasterService
  ) { }

  public get isLoanListPageActive() {
    return location.href.includes('home/loan') && !location.href.includes('loanoverview');
  }

  public get isBannerVisible() {
    return (
    this.loanStatus != 'W' &&
    location.href.includes('loanoverview') &&
    !this.masterSvc.isMinimize
    );
  }

  ngOnInit() {
    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);

    this.sharedService.isLoanList$.subscribe((value) => {
      this.checkLoanStatus();
    });

    this.dataService.getLoanObject().subscribe(res=>{
      this.localloanobject = res;
      this.checkLoanStatus();
    });

    this.layoutService.isSidebarExpanded().subscribe((value) => {
      this.isSidebarExpanded = value;
    });
  }

  minimizeBanner() {
    this.masterSvc.minimizeBanner();
  }

  isUsedByAnotherUser() {
    if(this.loanStatus) {
      return this.loanStatus.includes('_R');
    }
    return false;
  }

  checkLoanStatus() {
    try {
      if(!_.isEmpty(this.localloanobject)) {
        this.loanStatus = this.localloanobject.LoanMaster.Loan_Status.toUpperCase();

        if(this.loanStatus == 'R' || this.loanStatus == 'A') {
          this.bannerMessage = this.recomendedMessage;
          this.lockColor = 'recommended-lock';
        } else if(this.loanStatus == 'U') {
          this.bannerMessage = this.uploadedMessage;
          this.lockColor = 'recommended-lock';
        } else if(this.loanStatus.includes('_R')) {
          let status = this.loanStatus.split('_')[0];
          this.bannerMessage = 'Loan Status is ' + LoanStatusMapping[status]  + '; Loan is currently being Edited by ' + this.localloanobject.LoanMaster.Active_User_Name;
          this.lockColor = 'lock';
        } else {
          this.bannerMessage = this.inactiveMessage;
          this.lockColor = 'inactive-lock';
        }
      }
    } catch (error) {
      if (environment.isDebugModeActive) console.log('Error checking loan status');
    }
  }

  bannerAction() {
    if(this.loanStatus == 'R' || this.loanStatus == 'A' || this.loanStatus == 'U') {
      this.rescindLoan();
    } else {
      this.reviceCopyLoan();
    }
  }

  takeControl() {
    this.loanService.takeControl(this.localloanobject.Loan_Full_ID).subscribe(res => {
      if(res.ResCode == 1) {
        this.loancalculation.performcalculationonloanobject(res.Data);
      } else {
        this.toaster.error(res.Message);
      }
    }, error => {
      this.toaster.error('Something error occured.');
    });
  }

  readonly() {
    this.masterSvc.minimizeBanner();
  }

  reviceCopyLoan() {
    this.showDetailMessage = this.showDetailMessage ? false : true;
    this.detailMessage = "Loan Status is " + this.translateLoanStatus() + ", use gear option, Revive/Copy, to create a new working version";
    // this.alertify
    //   .confirm('Confirm', 'Do you Really Want to Revive/Copy this loan?')
    //   .subscribe(res => {
    //     if (res == true) {
    //       // REVIVE API
    //       if (this.localloanobject) {
    //         const body = <ChangeStatusModel>{
    //           FromLoanFullID: this.localloanobject.Loan_Full_ID,
    //           ToStatus: LoanStatusChange.ReviveOrCopy
    //         };

    //         this.loanService.changeStatus(body).subscribe(
    //           res => {
    //             this.navigatetoloan(res.Data);
    //           },
    //           error => {
    //             this.toaster.error('Revive/Copy Loan Failed.');
    //           }
    //         );
    //       }
    //     }
    //   });
  }

  navigatetoloan(loanID: string) {
    let id = loanID.replace("/", "-");
    this.loanid = id
    this.localstorageservice.store(environment.loanidkey, id);
    // Get default landing page from my preferences
    let preferences: Preferences = this.settingsService.preferences;
    //get loan here now
    let obj = this.localstorageservice.retrieve(environment.loankey);
    if ((obj == null || obj == undefined)) {
      this.localstorageservice.store(environment.loanGroup, []); //reset loan group if loan to be opened is not exist in local storage
      this.getLoanBasicDetailsFromFavList(preferences);
    }
    else {
      if (obj.Loan_Full_ID != this.loanid) {
        this.localstorageservice.store(environment.loanGroup, []);//reset loan group if loan to be opened is not exist in local storage
        this.getLoanBasicDetailsFromFavList(preferences);
      }
      else {
        this.localstorageservice.store(environment.currentpage, preferences.userSettings.landingPage);
        this.router.navigateByUrl("/home/loanoverview/"
          + id.replace("-", "/") + '/'
          + preferences.userSettings.landingPage);
      }
    }
  }

  getLoanBasicDetailsFromFavList(preferences: Preferences) {
    if (this.loanid != null) {
      let loaded = false;
      try {
        this.localstorageservice.clear(environment.loankey);
        this.localstorageservice.clear(environment.loanGroup);
      } catch (e) {
        if (environment.isDebugModeActive) console.log(`trying to clear a non existing local storage key : ${environment.loankey}`);
      }
      this.loanService.getLoanById(this.loanid.replace("/", "-")).subscribe(res => {
        this.masterSvc.isMinimize = false;
        this.logging.checkandcreatelog(3, 'Overview', "APi LOAN GET with Response " + res.ResCode);
        if (res.ResCode == 1) {

          this.loanvalidator.validateloanobject(res.Data).then(errors => {
            if (errors && errors.length > 0) {

              this.toaster.error("Loan Data is invalid ... Rolling Back to list")
              this.router.navigateByUrl("/home/loans");


            } else {
              let jsonConvert: JsonConvert = new JsonConvert();

              this.loancalculation.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model));
              this.localstorageservice.store(environment.errorbase, []);
              this.loancalculation.saveValidationsToLocalStorage(res.Data);

              //we are making a copy of it also
              this.localstorageservice.store(environment.loankey_copy, res.Data);
              this.localstorageservice.store(environment.currentpage, preferences.userSettings.landingPage);
              this.router.navigateByUrl("/home/loanoverview/"
                + this.loanid.replace("-", "/") + '/'
                + preferences.userSettings.landingPage);

              //loan affiliated loan, currently static
              const route = '/api/Loans/GetLoanGroups?LoanFullID=' + (res.Data as loan_model).Loan_Full_ID;
              this.apiService.get(route).subscribe((res: ResponseModel) => {
                if (res) {
                  let loanGrpups: Array<LoanGroup> = res.Data;
                  this.localstorageservice.store(environment.loanGroup, res.Data);
                }
              });

              this.localstorageservice.store(environment.loanCrossCollateral, []);
              this.loanService.getDistinctLoanIDList(this.loanid).subscribe(res => {
                if(res && res.ResCode == 1) {
                  this.localstorageservice.store(environment.loanCrossCollateral, res.Data);
                }
              });
            }
            this.localstorageservice.store(environment.exceptionStorageKey, []);
            this.localstorageservice.store(environment.modifiedbase, []);
            this.localstorageservice.store(environment.modifiedoptimizerdata, null);
          });
        }
        else {
          this.toaster.error("Could not fetch Loan Object from API")
        }
        loaded = true;
      });

    }
    else {
      this.toaster.error("Something went wrong");
    }
  }

  rescindLoan() {
    this.showDetailMessage = this.showDetailMessage ? false : true;
    this.detailMessage = "Loan Status is " + this.translateLoanStatus() + ", use gear option, Rescind, if nescessary";
    // this.alertify
    //   .confirm('Confirm', 'Do you Really Want to Rescind this loan?')
    //   .subscribe(res => {
    //     if (res == true) {
    //       if (this.localloanobject) {
    //         const body = <ChangeStatusModel>{
    //           FromLoanFullID: this.localloanobject.Loan_Full_ID,
    //           ToStatus: LoanStatusChange.Rescind
    //         };

    //         this.loanService.changeStatus(body).subscribe(
    //           res => {
    //             this.toaster.success('Rescind Loan Successful.');
    //             this.publishService.syncToDb();
    //           },
    //           error => {
    //             this.toaster.error('Rescind Loan Failed.');
    //           }
    //         );
    //       }
    //     }
    //   });
  }

  translateLoanStatus() {
    let status = '';

    switch(this.loanStatus) {
      case 'R':
        status = 'Recommended';
        break;

      case 'A':
        status = 'Approved';
        break;

      case 'U':
        status = 'Uploaded';
        break;

      case 'H':
        status = 'Historical';
        break;

      case 'P':
        status = 'Paid';
        break;

      case 'X':
        status = 'Written Off';
        break;

      case 'V':
        status = 'Voided';
        break;

      case 'Y':
        status = 'Withdrawn';
        break;

      case 'Z':
        status = 'Declined';
        break;

      default: break;
    }

    return status;
  }

}
