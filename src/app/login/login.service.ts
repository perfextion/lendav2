import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';
import { ResponseModel } from '@lenda/models/resmodels/reponse.model';
import { LoginModel } from './login.model';

import { ApiService } from '../services/api.service';

/**
 * Shared service for login
 */
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject(true);

  constructor(
    private localStorageService: LocalStorageService,
    private apiservice: ApiService
  ) {}

  /**
   * Observable for login
   */
  public isLoggedIn(): Observable<boolean> {
    return this.isUserLoggedIn.asObservable();
  }

  public loginbyuserid(body): Observable<any> {
    const route = '/api/user';
    return this.apiservice.post(route, body).map(res => res);
  }

  public loginByUsernameAndPassword(
    body: LoginModel
  ): Observable<ResponseModel> {
    const route = '/api/User/Login';
    return this.apiservice.post(route, body).map(res => res);
  }

  /**
   * This initiates the next login value for the subscribers
   * @param newValue boolean value to describe if login is successful or not
   */
  public login(newValue: boolean): void {
    this.isUserLoggedIn.next(newValue);
  }

  /**
   * Removes the stored local storage data
   * to clear the dirty values (not synced)
   */
  public removeDataFromLocalStorage() {
    this.localStorageService.clear(environment.loankey);
    this.localStorageService.clear(environment.collateralTables);
    this.localStorageService.clear(environment.syncRequiredItems);
    this.localStorageService.clear(environment.loanofficerstats);
    this.localStorageService.clear(environment.myPreferences);
    this.localStorageService.clear(environment.referencedatakey);
    this.localStorageService.clear(environment.loanGroup);
    this.localStorageService.clear(environment.loankey);
    this.localStorageService.clear(environment.loankey_copy);
    this.localStorageService.clear(environment.modifiedoptimizerdata);
    this.localStorageService.clear(environment.modifiedoptimizerdata);
  }

  getLoanOfficerStats(): Observable<ResponseModel> {
    const route = '/api/Loan/LOStats';
    return this.apiservice.get(route).map(res => res);
  }
}
