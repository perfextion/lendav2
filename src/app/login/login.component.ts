import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

import { Preferences } from '@lenda/preferences/models/index.model';
import { environment } from '@env/environment.prod';
import { LoginModel } from './login.model';
import { Logpriority } from '@lenda/models/loanmodel';

import { generate } from '@lenda/Workers/utility/randomgenerator';

import { LoginService } from './login.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { ReferenceService } from '@lenda/services/reference/reference.service';
import { AppsettingsApiService } from '@lenda/services/appsettingsapi.service';
import { LayoutService } from '@lenda/shared/layout/layout.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { AutoUnsubscribe, SubscriptionList } from '@lenda/services/auto-unsubscribe';

@Component({
  selector: 'auth-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
@AutoUnsubscribe({
  subscriptionKey: 'subscriptions'
})
export class LoginComponent implements OnInit {
  isValidForm: boolean = true;
  backgroundClass: string = 'bg1';

  public username: string;
  public password: string;

  isLoginInProgress: boolean;

  private subscriptions = new SubscriptionList();

  constructor(
    private router: Router,
    public toastr: ToastrService,
    public alertifyService: AlertifyService,
    public settingsapiService: AppsettingsApiService,
    public localst: LocalStorageService,
    public sessionst: SessionStorageService,
    public referencedataapi: ReferenceService,
    private loginService: LoginService,
    private layout: LayoutService,
    private loanApiService: LoanApiService,
    private settingsService: SettingsService
  ) {
    this.randomBg();
  }

  ngOnInit() {
    this.loginService.removeDataFromLocalStorage();
    this.loginService.login(false);
  }

  loginByUsernameAndPassword() {
    let body = <LoginModel>{
      Password: this.password,
      Username: this.username
    };

    this.isLoginInProgress = true;

    this.subscriptions.loginByUsernameAndPasswordSub = this.loginService.loginByUsernameAndPassword(body).subscribe(
      res => {
        if (res.ResCode == 1) {
          this.sessionst.store('UID', res.Data);
          this.localst.store(environment.officeid, res.Data.Office_ID);

          this.layout.toggleRightSidebar(false);

          this.localst.store(environment.logpriority, Logpriority.High);
          this.localst.store(environment.uid, res.Data.UserID);
          this.localst.store(
            environment.localStorage.userRole,
            res.Data.User_Type_ID
          );
          this.localst.store(environment.usersession, generate());
          this.localst.store(environment.errorbase, []);
          this.localst.store(environment.ghostUserDetails, null);
          this.localst.store(environment.exceptionStorageKey, []);
          this.localst.store(environment.modifiedbase, []);
          this.localst.store('loggedinuser', res.Data.FirstName);
          this.localst.store('loggedinusername', res.Data.Username);
          this.localst.store('isAdmin', res.Data.IsAdmin);
          this.localst.store('isloggedin', true);

          this.getLOStats();
          this.getreferencedata(res.Data.FirstName, res.Data.UserID);
          this.getMasterFarmerList();
          this.getMasterBorrowerList();
        } else {
          this.toastr.error(res.Message);
          this.isLoginInProgress = false;
        }
      },
      error => {
        this.toastr.error(error.ExceptionMessage);
        this.isLoginInProgress = false;
      }
    );
  }

  private getreferencedata(firstname: string, userId) {
    this.subscriptions.getreferencedataSub = this.referencedataapi.getreferencedata().subscribe(referencedata => {
      this.setDebugLevels(referencedata.Data);
      this.getMypreferences(firstname, referencedata.Data, userId);
    });
  }

  private setDebugLevels(referencedata: RefDataModel) {
    let frontEndDebugLevel = referencedata.ARM_Defaults.find(a => a.Default_Value_Type == 'FRONTEND_DEBUG_LEVEL');
    if(frontEndDebugLevel) {
      this.localst.store(environment.frontEndDebugLevel, frontEndDebugLevel.ARM_Default_Value);
    } else {
      this.localst.store(environment.frontEndDebugLevel, 2);
    }

    let backendDebugLevel = referencedata.ARM_Defaults.find(a => a.Default_Value_Type == 'BACKEND_DEBUG_LEVEL');
    if(backendDebugLevel) {
      this.localst.store(environment.backendDebugLevel, backendDebugLevel.ARM_Default_Value);
    } else {
      this.localst.store(environment.backendDebugLevel, 2);
    }
  }

  private getLOStats() {
    this.subscriptions.getLoanOfficerStatsSub = this.loginService.getLoanOfficerStats().subscribe(res => {
      if (res.ResCode == 1) {
        this.localst.store(environment.loanofficerstats, res.Data);
      }
    });
  }

  private getMasterFarmerList() {
    this.subscriptions.getFarmerListSub = this.loanApiService.getFarmerList().subscribe(res => {
      if (res.ResCode == 1) {
        this.localst.store(environment.masterfarmerlist, res.Data);
      } else {
        this.localst.store(environment.masterfarmerlist, []);
      }
    });
  }

  private getMasterBorrowerList() {
    this.subscriptions.getBorrowerListSub = this.loanApiService.getBorrowerList().subscribe(res => {
      if (res.ResCode == 1) {
        this.localst.store(environment.masterborrowerlist, res.Data);
      } else {
        this.localst.store(environment.masterborrowerlist, []);
      }
    });
  }

  private getMypreferences(firstname: string, referencedata, userId) {
    this.subscriptions.getMyPreferencesSub = this.settingsapiService.getMyPreferences().subscribe(res => {
      let obj: Preferences = <Preferences>{};

      if (res.Data != '') {
        obj = JSON.parse(res.Data);
      }

      referencedata[environment.favorites + userId] = obj.favouriteLoanList || [];
      this.localst.store(environment.referencedatakey, referencedata);

      this.settingsService.setPreferences(obj as any);
      this.localst.store(environment.myPreferences, obj);

      this.loginService.login(true);
      this.router.navigateByUrl('/home/loans');
      this.toastr.success('Welcome ' + firstname);
    });
  }

  /**
   * Randomly generates which background is to be displayed
   */
  private randomBg() {
    this.backgroundClass = `bg${Math.floor(Math.random() * 4) + 1}`;
  }
}


