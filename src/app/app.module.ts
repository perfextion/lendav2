import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';
import { MomentModule } from 'angular2-moment';
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatPaginatorIntl
} from '@angular/material';

import 'reflect-metadata';

import { ToastrModule } from 'ngx-toastr';
import { ChartsModule } from 'ng2-charts';
import { BarRatingModule } from 'ngx-bar-rating';
import { NgSelectModule } from '@ng-select/ng-select';
import { ClickOutsideModule } from 'ng-click-outside';
import { NgxMaskModule } from 'ngx-mask';
import { NgxCurrencyModule } from 'ngx-currency';
import { Ng5SliderModule } from 'ng5-slider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';
import { FileSaverModule } from 'ngx-filesaver';
import { LoadingBarHttpModule } from '@ngx-loading-bar/http';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

import { SharedModule } from './shared/shared.module';
import { UiComponentsModule } from './ui-components/ui-components.module';
import { MaterialModule } from './shared/material.module';

// START PIPES
import { CommitteStatusIconPipe } from './pipes/committe-status-icon.pipe';
import { SearchFilterPipe } from './pipes/search-filter.pipe';
//END PIPES

//START SERVICES

import { DeactivateGuard } from './services/route/deactivate.guard';

import { LoanApiService } from './services/loan/loanapi.service';
import { UserApiService } from './services/user/user.service';
import { GlobalService, ApiService } from './services';
//END SERVICES

//START COMPONENTS
import { AppComponent } from './app.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { HeaderComponent } from './shared/layout/header.component';
import { FooterComponent } from './shared/layout/footer.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MasterComponent } from './master/master.component';
import { LoanListComponent } from './components/loan-list/loan-list.component';
import { LoanOverviewComponent } from './components/loan-overview/loan-overview.component';
import { Borrowercalculationworker } from './Workers/calculations/borrowercalculationworker.service';
import { Borrowerincomehistoryworker } from './Workers/calculations/borrowerincomehistoryworker.service';
import { LoancalculationWorker } from './Workers/calculations/loancalculationworker';
import { BorrowerComponent } from './components/borrower/borrower.component';
import { BalancesheetComponent } from './components/borrower/balancesheet/balancesheet.component';
// import { NamingConventionComponent } from './components/naming-convention/naming-convention.component';
import { BorrowerapiService } from './services/borrower/borrowerapi.service';
import { LoggingService } from './services/Logs/logging.service';
import { LoancropunitcalculationworkerService } from './Workers/calculations/loancropunitcalculationworker.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';

import { NamingConventionapiService } from './services/admin/namingconventionapi.service';
import { AgGridModule } from 'ag-grid-angular';
import { NumericEditor } from '../app/aggridfilters/numericaggrid';
import { AggridTxtAreaComponent } from './aggridfilters/textarea';
import { LoadingModule } from 'ngx-loading';
import { LicenseManager } from 'ag-grid-enterprise/main';
import { SidebarModule } from 'ng-sidebar';

import { LoancrophistoryService } from './Workers/calculations/loancrophistory.service';
import { FarmComponent } from './components/farm/farm.component';
import { FarmcalculationworkerService } from './Workers/calculations/farmcalculationworker.service';
import { FarmapiService } from './services/farm/farmapi.service';
import { AutoFocusDirective } from './directives/auto-focus.directive';
import { BudgetComponent } from './components/budget/budget.component';
import { ReferenceService } from './services/reference/reference.service';
import { SelectEditor } from './aggridfilters/selectbox';
import { InsuranceComponent } from './components/insurance/insurance.component';
import { AssociationComponent } from './components/insurance/association/association.component';
import { DeleteButtonRenderer } from './aggridcolumns/deletebuttoncolumn';
import { InsuranceapiService } from './services/insurance/insuranceapi.service';
import { FarmsInfoComponent } from './components/insurance/farms-info/farms-info.component';
import { CropYieldInfoComponent } from './components/insurance/crop-yield-info/crop-yield-info.component';
import { LoanCropUnitsInfoComponent } from './components/insurance/loan-crop-units-info/loan-crop-units-info.component';
import { FarmerInfoComponent } from './components/borrower/farmer-info/farmer-info.component';
import {
  BorrowerInfoComponent,
  CoBorrowerDialogComponent
} from './components/borrower/borrower-info/borrower-info.component';
import { QuestionsComponent } from './components/borrower/questions/questions.component';
import { LoanviewerComponent } from './components/loanviewer/loanviewer.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { LoanbudgetComponent } from './components/budget/loan-budget/loanbudget.component';
import { EmailEditor } from './Workers/utility/aggrid/emailboxes';
import { NotificationFeedsService } from './shared/notification-feeds/notification-feeds.service';
import { NotificationFeedsComponent } from './shared/notification-feeds/notification-feeds.component';
import { RightSidebarComponent } from './shared/layout/right-sidebar/right-sidebar.component';
import { CommentTypeComponent } from './shared/layout/right-sidebar/comment-type.component';
import { MediaArticleComponent } from './shared/layout/right-sidebar/media-article/media-article.component';
import { UserArticleComponent } from './shared/layout/right-sidebar/user-article/user-article.component';
import { CreateLoanComponent } from './components/create-loan/create-loan.component';
import { SpinerComponent } from './shared/spiner/spiner.component';
import { BorrowerIncomeHistoryComponent } from './components/borrower/borrower-income-history/borrower-income-history.component';
import { FormatComponent } from './components/format/format.component';
//CROP
import { CropapiService } from './services/crop/cropapi.service';

import { CropComponent } from './components/crop/crop.component';
import { PriceComponent } from './components/crop/price/price.component';
import { YieldComponent } from './components/crop/yield/yield.component';
import { YieldDialogComponent } from './components/crop/yield/yield.component';

//Collateral
import { CollateralComponent } from './components/collateral/collateral.component';
import { FSAComponent } from './components/collateral/fsa/fsa.component';
import { CrossCollateralDetailComponent } from './components/collateral/cross-collateral/cross-collateral.component';
import { Collateralcalculationworker } from './Workers/calculations/collateralcalculationworker.service';
import { RatingComponent } from './components/borrower/farm-financial/rating/rating.component';
import { FarmFinancialComponent } from './components/borrower/farm-financial/farm-financial.component';
import { LoanMasterCalculationWorkerService } from './Workers/calculations/loan-master-calculation-worker.service';
import { CellValueComponent } from './components/borrower/shared/cell-value/cell-value.component';
import { QuestionscalculationworkerService } from './Workers/calculations/questionscalculationworker.service';
import { CurrencyDirective } from './components/borrower/shared/currency.directive';
import { PercentageDirective } from './components/borrower/shared/percentage.directive';
import { LoancroppracticeworkerService } from './Workers/calculations/loancroppracticeworker.service';
import { InsurancecalculationworkerService } from './Workers/calculations/insurancecalculationworker.service';
import { AssociationcalculationworkerService } from './Workers/calculations/associationcalculationworker.service';

import { OptimizerComponent } from './components/optimizer/optimizer.component';
import { PoliciesComponent } from './components/insurance/policies/policies.component';
import { ChipsListEditor } from './aggridcolumns/chipscelleditor';
import { EmptyEditor } from './aggridfilters/emptybox';
import { BudgetHelperService } from './components/budget/budget-helper.service';
import { OverallCalculationServiceService } from './Workers/calculations/overall-calculation-service.service';
import { MarketingContractsComponent } from './components/collateral/marketing-contracts/marketing-contracts.component';
import { MarketingcontractcalculationService } from './Workers/calculations/marketingcontractcalculation.service';
import { OptimizercalculationService } from './Workers/calculations/optimizercalculationservice.service';
import { AphComponent } from './components/insurance/aph/aph.component';
import { GlobalErrorHandler } from './services/global-error-handler.service';
import { ToasterService } from './services/toaster.service';

//RECORDS
import { AgGridTooltipComponent } from './aggridcolumns/tooltip/tooltip.component';
import { BooleanEditor } from './aggridfilters/booleanaggrid.';
import { BorrowerInfoFormComponent } from './components/borrower/borrower-info/borrower-info-form/borrower-info-form.component';
import { StyleguideComponent } from './styleguide/styleguide.component';
import { DevPracticesComponent } from './styleguide/dev-practices/dev-practices.component';
import { AgGridComponentsComponent } from './styleguide/ag-grid-components/ag-grid-components.component';
import { BlankCellRenderer } from './aggridcolumns/tooltip/aggridrenderers';
import { NamingConventionComponent } from './admin/naming-convention/naming-convention.component';

import { AppRoutingModule } from './app-routing.module';
import { AlertifyModule } from './alertify/alertify.module';
import { CheckoutModule } from './components/checkout/checkout.module';
import { CommitteeModule } from './components/committee/committee.module';
import { DebugModule } from './components/debug/debug.module';
import { FlowChartModule } from './components/flowchart/flowchart.module';
import { PreferencesModule } from './preferences/preferences.module';
import { ReportsModule } from './components/reports/reports.module';

import { LoanHistoryComponent } from './components/loan-history/loan-history.component';
import { ViewBalancesComponent } from './components/viewbalances/viewbalances.component';
import { WfrpComponent } from './components/insurance/wfrp/wfrp.component';
import { FsaService } from './components/collateral/fsa/fsa.service';
import { FixedRevenueComponent } from './components/budget/fixed-revenue/fixed-revenue.component';
import { LoanvalidatorService } from '@lenda/services/loanvalidator.service';
import { VoteButtonRenderer } from '@lenda/aggridcolumns/votebuttoncolumn';
import { DataService } from '@lenda/services/data.service';
import { ClosingComponent } from '@lenda/components/closing/closing.component';

// Comment Component
import { CommentsComponent } from './shared/layout/right-sidebar/comments/comments.component';

import { AppsettingsApiService } from '@lenda/services/appsettingsapi.service';
import { CheckboxCellComponent } from '@lenda/aggridfilters/checkbox';
import { AutocompleteCellEditor } from '@lenda/aggridcolumns/autocomplete-cell-editor/autocomplete-cell-editor';
import { RadioboxCellEditor } from '@lenda/aggridcolumns/radiobox-cell-editor/radiobox-cell-editor';

import { ValidateRangeValidatorDirective } from './directives/validate-range.directive';
import { ValidateDateRangeValidatorDirective } from './directives/validate-date.directive';
import { ValidationErrorsCountService } from './Workers/calculations/validationerrorscount.service';
import { QuestionlistComponent } from './components/insurance/questionlist/questionlist.component';
import { LoanConditionWorkerService } from './Workers/calculations/loanconditionworker.service';
import { LoanCalculationSchematicsStorageService } from '@lenda/Workers/calculations/loan-calculation-schematics.service';
import { wfrpworker } from './Workers/calculations/wfrpworker.service';
import { CollateralReportSliderComponent } from './components/collateral/collateral-report/collateral-report.component';
import { CollateralService } from './components/collateral/collateral.service';
import { SvgDefinitionsomponent } from './shared/svg/svg-definitions.component';
import { BudgetpublisherService } from './services/budget/budgetpublisher.service';
import { VoteButtonColumnRenderer } from './aggridcolumns/vote-button-column/vote-button-column.component';
import { DisburseComponent } from './components/disburse/disburse.component';
import { SettingButtonRenderer } from './aggridcolumns/settingsbuttonrender';
import { DisbursepopupComponent } from './aggridcolumns/disbursepopup/disbursepopup.component';
import { DisburseapiService } from './services/disburse/disburseapi.service';
import { DisbursedetailsComponent } from './aggridcolumns/disbursedetails/disbursedetails.component';
import { DisbursecommentComponent } from './aggridcolumns/disbursecomment/disbursecomment.component';
import { DatetTimeStampService } from './services/datet-time-stamp.service';
import { OptimizerverifyComponent } from './components/optimizer/optimizerverify/optimizerverify.component';
import { verifyPrimaryHeader } from './aggridcolumns/headerComponents/verifyprimaryHeader';
import { LoanAuditTrailService } from './services/audit-trail.service';
import { LoanTypeCalculationService } from './services/loan-type.service';
import { LoanToleranceCalculationService } from './Workers/calculations/loantolerancecalculatonworker.service';
import { OptimizerVerifyService } from './components/optimizer/optimizer.verify.service';
import { BudgetAutoCompleteCellEditor } from './aggridcolumns/budget-auto-complete-cell/budget-auto-complete-cell.component';
import { ManageFarmOverrideDialogComponent } from './components/farm/farm-override/manage-farm-override-dialog.component';
import { BudgetcalculationworkerService } from './Workers/calculations/budgetcalculationworker.service';
import { RefDataService } from './services/ref-data.service';
import { TermsCalculationWorkerService } from './Workers/calculations/termscalculationworker.service';
import { LoanDetailComponent } from './components/collateral/loan-detail/loan-detail.component';
import { DeleteNotifyPartyButtonRenderer } from './aggridcolumns/delete-notify-party/delete-notify-party-column';
import { verifyPrimaryHeadergroup } from './aggridcolumns/headerComponents/verifyprimaryHeadergroup';
import { verifyPrimaryHeaderAPH } from './aggridcolumns/headerComponents/verifyprimaryHeader.aph';
import { InsuranceverifyComponent } from './components/insurance/policies/insurance.verify.popup/insuranceverify.component';
import { ExportExcelService } from './services/export-excel.service';
import { HyperfieldValidationWorkerService } from './Workers/calculations/hyperfield-validation.service';
import { MasterService } from './master/master.service';
import { ExceptionWorkerService } from './Workers/calculations/exceptionworker.service';
import { LoanDocumentWorkerService } from './Workers/calculations/loandocumentworker.service';
import { CropPriceWorkerService } from './Workers/calculations/croppriceworker.service';
import { OtherIncomeComponent } from './components/crop/other-income/other-income.component';
import { OtherIncomeAutocompleteCellEditor } from './aggridcolumns/other-income-autocomplete/other-income-autocomplete.component';
import { QuicksyncResolver } from './routeresolvers/quicksyncResolver';
import { CropAutoCompleteEditor } from './aggridcolumns/crop-auto-complete-editor/crop-auto-complete-editor.component';
import { RefVCAFieldsWorkerService } from './Workers/calculations/refvcafieldsworker.service';
import { PrincialCheckboxCellRenderer } from './aggridcolumns/principal-checkbox-celleditor';
import { LoanSettingsService } from './preferences/settings/helpers/loan-settings.service';
import { CollateralReportAggregateComponent } from './components/collateral/collateral-report-aggregate/collateral-report-aggregate.component';
import { CreditAgreementFieldsComponent } from './components/collateral/credit-agreement-fields/credit-agreement-fields.component';
import { CheckLoanSyncService } from './services/loan/check-loan-sync.service';
import { LoanOverviewActivateGuard } from './components/loan-overview/loan-overview.activate.guard';
import { LoanOverviewDeactivateGuard } from './components/loan-overview/loan-overview.deactivate.guard';
import { DisburseConditions } from './components/disburse/disburse.condtions';
import { DisburseAutoCompleteEditor } from './aggridcolumns/Disburse-auto-complete-editor/disburse-auto-complete-editor.component';
import { CheckboxCellRenderer } from './aggridcolumns/checkbox-cell-editor/checkbox-cell-rendere';
import { LoanRxCalculationWorker } from './Workers/calculations/loanrxworker.service';
import { LoanRxDebugComponent } from './components/debug/loan-rx-debug/loan-rx-debug.component';
import { BorrowerSignerComponent } from './components/borrower/borrower-signer/borrower-signer.component';
import { UrlRResolver } from './routeresolvers/url.resolver';
import { SharedService } from './services/shared.service';
import { BorrowerNamePipe } from './pipes/borrower-name.pipe';
import { DateCellEditor } from './aggridcolumns/date-cell-editor/date-cell-editor.component';
import { FooterColumnComponent } from './components/loan-list/footer-column/footer-column.component';

LicenseManager.setLicenseKey(
  'MTUzNjQ0NzYwMDAwMA==712c48d48d0a3ec85f3243b1295999ec'
);

@NgModule({
  declarations: [
    NumericEditor,
    BooleanEditor,
    CheckboxCellComponent,
    PrincialCheckboxCellRenderer,
    SelectEditor,
    EmptyEditor,
    ChipsListEditor,
    AutocompleteCellEditor,
    DisburseAutoCompleteEditor,
    RadioboxCellEditor,
    AppComponent,
    SpinerComponent,
    DeleteButtonRenderer,
    CheckboxCellRenderer,
    DeleteNotifyPartyButtonRenderer,
    VoteButtonRenderer,
    BlankCellRenderer,
    LoginComponent,
    PageNotFoundComponent,
    NamingConventionComponent,
    DashboardComponent,
    MasterComponent,
    EmailEditor,
    FooterComponent,
    HeaderComponent,
    LoanListComponent,
    BorrowerNamePipe,
    LoanOverviewComponent,
    BorrowerComponent,
    verifyPrimaryHeader,
    verifyPrimaryHeadergroup,
    verifyPrimaryHeaderAPH,
    BalancesheetComponent,
    CropComponent,
    PriceComponent,
    YieldComponent,
    YieldDialogComponent,
    CoBorrowerDialogComponent,
    AggridTxtAreaComponent,
    FarmComponent,
    AutoFocusDirective,
    ValidateRangeValidatorDirective,
    ValidateDateRangeValidatorDirective,
    CurrencyDirective,
    PercentageDirective,
    BudgetComponent,
    InsuranceComponent,
    AssociationComponent,
    FarmsInfoComponent,
    CropYieldInfoComponent,
    LoanCropUnitsInfoComponent,
    FarmerInfoComponent,
    BorrowerInfoComponent,
    QuestionsComponent,
    LoanviewerComponent,
    LoanCropUnitsInfoComponent,
    QuestionsComponent,
    LoanbudgetComponent,
    FarmerInfoComponent,
    BorrowerInfoComponent,
    NotificationFeedsComponent,
    RightSidebarComponent,
    MediaArticleComponent,
    UserArticleComponent,
    CreateLoanComponent,
    CollateralComponent,
    FSAComponent,
    CrossCollateralDetailComponent,
    RatingComponent,
    FarmFinancialComponent,
    CellValueComponent,
    OptimizerComponent,
    DisbursepopupComponent,
    OptimizerverifyComponent,
    InsuranceverifyComponent,
    PoliciesComponent,
    CurrencyDirective,
    PercentageDirective,
    MarketingContractsComponent,
    CollateralReportSliderComponent,
    BorrowerIncomeHistoryComponent,
    AphComponent,
    WfrpComponent,
    ClosingComponent,
    AgGridTooltipComponent,
    DisbursedetailsComponent,
    DisbursecommentComponent,
    BorrowerInfoFormComponent,
    StyleguideComponent,
    DevPracticesComponent,
    AgGridComponentsComponent,
    WfrpComponent,
    LoanHistoryComponent,
    ViewBalancesComponent,
    FixedRevenueComponent,
    FormatComponent,
    CommentsComponent,
    CommitteStatusIconPipe,
    SearchFilterPipe,
    CommentTypeComponent,
    QuestionlistComponent,
    SvgDefinitionsomponent,
    VoteButtonColumnRenderer,
    SettingButtonRenderer,
    DisburseComponent,
    DisbursepopupComponent,
    DisbursedetailsComponent,
    DisbursecommentComponent,
    OptimizerverifyComponent,
    InsuranceverifyComponent,
    BudgetAutoCompleteCellEditor,
    ManageFarmOverrideDialogComponent,
    LoanDetailComponent,
    OtherIncomeComponent,
    OtherIncomeAutocompleteCellEditor,
    CropAutoCompleteEditor,
    CollateralReportAggregateComponent,
    CreditAgreementFieldsComponent,
    LoanRxDebugComponent,
    BorrowerSignerComponent,
    DateCellEditor,
    FooterColumnComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FileSaverModule,
    HttpModule,
    AppRoutingModule,
    HttpClientModule,
    NgSelectModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    LoadingModule,
    TextMaskModule,
    NgxJsonViewerModule,
    AgGridModule.withComponents([
      NumericEditor,
      BooleanEditor,
      CheckboxCellComponent,
      SelectEditor,
      ChipsListEditor,
      AutocompleteCellEditor,
      DisburseAutoCompleteEditor,
      OtherIncomeAutocompleteCellEditor,
      RadioboxCellEditor,
      CheckboxCellRenderer,
      EmptyEditor,
      BlankCellRenderer,
      AgGridTooltipComponent,
      BudgetAutoCompleteCellEditor,
      CropAutoCompleteEditor,
      DateCellEditor
    ]),
    ToastrModule.forRoot({
      timeOut: 1000
    }),
    SidebarModule.forRoot(),
    ChartsModule,
    BarRatingModule,
    AngularMultiSelectModule,
    ClickOutsideModule,
    LoadingBarHttpModule,
    PreferencesModule,
    CheckoutModule,
    DebugModule,
    ReportsModule,
    UiComponentsModule,
    NgxMaskModule.forRoot(),
    NgxCurrencyModule,
    MomentModule,
    Ng5SliderModule,
    NgbModule.forRoot(),
    FlowChartModule,
    CommitteeModule,
    AlertifyModule
  ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    UrlRResolver,
    LocalStorageService,
    SessionStorageService,
    OptimizerVerifyService,
    GlobalService,
    DisburseapiService,
    LoanApiService,
    CollateralService,
    FsaService,
    ApiService,
    CurrencyPipe,
    Borrowercalculationworker,
    Borrowerincomehistoryworker,
    Collateralcalculationworker,
    LoancalculationWorker,
    DataService,
    wfrpworker,
    QuicksyncResolver,
    LoanMasterCalculationWorkerService,
    BorrowerapiService,
    LoancropunitcalculationworkerService,
    LoancrophistoryService,
    FarmcalculationworkerService,
    LoggingService,
    AppsettingsApiService,
    FarmapiService,
    NamingConventionapiService,
    CropapiService,
    ReferenceService,
    InsuranceapiService,
    UserApiService,
    LoanvalidatorService,
    NotificationFeedsService,
    QuestionscalculationworkerService,
    LoancroppracticeworkerService,
    InsurancecalculationworkerService,
    AssociationcalculationworkerService,
    BudgetHelperService,
    BudgetpublisherService,
    OverallCalculationServiceService,
    MarketingcontractcalculationService,
    OptimizercalculationService,
    ToasterService,
    DataService,
    MasterService,
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    },
    CommitteStatusIconPipe,
    ValidationErrorsCountService,
    LoanConditionWorkerService,
    LoanCalculationSchematicsStorageService,
    DatetTimeStampService,
    LoanAuditTrailService,
    LoanTypeCalculationService,
    LoanToleranceCalculationService,
    BudgetcalculationworkerService,
    RefDataService,
    TermsCalculationWorkerService,
    ExportExcelService,
    HyperfieldValidationWorkerService,
    SettingsService,
    LoanSettingsService,
    DeactivateGuard,
    ExceptionWorkerService,
    DisburseConditions,
    CropPriceWorkerService,
    LoanDocumentWorkerService,
    RefVCAFieldsWorkerService,
    CheckLoanSyncService,
    LoanOverviewActivateGuard,
    LoanOverviewDeactivateGuard,
    LoanRxCalculationWorker,
    SharedService
  ],
  entryComponents: [
    DeleteButtonRenderer,
    DeleteNotifyPartyButtonRenderer,
    PrincialCheckboxCellRenderer,
    DisbursecommentComponent,
    verifyPrimaryHeader,
    verifyPrimaryHeadergroup,
    verifyPrimaryHeaderAPH,
    EmailEditor,
    YieldDialogComponent,
    CoBorrowerDialogComponent,
    VoteButtonRenderer,
    FSAComponent,
    OptimizerverifyComponent,
    InsuranceverifyComponent,
    VoteButtonColumnRenderer,
    SettingButtonRenderer,
    DisbursepopupComponent,
    ManageFarmOverrideDialogComponent,
    BorrowerSignerComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
