import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, Resolve } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { ToasterService } from '@lenda/services/toaster.service';
import { environment } from '@env/environment.prod';
import { JsonConvert } from 'json2typescript';
import { loan_model } from '@lenda/models/loanmodel';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class QuicksyncResolver implements Resolve<any>  {
    constructor(public localStorageService: LocalStorageService,
        public loanapi: LoanApiService,
        public logging: LoggingService,
        public loanserviceworker: LoancalculationWorker,
        public toasterService: ToasterService) { }

        resolve () {

        return this.checkifsyncdone();
    }
    checkifsyncdone(): Observable<boolean> {

        let localloanobject = this.localStorageService.retrieve(
            environment.loankey
        );
        // Loan Condition Calculation
        localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);
        return new Observable<boolean>(observer => {
            this.loanapi.syncloanobject(localloanobject).subscribe(respone => {

                if (respone.ResCode == 1) {
                    this.localStorageService.store(environment.modifiedbase, []);
                    this.loanapi.getLoanById(localloanobject.Loan_Full_ID).subscribe(res => {

                        this.logging.checkandcreatelog(
                            3,
                            'Overview',
                            'APi LOAN GET with Response ' + res.ResCode
                        );
                        if (res.ResCode == 1) {
                            this.toasterService.success('Records Synced');
                            let jsonConvert: JsonConvert = new JsonConvert();
                            this.loanserviceworker.performcalculationonloanobject(
                                jsonConvert.deserialize(res.Data, loan_model)
                            );
                            this.loanserviceworker.saveValidationsToLocalStorage(res.Data);
                            observer.next(true);
                        } else {
                            this.toasterService.error('Could not fetch Loan Object from API');
                            observer.next(false);
                        }

                    });
                } else {
                    this.toasterService.error(respone.Message || 'Error in Sync');
                    observer.next(false);
                }
            });
        })

    }


}
