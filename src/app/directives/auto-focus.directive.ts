import { Directive, ElementRef, OnDestroy, Input } from '@angular/core';

import { fromEvent } from 'rxjs';
import { ISubscription } from 'rxjs/Subscription';

// Directive decorator
@Directive({
  selector: '[autoFocus]'
})
export class AutoFocusDirective implements OnDestroy {
  private subscription: ISubscription;

  @Input('autoFocus') controlName: string;

  constructor(private el: ElementRef) {
    fromEvent(this.el.nativeElement, 'focus').subscribe(e => {
      setTimeout(() => {
        this.el.nativeElement.setSelectionRange(
          0,
          (this.el.nativeElement.value || '').length
        );
      }, 10);
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
