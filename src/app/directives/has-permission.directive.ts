import {
  Directive,
  Input,
  TemplateRef,
  ViewContainerRef,
  ElementRef,
  OnInit
} from '@angular/core';
import { IUser } from '@lenda/models/ref-data-model';
import { DataService } from '@lenda/services/data.service';

@Directive({
  selector: '[hasPermission]'
})
export class HasPermissionDirective {
  private currentUser: IUser;
  private permissions: Array<string> = [];
  private logicalOp = 'OR';
  private isHidden = true;

  constructor(
    private element: ElementRef,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private data: DataService
  ) {
    this.currentUser = this.data.current_user;
  }


  @Input()
  set hasPermission(val: string[]) {
    this.permissions = val;
    this.updateView();
  }

  @Input()
  set hasPermissionOp(permop: string) {
    this.logicalOp = permop;
    this.updateView();
  }

  private updateView() {
    if (this.checkPermission()) {
      if (this.isHidden) {
        this.viewContainer.createEmbeddedView(this.templateRef);
        this.isHidden = false;
      }
    } else {
      this.isHidden = true;
      this.viewContainer.clear();
    }
  }

  private checkPermission() {
    let hasPermission = false;

    if (this.currentUser) {
      for (const permission of this.permissions) {
        const permissionFound = this.currentUser[permission] == 1;

        if (permissionFound) {
          hasPermission = true;

          if (this.logicalOp === 'OR') {
            break;
          }
        } else {
          hasPermission = false;
          if (this.logicalOp === 'AND') {
            break;
          }
        }
      }
    }

    return hasPermission;
  }
}

export enum UIPermissions {
  IsAdmin = 'IsAdmin',
  SysAdmin = 'SysAdmin',
  RiskAdmin = 'RiskAdmin',
  Processor = 'Processor'
}
