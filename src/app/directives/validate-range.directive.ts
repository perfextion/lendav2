import { Directive, Input } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[validateRange]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: ValidateRangeValidatorDirective, multi: true }
    ]
})

export class ValidateRangeValidatorDirective implements Validator {
    @Input('start') public start: number;
    @Input('end') public end: number;

    validate(c: AbstractControl): { [key: string]: any } {
        
        let v = parseFloat(c.value);

        if (v < this.start || v > this.end) {
            c.setErrors({ notInRange: true });
            return { notInRange: true };
        } else {
            if (c.hasError('notInRange')) {
                delete c.errors['notInRange'];
            }
        }

        return null;
    }
}