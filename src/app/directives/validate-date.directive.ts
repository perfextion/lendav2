import { Directive, Input } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

import * as moment from 'moment';

moment().format('LLLL');

@Directive({
    selector: '[validateDateRange]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: ValidateDateRangeValidatorDirective, multi: true }
    ]
})

export class ValidateDateRangeValidatorDirective implements Validator {
    @Input('start') public start: number;
    @Input('end') public end: number;

    format = 'MM/DD/YYYY';

    validate(c: AbstractControl): { [key: string]: any } {
        
        let v = moment(c.value, this.format, true);

        if (!v.isValid() || v.isBefore(this.start) || v.isAfter(this.end)) {

            if(!c) return;
            c.setErrors({ notInRange: true });
            return { notInRange: true };
        } else {
            if (c.hasError('notInRange')) {
                delete c.errors['notInRange'];
            }
        }

        return null;
    }
}