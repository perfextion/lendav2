import { Component, ElementRef, ViewChild } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { Loan_Disburse } from "@lenda/models/disbursemodels";
import { MatTooltip } from "@angular/material";
import { debug } from "util";
import { DatePipe } from "@angular/common";
import { loan_model } from "@lenda/models/loanmodel";
import { ENTER_SELECTOR } from "@angular/animations/browser/src/util";

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'child-cell',
    template: `
    <mat-grid-list (mouseenter)='showtooltip()' (mouseleave) ="hidetooltip()" matTooltipDisabled=true #tooltip="matTooltip" matTooltipPosition="above" matTooltip="...." cols="10" rowHeight="33px">
  <mat-grid-tile colspan=7>  <button [ngClass]="setMyClasses()" style='min-width: 84px !important;' [disabled]="disabledAction" (click)="invokemethodRelative()" mat-button>  <mat-icon class='btncustom' mat-list-icon><i style='color: #ffffff;' class="flaticon flaticon-dollar-bills"></i></mat-icon>{{Status}}</button></mat-grid-tile>
  <mat-grid-tile colspan=1> <button style='margin-top: -17px;margin-left: 0px' [ngClass]="getapplicableStatus()" (click)="invokesettingsMethod()" mat-icon-button><mat-icon class="btncustom">mode_edit</mat-icon></button></mat-grid-tile>
  <mat-grid-tile colspan=2>
  <i *ngIf="declinedisabled" style='margin-bottom: 2px;margin-right: 5px' (click)="invokeDeleteMethod()" class="material-icons grid-action ag-grid-delete">  delete</i>
  <i *ngIf="!declinedisabled" style='margin-bottom: 2px;margin-right: 5px;font-size: 18px;' (click)="invokedeclineMethod()" class="material-icons grid-action ag-grid-delete">  thumb_down</i>
  </mat-grid-tile>
</mat-grid-list>
    `,
    styles: [
        `.btncustom {
            vertical-align: initial !important;
            font-size:16px;


        }
        .btn-active{
            color: white !important;
            background: #164c7b;
            border-width: 0px;
            border-style: solid;
            border-radius: 19px;
            padding: 0px;
            font-size: 10px;
            line-height: 20px;
            vertical-align: text-bottom;
        }
        .btn-deactive{
            color:gray !important;
            background: lightgray;
            border-width: 2px;
            border-style: solid;
            border-radius: 19px;
            padding: 0px;
            font-size: 10px;
            line-height: 20px;
            vertical-align: super;
        }
        .mat-icon-button{
            width: 18px !important;
            height: 18px !important;
        }


        `
    ]
})
// tslint:disable-next-line:component-class-suffix
export class SettingButtonRenderer implements ICellRendererAngularComp {
    public issubmit: boolean = false;
    public Userdetail: any;
    public params: any;
    public isprocess = false;
    public iseditable: boolean = false;
    public disabledAction = false;
    public declinedisabled = false;
    selecteditem: Loan_Disburse;
    public Status = 'Request';
    //public canedit=false;
    public style_pencil = {
        float: 'right',
        color: '#FFC000'
    };

    @ViewChild('tooltip') textarea: MatTooltip;

    setMyClasses() {
        if (this.disabledAction) {
            return 'btn-deactive';
        } else {
            return 'btn-active';
        }
    }
    agInit(params: any): void {
        this.params = params;
        this.Userdetail = this.params.user;
        this.selecteditem = this.params.data[this.params.colDef.field];
        if (this.selecteditem != null && this.selecteditem.Loan_Disburse_ID != null) {
        this.params.context.componentParent.invokesettingsMethodtest(this.selecteditem.Loan_Disburse_ID);
        }
        //this.iseditable = params.column.colId.replace('disburse_', '').includes('e');
        // if (this.selecteditem != null && this.selecteditem.History == null) {
        //     this.iseditable = true;
        // }
        if (params.column.colId.replace('disburse_', '').includes('e')) {
            this.issubmit = true;
        } else {
            // if(this.selecteditem.Status==0){
            //     this.iseditable=true;
            // }
        }

        try {
            this.checkexceptionStatus();
            if (this.selecteditem.History != null && this.selecteditem.History.length > 0) {
                this.checkpermissions();
            } else {
                this.declinedisabled = true;
            }


        } catch {

        }
    }

    showtooltip() {

        this.textarea.disabled = false;
        this.textarea.show();
        setTimeout(() => {
            try {
                let item = document.querySelector('mat-tooltip-component');
                if (this.selecteditem.History == undefined || this.selecteditem.History.length == 0) {
                    item.children[0].innerHTML = 'No History Available';
                } else {
                    let datePipe = new DatePipe('en-US');
                    let innerhtml = '';
                    this.selecteditem.History.forEach(element => {

                        innerhtml = innerhtml + '<div> ' + this.getstatus(element.Approval_Status) + ' ' + datePipe.transform(element.Approval_Date_Time, 'MM/dd/yyyy hh:mm:ss') + ' ' + element.Username + ' </div>';
                        if (element.Approval_Status == 1000) {
                            innerhtml = innerhtml + '<div style="color:blue"> Comment-' + element.Comments + '</div>';
                        }
                    });
                    item.children[0].innerHTML = innerhtml;
                }
            } catch {

            }
        }, 1000);

    }

    getstatus(value) {
        switch (value) {
            case 100:
                return 'Requested';

            case 200:
                return 'Approved';

            case 300:
                return 'Approved';

            case 400:
                return 'Approved';

            case 1000:
                return 'Declined';
            default:
                break;
        }
    }
    hidetooltip() {
        this.textarea.hide();
        this.textarea.disabled = true;
    }
    getapplicableStatus() {

        try {
            if (this.selecteditem != undefined && this.selecteditem.History != null && this.selecteditem.History.length > 0) {
                if (this.selecteditem.History[this.selecteditem.History.length - 1].Approval_Status > 0) {
                    return 'pencil-blue';
                }
            }
            switch (this.selecteditem.FC_PopupStatus) {
                case 0:
                    return 'pencil-orange';
                case 1:
                    return 'pencil-red';
                    break;
                case 2:
                    return 'pencil-blue';
                    break;
                default:
                    break;
            }
        } catch {
            return 'pencil-orange';
        }

    }
    checkpermissions() {
        let canhaveaction = false;
        if (this.selecteditem != undefined && this.selecteditem.History != null) {

            let currentstatus = this.selecteditem.History[this.selecteditem.History.length - 1];
            this.Status = 'Approve';
            switch (currentstatus.Approval_Status) {
                case 100:
                    if (this.Userdetail.Processor == 1) {
                        this.iseditable = false;
                    } else {
                        this.iseditable = true;
                    }

                    canhaveaction = true;
                    break;
                case 200:
                    if (this.Userdetail.RiskAdmin == 1) {
                        if (this.Userdetail.Processor == 1) {
                            this.iseditable = false;
                        } else {
                            this.iseditable = true;
                        }
                        canhaveaction = true;
                    } else {
                        this.iseditable = false;
                        canhaveaction = false;
                    }
                    break;
                case 300:
                    this.Status = 'Process';

                    if (this.Userdetail.Processor == 1) {
                          //this.iseditable=true;
                        this.isprocess = true;
                        if (this.Userdetail.Processor == 1) {
                            this.iseditable = false;
                        } else {
                            this.iseditable = true;
                        }
                        canhaveaction = true;
                    } else {
                        this.iseditable = false;
                        canhaveaction = false;
                    }
                    break;
                case 400:
                    this.iseditable = false;
                    this.Status = 'Approved';
                    this.declinedisabled = true;
                    canhaveaction = false;
                    break;
                default:
                    break;
            }
            if (this.declinedisabled != true) {
                if (this.Userdetail.Processor == 1) {
                    if (this.selecteditem.History == null || this.selecteditem.History.length == 0) {
                        this.declinedisabled = true;
                    }
                    this.declinedisabled = false;
                } else {
                    this.declinedisabled = true;
                }
            }
            if (canhaveaction) {
                if (!this.disabledAction) {
                    this.disabledAction = false;
                }
            } else {

                this.disabledAction = true;
            }



        }
    }

    getapprovalstatus() {
        try {
        return this.selecteditem.History[this.selecteditem.History.length - 1].Approval_Status;
        } catch {
            return 0;
        }
    }
    checkexceptionStatus() {
        try {
            let Exceptions = (this.params.context.componentParent.loanobject as loan_model).Loan_Exceptions;
            let hasexception = false;
            Exceptions.filter(p => p.Chevron_ID == 69 && p.ActionStatus != 3).forEach(element => {

                if (element.Exception_ID == 154 || this.getapprovalstatus() != 0) {
                    if (!hasexception) {

                        if (element.Complete_Ind == 1) {
                            hasexception = false;
                        } else {
                            hasexception = true;
                        }
                    }
                }
            });

            if (hasexception) {
                this.disabledAction = true;
            } else {
                this.disabledAction = false;
            }
        } catch (ex) {

        }
    }


    public invokemethodRelative() {

        if (this.selecteditem == undefined || this.selecteditem.Loan_Disburse_ID.toString().includes('e')) {
            //he is on first submit request
            this.invokesubmitMethod();
        } else {
            let status = this.selecteditem.History == null ? 0 : (this.selecteditem.History[this.selecteditem.History.length - 1].Approval_Status);
            if (status == 0) {
              if (this.selecteditem.Total_Disburse_Amount != 0) {
                this.params.context.componentParent.invokerequestMethod(this.selecteditem);
              }
            } else {
                this.invokeapproveMethod();
            }
        }
    }
    public invokesettingsMethod() {
        this.params.context.componentParent.invokesettingsMethod(this.selecteditem.Loan_Disburse_ID);
    }
    public invokesubmitMethod() {
        this.params.context.componentParent.invokesubmitMethod(this.params.column.colId);
    }

    public invokedetailsMethod() {
        this.params.context.componentParent.invokedetailsMethod(this.selecteditem.Loan_Disburse_ID);
    }
    public invokeapproveMethod() {
        let status = this.selecteditem.History[this.selecteditem.History.length - 1].Approval_Status;
        this.params.context.componentParent.invokeapproveMethod(this.selecteditem.Loan_Disburse_ID, status + 100);
    }
    public invokedeclineMethod() {
        this.params.context.componentParent.invokedeclineMethod(this.selecteditem.Loan_Disburse_ID, 100);
    }
    public invokeDeleteMethod() {
        this.params.context.componentParent.invokedeleteMethod(this.selecteditem == null ? -1 : this.selecteditem.Loan_Disburse_ID, 100);
    }

    refresh(): boolean {
        return false;
    }
}


