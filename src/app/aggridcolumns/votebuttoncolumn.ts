import {Component} from "@angular/core";
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
    selector: 'child-cell',
    template: `
    <span>
      <i *ngIf="params.data.CM_Role == 0" (click)="(!isDisable()) && invokeParentMethod(1)"
        class="material-icons grid-action side-margin" [style.color]="isDisable() ? 'gray' : 'green'">
        thumb_up
      </i>
      <i *ngIf="params.data.CM_Role == 0" (click)="(!isDisable()) && invokeParentMethod(2)"
        class="material-icons grid-action side-margin" [style.color]="isDisable() ? 'gray' : 'red'">
        thumb_down
      </i>
      <i *ngIf="params.data.CM_Role == 1" class="material-icons" style="color : green">
        check
      </i>
      <i *ngIf="params.data.CM_Role == 2" class="material-icons" style="color : red">
        close
      </i>
    </span>`,
    styles: [
        `.btn {
            line-height: 0.1
        }
        i.disabled{
            text-decoration : unset !important;
        }
        .side-margin{
            margin : 0 5px;
        }`
    ]
})


export class VoteButtonRenderer implements ICellRendererAngularComp {
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    public invokeParentMethod(buttonCode) {
        this.params.context.componentParent.VoteClicked(this.params.rowIndex, this.params.data,buttonCode)
    }

    refresh(): boolean {
        return false;
    }

    public isDisable(){
        return !this.params.colDef.isAdmin &&  !(this.params.data.User_ID == this.params.colDef.loggedInUser);
    }

}
