import {Component, ViewChild, ElementRef} from '@angular/core';
import { VerificationStages } from '@lenda/models/commonmodels';

@Component({
    selector: 'verify-header-group',
    host: {'class': 'ag-header-group-cell-label'},
    template: `

    <div class="ag-header-group-text">
    <div class="customHeaderLabel">{{params.displayName}}</div>
    <div *ngIf="params.enableSorting" (click)="onSortRequested('asc', $event)" [ngClass]="ascSort" class="customSortDownLabel"><i class="fa fa-long-arrow-down"></i></div>
    <div *ngIf="params.enableSorting" (click)="onSortRequested('desc', $event)" [ngClass]="descSort" class="customSortUpLabel"><i class="fa fa-long-arrow-up"></i></div>
    <div *ngIf="params.enableSorting" (click)="onSortRequested('', $event)" [ngClass]="noSort" class="customSortRemoveLabel"><i class="fa fa-times"></i></div>
    <div *ngIf="!showcancel" #menuButton class="customHeaderMenuButton" (click)="oniconClicked($event)"><mat-icon style="font-size:15px;vertical-align: none;margin-left:5px;cursor:pointer">{{params.menuIcon}}</mat-icon></div>
    <div *ngIf="showcancel" #menuButton class="customHeaderMenuButton" (click)="changestage($event)"><mat-icon style="font-size:15px;vertical-align: none;margin-left:5px;cursor:pointer">{{params.menuIcon}}</mat-icon></div>
    <div *ngIf="showcancel" #menuButton class="customHeaderMenuButton" (click)="oncloseClicked($event)"><mat-icon style="font-size:15px;vertical-align: none;margin-left:5px;cursor:pointer">cancel</mat-icon></div>
</div>
    `
})
export class verifyPrimaryHeadergroup {
    params: any;

    public ascSort: string;
    public descSort: string;
    public noSort: string;

    @ViewChild('menuButton', {read: ElementRef}) public menuButton;
    public showcancel=false;
    agInit(params): void {

        this.params = params;
        this.setcancelstatus();
        //params.column.addEventListener('sortChanged', this.onSortChanged.bind(this));
        //this.onSortChanged();
    }

    onMenuClicked() {
        this.params.showColumnMenu(this.menuButton.nativeElement);
    };

    onSortChanged() {
        this.ascSort = this.descSort = this.noSort = 'inactive';
        if (this.params.column.isSortAscending()) {
            this.ascSort = 'active';
        } else if (this.params.column.isSortDescending()) {
            this.descSort = 'active';
        } else {
            this.noSort = 'active';
        }
    }

    changestage($event){

        this.params.context.componentParent.verificationHelper.processStage(this.params.displayName.replace(' ',''));
    }

    oniconClicked($event){

             this.showcancel=true;
            this.params.context.componentParent.verificationHelper.verifypolicy(this.params.displayName.replace(' ',''));

    }
    oncloseClicked($event){
        this.showcancel=false;
        this.params.context.componentParent.verificationHelper.resetVerification(this.params.displayName.replace(' ',''));
    }
    onSortRequested(order, event) {
        this.params.setSort(order, event.shiftKey);
    }

    setcancelstatus()
    {

        try{
       this.showcancel= this.params.context.componentParent.verificationHelper['verifyButtonStage'+this.params.displayName.replace(' ','')]!=VerificationStages.Verify;
        }
        catch{
            this.showcancel=false;
        }
    }
}
