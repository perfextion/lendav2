import { Component, ViewChild, ElementRef } from '@angular/core';
import { VerificationStages } from '@lenda/models/commonmodels';

@Component({
    selector: 'verify-headerd',
    template: `
    <div class="center-xxd">
    <div class="customHeaderLabel">{{params.displayName}}</div>
    <div *ngIf="params.enableSorting" (click)="onSortRequested('asc', $event)" [ngClass]="ascSort" class="customSortDownLabel"><i class="fa fa-long-arrow-down"></i></div>
    <div *ngIf="params.enableSorting" (click)="onSortRequested('desc', $event)" [ngClass]="descSort" class="customSortUpLabel"><i class="fa fa-long-arrow-up"></i></div>
    <div *ngIf="params.enableSorting" (click)="onSortRequested('', $event)" [ngClass]="noSort" class="customSortRemoveLabel"><i class="fa fa-times"></i></div>
    <div *ngIf="params.isverify" #menuButton class="customHeaderMenuButton" [ngClass]="{'verifiedheader':disabled}" (click)="oniconClicked($event)"><mat-icon [ngClass]="{'headerblue':getstyle()}" style="font-size:15px;vertical-align: none;margin-left:5px;cursor:pointer">verified_user</mat-icon></div>
    <div *ngIf="!params.isverify" #menuButton class="customHeaderMenuButton" (click)="oncloseClicked($event)"><mat-icon style="font-size:15px;vertical-align: none;margin-left:5px;cursor:pointer;color:red">{{params.menuIcon}}</mat-icon></div>
</div>
    `
})
// tslint:disable-next-line: component-class-suffix
export class verifyPrimaryHeader {
    params: any;
    public openverificationaph: boolean = false;
    public openverification: boolean = false;
    public ascSort: string;
    public descSort: string;
    public noSort: string;
    public disabled: boolean;
    @ViewChild('menuButton', { read: ElementRef }) public menuButton;


    getstyle() {

        if (this.params.isAPH) {
            if (this.params.api.gridCore.eGridDiv.id == "IrrGrid") {
            return this.params.context.componentParent.optimizerverifyService.verifyButtonStageIIRAPH!=VerificationStages.Verify?'headerBlue':'';
            }
            else {
            return this.params.context.componentParent.optimizerverifyService.verifyButtonStageNIRAPH!=VerificationStages.Verify?'headerBlue':'';
            }
        } else {
            if (this.params.api.gridCore.eGridDiv.id == "IrrGrid") {
            return this.params.context.componentParent.optimizerverifyService.verifyButtonStageIIR!=VerificationStages.Verify?'headerBlue':'';
            }
            else {
            return this.params.context.componentParent.optimizerverifyService.verifyButtonStageNIR!=VerificationStages.Verify?'headerBlue':'';
            }
        }
    }
    agInit(params): void {

        this.params = params;
        try {
            if (params.alreadyverifiedhook.includes('IIR')) {
                this.disabled = params.context.componentParent.getverifiedstatusIIR(params.alreadyverifiedhook.replace('_IIR', ''));
            }
            else {
                this.disabled = params.context.componentParent.getverifiedstatusNIR(params.alreadyverifiedhook.replace('_NIR', ''));
            }
        } catch {

        }
        params.column.addEventListener('sortChanged', this.onSortChanged.bind(this));
        this.onSortChanged();
    }

    onMenuClicked() {
        this.params.showColumnMenu(this.menuButton.nativeElement);
    }

    onSortChanged() {
        this.ascSort = this.descSort = this.noSort = 'inactive';
        if (this.params.column.isSortAscending()) {
            this.ascSort = 'active';
        } else if (this.params.column.isSortDescending()) {
            this.descSort = 'active';
        } else {
            this.noSort = 'active';
        }
    }


    oniconClicked($event) {
        if (this.disabled) {
            return;
        }

        if (this.params.isAPH) {
              this.openverificationaph = true;
            if (this.params.api.gridCore.eGridDiv.id == "IrrGrid") {
                this.params.context.componentParent.verifyIIRAPH($event);
            }
            else {
                this.params.context.componentParent.verifyNIRAPH($event);
            }
        } else {
            this.openverification = true;
            if (this.params.api.gridCore.eGridDiv.id == "IrrGrid") {
                this.params.context.componentParent.verifyIIRClicked($event);
            }
            else {
                this.params.context.componentParent.verifyNIRClicked($event);
            }
        }

    }
    oncloseClicked($event) {

        this.openverificationaph = false;
        this.openverification = false;
        this.params.context.componentParent.resetVerification($event);

    }
    onSortRequested(order, event) {
        this.params.setSort(order, event.shiftKey);
    }
}
