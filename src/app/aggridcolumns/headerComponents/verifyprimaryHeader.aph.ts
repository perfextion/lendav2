import { Component, ViewChild, ElementRef } from '@angular/core';
import { VerificationStages } from '@lenda/models/commonmodels';

@Component({
    selector: 'verify-header',
    template: `
    <div class="center-xxd">
    <div class="customHeaderLabel">{{params.displayName}}</div> 
    <div *ngIf="params.enableSorting" (click)="onSortRequested('asc', $event)" [ngClass]="ascSort" class="customSortDownLabel"><i class="fa fa-long-arrow-down"></i></div> 
    <div *ngIf="params.enableSorting" (click)="onSortRequested('desc', $event)" [ngClass]="descSort" class="customSortUpLabel"><i class="fa fa-long-arrow-up"></i></div> 
    <div *ngIf="params.enableSorting" (click)="onSortRequested('', $event)" [ngClass]="noSort" class="customSortRemoveLabel"><i class="fa fa-times"></i></div>
    <div *ngIf="params.isverify" #menuButton class="customHeaderMenuButton" [ngClass]="{'verifiedheader':disabled}" (click)="oniconClicked($event)"><mat-icon [ngClass]="{'headerblue':getstyle()}" style="font-size:15px;vertical-align: none;margin-left:5px;cursor:pointer">{{params.menuIcon}}</mat-icon></div> 
    <div *ngIf="!params.isverify" #menuButton class="customHeaderMenuButton" (click)="oncloseClicked($event)"><mat-icon style="font-size:15px;vertical-align: none;margin-left:5px;cursor:pointer;color:red">{{params.menuIcon}}</mat-icon></div> 
</div>
    `
})
export class verifyPrimaryHeaderAPH {
    params: any;
    public openverification:boolean=false;
    public ascSort: string;
    public descSort: string;
    public noSort: string;

    @ViewChild('menuButton', { read: ElementRef }) public menuButton;
    disabled: boolean;
    getstyle(){
        if(this.params.context.componentParent.verifyButtonStage!=VerificationStages.Verify)
        {
            return 'headerBlue';
        }
        else
        {
            return '';
        }
    }
    agInit(params): void {

        this.params = params;
        try {
         this.disabled = params.context.componentParent.getverifiedstatus();
        }
        catch
        {

        }
        params.column.addEventListener('sortChanged', this.onSortChanged.bind(this));
        this.onSortChanged();
    }

    onMenuClicked() {
        this.params.showColumnMenu(this.menuButton.nativeElement);
    };

    onSortChanged() {
        this.ascSort = this.descSort = this.noSort = 'inactive';
        if (this.params.column.isSortAscending()) {
            this.ascSort = 'active';
        } else if (this.params.column.isSortDescending()) {
            this.descSort = 'active';
        } else {
            this.noSort = 'active';
        }
    }


    oniconClicked($event) 
    {
        
        if (this.disabled) {
            return;
        }
        this.params.context.componentParent.verifyClicked();
        this.openverification=true;
        this.params.context.componentParent.gridApi.refreshHeader();
    }
    oncloseClicked($event) {
        this.openverification=false;
        this.params.context.componentParent.resetVerification();
    }
    onSortRequested(order, event) {
        this.params.setSort(order, event.shiftKey);
    }
}