import { environment } from '@env/environment';
import { LocalStorageService } from 'ngx-webstorage';
import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { LoanStatus } from '../models/loanmodel';

@Component({
    selector: 'child-cell',
    template: `
    <span *ngIf="showButton">
        <i (click)="invokeParentMethod()" class="material-icons grid-action ag-grid-delete">
            delete
        </i>
    </span>`,
    styles: [
        `.btn {
            line-height: 0.1
        }`
    ]
})


export class DeleteButtonRenderer implements ICellRendererAngularComp {
    public params: any;
    public showButton: boolean;

    constructor(public localstorageservice: LocalStorageService) { }

    agInit(params: any): void {
        this.params = params;

        this.showButton = this.params.data.Other_Description_Text != 'Total';
        //do not display if budget rows and not isCustom
        // if( this.params.data.Loan_Budget_ID >=0){
        if (this.localstorageservice.retrieve(environment.currentpage) == 'budget') {
          if(this.params.data.Budget_Type == 'F' && !this.params.data.IsCustom) {
            this.showButton = false;
            return;
          }
          if (this.params.data.Loan_Budget_ID >= 0) {
              if (!this.params.data.IsCustom) {
                  this.showButton = false;
              } else {
                  this.showButton = true;
              }
          }
        }

        if (this.params.data.Other_Description_Text != 'Total') {
            if (params.column.colDef.cellClass == 'fixed-expense') {
                this.showButton = true;
            }
        }

        if(this.params.data.Assoc_Type_Code) {
          this.showButton = this.params.data.Assoc_Type_Code != 'PRI' || (params.data.Assoc_Type_Code == 'PRI' && params.data.ActionStatus != -1);
        }
    }

    public invokeParentMethod() {
        if(this.isLoanEditable()){
            this.params.context.componentParent.DeleteClicked(this.params.rowIndex, this.params.data)
        }
    }

    private isLoanEditable(){
        let localloanobject = this.localstorageservice.retrieve(environment.loankey);
        return localloanobject && localloanobject.LoanMaster.Loan_Status !== LoanStatus.Working ? false : true;
    }

    refresh(): boolean {
        return false;
    }
}
