import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisbursecommentComponent } from './disbursecomment.component';

describe('DisbursecommentComponent', () => {
  let component: DisbursecommentComponent;
  let fixture: ComponentFixture<DisbursecommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisbursecommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisbursecommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
