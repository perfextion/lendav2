import { Component, OnInit, NgZone } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-disbursecomment',
  templateUrl: './disbursecomment.component.html',
  styleUrls: ['./disbursecomment.component.scss']
})
export class DisbursecommentComponent implements OnInit {
  public Comment: string = '';

  constructor(
    public dialogRef: MatDialogRef<DisbursecommentComponent>,
    private ngZone: NgZone
  ) {}

  ngOnInit() {
    //not used
  }

  onNoClick() {
    this.ngZone.run(() => {
      this.dialogRef.close();
    });
  }

  onOkClick() {
    this.dialogRef.close(this.Comment);
  }
}
