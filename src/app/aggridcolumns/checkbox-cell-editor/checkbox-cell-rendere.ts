import { Component, ElementRef } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'checkbox-cell',
  template:
    '<input class="checkbox-cell" [ngClass]="type" *ngIf="!params?.data?.istotal" type="checkbox" [checked]="params.value" (click)="invokeParentMethod($event)" [disabled]="isDisabled">'
})
export class CheckboxCellRenderer implements ICellRendererAngularComp {
  public params: any;
  public edit = true;
  public type: string;

  constructor(private el: ElementRef) {}

  agInit(params: any): void {
    this.params = params;
    this.edit = params.edit;
    this.type = params.type;
  }

  invokeParentMethod($event: any) {
    if (this.edit) {
      this.params.context.componentParent.MethodFromCheckbox(
        this.params,
        this.type,
        $event
      );
    }
  }

  get isDisabled() {
    if (this.type == 'PTI') {
      return this.params.data.Owned == 1 || !this.edit;
    }

    return !this.edit;
  }

  refresh(): boolean {
    return false;
  }

  ngOnDestroy() {}
}
