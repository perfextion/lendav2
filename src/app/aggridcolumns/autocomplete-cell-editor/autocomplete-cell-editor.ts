import {
  Component,
  ViewChild
} from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { NgSelectComponent } from '@ng-select/ng-select';
import * as _ from 'lodash';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { AssociationComponent } from '@lenda/components/insurance/association/association.component';

@Component({
  selector: 'autocomplete-cell-editor',
  templateUrl: './autocomplete-cell-editor.html',
  styleUrls: ['./autocomplete-cell-editor.scss']
})
export class AutocompleteCellEditor implements ICellEditorAngularComp {
  dropdownList = [];
  selectedAssoc;
  params: any;
  previousWidth = 0;
  previousHeight = 0;

  @ViewChild('ngSelect') selectEL: NgSelectComponent;
  component: AssociationComponent;

  addTag = true;

  ROQ = ['AIP', 'THR', 'DIS'];
  associationTypeCode: string;

  constructor(private alertify: AlertifyService) {}

  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  ngAfterViewInit() {
    //this.selectEL.focus();
    //this.selectEL.open();
  }

  scroll = (): void => {};

  closeDropdown() {
    this.params.stopEditing();
  }

  onClickedOutside(event) {
    // this.params.stopEditing();
  }

  agInit(params: any): void {
    this.previousHeight = params.eGridCell.style.height;
    params.eGridCell.style.height = 'auto';

    this.params = params;
    this.component = params.context.componentParent;
    this.associationTypeCode = this.component.associationTypeCode;

    let tempList = this.component.Ref_Associations.filter(elem => {
      return (elem.Assoc_Type_Code == this.associationTypeCode && !elem.IsOther);
    });

    this.dropdownList = [];

    tempList.filter(item => item.Assoc_Name).forEach(item => {
      let index = this.component.rowData.findIndex(a => a.Assoc_Name == item.Assoc_Name);
      if(index == -1) {
        this.dropdownList.push(item);
      }
    });

    if(this.params.node.data.Assoc_ID) {
      this.selectedAssoc = this.params.node.data.Assoc_Name;
    }
  }

  getValue(): any {
    this.params.eGridCell.style.height = this.previousHeight;
    return this.selectedAssoc && _.trim(this.selectedAssoc).length > 0
      ? this.selectedAssoc
      : '';
  }

  isPopup(): boolean {
    return false;
  }

  itemChange(item) {
    if (item) {
      if(!item.Ref_Association_ID && this.ROQ.some(a => a == this.associationTypeCode)) {
        this.alertify
          .confirm(
            'Warning',
            'Press Confirm to prompt Risk Admin team for consideration of current invalid entry.'
          )
          .subscribe(res => {
            if (res) {
              this.addAssociation(item);
            } else {
              item.Assoc_Name = '';
              this.params.node.data.Assoc_Name = '';
              this.addAssociation({});
              this.params.api.refreshCells();
            }
          });
      } else {
        this.addAssociation(item);
      }
    }
  }

  private addAssociation(item: any) {

    let isUpdate = false;

    if(item) {
      if (item.Ref_Association_ID) {
        this.params.node.data.Ref_Assoc_ID = item.Ref_Association_ID || 0;
        isUpdate = true;
      } else {
        this.params.node.data.Ref_Assoc_ID = 0;
        isUpdate = false;
      }

      let assoc_default = this.component.refdata.Ref_Association_Defaults.find(
        a =>
          a.Assoc_ID == item.Ref_Association_ID &&
          a.Office_ID == this.component.Office_ID
      );

      if(assoc_default) {
        isUpdate = this.assignAssocDetails(assoc_default, isUpdate);
      } else {
        isUpdate = this.assignAssocDetails(item, isUpdate);
      }
    }

    if (isUpdate) {
      this.params.api.refreshCells();
    }
  }

  private assignAssocDetails(item: any, isUpdate: boolean) {
    if (item.Contact) {
      this.params.node.data.Contact = item.Contact;
      isUpdate = true;
    } else {
      this.params.node.data.Contact = '';
      isUpdate = false;
    }

    if (item.Location) {
      this.params.node.data.Location = item.Location;
      isUpdate = true;
    } else {
      this.params.node.data.Location = '';
      isUpdate = false;
    }

    if (item.Phone) {
      this.params.node.data.Phone = item.Phone;
      isUpdate = true;
    } else {
      this.params.node.data.Phone = '';
      isUpdate = false;
    }

    if (item.Email) {
      this.params.node.data.Email = item.Email;
      isUpdate = true;
    } else {
      this.params.node.data.Email = '';
      isUpdate = false;
    }

    if (item.Amount) {
      this.params.node.data.Amount = item.Amount;
      isUpdate = true;
    } else {
      this.params.node.data.Amount = '';
      isUpdate = false;
    }

    if (item.Preferred_Contact_Ind) {
      this.params.node.data.Preferred_Contact_Ind = item.Preferred_Contact_Ind;
      isUpdate = true;
    } else {
      this.params.node.data.Preferred_Contact_Ind = '';
      isUpdate = false;
    }

    if (item.Validation_ID) {
      this.params.node.data.Validation_ID = item.Validation_ID;
      isUpdate = true;
    } else {
      this.params.node.data.Validation_ID = '';
      isUpdate = false;
    }

    if (item.Exception_ID) {
      this.params.node.data.Exception_ID = item.Exception_ID;
      isUpdate = true;
    } else {
      this.params.node.data.Exception_ID = '';
      isUpdate = false;
    }

    return isUpdate;
  }
}
