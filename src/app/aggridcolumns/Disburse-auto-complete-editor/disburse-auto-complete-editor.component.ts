import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { NgSelectComponent } from '@ng-select/ng-select';
import * as _ from 'lodash';
import { DisbursepopupComponent } from '../disbursepopup/disbursepopup.component';

@Component({
  selector: 'app-disburse-auto-complete-editor',
  templateUrl: './disburse-auto-complete-editor.component.html',
  styleUrls: ['./disburse-auto-complete-editor.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DisburseAutoCompleteEditor implements OnInit {

  public landownerlist: Array<any> = [];
  selectedlandowner;
  params: any;
  previousWidth = 0;
  previousHeight = 0;

  @ViewChild('ngSelect') selectEL: NgSelectComponent;
  component: DisbursepopupComponent;

  constructor() {}

  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  ngAfterViewInit() {
    //this.selectEL.focus();
    //this.selectEL.open();
  }

  scroll = (): void => {};

  closeDropdown() {
    this.params.stopEditing();
  }

  onClickedOutside() {
    // this.params.stopEditing();
     ''
  }

  agInit(params: any): void {
    
    this.previousHeight = params.eGridCell.style.height;
    params.eGridCell.style.height = 'auto';
    this.selectEL.elementRef.nativeElement.style.width = '450px'

    this.params = params;
    this.component = params.context.componentParent;

    this.landownerlist = this.component.Get_Landowner_Farm_List();
    
  }

  getValue(): any {
    
    this.params.eGridCell.style.height = this.previousHeight;
    return this.selectedlandowner && _.trim(this.selectedlandowner).length > 0
      ? this.selectedlandowner
      : '';
  }

  isPopup(): boolean {
    return true;
  }

  itemChange(item) {
 
  }

}
