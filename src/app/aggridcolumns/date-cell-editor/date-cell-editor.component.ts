import {
  Component,
  AfterViewInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { ICellEditorAngularComp } from 'ag-grid-angular';

import { RegEx } from '@lenda/shared/shared.constants';
import { Get_Formatted_Date } from '@lenda/services/common-utils';

@Component({
  selector: 'app-date-cell',
  templateUrl: './date-cell-editor.component.html'
})
export class DateCellEditor implements ICellEditorAngularComp, AfterViewInit {
  private params: any;
  public value: string;

  @ViewChild('input', { read: ViewContainerRef })
  public input: ViewContainerRef;

  agInit(params: any): void {
    this.params = params;
    this.value = FORMAT_DATE_VALUE(this.params);
  }

  getValue(): any {
    return this.value;
  }

  isPopup(): boolean {
    return false;
  }

  isCancelBeforeStart(): boolean {
    return false;
  }

  onKeyDown(event): void {
    if (!this.isKeyPressedDate(event)) {
      if (event.preventDefault) event.preventDefault();
    }
  }

  onChange(event) {
    event.srcElement.parentElement.className = event.srcElement.parentElement.className.replace(
      'editable-color',
      'edited-color'
    );
  }

  // dont use afterGuiAttached for post gui events - hook into ngAfterViewInit instead for this
  ngAfterViewInit() {
    setTimeout(() => {
      this.input.element.nativeElement.focus();
      this.input.element.nativeElement.select();
    });
  }

  private isKeyPressedDate(event) {
    let charCode = this.getCharCodeFromEvent(event);
    let charStr = String.fromCharCode(charCode);
    return this.isCharDate(charStr);
  }

  private getCharCodeFromEvent(event) {
    event = event || window.event;
    return typeof event.which === 'undefined' ? event.keyCode : event.which;
  }

  private isCharDate(charStr) {
    return /^[0-9\/]+$/i.test(charStr);
  }
}

export function FORMAT_DATE_VALUE(params) {
  let currentValue = params.value;
  if (
    RegEx.DATE_FORMAT.test(currentValue) ||
    RegEx.MMDDYYY_FORMAT.test(currentValue)
  ) {
    let value = Get_Formatted_Date(currentValue);
    if (value == '01/01/1900') {
      return null;
    } else {
      return value;
    }
  } else {
    return null;
  }
}
