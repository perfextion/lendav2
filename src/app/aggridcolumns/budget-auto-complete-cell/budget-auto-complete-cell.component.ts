import { Component, ViewChild } from '@angular/core';
import { NgSelectComponent } from '@ng-select/ng-select';
import * as _ from 'lodash';
import { RefBudgetExpenseType } from '@lenda/models/ref-data-model';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { LoanbudgetComponent } from '@lenda/components/budget/loan-budget/loanbudget.component';
import { Loan_Budget } from '@lenda/models/loanmodel';
import { AlertifyService } from '@lenda/alertify/alertify.service';

@Component({
  selector: 'app-budget-auto-complete-cell-editor',
  templateUrl: './budget-auto-complete-cell.component.html',
  styleUrls: ['./budget-auto-complete-cell.component.scss']
})
export class BudgetAutoCompleteCellEditor implements ICellEditorAngularComp {
  budgetExpenseTypes: RefBudgetExpenseType[] = [];

  selectedbgtExpenseType: string;

  params: any;
  previousWidth = 0;
  previousHeight = 0;

  component: LoanbudgetComponent;
  Crop_Practice_ID: number;
  Budget_Type = '';

  @ViewChild('ngSelect') selectEL: NgSelectComponent;

  constructor(private alertify: AlertifyService) {}

  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  ngAfterViewInit() {}

  scroll = (): void => {};

  closeDropdown() {
    this.params.stopEditing();
  }

  onClickedOutside(event) {}

  agInit(params: any): void {
    this.previousHeight = params.eGridCell.style.height;
    params.eGridCell.style.height = 'auto';
    this.selectEL.elementRef.nativeElement.style.width = '140px';

    this.params = params;
    this.component = params.context.componentParent;

    if(this.component.cropPractice) {
      this.Crop_Practice_ID = this.component.cropPractice.Crop_Practice_ID;
    } else {
      this.Crop_Practice_ID = 0;
      this.Budget_Type = 'F';
    }

    let refExpenseTypes = this.component.Budget_Expense_Types;

    this.budgetExpenseTypes = [];
    const rowdata = this.component.rowData as Array<Loan_Budget>;

    if (refExpenseTypes) {
      if(this.Budget_Type == 'F') {
        refExpenseTypes.forEach(item => {
          if(item.Budget_Expense_Type_ID != 14 && item.Budget_Expense_Type_ID != 15) {
            this.budgetExpenseTypes.push(item);
          }
        });
      } else {
        refExpenseTypes.forEach(item => {
          let index = rowdata.findIndex(
            a => a.Expense_Type_ID == item.Budget_Expense_Type_ID
          );

          if (index == -1) {
            this.budgetExpenseTypes.push(item);
          }
        });
      }
    }

    if (this.params.node.data.Loan_Budget_ID) {
      this.selectedbgtExpenseType = this.params.node.data.Expense_Type_Name;
    }
  }

  getValue(): any {
    this.params.eGridCell.style.height = this.previousHeight;
    return this.selectedbgtExpenseType;
  }

  isPopup(): boolean {
    return false;
  }

  itemChange(expense: RefBudgetExpenseType) {
    if (expense) {
      let isAlreadyExistingItem = this.component.rowData.find(
        a => a.Expense_Type_Name == expense.Budget_Expense_Name
      );

      if (isAlreadyExistingItem && this.Budget_Type != 'F') {
        this.alertify
          .alert(
            'Alert',
            'You have already added this expense. Please try choose another.'
          )
          .subscribe(res => {
            this.params.node.data.Expense_Type_Name = '';
            this.params.api.refreshCells();

            this.params.api.startEditingCell({
              rowIndex: this.component.rowData.length - 1,
              colKey: 'Expense_Type_Name'
            });
          });
      } else {
        this.AddExpense(expense);
      }
    }
  }

  private AddExpense(expense: RefBudgetExpenseType) {
    let isUpdate = false;

    if (expense.Budget_Expense_Type_ID) {
      this.params.node.data.Expense_Type_ID = expense.Budget_Expense_Type_ID;
      isUpdate = true;
    } else {
      this.params.node.data.Expense_Type_ID = 0;
      isUpdate = false;
    }

    this.params.node.data.Standard_Ind = expense.Standard_Ind || 0;

    if (this.params.node.data.ActionStatus == 1) {
      let bgtDefault = this.component.refdata.BudgetDefault.find(
        a =>
          a.Budget_Expense_Type_ID == expense.Budget_Expense_Type_ID &&
          a.Crop_Practice_Id == this.Crop_Practice_ID
      );

      if (bgtDefault) {
        if (bgtDefault.ARM_Budget) {
          this.params.node.data.ARM_Budget_Acre = bgtDefault.ARM_Budget;
          isUpdate = true;
        } else {
          this.params.node.data.ARM_Budget_Acre = 0;
          isUpdate = false;
        }

        if (bgtDefault.Distributor_Budget) {
          this.params.node.data.Distributor_Budget_Acre =
            bgtDefault.Distributor_Budget;
          isUpdate = true;
        } else {
          this.params.node.data.Distributor_Budget_Acre = 0;
          isUpdate = false;
        }

        if (bgtDefault.Third_Party_Budget) {
          this.params.node.data.Third_Party_Budget_Acre =
            bgtDefault.Third_Party_Budget;
          isUpdate = true;
        } else {
          this.params.node.data.Third_Party_Budget_Acre = 0;
          isUpdate = false;
        }
      } else {
        this.params.node.data.ARM_Budget_Acre = 0;
        this.params.node.data.Distributor_Budget_Acre = 0;
        this.params.node.data.Third_Party_Budget_Acre = 0;
      }

      if(this.Budget_Type == 'F') {
        this.params.node.data.ARM_Budget_Acre = 0;
        this.params.node.data.Distributor_Budget_Acre = 0;
        this.params.node.data.Third_Party_Budget_Acre = 0;
        this.params.node.data.ARM_Budget_Crop = 0;
        this.params.node.data.Distributor_Budget_Crop = 0;
        this.params.node.data.Third_Party_Budget_Crop = 0;
      }
    }

    if (isUpdate) this.params.api.refreshCells();
  }
}
