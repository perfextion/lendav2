import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { YieldComponent } from '@lenda/components/crop/yield/yield.component';
import { NgSelectComponent } from '@ng-select/ng-select';
import { V_Crop_Price_Details } from '@lenda/models/cropmodel';
import * as _ from 'lodash';

@Component({
  selector: 'app-crop-auto-complete-editor',
  templateUrl: './crop-auto-complete-editor.component.html',
  styleUrls: ['./crop-auto-complete-editor.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CropAutoCompleteEditor implements OnInit {

  cropList: Array<V_Crop_Price_Details> = [];
  selectedCrop;
  params: any;
  previousWidth = 0;
  previousHeight = 0;

  @ViewChild('ngSelect') selectEL: NgSelectComponent;
  component: YieldComponent;

  constructor() {}

  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  ngAfterViewInit() {
    //this.selectEL.focus();
    //this.selectEL.open();
  }

  scroll = (): void => {};

  closeDropdown() {
    this.params.stopEditing();
  }

  onClickedOutside(event) {
    // this.params.stopEditing();
  }

  agInit(params: any): void {
    this.previousHeight = params.eGridCell.style.height;
    params.eGridCell.style.height = 'auto';
    this.selectEL.elementRef.nativeElement.style.width = '300px'

    this.params = params;
    this.component = params.context.componentParent;

    this.cropList = this.component.Get_Crops_Practices_List();
  }

  getValue(): any {
    this.params.eGridCell.style.height = this.previousHeight;
    return this.selectedCrop && _.trim(this.selectedCrop).length > 0
      ? this.selectedCrop
      : '';
  }

  isPopup(): boolean {
    return false;
  }

  itemChange(item) {
    this.addCrop(item);
  }

  private addCrop(item) {
    let isUpdate = false;

    if (item && item.key) {
      this.params.node.data.Crop_ID = item.key;
      isUpdate = true;
    } else {
      this.params.node.data.Crop_ID = undefined;
      isUpdate = false;
    }

    if (item && item.Crop_Code) {
      this.params.node.data.Crop_Code = item.Crop_Code;
      isUpdate = true;
    } else {
      this.params.node.data.Crop_Code = '';
      isUpdate = false;
    }

    if (item && item.Crop_Name) {
      this.params.node.data.Crop_Name = item.Crop_Name;
      isUpdate = true;
    } else {
      this.params.node.data.Crop_Name = '';
      isUpdate = false;
    }

    if (item && item.Crop_Type_Code) {
      this.params.node.data.cropType = item.Crop_Type_Code;
      isUpdate = true;
    } else {
      this.params.node.data.cropType = '';
      isUpdate = false;
    }

    if (item && item.Irr_Prac_Code) {
      this.params.node.data.Practice = item.Irr_Prac_Code;
      this.params.node.data.IrNI = item.Irr_Prac_Code;
      isUpdate = true;
    } else {
      this.params.node.data.Practice = '';
      this.params.node.data.IrNI = '';
      isUpdate = false;
    }

    if (item && item.Crop_Prac_Code) {
      this.params.node.data.Crop_Practice = item.Crop_Prac_Code;
      isUpdate = true;
    } else {
      this.params.node.data.Crop_Practice = '';
      isUpdate = false;
    }

    if (item && item.InsUOM) {
      this.params.node.data.InsUOM = item.InsUOM;
      isUpdate = true;
    } else {
      this.params.node.data.InsUOM = '';
      isUpdate = false;
    }

    if (isUpdate) this.params.api.refreshCells();
  }
}
