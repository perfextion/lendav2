import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { MembersComponent } from '@lenda/components/committee/members/members.component';

@Component({
  selector: 'child-cell',
  template: `
  <span *ngIf="showButton">
    <i (click)="invokeParentMethod()" class="material-icons grid-action ag-grid-delete">
      delete
    </i>
  </span>`,
  styles: [
      `.btn {
          line-height: 0.1
      }`
  ]
})
export class DeleteNotifyPartyButtonRenderer implements ICellRendererAngularComp {
  public params: any;
  public showButton: boolean;
  public memberComponent: MembersComponent;

  constructor() { }

  agInit(params: any): void {
      this.params = params;
      this.memberComponent = this.params.context.componentParent;
      this.showButton = this.params.data.CM_Role == 3;
  }

  public invokeParentMethod() {
    this.memberComponent.DeleteNotifyParty(this.params.data.CM_ID)
  }

  refresh(): boolean {
      return false;
  }
}
