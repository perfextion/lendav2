import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'checkbox-child-cell',
  template:
    '<input class="checkbox-child-cell pri" type="checkbox" [checked]="params.value" (change)="invokeParentMethod($event)">'
})
export class PrincialCheckboxCellRenderer implements ICellRendererAngularComp {
  public params: any;
  public edit = true;

  agInit(params: any): void {
    this.params = params;
  }

  public invokeParentMethod(event) {
    this.params.context.componentParent.Set_Princial_Ind(
      event.srcElement.checked,
      this.params.data.Assoc_ID
    );
  }

  refresh(): boolean {
    return false;
  }
}
