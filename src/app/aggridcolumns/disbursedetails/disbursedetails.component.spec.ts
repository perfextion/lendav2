import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisbursedetailsComponent } from './disbursedetails.component';

describe('DisbursedetailsComponent', () => {
  let component: DisbursedetailsComponent;
  let fixture: ComponentFixture<DisbursedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisbursedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisbursedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
