import { Component, OnInit, Inject, NgZone, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Loan_Disburse, Loan_Disburse_Detail } from '@lenda/models/disbursemodels';
import { dateformat } from '@lenda/aggridformatters/valueformatters';
import { getstatuscodesfromid } from '@lenda/components/disburse/disburse.rows.static';

@Component({
  selector: 'app-disbursedetails',
  templateUrl: './disbursedetails.component.html',
  styleUrls: ['./disbursedetails.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class DisbursedetailsComponent implements OnInit {
  public rowdata=new Array<any>();
  constructor(
    public dialogRef: MatDialogRef<DisbursedetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Loan_Disburse,
    private ngZone: NgZone
  ) {
    data.History.reverse().forEach(item => {
      let row=new Disburse_requesthistory();
      row.Date_Time=dateformat(item.Approval_Date_Time);
      if(item.Approval_Status==100 && item.Action_Code==1)
      {
        row.Status_Ind="Requested";
      }
      else
      row.Status_Ind=getstatuscodesfromid(item.Action_Code);
      row.Position=item.User_Role;
      row.User_Name=item.Username==null?"":item.Username;
      row.Comments=item.Comments;
      this.rowdata.push(row);
  });
    

  }

  ngOnInit() {
   //not used 
  }

  onNoClick(){
    this.ngZone.run(() => {
      this.dialogRef.close();
    });
  }

}
export enum Disburselevel{
  
  
 
  Processor='LO',
  Risk_Approval='RRM',
  LO_Approval='LO',
  Request='Borrower',
}
export class Disburse_requesthistory
{
  public User_Name:string;
  public Date_Time:string;
  public Position:string;
  public Status_Ind:string;
  public Comments:String;
}
