import { Component, OnInit, Inject, EventEmitter, NgZone, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, ErrorStateMatcher, MatInput } from '@angular/material';
import { getNumericCellEditor } from '@lenda/Workers/utility/aggrid/numericboxes';
import { getAlphaNumericCellEditor } from '@lenda/Workers/utility/aggrid/alphanumericboxes';
import { setgriddefaults, toggletoolpanel, cellclassmaker, CellType, isgrideditable, removeHeaderMenu } from '@lenda/aggriddefinations/aggridoptions';
import { currencyFormatter } from '@lenda/aggridformatters/valueformatters';
import { Loan_Disburse, Loan_Disburse_Rent_Detail, Loan_Disburse_Detail } from '@lenda/models/disbursemodels';
import { loan_model } from '@lenda/models/loanmodel';
import { SelectEditor } from '@lenda/aggridfilters/selectbox';
import { DisburseAutoCompleteEditor } from '../Disburse-auto-complete-editor/disburse-auto-complete-editor.component';
import { DataService } from '@lenda/services/data.service';
import * as _ from 'lodash';
import { environment } from '@env/environment.prod';
import { Validators, FormControl, FormGroupDirective, NgForm, AbstractControl, ValidatorFn } from '@angular/forms';
import { DeleteButtonRenderer } from '../deletebuttoncolumn';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { CheckboxCellComponent } from '@lenda/aggridfilters/checkbox';

@Component({
  selector: 'app-disbursepopup',
  templateUrl: './disbursepopup.component.html',
  styleUrls: ['./disbursepopup.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DisbursepopupComponent {
  //onDataRecieved = new EventEmitter();
  public components;
  public context;
  private gridApi;
  public rowData;
  public disabled = false;
  public formdata: Loan_Disburse;
  private columnApi;
  public columnDefs = [
    {
      headerName: 'Landowner',
      field: 'Landowner', pickfield: 'Landowner',
      editable: function (params) {
        // let Loan_Status = params.context.componentParent.Loan_Status;
        // return isgrideditable(true, Loan_Status);
        return true;
      }, width: 120,
      cellClass: function (params) {

        let Loan_Status = params.context.componentParent.Loan_Status;
        return cellclassmaker(CellType.Text, true, '', Loan_Status);

      },
      valueFormatter: function (params) {

        try {
          let landownderid = params.value.split(',')[0];
          let ands = getLandlords();
          return ands.values.find(p => p.key == Number(landownderid)).value;
        } catch {
          return "";
        }
      },
      minWidth: 120,
      maxWidth: 120,
      cellEditor: 'autocompleteEditor'
    },
    {
      headerName: 'ST | County',
      headerClass: "options", sortable: true, width: 90,
      field: 'StateCounty'
    },
    {
      headerName: 'FSN',
      headerClass: "options", sortable: true, width: 90,
      field: 'FSN'
    },
    {
      headerName: 'Section',
      headerClass: "options", sortable: true, width: 90,
      field: 'Section'
    },
    {
      headerName: 'Rated',
      headerClass: "options", sortable: true, width: 90,
      field: 'Rated'
    },
    {
      headerName: 'Request',
      field: 'Rent', width: 90,
      editable: true,
      cellClass: cellclassmaker(CellType.Integer, true),
      valueFormatter: currencyFormatter,
      tooltip: function (params) {//This will show valueFormatted if is present, if no just show the value.
        let errors = params.context.componentParent.errors.filter(p => p.index == params.rowIndex && p.column == 'Rent');
        let errortxt = '';
        if (errors != null) {
          errors.forEach(element => {
            if (element.type == '1') {
              errortxt = 'Required Field';
            }
            if (element.type == '2') {
              errortxt = errortxt + 'Requested Amount more than Available';
            }

          });

          return errortxt;
        }
        return null;
      }
    },
    {
      headerName: 'Reimb',
      field: 'Reimbursement', width: 50,
      cellRenderer: "checkbox", suppressSorting: true,
      cellRendererParams: { edit: true },
      cellClass: cellclassmaker(CellType.Text, true, ' center'),

    },
    {
      headerName: '',
      field: '', width: 50, cellClass: ['material-icons delete'], cellRenderer: "deletecolumn"



    }
  ];
  style: any = {
    marginTop: '10px',
    width: '700px',
    boxSizing: 'border-box'
  };
  loanobject: loan_model;
  rowdata: any;

  //Validations
  descFormControl: FormControl;
  reqharvestformControl: FormControl;
  //
  constructor(
    public dialogRef: MatDialogRef<DisbursepopupComponent>,
    private dataservice: DataService,
    public alertify: AlertifyService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ngZone: NgZone
  ) {
    this.formdata = data.disbursement as Loan_Disburse;
    try {
      this.disabled = this.formdata.History[this.formdata.History.length - 1].Approval_Status > 0;
      this.formdata.FC_PopupStatus = this.disabled == true ? 1 : 0;
    } catch {
      this.disabled = false;
    }

    this.rowdata = data.rowdata;
    if (this.formdata.RentDetails == null) {
      this.formdata.RentDetails = [];
    }
    this.rowData = new Array<any>();
    this.components = {
      numericCellEditor: getNumericCellEditor(),
      alphaNumericCellEditor: getAlphaNumericCellEditor()

    };


    this.loanobject = this.dataservice.localstorageservice.retrieve(environment.loankey);
    this.context = { componentParent: this };

    let hrvsdst = this.formdata.Details.find(p => p.Budget_Expense_ID == 48);
    if (this.formdata.Special_ACH_Ind == true) {
      this.radiostatus = 0;
    }
    if (this.formdata.Special_COC_Ind == true) {
      this.radiostatus = 1;
    }
    if (this.formdata.Special_CRP_Ind == true) {
      this.radiostatus = 2;
    }
    this.harvestrent = hrvsdst == undefined ? 0 : hrvsdst.Disburse_Detail_Requested_Amount;
    this.getdataforgrid();
    this.rentclass = this.getRentclass();
    setTimeout(() => {
      setrrors(this.errors);
    }, 500);

  }

  ngOnInit() {
    this.descFormControl = new FormControl('', [
      this.HarvestDescValidator()
    ]);
    this.reqharvestformControl = new FormControl('', [
      this.HarvestDescValidator()
    ]);
  }
  onOkClick(): void {
    this.aseesspopupstatus();
    this.dialogRef.close({ data: this.formdata, code: this.formdata.FC_PopupStatus });
  }
  public isDisabled() {
    return this.Get_Landowner_Farm_List().length == 0;
   }
  methodFromcheckbox(event) {
    this.formdata.RentDetails[event.id].Reimbusement = event.value;
  }
  public radiostatus = 0;
  radioChange($event) {
    switch ($event.value) {
      case 0:
        this.formdata.Special_ACH_Ind = true;
        this.formdata.Special_COC_Ind = false;
        this.formdata.Special_CRP_Ind = false;

        break;
      case 1:
        this.formdata.Special_ACH_Ind = false;
        this.formdata.Special_COC_Ind = true;
        this.formdata.Special_CRP_Ind = false;
        break;
      case 2:
        this.formdata.Special_ACH_Ind = false;
        this.formdata.Special_COC_Ind = false;
        this.formdata.Special_CRP_Ind = true;
        break;
      default:
        break;
    }


  }
  onGridReady(params) {

    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    //these two methods remove side option toolbar , sets column , removes header menu
    removeHeaderMenu(this.gridApi);
    toggletoolpanel(false, this.gridApi);
    //save settings here
    params.api.sizeColumnsToFit();
  }
  getdataforgrid(): any {

    if (this.formdata.RentDetails) {
      this.rowData = [];
      this.formdata.RentDetails.filter(p => p.ActionStatus != 3).forEach(element => {
        let row: any = {};
        row.Landowner = element.Landowner_ID + "," + element.Farm_ID;
        let farm = this.loanobject.Farms.find(p => p.Farm_ID == Number(element.Farm_ID));
        if (farm != null) {
          row.StateCounty = farm.FC_State_Name + " | " + farm.FC_County_Name;
          row.FSN = farm.FSN;
          row.Section = farm.Section;
          row.Rated = farm.Rated;
          row.Rent = element.Cash_Rent_Amount;
          row.Reimbursement = element.Reimbusement;
        }
        this.rowData.push(row);
      });
    }
  }

  rowvaluechanged($event) {
    let item = this.formdata.RentDetails[$event.rowIndex];
    if ($event.column.colId != "Rent") {
      item.Landowner_ID = $event.data.Landowner.split(',')[0];
      item.Farm_ID = $event.data.Landowner.split(',')[1];
      item.Cash_Rent_Amount = $event.data.Rent;
    } else {
      item.Cash_Rent_Amount = $event.data.Rent;
    }
    if (item.ActionStatus != 1) {
      item.ActionStatus = 2;
    }
    this.setcashrent();
    this.getdataforgrid();
    this.rentclass = this.getRentclass();

    this.gridApi.refreshCells();
    setTimeout(() => {
      setrrors(this.errors);
    }, 200);

  }

  setcashrent() {

    let value = 0;
    this.formdata.RentDetails.forEach(element => {
      value = value + Number(element.Cash_Rent_Amount);
    });
    if (this.formdata.Details.find(p => p.Budget_Expense_ID == 15) != null) {
      this.formdata.Details.find(p => p.Budget_Expense_ID == 15).Disburse_Detail_Requested_Amount = value;
    } else {
      let dt = new Loan_Disburse_Detail();
      dt.Budget_Expense_ID = 15;
      dt.Disburse_Detail_Requested_Amount = Number(value);
      if (this.formdata.Details == null) {
        this.formdata.Details = new Array<Loan_Disburse_Detail>();
      }
      dt.Status = 0;
      dt.ActionStatus = 1;
      this.formdata.Details.push(dt);
    }
  }
  addrow() {

    if (!this.isDisabled()) {
    let p = new Loan_Disburse_Rent_Detail();
    p.ActionStatus = 1;
    this.formdata.RentDetails.push(p);
    this.getdataforgrid();
    }
  }
  savedetails() {

  }
  onNoClick() {
    this.aseesspopupstatus();
    this.dialogRef.close({ data: false, code: this.formdata.FC_PopupStatus });
  }

  frameworkcomponents = {
    selectEditor: SelectEditor,
    autocompleteEditor: DisburseAutoCompleteEditor,
    deletecolumn: DeleteButtonRenderer,
    checkbox: CheckboxCellComponent
  };

  DeleteClicked(rowindex, data) {
    ;
    this.alertify.confirm("Confirm", "Do you Really Want to Delete this Record?").subscribe(res => {
      if (res == true) {
        let detail = this.formdata.RentDetails[rowindex];
        if (detail.Loan_Disburse_Rent_Detail_ID != null) {
          this.formdata.RentDetails[rowindex].ActionStatus = 3;
        } else {
          if (this.formdata.RentDetails.length == 1) {
            this.formdata.RentDetails = [];
          } else {
            this.formdata.RentDetails = this.formdata.RentDetails.splice(rowindex, 1);
          }
        }
        this.getdataforgrid();
      }
    });

  }
  Get_Landowner_Farm_List() {

    let selection = [];

    //let landlords = this.loanobject.Association.filter(p => p.Assoc_Type_Code == 'LLD');

    this.loanobject.Farms.forEach(farm => {
        if (farm.Owned != 1) {
          let Assoc_ID = 0;
          let Landowner = this.loanobject.Association.filter(p => p.Assoc_Type_Code == 'LLD').find(p => p.Assoc_Name == farm.Landowner);
          if (Landowner != null) {
            Assoc_ID = Landowner.Assoc_ID;
          }
          selection.push({
          farmState: farm.FC_State_Code,
          farmStateName: farm.FC_State_Name,
          farmcounty: farm.Farm_County_ID,
          farmcountyname: farm.FC_County_Name,
          FSN: farm.FSN,
          section: farm.Section,
          rated: farm.Rated,
          Landowner: farm.Landowner,
          key: farm.Farm_ID,
          Code: `${Assoc_ID},${farm.Farm_ID}`,
          value: `${farm.Landowner == "" ? '_' : farm.Landowner} | ${farm.FC_State_Name} | ${farm.FC_County_Name} | ${farm.Section} | ${farm.Rated}`
        });
      }
    selection = _.sortBy(selection, ['Landowner', 'farmState', 'farmcounty', 'FSN']);
  });

    return selection;

}

  checkforerrorsingrid() {
    this.rowData.forEach(element => {

    });
  }
  aseesspopupstatus() {
    if (this.disabled == true) {
      this.formdata.FC_PopupStatus = 1;
    } else {
      if (this.payment == 0 && this.harvest == 0 && this.cashrent == 0 && this.other == 0) {

        this.formdata.FC_PopupStatus = 0;
      } else
        if ((this.payment == 2 || this.payment == 0) && (this.harvest == 2 || this.harvest == 0) && (this.cashrent == 2 || this.cashrent == 0) && (this.other == 2 || this.other == 0)) {

          this.formdata.FC_PopupStatus = 2;
        } else
          if (this.payment == 1 || this.harvest == 1 || this.cashrent == 1 || this.other == 1) {
            this.formdata.FC_PopupStatus = 1;
          }
    }
  }
  private payment = 0;
  private cashrent = 0;
  private harvest = 0;
  private other = 0;
  //Classes For Validations
  getpaymentclass() {

    if (this.formdata.Special_ACH_Ind == true) {
      this.payment = 0;
      this.aseesspopupstatus();
      return 'pencil-orange';
    }
    if (this.formdata.Special_COC_Ind == true || this.formdata.Special_CRP_Ind == true) {
      this.payment = 2;
      this.aseesspopupstatus();
      return 'pencil-blue';
    } else {
      this.payment = 0;
      this.aseesspopupstatus();
      return 'pencil-orange';
    }
  }

  getotherclass() {

    if (this.formdata.Special_Comment != "" && this.formdata.Special_Comment != undefined) {
      this.other = 2;
      return 'pencil-blue';
    } else {
      this.other = 0;
      return 'pencil-orange';
    }
  }

  currencyInputChanged(value) {
    let num = value.replace(/[$,]/g, "");
    return Number(num);
  }
  harvestChange(event) {
    let harvestvalue = this.formdata.Details.find(p => p.Budget_Expense_ID == 48);
    let value = Number(event.target.value.replace('$', ''));
    if (harvestvalue != null) {
      harvestvalue.Disburse_Detail_Requested_Amount = value;
      let available = 0;
      let expensedetail = this.rowdata.filter(p => p.ExpenseID == 48)[0];
      expensedetail['disburse_' + this.formdata.Loan_Disburse_ID] = value;
      let sumofinitial = 0;
      for (let key in expensedetail) {
        if (key.includes('disburse')) {
          sumofinitial += expensedetail[key];
        }
      }
      expensedetail.Available = expensedetail.Commit - sumofinitial;
    } else {
      let dt = new Loan_Disburse_Detail();
      dt.Budget_Expense_ID = 48;
      dt.Disburse_Detail_Requested_Amount = Number(event.target.value);
      if (this.formdata.Details == null) {
        this.formdata.Details = new Array<Loan_Disburse_Detail>();
      }
      dt.Status = 0;
      dt.ActionStatus = 1;
      this.formdata.Details.push(dt);
    }
    this.harvestrent = value;
  }

  isharvestwarning = false;
  getHarvestclass() {
    this.isharvestwarning = false;
    let actualcolor = '';
    this.HarvestComment = '';
    this.HarvestRequest = '';
    let harvestvalue = this.formdata.Details.find(p => p.Budget_Expense_ID == 48);
    let harvestcomment = this.formdata.Special_Harvest_Comment == "" ? null : this.formdata.Special_Harvest_Comment;

    if (harvestvalue != null) {
      let actualharvestrow = this.rowdata.filter(p => p.ExpenseID == 48)[0];
      if (actualharvestrow.Available < 0) {
        this.harvest = 0;
        this.aseesspopupstatus();
        //this.HarvestRequest = 'error-disburse';
        this.reqharvestformControl.markAsTouched();
        this.isharvestwarning = true;
        actualcolor = 'pencil-orange';
      }
    } else {
      if (harvestcomment != "" && harvestcomment != null) {
        this.harvest = 0;
        this.aseesspopupstatus();
        this.HarvestRequest = 'error-disburse';
        this.reqharvestformControl.markAsTouched();
        actualcolor = 'pencil-red';
      }
    }

    if (harvestcomment == null && (harvestvalue == null || (harvestvalue.Disburse_Detail_Requested_Amount == null || harvestvalue.Disburse_Detail_Requested_Amount == 0))) {
      this.harvest = 0;
      this.aseesspopupstatus();
      actualcolor = 'pencil-orange';
    } else if (harvestcomment != null) {
      if (harvestvalue != null && harvestvalue.Disburse_Detail_Requested_Amount != null && harvestvalue.Disburse_Detail_Requested_Amount > 0) {
        this.harvest = 2;
        this.aseesspopupstatus();
        actualcolor = 'pencil-blue';
      } else {
        this.harvest = 0;
        this.aseesspopupstatus();
        this.HarvestRequest = 'error-disburse';
        this.reqharvestformControl.markAsTouched();
        actualcolor = 'pencil-orange';
      }
    } else if (harvestcomment == null) {
      if (harvestvalue != null && harvestvalue.Disburse_Detail_Requested_Amount != null && harvestvalue.Disburse_Detail_Requested_Amount > 0) {
        this.harvest = 1;
        this.aseesspopupstatus();
        this.HarvestComment = 'error-disburse';
        this.descFormControl.markAsTouched();
        actualcolor = 'pencil-red';
      } else {
        this.harvest = 0;
        this.aseesspopupstatus();
        actualcolor = 'pencil-orange';
      }
    }
    return actualcolor;
  }
  public harvestrent;
  public errors = [];
  rentclass = '';

  //Type1 -- Required
  //Type2 -- Exceeds
  getRentclass() {

    this.errors = [];
    let value = 0;
    this.formdata.RentDetails.forEach(element => {
      value = value + Number(element.Cash_Rent_Amount);
    });
    let cashrent = this.rowdata.find(p => p.ExpenseID == 15).Available;
    if (value > cashrent) {
      this.cashrent = 1;
      this.aseesspopupstatus();
      //return 'pencil-red'
    }
    let effective = 0;
    this.formdata.RentDetails.filter(p => p.ActionStatus != 3).forEach(element => {
      //Nul Checks
      if ((element.Landowner_ID == null || element.Landowner_ID == '_') && Number(element.Cash_Rent_Amount == undefined ? 0 : element.Cash_Rent_Amount) == 0) {
        effective = 1;
        this.errors.push({ index: this.formdata.RentDetails.indexOf(element), column: 'Landowner', type: '1' });
        this.errors.push({ index: this.formdata.RentDetails.indexOf(element), column: 'Rent', type: '1' });
      } else {
        if ((element.Landowner_ID != null && element.Landowner_ID != '_')) {
          if (Number(element.Cash_Rent_Amount == undefined ? 0 : element.Cash_Rent_Amount) == 0) {
            this.errors.push({ index: this.formdata.RentDetails.indexOf(element), column: 'Rent', type: '1' });
            effective = 1;
          } else {
            if (effective != 1) {
              effective = 2;
            }
          }
        } else {
          this.errors.push({ index: this.formdata.RentDetails.indexOf(element), column: 'Landowner' });
          if (Number(element.Cash_Rent_Amount == undefined ? 0 : element.Cash_Rent_Amount) == 0) {
            this.errors.push({ index: this.formdata.RentDetails.indexOf(element), column: 'Rent', type: '1' });
          }
        }
      }
      //Farm-increased Rent Checks
      try {
        let farm = this.loanobject.Farms.find(p => p.Farm_ID == Number(element.Farm_ID));
        if (Number(element.Cash_Rent_Amount) > (farm.Cash_Rent_Total - farm.Cash_Rent_Waived_Amount)) {
          this.errors.push({ index: this.formdata.RentDetails.indexOf(element), column: 'Rent', type: '2' });
          effective = 1;
        }
      } catch {

      }
    });
    this.errors = _.uniq(this.errors);
    switch (effective) {
      case 0:
        this.cashrent = 0;
        this.aseesspopupstatus();
        return 'pencil-orange';

      case 1:
        this.cashrent = 1;
        this.aseesspopupstatus();
        return 'pencil-red';

      case 2:
        this.cashrent = 2;
        this.aseesspopupstatus();
        return 'pencil-blue';


      default:
        return 'pencil-orange';
    }

  }

  HarvestDescValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (control.errors != undefined && control.errors.required == true) {
        return { 'ageRange': true };
      }
      return null;
    };
  }



  //Validation Classes for Objects
  public HarvestComment: string = '';
  public HarvestRequest: string = '';



}

function getLandlords() {

  let loanobj = JSON.parse('[' + window.localStorage.getItem('ng2-webstorage|currentselectedloan') + ']')[0] as loan_model;
  let values = [];

  loanobj.Association.filter(p => p.Assoc_Type_Code == 'LLD' && p.ActionStatus != 3).forEach(element => {
    values.push({ value: element.Assoc_Name, key: element.Assoc_ID });
  });

  return { values: values };

}
function getFarms(id) {
  try {
    let loanobj = JSON.parse('[' + window.localStorage.getItem('ng2-webstorage|currentselectedloan') + ']')[0] as loan_model;
    let values = [];
    let landlord = loanobj.Association.filter(p => p.Assoc_Type_Code == 'LLD' && p.ActionStatus != 3).find(p => p.Assoc_ID == id).Assoc_Name;
    loanobj.Farms.filter(p => p.Landowner == landlord && p.ActionStatus != 3).forEach(element => {
      values.push({ value: element.FSN, key: element.Farm_ID });
    });

    return { values: values };
  } catch {
    return { values: [] };
  }
}
function setrrors(errors: any[]) {

  errors.forEach(obj => {
    try {

      let filter = Array.prototype.filter;
      let selectedelements = document.querySelectorAll('[row-index="' + obj.index + '"]');
      let filtered = selectedelements;
      Array.from(filtered).forEach(element => {
        try {
          let cell = element.querySelector('[col-id="' + obj.column + '"]');

          cell.classList.add("verification-error");

        } catch (ex) {

        }
      });

    } catch (ex) {

    }
  });
}



