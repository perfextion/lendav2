import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { MembersComponent } from '@lenda/components/committee/members/members.component';
import { SelectedCommitteeStatus } from '@lenda/models/committee-status/committee-status.model';
import { MatCheckboxChange } from '@angular/material';

@Component({
  selector: 'vote-button-cell',
  templateUrl: './vote-button-column.component.html',
  styleUrls: ['./vote-button-column.component.scss']
})
export class VoteButtonColumnRenderer implements ICellRendererAngularComp {
  params: any;
  isCrossCollaterized: boolean;
  crollCol: boolean;
  memberComponent: MembersComponent;

  canVote: boolean;

  agInit(params: any): void {
    this.params = params;
    this.memberComponent = this.params.context.componentParent;
    this.isCrossCollaterized = this.memberComponent.isCrossCollaterized;

    this.canVote = !this.memberComponent.approved && !this.memberComponent.declined;
  }

  get rowData() {
    return this.memberComponent.rowData;
  }

  get isVotePending() {
    try {
      return this.rowData.some(
        c => c.CM_Role == 1 && (c.Vote == 0 || c.Vote == 3)
      );
    } catch {
      return false;
    }
  }

  public vote(buttonCode: number) {
    if (this.isDisable || !this.canVote) {
      return false;
    }

    const defaultVote: SelectedCommitteeStatus = {
      committeeStatusId: buttonCode,
      innerCommitteeStatusId: 0
    };

    if (buttonCode == 2) {
      this.memberComponent.declineVoteAlert().subscribe(res => {
        if (res) {
          this.memberComponent.recordVote(
            defaultVote,
            this.params.data,
            this.crollCol
          );
        }
      });
    } else {
      this.memberComponent.recordVote(
        defaultVote,
        this.params.data,
        this.crollCol
      );
    }
  }

  rescindVote(committeeMemberId: number, User_ID: number) {
    if (!this.canVote) {
      return false;
    }

    this.memberComponent.rescindVote(committeeMemberId, User_ID, this.crollCol);
  }

  refresh(): boolean {
    return false;
  }

  openEmojiDialog() {
    if (!this.canVote) {
      return false;
    }

    this.memberComponent.emojiClicked(this.params.data, this.crollCol);
  }

  public get IsLoggedInUser() {
    return this.params.data.User_ID == this.params.colDef.loggedInUser;
  }

  public get isDisable() {
    return (
      !this.params.colDef.isAdmin &&
      !(this.params.data.User_ID == this.params.colDef.loggedInUser)
    );
  }

  changeCrossCol(event: MatCheckboxChange) {
    this.crollCol = event.checked;
  }

  get canVoteOnLoan(){
    return this.memberComponent.isLoanInVotingCondition;
  }
}
