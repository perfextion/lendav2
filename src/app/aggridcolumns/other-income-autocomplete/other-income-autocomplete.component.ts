import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { OtherIncomeComponent } from '@lenda/components/crop/other-income/other-income.component';
import { NgSelectComponent } from '@ng-select/ng-select';
import * as _ from 'lodash';
import { Ref_Other_Income } from '@lenda/models/ref-data-model';
import { AlertifyService } from '@lenda/alertify/alertify.service';

@Component({
  selector: 'app-other-income-autocomplete',
  templateUrl: './other-income-autocomplete.component.html',
  styleUrls: ['./other-income-autocomplete.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OtherIncomeAutocompleteCellEditor
  implements ICellEditorAngularComp {
  dropdownList: Array<Ref_Other_Income> = [];
  selectedIncome;
  params: any;
  previousWidth = 0;
  previousHeight = 0;

  canAdd = false;

  @ViewChild('ngSelect') selectEL: NgSelectComponent;
  component: OtherIncomeComponent;

  constructor(private alertify: AlertifyService) {}

  agInit(params: any): void {
    this.previousHeight = params.eGridCell.style.height;
    params.eGridCell.style.height = 'auto';

    this.params = params;
    this.component = params.context.componentParent;

    let Ref_Other_Incomes = this.component.Ref_Other_Incomes;

    Ref_Other_Incomes.forEach(income => {
      this.dropdownList.push(income);
    });

    this.dropdownList = _.sortBy(this.dropdownList, a => a.Sort_Order);

    if(this.params.node.data.Loan_Other_Income_ID) {
      this.selectedIncome = this.params.node.data.Other_Income_Name;
    }
  }

  getValue(): any {
    this.params.eGridCell.style.height = this.previousHeight;
    return this.selectedIncome && _.trim(this.selectedIncome).length > 0
      ? this.selectedIncome
      : '';
  }

  isPopup(): boolean {
    return false;
  }

  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  closeDropdown() {
    this.params.stopEditing();
  }

  scroll = (): void => {};

  itemChange(otherIncome: any) {
    let isUpdate = false;

    let text = otherIncome as any;

    if(Object.prototype.toString.call(text) == '[object String]') {
      otherIncome = {
        Other_Income_ID: 0,
        Other_Income_Name: text
      }
    }

    if(!otherIncome.Other_Income_ID) {
      let ExistingRefdata = this.component.Ref_Other_Incomes.find(a => a.Other_Income_Name == otherIncome.Other_Income_Name);

      if(ExistingRefdata) {
        otherIncome = ExistingRefdata;
      }
    }

    isUpdate = this.Add_Other_Income(otherIncome, isUpdate);

    if (isUpdate) this.params.api.refreshCells();
  }

  private Add_Other_Income(otherIncome: Ref_Other_Income, isUpdate: boolean) {
    if (otherIncome.Other_Income_ID) {
      this.params.node.data.Other_Income_ID = otherIncome.Other_Income_ID;
      isUpdate = true;
    } else {
      this.params.node.data.Other_Income_ID = 0;
    }

    if (otherIncome.Other_Income_Name) {
      this.params.node.data.Other_Income_Name = otherIncome.Other_Income_Name;
      isUpdate = true;
    }

    if (otherIncome.Sort_Order) {
      this.params.node.data.Sort_Order = otherIncome.Sort_Order;
      isUpdate = true;
    }

    return isUpdate;
  }

  onClickedOutside(event) {
    // this.params.stopEditing();
  }
}
