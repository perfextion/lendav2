import {Component, ViewContainerRef, ViewChild} from "@angular/core";
import {ICellEditorAngularComp} from "ag-grid-angular";
import { isNumber } from "util";
import * as _ from 'lodash';
@Component({
    selector: 'radiobox-cell-editor',
    templateUrl: './radiobox-cell-editor.html',
    styleUrls: ['./radiobox-cell-editor.scss']
})
export class RadioboxCellEditor implements ICellEditorAngularComp {
    private params: any;
    public selectedValue:any;
    public values=[];
    previousWidth;
    previousHeight;

    @ViewChild('container', {read: ViewContainerRef}) public container;
    ngOnInit(){
        window.addEventListener('scroll', this.scroll, true);
    }
    ngOnDestroy() {
        window.removeEventListener('scroll', this.scroll, true);
    }

    ngAfterViewInit() {

    }

    scroll = ():void => {
    };


    agInit(params: any): void {
        // console.log(params);
        this.params = params;
        this.values=params.values;
        if(isNumber(params.value)) {
            this.selectedValue=parseInt(params.value);
        }
        else {
            this.selectedValue=params.value;
        }

        this.previousWidth = params.eGridCell.style.width;
        this.previousHeight = params.eGridCell.style.height;

        if ( parseInt(this.previousWidth + "") < 100)  {
            params.eGridCell.style.width = "100px";
        }
    }

     getValue():any {
        this.params.eGridCell.style.width = this.previousWidth;
        this.params.eGridCell.style.height = this.previousHeight;
        return this.selectedValue;
    }

    isPopup(): boolean {
        return false;
    }

    onKeyDown(event): void {
    }
}
