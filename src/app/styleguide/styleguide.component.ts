import { Component, OnInit } from '@angular/core';
import { ExcelToJsonService } from "@lenda/services/excel-to-json.service";
import * as XLSX from 'ts-xlsx';

@Component({
  selector: 'app-styleguide',
  templateUrl: './styleguide.component.html',
  styleUrls: ['./styleguide.component.scss'],
  providers: [ExcelToJsonService]
})
export class StyleguideComponent implements OnInit {
  file: File;
  arrayBuffer: any;
  fileReader: FileReader = new FileReader();

  constructor(private excelToJsonService: ExcelToJsonService) { }

  ngOnInit() {
    this.fileReader.onload = (e) => {
      console.log(this.excelToJsonService.attachFileReaderEvent(this.fileReader));
    }
  }

  incomingfile(event) {
    this.file = event.target.files[0];
  }

  Upload() {
    this.fileReader.readAsArrayBuffer(this.file);
  }
}
