import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevPracticesComponent } from './dev-practices.component';

describe('DevPracticesComponent', () => {
  let component: DevPracticesComponent;
  let fixture: ComponentFixture<DevPracticesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevPracticesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevPracticesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
