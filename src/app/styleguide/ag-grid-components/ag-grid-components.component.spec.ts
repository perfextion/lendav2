import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgGridComponentsComponent } from './ag-grid-components.component';

describe('AgGridComponentsComponent', () => {
  let component: AgGridComponentsComponent;
  let fixture: ComponentFixture<AgGridComponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgGridComponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgGridComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
