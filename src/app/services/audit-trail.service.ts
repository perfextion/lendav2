import { Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { Observable } from 'rxjs/Observable';

import { environment } from '@env/environment.prod';

import {
  LoanAuditTrail,
  Loan_Audit_Trail_Collection,
  Audit_Trail_Item,
  Audit_Item
} from '@lenda/models/audit-trail/audit-trail';
import { ResponseModel } from '@lenda/models/resmodels/reponse.model';
import { loan_model } from '@lenda/models/loanmodel';
import { LoanMaster } from '@lenda/models/ref-data-model';

import { ApiService } from './api.service';

/**
 * ===
 * Audit Trail Service
 * ===
 */
@Injectable()
export class LoanAuditTrailService {
  constructor(
    private api: ApiService,
    private localStorage: LocalStorageService
  ) {}

  addAuditTrail(loanAudit: LoanAuditTrail): Observable<ResponseModel> {
    const userId = this.localStorage.retrieve(environment.uid);

    loanAudit.User_ID = userId;
    loanAudit.Date_Time = new Date();

    if (!loanAudit.Audit_Field_ID) {
      loanAudit.Audit_Field_ID = '';
    }

    const route = '/api/Loan/AddAuditTrail';

    return this.api.post(route, loanAudit);
  }

  /**
   * Add Audit Trail from Text and ID
   */
  addAuditTrailText(
    auditText: string,
    type: string,
    id?: string,
    loan?: loan_model
  ) {
    if (!loan) {
      loan = this.localStorage.retrieve(environment.loankey);
    }

    const loanMaster: LoanMaster = loan.LoanMaster;

    const audit = <LoanAuditTrail>{
      Audit_Field_ID: id,
      Audit_Trail_Text: auditText,
      Loan_Status: loanMaster.Loan_Status,
      Loan_ID: loanMaster.Loan_Full_ID,
      Loan_Full_ID: loanMaster.Loan_Full_ID,
      Audit_Trail_Type: type,
      Loan_Seq_Num: loanMaster.Loan_Seq_num.toString()
    };

    return this.addAuditTrail(audit);
  }

  addAuditTrailAsync(
    auditText: string,
    type: string,
    id?: string,
    loan?: loan_model
  ) {
    this.addAuditTrailText(auditText, type, id, loan).subscribe(_ => {});
  }

  addAuditTrailCollection(
    loanAudit: Loan_Audit_Trail_Collection
  ): Observable<ResponseModel> {
    const userId = this.localStorage.retrieve(environment.uid);

    loanAudit.User_ID = userId;
    loanAudit.Date_Time = new Date();

    const route = '/api/Loan/AddAuditTrailCollection';

    return this.api.post(route, loanAudit);
  }

  addAuditTrailTexts(texts: Array<Audit_Item>, loan?: loan_model) {
    if (texts && texts.length > 0) {
      if (!loan) {
        loan = this.localStorage.retrieve(environment.loankey);
      }

      const loanMaster: LoanMaster = loan.LoanMaster;

      const auditItems = texts.map(item => {
        return <Audit_Trail_Item>{
          Audit_Field_ID: item.id || '',
          Audit_Trail_Text: item.text,
          Audit_Trail_Type: item.type
        };
      });

      const audit = <Loan_Audit_Trail_Collection>{
        Audit_Trails: auditItems,
        Loan_Status: loanMaster.Loan_Status,
        Loan_ID: loanMaster.Loan_Full_ID,
        Loan_Full_ID: loanMaster.Loan_Full_ID,
        Loan_Seq_Num: loanMaster.Loan_Seq_num.toString()
      };

      return this.addAuditTrailCollection(audit);
    }
  }

  addAuditTrailTextsAsync(texts: Array<Audit_Item>, loan?: loan_model) {
    if (texts && texts.length > 0) {
      this.addAuditTrailTexts(texts, loan).subscribe(_ => {});
    }
  }
}
