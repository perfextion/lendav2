import {
  RMA_Insurance_Offer,
  RMA_Insurance_Offer_Model,
  LoanMaster,
  RefDataModel,
  Ref_Crop_Basis,
  Ref_Option_Adj_Lookup,
  RefCrop
} from '@lenda/models/ref-data-model';
import { Loan_Crop_Practice, loan_model, BorrowerEntityType, borrower_model } from '@lenda/models/loanmodel';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';
import { Loan_Policy } from '@lenda/models/insurancemodel';

export enum SearchDirection {
  LTR,
  RTL
}

export interface IDictionary<T> {
  [key: string]: T;
}

/**
 * Relax Key Search in Dictionary
 * ====
 * Left To Right
 *
 */
export function RKSFind(
  key: string,
  discounts: IDictionary<number>,
  default_value = 100
) {
  let disc = Object.keys(discounts).find(
    k => k.toLowerCase() == key.toLowerCase()
  );

  if (disc) {
    return discounts[disc];
  }

  if (key.includes('_')) {
    return RKSFind(
      key.substring(0, key.lastIndexOf('_')),
      discounts,
      default_value
    );
  }

  return default_value;
}

/**
 * Relax Key Search in Dictionary
 * ====
 * Right to Left
 *
 */
export function RKSFindX(
  key: string,
  discounts: { [key: string]: number },
  default_value = 100,
  dir = SearchDirection.RTL
) {
  let disc = Object.keys(discounts).find(
    k => k.toLowerCase() == key.toLowerCase()
  );

  if (disc) {
    return discounts[disc];
  }

  if (key.includes('_')) {
    let newKey: string;

    if (dir == SearchDirection.RTL) {
      newKey = key.substring(0, key.lastIndexOf('_'));
    } else {
      newKey = key.substring(key.indexOf('_') + 1);
    }

    return RKSFindX(newKey, discounts, default_value, dir);
  }

  return default_value;
}

/**
 * Relax Key Search
 * ====
 * RMA Insurance Offer Data
 *
 */
export function RKS_RMAFind(
  key: string,
  State_ID: number,
  County_ID: number,
  rma_data: Array<RMA_Insurance_Offer>
): RMA_Insurance_Offer_Model {
  let disc = rma_data.find(
    a =>
      a.RMA_Price_Key.toLowerCase() == key.toLowerCase() &&
      a.State_ID == State_ID &&
      a.County_ID == County_ID
  );

  if (disc) {
    return <RMA_Insurance_Offer_Model>{
      RMA_Price_Key: key,
      MPCI_Base_Price: disc.MPCI_Base_Price || 0,
      MPCI_Harvest_Price: disc.MPCI_Harvest_Price || 0,
      Price_Established: disc.Price_Established || 0
    };
  } else {
    disc = rma_data.find(
      a =>
        a.RMA_Price_Key.toLowerCase() == key.toLowerCase() &&
        a.State_ID == State_ID &&
        a.County_ID == 0
    );

    if (disc) {
      return <RMA_Insurance_Offer_Model>{
        RMA_Price_Key: key,
        MPCI_Base_Price: disc.MPCI_Base_Price || 0,
        MPCI_Harvest_Price: disc.MPCI_Harvest_Price || 0,
        Price_Established: disc.Price_Established || 0
      };
    } else {
      disc = rma_data.find(
        a =>
          a.RMA_Price_Key.toLowerCase() == key.toLowerCase() &&
          a.State_ID == 0 &&
          a.County_ID == 0
      );

      if (disc) {
        return <RMA_Insurance_Offer_Model>{
          RMA_Price_Key: key,
          MPCI_Base_Price: disc.MPCI_Base_Price || 0,
          MPCI_Harvest_Price: disc.MPCI_Harvest_Price || 0,
          Price_Established: disc.Price_Established || 0
        };
      } else {
        if (key.includes('_')) {
          return RKS_RMAFind(key.substring(0, key.lastIndexOf('_')), State_ID, County_ID, rma_data);
        }
      }
    }
  }

  return new RMA_Insurance_Offer_Model(key);
}

export function RKS_RMAADJUSTMENTFind(key: string, rma_data: Array<Ref_Option_Adj_Lookup>) {
  let data= rma_data.find(p => p.Option_Adj_Key == key);

  if(data != null) {
    return data.Option_Adj_Price;
  } else {
    return 0;
  }
}

export function RKS_RMAADJUSTMENTPercFind(key: string, rma_data: Array<Ref_Option_Adj_Lookup>) {
  let data= rma_data.find(p => p.Option_Adj_Key == key);

  if(data != null) {
    return data.Option_Adj_Percent;
  } else {
    return 0;
  }
}

/**
 * Relax Key Search
 * ====
 * RMA Insurance Offer Data
 *
 * Right to Left
 */
export function RKS_RMAFindX(
  key: string,
  rma_data: Array<RMA_Insurance_Offer>,
  dir = SearchDirection.RTL
): RMA_Insurance_Offer_Model {
  let disc = rma_data.find(
    a => a.RMA_Price_Key.toLowerCase() == key.toLowerCase()
  );

  if (disc) {
    return <RMA_Insurance_Offer_Model>{
      RMA_Price_Key: key,
      MPCI_Base_Price: disc.MPCI_Base_Price || 0,
      MPCI_Harvest_Price: disc.MPCI_Harvest_Price || 0,
      Price_Established: disc.Price_Established || 0
    };
  }

  if (key.includes('_')) {
    let newKey: string;

    if (dir == SearchDirection.RTL) {
      newKey = key.substring(0, key.lastIndexOf('_'));
    } else {
      newKey = key.substring(key.indexOf('_') + 1);
    }

    return RKS_RMAFindX(newKey, rma_data, dir);
  }

  return new RMA_Insurance_Offer_Model(key);
}

/**
 * Get Crop Name By Crop Practice with Column display settings
 */
export function Get_Crop_Name_By_Crop_Practice(
  bgt: Loan_Crop_Practice,
  column_settings: ColumnsDisplaySettings
) {
  let name = bgt.FC_CropName;

  if (column_settings.cropType) {
    name = name + ' | ' + bgt.FC_Crop_Type;
  }

  name = name + ' | ' + bgt.FC_PracticeType;

  if (column_settings.cropPrac) {
    name = name + ' | ' + bgt.FC_Crop_Practice_Type;
  }

  return name;
}

/**
 * Get Crop Name By Crop Practice
 */
export function Get_Crop_Name_By_Crop_Practice_Without_Column_Settings(
  bgt: Loan_Crop_Practice
) {
  let name = bgt.FC_CropName;

  name = name + ' | ' + bgt.FC_Crop_Type;
  name = name + ' | ' + bgt.FC_PracticeType;
  name = name + ' | ' + bgt.FC_Crop_Practice_Type;
  return name;
}

/**
 * Return Array of Year based on Crop Year
 * @param cropYear Crop Year
 */
export function CropYears(cropYear: number) {
  return Get_Range(cropYear, 7);
}

/**
 * Generate Array From Range
 * @param start Start Number
 * @param count End Number
 */
export function Get_Range(start: number, count: number) {
  return Array.from({ length: count },(v, k) => {
      return start - k - 1;
  });
}

export function To_Friendly_Name(str: string) {
  if(str) {
    return str.split(new RegExp('[_&]', 'g')).join(' ');
  }
  return str;
}

export function To_Friendly_Policy_Name(str: string) {
  if(str) {
    return str.split(new RegExp('[_&]', 'g')).join(' | ');
  }
  return str;
}

export function To_Friendly_CSS_Name(str: string) {
  if(str) {
    return str.split(new RegExp('[$@&, ]', 'g')).join('_');
  }
  return str;
}

export function precise_round(num, decimals) {
  let t = Math.pow(10, decimals);
  num = parseFloat(num);

  return (Math.round((num * t) + (decimals > 0 ? 1 : 0) * (Math.sign(num) * (10 / Math.pow(100, decimals)))) / t);
}


export function Get_Distributor_Name(loan: loan_model, ellipsis = true) {
  if(loan) {
    let lm: LoanMaster = loan.LoanMaster;

   if(lm) {
    if(lm.Distributor_ID > 0 && lm.Distributor_Name) {
      if(lm.Distributor_Name.length > 12 && ellipsis) {
        return lm.Distributor_Name.substr(0, 13) + '...';
      }
      return lm.Distributor_Name;
    }
    return 'Distributor';
   }
    return 'Distributor';
  }

  return 'Distributor';
}

export function Get_Borrower_Name(borrower: borrower_model | LoanMaster) {
  let name = '';

  if(
    borrower.Borrower_Entity_Type_Code == BorrowerEntityType.Individual ||
    borrower.Borrower_Entity_Type_Code == BorrowerEntityType.IndividualWithSpouse
  ) {
    if(borrower.Borrower_Last_Name) {
      name =  borrower.Borrower_Last_Name
    }

    if(borrower.Borrower_First_Name) {
      name += (name ? ', ' : '') + borrower.Borrower_First_Name;
    }

    if(borrower.Borrower_MI) {
      name += ' ' + borrower.Borrower_MI
    }

  } else {
    name = borrower.Borrower_First_Name;
  }

  return name;
}

/**
 * Relax Key Search for Budget
 * @param crop_key Crop Full Key - eg. CRN_0_IRR_IRR
 * @param office_id Office Id of Loan - eg. 2
 * @param region_id Region Id of Loan - eg. 4
 * @param ref_data Reference data
 */
export function Get_Default_Budget(
  crop_key: string,
  office_id: number,
  region_id: number,
  ref_data: RefDataModel
) : {
  crop_key: string,
  office_id: number,
  region_id: number
} {
  let isBudgetExists = ref_data.BudgetDefault.some(
    a =>
      a.Crop_Full_Key == crop_key &&
      a.Z_Office_ID == office_id &&
      a.Z_Region_ID == region_id
  );

  if(isBudgetExists) {
    return {
      crop_key: crop_key,
      office_id: office_id,
      region_id: region_id
    };
  } else {
    isBudgetExists = ref_data.BudgetDefault.some(
      a =>
        a.Crop_Full_Key == crop_key &&
        a.Z_Office_ID == 0 &&
        a.Z_Region_ID == region_id
    );

    if(isBudgetExists) {
      return {
        crop_key: crop_key,
        office_id: 0,
        region_id: region_id
      };
    } else {
      isBudgetExists = ref_data.BudgetDefault.some(
        a =>
          a.Crop_Full_Key == crop_key &&
          a.Z_Office_ID == 0 &&
          a.Z_Region_ID == 0
      );
      if(isBudgetExists) {
        return {
          crop_key: crop_key,
          office_id: 0,
          region_id: 0
        };
      } else {
        if(crop_key.includes('_')) {
          crop_key = crop_key.substring(0, crop_key.lastIndexOf('_'));
          return Get_Default_Budget(crop_key, office_id, region_id, ref_data);
        } else {
          return {
            crop_key: '0_0_0_0',
            office_id: 0,
            region_id: 0
          };
        }
      }
    }
  }
}


export function UTC_NOW() {
  let date = new Date();
  let now_utc =  Date.UTC(
    date.getUTCFullYear(),
    date.getUTCMonth(),
    date.getUTCDate(),
    date.getUTCHours(),
    date.getUTCMinutes(),
    date.getUTCSeconds()
  );

  return new Date(now_utc);
}

/**
 * Trims the string based on max length
 * @param str String
 * @param max_length Max Length
 */
export function Substring(str: string, max_length: number) {
  if(str && str.length > max_length) {
    return str.substring(0, max_length) + '..';
  }
  return str;
}


export function DateDiff(date1: any, date2: any) {
  let ms = +new Date(date1) - (+new Date(date2));

  let  s = Math.floor(ms / 1000);
  let m = Math.floor(s / 60);
  s = s % 60;
  let h = Math.floor(m / 60);
  m = m % 60;
  let d = Math.floor(h / 24);
  h = h % 24;

  return {
    minutes: String(m).padStart(2, '0'),
    hours: String(h).padStart(2, '0'),
    days: d
  }
}

/**
 * MM/dd/yyyy
 * @param strDate Date
 */
export function Get_Formatted_Date(strDate: any) {
  let date = new Date(strDate);
  let d = (date.getMonth() + 1).toString().padStart(2, '0') + '/' + date.getDate().toString().padStart(2, '0') + '/' + date.getFullYear();
  return d;
}

/**
 * Get Crop Basis by Relax Key Search
 */
export function Get_Crop_Basis(
  crop_code: string,
  crop_type: string,
  office_id: number,
  region_id: number,
  ref_data: RefDataModel
): Ref_Crop_Basis {
  try {
    let basis = ref_data.Ref_Crop_Basis_List.find(
      a =>
        a.Crop_Code == crop_code &&
        a.Crop_Type_Code == crop_type &&
        a.Z_Region_ID == region_id &&
        a.Z_Office_ID == office_id
    );

    if(basis) {
      return basis;
    } else {
      basis = ref_data.Ref_Crop_Basis_List.find(
        a =>
          a.Crop_Code == crop_code &&
          a.Crop_Type_Code == crop_type &&
          a.Z_Office_ID == 0 &&
          a.Z_Region_ID == region_id
      );

      if(basis) {
        return basis;
      } else {
        basis = ref_data.Ref_Crop_Basis_List.find(
          a =>
            a.Crop_Code == crop_code &&
            a.Crop_Type_Code == crop_type &&
            a.Z_Office_ID == 0 &&
            a.Z_Region_ID == 0
        );

        if(basis) {
          return basis;
        } else {
          basis = ref_data.Ref_Crop_Basis_List.find(
            a =>
              a.Crop_Code == crop_code &&
              !a.Crop_Type_Code &&
              a.Z_Region_ID == region_id &&
              a.Z_Office_ID == office_id
          );

          if(basis) {
            return basis;
          } else {
            basis = ref_data.Ref_Crop_Basis_List.find(
              a =>
                a.Crop_Code == crop_code &&
                !a.Crop_Type_Code &&
                a.Z_Office_ID == 0 &&
                a.Z_Region_ID == region_id
            );

            if(basis) {
              return basis;
            } else {
              basis = ref_data.Ref_Crop_Basis_List.find(
                a =>
                  a.Crop_Code == crop_code &&
                  !a.Crop_Type_Code &&
                  a.Z_Office_ID == 0 &&
                  a.Z_Region_ID == 0
              );

              if(basis) {
                return basis;
              } else {
                basis = ref_data.Ref_Crop_Basis_List.find(
                  a =>
                    a.Crop_Code == 'DEFAULT' &&
                    a.Z_Office_ID == 0 &&
                    a.Z_Region_ID == 0
                );
                if(basis) {
                  return basis;
                } else {
                  let basis = new Ref_Crop_Basis();
                  basis.Crop_Code = crop_code;
                  basis.Crop_Type_Code = crop_type;
                  basis.Basis_Adj = 0;
                  return basis;
                }
              }
            }
          }
        }
      }
    }

  } catch {
    let basis = new Ref_Crop_Basis();
    basis.Crop_Code = crop_code;
    basis.Crop_Type_Code = crop_type;
    basis.Basis_Adj = 0;
    return basis;
  }

}

/**
 * Get Crop Price by Relax Key Search
 * @param crop_code Crop Code
 * @param cropType Crop Type
 * @param region_id Region ID
 * @param ref_data Reference Data
 */
export function Get_Crop_Price(
  crop_code: string,
  cropType: string,
  region_id: number,
  ref_data: RefDataModel
) {
  try {
    let ref_crop = ref_data.Crops.find(a => a.Crop_Code == crop_code && a.Crop_Type == cropType);
    if(ref_crop) {
      return ref_crop;
    } else {
      ref_crop = ref_data.Crops.find(a => a.Crop_Code == crop_code && !a.Crop_Type);
      if(ref_crop) {
        return ref_crop;
      } else {
        let crop = new RefCrop();
        crop.Crop_Code = crop_code;
        crop.Crop_Type = cropType;
        return crop;
      }
    }
  } catch {
    let crop = new RefCrop();
    crop.Crop_Code = crop_code;
    crop.Crop_Type = cropType;
    crop.Crop_Price = 0;
    return crop;
  }
}

export function Get_Policy_Key(loanObj: loan_model, item: Loan_Policy): string {
  let key =  loanObj.LoanMaster.Crop_Year + '_' + item.Crop_Code;

  if(item.Crop_Type_Code == '' || item.Crop_Type_Code == '-') {
    key += '_0_';
  } else {
    key += ('_' + item.Crop_Type_Code + '_');
  }

  key += (item.Irr_Practice_Type_Code + '_' + item.Crop_Practice_Type_Code);

  return key;
}

export function Get_Policy_Adjustment_Key(loanObj: loan_model, item: Loan_Policy): string {
  let key =  item.Crop_Code + '_' + item.Ins_Plan;

  if(item.Ins_Plan_Type == '') {
    key += '';
  } else {
    key += ('_' + item.Ins_Plan_Type + '_');
    if(item.Option == null) {
      key += '';
    } else {
      key += (item.Option);
    }
  }
  return key;
}

export function Get_Spouse_Name(lm: LoanMaster) {
  let name = lm.Spouse_Last_name + ( lm.Spouse_Last_name ? ', ' : '' ) + lm.Spouse_First_Name + ' ' + lm.Spouse_MI;
  return name;
}


export function ToDictionary<T>(arr: Array<T>, key, value): {[key: string]: T} {
  if(arr && arr.length > 0) {
    let obj = {};

    arr.forEach(i => {
      if(key in i) {
        obj[i[key]] = i[value];
      }
    });
    return obj;
  }
  return {};
}

export function NumberOnly(e, allowDecimal = true) {
  let charCode = (e.which) ? e.which : e.keyCode;
  let value = e.target.value;
  if (allowDecimal) {
    if (((charCode != 46 || (charCode == 46 && value == '')) || value.indexOf('.') != -1) && (charCode < 48 || charCode > 57)) {
      return false;
    }
  } else {
    if (charCode == 13 || (charCode > 31 && (charCode < 48 || charCode > 57))) {
        return false;
    }
  }
}

export function isKeyPressedNumeric(event, value, allowDecimal = true) {
  event = event || window.event;
  const charCode = (typeof event.which == "undefined") ? event.keyCode : event.which;
  const charStr = event.key ? event.key : String.fromCharCode(charCode);
  return isCharNumeric(charStr, value, allowDecimal);
}

export function isCharNumeric(charStr, value, allowDecimal): boolean {
  if(charStr == "Backspace" || charStr == 'ArrowLeft' || charStr == 'ArrowRight' || charStr == 'Delete') {
    return true;
  }

  if(allowDecimal && charStr == "." && value && !value.toString().includes(".")) {
    return true;
  }

  return !!/\d/.test(charStr);
}


export function Get_Collateral_Action_Adj_Ins_Disc(code: string, refData: RefDataModel) {
  let disc = refData.CollateralActionDiscAdj.find(a => a.Collateral_Action_Type_Code == code);
  if(disc) {
    return disc.Collateral_Ins_Disc_Adj_Percent;
  } else {
    return 100;
  }
}

export function Get_Collateral_Action_Adj_Mkt_Disc(code: string, refData: RefDataModel) {
  let disc = refData.CollateralActionDiscAdj.find(a => a.Collateral_Action_Type_Code == code);
  if(disc) {
    return disc.Collateral_Ins_Disc_Adj_Percent;
  } else {
    return 100;
  }
}

export function Get_Collateral_Mkt_Disc(code: string, refData: RefDataModel) {
  let mkt_disc = refData.CollateralMktDisc.find(a => a.Collateral_Category_Code == code);
  if(mkt_disc) {
    return mkt_disc.Disc_Percent;
  } else {
    return 100;
  }
}


export function Get_Ins_Disc(
  refData: RefDataModel,
  plan: string,
  plan_type: string,
  crop_Code?: string,
  crop_type?: string,
  irr_prac?: string,
  crop_prac?: string
) {
  const cropInsDiscs = refData.CollateralInsDisc.filter(a => a.Collateral_Category_Code == 'CRP');

  let ins_disc = cropInsDiscs.find(
    a =>
      a.Ins_Plan_Code.toUpperCase() == plan.toUpperCase() &&
      a.Ins_Type_Code.toUpperCase() == plan_type.toUpperCase() &&
      a.Collateral_Code == crop_Code &&
      a.Collateral_Type_Code == crop_type &&
      a.Irr_Practice_Type_Code == irr_prac &&
      a.Crop_Practice_Type_Code == crop_prac
  );

  if(ins_disc) {
    return ins_disc.Disc_Percent;
  } else {
    ins_disc = cropInsDiscs.find(
      a =>
        a.Ins_Plan_Code.toUpperCase() == plan.toUpperCase() &&
        a.Ins_Type_Code.toUpperCase() == plan_type.toUpperCase() &&
        a.Collateral_Code == crop_Code &&
        a.Collateral_Type_Code == crop_type &&
        a.Irr_Practice_Type_Code == irr_prac
    );

    if(ins_disc) {
      return ins_disc.Disc_Percent;
    } else {
      ins_disc = cropInsDiscs.find(
        a =>
          a.Ins_Plan_Code.toUpperCase() == plan.toUpperCase() &&
          a.Ins_Type_Code.toUpperCase() == plan_type.toUpperCase() &&
          a.Collateral_Code == crop_Code &&
          a.Collateral_Type_Code == crop_type &&
          !a.Irr_Practice_Type_Code
      );

      if(ins_disc) {
        return ins_disc.Disc_Percent;
      } else {
        ins_disc = cropInsDiscs.find(
          a =>
            a.Ins_Plan_Code.toUpperCase() == plan.toUpperCase() &&
            a.Ins_Type_Code.toUpperCase() == plan_type.toUpperCase() &&
            a.Collateral_Code == crop_Code &&
            !a.Collateral_Type_Code &&
            !a.Irr_Practice_Type_Code
        );

        if(ins_disc) {
          return ins_disc.Disc_Percent;
        } else {
          ins_disc = cropInsDiscs.find(
            a =>
              a.Ins_Plan_Code.toUpperCase() == plan.toUpperCase() &&
              a.Ins_Type_Code.toUpperCase() == plan_type.toUpperCase() &&
              !a.Collateral_Code &&
              !a.Collateral_Type_Code &&
              !a.Irr_Practice_Type_Code
          );

          if(ins_disc) {
            return ins_disc.Disc_Percent;
          } else {
            ins_disc = cropInsDiscs.find(
              a =>
                a.Ins_Plan_Code.toUpperCase() == plan.toUpperCase() &&
                !a.Ins_Type_Code &&
                !a.Collateral_Code &&
                !a.Collateral_Type_Code &&
                !a.Irr_Practice_Type_Code
            );

            if(ins_disc) {
              return ins_disc.Disc_Percent;
            } else {
              return 100;
            }
          }
        }
      }
    }
  }
}

/**
 * Returns Loan Status
 * @param LoanStatus Loan Status
 */
export function Get_Loan_Status(LoanStatus: string) {
  if(LoanStatus && LoanStatus.includes('_')) {
    return LoanStatus.substring(0, LoanStatus.indexOf('_'));
  }
  return LoanStatus;
}

export function GetCurrentCropYear() {
  const d = new Date()

  const month = d.getMonth();
  const date = d.getDate();
  const year = d.getFullYear();

  const SEPTEMBER_MONTH = 8;

  if(month > SEPTEMBER_MONTH) {
    return year + 1;
  } else if (month == SEPTEMBER_MONTH && date >= 1) {
    return year + 1;
  } else {
      return year;
  }
}

export function IsCurrentYearLoan(cropYear: number) {
  let currentCropYear = GetCurrentCropYear();
  return currentCropYear == cropYear;
}
