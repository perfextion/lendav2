import { Loan_Policy } from '@lenda/models/insurancemodel';
import { Helper } from '../math.helper';

/**
 * Insurance Calculation Helper
 * ===
 */
export class InsCalcHelper {
  /**
   * Get Liability based on Plan
   * @param plan Policy
   */
  public static Liability(plan: Loan_Policy) {
    switch (plan.Ins_Plan.toLowerCase()) {
      case PlanTypes.STAX:
      case PlanTypes.SCO:
        return plan.Area_Yield * plan.Price;

      case PlanTypes.RAMP:
        return plan.Liability_Percent;

      case PlanTypes.PCI:
        return plan.Custom1 + plan.Custom2;

      case PlanTypes.CROPHAIL:
        if (plan.Ins_Plan_Type == CROPHAILPLANTYPES.BASIC) {
          return plan.Liability_Percent;
        }
        return Insurance_Default.MaxLiability;

      default:
        return Insurance_Default.MaxLiability;
    }
  }

  /**
   * Overlap Factor based on Plan Type
   * @param plan Plan
   * @param coverage_to_mpci Coverage To MPCI
   * @param band Band
   */
  public static Overlap_Factor(
    plan: Loan_Policy,
    coverage_to_mpci: number,
    band: number
  ) {
    switch (plan.Ins_Plan.toLowerCase()) {
      case PlanTypes.MPCI:
      case PlanTypes.PCI:
      case PlanTypes.CROPHAIL:
        return 1;
      default:
        return Helper.divide(coverage_to_mpci, band);
    }
  }

  /**
   * Acturial Factor based on Plan Type
   * @param plan Plan
   */
  public static Acturial_Factor(plan: Loan_Policy) {
    switch (plan.Ins_Plan.toLowerCase()) {
      case PlanTypes.CROPHAIL:
        if (plan.Option == CROPHAILPLANOPTIONS.WIND) {
          return 0.15; // plan.Actuarial_Wind / 100;
        }
        return 0.15; //plan.Actuarial_Wind / 100;

      default:
        return 1;
    }
  }

  /**
   * Coverage Add Percent based on Plan Type
   * @param plan Plan
   * @param coverage_percent Coverage Percent
   */
  public static Coverage_Add_Percent(
    plan: Loan_Policy,
    coverage_percent: number
  ) {
    switch (plan.Ins_Plan.toLowerCase()) {
      case PlanTypes.STAX:
      case PlanTypes.SCO:
      case PlanTypes.PCI:
        return 0;

      case PlanTypes.CROPHAIL:
        if (plan.Ins_Plan_Type == CROPHAILPLANTYPES.BASIC) {
          return 0;
        }
        return coverage_percent;

      case PlanTypes.MPCI:
      case PlanTypes.HMAX:
      case PlanTypes.RAMP:
      case PlanTypes.ICE:
      case PlanTypes.ABC:
        return coverage_percent;

      default:
        return coverage_percent;
    }
  }

  /**
   * Coverage Add Amount based on Policy
   * @param plan Plan
   * @param coverage_percent Coverage Percent
   * @param liability Liability
   */
  public static Coverage_Add_Amount(
    plan: Loan_Policy,
    coverage_percent: number,
    liability: number
  ) {
    switch (plan.Ins_Plan.toLowerCase()) {
      case PlanTypes.STAX:
      case PlanTypes.SCO:
      case PlanTypes.PCI:
        return coverage_percent * liability;

      case PlanTypes.CROPHAIL:
        if (plan.Ins_Plan_Type == CROPHAILPLANTYPES.BASIC) {
          return coverage_percent * liability;
        }
        return 0;

      case PlanTypes.MPCI:
      case PlanTypes.HMAX:
      case PlanTypes.RAMP:
      case PlanTypes.ICE:
      case PlanTypes.ABC:
        return 0;

      default:
        return 0;
    }
  }
}

export const Insurance_Default = {
  MaxLiability: 99999999
};

export enum PlanTypes {
  MPCI = 'mpci',

  HMAX = 'hmax',
  ICE = 'ice',
  STAX = 'stax',
  SCO = 'sco',
  PCI = 'pci',
  CROPHAIL = 'crophail',
  ABC = 'abc',
  RAMP = 'ramp'
}

export enum CROPHAILPLANTYPES {
  BASIC = 'BASIC',
  COMPANION = 'COMPANION'
}

export enum CROPHAILPLANOPTIONS {
  HAIL,
  WIND
}
