import { Injectable } from '@angular/core';
import { ApiService } from '..';
import { ResponseModel } from '../../models/resmodels/reponse.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class InsuranceapiService {

  constructor(private apiservice: ApiService) { }

  saveupdateAgent(Object:any) : Observable<ResponseModel>{
    const route='/api/Farm/EditLoanCropFarm';
    return this.apiservice.post(route,Object).map(res=>res);
   }
   updateAPHVerification(id: number,iscropunit:boolean): Observable<ResponseModel> {
    const route = '/api/Insurance/VerifyAph?recordid=' + id +"&isCropUnit="+iscropunit;
    return this.apiservice.get(route).map(res => res);
  }

}
