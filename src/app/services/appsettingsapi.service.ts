import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ResponseModel } from '@lenda/models/resmodels/reponse.model';

import { ApiService } from '@lenda/services/api.service';
import { AdminUser } from '@lenda/models/loanmodel';

@Injectable()
export class AppsettingsApiService {
  constructor(private apiservice: ApiService) {}

  syncMyPreferences(preferences: string): Observable<ResponseModel> {
    const route = '/api/Settings/SaveMyPreferences';
    return this.apiservice.post(route, preferences).map(res => res);
  }

  getMyPreferences(): Observable<ResponseModel> {
    const route = '/api/Settings/GetMyPreferences';
    return this.apiservice.get(route).map(res => res);
  }

  getMyPreferencesbyuserid(userid: number): Observable<ResponseModel> {
    const route = '/api/Settings/GetMyPreferences';
    return this.apiservice.getGhost(route, userid).map(res => res);
  }

  getAdminList(): Observable<ResponseModel<Array<AdminUser>>> {
    const route = '/api/Loan/GetAdminList';
    return this.apiservice.get(route).map(res => res);
  }

  copyPreferencesDefaults(userid: number, officeId: number, From_User_ID: string): Observable<ResponseModel<string>> {
    const route = '/api/Loan/CopyPreferencesDefaults';

    const body = {
      User_ID: userid,
      Office_ID: officeId,
      From_User_ID: From_User_ID
    };

    return this.apiservice.post(route, body).map(res => res);
  }
}
