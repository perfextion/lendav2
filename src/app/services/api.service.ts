import { Injectable, Inject } from '@angular/core';
import {
  Headers,
  Http,
  Response,
  RequestOptions,
  ResponseContentType,
  URLSearchParams
} from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';
import { throwError } from 'rxjs';

@Injectable()
export class ApiService {
  constructor(
    @Inject(Http) private http: Http,
    private router: Router,
    private toaster: ToastrService,
    private localst: LocalStorageService
  ) {}

  private setHeaders(userid?: number): Headers {
    const headersConfig = {
      'Content-Type': 'application/json',
      Accept: 'application/json'
    };

    if (this.localst.retrieve('token')) {
      headersConfig['Authorization'] = this.localst.retrieve('token');
    }
    headersConfig['batchid'] = this.localst.retrieve(environment.usersession);
    headersConfig['userid'] = userid || this.localst.retrieve(environment.uid) || 7;
    headersConfig['dbg_level'] = this.localst.retrieve(environment.logpriority);
    return new Headers(headersConfig);
  }

  private formatErrors(error: any) {
    return throwError(error.json());
  }

  public get(
    path: string,
    params: URLSearchParams = new URLSearchParams()
  ): Observable<any> {
    return this.http
      .get(`${environment.apiUrl}${path}`, {
        headers: this.setHeaders(),
        search: params
      })
      .catch(this.formatErrors)
      .map((res: Response) => res.json());
  }

  public getGhost(
    path: string,
    ghostUserId: number,
    params: URLSearchParams = new URLSearchParams()
  ): Observable<any> {
    return this.http
      .get(`${environment.apiUrl}${path}`, {
        headers: this.setHeaders(ghostUserId),
        search: params
      })
      .catch(this.formatErrors)
      .map((res: Response) => res.json());
  }

  public put(path: string, body: Object = {}): Observable<any> {
    return this.http
      .put(`${environment.apiUrl}${path}`, JSON.stringify(body), {
        headers: this.setHeaders()
      })
      .catch(this.formatErrors)
      .map((res: Response) => res.json());
  }

  public post(
    path: string,
    body: Object = {},
    params: URLSearchParams = new URLSearchParams()
  ): Observable<any> {
    return this.http
      .post(`${environment.apiUrl}${path}`, JSON.stringify(body), {
        headers: this.setHeaders(),
        search: params
      })
      .catch(this.formatErrors)
      .map((res: Response) => res.json());
  }

  public postGhost(
    path: string,
    body: Object = {},
    ghostUserID: number,
    params: URLSearchParams = new URLSearchParams()
  ): Observable<any> {
    return this.http
      .post(`${environment.apiUrl}${path}`, JSON.stringify(body), {
        headers: this.setHeaders(ghostUserID),
        search: params
      })
      .catch(this.formatErrors)
      .map((res: Response) => res.json());
  }

  public postpdf(loanobjstr: any): Observable<any> {
    const path = '/api/Loan/GeneratePdf';

    const headers = this.setHeaders();
    headers.set('Accept', 'application/octet-stream');

    let options = new RequestOptions({
      responseType: ResponseContentType.Blob,
      headers: headers
    });

    return this.http
      .post(`${environment.apiUrl}${path}`, loanobjstr, options)
      .map((response: Response) => <Blob>response.blob());
  }

  public delete(path): Observable<any> {
    return this.http
      .delete(`${environment.apiUrl}${path}`, { headers: this.setHeaders() })
      .catch(this.formatErrors)
      .map((res: Response) => res.json());
  }
}
