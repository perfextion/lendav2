import {
  ErrorHandler,
  Inject,
  Injector,
  Injectable
} from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment';

import { Logs, SeverityEnum } from '../models/loanmodel';

import { ToasterService } from './toaster.service';
import { LoggingService } from './Logs/logging.service';

@Injectable()
export class GlobalErrorHandler extends ErrorHandler {
  constructor(
    @Inject(Injector) private injector: Injector,
    @Inject(DOCUMENT) private document: Document
  ) {
    // The true paramter tells Angular to rethrow exceptions, so operations like 'bootstrap' will result in an error
    // when an error happens. If we do not rethrow, bootstrap will always succeed.
    super();
  }

  // Need to get ToastrService from injector rather than constructor injection to avoid cyclic dependency error
  private get toasterService(): ToasterService {
    return this.injector.get(ToasterService);
  }

  // Need to get ToastrService from injector rather than constructor injection to avoid cyclic dependency error
  private get LoggingService(): LoggingService {
    return this.injector.get(LoggingService);
  }
  // Need to get ToastrService from injector rather than constructor injection to avoid cyclic dependency error
  private get localstorageService(): LocalStorageService {
    return this.injector.get(LocalStorageService);
  }

  handleError(error: Error) {
    console.log('==== Error handled by Global Error Handler ===');

    let parts = this.document.location.href.split('/');
    this.toasterService.error(error.message);
    let userid = this.localstorageService.retrieve(environment.uid);

    let obj = new Logs();
    obj.Log_Id = 0;
    obj.Log_Message = error.message;
    obj.Sql_Error = error.stack;
    obj.Severity = SeverityEnum.SqlError;
    obj.Loan_Full_ID = this.localstorageService.retrieve(environment.loanidkey) || '0';
    obj.Log_Section = '';
    obj.userID = userid;
    obj.SourceName = 'FrontEnd';
    obj.SourceDetail = parts[2];
    obj.Log_Section = parts[parts.length - 1];

    this.LoggingService.createlog(obj).subscribe(res => {});

    // IMPORTANT: Rethrow the error otherwise it gets swallowed
    throw error;
  }
}
