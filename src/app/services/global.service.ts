import { Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

@Injectable()
export class GlobalService {
  constructor(public localstorageservice: LocalStorageService) {}

  updateLocalStorage(keyword: string) {
    let modifiedvalues = this.localstorageservice.retrieve(
      environment.modifiedbase
    ) as Array<String>;

    for (let i = modifiedvalues.length; i--; ) {
      if (modifiedvalues[i].indexOf(keyword) >= 0) modifiedvalues.splice(i, 1);
    }

    this.localstorageservice.store(environment.modifiedbase, modifiedvalues);
  }
}
