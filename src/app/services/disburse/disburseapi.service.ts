import { Injectable } from '@angular/core';
import { ApiService } from '..';
import { Observable } from 'rxjs';
import { ResponseModel } from '@lenda/models/resmodels/reponse.model';

@Injectable()
export class DisburseapiService {

  constructor(private apiservice: ApiService) { }

  SaveUpdateDisbursement(Object:any) : Observable<ResponseModel>{
    const route='/api/Disburse/SaveUpdateDisbursement';
    return this.apiservice.post(route,Object).map(res=>res);
   }

   
  GetDisbursement(loanfullid:number) : Observable<ResponseModel>{
    const route='/api/Disburse/GetDisbursements?Loanid='+loanfullid;
    return this.apiservice.get(route,null).map(res=>res);
   }

   AddnewDisubrsementAction(Object:any):Observable<ResponseModel>{
     
    const route='/api/Disburse/AddnewDisubrsementAction';
    return this.apiservice.post(route,Object).map(res=>res);
   }

   AddnewDisubrsementActionSingle(Object:any):Observable<ResponseModel>{
     
    const route='/api/Disburse/AddnewDisubrsementActionSingle';
    return this.apiservice.post(route,Object).map(res=>res);
   }

   DeleteDisubrsement(id:any):Observable<ResponseModel>{
     
    const route='/api/Disburse/DeleteDisubrsement?id='+id;
    return this.apiservice.get(route,null).map(res=>res);
   }

}
