import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class SharedService {
  // Logic to check if it is loan list page
  private isLoanList = new BehaviorSubject<boolean>(false);
  isLoanList$ = this.isLoanList.asObservable();

  private isLoanDetailExtra = new BehaviorSubject<boolean>(false);
  isLoanDetailExtra$ = this.isLoanDetailExtra.asObservable();

  isLoanListActive(_isLoanList: boolean) {
    this.isLoanList.next(_isLoanList);
  }

  isLoanDetailExtraExpanded(_isLoanDetailExtra: boolean) {
    this.isLoanDetailExtra.next(_isLoanDetailExtra);
  }
}
