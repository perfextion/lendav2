import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ApiService } from '../api.service';
import { User } from '../../models/user';
import { ResponseModel } from '@lenda/models/resmodels/reponse.model';
import { EmailModel } from '@lenda/models/email.model';
import { GetAllUsers } from '@lenda/components/committee/members/member.model';
import { shareReplay } from 'rxjs/operators';
@Injectable()

export class UserApiService {
  headerFilter: string;
  users: Observable<ResponseModel<Array<GetAllUsers>>>;

  constructor(private apiservice: ApiService) { }

  getUsers(): Observable<ResponseModel<Array<GetAllUsers>>> {
    const route = '/api/User/GetAllUsers';
    if(this.users) {
      return this.users;
    }
    this.users = this.apiservice.get(route).pipe(shareReplay());
    return this.users;
  }


  sendUsersEmail(users: any, loanNumber: string, EmailTemplateType: number): Observable<Array<User>> {
    const route = '/api/User/SendLoanApprovalEmail';
    return this.apiservice.post(route, { users, loanNumber, EmailTemplateType }).map(res => res);
  }

  sendEmailToCommittee(body: EmailModel): Observable<ResponseModel> {
    const url = '/api/Email/Send';
    return this.apiservice.post(url, body).map(res => res);
  }
}
