/**
 * Scale Helper
 * ===
 * Scale Helper to get Scale Calculation
 */
export const ScaleHelper = {
  /**
   * Get Scale Position with Reference Scale and Actual Scale
   * @param refScale  Reference Scale - Array of numbers - `Stored in  Database`
   * @param actualScale Actual Scale - Array of numbers - `Used in UI`
   * @returns `Scale Position in UI`
   */
  getPosition: (
    refScale: Array<number>,
    actualScale: Array<number>,
    point: number
  ) => {
    if (point <= refScale[0]) {
      return actualScale[0];
    }

    if (point >= refScale[refScale.length - 1]) {
      return actualScale[refScale.length - 1];
    }

    let index = refScale.findIndex(a => a >= point);

    let x = [refScale[index - 1], refScale[index]];
    let y = [actualScale[index - 1], actualScale[index]];

    return y[0] + (point - x[0]) * ((y[1] - y[0]) / (x[1] - x[0]));
  }
};
