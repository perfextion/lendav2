import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ResponseModel } from '../../models/resmodels/reponse.model';
import { ApiService } from '../api.service';
import { Logpriority, Logs } from '../../models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../environments/environment.prod';
import { DOCUMENT } from '@angular/common';

@Injectable()
export class LoggingService {
  constructor(
    private apiservice: ApiService,
    public localst: LocalStorageService,
    @Inject(DOCUMENT) private document: Document
  ) {}

  createlog(Object: any): Observable<ResponseModel> {
    const route = '/api/Logs/AddLog';
    return this.apiservice.post(route, Object).map(res => res);
  }

  checkandcreatelog(level: Logpriority, section: string, message: string) {
    //Here this is what logs means
    // 0 - no logs
    // 1 - priority logs like main calulations and totaltime
    // 2 - all of them

    level = level || 0; //default to zero
    // let res=this.localst.retrieve(environment.logpriority);
    let res = Logpriority.None;
    let userid = this.localst.retrieve(environment.uid);
    let Loan_Full_ID = this.localst.retrieve(environment.loanidkey);
    if (level <= res) {
      let obj = new Logs();
      obj.Log_Id = 0;
      obj.Loan_Full_ID = Loan_Full_ID || '0';
      obj.Log_Message = message;
      obj.Log_Section = section;
      obj.SourceName = 'FrontEnd';
      obj.Severity = this.getSeverity(level);
      let parts = this.document.location.href.split('/');
      obj.SourceDetail = parts[2];
      obj.userID = userid;
      this.createlog(obj).subscribe(_ => {});
    }
    //ignore else
  }

  private getSeverity(level: number) {
    return level;
  }
}
