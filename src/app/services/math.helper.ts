/**
 * Math Helper
 * ===========
 */
export const Helper = {
  /**
   * Divide
   * ======
   * @param x Numerator
   * @param y Denominator
   * @returns 0 if y is zero
   */
  divide: (x: number, y: number) => {
    x = x == undefined ? 0 : x;
    y = y == undefined ? 0 : y;

    if (y == 0) {
      return 0;
    }
    return x / y;
  },

  /**
   * Percent
   * =====
   * @returns X/Y * 100 %
   */
  percent: (x: number, y: number) => {
    x = x == undefined ? 0 : x;
    y = y == undefined ? 0 : y;

    if (y == 0) {
      return 0;
    }
    let perc = (x * 100) / y;

    return parseFloat(perc.toFixed(1));
  },

  sum: (collection: Array<any>, func: (a: any) => any) => {
    let total = 0;

    for (let item of collection) {
      let x = parseFloat(func.call(null, item));
      if (isNaN(x)) {
        total += 0;
      } else {
        total += x;
      }
    }

    return total;
  },

  rating: (value: number) => {
    return parseFloat(value.toFixed(2)) * 100;
  },

  /**
   * @returns `x`/`y` if `y > 0` or `defaultValue`
   */
  IFERROR: (x: number, y: number, defaultValue = 1) => {
    x = x == undefined ? 0 : x;
    y = y == undefined ? 0 : y;

    if (y == 0) {
      return defaultValue;
    }
    return x / y;
  },

  number: (x: any) => {
    if (isNaN(x)) {
      return 0;
    }
    return x;
  },

  isDecimal: (value: any) => {
    return !isNaN(value) && value % 1 != 0;
  }
};
