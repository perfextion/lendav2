import { loan_model } from '@lenda/models/loanmodel';
import * as _ from 'lodash';

export function Biggest_AIP(loan: loan_model) {
  let mkt_values = loan.LoanPolicies.filter(p => p.Select_Ind && p.AIP_ID).map(
    a => {
      let cropUnits = loan.LoanCropUnits.filter(
        c =>
          c.Crop_Code == a.Crop_Code &&
          c.Crop_Practice_Type_Code == a.Crop_Practice_Type_Code
      );

      return {
        Policy: a,
        AIP_ID: a.AIP_ID,
        MKT_Value: cropUnits.reduce((x, c) => {
          return x + c.Disc_Mkt_Value;
        }, 0)
      };
    }
  );

  let buggestAIP = _.maxBy(mkt_values, m => m.MKT_Value);
}

export function Primary_Collateral(loan: loan_model) {}

export function Non_Revenue_Insurance_Collateral(loan: loan_model) {}

export function Revenue_Insurance_Collateral(loan: loan_model) {}
