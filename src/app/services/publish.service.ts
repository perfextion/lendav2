import { Injectable, EventEmitter } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { finalize } from 'rxjs/operators';

import { LocalStorageService } from 'ngx-webstorage';
import { JsonConvert } from 'json2typescript';

import { environment } from '@env/environment.prod';
import { loan_model } from '../models/loanmodel';
import { LoanApiService } from './loan/loanapi.service';
import { LoggingService } from './Logs/logging.service';
import { LoancalculationWorker } from '../Workers/calculations/loancalculationworker';
import { ToasterService } from './toaster.service';
import { DataService } from './data.service';
import { Page } from '@lenda/models/page.enum';

/**
 * Global service for publish button
 * ===
 * This service file detects if there are local edits on any page
 */
@Injectable({
  providedIn: 'root'
})
export class PublishService {
  private static _syncRequired: BehaviorSubject<Sync[]> = new BehaviorSubject(
    []
  );

  private static _syncItems: Sync[] = [];

  /**
   * Keeps track of Sync in Progress
   */
  public static isSyncInProgress: boolean = false;
  public syncCompletedevent: EventEmitter<loan_model> = new EventEmitter();

  constructor(
    public localStorageService: LocalStorageService,
    public loanapi: LoanApiService,
    public logging: LoggingService,
    public loanserviceworker: LoancalculationWorker,
    public toasterService: ToasterService,
    public dataservice:DataService
  ) {
    let syncItems: Array<any> = this.localStorageService.retrieve(environment.syncRequiredItems);
    if(!!syncItems){
      syncItems.forEach(p => {
        this.enableSync(p._page);
      });
    }
  }

  /**
   * Method to mark if page requires sync or not
   * @param pageName Name of page
   * @param isEnabled if sync is required or not
   */
  public enableSync(pageName: Page) {
    let sync = new Sync(pageName);
    this.syncRequiredOnPage(sync);
  }

  /**
   * Observable for checking if sync is required
   */
  public listenToSyncRequired(): Observable<Sync[]> {
    return PublishService._syncRequired.asObservable();
  }

  /**
   * Inform the subject that sync is required for this page
   * @param sync sync object containing page name and boolean value if sync is required or not
   */
  public syncRequiredOnPage(sync: Sync): void {
    let item = PublishService._syncItems.find(p => {
      return sync.page === p.page;
    });

    // If item already exists, update that otherwise make a new entry
    if (item) {
      item.isSyncRequired = sync.isSyncRequired;
    } else {
      PublishService._syncItems.push(sync);
    }

    // Push this on local storage
    this.localStorageService.store(
      environment.syncRequiredItems,
      PublishService._syncItems
    );

    // Trigger the change in subject
    PublishService._syncRequired.next(PublishService._syncItems);
  }

  /**
   * Sync complete event
   * ===
   * This marks the completion of sync event i.e. all pages are synched
   */
  public syncCompleted(): void {
    PublishService._syncItems = [];

    this.localStorageService.store(
      environment.syncRequiredItems,
      PublishService._syncItems
    );
    PublishService._syncRequired.next(PublishService._syncItems);
  }

  /**
   * Syncs database object
   * TODO: Check if the functionality implementation is same everywhere
   * If it is same, then dont emit event for onpublish rather use this method
   */
  syncToDb() {
    PublishService.isSyncInProgress=true;
    let localloanobject = this.localStorageService.retrieve(
      environment.loankey
    );

    // Loan Condition Calculation
    localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);
    localloanobject=this.loanserviceworker.WaiveofftempdisburseErrors(localloanobject);
    this.loanapi.syncloanobject(localloanobject).subscribe(respone => {
      if (respone.ResCode == 1) {
        this.localStorageService.store(environment.modifiedbase, []);
        this.loanapi
          .getLoanById(localloanobject.Loan_Full_ID)
          .pipe(finalize(() => {
            PublishService.isSyncInProgress=false;
          }))
          .subscribe(res => {
            this.logging.checkandcreatelog(
              3,
              'Overview',
              'APi LOAN GET with Response ' + res.ResCode
            );
            if (res.ResCode == 1) {

              this.toasterService.success('Records Synced');

              let jsonConvert: JsonConvert = new JsonConvert();
              let subsrciption=this.dataservice.getLoanObject().subscribe(p=>{
                this.syncCompletedevent.next(jsonConvert.deserialize(res.Data, loan_model));
                subsrciption.unsubscribe();
              })
              this.loanserviceworker.performcalculationonloanobject(
                jsonConvert.deserialize(res.Data, loan_model)
              );
              this.loanserviceworker.saveValidationsToLocalStorage(res.Data);
            } else {
              this.toasterService.error('Could not fetch Loan Object from API');
            }
          });
      } else {
        this.toasterService.error(respone.Message || 'Error in Sync');
        PublishService.isSyncInProgress = false;
      }
    }, error => {
      PublishService.isSyncInProgress = false;
    });
  }

  checkifsyncdone(): Observable<boolean> {

    let localloanobject = this.localStorageService.retrieve(
      environment.loankey
    );
    // Loan Condition Calculation
    localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);
    return new Observable<boolean>(observer => {
      this.loanapi.syncloanobject(localloanobject).subscribe(respone => {

        if (respone.ResCode == 1) {
          this.localStorageService.store(environment.modifiedbase, []);
          this.loanapi.getLoanById(localloanobject.Loan_Full_ID).subscribe(res => {

            this.logging.checkandcreatelog(
              3,
              'Overview',
              'APi LOAN GET with Response ' + res.ResCode
            );
            if (res.ResCode == 1) {
              this.toasterService.success('Records Synced');
              let jsonConvert: JsonConvert = new JsonConvert();
              this.loanserviceworker.performcalculationonloanobject(
                jsonConvert.deserialize(res.Data, loan_model)
              );
              this.loanserviceworker.saveValidationsToLocalStorage(res.Data);
              observer.next(true);
            } else {
              this.toasterService.error('Could not fetch Loan Object from API');
              observer.next(false);
            }

          });
        } else {
          this.toasterService.error(respone.Message || 'Error in Sync');
          observer.next(false);
        }
      });
    })

  }
}

// HELPERS
// =======

/**
 * Sync class to check which page requires synching
 */
export class Sync {
  private _isSyncRequired: boolean = false;
  private _page: Page;

  constructor(page: Page, isSyncRequired: boolean = true) {
    this._page = page;
    this._isSyncRequired = isSyncRequired;
  }

  /**
   * Getter method to get current pagename
   */
  get page() {
    return this._page;
  }

  /**
   * Getter method for isSyncRequired for this page
   */
  get isSyncRequired() {
    return this._isSyncRequired;
  }

  /**
   * Setter method for isSyncRequired
   */
  set isSyncRequired(value) {
    this._isSyncRequired = value;
  }
}
