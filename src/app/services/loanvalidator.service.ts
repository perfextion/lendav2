import { Injectable } from '@angular/core';

import { validate } from 'class-validator';

import { loan_model } from '@lenda/models/loanmodel';
import { LoanMaster } from '@lenda/models/ref-data-model';

@Injectable()
export class LoanvalidatorService {
  constructor() {}

  validateloanobject(loanmodel: loan_model) {
    //check loan master
    //check for negative values here

    let parsedmodel = loanmodel.LoanMaster as LoanMaster;
    return validate(parsedmodel);
  }
}
