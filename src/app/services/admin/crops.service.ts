import { Injectable } from '@angular/core';
import { ApiService } from '@lenda/services';
import { Observable } from 'rxjs/Observable';
import { ResponseModel } from '../../models/resmodels/reponse.model';
import { environment } from './../../../environments/environment';

@Injectable()
export class CropsService {
  constructor(private apiservice: ApiService) {}

  updateCrop(params): Observable<ResponseModel> {
    return this.apiservice
      .post(environment.apiEndpoints.adminEndpoints.updateCrop, params)
      .map(res => res);
  }

  deleteCrop(id): Observable<ResponseModel> {
    return this.apiservice
      .post(environment.apiEndpoints.adminEndpoints.deleteCrop + '/' + id)
      .map(res => res);
  }

  getAllCropList(): Observable<ResponseModel> {
    return this.apiservice
      .get(environment.apiEndpoints.adminEndpoints.allCropList)
      .map(res => res);
  }
}
