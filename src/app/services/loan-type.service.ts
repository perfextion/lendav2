import { Injectable } from '@angular/core';

import { environment } from '@env/environment.prod';

import {
  loan_model,
  Loan_Type_Codes,
  Loantype_dsctext
} from '@lenda/models/loanmodel';
import { LoanMaster } from '@lenda/models/ref-data-model';
import { LoanAuditTrail } from '@lenda/models/audit-trail/audit-trail';

import { LoanAuditTrailService } from '@lenda/services/audit-trail.service';
import { LoggingService } from './Logs/logging.service';

@Injectable()
export class LoanTypeCalculationService {
  constructor(
    private auditTrail: LoanAuditTrailService,
    private loggin: LoggingService
  ) {}

  /**
   * Calculates Loan Type based on Budget and other parameters
   * @param localloanobj Loan Model
   */
  calculateLoanType(localloanobj: loan_model) {
    if (environment.isDebugModeActive) console.time('Calculate Loan Type');

    try {
      const loanMaster: LoanMaster = localloanobj.LoanMaster;
      let oldType = loanMaster.Loan_Type_Code;

      if (
        loanMaster.Loan_Total_Budget == null ||
        loanMaster.Loan_Total_Budget == 0
      ) {
        loanMaster.Loan_Type_Code = Loan_Type_Codes.AllIn;
      } else if (
        loanMaster.Ag_Pro_Requested_Credit > 0 &&
        loanMaster.Dist_Total_Budget >= 0
      ) {
        loanMaster.Loan_Type_Code = Loan_Type_Codes.AgPro;
      } else if (loanMaster.Dist_Total_Budget == 0) {
        loanMaster.Loan_Type_Code = Loan_Type_Codes.AllIn;
      } else if (loanMaster.Dist_Total_Budget > 0) {
        loanMaster.Loan_Type_Code = Loan_Type_Codes.AgInput;
      } else if (loanMaster.Ag_Pro_Requested_Credit > 0) {
        loanMaster.Loan_Type_Code = Loan_Type_Codes.AgPro;
      }

      loanMaster.Loan_Type_Name = Loantype_dsctext(loanMaster.Loan_Type_Code);

      // If Loan Type Changed, Log in Audit Trail
      if (oldType != loanMaster.Loan_Type_Code) {
        const auditText = `Loan Type Changed from ${Loantype_dsctext(
          oldType
        )} to ${Loantype_dsctext(loanMaster.Loan_Type_Code)}`;

        const audit = <LoanAuditTrail>{
          Audit_Trail_Text: auditText,
          Audit_Field_ID: 'Loan Type Update',
          Loan_Status: loanMaster.Loan_Status,
          Loan_ID: loanMaster.Loan_Full_ID,
          Loan_Full_ID: loanMaster.Loan_Full_ID,
          Audit_Trail_Type: 'Loan Type',
          Loan_Seq_Num: String(loanMaster.Loan_Seq_num)
        };

        this.auditTrail.addAuditTrail(audit).subscribe(res => {
          if (res.ResCode != 1) {
            this.loggin.checkandcreatelog(
              3,
              'Audit Trail',
              'Adding Audit trail Failed with Response ' + res.ResCode
            );
          }
        });
      }

      if (environment.isDebugModeActive) console.timeEnd('Calculate Loan Type');

      return loanMaster.Loan_Type_Code;
    } catch {
      if (environment.isDebugModeActive) console.timeEnd('Calculate Loan Type');
    }
    return localloanobj.LoanMaster.Loan_Type_Code;
  }
}
