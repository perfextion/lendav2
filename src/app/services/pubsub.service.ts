import { Injectable } from '@angular/core';

import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PubSubService {
  private subject = new Subject<any>();

  /**
   * Sends event via the shared bus
   * @param name name of event
   * @param data payload data
   */
  sendEvent(name: string, data: any) {
    this.subject.next({
      name: name,
      data: data
    });
  }

  /**
   * Clear the shared bus
   */
  clearMessage() {
    this.subject.next();
  }

  /**
   * Subscribe pubsub bus
   */
  subscribeToEvent(): Observable<{ name: string; data: any }> {
    return this.subject.asObservable();
  }
}
