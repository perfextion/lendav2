import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Page } from '@lenda/models/page.enum';

@Injectable({
  providedIn: 'root'
})
export class PageInfoService {
  // Logic to check if it is loan list page
  currentPage = new BehaviorSubject<Page>(Page.loanlist);

  setCurrentPage(value: Page) {
    this.currentPage.next(value);
  }
}
