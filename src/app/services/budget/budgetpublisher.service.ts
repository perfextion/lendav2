import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';

@Injectable()
export class BudgetpublisherService {
  public AllinClicked: Subject<boolean> = new Subject<boolean>();
  public AgProClicked: Subject<boolean> = new Subject<boolean>();

  constructor() {}

  publishAllInChanged(value) {
    this.AllinClicked.next(value);
  }

  publishAgProChanged(value) {
    this.AgProClicked.next(value);
  }
}
