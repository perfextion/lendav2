import { Injectable } from '@angular/core';
import { ValidationErrorsCountService } from '@lenda/Workers/calculations/validationerrorscount.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';

@Injectable()
export class CheckLoanSyncService {
  constructor(
    private validationcount: ValidationErrorsCountService,
    private alertify: AlertifyService
  ) {}

  checkLoanSync() {
    let hasValidations = this.validationcount.hasLevel2Validations();

    if (hasValidations) {
      this.alertify.alert(
        'Alert',
        'Please resolve all critical validations before publishing.'
      );
      return false;
    } else {
      return true;
    }
  }
}
