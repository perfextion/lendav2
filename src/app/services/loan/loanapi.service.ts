import { Loan_Association } from './../../models/loanmodel';
import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { ResponseModel } from '../../models/resmodels/reponse.model';
import {
  copy_loan_model,
  ChangeStatusModel
} from '../../models/copy-loan/copyLoanModel';
import { environment } from '../../../environments/environment.prod';
import { ApiService } from '../api.service';
import {
  loan_model,
  loan_farmer,
  borrower_model,
  Loan_Committee,
  Risk_Other_Queue,
  User_Pending_Action,
  CalculatedVariables
} from '../../models/loanmodel';
import { Subject } from 'rxjs';
const API_URL = environment.apiUrl;
import {
  Headers,
  Response,
  RequestOptions,
  ResponseContentType,
  URLSearchParams
} from '@angular/http';
import { of } from 'rxjs/observable/of';
import {
  AddPendingActionModel,
  Remove_PA,
  AddPAByRoles,
  EmailAndNotificationModel
} from '@lenda/models/pending-action/add-pending-action.model';
import { UpdateWatchListIndModel } from '@lenda/models/resmodels/loan-list.model';
import { Master_Borrower } from '@lenda/models/master-borrower.model';
import { EditLoanParamsModel } from '@lenda/models/edit-loan-params/edit-loan-params.model';
import { AddNotifyPartyResponse } from '@lenda/components/committee/members/members.component';
import { Sync_Status } from '@lenda/models/syncstatusmodel';
import { GetAllUsers } from '@lenda/components/committee/members/member.model';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { MatDialog } from '@angular/material';
import { SyncValidationWarningComponent } from '@lenda/alertify/components/sync-validation-warning/sync-validation-warning.component';
import { switchMap } from 'rxjs/operators';
import { Loan_Document } from '@lenda/models/loan/loan_document';
import { LoanAuditTrail } from '@lenda/models/audit-trail/audit-trail';
import { LogsRequestParams, LogResponseModel } from '@lenda/components/debug/log-debug/loan-logs-debug/logs.model';

@Injectable()
export class LoanApiService {
  headerFilter: string;
  constructor(
    @Inject(Http) private http: Http,
    private apiservice: ApiService,
    private settingsService: SettingsService,
    private dialog: MatDialog
  ) {}

  getLoanList(querystring?: string): Observable<ResponseModel> {
    let route = '/api/Loans/GetAllLoans';
    if (querystring) {
      route = `${route}?${querystring}`;
    }
    return this.apiservice.get(route).map(res => res);
  }

  getLoanListForSync(query: string): Observable<ResponseModel> {
    let route = '/api/Loans/GetAllLoansForSync';
    const body = {
      Query: query
    };

    return this.apiservice.post(route, body).map(res => res);
  }

  /**
   * Gets loan list based on settings in my preferences
   * @param settings settings object to be posted to api
   */
  getLoanListByPost(settings, ghostUserID: number): Observable<ResponseModel> {
    let route = '/api/Loans/GetAllLoans';
    return this.apiservice.postGhost(route, settings, ghostUserID).map(res => res);
  }

  getLoanById(loanid: string): Observable<ResponseModel<loan_model>> {
    const route = '/api/Loans/GetLoanbyId?loanfullid=' + loanid;
    return this.apiservice.get(route).map(res => res);
  }

  UploadLoanToNortridge(loanid: string): Observable<ResponseModel> {
    const route = '/api/Nortridge/UploadLoan?Loanid=' + loanid + "&pro=Y";
    return this.apiservice.get(route).map(res => res);
  }

  Log_Out(body: any) {
    const route = '/api/User/Logout';
    return this.apiservice.post(route, body).map(res => res);
  }

  checkActiveUser(loanid: string): Observable<ResponseModel<boolean>> {
    const route = '/api/Loans/Check_Active_User?loanfullid=' + loanid;
    return this.apiservice.get(route).map(res => res);
  }

  takeControl(loanid: string): Observable<ResponseModel<loan_model>> {
    const route = '/api/Loans/Take_Control?loanfullid=' + loanid;
    return this.apiservice.get(route).map((res: ResponseModel<loan_model>) => {
      res.Data.SyncStatus = {
        Status_Farm: Sync_Status.NOCHANGE,
        Status_Crop_Practice: Sync_Status.NOCHANGE,
        Status_Insurance_Policies: Sync_Status.NOCHANGE,
        Status_Borrower: Sync_Status.NOCHANGE,
        Status_Farmer: Sync_Status.NOCHANGE
      };
      res.Data.CalculatedVariables = new CalculatedVariables();
      return res;
    });
  }

  /**
   * Api Method to retrieve cross Collateral Records.
   * @param loanid The Loan FUll ID of the selected Loan.
   */
  getDistinctLoanIDList(loanid: string): Observable<ResponseModel> {
    const route = '/api/Loans/GetDistinctLoanIDList';
    let params: URLSearchParams = new URLSearchParams();
    params.set('LoanFullID', loanid);
    return this.apiservice.get(route, params).map(res => res);
  }

  getLoanGroups(loanid: string): Observable<ResponseModel> {
    const route = '/api/Loans/GetLoanGroups?LoanFullID=' + loanid;
    return this.apiservice.get(route).map(res => res);
  }

  copyloan(copyloanmodel: copy_loan_model): Observable<ResponseModel> {
    if (copyloanmodel) {
      const route = '/api/Loan/CopyLoan';
      return this.apiservice.post(route, copyloanmodel).map(res => res);
    }
  }
  summerypdfget(loanobj: number): Observable<ResponseModel> {
    const route = 'api/Loan/GeneratePdf?loanobj=' + loanobj;
    return this.apiservice.get(route).map(res => res);
  }

  syncloanobject(loanobj: loan_model): Observable<ResponseModel<loan_model>> {
    try {

      if (this.settingsService.preferences.userSettings.showTotalsSettings.isValidationWarningDuringSyncEnabled) {
        return this.syncValidationErrorDialog().pipe(
          switchMap(() => this.innerSync(loanobj))
        );
      } else {
        return this.innerSync(loanobj);
      }
    } catch {
      return this.innerSync(loanobj);
    }
  }

  private innerSync(loanobj: loan_model): Observable<ResponseModel> {
    const route = '/api/Loan/SyncLoanObject';
    this.Validate_Loan_Object(loanobj);

    return this.apiservice.post(route, loanobj).map(res => res);
  }

  private Validate_Loan_Object(loanobj: loan_model) {
    loanobj.Association = loanobj.Association.filter(a => !!a.Assoc_Name || a.ActionStatus == 3);

    // Crop Tab
    loanobj.CropYield = loanobj.CropYield.filter(a => !!a.Crop_Code);
    loanobj.LoanMarketingContracts = loanobj.LoanMarketingContracts.filter(a => !!a.Crop_Code);
    loanobj.LoanOtherIncomes = loanobj.LoanOtherIncomes.filter(a => !!a.Other_Income_Name);

    // Farm Tab
    loanobj.Farms = loanobj.Farms.filter(a => a.Farm_State_ID > 0 && a.Farm_County_ID > 0);

    loanobj.LoanBudget = loanobj.LoanBudget.filter(a => !!a.Expense_Type_Name);
  }

  syncloanobject2(loanobj: loan_model): Observable<ResponseModel> {
    const route = '/api/Loan/SyncLoanObject2';
    return this.apiservice.post(route, loanobj).map(res => res);
  }

  recommendLoan(
    loanFullID: string,
    crop_year: number,
    systemMessage: string,
    user_id: number
  ): Observable<ResponseModel> {
    if (loanFullID) {
      const body = {
        Loan_Full_Id: loanFullID,
        System_Message: systemMessage,
        Crop_Year: crop_year,
        User_Id: user_id
      };
      const route = '/api/Loan/RecommendLoan';
      return this.apiservice.post(route, body).map(res => res);
    }
  }

  approveOrRejectQueue(queue: Risk_Other_Queue): Observable<ResponseModel> {
    if (queue) {
      const route = '/api/Loan/ApproveOrRejectQueue';
      return this.apiservice.post(route, queue).map(res => res);
    }
  }
  // syncloanborrower(loanId : number,  loanborrowerobj: borrower_model): Observable<ResponseModel> {
  //   const route = '/api/Loan/EditLoanBorrower?loanId='+loanId;
  //   return this.apiservice.put(route, loanborrowerobj).map(res => res);
  // }
  // syncloanfarmer(loanId : number,  loanfarmerObject: loan_farmer): Observable<ResponseModel> {
  //   const route = '/api/Loan/EditLoanFarmer?loanId='+loanId;
  //   return this.apiservice.put(route, loanfarmerObject).map(res => res);
  // }

  Create_New_Loan(loanObj): Observable<ResponseModel<loan_model>> {
    const route = '/api/Loan/Create_New_Loan';
    return this.apiservice.post(route, loanObj).map(res => res);
  }

  Copy_And_Create_New_Loan_To_Current_Year(loanObj): Observable<ResponseModel<loan_model>> {
    const route = '/api/Loan/Copy_And_Create_New_Loan_To_Current_Year';
    return this.apiservice.post(route, loanObj).map(res => res);
  }

  createLoan_New(loanObj): Observable<ResponseModel> {
    const route = '/api/Loan/CreateLoan_New';
    return this.apiservice.post(route, loanObj).map(res => res);
  }

  addComment(loanCommentObj): Observable<ResponseModel<number>>  {
    const route = '/api/Loan/Comments';
    return this.apiservice.post(route, loanCommentObj).map(res => res);
  }

  getComments(loanFullID: string, userId) {
    const route = '/api/Loan/Comments?loanfullid=' + loanFullID + '&userId=' + userId;
    return this.apiservice.get(route).map(res => res);
  }

  loanListComments(userId: number): Observable<ResponseModel> {
    const route = '/api/Loan/ListComments?&userId=' + userId;
    return this.apiservice.get(route).map(res => res);
  }

  getFilter(): Observable<string> {
    return this.storageSub.asObservable();
  }

  setFilter(filter): void {
    this.headerFilter = filter;
    this.storageSub.next(filter.toString());
  }
  private storageSub = new Subject<string>();
  getFarmerList(): Observable<ResponseModel> {
    const route = '/api/Loan/Farmers';
    return this.apiservice.get(route).map(res => res);
  }

  getCoBorrower(coBorrowerId: number) {
    const route = '/api/Loan/Farmer?id=' + coBorrowerId;
    return this.apiservice.get(route).map(res => res);
  }

  getBorrowerList(): Observable<ResponseModel<Array<Master_Borrower>>> {
    const route = '/api/Loan/Borrowers';
    return this.apiservice.get(route).map(res => res);
  }

  getBorrower(
    BorrowerId: number,
    CropYear: number
  ): Observable<ResponseModel<Master_Borrower>> {
    const route = '/api/Loan/Borrower?BorrowerId=' + BorrowerId + '&CropYear=' + CropYear;
    return this.apiservice.get(route).map(res => res);
  }

  getBorrowerBySSN_Hash(
    ssn_hash: string,
    CropYear: number
  ): Observable<ResponseModel<Master_Borrower>> {
    const route = '/api/Loan/Get_Borrower';
    const body = {
      Borrower_SSN_Hash: ssn_hash,
      CropYear: CropYear
    };
    return this.apiservice.post(route, body).map(res => res);
  }

  addNotifyParty(
    notifyParty
  ): Observable<ResponseModel<AddNotifyPartyResponse>> {
    const route = '/api/Loan/AddNotifyParty';
    return this.apiservice.post(route, notifyParty).map(res => res);
  }

  removeNotifyParty(notifyParty) {
    const route = '/api/Loan/RemoveNotifyParty';
    return this.apiservice.post(route, notifyParty).map(res => res);
  }

  getAllUsers(): Observable<ResponseModel<Array<GetAllUsers>>> {
    const route = '/api/User/GetAllUsers';
    return this.apiservice.get(route).map(res => res);
  }

  readComment(commentObj) {
    const route = '/api/Loan/ReadComment';
    return this.apiservice.post(route, commentObj).map(res => res);
  }

  updateWatchListInd(body: UpdateWatchListIndModel): Observable<ResponseModel> {
    const route = '/api/Loan/UpdateWatchList';
    return this.apiservice.post(route, body).map(res => res);
  }

  getLoanOfficerStats(): Observable<ResponseModel> {
    const route = '/api/Loan/LOStats';
    return this.apiservice.get(route).map(res => res);
  }

  changeStatus(body: ChangeStatusModel): Observable<ResponseModel> {
    const route = '/api/Loan/ChangeStatus';
    return this.apiservice.post(route, body).map(res => res);
  }

  addPendingAction(
    addPendingAction: AddPendingActionModel
  ): Observable<ResponseModel<Array<User_Pending_Action>>> {
    const route = `/api/Loan/PendingAction`;
    return this.apiservice.post(route, addPendingAction).map(res => res);
  }

  sendEmailAndNotification(
    emailModel: EmailAndNotificationModel
  ): Observable<ResponseModel<Array<User_Pending_Action>>> {
    const route = `/api/Email/SendEmail`;
    return this.apiservice.post(route, emailModel).map(res => res);
  }

  addPendingActionByRole(
    addPendingAction: AddPAByRoles
  ): Observable<ResponseModel<Array<User_Pending_Action>>> {
    const route = `/api/Loan/PendingActionByRole`;
    return this.apiservice.post(route, addPendingAction).map(res => res);
  }

  removePendingAction(
    remove_pa: Remove_PA
  ): Observable<ResponseModel<Array<User_Pending_Action>>> {
    const route = `/api/Loan/RemovePendingAction`;
    return this.apiservice.post(route, remove_pa).map(res => res);
  }

  editLoanParams(body: EditLoanParamsModel): Observable<ResponseModel> {
    const route = `/api/Loan/EditLoanParameters`;
    return this.apiservice.post(route, body).map(res => res);
  }

  public syncValidationErrorDialog() {
    let dialogRef = this.dialog.open(SyncValidationWarningComponent, {
      panelClass: 'sync-validation-error-dialog',
      autoFocus: false,
      disableClose: true
    });

    return dialogRef.afterClosed();
  }

  public Update_Reminder_Ind(body: object): Observable<ResponseModel> {
    const route = '/api/Loan/UpdatReminderInd';
    return this.apiservice.post(route, body).map(res => res);
  }

  public UploadDocuments(body: object): Observable<ResponseModel<Array<Loan_Document>>> {
    const route = '/api/Loan/UploadDocuments';
    return this.apiservice.post(route, body).map(res => res);
  }

  public getAuditTrails(LoanFullID: string): Observable<ResponseModel<Array<LoanAuditTrail>>> {
    const route = `/api/Loan/AuditTrails/${LoanFullID}`;
    return this.apiservice.get(route).map(res => res);
  }

  public deleteAuditTrails(LoanFullID: string, type: string): Observable<ResponseModel<any>> {
    const route = `/api/Loan/DeleteAuditTrails/${LoanFullID}/${type}`;
    return this.apiservice.get(route).map(res => res);
  }

  public getLogs(params: LogsRequestParams): Observable<ResponseModel<LogResponseModel>> {
    const route = '/api/Loan/GetLogs';
    return this.apiservice.post(route, params).map(res => res);
  }

  //Nortridge methods

  SyncLoantoNortridge_full(loanid: string): Observable<ResponseModel> {
    const route = `/api/Nortridge/SyncNortridgetoDB?Loanid=` + loanid+ "&pro=Y";
    return this.apiservice.get(route).map(res => res);
  }

  SyncLoantoNortridge_Creditlines(loanid: string): Observable<ResponseModel> {
    const route = `/api/Nortridge/UpdateLoanCreditLineSingle?Loanid=` + loanid+ "&pro=Y";
    return this.apiservice.get(route).map(res => res);
  }

  SyncLoantoNortridge_Balances(loanid: string): Observable<ResponseModel> {
    const route = `/api/Nortridge/UpdateLoanFieldsSingle?Loanid=` + loanid+ "&pro=Y";
    return this.apiservice.get(route).map(res => res);
  }

  //Associations
   SaveAssociationImmediate(params: object): Observable<ResponseModel> {
    const route = '/api/Loan/AssociationImmediate';
    return this.apiservice.post(route, params).map(res => res);
  }

  //Loan MAster Update Migration Section as on 10/29/2019

  UpdateMigrationAdjustment(params: object): Observable<ResponseModel> {
    const route = '/api/Migration/UpdateMigrationAdjustment';
    return this.apiservice.post(route, params).map(res => res);
  }


  GenerateMigratedAdjustmentsForLoans(): Observable<ResponseModel> {
    const route = '/api/Migration/GenerateMigratedLoanAdjustment';
    return this.apiservice.get(route).map(res => res);
  }

  GenerateCashflowAndRiskCushionAdjustment(office_id: number): Observable<ResponseModel> {
    const body = {
      Office_ID: office_id
    };

    const route = '/api/Migration/GenerateCashflowAndRiskCushionAdjustment';
    return this.apiservice.post(route, body).map(res => res);
  }

  GenerateMktAndInsValueAdjustment(office_id: number): Observable<ResponseModel> {
    const body = {
      Office_ID: office_id
    };

    const route = '/api/Migration/GenerateMktAndInsValueAdjustment';
    return this.apiservice.post(route, body).map(res => res);
  }

  ClearMigratedAdjustmentsForLoans(office_id: number): Observable<ResponseModel> {
    const body = {
      Office_ID: office_id
    };

    const route = '/api/Migration/ClearMigratedAdjustments';
    return this.apiservice.post(route, body).map(res => res);
  }
}
