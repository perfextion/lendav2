import { Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { Loansettings } from '@lenda/models/loansettings';

import { DataService } from './data.service';

@Injectable()
export class ExportExcelService {
  private localloanobject: loan_model;

  private _updatedExportToExcelStatus = {};

  constructor(
    private localstorage: LocalStorageService,
    private dataService: DataService
  ) {
    this.localloanobject = this.localstorage.retrieve(environment.loankey);

    this.dataService.getLoanObject().subscribe(res => {
      this.localloanobject = res;
    });

    this.localstorage.observe(environment.loanidkey).subscribe(_ => {
      this._updatedExportToExcelStatus = {};
    });
  }

  removeExportToExcelTime(page: any) {
    if (!this._updatedExportToExcelStatus[page]) {
      this._updatedExportToExcelStatus[page] = true;
      this.updateExportToExcelTime(page, '');
    }
  }

  updateExportToExcelTime(page: any, time: any) {
    let exportExcelSettings = this.getExcelSettings();
    exportExcelSettings[page] = String(time);

    this.saveExcelSettings(exportExcelSettings);
  }

  validateImportExcel(page: any) {
    let exportExcelSettings = this.getExcelSettings();
    return !exportExcelSettings[page];
  }

  private getExcelSettings() {
    let loan_settings: Loansettings;

    try {
      loan_settings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
    } catch {
      loan_settings = <Loansettings>{};
    }

    let exportExcelSettings = loan_settings.Export_Excel_Settings;

    if (!exportExcelSettings) {
      exportExcelSettings = {};
    }

    return exportExcelSettings;
  }

  private saveExcelSettings(settings: any) {
    let loan_settings: Loansettings;

    try {
      loan_settings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
    } catch {
      loan_settings = <Loansettings>{};
    }

    if (!loan_settings.Export_Excel_Settings) {
      loan_settings.Export_Excel_Settings = {};
    }

    loan_settings.Export_Excel_Settings = settings;

    this.localloanobject.LoanMaster.Loan_Settings = JSON.stringify(
      loan_settings
    );
    this.localstorage.store(environment.loankey, this.localloanobject);
  }
}
