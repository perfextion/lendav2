import { Injectable } from '@angular/core';

import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { Subject, Observable } from 'rxjs';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { IUser } from '@lenda/models/ref-data-model';

@Injectable()
export class DataService {
  /**
   * the ariable to store the reference data in local storage
   */
  private refData: any;

  /**
   * an observale local loan object variable
   */
  private loanObject: Subject<loan_model> = new Subject<loan_model>();

  /**
   * Current Logged in User
   */
  public current_user: IUser;

  constructor(
    public localstorageservice: LocalStorageService,
    private session: SessionStorageService
  ) {
    this.loanObject.next(
      this.localstorageservice.retrieve(environment.loankey)
    );

    this.refData = this.localstorageservice.retrieve(
      environment.referencedatakey
    );

    this.current_user = this.session.retrieve('UID');
    this.session.observe('UID').subscribe(res => {
      this.current_user = res;
    });
  }

  /**
   * Will the passed loan object in to local storage and update the observable loanObject to notify all observers
   * @param loanObject the loan object to be set in the local storage
   */
  setLoanObject(loanObject: loan_model) {

    if (loanObject) {
      this.localstorageservice.store(environment.loankey, loanObject);
      let storedLoanObject = this.localstorageservice.retrieve(
        environment.loankey
      );
       this.loanObject.next(storedLoanObject);
    }
  }


  setLoanObjectwithoutsubscription(loanObject: loan_model) {
    if (loanObject) {
      this.localstorageservice.store(environment.loankey, loanObject);
      let storedLoanObject = this.localstorageservice.retrieve(
        environment.loankey
      );
      //this.loanObject.next(storedLoanObject);
    }
  }
  /**
   * get the latest loan object
   */
  getLoanObject(): Observable<loan_model> {
    return this.loanObject.asObservable();
  }

  /**
   * set the Reference data to the local storage
   * @param refData the reference data object to be stored in local storage
   */
  setRefData(refData: any) {
    if (refData) {
      this.localstorageservice.store(environment.referencedatakey, refData);
      this.refData = refData;
    }
  }

  /**
   * get the latest reference data
   */
  getRefData(): any {
    if(!this.refData){
      this.refData = this.localstorageservice.retrieve(environment.referencedatakey);
    }
    return this.refData;
  }
}

