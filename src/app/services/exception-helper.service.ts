import { loan_model, Loan_Association, Loan_Collateral, Collateral_Category_Code } from '@lenda/models/loanmodel';
import { Loan_Farm } from '@lenda/models/farmmodel.';
import { Loan_Policy } from '@lenda/models/insurancemodel';
import {
  RefDataModel,
  Ref_Document_Type,
  LoanMaster
} from '@lenda/models/ref-data-model';
import { RegEx } from '@lenda/shared/shared.constants';
import { Loan_Document } from '@lenda/models/loan/loan_document';
import { RefQuestions } from '@lenda/models/loan-response.model';
import { Loan_Disburse_Detail } from '@lenda/models/disbursemodels';

export class Eval_Helper {

  static Map_Table(Table_Name: string) {
    if (!Table_Name) {
      return null;
    }

    if(Table_Name.includes('Association')) {
      return 'Association';
    }

    switch (Table_Name.toUpperCase()) {
      case 'FARM':
      case 'FARMS':
      case 'LOAN_FARM':
      case 'LOAN_FARMS':
        return 'Farms';

      case 'YIELD':
      case 'CROPYIELD':
        return 'CropYield';

      case 'LOAN_ASSOCIATION':
      case 'LOAN_ASSOCIATIONS':
      case 'ASSOCIATION':
      case 'ASSOCIATIONS':
        return 'Association';

      case 'BUDGET':
      case 'EXPENSES':
      case 'LOANBUDGET':
      case 'LOAN_BUDGET':
        return Local_Table.LoanBudget;

      case 'BUYER':
      case 'BUY':
        return 'Association_BUY';

      case 'REBATOR':
      case 'REBATE':
      case 'REB':
        return 'Association_REB';

      case 'THIRDPARTY':
      case 'THIRD_PARTY':
      case 'THR':
        return 'Association_THR';

      case 'AIP':
        return 'Association_AIP';

      case 'AGENCY':
      case 'AGY':
        return 'Association_AGY';

      case 'LANDLORD':
      case 'LLD':
      case 'LAN':
        return 'Association_LLD';

      case 'LIENHOLDER':
      case 'LEI':
        return 'Association_LEI';

      case 'DISTRIBUTER':
      case 'DISTRIBUTOR':
      case 'DIS':
        return 'Association_DIS';

      case 'PRINCIPAL':
      case 'PRI':
        return 'Association_PRI';

      case 'EQUIPMENT':
      case 'EQUIPMENT_PROVIDER':
      case 'EQUIPMENTPROVIDER':
      case 'EQP':
        return 'Association_EQP';

      case 'GUARANTOR':
      case 'GUA':
          return 'Association_GUA';

      case 'BORROWER':
      case 'BOR':
          return 'Association_BOR';

      case 'SBI':
          return 'Association_SBI';

      case 'HARVESTER':
      case 'HARVESTOR':
      case 'HRV':
          return 'Association_HRV';

      case 'REFERRED_FROM':
      case 'REFERREDFROM':
      case 'REF':
        return 'Association_REF';

      case 'CREDIT_REFERENCE':
      case 'CRF':
      case 'CREDITREFERENCE':
          return 'Association_CRF';

      case 'POLICY':
      case 'POLICIES':
      case 'LOANPOLICY':
      case 'LOANPOLICIES':
      case 'LOAN_POLICY':
      case 'LOAN_POLICIES':
        return 'LoanPolicies';

      case 'QUESTION':
      case 'QUESTIONS':
      case 'REFQUESTION':
      case 'LOANQRESPONSE':
        return 'Questions';

      case 'CALCULATEDVARIABLES':
        return 'CalculatedVariables';


      case 'DISBURSE':
        return Local_Table.Disburse;

      case 'DISBURSE_EXPENSE':
        return Local_Table.Disburse_Expense;

      case 'DISBURSE_REIMBURSEMENT_REQUEST':
        return Local_Table.Disburse_Reimbursement_Request;

      case 'CROP':
      case 'CROPS':
      case 'LOAN_CROP':
      case 'LOANCROP':
      case 'LOANCROPS':
        return Local_Table.LoanCrops;

      case 'ASSOC_REPLICATION':
          return Local_Table.Assoc_Replication;

      case 'COLLATERAL':
      case 'LOAN_COLLATERAL':
          return Local_Table.COLLATERAL;

      default:
        return 'LoanMaster';
    }
  }

  static Get_Document_Key(
    doc: Ref_Document_Type,
    Local_Table_Name: string,
    loan: loan_model,
    entity: any,
    refdata: RefDataModel
  ) {
    let tbl_name = Local_Table_Name || Eval_Helper.Map_Table(doc.Table_Name);

    if(!tbl_name) {
      return '';
    }

    if (tbl_name == Local_Table.LoanMaster) {
      if (doc.Replication_Association_Type_Code && entity) {
        return loan.LoanMaster.Borrower_ID + '_' + entity.Assoc_Name;
      }
      return loan.LoanMaster.Borrower_ID;
    }

    if (tbl_name == Local_Table.Farms) {
      let f = entity as Loan_Farm;

      if(f.Owned == 0) {
        return f.Landowner;
      }

      return (
        f.Farm_State_ID + '_' + f.Farm_County_ID + '_' + f.FSN + '_' + f.Section
      );
    }

    if (tbl_name == Local_Table.Association || tbl_name.includes('Association') || tbl_name == Local_Table.Assoc_Replication) {
      let a = entity as Loan_Association;

      return a.Assoc_Name + '_' + a.Assoc_Type_Code;
    }

    if (tbl_name == Local_Table.LoanPolicies) {
      let a = entity as Loan_Policy;

      return a.Policy_Key;
    }

    if(tbl_name == Local_Table.COLLATERAL) {
      let a = entity as Loan_Collateral;
      if(a.Collateral_Category_Code == Collateral_Category_Code.StoredCrop) {
        return a.Collateral_Description + '|' + a.Measure_Code + '|' + a.Measure_Description + '|' + a.Crop_Detail + '|' + a.Crop_Type;
      }
      return a.Collateral_Description + '|' + a.Measure_Code + '|' + a.Measure_Description;
    }

    if (tbl_name == Local_Table.CropYield) {
      let crop = refdata.CropList.find(
        a => a.Crop_And_Practice_ID == entity.Crop_ID
      );

      try {
        return (
          crop.Crop_Code +
          '_' +
          crop.Crop_Type_Code +
          '_' +
          crop.Irr_Prac_Code +
          '_' +
          crop.Crop_Prac_Code
        );
      } catch {
        return '';
      }
    }

    if (tbl_name == Local_Table.Disburse_Reimbursement_Request) {
      let a = entity as Loan_Disburse_Detail;
      return a.Loan_Disburse_ID + '_' + a['Farm_ID'];
    }

    if(tbl_name == Local_Table.Disburse_Expense) {
      return entity.Loan_Disburse_ID + '_' + entity.Expense_ID;
    }

    if(tbl_name == Local_Table.LoanCrops) {
      return entity.Crop_ID + '_' + entity.Crop_Code;
    }

    return '';
  }

  static Get_Exception_Key(
    Table_Name: string,
    loan: loan_model,
    entity: any,
    refdata: RefDataModel
  ): any {
    let tbl_name = Eval_Helper.Map_Table(Table_Name);

    if (tbl_name == Local_Table.Questions) {
      let q: RefQuestions = entity;
      return 'Question_' + q.Question_ID;
    }

    if (tbl_name == Local_Table.LoanMaster) {
      return loan.LoanMaster.Borrower_ID;
    }

    if (tbl_name == Local_Table.Farms) {
      let f = entity as Loan_Farm;

      return (
        f.Farm_State_ID + '_' + f.Farm_County_ID + '_' + f.FSN + '_' + f.Section
      );
    }

    if (tbl_name == Local_Table.Association || tbl_name.includes('Association') || tbl_name == Local_Table.Assoc_Replication) {
      let a = entity as Loan_Association;

      return a.Assoc_Name + '_' + a.Assoc_Type_Code;
    }

    if (tbl_name == Local_Table.LoanPolicies) {
      let a = entity as Loan_Policy;

      return a.Policy_Key;
    }

    if (tbl_name == Local_Table.COLLATERAL) {
      let a = entity as Loan_Collateral;

      if(a.Collateral_Category_Code == Collateral_Category_Code.StoredCrop) {
        return a.Collateral_Category_Code + '_' + a.Collateral_Description + '_' + a.Measure_Code + '_' + a.Crop_Detail + '_' + a.Crop_Type;
      }

      return a.Collateral_Category_Code + '_' + a.Collateral_Description + '_' + a.Measure_Code;
    }

    if (tbl_name == Local_Table.CropYield) {
      let crop = refdata.CropList.find(
        a => a.Crop_And_Practice_ID == entity.Crop_ID
      );

      try {
        return (
          crop.Crop_Code +
          '_' +
          crop.Crop_Type_Code +
          '_' +
          crop.Irr_Prac_Code +
          '_' +
          crop.Crop_Prac_Code
        );
      } catch {
        return '';
      }
    }

    if(tbl_name == Local_Table.Disburse) {
      return entity.Loan_Disburse_ID;
    }

    if(tbl_name == Local_Table.Disburse_Expense) {
      return entity.Disburse_ID + '_' + entity.Expense_ID;
    }

    if(tbl_name == Local_Table.Disburse_Reimbursement_Request) {
      return entity.Farm_ID + '_' + entity.Loan_Disburse_ID;
    }

    if(tbl_name == Local_Table.LoanCrops) {
      return entity.Crop_ID + '_' + entity.Crop_Code;
    }

    if(Table_Name == Local_Table.LoanBudget) {
      return entity.Expense_Type_ID + '_' + entity.Expense_Type_Name;
    }

    return loan.LoanMaster.Borrower_ID;
  }

  static Get_Doc_Override_Name(
    doc: Ref_Document_Type,
    Table_Name: string,
    loan: loan_model,
    entity: any
  ) {
    let tbl_name = Eval_Helper.Map_Table(Table_Name);

    let key = '';

    if (tbl_name == Local_Table.LoanMaster) {
      key =
        loan.LoanMaster.Borrower_First_Name +
        ', ' +
        loan.LoanMaster.Borrower_Last_Name;

      if (doc.Replication_Association_Type_Code && entity) {
        let a = entity as Loan_Association;
        key += ' ' + entity.Assoc_Name;
      }
    }

    if (tbl_name == Local_Table.Farms) {
      key = entity.FSN;

      if (doc.Replication_Association_Type_Code) {
        key = key + ' ' + entity.Landowner;
      }
    }

    if (tbl_name == Local_Table.Association || tbl_name == Local_Table.Assoc_Replication) {
      key = entity.Assoc_Name;
    }

    if (tbl_name == Local_Table.COLLATERAL) {
      key = entity.Collateral_Category_Code + ' | '
          + (entity.Collateral_Description || '-') + ' | '
          + (entity.Measurement_Type_Name || '-') + ' | '
          + (entity.Measure_Description || '-');
    }

    if (doc.Document_Name_Override) {
      if (doc.Document_Name_Override.includes('{')) {
        return this.Replace(doc, Table_Name, loan, entity);
      } else {
        return doc.Document_Name_Override.replace('#', key);
      }
    }
  }

  static Get_Document_Details(
    doc: Ref_Document_Type,
    Table_Name: string,
    loan: loan_model,
    entity: any,
    refdata: RefDataModel
  ) {
    let tbl_name = Eval_Helper.Map_Table(Table_Name);

    if (tbl_name == Local_Table.LoanMaster) {
      let lm: LoanMaster = loan.LoanMaster;

      let key =
        lm.Borrower_First_Name +
        '|' +
        lm.Borrower_Last_Name +
        '|' +
        lm.Borrower_Email;

      if (doc.Replication_Association_Type_Code && entity) {
        key += '|' + entity.Assoc_Name;
      }

      return key;
    }

    if (tbl_name == Local_Table.Farms) {
      let f = entity as Loan_Farm;

      if(f.Owned == 0) {
        return f.Landowner;
      }

      return (
        f.Farm_State_ID +
        '|' +
        f.Farm_County_ID +
        '|' +
        f.FSN +
        '|' +
        f.Section +
        '|' +
        f.Landowner
      );
    }

    if (tbl_name == Local_Table.Association || tbl_name == Local_Table.Assoc_Replication) {
      let a = entity as Loan_Association;

      return a.Assoc_Name + '|' + a.Assoc_Type_Code;
    }

    if (tbl_name == Local_Table.LoanPolicies) {
      let a = entity as Loan_Policy;

      return a.Policy_Key;
    }

    if(tbl_name == Local_Table.COLLATERAL) {
      let a = entity as Loan_Collateral;
      if(a.Collateral_Category_Code == Collateral_Category_Code.StoredCrop) {
        return a.Collateral_Description + '|' + a.Measure_Code + '|' + a.Measure_Description + '|' + a.Crop_Detail + '|' + a.Crop_Type;
      }
      return a.Collateral_Description + '|' + a.Measure_Code + '|' + a.Measure_Description;
    }

    if (tbl_name == Local_Table.CropYield) {
      let crop = refdata.CropList.find(
        a => a.Crop_And_Practice_ID == entity.Crop_ID
      );

      try {
        return (
          crop.Crop_Code +
          '|' +
          crop.Crop_Type_Code +
          '|' +
          crop.Irr_Prac_Code +
          '|' +
          crop.Crop_Prac_Code
        );
      } catch {
        return '';
      }
    }

    if(tbl_name == Local_Table.Disburse) {
      let disburse = entity as Loan_Disburse_Detail;
      return disburse.Budget_Expense_ID + '_' + disburse.Loan_Disburse_ID;
    }

    if(tbl_name == Local_Table.Disburse_Expense) {
      return entity.Loan_Disburse_ID + '_' + entity.Expense_ID;
    }

    if(tbl_name == Local_Table.Disburse_Reimbursement_Request) {
      return entity.Farm_ID + '_' + entity.Loan_Disburse_ID;
    }

    if(tbl_name == Local_Table.LoanCrops) {
      return entity.Crop_ID + '_' + entity.Crop_Code;
    }

    return '';
  }

  static Get_Replication_Association_ID(
    doc: Ref_Document_Type,
    Table_Name: string,
    loan: loan_model,
    entity: any
  ): number {
    Table_Name = this.Map_Table(Table_Name);

    if (entity) {
      if (Table_Name == Local_Table.Association) {
        let a = entity as Loan_Association;
        return a.Assoc_ID;
      }

      if (Table_Name == Local_Table.CropYield) {
        return entity['Crop_ID'];
      }

      if (Table_Name == Local_Table.Farms) {
        let f = entity as Loan_Farm;

        return f.Farm_ID;
      }

      if (Table_Name == Local_Table.LoanMaster) {
        if (doc.Replication_Association_Type_Code) {
          return entity['Assoc_ID'];
        }
        return loan.LoanMaster.Borrower_ID;
      }

      if (Table_Name == Local_Table.LoanPolicies) {
        return entity['Loan_Policy_ID'];
      }
    }
    return 0;
  }

  static Fields(Field_ID: string) {
    try {
      if (!Field_ID) {
        return [];
      }
      return Field_ID.split(',');
    } catch (ex) {
      return [];
    }
  }

  static isEqual(value: any, other: any) {
    if (value == other) return true;
    // Get the value type
    let type = Object.prototype.toString.call(value);

    // If the two objects are not the same type, return false
    if (type !== Object.prototype.toString.call(other)) return false;

    // If items are not an object or array, return false
    if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

    // Compare the length of the length of the two items
    let valueLen =
      type === '[object Array]' ? value.length : Object.keys(value).length;
    let otherLen =
      type === '[object Array]' ? other.length : Object.keys(other).length;
    if (valueLen !== otherLen) return false;

    // Compare two items
    let compare = function(item1, item2) {
      // Get the object type
      let itemType = Object.prototype.toString.call(item1);

      // If an object or array, compare recursively
      if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
        if (!this.isEqual(item1, item2)) return false;
      }

      // Otherwise, do a simple comparison
      else {
        // If the two items are not the same type, return false
        if (itemType !== Object.prototype.toString.call(item2)) return false;

        // Else if it's a function, convert to a string and compare
        // Otherwise, just compare
        if (itemType === '[object Function]') {
          if (item1.toString() !== item2.toString()) return false;
        } else {
          if (item1 !== item2) return false;
        }
      }
    };

    // Compare properties
    if (type === '[object Array]') {
      for (let i = 0; i < valueLen; i++) {
        if (compare(value[i], other[i]) === false) return false;
      }
    } else {
      for (let key in value) {
        if (value.hasOwnProperty(key)) {
          if (compare(value[key], other[key]) === false) return false;
        }
      }
    }

    // If nothing failed, return true
    return true;
  }

  static ReplaceHash_Ex(
    str: string,
    loan: loan_model,
    Table_Name: string,
    entity: any
  ) {
    if (str.includes('##')) {
      const regex = new RegExp(RegEx.BetweenTwoHashes, 'gi');

      str = str.replace(regex, match => {
        let code = match.substring(
          match.indexOf('##') + 2,
          match.lastIndexOf('##')
        );

        let val = '';

        if (Table_Name == Local_Table.LoanMaster) {
          if (code == 'LoanMaster.Borrower_Rating') {
            val =  '*'.repeat(eval(`loan.` + code));
          }
          val =  eval(`loan.` + code);
        } else {
          if (code.includes('.')) {
            code = code.split('.')[1];
          }

          if (entity) {
            val = eval(`entity.` + code);
          } else {
            val = eval(`loan.` + code);
          }
        }

        if(val == null || val == undefined || (val && String(val).trim() == '')) {
          return '-';
        }
        return val;
      });

      return str;
    }

    return str;
  }

  static ReplaceHash_Multiple(
    doc: Ref_Document_Type,
    Table_Name: string,
    loan: loan_model,
    entity: any
  ) {
    let str = doc.Document_Name_Override;

    if (str.includes('##')) {
      const regex = new RegExp(RegEx.BetweenTwoHashes, 'gi');
      str = str.replace(regex, match => {
        let code = match.substring(
          match.indexOf('##') + 2,
          match.lastIndexOf('##')
        );

        if (Table_Name == Local_Table.LoanMaster) {
          if (doc.Replication_Association_Type_Code && entity) {
            return eval(`entity.Assoc_Name`);
          } else {
            return eval(`loan.` + code);
          }
        }

        if (Table_Name.includes('Association')) {
          return eval(`entity.Assoc_Name`);
        }

        if (entity) {
          return eval(`entity.` + code);
        } else {
          return eval(`loan.` + code);
        }
      });
      return str;
    }

    return str;
  }

  static Replace(
    doc: Ref_Document_Type,
    Table_Name: string,
    loan: loan_model,
    entity: any
  ) {
    let str = doc.Document_Name_Override;

    let regex = new RegExp(RegEx.BetweenCurlyBraces, 'gi');
    str = str.replace(regex, match => {
      let code = match.substring(match.indexOf('{') + 1, match.indexOf('}'));
      let value = '';

      if (Table_Name == Local_Table.LoanMaster) {
        if (doc.Replication_Association_Type_Code && entity) {
          value = eval(`entity.Assoc_Name`);
        } else {
          value = eval(`loan.` + code);
        }
      } else if (Table_Name && Table_Name.includes('Association')) {
        value = eval(`entity.Assoc_Name`);
      } else {
        if (entity) {
          value = eval(`entity.` + code);
        } else {
          value = eval(`loan.` + code);
        }
      }

      if(!value) {
        value = '-';
      }

      return value;
    });

    return str;
  }

  static EvalComplexHypercode(
    hypercode: string,
    loan: loan_model,
    entity: any
  ): boolean {
    const reg = /\&\& | \|\|/;

    if (hypercode) {
      if (hypercode.includes('||') || hypercode.includes('&&')) {
        let hypercodes = hypercode.split(reg);

        hypercodes.forEach(a => {
          let v = eval('entity.' + a);
          hypercode = hypercode.replace(a, v);
        });

        let value = eval(hypercode);
        return value;
      } else {
        let v = eval('entity.' + hypercode);
        return v;
      }
    }
    return false;
  }

  static Is_Valid_Data(Table_Name: string, loan: loan_model, entity: any) {
    if (entity) {
      if (Table_Name == Local_Table.Association) {
        let a = entity as Loan_Association;
        return !!a.Assoc_Name;
      }

      if (Table_Name == Local_Table.CropYield) {
        return entity['Crop_ID'];
      }

      if (Table_Name == Local_Table.Farms) {
        let f = entity as Loan_Farm;

        return !!f.Farm_County_ID && !!f.Farm_State_ID;
      }

      if (Table_Name == Local_Table.LoanPolicies) {
        return !!entity['Loan_Policy_ID'];
      }
    }
    return !!loan.LoanMaster.Borrower_ID;
  }

  static Temp_ID(
    doc: Ref_Document_Type,
    Local_Table_Name: string,
    loan: loan_model,
    entity: any
  ) {
    let Table_Name = Local_Table_Name || this.Map_Table(doc.Table_Name);

    if (entity) {
      if (Table_Name == Local_Table.Association || Table_Name == Local_Table.Assoc_Replication) {
        let a = entity as Loan_Association;
        return a.Assoc_ID;
      }

      if (Table_Name == Local_Table.CropYield) {
        return entity['Crop_ID'];
      }

      if (Table_Name == Local_Table.Farms) {
        let f = entity as Loan_Farm;

        if(f.Owned == 0) {
          return f.Landowner;
        }

        return f.Farm_ID;
      }

      if(Table_Name == Local_Table.COLLATERAL) {
        let c = entity as Loan_Collateral;
        return c.Collateral_ID;
      }

      if (Table_Name == Local_Table.LoanPolicies) {
        return entity['Loan_Policy_ID'];
      }

      if (Table_Name == Local_Table.LoanMaster) {
        if (doc.Replication_Association_Type_Code) {
          let a = entity as Loan_Association;
          return a.Assoc_ID;
        }
        return loan.LoanMaster.Borrower_ID;
      }

      if(Table_Name == Local_Table.Disburse) {
        let disburse = entity as Loan_Disburse_Detail;
        return disburse.Loan_Disburse_ID;
      }

      if(Table_Name == Local_Table.Disburse_Reimbursement_Request) {
        return entity.Loan_Disburse_ID;
      }
    }
    return loan.LoanMaster.Borrower_ID;
  }

  static Get_ID_And_Key(
    doc: Loan_Document,
    ref_doc: Ref_Document_Type,
    Local_Table_Name: string,
    loan: loan_model,
    entity: any
  ) {
    try {
      let obj = JSON.parse(doc.Document_Key);
      return {
        Key: obj.Key,
        ID: obj.ID
      };
    } catch {
      return {
        Key: doc.Document_Key,
        ID: this.Temp_ID(ref_doc, Local_Table_Name, loan, entity)
      };
    }
  }
}

export enum Local_Table {
  Farms = 'Farms',
  CropYield = 'CropYield',
  Association = 'Association',
  LoanPolicies = 'LoanPolicies',
  LoanMaster = 'LoanMaster',
  Questions = 'Questions',
  CalculatedVariables = 'CalculatedVariables',
  Disburse = 'Disburse',
  Disburse_Expense = 'Disburse_Expense',
  Disburse_Reimbursement_Request = 'Disburse_Reimbursement_Request',
  LoanCrops = 'LoanCrops',
  Assoc_Replication = 'Assoc_Replication',
  COLLATERAL = 'COLLATERAL',
  LoanBudget = "LoanBudget"
}
