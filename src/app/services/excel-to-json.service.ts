import { Injectable } from '@angular/core';

import * as XLSX from 'ts-xlsx';

@Injectable()
export class ExcelToJsonService {
  arrayBuffer: any;

  /**
   * Json value for file
   * @param fileReader filereader sent to read buffer
   * Returns json
   */
  attachFileReaderEvent(fileReader: FileReader) {
    this.arrayBuffer = fileReader.result;
    let data = new Uint8Array(this.arrayBuffer);
    let arr = new Array();

    for (let i = 0; i != data.length; ++i) {
      arr[i] = String.fromCharCode(data[i]);
    }

    let bstr = arr.join('');

    let workbook = XLSX.read(bstr, { type: 'binary' });
    let first_sheet_name = workbook.SheetNames[0];
    let worksheet = workbook.Sheets[first_sheet_name];
    return XLSX.utils.sheet_to_json(worksheet, { raw: true });
  }
}
