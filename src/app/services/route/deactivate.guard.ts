import { Injectable } from '@angular/core';
import {
  CanDeactivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';

import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoanApiService } from '../loan/loanapi.service';
import { LoggingService } from '../Logs/logging.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { ValidationErrorsCountService } from '@lenda/Workers/calculations/validationerrorscount.service';
import { ToasterService } from '../toaster.service';
import { JsonConvert } from 'json2typescript';
import { loan_model } from '@lenda/models/loanmodel';
import { PublishService } from '../publish.service';

export interface CanComponentDeactivate {
  confirm(): boolean;
}

@Injectable()
export class DeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
  //NOT IN USE
  constructor(
    private alertify: AlertifyService,
    public localStorageService: LocalStorageService,
    public loanapi: LoanApiService,
    public logging: LoggingService,
    public loanserviceworker: LoancalculationWorker,
    public toasterService: ToasterService,
    public route: Router,
    public pubservice: PublishService,
    private validationcount: ValidationErrorsCountService
  ) {}

  canDeactivate(
    component: CanComponentDeactivate,
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): Observable<boolean> {
    if (component.confirm() && nextState.url.includes('loanoverview')) {
      // if(this.checkLoanSync(next)) {
      this.checkifsyncdone().subscribe(p => {
        if (p == true) {
          this.route.navigateByUrl(nextState.url.toString());
        }
      });
      // }
      return Observable.of(false);
    } else {
      return Observable.of(true);
    }
  }

  checkifsyncdone(): Observable<boolean> {
    let localloanobject = this.localStorageService.retrieve(
      environment.loankey
    ) as loan_model;

    localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);
    this.Remove_Invalid_Crops(localloanobject);

    return new Observable<boolean>(observer => {
      if (PublishService.isSyncInProgress == false) {
        PublishService.isSyncInProgress = true;
        this.pubservice.syncCompleted();
        this.loanapi.syncloanobject(localloanobject).subscribe(respone => {
          if (respone.ResCode == 1) {
            this.localStorageService.store(environment.modifiedbase, []);
            this.loanapi
              .getLoanById(localloanobject.Loan_Full_ID)
              .pipe(
                finalize(() => {
                  PublishService.isSyncInProgress = false;
                })
              )
              .subscribe(res => {
                this.logging.checkandcreatelog(
                  3,
                  'Overview',
                  'APi LOAN GET with Response ' + res.ResCode
                );
                if (res.ResCode == 1) {
                  this.toasterService.success('Records Synced');
                  let jsonConvert: JsonConvert = new JsonConvert();
                  this.loanserviceworker.performcalculationonloanobject(
                    jsonConvert.deserialize(res.Data, loan_model)
                  );
                  this.loanserviceworker.saveValidationsToLocalStorage(res.Data);
                  observer.next(true);
                } else {
                  this.toasterService.error(
                    'Could not fetch Loan Object from API'
                  );
                  observer.next(false);
                }
              });
          } else {
            this.toasterService.error(respone.Message || 'Error in Sync');
            PublishService.isSyncInProgress = false;
            observer.next(false);
          }
        }, error => {
          PublishService.isSyncInProgress = false;
        });
      } else {
        observer.next(false);
      }
    });
  }

  Remove_Invalid_Crops(localloanobject: loan_model): any {
    let refdata = this.localStorageService.retrieve(environment.referencedatakey);

    if(localloanobject.CropYield) {
      for(let CropYield of localloanobject.CropYield) {
        let cropIndex = refdata.CropList.findIndex(a => a.Crop_And_Practice_ID == CropYield.Crop_ID);

        if(cropIndex == -1) {
          localloanobject.CropYield.splice(localloanobject.CropYield.indexOf(CropYield), 1);
        }
      }
    }
  }

  /**
   * Check if no Validation Errors for the said Page.
   */
  checkLoanSync(next: ActivatedRouteSnapshot) {
    let hasValidations = this.validationcount.hasLevel2Validations(next.data.page);

    if (hasValidations) {
      this.alertify.alert(
        'Alert',
        'Please resolve all critical validations.'
      );
      return false;
    } else {
      return true;
    }
  }
}
