import { ISubscription } from 'rxjs/Subscription';

interface AutoUnsubscribeArgs {
  subscriptionList?: Array<string>;
  subscriptionKey?: string;
}

export class SubscriptionList {
  [key: string]: ISubscription;
}

export function AutoUnsubscribe(args?: AutoUnsubscribeArgs) {
  return function(constructor) {
    const original = constructor.prototype.ngOnDestroy;

    constructor.prototype.ngOnDestroy = function() {
      if (args) {
        if (args.subscriptionList) {
          for (const prop in args.subscriptionList) {
            const property = this[prop] as ISubscription;
            if (property && typeof property.unsubscribe === 'function') {
              property.unsubscribe();
            }
          }
        }

        const subscriptionKey = args.subscriptionKey || 'subscriptionList';

        if (
          this[subscriptionKey] &&
          this[subscriptionKey] instanceof SubscriptionList
        ) {
          const subscriptionList = this[subscriptionKey] as SubscriptionList;
          for (const prop in subscriptionList) {
            const property = subscriptionList[prop];
            if (property && typeof property.unsubscribe === 'function') {
              property.unsubscribe();
            }
          }
        }
      }

      if (original && typeof original === 'function') {
        original.apply(this, arguments);
      }
    };
  };
}
