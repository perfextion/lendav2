import { LoggingService } from '@lenda/services/Logs/logging.service';
import { Injectable, EventEmitter } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { RefDataModel, RefDiscount } from '@lenda/models/ref-data-model';
import { DiscountKeys } from '@lenda/models/discount/discount-codes';

@Injectable()
export class RefDataService {
  private refData: RefDataModel;

  riskScale: Array<number>;
  returnScale: Array<number>;

  returnScaleUpdated = new EventEmitter<number[]>(true);

  constructor(private localstorage: LocalStorageService, private logging: LoggingService) {
    this.setRefData();
  }

  //#region maturity date

  getDefaultMaturityDate(Crop_Year: any) {
    this.setRefData();
    let matDate = this.getDefaultMaturityDateString(Crop_Year);
    return new Date(matDate);
  }

  /**
   * @returns Default Maturity date in YYYY/MM/DD
   */
  getDefaultMaturityDateString(Crop_Year: any) {
    // tslint:disable-next-line: radix
    let currentYear = parseInt(Crop_Year);

   this.setRefData();

    let _disc = this.getDiscount(
      DiscountKeys.MaturityDate
    ).Discount_Value;
    //What if the found value is null
    let disc = "0";

    if (_disc != null) {
      disc = _disc.toString();
    } else {
      //log it
      this.logging.checkandcreatelog(
        0,
        'Discount Ref info',
        'Couldnot get the maturity date value for  ' + Crop_Year
      );

      disc = '1215';
    }

    let matDate = `${currentYear}/${disc.substring(0, 2)}/${disc.substring(
      2,
      4
    )}`;

    return matDate;
  }

  getCostToCarry() {
    this.setRefData();

    let disc = this.refData.Discounts.find(
      d => d.Discount_Key == DiscountKeys.COST_TO_CARRY
    );
    if (disc) {
      return disc.Discount_Value;
    }
    return 12;
  }

  //#endregion maturity date

  private setRefData() {
    if (!this.refData) {
      this.refData = this.localstorage.retrieve(environment.referencedatakey);
    }
  }

  getRiskScale(): Array<number> {
    if (!this.riskScale) {
      let scale = [0, 2.5, 5.0, 7.5, 10.0];

      this.setRefData();

      try {
        scale = this.refData.Discounts.filter(d =>
          d.Discount_Key.includes(DiscountKeys.RISK_SCALE)
        ).map(d => d.Discount_Value);
      } catch {
        scale = [0, 2.5, 5.0, 7.5, 10.0];
      }

      this.riskScale = scale;
    }

    return this.riskScale;
  }

  getReturnScale(factor = 0): Array<number> {
    this.setRefData();
    this.returnScale = [5.0, 6.4, 7.8, 10.8, 13.7];

    try {
      this.returnScale = this.refData.Discounts.filter(d =>
        d.Discount_Key.includes(DiscountKeys.RETURN_SCALE)
      )
        .map(d => d.Discount_Value)
        .map(d => d + factor);
    } catch {
      this.returnScale = [5.0, 6.4, 7.8, 10.8, 13.7];
    }

    return this.returnScale;
  }

  updateReturnScale(updateUnit: number) {
    let returnScale = this.getReturnScale();
    returnScale = returnScale.map(a => a + updateUnit);

    this.returnScale = returnScale;
    this.returnScaleUpdated.next(returnScale);
  }

  private getDiscount(disc_key: string): RefDiscount {
    try {
      return this.refData.Discounts.find(d => d.Discount_Key == disc_key);
    } catch {
      return <RefDiscount>{};
    }
  }
}
