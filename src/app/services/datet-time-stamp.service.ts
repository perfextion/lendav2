import { Injectable } from '@angular/core';

@Injectable()
export class DatetTimeStampService {

  dateTimeStamp = new Date();

  constructor() { }

  getDate() {
    return `${this.dateTimeStamp.getMonth() + 1}.${this.dateTimeStamp.getDate()}.${this.dateTimeStamp.getFullYear()}`;
  }

  getTime() {
    let time = this.dateTimeStamp.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }).replace(':', '.');
    if(this.dateTimeStamp.getHours() < 10) {
      time = '0' + time;
    }

    return time.replace(' ', '');
  }

  getTimeStamp() {
    return `${this.getDate()}_${this.getTime()}`;
  }
}
