import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MaterialModule } from '@lenda/shared/material.module';

import { RatingStarsComponent } from '@lenda/ui-components/rating-stars/rating-stars.component';
import { IconLoanStatusComponent } from '@lenda/ui-components/icon-loan-status/icon-loan-status.component';
import { RangebarComponent } from '@lenda/ui-components/rangebar/rangebar.component';
import { SvgTooltipComponent } from '@lenda/ui-components/svg-tooltip/svg-tooltip.component';
import { BtnPublishComponent } from '@lenda/ui-components/btn-publish/btn-publish.component';
import { AgGridNoRowsAvailableComponent } from '@lenda/ui-components/ag-grid-no-rows-available/ag-grid-no-rows-available.component';
import { BtnGroupExpandCollapseComponent } from '@lenda/ui-components/btn-group-expand-collapse/btn-group-expand-collapse.component';
import { BtnFloatingActionComponent } from '@lenda/ui-components/btn-floating-action/btn-floating-action.component';
import { IconPendingActionStatusComponent } from './icon-pending-action-status/icon-pending-action-status.component';
import { ImportFileComponent } from './import-file/import-file.component';
import { ControlMessagesComponent } from '@lenda/directives/control-messages.component';
import { CalendarInputComponent } from './calendar-input/calendar-input.component';

import { HasPermissionDirective } from '@lenda/directives/has-permission.directive';

import { FavouriteLoanPipe } from '@lenda/pipes/favourite-loan.pipe';

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: [
    RangebarComponent,
    SvgTooltipComponent,
    BtnPublishComponent,
    RatingStarsComponent,
    AgGridNoRowsAvailableComponent,
    BtnGroupExpandCollapseComponent,
    IconLoanStatusComponent,
    BtnFloatingActionComponent,
    IconPendingActionStatusComponent,
    ImportFileComponent,
    HasPermissionDirective,
    FavouriteLoanPipe,
    ControlMessagesComponent,
    CalendarInputComponent
  ],
  exports: [
    CommonModule,
    RangebarComponent,
    SvgTooltipComponent,
    BtnPublishComponent,
    RatingStarsComponent,
    AgGridNoRowsAvailableComponent,
    BtnGroupExpandCollapseComponent,
    IconLoanStatusComponent,
    BtnFloatingActionComponent,
    IconPendingActionStatusComponent,
    ImportFileComponent,
    HasPermissionDirective,
    FavouriteLoanPipe,
    ControlMessagesComponent,
    CalendarInputComponent
  ]
})
export class UiComponentsModule {}
