import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-rating-stars',
  templateUrl: './rating-stars.component.html',
  styleUrls: ['./rating-stars.component.scss']
})
export class RatingStarsComponent implements OnInit {
  @Input()
  rating: number = 0;

  @Input()
  isSmallSize = true;

  @Input() inGreenColor : boolean = false;

  constructor() {}

  ngOnInit() {}

  get width() {
    if(!this.isSmallSize){
      return '120px';
    }
    return '100px';
  }

  get imageHeight() {
    if (this.isSmallSize) {
      return 12;
    }
    return 20;
  }

  get imageWidth() {
    if (this.isSmallSize) {
      return 12;
    }
    return 20;
  }

  get height() {
    if (this.isSmallSize) {
      return '12px';
    }
    return '24px';
  }

  get x() {
    if (this.isSmallSize) {
      return 15;
    }
    return 20;
  }

  get y() {
    if (this.isSmallSize) {
      return 20;
    }
    return 25;
  }
}
