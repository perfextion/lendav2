import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ViewEncapsulation,
  OnChanges
} from '@angular/core';

import {
  NgbDateStruct,
  NgbDatepicker,
  NgbDate
} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-calendar-input',
  templateUrl: './calendar-input.component.html',
  styleUrls: ['./calendar-input.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CalendarInputComponent implements OnChanges {
  @Input() ID: number;
  @Input() model: string;
  @Output() modelChange = new EventEmitter<string>();

  @Input() title: string;

  @Input() disabled: boolean = false;

  value: NgbDateStruct = <NgbDateStruct>{};

  @ViewChild('dp') datepicker: NgbDatepicker;

  constructor() {}

  ngOnChanges() {
    if (this.model) {
      this.value = this.getDate();
    }

    setTimeout(() => {
      if (this.datepicker) {
        this.datepicker.writeValue(this.value);
        this.datepicker.navigateTo(this.value);
      }
    }, 1000);
  }

  getDate() {
    let d = new Date(this.model);
    return <NgbDateStruct>{
      day: d.getDate(),
      year: d.getFullYear(),
      month: d.getMonth() + 1
    };
  }

  valueChange($event: NgbDate) {
    this.value = $event;

    let v = new Date(this.value.year, this.value.month - 1, this.value.day);
    this.model = v.toISOString();
  }

  applyChanges() {
    if (!this.disabled) {
      this.modelChange.emit(this.model);
    }
  }
}
