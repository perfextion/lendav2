import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-btn-group-expand-collapse',
  templateUrl: './btn-group-expand-collapse.component.html',
  styleUrls: ['./btn-group-expand-collapse.component.scss']
})
export class BtnGroupExpandCollapseComponent implements OnInit {
  @Output() expand = new EventEmitter();
  @Output() collapse = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  /**
   * Expand all functionality for all accordions in the page
   */
  expandAll() {
    this.expand.emit();
  }

  /**
   * Collapse all functionality for all accordions in the page
   */
  collapseAll() {
    this.collapse.emit();
  }
}
