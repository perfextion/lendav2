import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'app-svg-tooltip',
  templateUrl: './svg-tooltip.component.html',
  styleUrls: ['./svg-tooltip.component.scss']
})
export class SvgTooltipComponent implements OnInit {
  @Input() elRef: ElementRef<HTMLDivElement>;

  ngOnInit() {}
}
