import { Injectable, ElementRef, Renderer2, Input } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../environments/environment.prod';
import { SchematicsSettings } from '@lenda/models/schematics/index.model';
import { FlowchartService } from '@lenda/components/flowchart/flowchart.service';
import { SchematicsItemSettings } from '@lenda/models/schematics/schematics-item-settings.model';
import { CriticalItem, CriticalItemAction, CriticalItemLevel } from '@lenda/models/schematics/base-entities/critical-item.model';

/**
 * Shared service for collateral
 */
@Injectable()
export class SvgTooltipService {
  private textElements = [];
  private lineBreaks = [];
  private isDirty;
  private type;
  private schematicsSettings: SchematicsSettings;

  constructor(
    private localstorage: LocalStorageService,
    private elRef: ElementRef,
    private renderer: Renderer2,
    private flowchartService: FlowchartService
  ) { }

  /**
   * Method to add tooltip on icon bubbles
   * @param targetElements Target tooltip icon
   * @param type tooltip icon type
   * @param max Maximum number of items in tooltip ( Optional )
   */
  initTooltip(targetElements, type, max?: number) {

    this.type = type;

    for (let icon of targetElements) {
      // Mouseenter event for tooltip
      icon.addEventListener('mouseover', event => {
        this.showTooltipText(event, max);
      });

      // Mouseleave event for tooltip
      icon.addEventListener('mouseleave', event => {
        this.removeTooltipText(event);
      });
    }
  }

  showTooltipText(event, max?: number) {
    let tooltip = this.elRef.nativeElement.querySelector('.tooltip');
    // TODO: Move this logic out of this component
    // ===
    let hoverNodeId = this.getAttribute(event.target, 'data-tooltip-source');
    let texts = this.getTooltipText(hoverNodeId);

    // Hardcoded tooltip value for Range
    // TODO: Remove these and make it dynamic
    if (this.type === 'range') {
      texts = ['Current Value: << value >>', 'Strong: << Strong value >>'];
    }
    // ===
    this.addTooltipNodes(tooltip, texts, event, max);
  }

  removeTooltipText(event) {
    let tooltip = this.elRef.nativeElement.querySelector('.tooltip');
    tooltip.querySelectorAll('div').forEach(function (node) {
      node.parentNode.removeChild(node);
    });
    this.renderer.setStyle(tooltip, 'height', '0px');
    this.renderer.setStyle(tooltip, 'top', '0px');
    this.renderer.removeClass(tooltip, 'active');
    this.isDirty = false;
  }

  getTooltipText(source) {
    this.schematicsSettings = this.flowchartService.getSchematicSettings();
    let nodeSettings: SchematicsItemSettings = this.schematicsSettings[source];

    let texts = [];

    if(source == 'committee'){
      if (nodeSettings && nodeSettings.infos.length !== 0) {
        return nodeSettings.infos;
      }
      return texts;
    } else {
      if (nodeSettings && nodeSettings.infos.length !== 0) {
        for (let item of nodeSettings.infos) {
          texts.push(item.infoText);
        }
      }
      return texts;
    }
  }


  addTooltipNodes(tooltip: HTMLElement, texts: Array<any>, event, max?: number, hoverId?) {
    if (!this.isDirty && texts.length > 0) {
      this.textElements = [];
      this.lineBreaks = [];

      // Tooltip Parent Container Component
      let parentElement = this.renderer.createElement('div');
      this.renderer.addClass(parentElement, 'tooltip-container');

      this.renderer.addClass(tooltip, 'active');
      this.renderer.setStyle(tooltip, 'left', event.pageX + 'px');

      if (max && max > 0 && max < texts.length) {
        for (let i = 0; i < max; i++) {
          this.addTooltipTextNode(texts[i], parentElement, tooltip, event);
        }
        this.addTooltipTextNode('...', parentElement, tooltip, event);
      } else {
        for (let item of texts) {
          this.addTooltipTextNode(item, parentElement, tooltip, event);
        }
      }

      this.renderer.setStyle(tooltip,'height', 20 * this.textElements.length + 10 + 'px');
      this.renderer.setStyle(tooltip,'top', event.pageY - (10 + 25 * this.textElements.length) + 'px');

      this.isDirty = true;
    }
  }

  private addTooltipTextNode(item: any, parentElement: any, tooltip: any, event: any) {
    let text: HTMLElement, lineBreak: HTMLElement, pTag: HTMLElement;

    if(Object.prototype.toString.call(item) == '[object Object]'){
      if(item.infoText && item.isActionRequired != undefined) {
        text = this.renderer.createText(item.infoText);
        pTag = this.getTextNodeWithCheckbox(item.infoText, item.isActionRequired);
      } else {
        text = this.renderer.createText(item.infoText);
        pTag = this.getTextNodeWithCheckbox(item.infoText, null, false);
      }
    } else {
      text = this.renderer.createText(item);
      pTag = this.getTextNodeWithCheckbox(item || "[Sample Text]", null, false);
    }

    lineBreak = this.renderer.createElement('br');
    this.textElements.push(text);
    this.lineBreaks.push(lineBreak);

    // this.renderer.appendChild(parentElement, text);
    // this.renderer.appendChild(parentElement, lineBreak);

    this.renderer.appendChild(parentElement, pTag);
    this.renderer.appendChild(tooltip, parentElement);
  }

  /**
   * Create Text node with `Text and Icon` based on params
   * @param text Text
   * @param action (`Accepted`, `Rejected`, `Pending`)
   * @param showIcon Visibility of Icon
   */
  private getTextNodeWithCheckbox(text: string, action: CriticalItemAction, showIcon = true): HTMLElement {
    let pTag = this.renderer.createElement('span');
    this.renderer.addClass(pTag, 'tooltip-ellipsis');

    let span = this.renderer.createElement('span');

    // Specific to Committee for now
    if(showIcon) {
      if (text != '...') {
        let checkbox = this.renderer.createElement('i');
        this.renderer.addClass(checkbox, 'material-icons');
        this.renderer.addClass(checkbox, 'mat-12');

        if (action == CriticalItemAction.Success) {
          this.renderer.appendChild(checkbox, this.renderer.createText('check_box'));
        } else if(action == CriticalItemAction.Rejected) {
          this.renderer.appendChild(checkbox, this.renderer.createText('clear'));
        } else {
          this.renderer.appendChild(checkbox, this.renderer.createText('check_box_outline_blank'));
        }

        this.renderer.appendChild(pTag, checkbox);
        this.renderer.setStyle(span, 'padding-left', '10px');
      } else {
        this.renderer.setStyle(span, 'padding-left', '25px');
      }
    } else {
      this.renderer.setStyle(span, 'padding-left', '5px');
    }

    this.renderer.appendChild(span, this.renderer.createText(text));
    this.renderer.appendChild(pTag, span);

    return pTag;
  }

  //#region TooltipForIcons

  /**
   * Add tooltip on `Loan icons` (Farm, Committee, budget etc.)
   *
   * Add a class `loan-icon` to the icon to enable popover over icon
   * @param targetElements Target tooltip loan-icons
   * @param type tooltip icon type
   * @param max Maximum number of items in tooltip ( Optional )
   */
  initTooltipForIcons(targetElements: Array<HTMLElement>, type: string, max?: number): any {
    this.type = type;

    for (let icon of targetElements) {
      // Mouseenter event for tooltip
      icon.addEventListener('mouseover', event => {
        this.showTooltipTextForIcon(event, max);
      });

      // Mouseleave event for tooltip
      icon.addEventListener('mouseleave', event => {
        this.removeTooltipText(event);
      });
    }
  }

  private showTooltipTextForIcon(event, max?: number): any {
    let tooltip = this.elRef.nativeElement.querySelector('.tooltip');
    let hoverNodeId = this.getAttribute(event.target, 'data-tooltip-source');

    if (!!hoverNodeId) {
      let texts = this.getTooltipTextForIcon(hoverNodeId);

      if(texts.length > 0) {
        this.addTooltipNodes(tooltip, texts, event, max, hoverNodeId);
      }
    }
  }

  private getTooltipTextForIcon(source) {
    this.schematicsSettings = this.flowchartService.getSchematicSettings();
    let nodeSettings: SchematicsItemSettings = this.schematicsSettings[source];

    let texts = [];

    try {
      if (nodeSettings && nodeSettings.hoverInfos && nodeSettings.hoverInfos.length !== 0) {
        for (let item of nodeSettings.hoverInfos) {
          texts.push(item.infoText);
        }
      }
    } catch {
      texts = [];
    }

    return texts;
  }


  //#endregion TooltipForIcons

  //#region TooltipForErrorAndValidationIcons

  /**
   * Add `Tooltip` on `error` and `warning` icons
   * @param errorAndWarningElements Target error and warning icons
   * @param type tooltip icon type
   * @param max Maximum number of items in tooltip ( Optional )
   */
  initTooltipForErrorAndValidationIcons(errorAndWarningElements, type: string, max?: number): any {
    this.type = type;

    for (let icon of errorAndWarningElements) {
      // Mouseenter event for tooltip
      icon.addEventListener('mouseover', event => {
        this.showTooltipTextForErrorAndValidationIcon(event, max);
      });

      // Mouseleave event for tooltip
      icon.addEventListener('mouseleave', event => {
        this.removeTooltipText(event);
      });
    }
  }

  private showTooltipTextForErrorAndValidationIcon(event, max: number): any {
    let tooltip: HTMLElement = this.elRef.nativeElement.querySelector('.tooltip');
    let errorsOrWarnings: string = this.getAttribute(event.target, 'data-source');
    let level: CriticalItemLevel = this.getAttribute(event.target, 'data-source-level');
    let hoverNodeId: string = this.getAttribute(event.target,'data-tooltip-source');

    if (hoverNodeId && errorsOrWarnings) {
      let texts = this.getTooltipTextForErrorAndValidation(hoverNodeId,errorsOrWarnings, level);

      if (texts && texts.length > 0) {
        this.addTooltipNodes(tooltip, texts, event, max);
      }
    }
  }

  private getTooltipTextForErrorAndValidation(hoverNodeId: any, errorsOrWarnings, level: CriticalItemLevel): any {
    this.schematicsSettings = this.flowchartService.getSchematicSettings();
    let texts = [];

    try {
      let nodeSettings = this.schematicsSettings[hoverNodeId];

      if (nodeSettings && nodeSettings[errorsOrWarnings] && nodeSettings[errorsOrWarnings].length > 0) {
        let items: CriticalItem[] = nodeSettings[errorsOrWarnings];
        items = items.filter(e => e.level == level);

        for (let item of items) {
          texts.push(item.infoText);
        }
      }
    } catch {}

    return texts;
  }

  //#endregion TooltipForErrorAndValidationIcons

  /**
   * Get value of html attribute ( also parent html attribute )
   *
   * Example - `<span data-source='test'>TEXT</span>`
   * By using this function, pass `data-source` as `attr`,
   * then it will return `test`
   * @param event Target HTMLElement
   * @param attr HTML Attribute
   */
  private getAttribute(event, attr: string) {
    if (event.hasAttribute(attr)) {
      return event.getAttribute(attr);
    }
    return this.getAttribute(event.parentNode, attr);
  }

}
