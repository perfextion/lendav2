import { Component, OnChanges, Input } from '@angular/core';

import { ToleranceLevel } from '@lenda/Workers/calculations/loantolerancecalculatonworker.service';

@Component({
  selector: 'app-icon-loan-status',
  templateUrl: './icon-loan-status.component.html',
  styleUrls: ['./icon-loan-status.component.scss']
})
export class IconLoanStatusComponent implements OnChanges {
  @Input() loanStatus: string;
  private _tolerance: ToleranceLevel;
  public toleranceClass;

  @Input('cssClass') class: string = '';

  constructor() {}

  @Input() set tolerance(val) {
    this._tolerance = val ? val: ToleranceLevel.NoTolerence;

    if (this._tolerance == ToleranceLevel.NoTolerence) {
      this.toleranceClass = 'success';
    } else if (this._tolerance === ToleranceLevel.WithinTolerance) {
      this.toleranceClass = 'warning';
    } else {
      this.toleranceClass = 'error';
    }
  };

  ngOnChanges() {
    if(this.loanStatus && this.loanStatus.includes('_')) {
      this.loanStatus = this.loanStatus.substring(0, this.loanStatus.indexOf('_'))
    }
  }
}
