import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from '@angular/core';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-import-file',
  templateUrl: './import-file.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ImportFileComponent implements OnInit {
  @Input() columnDefs: any[];
  @Input() isDisabled: boolean;

  @Output() importExcel = new EventEmitter();

  @ViewChild('fileToUpload') fileToUpload: ElementRef<HTMLInputElement>;

  constructor() {}

  ngOnInit() {}

  importFile($event) {
    let files = $event.target.files;

    let file = files[0];

    let fileReader = new FileReader();

    fileReader.onload = e => {
      let arrayBuffer = fileReader.result as any;
      let workbook = this.convertDataToWorkbook(arrayBuffer);
      let first_sheet_name = workbook.SheetNames[0];
      let worksheet = workbook.Sheets[first_sheet_name];

      let columns = this.columnDefs.map(a => a.field).filter(a => a != 'value');

      let data = XLSX.utils.sheet_to_json(worksheet, { header: columns });

      this._clearfileToUpload();

      if (data && data.length > 0) {
        data.shift();
        this.importExcel.emit(data);
      }
    };

    fileReader.readAsArrayBuffer(file);
  }

  private _clearfileToUpload() {
    this.fileToUpload.nativeElement.value = '';
  }

  convertDataToWorkbook(data) {
    /* convert data to binary string */
    data = new Uint8Array(data);
    let arr = new Array();

    for (let i = 0; i !== data.length; ++i) {
      arr[i] = String.fromCharCode(data[i]);
    }

    let bstr = arr.join('');

    return XLSX.read(bstr, { type: 'binary' });
  }

  hasSameColumns(columns: Array<string>, row: object) {
    let headers = Object.keys(row);
    return this.arraysEqual(columns, headers);
  }

  arraysEqual(a: Array<string>, b: Array<string>) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;

    for (let i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) return false;
    }
    return true;
  }
}
