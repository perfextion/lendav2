import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { SvgTooltipService } from '../svg-tooltip/svg-tooltip.service';
import * as d3 from 'd3';

@Component({
  selector: 'app-rangebar',
  templateUrl: './rangebar.component.html',
  styleUrls: ['./rangebar.component.scss'],
  providers: [SvgTooltipService]
})
export class RangebarComponent implements OnInit {
  @Input() isGridlinesEnabled: boolean = true;
  @Input() strong;
  @Input() stable;
  @Input() weak;
  @Input() value = 0;
  @Input() isRangeBarVisible: boolean = false;
  private scaleStart: number;
  private scaleEnd: number;

  public valuePercent;
  @Input() showBenchmarkValues = true;
  constructor(
    public svgTooltipService: SvgTooltipService,
    public elRef: ElementRef
  ) { }

  ngOnInit() {
    this.calculateScale();
    this.calculateValuePercentage();
  }

  /**
   * Calculate scale start and end based on strong and stable values received
   */
  calculateScale() {
    let ratio = this.strong - this.stable;
    this.scaleStart = this.strong + ratio;
    this.scaleEnd = this.stable - ratio;
  }

  ngAfterViewInit() {
    this.svgTooltipService.initTooltip(this.elRef.nativeElement.querySelectorAll('.main-value'), 'range');
  }

  calculateValuePercentage() {
    // Calculate the value
    let ratio = this.strong - this.stable;
    if (ratio < 0) {
      this.valuePercent = (this.value - this.scaleStart) * 100 / Math.abs(this.scaleEnd - this.scaleStart);
    } else  {
      this.valuePercent = (this.scaleStart - this.value) * 100 / Math.abs(this.scaleEnd - this.scaleStart);
    }

    // If it is beyond threshold then set the value percent
    if (this.valuePercent < 0 || isNaN(this.valuePercent)) {
      this.valuePercent = 0;
    } else if (this.valuePercent > 100) {
      this.valuePercent = 100;
    }

    // Add % to number value
    this.valuePercent = `${this.valuePercent}%`;
  };
}
