import {
  Component,
  OnInit,
  Input,
  ViewChild,
  Output,
  EventEmitter
} from '@angular/core';
import { MatMenuTrigger } from '@angular/material';

import { RefDataModel } from '@lenda/models/ref-data-model';

@Component({
  selector: 'app-icon-pending-action-status',
  templateUrl: './icon-pending-action-status.component.html',
  styleUrls: ['./icon-pending-action-status.component.scss']
})
export class IconPendingActionStatusComponent implements OnInit {
  @Input() status: number = 1;
  @Input() Reminder_Ind: number = 0;
  @Input() refData: RefDataModel;

  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  @Output() reminderChange = new EventEmitter<any>();

  model: boolean;

  constructor() {}

  ngOnInit() {
    this.model = this.Reminder_Ind == 1;
  }

  onClick($event: Event) {
    $event.stopPropagation();
    $event.preventDefault();
    this.trigger.closeMenu();
  }

  onRightClick($event: Event) {
    $event.stopPropagation();
    $event.preventDefault();
    this.trigger.openMenu();
  }

  onSliderChange(event) {
    try {
      this.Reminder_Ind = event.checked ? 1 : 0;
      let pa = this.refData.RefPendingActions.find(a => a.Ref_PA_ID == this.status);
      if(pa) {
        this.reminderChange.emit({
          Reminder_Ind: this.Reminder_Ind,
          PA_Group_Code: pa.PA_Group
        });
      }
    } catch {

    }
  }
}
