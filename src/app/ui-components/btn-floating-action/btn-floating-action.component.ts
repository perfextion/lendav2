import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';

import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';

import { loan_model } from '@lenda/models/loanmodel';
import { environment as env } from '@env/environment';

import { PublishService, Sync } from '@lenda/services/publish.service';
import { DataService } from '@lenda/services/data.service';
import { ValidationErrorsCountService } from '@lenda/Workers/calculations/validationerrorscount.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';

@Component({
  selector: 'app-btn-floating-action',
  templateUrl: './btn-floating-action.component.html',
  styleUrls: ['./btn-floating-action.component.scss']
})
export class BtnFloatingActionComponent implements OnInit, OnDestroy {
  public isPublishEnabled: boolean = false;

  private localloanobject: loan_model = new loan_model();
  private subscription: ISubscription;
  private listenToSyncRequiredSubscription: ISubscription;

  @Input() currentPageName: string;
  @Output() publishClicked = new EventEmitter();

  constructor(
    private publishService: PublishService,
    private dataService: DataService,
    public localStorageService: LocalStorageService,
    private validationcount: ValidationErrorsCountService,
    private alertify: AlertifyService
  ) {
    this.localloanobject = this.localStorageService.retrieve(env.loankey);
  }

  ngOnInit() {
    // Checks if current page is dirty for page header publish button
    this.listenToSyncRequiredSubscription = this.publishService
      .listenToSyncRequired()
      .subscribe(syncItems => {
        this.isPublishEnabled = this._isPageSyncRequired(syncItems);
      });

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localloanobject = res;
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.listenToSyncRequiredSubscription) {
      this.listenToSyncRequiredSubscription.unsubscribe();
    }
  }

  /**
   * Publish button click
   */
  onClick() {
    // if(this.checkLoanSync()) {
    this.checkUpdateBudgetInd();
    // }
  }

  // HELPERS
  // ===

  /**
   * Checks if currently active page requires synching
   * @param syncItems Array of sync pages
   */
  private _isPageSyncRequired(syncItems: Sync[]) {
    return syncItems.some(item => item.page === this.currentPageName);
  }

  /**
   * Check if no Validation Errors for the said Page.
   */
  checkLoanSync() {
    let hasValidations = this.validationcount.hasLevel2Validations();

    if (hasValidations) {
      this.alertify.alert(
        'Alert',
        'Please resolve all critical validations before publishing.'
      );
      return false;
    } else {
      return true;
    }
  }

  checkUpdateBudgetInd() {
    if (this.localloanobject.Update_Budget_Ind) {
      this.alertify
        .confirm(
          'Confirm',
          'Warning: You have changed default data. Do you want to update your defaults?'
        )
        .subscribe(res => {
          if (res) {
            this.localloanobject.Update_Budget_Ind = 1;
          } else {
            this.localloanobject.Update_Budget_Ind = 0;
          }
          this.publishClicked.emit();
        });
    } else {
      this.publishClicked.emit();
    }
  }
}
