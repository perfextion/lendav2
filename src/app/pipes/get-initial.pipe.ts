import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
  name: 'getinitial'
})
export class GetInitialPipe implements PipeTransform {
  transform(value: string, args: any[]): string | boolean {
    if (value === null) {
      return false;
    }
    return value.substring(0, 1);
  }
}
