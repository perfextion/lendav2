import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'money'
})
export class MoneyPipe implements PipeTransform {
  transform(input = 0, args?: any): any {
    let exp,
      rounded,
      suffixes = ['k', 'M', 'G', 'T', 'P', 'E'];

    if (Number.isNaN(input)) {
      return null;
    }

    if (input < 1000) {
      return `$${Number(input).toLocaleString('en-US')}`;
    }

    exp = Math.floor(Math.log(input) / Math.log(1000));

    return "$"+ Number((input / Math.pow(1000, exp)).toFixed(args)).toLocaleString() + suffixes[exp - 1];
  }
}
