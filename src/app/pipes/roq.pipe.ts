import { Pipe, PipeTransform } from '@angular/core';
import { Risk_Other_Queue } from '@lenda/models/loanmodel';

@Pipe({
  name: 'roqFilter'
})
export class RiskOtherQueueFilterPipe implements PipeTransform {
  transform(
    value: Array<Risk_Other_Queue>,
    key: string
  ): Array<Risk_Other_Queue> {
    try {
      if(key) {
        return value.filter(a => a.Field_ID == key);
      }
      return value;
    } catch {
      return [];
    }
  }
}
