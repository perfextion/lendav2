import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'usdCurrency'
})
export class UsdCurrencyPipe implements PipeTransform {
  transform(value: any, digitInfo = '1.0-0'): any {
    let cp = new CurrencyPipe('en-US');
    return cp.transform(value, 'USD', 'symbol', digitInfo);
  }
}
