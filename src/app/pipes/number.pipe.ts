import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({
  name: 'lendaNumber'
})

export class LendaNumberPipe implements PipeTransform {
  transform(value: any, digitInfo = '1.0-0'): any {
    let np = new DecimalPipe('en-US');
    return np.transform(value, digitInfo);
  }
}
