import { Pipe, PipeTransform } from '@angular/core';
import { Master_Borrower } from '@lenda/models/master-borrower.model';

@Pipe({
  name: 'borrowerName'
})
export class BorrowerNamePipe implements PipeTransform {
  transform(borrower: Master_Borrower): string {
    if(borrower) {
      let name = borrower.Borrower_Last_Name || '';
      if(name.trim()) {
        name += ', ';
      }

      name += borrower.Borrower_First_Name;

      name += ` (${borrower.Borrower_SSN_Hash})`;
      return name;
    }
    return '';
  }
}
