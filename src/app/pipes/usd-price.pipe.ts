import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'usdPrice'
})
export class UsdPricePipe implements PipeTransform {
  transform(value: any, digitInfo = '1.4-4'): any {
    let cp = new CurrencyPipe('en-US');
    return cp.transform(value, 'USD', 'symbol', digitInfo);
  }
}
