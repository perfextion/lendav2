import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'favouriteLoan'
})
export class FavouriteLoanPipe implements PipeTransform {
  transform(loan: FavouriteLoan): string {
    if (loan) {
      let loanId = loan.loanFullID.replace('/', '-');

      return loanId + ' : '  + loan.borrowerName + ' ' + loan.loanCropYear + ' ' + loan.loanTypeAndDistCode;
    }
    return '';
  }
}

export class FavouriteLoan {
  loanFullID: string;
  loanCropYear: number;
  borrowerName: string;
  loanTypeAndDistCode: string;
}
