import { Injectable, Pipe, PipeTransform } from '@angular/core';

import { CommitteeStatusMock } from '@lenda/models/committee-status/mock-committee-status';

@Pipe({
    // tslint:disable-next-line:pipe-naming
    name: "committestatusicon"
})

@Injectable()
export class CommitteStatusIconPipe implements PipeTransform {
    transform(value: number, committeStatusParentIconId: number): string {
        let committeeStatus = CommitteeStatusMock.filter(s => s.id == committeStatusParentIconId);
        if (committeeStatus.length > 0) {
            let innerCommitteeStatus = committeeStatus[0].innerStatus.filter(i => i.id == value);
            if (innerCommitteeStatus.length > 0) {
                return innerCommitteeStatus[0].icon;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }
}
