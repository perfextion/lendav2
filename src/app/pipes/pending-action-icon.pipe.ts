import { Pipe, PipeTransform } from '@angular/core';
import { loan_model } from '@lenda/models/loanmodel';
import {
  PA_Icon_Color,
  PA_Icon_Color_Loan,
  PA_Icon_Color_With_Reminder,
  PA_Icon_Color_Loan_With_Reminder
} from '@lenda/models/pending-action/add-pending-action.model';

@Pipe({
  name: 'pendingActionIcon',
  pure: false
})
export class PendingActionIconPipe implements PipeTransform {
  transform(
    currentLoan: loan_model,
    PA_Code: string,
    loggedInUserId: number
  ): string {
    try {
      let PA = currentLoan.User_Pending_Actions.filter(
        pa =>
          pa.PA_Group_Code == PA_Code &&
          pa.User_ID == loggedInUserId &&
          pa.ActionStatus != 3 &&
          pa.Status == 1
      );

      if (PA.length > 0) {
        if (PA.some(a => a.Reminder_Ind == 1)) {
          return PA_Icon_Color_With_Reminder[PA[0].PA_Code];
        } else {
          return PA_Icon_Color[PA[0].PA_Code];
        }
      } else {
        PA = currentLoan.User_Pending_Actions.filter(
          pa =>
            pa.PA_Group_Code == PA_Code &&
            pa.ActionStatus != 3 &&
            pa.Status == 1
        );

        if (PA.length > 0) {
          if (PA.some(a => a.Reminder_Ind == 1)) {
            return PA_Icon_Color_Loan_With_Reminder[PA[0].PA_Code];
          } else {
            return PA_Icon_Color_Loan[PA[0].PA_Code];
          }
        } else {
          return 'flaticon-icon-disabled';
        }
      }
    } catch {
      return 'flaticon-icon-disabled';
    }
  }
}
