/* #region  Imports are Done in this Region */
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import * as XLSX from 'xlsx';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { Loansettings, Loan_Key_Visibilty } from '@lenda/models/loansettings';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';
import { loan_model, LoanStatus } from '@lenda/models/loanmodel';
import { APH_Units, Loan_Crop_Unit } from '@lenda/models/cropmodel';
import { Page } from '@lenda/models/page.enum';
import { percentageFormatter, CropidtonameFormatterRef, numberFormatter } from '@lenda/aggridformatters/valueformatters';
import { isgrideditable, headerclassmaker, CellType, cellclassmaker, cellclassmakerverify, calculatecolumnwidths } from '@lenda/aggriddefinations/aggridoptions';
import { TableId, SourceComponent, VerificationStages, errormodel } from '@lenda/models/commonmodels';
import { BooleanEditor } from '@lenda/aggridfilters/booleanaggrid.';
import { extractStateValues, getfilteredcountiesAPH, lookupCountyRefValue, lookupStateValueFromPassedRefObject } from '@lenda/Workers/utility/aggrid/stateandcountyboxes';
import { lookupUOM, lookupCropTypeWithRefData, lookupCropPracticeTypeWithRefData } from '@lenda/Workers/utility/aggrid/cropboxes';
import { RatedValues } from '@lenda/components/farm/farm.model';

import { verifyPrimaryHeaderAPH } from '@lenda/aggridcolumns/headerComponents/verifyprimaryHeader.aph';
import { CropAutoCompleteEditor } from '@lenda/aggridcolumns/crop-auto-complete-editor/crop-auto-complete-editor.component';
import { CheckboxCellRenderer } from '@lenda/aggridcolumns/checkbox-cell-editor/checkbox-cell-rendere';
import { DeleteButtonRenderer } from '@lenda/aggridcolumns/deletebuttoncolumn';
import { numberValueSetter, getNumericCellEditor, numberValueSetterwithdash } from '@lenda/Workers/utility/aggrid/numericboxes';
import { SelectEditor } from '@lenda/aggridfilters/selectbox';

import { InsuranceService } from '../insurance.service';
import { ToasterService } from '@lenda/services/toaster.service';
import { ExportExcelService } from '@lenda/services/export-excel.service';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';
import { MasterService } from '@lenda/master/master.service';
import { APHValidation } from '@lenda/Workers/utility/validation-functions';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { InsuranceapiService } from '@lenda/services/insurance/insuranceapi.service';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { PublishService } from '@lenda/services/publish.service';
import { DataService } from '@lenda/services/data.service';

declare function setmodifiedall(array, keyword): any;
declare function setverificationerrorall(array, keyword): any;
declare function setmodifiedsingle(obj, keyword): any;
/* #endregion */

/* #region  Component Decleration Here */
@Component({
  selector: 'app-aph',
  templateUrl: './aph.component.html',
  styleUrls: ['./aph.component.scss'],
  encapsulation: ViewEncapsulation.None
})
/* #endregion */
export class AphComponent implements OnInit, OnDestroy {
  /* #region  Variables Declartions */
  public btnVaph = {
    "margin-left": '500px',
    "margin-top": '0px'
  };
  public columnDefs = [];
  public rowData = [];
  private localloanobject: loan_model;
  public components;
  public refdata: any = {};
  public verificationdata = [];
  public gridApi: any;
  public columnApi: any;
  public isAdding: boolean;
  public context;
  public style = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };
  public columnsDisplaySettings: ColumnsDisplaySettings = new ColumnsDisplaySettings();
  // CAN BE USED LATER SO HERE
  //frameworkComponents: { booleaneditor: typeof BooleanEditor; selectEditor: typeof SelectEditor; deletecolumn: typeof DeleteButtonRenderer,  autocompleteEditor:CropAutoCompleteEditor };
  private _enableCopy: boolean;
  public frameworkComponents: any;
  private LoankeySettings: Loan_Key_Visibilty;
  public verifyButtonStage = VerificationStages.Verify;
  public defaultColDef = { sortable: true };
  private startingconstleft: number = 0;
  /* #endregion */

  private errorSubscription: ISubscription;
  private currenterrors: Array<errormodel>;
  private preferencesChangeSub: ISubscription;

  constructor(
    private localstorageservice: LocalStorageService,
    private loanserviceworker: LoancalculationWorker,
    public loanapi: LoanApiService,
    private publishService: PublishService,
    private dataService: DataService,
    private apiservice: InsuranceapiService,
    private dtss: DatetTimeStampService,
    private settingsService: SettingsService,
    public alertify: AlertifyService,
    private insService: InsuranceService,
    private toastr: ToasterService,
    public excel: ExportExcelService,
    public masterSvc: MasterService,
    private validationservice: ValidationService
  ) {

    this.components = { numericCellEditor: getNumericCellEditor() };

    this.frameworkComponents = {
      booleaneditor: BooleanEditor,
      selectEditor: SelectEditor,
      deletecolumn: DeleteButtonRenderer,
      autocompleteEditor: CropAutoCompleteEditor,
      checkboxCellRenderer: CheckboxCellRenderer
    };

    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
    this.context = { componentParent: this };
  }

  public gridOptions = {
    getRowNodeId: data => `${TableId.APH}${data.Loan_CU_ID}`
  };


  ngOnInit() {

    /* #region  BETA TESTING CODE NOT STABLE  */
    // this.subscription = this.dataService.getLoanObject().subscribe(res => {
    //   if (res != null) {
    //     console.time('APH observer');
    //     this.localloanobject = res;

    //     if(this.localloanobject.srccomponentedit == SourceComponent.APH) {
    //       this.rowData[res.lasteditrowindex] = this.localloanobject.AphUnits.filter(p => p.ActionStatus != 3)[res.lasteditrowindex];
    //       this.localloanobject.srccomponentedit = undefined;
    //       this.localloanobject.lasteditrowindex = undefined;
    //     } else if (res.AphUnits || this.localloanobject.srccomponentedit == SourceComponent.APH_Excel_Upload) {
    //       this.rowdataset();
    //     } else {
    //       this.rowData = [];
    //     }
    //   }
    //   this.setModifiedAPHTable();
    //   this.setVerificationError();
    //   console.timeEnd('APH observer');
    // });
    /* #endregion */

    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);
    this.rowDataset();

    setTimeout(() => {
      this.setModifiedAPHTable();
      this.setValidationErrorForAPH();
    }, 10);

    setTimeout(() => {
      this.getCurrentErrors();
    }, 1000);

    this.getUserPreferences();

    this.errorSubscription = this.localstorageservice.observe(environment.errorbase).subscribe(() => {
      setTimeout(() => {
        this.getCurrentErrors();
      }, 5);
    });
  }

  private getCurrentErrors() {
    if (environment.isDebugModeActive) console.time('Highlight Validation - APH');

    this.currenterrors = (this.localstorageservice.retrieve(
      environment.errorbase
    ) as Array<errormodel>).filter(p => p.chevron == 'Aph');

    this.validationservice.highlighErrorCells(this.currenterrors, TableId.APH);

    if (environment.isDebugModeActive) console.timeEnd('Highlight Validation - APH');
  }

  private rowDataset() {

    if (this.localloanobject && this.localloanobject.LoanCropUnits) {
      this.rowData = [];
      this.localloanobject.LoanCropUnits.filter(p => p.ActionStatus != 3).forEach(element => {
        let _rowdataobject: any = {};
        _rowdataobject.Added_Land = element.Added_Land == undefined ? element.Added_Land : 0;
        //Find State and County
        let matchedFarm = this.localloanobject.Farms.find(f => f.Farm_ID == element.Farm_ID);
        if (matchedFarm) {
          _rowdataobject.State_ID = matchedFarm.Farm_State_ID;
          _rowdataobject.County_ID = matchedFarm.Farm_County_ID;
          _rowdataobject.FC_StateName = lookupStateValueFromPassedRefObject(_rowdataobject.State_ID, this.refdata);
          _rowdataobject.FC_CountyName = lookupCountyRefValue(_rowdataobject.County_ID, this.refdata);
        }
        //
        _rowdataobject.Loan_CU_ID = element.Loan_CU_ID;
        _rowdataobject.Crop_Code = element.Crop_Code;
        _rowdataobject.Croptype = element.Crop_Type_Code;
        _rowdataobject.Crop_Practice_Type_Code = element.Crop_Practice_Type_Code;
        _rowdataobject.Crop_Practice_Type = lookupCropPracticeTypeWithRefData(element.Crop_Practice_ID, this.refdata);
        _rowdataobject.Prod_Perc = element.FC_ProdPerc;
        _rowdataobject.LandLord = element.FC_Landlord;
        _rowdataobject.FSN = element.FC_FSN;
        _rowdataobject.Section = element.FC_Section;
        _rowdataobject.Rating = element.FC_Rating;
        _rowdataobject.Rate_Yield = element.Rate_Yield;
        _rowdataobject.Ins_APH = element.Ins_APH;
        _rowdataobject.CU_Acres = element.CU_Acres;
        _rowdataobject.Fc_Custom = false;
        _rowdataobject.UoM = lookupUOM(element.Crop_Code, this.refdata);
        _rowdataobject.Added_Land = element.Added_Land;
        _rowdataobject.Verf_Ins_APH = element.Verf_Ins_APH;
        if (element.Verf_Ins_APH_Status == true) {
          _rowdataobject.Verf_Ins_APH = element.Ins_APH;
        }
        _rowdataobject.Verf_Ins_APH_Status = element.Verf_Ins_APH_Status;

        this.rowData.push(_rowdataobject);
      });
      this.rowData = _.orderBy(this.rowData, ["FC_StateName", "FC_CountyName", "FSN", "Crop_Code", "Croptype", "Crop_Practice_Type"]);
      if (this.localloanobject.AphUnits != undefined) {
        let _aphdata = this.localloanobject.AphUnits.filter(p => p.ActionStatus != 3);
        _aphdata.forEach(iteration => {
          if (iteration.Verf_Ins_APH_Status == true) {
            iteration.Verf_Ins_APH = iteration.Ins_APH;
          }
          iteration.Fc_Custom = true;
          iteration.ActionStatus = iteration.ActionStatus != null ? iteration.ActionStatus : 3;
        });
        this.rowData = this.rowData.concat(_aphdata);
      }
    }
  }


  Get_Crops_Practices_List() {
    let selection = [];

    let existingCrops = this.localloanobject.CropYield.filter(p => p.ActionStatus != 3);

    this.refdata.CropList.forEach(iteration => {
      if (!existingCrops.some(_crop => _crop.Crop_ID == iteration.Crop_And_Practice_ID)) {
        selection.push({
          Crop_Code: iteration.Crop_Code,
          Crop_Name: iteration.Crop_Name,
          Crop_Type_Code: iteration.Crop_Type_Code,
          Irr_Prac_Code: iteration.Irr_Prac_Code,
          Crop_Prac_Code: iteration.Crop_Prac_Code,
          Crop_And_Practice_ID: iteration.Crop_And_Practice_ID,
          key: iteration.Crop_And_Practice_ID,
          value: `${iteration.Crop_Name} | ${iteration.Crop_Type_Code} | ${iteration.Irr_Prac_Code}`,
          InsUOM: lookupUOM(iteration.Crop_Code, this.refdata)
        });
      }
    });

    selection = _.uniqBy(selection, 'key');
    selection = _.sortBy(selection, ['Crop_Name', 'Crop_Type_Code', 'Irr_Prac_Code', 'Crop_Prac_Code']);

    return selection;
  }

  sort(_$event) {
    // Write code if sort needed
  }

  declareColumns() {
    this.columnDefs = [
      {
        headerName: 'State', field: 'FC_StateName',
        cellClass: function (params) {
          if (params.data.Fc_Custom && params.data.ActionStatus != -1) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return cellclassmaker(CellType.Text, true, '', Loan_Status);
          }
        }, cellRenderer: function (params) {
          if (params.data.Fc_Custom) {
            return params.value + '<i class="tiny material-icons icon-green">add</i>';
          } else {
            return params.value;
          }
        },
        cellEditor: 'selectEditor',
        cellEditorParams: {
          values: extractStateValues(this.refdata.StateList)
        },
        // valueFormatter: StateidtonameFormatter,
        valueSetter: function (params) {
          // tslint:disable-next-line:radix
          params.data.State_ID = parseInt(params.newValue);
          try {
            params.data.FC_StateName =
              params.context.componentParent.refdata.StateList.find(p => p.State_ID == parseInt(params.newValue, 10)).State_Abbrev;
          } catch {
            params.data.FC_StateName = null;
          }

        },
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          return isgrideditable((params.data.Fc_Custom && params.data.ActionStatus != -1), Loan_Status);
        },
        width: 90
      },
      {
        headerName: 'County', field: 'FC_CountyName',
        cellClass: function (params) {
          if (params.data.Fc_Custom && params.data.ActionStatus != -1) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return cellclassmaker(CellType.Text, true, '', Loan_Status);
          }
        },
        cellEditor: 'selectEditor',
        cellEditorParams: getfilteredcountiesAPH,
        //   valueFormatter: function (params) {

        //     return CountyidtonameFormatter(params, params.context.componentParent.refdata);

        // },
        valueSetter: function (params) {
          // tslint:disable-next-line: radix
          params.data.County_ID = parseInt(params.newValue);
          try {
            params.data.FC_CountyName =
              params.context.componentParent.refdata.CountyList.find(p => p.County_ID == parseInt(params.newValue, 10)).County_Name;
          } catch {
            params.data.FC_CountyName = null;
          }
        },
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          return isgrideditable((params.data.Fc_Custom && params.data.ActionStatus != -1), Loan_Status);
        },
        width: 90,
        minWidth: 90
      },
      {
        headerName: 'FSN', field: 'FSN', resizable: true, editable: function (params) {
          return isgrideditable(params.data.Fc_Custom && params.data.ActionStatus != -1);
        },
        cellClass: function (params) {
          if (params.data.Fc_Custom && params.data.ActionStatus != -1) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return cellclassmaker(CellType.Text, true, '', Loan_Status);
          }
        },
        width: 120
      },
      {
        headerName: 'Crop',
        field: 'Crop_Code', pickfield: 'Crop_Code',
        editable: function (params) {
          return isgrideditable((params.data.Fc_Custom && params.data.ActionStatus != -1));
        }, width: 90,
        cellClass: function (params) {
          if (params.data.Fc_Custom && params.data.ActionStatus != -1) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return cellclassmaker(CellType.Text, true, '', Loan_Status);
          }
        },
        minWidth: 90,
        maxWidth: 90,
        valueSetter: function () {

        },
        cellEditor: 'autocompleteEditor',
        valueFormatter: params => {
          return CropidtonameFormatterRef(params, params.context.componentParent.refdata);
        },
      },
      {
        headerName: 'Crop Type',
        field: 'Croptype',
        tooltip: params => params.data['Croptype'],
        minWidth: 90, width: 90,
        editable: false,
        hide: !this.columnsDisplaySettings.cropType
      },
      {
        headerName: 'Irr Prac',
        field: 'Crop_Practice_Type_Code',
        tooltip: params => params.data['Crop_Practice_Type_Code'],
        width: 90,
        editable: false,
        hide: !this.columnsDisplaySettings.irrPrac
      },
      {
        headerName: 'Crop Prac',
        field: 'Crop_Practice_Type',
        tooltip: params => params.data['Crop_Practice_Type'],
        editable: false,
        width: 90,
        hide: !this.columnsDisplaySettings.cropPrac
      },
      {
        headerName: 'Prod %', field: 'Prod_Perc', editable: function (params) {
          return isgrideditable((params.data.Fc_Custom && params.data.ActionStatus != -1));
        },
        cellClass: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          if ((params.data.Fc_Custom && params.data.ActionStatus != -1)) {
            return cellclassmaker(CellType.Integer, true, '', Loan_Status);
          } else {
            return cellclassmaker(CellType.Integer, false, '', Loan_Status);
          }

        },
        valueFormatter: function (params) {
          return percentageFormatter(params, 1);
        },
        width: 90
      },
      {
        headerName: 'Landlord',
        field: 'LandLord',
        tooltip: params => params.data['LandLord'],
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          return isgrideditable(params.data.Fc_Custom, Loan_Status);
        },
        cellClass: function (params) {
          if (params.data.Fc_Custom && params.data.ActionStatus != -1) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return cellclassmaker(CellType.Text, true, '', Loan_Status);
          }
        },
        cellEditor: 'selectEditor',
        cellEditorParams: function () {
          return getLandlords();
        },
        width: 120,
        minWidth: 120
      },
      {
        headerName: 'Section',
        field: 'Section',
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          return isgrideditable(params.data.Fc_Custom && params.data.ActionStatus != -1, Loan_Status);
        },
        cellClass: function (params) {
          if (params.data.Fc_Custom && params.data.ActionStatus != -1) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return cellclassmaker(CellType.Text, true, '', Loan_Status);
          }
        },
        width: 90,
        hide: !this.columnsDisplaySettings.section
      },
      {
        headerName: 'Rated',
        field: 'Rating',
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          return isgrideditable(params.data.Fc_Custom && params.data.ActionStatus != -1, Loan_Status);
        },
        cellClass: function (params) {
          if (params.data.Fc_Custom && params.data.ActionStatus != -1) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return cellclassmaker(CellType.Text, true, '', Loan_Status);
          }
        },
        cellEditor: 'selectEditor',
        cellEditorParams: {
          values: RatedValues
        },
        width: 90,
        hide: !this.columnsDisplaySettings.rated
      },
      {
        headerName: 'Rate Yield', suppressSorting: true,
        field: 'Rate_Yield',
        cellClass: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          return cellclassmaker(CellType.Integer, true, '', Loan_Status);
        },
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          return isgrideditable(true, Loan_Status);
        },
        cellEditor: "numericCellEditor",
        valueSetter: numberValueSetter,
        width: 90,
        minWidth: 90,
        valueFormatter: function (params) {
          return Math.round(params.value);
        },
        hide: !this.columnsDisplaySettings.rateYield
      },
      {
        headerName: 'APH', field: 'Ins_APH', suppressSorting: true, headerComponentFramework: verifyPrimaryHeaderAPH, headerComponentParams: {
          menuIcon: "verified_user", isverify: true, alreadyverified: this.getverifiedstatus()
        }, editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          if (params.context.componentParent.verifyButtonStage == VerificationStages.Check) {
            return isgrideditable(false, Loan_Status);
          } else
            if (params.context.componentParent.verifyButtonStage == VerificationStages.Check2) {
              if (params.data.Verf_Ins_APH != null) {
                return isgrideditable(true, Loan_Status);
              }
            } else {
              return isgrideditable(true, Loan_Status);
            }
        }, cellEditor: "numericCellEditor",
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          if (params.context.componentParent.verifyButtonStage == VerificationStages.Check) {
            if (!params.data.Verf_Ins_APH_Status == true) {
              return cellclassmaker(CellType.Integer, false, "redactedcell", Loan_Status);
            } else {
              return cellclassmaker(CellType.Integer, false, '', Loan_Status);
            }
          } else
            if (params.context.componentParent.verifyButtonStage == VerificationStages.Check2) {
              if (params.data.Verf_Ins_APH != null) {
                return cellclassmaker(CellType.Integer, true, '', Loan_Status);
              } else {
                return cellclassmaker(CellType.Integer, false, "redactedcell", Loan_Status);
              }
            } else {
              return cellclassmaker(CellType.Integer, true, '', Loan_Status);
            }

        },
        cellEditorParams: (params) => {
          return { value: params.data.Ins_APH || 0 };
        },
        valueSetter: numberValueSetter, width: 90,
        validations: {
          aph: APHValidation
        },
        minWidth: 90, valueFormatter: function (params) {
          let editparam = params.data.Verf_Ins_APH_Status == undefined ? false : params.data.Verf_Ins_APH_Status;
          if (params.context.componentParent.verifyButtonStage == VerificationStages.Check && !editparam) {
            return "";
          } else {
            params.value = Math.round(params.value);
            return numberFormatter(params);
          }
        }

      },
      {
        headerName: 'APH (Verify)', suppressSorting: true, supressExcel: true, field: 'Verf_Ins_APH', headerComponentFramework: verifyPrimaryHeaderAPH, headerComponentParams: {
          menuIcon: "cancel", isverify: false
        },
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          let editparam = params.data.Verf_Ins_APH_Status == undefined ? false : params.data.Verf_Ins_APH_Status;
          return isgrideditable(!editparam, Loan_Status);
        },
        suppressToolPanel: true,
        cellEditor: "numericCellEditor",
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          let editparam = params.data.Verf_Ins_APH_Status == undefined ? false : params.data.Verf_Ins_APH_Status;
          return cellclassmakerverify(CellType.Integer, !editparam, Loan_Status);
        },
        cellEditorParams: (params) => {
          return { value: params.data.Verf_Ins_APH || 0 };
        },

        valueSetter: numberValueSetterwithdash, width: 120,
        minWidth: 120,
        hide: true,
        cellRenderer: function (params) {
          let editparam = params.data.Verf_Ins_APH_Status == undefined ? false : params.data.Verf_Ins_APH_Status;
          if (editparam) {
            return '<i class="tiny material-icons icon-yellow">done</i>';
          } else {
            return params.value;
          }
        },
      },
      {
        headerName: 'UoM', suppressSorting: true, field: 'UoM',
        editable: function () {
          return false; // isgrideditable(params.data.Fc_Custom && params.data.ActionStatus != -1);
        },

        width: 90,
      },
      {
        headerName: 'Added Land',
        field: 'Added_Land',
        suppressSorting: true,
        cellClass: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          return cellclassmaker(CellType.Text, true, '', Loan_Status);
        },
        cellRenderer: 'checkboxCellRenderer',
        cellRendererParams: params => {
          return {
            type: 'Added_Land',
            edit: isgrideditable(true, params.context.componentParent.Loan_Status)
          };
        },
        width: 90,
        minWidth: 90
      },
      {
        headerName: '',
        field: 'value',
        suppressSorting: true,
        supressExcel: true,
        cellRendererSelector: function (params) {
          if (params.data.Fc_Custom) {
            return {
              component: 'deletecolumn'
            };
          } else {
            return null;
          }
        },
        suppressToolPanel: true,
        minWidth: 60, width: 60, maxWidth: 60,
      },
      {
        headerName: 'Custom', suppressSorting: true,
        field: 'Fc_Custom',
        supressExcel: true,
        hide: true,
        suppressToolPanel: true,
        minWidth: 60, width: 60, maxWidth: 60,
      }
    ];
  }

  DeleteClicked(rowIndex: any, data) {

    this.alertify.deleteRecord().subscribe(_taskResponse => {
      if (_taskResponse) {

        let item = this.localloanobject.AphUnits.find(p => p.Loan_CU_ID == data.Loan_CU_ID);

        if (item.ActionStatus == 1) {
          let index = this.localloanobject.AphUnits.indexOf(item);
          this.localloanobject.AphUnits.splice(index, 1);
        } else {
          item.ActionStatus = 3;
        }

        this.rowData.splice(rowIndex, 1);
        this.gridApi.setRowData(this.rowData);

        this.localloanobject.srccomponentedit = SourceComponent.APH;
        this.dataService.setLoanObject(this.localloanobject);
        this.publishService.enableSync(Page.insurance);
      }
    });
    //this.gridApi.redrawRows();
  }

  getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;
    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe((preferences) => {
      this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
      this.hideUnhideLoanKeys();
      this.declareColumns();
    });
    this.hideUnhideLoanKeys();
    this.declareColumns();
  }

  hideUnhideLoanKeys() {
    let LoanSettings: Loansettings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
    if (LoanSettings != undefined || LoanSettings != null) {
      if (LoanSettings.Loan_key_Settings != undefined || LoanSettings.Loan_key_Settings != null) {
        this.LoankeySettings = LoanSettings.Loan_key_Settings;
      } else {
        this.LoankeySettings = new Loan_Key_Visibilty();
      }
    } else {
      this.LoankeySettings = new Loan_Key_Visibilty();
    }

    this.columnsDisplaySettings.cropPrac = this.LoankeySettings.Crop_Practice == null ? this.columnsDisplaySettings.cropPrac : this.LoankeySettings.Crop_Practice;
    this.columnsDisplaySettings.cropType = this.LoankeySettings.Crop_Type == null ? this.columnsDisplaySettings.cropType : this.LoankeySettings.Crop_Type;
    this.columnsDisplaySettings.rated = this.LoankeySettings.Rated == null ? this.columnsDisplaySettings.rated : this.LoankeySettings.Rated;
    this.columnsDisplaySettings.section = this.LoankeySettings.Section == null ? this.columnsDisplaySettings.section : this.LoankeySettings.Section;
    this.columnsDisplaySettings.rateYield = this.LoankeySettings.RateYield == null ? this.columnsDisplaySettings.rateYield : this.LoankeySettings.RateYield;
  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
    if(this.errorSubscription) {
      this.errorSubscription.unsubscribe();
    }

    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  setModifiedAPHTable() {
    setmodifiedall(
      this.localstorageservice.retrieve(environment.modifiedbase),
      TableId.APH
    );
  }

  setVerificationError() {
    setverificationerrorall(
      this.verificationdata,
      TableId.APH
    );

  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    setTimeout(() => {
      this.setModifiedAPHTable();
    }, 10);
  }

  syncenabled() {
    if (this.localloanobject && this.localloanobject.LoanCropUnits.length > 0) {
      return this.localloanobject.LoanCropUnits.filter(cropunit => cropunit.ActionStatus).length > 0;
    } else {
      return false;
    }
  }

  //addrow here
  addRownew() {
    if (!this.isLoanEditable()) {
      return;
    }

    let isRowWitoutStateCountyCrop = this.rowData.some(
      _variable => _variable.Fc_Custom && (!_variable.FC_StateName || !_variable.FC_CountyName)
    );

    if (isRowWitoutStateCountyCrop) {
      this.alertify.alert(
        'Alert',
        'Warning: You already have a blank APH row without State/County'
      );
    } else {
      let _newItem = new APH_Units();

      this.isAdding = true;
      this._enableCopy = true;
      _newItem.Fc_Custom = true;
      _newItem.ActionStatus = 1;
      _newItem.Loan_CU_ID = getRandomInt(Math.pow(10, 5), Math.pow(10, 7));

      this.gridApi.updateRowData({ add: [_newItem] });
      this.rowData.push(_newItem);

      if (this.localloanobject.AphUnits == undefined) {
        this.localloanobject.AphUnits = new Array<APH_Units>();
      }

      this.localloanobject.AphUnits.push(_newItem);
      this.dataService.setLoanObject(this.localloanobject);
      this.setValidationErrorForAPH();
    }
  }

  rowvaluechanged(value: any) {

    this.excel.removeExportToExcelTime('aph');

    // if(value && value.oldValue == value.value) return;
    if (!value && !value.column && !value.column.colId) { return; }

    //---------Modified yellow values-------------

    //MODIFIED YELLOW VALUES
    let modifiedvalues = this.localstorageservice.retrieve(
      environment.modifiedbase
    ) as Array<String>;

    if (
      !modifiedvalues.includes(
        TableId.APH + value.data.Loan_CU_ID + '_' + value.colDef.field
      )
    ) {
      modifiedvalues.push(
        TableId.APH + value.data.Loan_CU_ID + '_' + value.colDef.field
      );

      this.localstorageservice.store(environment.modifiedbase, modifiedvalues);

      setmodifiedsingle(
        TableId.APH + value.data.Loan_CU_ID + '_' + value.colDef.field,
        TableId.APH
      );
    }
    //MODIFIED YELLOW VALUES
    //--------------------------------------------

    let edittedCUIndex = -1;
    if (!value.data.Fc_Custom) {
      let _currentrow = value.data;

      if (_currentrow && _currentrow.Loan_CU_ID) {
        edittedCUIndex = this.localloanobject.LoanCropUnits.findIndex(
          cu => cu.Loan_CU_ID == _currentrow.Loan_CU_ID
        );
        let _editedCU = this.localloanobject.LoanCropUnits[edittedCUIndex];
        if (_editedCU) {
          _editedCU.Added_Land = _currentrow.Added_Land == "1" ? true : false;
          _editedCU.Ins_APH = _currentrow.Ins_APH;
          _editedCU.Rate_Yield = _currentrow.Rate_Yield;
          _editedCU.Verf_Ins_APH = _currentrow.Verf_Ins_APH;
          _editedCU.Verf_Ins_APH_touched = _currentrow.Verf_Ins_APH_touched;
          if (_editedCU.Verf_Ins_APH_Status == true) {
            _editedCU.Verf_Ins_APH_Status = false;
            _editedCU.Verf_Ins_APH = 0;
          }
          _editedCU.ActionStatus = 2;
        }
      }
    } else {
      let rowindex = (value.rowIndex - this.localloanobject.LoanCropUnits.length);
      if (value.column.colId == "Added_Land") {
        this.localloanobject.AphUnits[rowindex][value.column.colId] = (value.value == "1" ? true : false);
      } else {
        this.localloanobject.AphUnits[rowindex][value.column.colId] = value.value;
      }
      if (value.column.colId == 'Crop_Code') {
        this.localloanobject.AphUnits[rowindex].Crop_Practice_Type_Code = value.data.Crop_Practice;
      }
      if (this.localloanobject.AphUnits[rowindex].ActionStatus != 1) {
        this.localloanobject.AphUnits[rowindex].ActionStatus = 2;
      }
    }

    this.localloanobject.srccomponentedit = SourceComponent.APH;
    this.localloanobject.lasteditrowindex = edittedCUIndex;
    this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
    this.publishService.enableSync(Page.insurance);
    this.rowDataset();
    this.gridApi.refreshCells();
    this.gridApi.redrawRows();

    this.setValidationErrorForAPH();

    setTimeout(() => {
      this.getCurrentErrors();
    }, 1000);
  }

  onGridReady(params) {
    if (environment.isDebugModeActive) { console.time('APH Grid'); }
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    //these two methods remove side option toolbar , sets column , removes header menu
    //setgriddefaults(this.gridApi, this.columnApi);
    //params.api.sizeColumnsToFit();
    //this.gridApi.refreshCells();
    ///////////////////////////////////////////////////////////////////////////////////

    let width = this.getColumnleftmargin("Ins_APH") + 10 + "px";
    this.startingconstleft = this.getColumnleftmargin("Ins_APH") + 10;
    this.btnVaph["margin-left"] = width;
    setTimeout(() => {
      // this.sortBySetter();
    }, 1000);
    this.adjustToFitAgGrid();

    if (environment.isDebugModeActive) { console.timeEnd('APH Grid'); }
  }

  isLoanEditable() {
    return !this.localloanobject || this.localloanobject.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  //An Array of properties that Represent Keys in the same table
  adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  //public Loankeys = ["Croptype", "Crop_Practice_Type_Code",'Crop_Practice_Type_Code1', "Section", "Rating",'Rate_Yield'];
  displayColumnsChanged($event) {

    if (this.columnApi != undefined && $event != undefined) {

      //first One to save to Loan Settings
      let LoanSettings: Loansettings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
      if (LoanSettings == undefined || LoanSettings == null) {
        LoanSettings = new Loansettings();
      }
      if (LoanSettings.Loan_key_Settings == undefined || LoanSettings.Loan_key_Settings == null) {
        LoanSettings.Loan_key_Settings = new Loan_Key_Visibilty();
      }
      //Since we dont have column thats changed so lets see manually
      // this.Loankeys.forEach(key => {
      let mainkey = "";
      switch ($event.column.colDef.headerName) {
        case "Rating":
          mainkey = "Rated";
          break;
        case "Section":
          mainkey = "Section";
          break;
        case "Crop Prac":
          mainkey = "Crop_Practice";
          break;
        case "Irr Prac":
          mainkey = "Irr_Practice";
          break;
        case "Crop Type":
          mainkey = "Crop_Type";
          break;
        case "Rate Yield":
          mainkey = "RateYield";
          break;
      }

      let column = $event.column;
      LoanSettings.Loan_key_Settings[mainkey] = column.visible;
      this.localloanobject.LoanMaster.Loan_Settings = JSON.stringify(LoanSettings);
      //window.localStorage.setItem("ng2-webstorage|currentselectedloan", JSON.stringify(this.localloanobject));
      this.dataService.setLoanObjectwithoutsubscription(this.localloanobject);

    }

    //Second Execution
    this.adjustToFitAgGrid();
  }

  getColumnleftmargin(column: string) {
    let columns = this.columnApi.getAllDisplayedVirtualColumns();

    let width = 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].colId != column) {
        width = width + columns[i].actualWidth;
      } else {
        break;
      }
    }
    return width;
  }

  verifyClicked() {

    if (this.verifyButtonStage == VerificationStages.Verify) {
      let columns = this.columnApi.getAllColumns().filter(p => p.colId.includes("Verf_"));
      columns.forEach(element => {
        this.columnApi.setColumnVisible(element.colId, true);
      });
      this.verifyButtonStage = VerificationStages.Check;
    } else
      if (this.verifyButtonStage == VerificationStages.Check) {
        if (this.verifyColumnvalues()) {

          this.verifyButtonStage = VerificationStages.Check2;
        }
      } else {
        if (this.verifyColumnvalues()) {
          this.verifyButtonStage = VerificationStages.Check2;
        }
      }
    this.gridApi.redrawRows();
    this.setModifiedAPHTable();
    this.setVerificationError();
  }

  bodyScroll(event) {
    if (this.columnApi != null) {

      let width = Number(this.btnVaph["margin-left"].replace('px', ''));
      width = this.startingconstleft - event.left;
      this.btnVaph["margin-left"] = width + "px";
    }
  }
  verifyColumnvalues() {
    this.verificationdata = [];
    let haserror = false;
    this.rowData.filter(p => p.Verf_Ins_APH_touched = true && p.Verf_Ins_APH != null).forEach(element => {
      if (element.Ins_APH != element.Verf_Ins_APH) {
        haserror = true;
        this.verificationdata.push(TableId.APH + element.Loan_CU_ID + "_" + "Verf_Ins_APH");
      } else {
        element.Verf_Ins_APH_Status = true;
        if (!element.Fc_Custom != true) {
          this.localloanobject.AphUnits.find(p => p.Loan_CU_ID == element.Loan_CU_ID).Verf_Ins_APH_Status = true;
        } else {
          this.localloanobject.LoanCropUnits.find(p => p.Loan_CU_ID == element.Loan_CU_ID).Verf_Ins_APH_Status = true;
        }
        this.updateAPHVerification(element.Loan_CU_ID, element.Fc_Custom != true);
      }
    });

    this.localstorageservice.store(environment.verificationbase, this.verificationdata);
    this.localstorageservice.store(environment.loankey, this.localloanobject);
    return haserror;
  }
  updateAPHVerification(id, iscropunit) {
    this.apiservice.updateAPHVerification(id, iscropunit).subscribe(() => {

    });
  }
  resetVerification() {

    let _columns = this.columnApi.getAllColumns().filter(p => p.colId.includes("Verf_"));
    _columns.forEach(element => {
      this.columnApi.setColumnVisible(element.colId, false);
    });
    this.verifyButtonStage = VerificationStages.Verify;
    this.gridApi.redrawRows();
    this.gridApi.refreshHeader();
  }
  onCellClicked(event: any) {
    if (event.type == 'cellClicked' && event.colDef.field != 'value' && this._enableCopy) {
      this._enableCopy = false;

      let current_row: APH_Units = this.rowData[event.rowIndex];
      let previous_row: APH_Units = this.rowData[event.rowIndex - 1];

      if (!!current_row && current_row.ActionStatus == 1 && !!previous_row) {
        const column: string = event.colDef.field;
        const columns_to_copy = this.insService.getColumnsToCopy(this.columnDefs, column);
        current_row = this.insService.copyrow(previous_row, current_row, columns_to_copy);

        this.rowData[event.rowIndex] = current_row;

        this.gridApi.updateRowData({ update: [current_row] });
      }
    }
  }
  onBtExport() {
    const _timestamp = this.dtss.getTimeStamp();

    let columnKeys = this.columnDefs.filter(a => !a.supressExcel);
    columnKeys = columnKeys.map(a => a.field);

    const params = {
      skipHeader: false,
      columnGroups: true,
      skipFooters: false,
      skipGroups: false,
      skipPinnedTop: false,
      skipPinnedBottom: false,
      allColumns: true,
      exportMode: 'xlsx',
      suppressTextAsCDATA: true,
      columnKeys: columnKeys,
      fileName: `APH_${this.localloanobject.Loan_Full_ID}_${_timestamp}.xls`,
      sheetName: 'Sheet 1',
      processCellCallback: param => {
        if (param.column.colDef.headerName == 'Added Land') {
          if (param.value) {
            return 'Yes';
          }
          return 'No';
        } else {
          return param.value;
        }
      }
    };

    this.excel.updateExportToExcelTime('aph', _timestamp);

    // Export Data to Excel file
    let aphdata = this.gridApi.getDataAsExcel(params);

    let parser = new DOMParser();
    let aphXML = parser.parseFromString(aphdata, 'text/xml');

    let oSerializer = new XMLSerializer();
    let sXML = oSerializer.serializeToString(aphXML);

    let workbook = XLSX.read(sXML, { type: 'string' });
    XLSX.writeFile(workbook, `APH_${this.localloanobject.Loan_Full_ID}_${_timestamp}.xlsx`);

    this.publishService.enableSync(Page.insurance);
  }
  getverifiedstatus() {
    return this.localloanobject.LoanCropUnits.filter(p => p.Verf_Ins_APH_Status != true).length == 0;
  }
  onImportExcel(data: APH_Units[]) {
    try {
      if (data) {

        const res = this.insService.getValidAPHs(data, this.localloanobject);
        let cropUnits = res.data.filter(a => a.isCU).map(a => a.row);
        let aphUnits = res.data.filter(a => !a.isCU).map(a => a.row);

        if (res.addedRows > 0 || res.deletedRows > 0) {
          this.alertify.confirm('Confirm', 'You have added and deleted some rows. Are you sure you want to continue?')
            .subscribe(_res => {
              if (_res) {
                this.processExcelUpload(cropUnits, aphUnits);
              }
            });
        } else {
          this.processExcelUpload(cropUnits, aphUnits);
        }
      }
    } catch (ex) {
      this.toastr.error(ex.message);
    }
  }
  private processExcelUpload(cropUnits: Array<Loan_Crop_Unit>, aphUnits: Array<APH_Units>) {
    this.localloanobject.LoanCropUnits = cropUnits;
    this.localloanobject.AphUnits = aphUnits;

    this.localloanobject.srccomponentedit = SourceComponent.APH_Excel_Upload;

    this.rowDataset();

    this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
    this.publishService.enableSync(Page.insurance);
  }
  MethodFromCheckbox(params: any, type: string, $event: any) {
    if (type == 'Added_Land') {
      let obj = params.data;
      let edittedCU;

      if (obj && obj.Loan_CU_ID) {
        let edittedCUIndex = this.localloanobject.LoanCropUnits.findIndex(
          cu => cu.Loan_CU_ID == obj.Loan_CU_ID
        );

        edittedCU = this.localloanobject.LoanCropUnits[edittedCUIndex];
      }

      if ($event.srcElement.checked) {
        params.data['Added_Land'] = true;
        if (edittedCU) {
          edittedCU.Added_Land = true;
        }
      } else {
        params.data['Added_Land'] = false;
        if (edittedCU) {
          edittedCU.Added_Land = false;
        }
      }

      if (params.data.ActionStatus != 1) {
        params.data.ActionStatus = 2;
        if (edittedCU) {
          edittedCU.ActionStatus = 2;
        }
      }

      this.localloanobject.lasteditrowindex = params.rowIndex;
      this.localloanobject.srccomponentedit = SourceComponent.APH;

      this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
      this.publishService.enableSync(Page.insurance);
    }
  }

  get Loan_Status() {
    if (this.localloanobject) {
      return this.localloanobject.LoanMaster.Loan_Status;
    }
    return '';
  }

  get hasData() {
    try {
      return this.rowData.some(a => a.Ins_APH > 0);
    } catch {
      return false;
    }
  }

  private setValidationErrorForAPH() {
    if (environment.isDebugModeActive) console.time('APH Validation');

    this.validationservice.validateTableFields<any>(
      this.rowData,
      this.columnDefs,
      Page.insurance,
      'Aph_',
      'Aph',
      'Loan_CU_ID'
    );

    if (environment.isDebugModeActive) console.timeEnd('APH Validation');
  }

}
function getLandlords() {
  let loanobj = JSON.parse('[' + window.localStorage.getItem('ng2-webstorage|currentselectedloan') + ']')[0] as loan_model;
  let values = [];

  loanobj.Association.filter(p => p.Assoc_Type_Code == 'LLD' && p.ActionStatus != 3).forEach(element => {
    values.push({ value: element.Assoc_Name, key: element.Assoc_Name });
  });

  return { values: values };
}
