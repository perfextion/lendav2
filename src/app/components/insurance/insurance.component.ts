import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../environments/environment.prod';
import { PublishService } from '../../services/publish.service';
import { Page } from '@lenda/models/page.enum';
import { InsuranceService } from './insurance.service';
import { loan_model } from '@lenda/models/loanmodel';
import { Chevron } from '@lenda/models/loan-response.model';
import { MasterService } from '@lenda/master/master.service';

@Component({
  selector: 'app-insurance',
  templateUrl: './insurance.component.html',
  styleUrls: ['./insurance.component.scss'],
  providers: [InsuranceService]
})
export class InsuranceComponent implements OnInit {
  public optedInsuranceOptions: Array<any> = [];
  public currentPageName: string = Page.insurance;
  private loanmodel: loan_model;
  public Chevron: typeof Chevron = Chevron;

  isAgencyOpen: boolean = false;
  isAIPOpen: boolean = false;

  constructor(
    private insuranceService: InsuranceService,
    private localStorageService: LocalStorageService,
    private publishService: PublishService,
    public masterSvc: MasterService
  ) { }

  ngOnInit() {

    this.loanmodel = this.localStorageService.retrieve(environment.loankey);
    //this.getOptedInsuranceOptions();

    this.checkIfAssocChevronsIsOpen();
  }

  checkIfAssocChevronsIsOpen() {

    this.loanmodel.Association.forEach(el => {
      if (el.Assoc_Type_Code == 'AGY') {
        this.isAgencyOpen = true;
      }

      if(el.Assoc_Type_Code == 'AIP') {
        this.isAIPOpen = true;
      }
    });
  }

  getEnumValueForChevron(name) {
    return this.Chevron[name];
  }

  getOptedInsuranceOptions() {
    this.optedInsuranceOptions = [];
    this.optedInsuranceOptions.push('MPCI');
    this.loanmodel.InsurancePolicies.forEach(ip => {
      if (ip.ActionStatus != 3) {
        ip.Subpolicies.forEach(sip => {

          if (sip.ActionStatus != 3 && sip.Ins_Type && (this.optedInsuranceOptions.indexOf(sip.Ins_Type) == -1)) {
            this.optedInsuranceOptions.push(sip.Ins_Type);
          }
        });
      }
    });
  }
  /**
  * Sync to database - publish button event
  */
  synctoDb() {

    this.publishService.syncCompleted();
    //this.insuranceService.syncToDb(this.localStorageService.retrieve(environment.loankey));
    this.publishService.syncToDb();
  }

}
