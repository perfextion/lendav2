import {
  Component,
  OnInit,
  Input,
  OnChanges,
  EventEmitter,
  Output,
  OnDestroy
} from '@angular/core';

import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import {
  loan_model,
  Loan_Association,
  AssociationTypeCode,
  LoanStatus
} from '@lenda/models/loanmodel';
import {
  numberValueSetter,
  getNumericCellEditor,
  getPhoneCellEditor,
  formatPhoneNumberAsYouType
} from '@lenda/Workers/utility/aggrid/numericboxes';
import { SelectEditor } from '@lenda/aggridfilters/selectbox';
import { DeleteButtonRenderer } from '@lenda/aggridcolumns/deletebuttoncolumn';
import { EmailEditor } from '@lenda/Workers/utility/aggrid/emailboxes';
import {
  Preferred_Contact_Ind_Options,
  PreferredContactFormatter
} from '@lenda/Workers/utility/aggrid/preferredcontactboxes';
import { getAlphaNumericCellEditor } from '@lenda/Workers/utility/aggrid/alphanumericboxes';
import { Page } from '@lenda/models/page.enum';
import {
  CellType,
  cellclassmaker,
  isgrideditable,
  setgriddefaults,
  headerclassmaker,
  calculatecolumnwidths
} from '@lenda/aggriddefinations/aggridoptions';
import { AutocompleteCellEditor } from '@lenda/aggridcolumns/autocomplete-cell-editor/autocomplete-cell-editor';
import {
  ThirdPartyAmountValidation,
  ROQ_Association_Validation,
  AssoicationNameValidation,
  DuplicateAssoicationNameContactValidation,
  LienholderSubordinationValidation,
  LienholderDocumentationValidation,
  EmailValidation
} from '@lenda/Workers/utility/validation-functions';
import { errormodel } from '@lenda/models/commonmodels';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';
import { RefChevron, RefDataModel, RefAssociation, Ref_Association_Default, Ref_Association_Type } from '@lenda/models/ref-data-model';
import { currencyFormatter } from '@lenda/aggridformatters/valueformatters';
import { LoanSettings, TableState, TableAddress, Chevrons } from '@lenda/preferences/models/loan-settings/index.model';
import { PrincialCheckboxCellRenderer } from '@lenda/aggridcolumns/principal-checkbox-celleditor';
import { Lienholder_Documents } from './lienholder-docs';
import { EditDefaultSettings } from '@lenda/preferences/models/user-settings/edit-default-settings.model';

import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { DataService } from '@lenda/services/data.service';
import { PublishService } from '@lenda/services/publish.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { InsuranceapiService } from '@lenda/services/insurance/insuranceapi.service';
import { ToasterService } from '@lenda/services/toaster.service';

declare function setmodifiedall(array, keyword): any;
declare function setmodifiedsingle(obj, keyword): any;
/// <reference path="../../../Workers/utility/aggrid/numericboxes.ts" />
@Component({
  selector: 'app-association',
  templateUrl: './association.component.html',
  styleUrls: ['./association.component.scss']
})
export class AssociationComponent implements OnInit, OnChanges, OnDestroy {
  public refdata: RefDataModel = <RefDataModel>{};
  public isAdding: boolean = false;
  public isgriddirty: boolean;
  public indexsedit = [];
  public columnDefs = [];

  private localloanobject: loan_model = new loan_model();

  private ReferredTypes = [
    'Word of Mouth',
    'Distributer',
    'Agency',
    'Bank',
    'Other'
  ];
  private Responses = ['Positive', 'Neutral', 'Negative'];

  private subscription: ISubscription;

  @Input('header')
  public header: string = '';

  @Input('firstColumnHeader')
  public firstColumnHeader: string = '';

  @Input('associationTypeCode')
  public associationTypeCode: AssociationTypeCode;

  @Input('chevronId')
  public chevronId: number;

  @Input('withoutChevron')
  public withoutChevron: boolean = false;

  @Output('onRowCountChange')
  public onRowCountChange: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  public currentPageName: Page;

  @Input()
  public expanded: boolean = true;

  @Input()
  public isViewOnly: boolean = false;

  //#region Ag grid Configuration

  public rowData = new Array<Loan_Association>();
  public components;
  public context;
  public frameworkcomponents;
  public editType;
  public pinnedBottomRowData = [];
  private gridApi;
  public columnApi: any;
  private states = [];

  public Ref_Association_Defaults: Ref_Association_Default[] = [];
  public Ref_Associations: RefAssociation[] = [];

  public currenterrors: Array<errormodel>;

  public style: any = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };

  private errosub: ISubscription;
  private preferencesChangeSub: ISubscription;

  returncountylist() {
    return this.refdata.CountyList;
  }

  gridOptions = {
    getRowNodeId: data => `${this.associationChevron.Chevron_Code}_${data.Assoc_ID}`
  };

  associationChevron: RefChevron = <RefChevron>{};

  public get tableId() {
    return 'Table_' + this.associationChevron.Chevron_Code;
  }

  public get rowId() {
    return this.associationChevron.Chevron_Code + '_';
  }

  public Office_ID: number = 0;
  private document_list: Array<{key: string, value: string}> = [];

  private editDefaultSettings: EditDefaultSettings;

  public get canUpdateDefault() {
    if (this.editDefaultSettings) {
      return this.editDefaultSettings.isNameAssociationEnabled;
    }
    return false;
  }

  private ref_assoc_type: Ref_Association_Type;

  //#endregion Ag grid Configuration

  constructor(
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public insuranceservice: InsuranceapiService,
    public logging: LoggingService,
    public alertify: AlertifyService,
    public toast: ToasterService,
    public loanapi: LoanApiService,
    private publishService: PublishService,
    private dataService: DataService,
    private validationService: ValidationService,
    private dtss: DatetTimeStampService,
    private sessionStorage: SessionStorageService,
    private settingsService: SettingsService
  ) {
    this.frameworkcomponents = {
      emaileditor: EmailEditor,
      selectEditor: SelectEditor,
      deletecolumn: DeleteButtonRenderer,
      autocompleteEditor: AutocompleteCellEditor,
      checkbox: PrincialCheckboxCellRenderer
    };

    this.components = {
      numericCellEditor: getNumericCellEditor(),
      alphaNumericCellEditor: getAlphaNumericCellEditor(),
      phoneCellEditor: getPhoneCellEditor()
    };

    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);
    this.states = this.refdata.StateList;
    this.Ref_Association_Defaults = this.refdata.Ref_Association_Defaults || [];
    this.Ref_Associations = this.refdata.RefAssociations || [];

    this.Office_ID = this.localstorageservice.retrieve(environment.officeid) as number;
    if (!this.Office_ID) {
      let user = this.sessionStorage.retrieve('UID');
      this.Office_ID = user.Office_ID;
    }

    this.document_list = Lienholder_Documents;
  }

  private getUserPreferences() {
    this.editDefaultSettings = this.settingsService.preferences.userSettings.editDefaultSettings;

    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe((preferences) => {
      this.editDefaultSettings = preferences.userSettings.editDefaultSettings;
    });
  }

  ngOnInit() {
    this.getUserPreferences();
    this.prepareColDefs();

    if (this.refdata) {
      this.associationChevron = (this.refdata as RefDataModel).RefChevrons.find(c => c.Chevron_ID == this.chevronId);
    } else {
      this.associationChevron = <RefChevron>{};
    }

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
       if (environment.isDebugModeActive) { console.time(this.associationChevron.Chevron_Name + ' Observer'); }

      if (res) {
        this.localloanobject = res;
        if (this.localloanobject.Association) {
          if ( this.localloanobject.srccomponentedit == this.associationTypeCode + 'AssociationComponent' ) {
            this.rowData[this.localloanobject.lasteditrowindex] = this.localloanobject.Association.filter( p =>
                p.ActionStatus != 3 &&
                p.Assoc_Type_Code == this.associationTypeCode
            )[this.localloanobject.lasteditrowindex];
          } else {
            this.rowData = this.localloanobject.Association.filter(p =>
                p.ActionStatus != 3 &&
                p.Assoc_Type_Code == this.associationTypeCode
            );
          }
        }

        this.setValidationErrorForAssociation();
        setTimeout(() => {
          this.getcurrenterrors();
          setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), this.rowId);
        }, 5);
      }

       if (environment.isDebugModeActive) { console.timeEnd(this.associationChevron.Chevron_Name + ' Observer'); }
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.errosub) {
      this.errosub.unsubscribe();
    }

    if (this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  ngOnChanges() {
    this.ref_assoc_type = this.refdata.Ref_Association_Types.find(
      a =>
        a.Assoc_Type_Code == this.associationTypeCode
    );

    this.prepareColDefs();

    this.errosub = this.localstorageservice.observe(environment.errorbase).subscribe(() => {
      setTimeout(() => {
        this.getcurrenterrors();
        setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), this.rowId);
      }, 5);
    });

    setTimeout(() => {
      setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), this.rowId);
    }, 5);
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    setgriddefaults(this.gridApi, this.columnApi);

    //subscription to State
    this.gridApi.addEventListener('displayedColumnsChanged', (event) => {
      if (environment.isDebugModeActive) { console.time("started"); }

      let state: any = this.columnApi.getColumnState();

      // if(state.filter(p=>p.hide==false).length>0)
      // {
      let obj = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings) as LoanSettings;
      if (obj.Table_States == undefined || obj.Table_States == null) {
        obj.Table_States = new Array<TableState>();
      }
      let tempstate = this.getAssocTableState();
      let data = obj.Table_States.find(p => p.address.page == tempstate.address.page && p.address.chevron == tempstate.address.chevron);
      if (data != undefined) {
      data.data = state;
      } else {
        let farm_farmstate = new TableState();
        farm_farmstate.address = new TableAddress();
        farm_farmstate = tempstate;
        farm_farmstate.data = state;
        obj.Table_States.push(farm_farmstate);
      }

      this.localloanobject.LoanMaster.Loan_Settings = JSON.stringify(obj);
      this.dataService.setLoanObjectwithoutsubscription(this.localloanobject);
      if (environment.isDebugModeActive) { console.timeEnd("started"); }
    });

    let local: any = JSON.parse(window.localStorage.getItem("ng2-webstorage|currentselectedloan"));
    let loansettingd = (JSON.parse(local.LoanMaster.Loan_Settings) as LoanSettings);

    if (loansettingd && loansettingd.Table_States != undefined && loansettingd.Table_States != null) {
      let state = this.getAssocTableState();
      let data = loansettingd.Table_States.find(p => p.address.page == state.address.page && p.address.chevron == state.address.chevron);

      if (data) {
        this.columnApi.setColumnState(data.data);
      }
    }

    this.setValidationErrorForAssociation();
    this.adjustToFitAgGrid();
  }

  private getAssocTableState(assoc_table_state?: TableState) {
    if (!assoc_table_state) {
      assoc_table_state = new TableState();
    }

    if (
      assoc_table_state.address == undefined ||
      assoc_table_state.address == null
    ) {
      assoc_table_state.address = new TableAddress();
    }

    if (this.associationTypeCode == AssociationTypeCode.Landlord) {
      assoc_table_state.address.chevron = Chevrons.farm_landlord;
      assoc_table_state.address.page = Page.farm;
    }

    if (this.associationTypeCode == AssociationTypeCode.Agency) {
      assoc_table_state.address.chevron = Chevrons.insurance_agency;
      assoc_table_state.address.page = Page.insurance;
    }

    if (this.associationTypeCode == AssociationTypeCode.AIP) {
      assoc_table_state.address.chevron = Chevrons.insurance_aip;
      assoc_table_state.address.page = Page.insurance;
    }

    if (this.associationTypeCode == AssociationTypeCode.Buyer) {
      assoc_table_state.address.chevron = Chevrons.collateral_buyer;
      assoc_table_state.address.page = Page.collateral;
    }

    if (this.associationTypeCode == AssociationTypeCode.CreditRererrence) {
      assoc_table_state.address.chevron = Chevrons.borrower_creditreference;
      assoc_table_state.address.page = Page.borrower;
    }

    if (this.associationTypeCode == AssociationTypeCode.Distributor) {
      assoc_table_state.address.chevron = Chevrons.budget_distributor;
      assoc_table_state.address.page = Page.budget;
    }

    if (this.associationTypeCode == AssociationTypeCode.Guarantor) {
      assoc_table_state.address.chevron = Chevrons.collateral_guaranter;
      assoc_table_state.address.page = Page.collateral;
    }

    if (this.associationTypeCode == AssociationTypeCode.Harvester) {
      assoc_table_state.address.chevron = Chevrons.budget_harvestor;
      assoc_table_state.address.page = Page.budget;
    }

    if (this.associationTypeCode == AssociationTypeCode.LienHolder) {
      assoc_table_state.address.chevron = Chevrons.collateral_lienholder;
      assoc_table_state.address.page = Page.collateral;
    }

    if (this.associationTypeCode == AssociationTypeCode.Rebator) {
      assoc_table_state.address.chevron = Chevrons.crop_rebator;
      assoc_table_state.address.page = Page.crop;
    }

    if (this.associationTypeCode == AssociationTypeCode.ReferredFrom) {
      assoc_table_state.address.chevron = Chevrons.borrower_reffrom;
      assoc_table_state.address.page = Page.borrower;
    }

    if (this.associationTypeCode == AssociationTypeCode.ThirdParty) {
      assoc_table_state.address.chevron = Chevrons.budget_thirdparty;
      assoc_table_state.address.page = Page.budget;
    }

    return assoc_table_state;
  }

  adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  displayedColumnsChanged(params) {
    this.adjustToFitAgGrid();
  }

  private prepareColDefs() {
    if (this.header && this.associationTypeCode) {
      this.columnDefs = [
        {
          headerName: this.firstColumnHeader ? this.firstColumnHeader : this.header,
          field: 'Assoc_Name',
          minWidth: 180,
          width: 180,
          maxWidth: 180,
          tooltip: params => params.data['Assoc_Name'],
          cellClass: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

            let isViewOnly = params.context.componentParent.isViewOnly;
            let iseditable = !isViewOnly || (isViewOnly && params.data.ActionStatus != -1);
            return cellclassmaker(CellType.Text, iseditable, '', status);
          },
          editable: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

            let isViewOnly = params.context.componentParent.isViewOnly;
            let iseditable = !isViewOnly || (isViewOnly && params.data.ActionStatus != -1);
            return isgrideditable(iseditable, status);
          },
          cellEditor: 'autocompleteEditor',
          validations: {
            roq: ROQ_Association_Validation,
            name: AssoicationNameValidation,
            duplicate: DuplicateAssoicationNameContactValidation,
            subordination: LienholderSubordinationValidation
          }
        },
        {
          headerName: this.chevronId && this.chevronId == 23 ? 'Agent' : 'Contact',
          field: 'Contact',
          minWidth: 120,
          width: 120,
          maxWidth: 120,
          tooltip: params => params.data['Contact'],
          cellClass: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

            let isViewOnly = params.context.componentParent.isViewOnly;
            let iseditable = !isViewOnly || (isViewOnly && params.data.ActionStatus != -1);
            return cellclassmaker(CellType.Text, iseditable, '', status);
          },
          editable: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

            let isViewOnly = params.context.componentParent.isViewOnly;
            let iseditable = !isViewOnly || (isViewOnly && params.data.ActionStatus != -1);
            return isgrideditable(iseditable, status);
          },
          cellEditor: 'agTextCellEditor',
          validations: {
            required: this.ref_assoc_type ? this.ref_assoc_type.Contact_Req_Ind == 1 : false
          }
        },
        {
          headerName: 'Location',
          field: 'Location',
          minWidth: 120,
          width: 120,
          maxWidth: 120,
          tooltip: params => params.data['Location'],
          cellClass: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

            let isViewOnly = params.context.componentParent.isViewOnly;
            let iseditable = !isViewOnly || (isViewOnly && params.data.ActionStatus != -1);
            return cellclassmaker(CellType.Text, iseditable, '', status);
          },
          editable: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

            let isViewOnly = params.context.componentParent.isViewOnly;
            let iseditable = !isViewOnly || (isViewOnly && params.data.ActionStatus != -1);
            return isgrideditable(iseditable, status);
          },
          cellEditor: 'agTextCellEditor',
          validations: {
            required: this.ref_assoc_type ? this.ref_assoc_type.Location_Req_Ind == 1 : false
          }
        },
        {
          headerName: 'Phone',
          field: 'Phone',
          minWidth: 120,
          width: 120,
          maxWidth: 120,
          tooltip: params => params.data['Phone'],
          cellClass: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

            let isViewOnly = params.context.componentParent.isViewOnly;
            let iseditable = !isViewOnly || (isViewOnly && params.data.ActionStatus != -1);
            return cellclassmaker(CellType.Text, iseditable, '', status);
          },
          editable: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

            let isViewOnly = params.context.componentParent.isViewOnly;
            let iseditable = !isViewOnly || (isViewOnly && params.data.ActionStatus != -1);
            return isgrideditable(iseditable, status);
          },
          headerClass: headerclassmaker(CellType.Text),
          cellEditor: 'phoneCellEditor',
          valueFormatter: formatPhoneNumberAsYouType
        },
        {
          headerName: 'Email',
          field: 'Email',
          minWidth: 120,
          width: 120,
          maxWidth: 120,
          tooltip: params => params.data['Email'],
          cellClass: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

            let isViewOnly = params.context.componentParent.isViewOnly;
            let iseditable = !isViewOnly || (isViewOnly && params.data.ActionStatus != -1);
            return cellclassmaker(CellType.Text, iseditable, '', status);
          },
          editable: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

            let isViewOnly = params.context.componentParent.isViewOnly;
            let iseditable = !isViewOnly || (isViewOnly && params.data.ActionStatus != -1);
            return isgrideditable(iseditable, status);
          },
          cellEditor: 'agTextCellEditor',
          valueSetter: function(params) {
            let regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            let regedxp = new RegExp(regexp);
           if (regedxp.test(params.newValue)) {
             params.data.Email = params.newValue;
           } else {
            params.context.componentParent.toast.error('Invalid Email Format');
            params.data.Email = '';
           }
          },
          validations: {
            required: this.ref_assoc_type ? this.ref_assoc_type.Email_Req_Ind == 1 : false,
            email: EmailValidation
          }
        },
        {
          headerName: 'Pref Contact',
          width: 120,
          minWidth: 120,
          maxWidth: 120,
          field: 'Preferred_Contact_Ind',
          cellClass: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

            let isViewOnly = params.context.componentParent.isViewOnly;
            let iseditable = !isViewOnly || (isViewOnly && params.data.ActionStatus != -1);
            return cellclassmaker(CellType.Text, iseditable, '', status);
          },
          editable: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

            let isViewOnly = params.context.componentParent.isViewOnly;
            let iseditable = !isViewOnly || (isViewOnly && params.data.ActionStatus != -1);
            return isgrideditable(iseditable, status);
          },
          cellEditor: 'selectEditor',
          cellEditorParams: { values: Preferred_Contact_Ind_Options },
          valueFormatter: PreferredContactFormatter,
          validations: {
            required: this.ref_assoc_type ? this.ref_assoc_type.Preffered_Contact_Req_Ind == 1 : false
          }
        },
        {
          headerName: '',
          field: 'value',
          minWidth: 60,
          width: 60,
          maxWidth: 60,
          suppressToolPanel: true,
          cellRendererSelector: function(params) {
            let isViewOnly = params.context.componentParent.isViewOnly;
            if (isViewOnly) {
              return null;
            }
            return {
              component: 'deletecolumn'
            };
          }
        }
      ];

      if (this.associationTypeCode == AssociationTypeCode.LienHolder) {
        let documentColumn = {
          headerName: 'Documentation',
          width: 120,
          minWidth: 120,
          maxWidth: 120,
          field: 'Documentation',
          tooltip: (params: any) => params.data.Documentation,
          validations: {
            Documentation: LienholderDocumentationValidation,
            required: this.ref_assoc_type ? this.ref_assoc_type.Value_Req_Ind == 1 : false
          },
          headerClass: headerclassmaker(CellType.Text),
          cellClass: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return cellclassmaker(CellType.Integer, true, '', status);
          },
          editable: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return isgrideditable(true, status);
          },
          cellEditor: 'selectEditor',
          cellEditorParams: params =>  {
            return {
              values: this.document_list
            };
          }
        };
        this.columnDefs.splice(this.columnDefs.length - 1, 0, documentColumn);
      }

      if (this.associationTypeCode == AssociationTypeCode.ThirdParty) {
        let amountCol = {
          headerName: 'Amount',
          width: 120,
          minWidth: 120,
          maxWidth: 120,
          field: 'Amount',
          headerClass: headerclassmaker(CellType.Integer),
          cellClass: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return cellclassmaker(CellType.Integer, true, '', status);
          },
          editable: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return isgrideditable(true, status);
          },
          cellEditor: 'numericCellEditor',
          valueSetter: numberValueSetter,
          validations: {
            amount: ThirdPartyAmountValidation,
            required: this.ref_assoc_type ? this.ref_assoc_type.Value_Req_Ind == 1 : false
          },
          cellEditorParams: params => {
            return { value: params.data.Amount || 0 };
          },
          valueFormatter: function (params) {
            return params ? currencyFormatter(params, 0) : '';
          },
        };
        this.columnDefs.splice(this.columnDefs.length - 1, 0, amountCol);
      }

      if (this.associationTypeCode == AssociationTypeCode.ReferredFrom) {
        let REFColumn = {
          headerName: 'Type',
          width: 120,
          minWidth: 120,
          maxWidth: 120,
          field: 'Referred_Type',
          cellClass: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return cellclassmaker(CellType.Text, true, '', status);
          },
          editable: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return isgrideditable(true, status);
          },
          cellEditor: 'selectEditor',
          cellEditorParams: {
            values: this.getSelectBoxValueFromPremitiveArray(this.ReferredTypes)
          },
          validations: {
            required: this.ref_assoc_type ? this.ref_assoc_type.Value_Req_Ind == 1 : false
          }
        };
        this.columnDefs.splice(this.columnDefs.length - 2, 0, REFColumn);
      }

      if (this.associationTypeCode == AssociationTypeCode.CreditRererrence) {
        let CRFColumn = {
          headerName: 'Response',
          width: 120,
          minWidth: 120,
          maxWidth: 120,
          field: 'Response',
          cellClass: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return cellclassmaker(CellType.Text, true, '', status);
          },
          editable: function(params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return isgrideditable(true, status);
          },
          cellEditor: 'selectEditor',
          cellEditorParams: {
            values: this.getSelectBoxValueFromPremitiveArray(this.Responses)
          },
          validations: {
            required: this.ref_assoc_type ? this.ref_assoc_type.Value_Req_Ind == 1 : false
          }
        };
        this.columnDefs.splice(this.columnDefs.length - 2, 0, CRFColumn);
      }

      this.context = {
        componentParent: this,
        associationTypeCode: this.associationTypeCode,
        states: this.states,
        names: this.Ref_Association_Defaults
      };

      this.getdataforgrid();
      this.editType = '';
    } else {
      throw new Error(
        "'header' and 'associationTypeCode' are required for Association Component"
      );
    }
  }

  public Set_Princial_Ind(Principal_Ind: boolean, Assoc_ID: number) {
    let assoc = this.localloanobject.Association.find(a => a.Assoc_ID == Assoc_ID);

    if (assoc) {
      if (Principal_Ind) {
        assoc.Principal_Ind = 1;
      } else {
        assoc.Principal_Ind = 0;
      }

      assoc.ActionStatus = 2;
      this.localloanobject.srccomponentedit = this.associationTypeCode + 'AssociationComponent';
      this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
      this.publishService.enableSync(Page.collateral);
    }
  }

  private getcurrenterrors() {
    this.currenterrors = (this.localstorageservice.retrieve(environment.errorbase) as Array<errormodel>).filter(
      p => p.chevron == this.associationChevron.Chevron_Code
    );

    this.validationService.highlighErrorCells(this.currenterrors, this.tableId);
  }

  private getdataforgrid() {
    if (this.localloanobject != null && this.localloanobject != undefined) {
      if (this.localloanobject.Association) {
        this.rowData = this.localloanobject.Association.filter(
          p =>
            p.ActionStatus != 3 && p.Assoc_Type_Code == this.associationTypeCode
        );
      } else {
        this.rowData = null;
      }
    }

    setTimeout(() => {
      this.getcurrenterrors();
    }, 5);

    this.setValidationErrorForAssociation();
  }

  private getSelectBoxValueFromPremitiveArray(array: Array<string>) {
    return array.map(item => {
      return { key: item, value: item };
    });
  }

  defaultColumns = ['Contact', 'Email', 'Location', 'Phone'];

  public rowvaluechanged(params: any) {
    if (params && (params.oldValue == params.value)) {
      return;
    }

    let column = params.colDef.field;

    if ( params.data.ActionStatus == 1 && column != 'Assoc_Name') {
      if (!params.data.Assoc_Name) {
        let h = this.firstColumnHeader ? this.firstColumnHeader : this.header;

        this.alertify.confirm(
          'Confirm',
          `Cannot Enter a row with Empty ${h} `
        ).subscribe(res => {
          if (res) {
            params.data[column] = '';

            this.gridApi.refreshCells();

            this.gridApi.startEditingCell({
              rowIndex: this.rowData.length - 1,
              colKey: 'Assoc_Name'
            }); `                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                `;
          } else {
            let localObjIndex = this.localloanobject.Association.findIndex(
              assoc => assoc.Assoc_ID == params.data.Assoc_ID
            );

            this.localloanobject.Association.splice(localObjIndex, 1);

            this.localloanobject.srccomponentedit = undefined;
            this.localloanobject.lasteditrowindex = undefined;
            this.loanserviceworker.performcalculationonloanobject(
              this.localloanobject
              );
            this.publishService.enableSync(this.currentPageName);
          }
        });
      } else {
        this._rowValueChanged(params);
      }
    } else {
      this._rowValueChanged(params);
    }
  }

  private _rowValueChanged(params) {
    let column = params.colDef.field;

    //#region MODIFIED YELLOW VALUES

    let modifiedvalues = this.localstorageservice.retrieve(environment.modifiedbase) as Array<String>;

    if (!modifiedvalues.includes(this.rowId + params.data.Assoc_ID + "_" + params.colDef.field)) {
      modifiedvalues.push(this.rowId + params.data.Assoc_ID + "_" + params.colDef.field);
      setmodifiedsingle(this.rowId + params.data.Assoc_ID + '_' + params.colDef.field, this.rowId);
      this.localstorageservice.store(environment.modifiedbase, modifiedvalues);
    }

    //#endregion MODIFIED YELLOW VALUES

   let obj = params.data as Loan_Association;
   if (obj.ActionStatus != 1) {
     obj.ActionStatus = 2;
   }

   if (this.canUpdateDefault && this.defaultColumns.some(col => col == column)) {
     let showAlert = !obj['Alert'] && !!obj.Ref_Assoc_ID;

     if (showAlert) {
       obj['Alert'] = true;

       this.alertify.confirm(
         'Confirm',
         'Default data is being changed. Do you want to update your defaults?'
       )
       .subscribe(res => {
         if (!res) {
           obj['ActionDefault'] = 0;
           this._doCalculation(params);

         } else {
           obj['ActionDefault'] = 1;
           this._doCalculation(params);
         }
       });
     } else {
       this._doCalculation(params);
     }
   } else {
     this._doCalculation(params);
   }
  }

  private _doCalculation(params) {
    this.localloanobject.srccomponentedit = this.associationTypeCode + 'AssociationComponent';
    this.localloanobject.lasteditrowindex = params.rowIndex;

    this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
    this.publishService.enableSync(this.currentPageName);
    this.setValidationErrorForAssociation();
    this.getcurrenterrors();
  }

  //Grid Events
  public addrow() {

    let newAddedRowWithoutAssocName = this.localloanobject.Association.filter(a => {
      return a.ActionStatus == 1 && a.Assoc_Type_Code == this.associationTypeCode && !a.Assoc_Name;
    });

    if (newAddedRowWithoutAssocName.length > 0) {
      this.alertify.alert('Alert',  'You already have a blank added row.');
      return;
    }

    let newItem = new Loan_Association();
    this.isAdding = true;
    newItem.ActionStatus = 1;
    newItem.Assoc_ID = getRandomInt(Math.pow(10, 6), Math.pow(10, 9));
    newItem.Loan_Full_ID = this.localloanobject.Loan_Full_ID;
    newItem.Loan_ID = this.localloanobject.LoanMaster.Loan_ID;
    newItem.Loan_Seq_Num = this.localloanobject.LoanMaster.Loan_Seq_num;
    newItem.Office_ID = this.Office_ID;
    newItem.Assoc_Type_Code = this.associationTypeCode;
    newItem.Preferred_Contact_Ind = 0;
    if(newItem.Assoc_Type_Code == AssociationTypeCode.AIP || newItem.Assoc_Type_Code == AssociationTypeCode.Agency) {
      this.loanapi.SaveAssociationImmediate(newItem).subscribe(_res => {
        newItem.Assoc_ID =_res.Data;
        newItem.ActionStatus = 0;
        this._updateAssoc(newItem);
      });
    } else {
      this._updateAssoc(newItem);
    }
  }

  private _updateAssoc(newItem: Loan_Association) {
    this.gridApi.updateRowData({ add: [newItem] });
    this.gridApi.startEditingCell({
      rowIndex: this.rowData.length,
      colKey: 'Assoc_Name'
    });

    if (this.associationTypeCode == AssociationTypeCode.Distributor) {
      this.onAddDistributor(newItem);
    }

    this.localloanobject.Association.push(newItem);
    this.rowData.push(newItem);
    this.publishService.enableSync(this.currentPageName);
    this.adjustToFitAgGrid();

    this.setValidationErrorForAssociation();
  }

  /**
   * Export Rebator Data
   */
  public onBtExport() {
    const params = {
      skipHeader: false,
      columnGroups: true,
      skipFooters: false,
      skipGroups: false,
      skipPinnedTop: false,
      skipPinnedBottom: false,
      allColumns: true,
      fileName: `${this.header}_${this.localloanobject.Loan_Full_ID}_${this.dtss.getTimeStamp()}.xlxs`,
      sheetName: this.header,
      processCellCallback: param => {
        if (param.value && param.column.colDef.headerName == 'Pref Contact') {
          return PreferredContactFormatter(param);
        // } else if (param.value && param.column.colDef.headerName == 'Amount') {
        //   return '$' + param.value;
        // } else if (param.column.colDef.headerName == 'Ins UOM') {
        //   return 'bu';
        } else if (param.value && param.column.colDef.headerName == 'Phone') {
          return formatPhoneNumberAsYouType(param);
        } else {
          return param.value;
        }
      }
    };
    // Export Data to Excel file
    this.gridApi.exportDataAsExcel(params);
  }

  public DeleteClicked(rowIndex: any, data: Loan_Association) {
    this.alertify.deleteRecord().subscribe(res => {
      if (res == true) {
        let localObjIndex = this.localloanobject.Association.findIndex(
          assoc => assoc.Assoc_ID == data.Assoc_ID
        );

        if (localObjIndex > -1) {
          if (data.ActionStatus == 1) {
            this.localloanobject.Association.splice(localObjIndex, 1);
          } else {
            this.localloanobject.Association[localObjIndex].ActionStatus = 3;
          }

          this.localloanobject.srccomponentedit = undefined;
          this.localloanobject.lasteditrowindex = undefined;
        }

        if (data.Assoc_Type_Code == AssociationTypeCode.Landlord) {
          this.onDeleteLandlord(data);
        }

        if (data.Assoc_Type_Code == AssociationTypeCode.Buyer) {
          this.onDeleteBuyer(data);
        }

        if (data.Assoc_Type_Code == AssociationTypeCode.Distributor) {
          this.onDeleteDistributor();
        }

        this.loanserviceworker.performcalculationonloanobject(
          this.localloanobject
          );
        this.publishService.enableSync(this.currentPageName);
        // this.setValidationErrorForAssociation();
        this.isAdding = false;
      }
    });
  }

  private onDeleteLandlord(lld: Loan_Association) {
    let farms = this.localloanobject.Farms.filter(a => a.ActionStatus != 3 && a.Landowner == lld.Assoc_Name);
    farms.forEach(a => {
      a.Landowner = '';
      a.ActionStatus = 2;
    });
  }

  private onDeleteBuyer(buyer: Loan_Association) {
    let contracts = this.localloanobject.LoanMarketingContracts.filter(
      m => m.ActionStatus != 3 && m.Assoc_ID == buyer.Assoc_ID
    );

    contracts.forEach(a => {
      a.Assoc_ID = null;
      a.ActionStatus = 2;
    });
  }

  private onDeleteDistributor() {
    let dist = this.localloanobject.Association.filter(
      a => a.ActionStatus != 3 && a.Assoc_Type_Code == AssociationTypeCode.Distributor
    );

    if (dist.length == 0) {
      this.localloanobject.LoanMaster.Distributor_ID = 0;
    }
  }

  private onAddDistributor(newDist: Loan_Association) {
    let dist = this.localloanobject.Association.filter(
      a => a.ActionStatus != 3 && a.Assoc_Type_Code == AssociationTypeCode.Distributor
    );

    if (dist.length == 0) {
      this.localloanobject.LoanMaster.Distributor_ID = newDist.Assoc_ID;
    }
  }

  private setValidationErrorForAssociation() {
    if (environment.isDebugModeActive) {  console.time('Validation Association'); }
    if (this.associationChevron.Chevron_Code) {
      // this.hyperfieldValidation.processHyperfieldValidation(this.chevronId, this.rowData, this.associationChevron.Chevron_Code);

      this.validationService.validateTableFields<Loan_Association>(
        this.rowData,
        this.columnDefs,
        this.currentPageName,
        this.rowId,
        this.associationChevron.Chevron_Code,
        'Assoc_ID'
      );
    }

    if (environment.isDebugModeActive) { console.timeEnd('Validation Association'); }
  }

  public isLoanEditable() {
    return !this.localloanobject || this.localloanobject.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  public get hasData() {
    try {
      if (this.rowData) {
        return this.rowData.length > 0;
      }
      return false;
    } catch {
      return false;
    }
  }
}
