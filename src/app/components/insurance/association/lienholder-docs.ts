export const Lienholder_Documents = [
  {
    key: '',
    value: ''
  },
  {
    key: 'Lienholder has not subordinated',
    value: 'Lienholder has not subordinated.'
  },
  {
    key: 'Lienholder Subordination : all crops; open-ended amount',
    value: 'Lienholder Subordination : all crops; open-ended amount.'
  },
  {
    key: 'Lienholder Subordination : all crops; amount limited to total commitment',
    value: 'Lienholder Subordination : all crops; amount limited to total commitment.'
  },
  {
    key: 'Lienholder Subordination : specific crops; open-ended amount',
    value: 'Lienholder Subordination : specific crops; open-ended amount.'
  },
  {
    key: 'Lienholder Subordination : specific crops; amount limited to total commitment',
    value: 'Lienholder Subordination : specific crops; amount limited to total commitment.'
  },
  {
    key:  'Lienholder Subordination : ARM collateral with conditions',
    value: 'Lienholder Subordination : ARM collateral with conditions.'
  }
];
