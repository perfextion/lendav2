import {
  Component,
  OnInit,
  ViewChild,
  ViewChildren,
  QueryList
} from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';

import { loan_model } from '@lenda/models/loanmodel';
import { Chevron, RefQuestions } from '@lenda/models/loan-response.model';
import { Page } from '@lenda/models/page.enum';
import { environment } from '@env/environment.prod';
import {
  Loansettings,
  Insurance_Policy_Settings
} from '@lenda/models/loansettings';

import { QuestionsComponent } from '@lenda/components/borrower/questions/questions.component';

import { MasterService } from '@lenda/master/master.service';
import { PubSubService } from '@lenda/services/pubsub.service';

@Component({
  selector: 'app-questionlist',
  templateUrl: './questionlist.component.html',
  styleUrls: ['./questionlist.component.scss']
})
export class QuestionlistComponent implements OnInit {
  public insurancePlanSettings: Insurance_Policy_Settings;
  public optedInsuranceOptions: Array<any> = [];
  public currentPageName: string = Page.insurance;
  public Chevron: typeof Chevron = Chevron;

  private loanmodel: loan_model;

  @ViewChild('mpci') mpciQuestions: QuestionsComponent;

  public hasData: boolean;

  @ViewChildren(QuestionsComponent)
  set questions(questions: QueryList<QuestionsComponent>) {
    try {
      if (questions) {
        let totalQuestions: Array<RefQuestions> = questions.reduce((a, b) => {
          a.push(...b.questions);
          return a;
        }, []);

        this.hasData = totalQuestions.some(a => !!a.FC_Response_Detail);
      }
    } catch {
      this.hasData = false;
    }
  }

  constructor(
    private localStorageService: LocalStorageService,
    private pubsub: PubSubService,
    public masterSvc: MasterService
  ) {}

  ngOnInit() {
    this.loanmodel = this.localStorageService.retrieve(environment.loankey);

    try {
      let obj = JSON.parse(this.loanmodel.LoanMaster.Loan_Settings) as Loansettings;
      if (obj.insurance_policy_Settings) {
        this.insurancePlanSettings = obj.insurance_policy_Settings;
      } else {
        this.insurancePlanSettings = new Insurance_Policy_Settings();
      }
    } catch {
      this.insurancePlanSettings = new Insurance_Policy_Settings();
    }

    this.getOptedInsuranceOptions();

    this.pubsub.subscribeToEvent().subscribe(event => {
      if (event.name === 'event.insurance.setting.updated') {
        this.insurancePlanSettings.policy_visibility_state[event.data.value] = event.data.checkboxEvent.checked;
        this.getOptedInsuranceOptions();
      }
    });
  }

  public getEnumValueForChevron(name) {
    if (name == 'Insurance_SELECT') {
      return this.Chevron.Insurance_ABC;
    }

    return this.Chevron[name];
  }

  public getOptedInsuranceOptions() {
    this.optedInsuranceOptions = [];

    try {
      Object.keys(this.insurancePlanSettings.policy_visibility_state).forEach(
        plan => {
          if (this.insurancePlanSettings.policy_visibility_state[plan]) {
            this.optedInsuranceOptions.push(plan);
          }
        }
      );
    } catch {
      this.optedInsuranceOptions = ['MPCI'];
    }
  }

  public get showAllQuestions() {
    if (this.mpciQuestions) {
      return this.mpciQuestions.showAllQuestion;
    }
    return false;
  }

  public getHeader(plan: string) {
    if (plan == 'ABC') {
      return 'SELECT';
    }
    return plan;
  }
}
