import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../../environments/environment';
import { loan_model, Loan_Association, Loan_Wfrp } from '../../../models/loanmodel';
import { PublishService } from '../../../services/publish.service';
import { Page } from '@lenda/models/page.enum';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';

@Component({
  selector: 'app-wfrp',
  templateUrl: './wfrp.component.html',
  styleUrls: ['./wfrp.component.scss']
})
export class WfrpComponent implements OnInit {

  public Premium: number = 0;
  public Level: number = 0;
  public Revenue: number = 0;
  public AIPs: Loan_Association[];
  public selectedAIP: number;
  public loanwfrp: Loan_Wfrp;
  constructor(private localstorage: LocalStorageService,private publishservic:PublishService,
    private loanCalculationWorker : LoancalculationWorker) { }

  ngOnInit() {

    this.AIPs = (this.localstorage.retrieve(environment.loankey) as loan_model).Association.filter(p => p.Assoc_Type_Code == "AIP");
    this.loanwfrp = (this.localstorage.retrieve(environment.loankey) as loan_model).Loanwfrp;
    this.loadvalues();
  }

  loadvalues() {
    this.Premium = this.loanwfrp.Premium;
    this.Revenue = this.loanwfrp.Approved_Revenue;
    this.selectedAIP = this.loanwfrp.Proposed_AIP;
    this.Level = this.loanwfrp.Level;
  }

  updatevalues() {

    this.loanwfrp.Premium = this.Premium;
    this.loanwfrp.Approved_Revenue = this.Revenue;
    this.loanwfrp.Proposed_AIP = this.selectedAIP
    this.loanwfrp.Level = this.Level
    let loan = this.localstorage.retrieve(environment.loankey) as loan_model;
    loan.Loanwfrp = this.loanwfrp;
    this.loanCalculationWorker.performcalculationonloanobject(loan,false);
    this.publishservic.enableSync(Page.insurance)

  }


}
