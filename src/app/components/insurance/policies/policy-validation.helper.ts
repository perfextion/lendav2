import {
  RefDataModel,
  Ref_Ins_Validation_Rule,
  Ref_Ins_Eligibility_Rule
} from '@lenda/models/ref-data-model';
import { InsuranceValidationData } from '@lenda/models/insurancemodel';

export class PolicyValidationHelper {
  static getInsuranceValidationData(refdata: RefDataModel) {
    let insValidationData: InsuranceValidationData = new InsuranceValidationData();

    //MPCI CAT
    let rule = getValidationRule('MPCI', 'CAT', refdata);
    insValidationData.MPCI_CAT_UNIT = getValues(rule, 'Unit');

    //MPCI_YP
    rule = getValidationRule('MPCI', 'YP', refdata);
    insValidationData.MPCI_YP_UNIT = getValues(rule, 'Unit');
    insValidationData.MPCI_YP_OPTION = getValues(rule, 'Option_Type');
    insValidationData.MPCI_YP_UPPERPER = getValues(rule, 'Upper_Pct');

    // MPCI_RP-HPE
    rule = getValidationRule('MPCI', 'RP-HPE', refdata);
    insValidationData.MPCI_RP_HPE_UNIT = getValues(rule, 'Unit');
    insValidationData.MPCI_RP_HPE_OPTION = getValues(rule, 'Option_Type');
    insValidationData.MPCI_RP_HPE_UPPERPER = getValues(rule, 'Upper_Pct');

    // MPCI_ARH
    rule = getValidationRule('MPCI', 'ARH', refdata);
    insValidationData.MPCI_ARH_UNIT = getValues(rule, 'Unit');
    insValidationData.MPCI_ARH_OPTION = getValues(rule, 'Option_Type');
    insValidationData.MPCI_ARH_UPPERPER = getValues(rule, 'Upper_Pct');
    insValidationData.MPCI_ARH_YIELDPER = getValues(rule, 'Yield_Pct');
    insValidationData.MPCI_ARH_PRICEPER = getValues(rule, 'Price_Pct');

    // MPCI_RP
    rule = getValidationRule('MPCI', 'RP', refdata);
    insValidationData.MPCI_RP_UNIT = getValues(rule, 'Unit');
    insValidationData.MPCI_RP_OPTION = getValues(rule, 'Option_Type');
    insValidationData.MPCI_RP_UPPERPER = getValues(rule, 'Upper_Pct');

    // WFRP
    rule = getValidationRule('WFRP', '', refdata);
    insValidationData.WFRP_UPPERPER = getValues(rule, 'Upper_Pct');

    // STAX
    rule = getValidationRule('STAX', '', refdata);
    insValidationData.STAX_UNIT = getValues(rule, 'Unit');
    insValidationData.STAX_UPPERPER = getValues(rule, 'Upper_Pct');
    insValidationData.STAX_LOWERPER = getValues(rule, 'Lower_Pct');
    insValidationData.STAX_YIELD = getValues(rule, 'Yield_Pct');
    insValidationData.STAX_RANGEMAX = rule.Range_Max || "20";

    //SCO
    rule = getValidationRule('SCO', '', refdata);
    insValidationData.SCO_UNIT = getValues(rule, 'Unit');

    //HMAX
    rule = getValidationRule('HMAX', 'STANDARD', refdata);
    insValidationData.HMAX_STANDARD_UPPERPER = getValues(rule, 'Upper_Pct');
    insValidationData.HMAX_STANDARD_LOWERPER = getValues(rule, 'Lower_Pct');
    insValidationData.HMAX_STANDARD_PRICE = getValues(rule, 'Price_Pct');

    rule = getValidationRule('HMAX', 'X1', refdata);
    insValidationData.HMAX_X1_UPPERPER = getValues(rule, 'Upper_Pct');
    insValidationData.HMAX_X1_LOWERPER = getValues(rule, 'Lower_Pct');
    insValidationData.HMAX_X1_PRICE = getValues(rule, 'Price_Pct');

    rule = getValidationRule('HMAX', 'MAXRP', refdata);
    insValidationData.HMAX_MAXRP_UPPERPER = getValues(rule, 'Upper_Pct');
    insValidationData.HMAX_MAXRP_LOWERPER = getValues(rule, 'Lower_Pct');
    insValidationData.HMAX_MAXRP_PRICE = getValues(rule, 'Price_Pct');

    //RAMP
    rule = getValidationRule('RAMP', 'RY', refdata);
    insValidationData.RAMP_RY_UNIT = getValues(rule, 'Unit');
    insValidationData.RAMP_RY_UPPERPER = getValues(rule, 'Upper_Pct');
    insValidationData.RAMP_RY_LOWERPER = getValues(rule, 'Lower_Pct');
    insValidationData.RAMP_RY_PRICE = getValues(rule, 'Price_Pct');
    insValidationData.RAMP_RY_LIABILITYMAX = getValues(
      rule,
      'Liability_Max_Upper_Pct_75'
    );
    insValidationData.RAMP_RY_LIABILITYMAX85 = getValues(
      rule,
      'Liability_Max_Upper_Pct_85'
    );

    rule = getValidationRule('RAMP', 'RR', refdata);
    insValidationData.RAMP_RR_UNIT = getValues(rule, 'Unit');
    insValidationData.RAMP_RR_UPPERPER = getValues(rule, 'Upper_Pct');
    insValidationData.RAMP_RR_LOWERPER = getValues(rule, 'Lower_Pct');
    insValidationData.RAMP_RR_PRICE = getValues(rule, 'Price_Pct');
    insValidationData.RAMP_RR_LIABILITYMAX = getValues(
      rule,
      'Liability_Max_Upper_Pct_75'
    );
    insValidationData.RAMP_RR_LIABILITYMAX85 = getValues(
      rule,
      'Liability_Max_Upper_Pct_85'
    );

    //ICE
    rule = getValidationRule('ICE', 'BY', refdata);
    insValidationData.ICE_BY_UNIT = getValues(rule, 'Unit');
    insValidationData.ICE_BY_YIELD = getValues(rule, 'Yield_Pct');

    rule = getValidationRule('ICE', 'BR', refdata);
    insValidationData.ICE_BR_UNIT = getValues(rule, 'Unit');
    insValidationData.ICE_BR_YIELD = getValues(rule, 'Yield_Pct');

    rule = getValidationRule('ICE', 'CY', refdata);
    insValidationData.ICE_CY_UNIT = getValues(rule, 'Unit');
    insValidationData.ICE_CY_YIELD = getValues(rule, 'Yield_Pct');

    rule = getValidationRule('ICE', 'CR', refdata);
    insValidationData.ICE_CR_UNIT = getValues(rule, 'Unit');
    insValidationData.ICE_CR_YIELD = getValues(rule, 'Yield_Pct');

    //ABC
    rule = getValidationRule('SELECT', 'REVENUE', refdata);
    insValidationData.SELECT_REVENUE_UNIT = getValues(rule, 'Unit');
    insValidationData.SELECT_REVENUE_UPPER_PER = getValues(rule, 'Upper_Pct');
    insValidationData.SELECT_REVENUE_LOWER_PER = getValues(rule, 'Lower_Pct');

    rule = getValidationRule('SELECT', 'YIELD', refdata);
    insValidationData.SELECT_YIELD_UNIT = getValues(rule, 'Unit');
    insValidationData.SELECT_YIELD_UPPER_PER = getValues(rule, 'Upper_Pct');
    insValidationData.SELECT_YIELD_LOWER_PER = getValues(rule, 'Lower_Pct');

    //PCI
    rule = getValidationRule('PCI', '', refdata);
    insValidationData.PCI_FCMC = getValues(rule, 'FCMC');

    //PCI_AIP

    //CROPHAIL
    rule = getValidationRule('CROP HAIL', 'BASIC', refdata);
    insValidationData.CROPHAIL_BASIC_DEDUCTMAX = getValues(
      rule,
      'Deduct_Percent_Max'
    );

    rule = getValidationRule('CROP HAIL', 'COMPANION', refdata);
    insValidationData.CROPHAIL_COMPANION_YIELD = getValues(rule, 'Yield_Pct');
    insValidationData.CROPHAIL_COMPANION_PRICE = getValues(rule, 'Price_Pct');
    insValidationData.CROPHAIL_COMPANION_DEDUCTMAX = getValues(
      rule,
      'Deduct_Percent_Max'
    );

    insValidationData.RAMP_Liability_Max_Range = 85;

    let eligibility_rule: Ref_Ins_Eligibility_Rule;

    eligibility_rule = getEligibilityRule('HMAX', 'STANDARD', refdata);
    insValidationData.HMAX_STANDARD_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.HMAX_STANDARD_ELLIGIBLESTATES = eligibility_rule.Eligible_States;
    insValidationData.HMAX_STANDARD_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('HMAX', 'X1', refdata);
    insValidationData.HMAX_X1_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.HMAX_X1_ELLIGIBLESTATES = eligibility_rule.Eligible_States;
    insValidationData.HMAX_X1_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('HMAX', 'MAXRP', refdata);
    insValidationData.HMAX_MAXRP_ELLIGIBLESTATES = eligibility_rule.Eligible_States;
    insValidationData.HMAX_MAXRP_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.HMAX_MAXRP_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('RAMP', 'RY', refdata);
    insValidationData.RAMP_RY_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.RAMP_RY_ELLIGIBLESTATES = eligibility_rule.Eligible_States;
    insValidationData.RAMP_RY_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('RAMP', 'RR', refdata);
    insValidationData.RAMP_RR_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.RAMP_RR_ELLIGIBLESTATES = eligibility_rule.Eligible_States;
    insValidationData.RAMP_RR_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('ICE', 'BY', refdata);
    insValidationData.ICE_BY_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.ICE_BY_ELLIGIBLESTATES = eligibility_rule.Eligible_States;
    insValidationData.ICE_BY_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('ICE', 'BR', refdata);
    insValidationData.ICE_BR_ELLIGIBLESTATES = eligibility_rule.Eligible_States;
    insValidationData.ICE_BR_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.ICE_BR_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('ICE', 'CY', refdata);
    insValidationData.ICE_CY_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.ICE_CY_ELLIGIBLESTATES = eligibility_rule.Eligible_States;
    insValidationData.ICE_CY_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('ICE', 'CR', refdata);
    insValidationData.ICE_CR_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.ICE_CR_ELLIGIBLESTATES = eligibility_rule.Eligible_States;
    insValidationData.ICE_CR_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('SELECT', 'YIELD', refdata);
    insValidationData.SELECT_Yield_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.SELECT_Yield_ELIGIBLE_STATES = eligibility_rule.Eligible_States;
    insValidationData.SELECT_Yield_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('SELECT', 'REVENUE', refdata);
    insValidationData.SELECT_REVENUE_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.SELECT_REVENUE_ELIGIBLE_STATES = eligibility_rule.Eligible_States;
    insValidationData.SELECT_REVENUE_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('PCI', '', refdata);
    insValidationData.PCI_ELLIGIBLESTATES = eligibility_rule.Eligible_States;
    insValidationData.PCI_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.PCI_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('CROPHAIL', 'BASIC', refdata);
    insValidationData.CROPHAIL_BASIC_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.CROPHAIL_BASIC_ELLIGIBLESTATES = eligibility_rule.Eligible_States;
    insValidationData.CROPHAIL_BASIC_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    eligibility_rule = getEligibilityRule('CROPHAIL', 'COMPANION', refdata);
    insValidationData.CROPHAIL_COMPANION_ELLIGIBLECROPS = eligibility_rule.Eligible_Crop_Codes;
    insValidationData.CROPHAIL_COMPANION_ELLIGIBLESTATES = eligibility_rule.Eligible_States;
    insValidationData.CROPHAIL_COMPANION_INELLIGIBLEPRACTICES = eligibility_rule.Ineligible_Crop_Practices;

    return insValidationData;
  }
}

function getValidationRule(
  plan: string,
  planType: string,
  refdata: RefDataModel
) {
  let rule: Ref_Ins_Validation_Rule;

  if (!planType || planType == '-') {
    rule = refdata.Ref_Ins_Validation_Rules.find(a => a.Ins_Plan == plan);
  } else {
    rule = refdata.Ref_Ins_Validation_Rules.find(
      a => a.Ins_Plan == plan && a.Ins_Plan_Type == planType
    );
  }

  return rule || new Ref_Ins_Validation_Rule();
}

/**
 * Gets `Insurance Eligibility rule` based on `Plan` and `Plan Type`
 * @param plan  Insurance Plan
 * @param planType  Insurance Plan Type
 * @param refdata Reference Data
 */
function getEligibilityRule(
  plan: string,
  planType: string,
  refdata: RefDataModel
) {
  let rule: Ref_Ins_Eligibility_Rule;

  if (!planType || planType == '-') {
    rule = refdata.Ref_Ins_Eligibility_Rules.find(a => a.Ins_Plan == plan);
  } else {
    rule = refdata.Ref_Ins_Eligibility_Rules.find(
      a => a.Ins_Plan == plan && a.Ins_Plan_Type == planType
    );
  }

  return rule || new Ref_Ins_Eligibility_Rule();
}

/**
 * Gets Value for `Key` from `Insurance Validation Rule`
 * @param rule Insurance Validation Rule
 * @param key Key
 */
function getValues(rule: Ref_Ins_Validation_Rule, key: string) {
  if (key == 'OPTION') {
    return getArray_Local(rule.Option_Type);
  } else if (key == 'UPPERPER') {
    return getArray_Local(rule.Upper_Pct);
  } else {
    return getArray_Local(rule[key]);
  }
}

function getArray_Local(value) {
  let arrValue: [];
  if (value) {
    value = String(value);
    arrValue = value.split(',');
  }
  return arrValue || [];
}
