export const insurancecolumnaliases = {
  Yield: 'Area Yield',
  UpperLimit_: 'Upper %',
  LowerLimit_: 'Lower %',
  YieldPct_: 'Yield Prot %',
  Premium: 'Premium',
  Deduct: 'Late Deduct Unit',
  PricePct_: 'Price Prot %',
  FCMC: 'FCMC Upper',
  Icc: 'ICC',
  Liability: 'Liability',
  Wind: 'Wind',
  DeductPct_: 'Deduct %'
};

export function getnameforheader(header: string, Section: string) {
  // if(Section=="STAX" && header=='YieldPct_' ){
  //  return "Prot Factor %";
  // }
  return insurancecolumnaliases[header];
}

export const propertyMapper = {
  HighlyRated: 'Eligible_Ind',
  Unit_MPCI: 'Ins_Unit_Type_Code',
  Subtype_MPCI: 'Ins_Plan_Type',
  Option_MPCI: 'Option',
  Level: 'Upper_Level',
  PriceProt_Pct: 'Price_Percent',
  Price: 'Price',
  Premium: 'Premium',
  YieldProt_Pct: 'Yield_Percent',
  Agent_Id: 'Agent_ID',
  Agency_Id: 'Agency_ID',
  Aip: 'AIP_ID',
  Unit: 'Ins_Unit_Type_Code',
  Subtype: 'Ins_Plan_Type',
  Option: 'Option',
  Yield_Pct: 'Yield_Percent', //keep it always on top of Yield
  Yield: 'Area_Yield',
  Upper_Limit: 'Upper_Level',
  Lower_Limit: 'Lower_Level',
  Late_Deduct: 'Late_Deductible',
  Deduct_Pct: 'Deductible_Percent',
  Deduct: 'Deductible_Units',
  FCMC: 'Custom1',
  Icc: 'Custom2',
  Liability: 'Liability_Percent',
  Wind: 'Actuarial_Wind',
  AgentId: 'Agent_ID',
  AgencyId: 'Agency_ID',
  ProposedAIP: 'AIP_ID',
  Prot_Factor: 'Custom1'
};

export function getApplicableProperty(propertyFinderstring: string) {
  if (propertyFinderstring == 'Price_Pct') {
    propertyFinderstring = 'PriceProt_Pct';
  }

  let _obj = Object.keys(propertyMapper)
    .find(p => propertyFinderstring.toLowerCase().includes(p.toLowerCase()))
    .toString();

  return propertyMapper[_obj];
}

export function getcolumnkey(value: string) {
  try {
    let _key = Object.keys(propertyMapper).find(
      k => propertyMapper[k].toLowerCase() == value.toLowerCase()
    );

    return _key;
  } catch {
    return '';
  }
}

