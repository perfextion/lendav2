import { Loan_Association } from './../../../models/loanmodel';
import { CountyidtonameFormatter, percentageFormatter, currencyFormatter, EmptyFormatter } from "@lenda/aggridformatters/valueformatters";
import { getApplicableProperty } from "./policiescolumnalias";
import { environment } from "@env/environment.prod";
import { Loansettings, Insurance_Policy_Settings } from "@lenda/models/loansettings";

export function getArray(value) {
  let _arrValue: [];
  if (value != null) {
    _arrValue = value.split(',');
  }
  return _arrValue;
}
export function countyidtonameFormattermapper(params) {
  return CountyidtonameFormatter(params, params.context.componentParent.refdata);
}
export function gettogglename(name: string) {
  if (name == "CROPHAIL") {
    return "CROP HAIL";
  } else {
    if (name == "CROP HAIL") {
      return "CROPHAIL";
    } else {
      return name;
    }
  }
}
export function getOptionClass(option: string) {
  if (option != 'WFRP') {
    option = this.gettogglename(option);
    let _haspolicies = this.loanmodel.LoanPolicies.find(p => p.Ins_Plan == option) != undefined;
    if (!_haspolicies) {
      return "lightgraytext";
    }
  } else {
    if (!this.Insurancetoggles.WFRP) {
      return "lightgraytext";
    }
  }
}
export function getcropnamebycropid(code: string, refdata) {
  let _cropname = refdata.CropList.find(p => p.Crop_Code == code).Crop_Name;
  return _cropname;
}

export function getcroppracticebyVcropid(id: number, refdata) {
  let _croppractice = refdata.CropList.find(p => p.Crop_And_Practice_ID == id).Irr_Prac_Code;
  return _croppractice;
}
export function lookupStateValue(key, refdata) {
  if (!isNaN(key) && parseInt(key, 10)) {
    // tslint:disable-next-line:radix
    return refdata.StateList.find(p => p.State_ID == parseInt(key)).State_Abbrev;
  } else {
    return '';
  }
}
export function lookupCountyValue(cropkey, refdata) {
  if (!isNaN(cropkey) && parseInt(cropkey, 10)) {
    // tslint:disable-next-line:radix
    return refdata.CountyList.find(p => p.County_ID == parseInt(cropkey)).County_Name;
  } else {
    return '';
  }
}
export function moveVerificationColumntolast(columnApi) {
  let _columngroups = columnApi.getAllDisplayedColumnGroups();
  let _allColumns = columnApi.getAllGridColumns();

  _columngroups.filter(p => p.children.length > 1).forEach(_group => {
    if (_group.originalColumnGroup.colGroupDef.headerName != 'MPCI') {
      let _item = _group.originalColumnGroup.children.filter(p => !p.colId.includes('_Verify')).slice(-1)[0];
      let _lastindex = _allColumns.lastIndexOf(_item);
      _group.originalColumnGroup.children.filter(p => p.colId.includes('_Verify')).forEach(element => {
        columnApi.moveColumn(element.colId, _lastindex + 1);
        // lastindex=lastindex+1;
      });
    }
  });
}
export function getlastfilledrow(idx: number, policy): any {
  try {
  if (idx == 0) {
    return null;
  }
  let _tempiterationflag = false;
  let _rowindex = idx;
  while (_tempiterationflag == false) {
    let row = this.rowData[_rowindex - 1];
    if (row != undefined) {
      if (policy == 'MPCI') {
        if (row[policy + "_Enabled"] == true) {
          _tempiterationflag = true;
        }
      } else {
        if (row[policy] == true) {
          _tempiterationflag = true;
        }
      }

    } else {
      _tempiterationflag = true;
    }

    _rowindex = _rowindex - 1;
  }
  return this.rowData[_rowindex];
} catch (ex) {
  return this.rowData[this.rowData.length - 1];
}
}
export function copyrow(from_row: any, to_farm: any, cols_to_copy: Array<any>, index: number) {

  cols_to_copy.filter(a => a.headerName != '').forEach(col => {
    try {
      let _field = col.field;
      let _reversefield = (getApplicableProperty(_field));
      this.loanobj.LoanPolicies[index][_reversefield] = from_row[_field];
      to_farm[_field] = from_row[_field];
    } catch {

    }
  });

  return to_farm;
}
export function  getColumnsToCopy(colDef: Array<any>, columnName: string, policy: string) {
  let _children = colDef.filter(p => p.headerName == policy)[0].children;
  let _index = _children.findIndex(a => a.field == columnName);
  return _children.slice(0, _index);
}
//UTILITY METHODS
  //this method is to get a subpolicy by a type associated with MPCI Policy
  export function CompareandgetSubPolicy(id, type, loanmodel) {
    try {
      let _mainPolicy = loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == id && p.ActionStatus != 3);
      //no lets find with Key
      let _subpolicy = loanmodel.LoanPolicies.find(p => p.Policy_Key == _mainPolicy.Policy_Key && p.Ins_Plan.toLowerCase() == type.toLowerCase() && p.HR_Exclusion_Ind == _mainPolicy.HR_Exclusion_Ind && p.Select_Ind == true);
      return _subpolicy;
    } catch {
      return null;
    }
  }
  export function CompareandgetSubPolicyCustom(id, type, loanmodel) {

    let _mainPolicy = loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == id && p.ActionStatus != 3 && p.IsCustom == true);
    //no lets find with Key
    let _subpolicy = loanmodel.LoanPolicies.find(p => p.Policy_Key == _mainPolicy.Policy_Key && p.IsCustom == true && p.Ins_Plan.toLowerCase() == type.toLowerCase() && p.HR_Exclusion_Ind == _mainPolicy.HR_Exclusion_Ind && p.Select_Ind == true);
    return _subpolicy;
  }
  export function  getsubtypeforinsurance(type: string) {
    if (type == "STAX") {
      return null;
    }
    if (type == "SCO") {
      return null;
    }
    if (type == "HMAX") {
      return { values: ['Standard', 'X1', 'MaxRP'] };
    }
    if (type == "RAMP") {
      return { values: ['RY', 'RR'] };
    }
    if (type == "ICE") {
      return { values: ['BY', 'BR', 'CY', 'CR'] };
    }
    if (type == "SELECT") {
      return { values: ['Yield', 'Revenue'] };
    }
    if (type == "PCI") {
      return null;
    }
    if (type == "CROPHAIL") {
      return { values: ['Basic', 'Companion'] };
    }
  }

  export function  getformatterforcolumn(columnname) {
    if (environment.isDebugModeActive) { console.time("getformatterforcolumn start Time"); }
    columnname = columnname.substr(0, columnname.lastIndexOf("_")).replace('%', '');

    let _formatter;
    switch (columnname) {
      case "Price_Pct":
        _formatter = function (params) {
          return percentageFormatter(params, 0);
        };
        break;

      case "Yield_Pct":
        _formatter = function (params) {
          return percentageFormatter(params, 0);
        };
        break;

      case "Price_Pct":
        _formatter = function (params) {
          return percentageFormatter(params, 0);
        };
        break;

      case "Premium":
        _formatter = function (params) {
          return currencyFormatter(params, 2);
        };
        break;

      case "Upper_Limit":
        _formatter = function (params) {
          return percentageFormatter(params, 0);
        };
        break;

      case "Prot_Factor":
        _formatter = function (params) {
          return percentageFormatter(params, 1);
        };
        break;

      case "Lower_Limit":
        _formatter = function (params) {
          return percentageFormatter(params, 0);
        };
        break;

      case "Liability":
        _formatter = function (params) {
          return currencyFormatter(params, 2);
        };
        break;

      case "Deduct":
        _formatter = _formatter = function (params) {
          return currencyFormatter(params, 2);
        };
        break;

      case "Deduct_amt":
        _formatter = _formatter = function (params) {
          return currencyFormatter(params, 2);
        };
        break;

      case "Deduct_pct":
        _formatter = function (params) {
          return percentageFormatter(params, 1);
        };
        break;

      case "Deduct_Pct":
        _formatter = function (params) {
          return percentageFormatter(params, 0);
        };
        break;

      case "FCMC":
        _formatter = _formatter = function (params) {
          return currencyFormatter(params, 2);
        };
        break;

      case "Icc":
        _formatter = _formatter = function (params) {
          return currencyFormatter(params, 2);
        };
        break;

      default:
        _formatter = EmptyFormatter;
        break;
    }
    if (environment.isDebugModeActive) { console.timeEnd("getformatterforcolumn start Time"); }
    return _formatter;
  }
export function getpossiblecombinationsforpolicy(value: string) {
  let _rendervalues = [];
  if (value == "HMAX") { //these values are Suffixed rather than prefixed
    //HMAX
    _rendervalues = ['Upper_Limit_HMAX', 'Lower_Limit_HMAX', 'Late_Deduct_HMAX', 'Premium_HMAX', 'Price_Pct_HMAX'];
    //HMAX
  }
  if (value == "SCO") { //these values are Suffixed rather than prefixed
    //HMAX
    _rendervalues = ['Upper_Limit_SCO', 'Yield_SCO', 'Premium_SCO'];
    //HMAX
  }
  if (value == "STAX") {
    _rendervalues = ['Upper_Limit_STAX', 'Lower_Limit_STAX', 'Yield_STAX', 'Prot_Factor_STAX', 'Yield_Pct_STAX', 'Premium_STAX'];
  }
  if (value == "RAMP") {
    _rendervalues = ['Upper_Limit_RAMP', 'Lower_Limit_RAMP', 'Price_Pct_RAMP', 'Liability_RAMP', 'Premium_RAMP'];
  }
  if (value == "ICE") {
    _rendervalues = ['Upper_Limit_ICE', 'Lower_Limit_ICE', 'Premium_ICE', 'Deduct_ICE', 'Yield_Pct_ICE', 'Price_Pct_ICE'];
  }
  if (value == "SELECT") {
    _rendervalues = ['Upper_Limit_SELECT', 'Lower_Limit_SELECT', 'Premium_SELECT'];
  }
  if (value == "PCI") {
    _rendervalues = ['FCMC_PCI', 'Premium_PCI', 'Icc_PCI'];
  }
  if (value == "CROPHAIL") {
    _rendervalues = ['Upper_Limit_CROPHAIL', 'Price_Pct_CROPHAIL', 'Liability_CROPHAIL', 'Deduct_Pct_CROPHAIL', 'Yield_Pct_CROPHAIL', 'Premium_CROPHAIL', 'Wind_CROPHAIL'];
  }

  return _rendervalues;
}
export function getunitsforinsurance(type: string) {
  if (type == "STAX") {
    return null;
  }
  if (type == "SCO") {
    return null;
  }
  if (type == "HMAX") {
    return { values: ['EU'] };
  }
  if (type == "RAMP") {
    return { values: ['EU', 'OU'] };
  }
  if (type == "ICE") {
    return { values: ['EU', 'OU'] };
  }
  if (type == "SELECT") {
    return { values: ['EU'] };
  }
  if (type == "PCI") {
    return { values: ['EU'] };
  }
  if (type == "CROPHAIL") {
    return { values: ['OU'] };
  }
}
export function getoptionsforinsurance(type: string) {
  if (type == "MPCI") {
    return { values: ['', 'P5', 'TA', 'YA', 'YE', 'SE'] };
  }
  if (type == "STAX") {
    return null;
  }
  if (type == "SCO") {
    return null;
  }
  if (type == "HMAX") {
    return null;
  }
  if (type == "RAMP") {
    return null;
  }
  if (type == "ICE") {
    return null;
  }
  if (type == "SELECT") {
    return null;
  }
  if (type == "PCI") {
    return null;
  }
  if (type == "CROPHAIL") {
    return null;
  }
}


export function getLoanSettingFromMyPreferences(objsettings: Loansettings) {
  if (objsettings.insurance_policy_Settings == undefined) {
    objsettings.insurance_policy_Settings = new Insurance_Policy_Settings();
  }
  let _item = objsettings.insurance_policy_Settings.policy_visibility_state;
  _item.ABC = this.insurancePlanSettings.isAbcEnabled;
  _item.CROPHAIL = this.insurancePlanSettings.isCropHailEnabled;
  _item.HMAX = this.insurancePlanSettings.isHmaxEnabled;
  _item.ICE = this.insurancePlanSettings.isIceEnabled;
  _item.PCI = this.insurancePlanSettings.isPciEnabled;
  _item.RAMP = this.insurancePlanSettings.isRampEnabled;
  _item.SCO = this.insurancePlanSettings.isScoEnabled;
  _item.STAX = this.insurancePlanSettings.isStaxEnabled;
  _item.HR = this.insurancePlanSettings.isHrEnabled;
  return objsettings;
}

export function assignAssocDetails(item: any) {
  let itemret = new Loan_Association();
  if (item.Contact) {
    itemret.Contact = item.Contact;

  } else {
    itemret.Contact = '';

  }

  if (item.Location) {
    itemret.Location = item.Location;

  } else {
    itemret.Location = '';

  }

  if (item.Phone) {
    itemret.Phone = item.Phone;

  } else {
    itemret.Phone = '';

  }

  if (item.Email) {
    itemret.Email = item.Email;

  } else {
    itemret.Email = '';

  }

  if (item.Amount) {
    itemret.Amount = item.Amount;

  } else {
    itemret.Amount = 0;

  }

  if (item.Preferred_Contact_Ind) {
    itemret.Preferred_Contact_Ind = item.Preferred_Contact_Ind;

  } else {
    itemret.Preferred_Contact_Ind = 0;

  }

  if (item.Validation_ID) {
    itemret.Validation_ID = item.Validation_ID;

  } else {
    itemret.Validation_ID = 0;

  }

  if (item.Exception_ID) {
    itemret.Exception_ID = item.Exception_ID;

  } else {
    itemret.Exception_ID = 0;

  }
return itemret;

}
