import { VerificationStages, TableId } from "@lenda/models/commonmodels";
import { cellclassmaker, CellType } from "@lenda/aggriddefinations/aggridoptions";
import { Loan_Policy } from "@lenda/models/insurancemodel";
import { getApplicableProperty } from "./policiescolumnalias";
import { MatDialog } from "@angular/material";
import { InsuranceverifyComponent } from "./insurance.verify.popup/insuranceverify.component";
declare function setverificationerrorallInsurance(array, keyword): any;

export class PoliciesVerificationHelper {

    //Verify Properties

    public verifyButtonStageMPCI = VerificationStages.Verify;
    public verifyButtonStageSTAX = VerificationStages.Verify;
    public verifyButtonStageSCO = VerificationStages.Verify;
    public verifyButtonStageRAMP = VerificationStages.Verify;
    public verifyButtonStageICE = VerificationStages.Verify;
    public verifyButtonStageCROPHAIL = VerificationStages.Verify;
    public verifyButtonStageSELECT = VerificationStages.Verify;
    public verifyButtonStageHMAX = VerificationStages.Verify;
    public verifyButtonStagePCI = VerificationStages.Verify;
    // END VERIFY PROPERTIES
    private columnApi: any;
    private gridApi: any;
    private parent: any;
    private dialog: MatDialog;
    constructor(parent, dialog) {
        this.parent = parent;
        this.dialog = dialog;
    }
    setapis(colapi, gridapi) {
        this.gridApi = gridapi;
        this.columnApi = colapi;
    }
    verifypolicy(headername: string) {
        //Open Popup Here
        ;
        const _dialogRef = this.dialog.open(InsuranceverifyComponent, {
            width: '600px',
            data: this.parent.loanmodel
          });
          _dialogRef.afterClosed().subscribe(_response => {


             if (_response != undefined && _response.Docresult == true) {
                //
                let _columns = this.columnApi.getAllColumns().filter(p => p.colId.includes("Verify") && p.colId.includes(headername));
                _columns.forEach(element => {
                    this.columnApi.setColumnVisible(element.colId, true);
                });
                this['verifyButtonStage' + headername] = VerificationStages.Check;
                this.gridApi.refreshCells({ force: true});
             } else {
                 this.resetVerification(headername);
             }

          });
        // refresh to take
        this.gridApi.redrawRows();


    }

    resetVerification(headername: string) {
        let _columns = this.columnApi.getAllColumns().filter(p => p.colId.includes("Verify") && p.colId.includes(headername));
        _columns.forEach(element => {
            this.columnApi.setColumnVisible(element.colId, false);
        });
        this['verifyButtonStage' + headername] = VerificationStages.Verify;
        this.gridApi.redrawRows();
    }

    processStage(headername: string) {
        this['verifyButtonStage' + headername] = VerificationStages.Check2;

         this.verifyColumnvalues(headername);
    }


    verificationdata = [];
    verifyColumnvalues(displayName) {
        //

        displayName = displayName.replace(' ', '');
        let _properties = this.getvaluesforpolicytoverify(displayName);
        this.verificationdata = [];
        this.parent.loanmodel.LoanPolicies.filter(p => p.ActionStatus != 3 && p.IsCustom != true && p.Ins_Plan == displayName).forEach((rec: Loan_Policy) => {
            let _isverified = true;
            _properties.forEach(element => {
                let _property = element;
                if (displayName == 'MPCI') {
                    _property = this.MPCIMapper(_property);
                   if (rec[_property] == null && rec[_property + "_Verify"] == 0) {

                   } else
                   {
                    if (rec[_property] != rec[_property + "_Verify"]) {
                        _isverified = false;
                        this.verificationdata.push('Ins_' + rec.Loan_Policy_ID + "_" + element + "_Verify");
                    }
                    else
                    {

                    }
                  }
                } else {

                    _property = getApplicableProperty(_property.substr(0, _property.lastIndexOf(displayName) - 1));
                    if (rec[_property] != rec[_property + "_Verify"]) {
                        _isverified = false;

                        let _parentpolicy = this.parent.loanmodel.LoanPolicies.find(p => p.Policy_Key == rec.Policy_Key && p.Ins_Plan == "MPCI");
                        this.verificationdata.push('Ins_' + _parentpolicy.Loan_Policy_ID + "_" + element + "_Verify");
                    }
                }

            });
            if (_isverified) {
            rec.isVerified = true;
            }
        });

         // this.localstorage.store(environment.verificationbase,this.verificationdataIIR)
             this.parent.saveloanobject(this.parent.loanmodel);

             this.seterrors();
    }


    seterrors() {
        this.gridApi.redrawRows();
        setverificationerrorallInsurance(
            this.verificationdata,
            TableId.Insurance
        );

    }
    getvaluesforpolicytoverify(value: string) {
        let _rendervalues = [];
        if (value == "MPCI") { //these values are Suffixed rather than prefixed
            _rendervalues = ['Level_MPCI', 'Subtype_MPCI', 'Unit_MPCI', 'Option_MPCI', 'YieldProt_Pct_MPCI', 'PriceProt_Pct_MPCI', 'Premium_MPCI'];
        }
        if (value == "STAX") { //these values are Suffixed rather than prefixed
            _rendervalues =  ['Unit_STAX', 'Yield_STAX', 'Upper_Limit_STAX', 'Lower_Limit_STAX', "Prot_Factor_STAX", 'Yield_Pct_STAX', 'Premium_STAX'];
        }
        if (value == "SCO") { //these values are Suffixed rather than prefixed
            _rendervalues =  ['Unit_SCO', 'Yield_SCO', 'Upper_Limit_SCO', 'Premium_SCO'];
        }
        if (value == "HMAX") { //these values are Suffixed rather than prefixed
            _rendervalues = ['Unit_HMAX', 'Upper_Limit_HMAX', 'Lower_Limit_HMAX', 'Late_Deduct_HMAX', 'Price_Pct_HMAX', 'Premium_HMAX'];
        }
        if (value == "RAMP") { //these values are Suffixed rather than prefixed
            _rendervalues = ['Unit_RAMP', 'Upper_Limit_RAMP', 'Lower_Limit_RAMP', 'Price_Pct_RAMP', 'Liability_RAMP', 'Premium_RAMP'];
        }
        if (value == "ICE") { //these values are Suffixed rather than prefixed
            _rendervalues = ['Unit_ICE', 'Upper_Limit_ICE', 'Lower_Limit_ICE', 'Yield_Pct_ICE', 'Premium_ICE'];
        }
        if (value == "SELECT") { //these values are Suffixed rather than prefixed
            _rendervalues = ['Unit_SELECT', 'Upper_Limit_SELECT', 'Lower_Limit_SELECT', 'Premium_SELECT'];
        }
        if (value == "PCI") { //these values are Suffixed rather than prefixed
            _rendervalues = ['Unit_PCI', 'FCMC_PCI', 'Icc_PCI', 'Premium_PCI'];
        }
        if (value == "CROPHAIL") {
            _rendervalues = ['Unit_CROPHAIL', 'Liability_CROPHAIL', 'Wind_CROPHAIL', 'Upper_Limit_CROPHAIL', 'Price_Pct_CROPHAIL', 'Yield_Pct_CROPHAIL', 'Deduct_Pct_CROPHAIL', 'Premium_CROPHAIL'];
        }
        return _rendervalues;
    }
    MPCIMapper(key: string) {
        switch (key) {
            case 'Level_MPCI':
                return 'Upper_Level';
                break;
            case 'Subtype_MPCI':
                return 'Ins_Plan_Type';
                break;
            case 'Unit_MPCI':
                return 'Ins_Unit_Type_Code';
                break;
            case 'Option_MPCI':
                return 'Option';
                break;
            case 'YieldProt_Pct_MPCI':
                return 'Yield_Percent';
                break;
            case 'PriceProt_Pct_MPCI':
                return 'Price_Percent';
                break;
            case 'Premium_MPCI':
                return 'Premium';
                break;
            default:
                break;
        }

    }


    MPCIColumnAgentforClass(Columnname: string, params: any) {

        let _isverify = false;

        if (this.verifyButtonStageMPCI == VerificationStages.Check) {
            _isverify = true;
        }

        let _Loan_Status = params.context.componentParent.Loan_Status;

        switch (Columnname) {
            case 'Subtype_MPCI':
                if (params.data.MPCI_Enabled == true) {
                    if (!_isverify) {
                        return cellclassmaker(CellType.Text, true, '', _Loan_Status);
                    } else {
                        if (params.data.Verified_MPCI != true) {
                        return cellclassmaker(CellType.Text, false, "blackcell", _Loan_Status);
                        } else {
                        return cellclassmaker(CellType.Text, true, '', _Loan_Status);
                        }
                    }
                } else {
                    return "grayedcell";
                }

            case 'Unit_MPCI':
                if (params.data.MPCI_Enabled == true) {
                    if (!_isverify) {
                        return cellclassmaker(CellType.Text, true, '', _Loan_Status);
                    } else {
                            if (params.data.Verified_MPCI != true) {
                            return cellclassmaker(CellType.Text, false, "blackcell", _Loan_Status);
                            } else {
                            return cellclassmaker(CellType.Text, true, '', _Loan_Status);
                            }
                        }
                } else {
                    return "grayedcell";
                }

            case 'Option_MPCI':
                let result = params.context.componentParent.getstatusofeditibilty(params);
                if (params.data.MPCI_Enabled == true) {
                    if (!_isverify) {
                        if (result) {
                            return cellclassmaker(CellType.Text, true, '', _Loan_Status);
                        } else {
                            return cellclassmaker(CellType.Text, false, '', _Loan_Status);
                        }
                    } else {
                        if (params.data.Verified_MPCI != true) {
                        return cellclassmaker(CellType.Text, false, "blackcell", _Loan_Status);
                        } else {
                        return cellclassmaker(CellType.Text, true, '', _Loan_Status);
                        }
                    }
                } else {
                    return "grayedcell";
                }

            case 'Level_MPCI':
                if (params.data.MPCI_Enabled == true) {

                    if (!_isverify) {
                        if (params.data.Subtype_MPCI != "CAT") {
                            return cellclassmaker(CellType.Integer, true, '', _Loan_Status);
                        } else {
                            return cellclassmaker(CellType.Integer, false, '', _Loan_Status);
                        }
                    } else {
                        if (params.data.Verified_MPCI != true) {
                        return cellclassmaker(CellType.Text, false, "blackcell", _Loan_Status);
                        } else {
                        return cellclassmaker(CellType.Text, true, '', _Loan_Status);
                        }
                    }
                } else {
                    return cellclassmaker(CellType.Integer, false, "grayedcell", _Loan_Status);
                }

            case 'YieldProt_Pct_MPCI':
                if (params.data.MPCI_Enabled == true) {
                    if (!_isverify) {
                        if (params.data.Subtype_MPCI == "ARH") {
                            return cellclassmaker(CellType.Integer, true, '', _Loan_Status);
                        } else {
                            return cellclassmaker(CellType.Integer, false, '', _Loan_Status);
                        }
                    } else {
                        if (params.data.Verified_MPCI != true) {
                        return cellclassmaker(CellType.Text, false, "blackcell", _Loan_Status);
                        } else {
                        return cellclassmaker(CellType.Text, true, '', _Loan_Status);
                        }
                    }
                } else {
                    return cellclassmaker(CellType.Integer, false, "grayedcell", _Loan_Status);
                }

            case 'PriceProt_Pct_MPCI':
                if (params.data.MPCI_Enabled == true) {
                    if (!_isverify) {
                        if (params.data.Subtype_MPCI == "ARH") {
                            return cellclassmaker(CellType.Integer, true, '', _Loan_Status);
                        } else {
                            return cellclassmaker(CellType.Integer, false, '', _Loan_Status);
                        }
                    } else {
                        if (params.data.Verified_MPCI != true) {
                        return cellclassmaker(CellType.Text, false, "blackcell", _Loan_Status);
                        } else {
                        return cellclassmaker(CellType.Text, true, '', _Loan_Status);
                        }
                    }
                } else {
                    return cellclassmaker(CellType.Integer, false, "grayedcell", _Loan_Status);
                }

            case 'Premium_MPCI':
                if (params.data.MPCI_Enabled == true) {
                    if (!_isverify) {
                        return cellclassmaker(CellType.Integer, true, '', _Loan_Status);
                    } else {
                            if (params.data.Verified_MPCI != true) {
                            return cellclassmaker(CellType.Text, false, "blackcell", _Loan_Status);
                            } else {
                            return cellclassmaker(CellType.Text, true, '', _Loan_Status);
                            }
                        }
                } else {
                    return "grayedcell";
                }



            default:
                break;
        }
    }

    MPCIColumnAgentforEdit(Columnname: string, params: any) {

        let _isverify = false;
        if (this.verifyButtonStageMPCI == VerificationStages.Check) {
            return false;
        }
        switch (Columnname) {
            case 'Subtype_MPCI':
                if (params.data.MPCI_Enabled == true) {
                    return true;
                } else {
                    return false;
                }

            case 'Unit_MPCI':
                if (params.data.MPCI_Enabled == true) {
                    return true;
                } else {
                    return false;
                }

            case 'Option_MPCI':
                let result = params.context.componentParent.getstatusofeditibilty(params);
                if (params.data.MPCI_Enabled == true) {
                    if (!_isverify) {
                        if (result) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }

            case 'Level_MPCI':
                if (params.data.MPCI_Enabled == true) {
                    if (!_isverify) {
                        if (params.data.Subtype_MPCI != "CAT") {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }

            case 'YieldProt_Pct_MPCI':
                if (params.data.MPCI_Enabled == true) {
                    if (!_isverify) {
                        if (params.data.Subtype_MPCI == "ARH") {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }

            case 'PriceProt_Pct_MPCI':
                if (params.data.MPCI_Enabled == true) {
                    if (!_isverify) {
                        if (params.data.Subtype_MPCI == "ARH") {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }

            case 'Premium_MPCI':
                if (params.data.MPCI_Enabled == true) {
                    if (!_isverify) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }



            default:
                break;
        }
    }


}
