import { count } from 'rxjs/operators';
import { Component, OnInit, Inject, NgZone, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DisbursepopupComponent } from '@lenda/aggridcolumns/disbursepopup/disbursepopup.component';
import { LoanConditionWorkerService } from '@lenda/Workers/calculations/loanconditionworker.service';
import { loan_model } from '@lenda/models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment';

@Component({
  selector: 'app-insuranceverify',
  templateUrl: './insuranceverify.component.html',
  styleUrls: ['./insuranceverify.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InsuranceverifyComponent implements OnInit {

  private loandata: loan_model;
  public Documents = [];

  constructor(
    public dialogRef: MatDialogRef<InsuranceverifyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ngZone: NgZone,
    private conditionWorker: LoanConditionWorkerService,
    private localstorage: LocalStorageService
  ) {
    this.loandata = data;
    let userid = this.localstorage.retrieve(environment.uid);

    this.loandata.LoanConditions = this.conditionWorker.preparePolicyVerificationConditions(this.loandata, userid);
    if (this.loandata.Association.filter(p => p.Assoc_Type_Code == "AIP").length > 0) {
    this.Documents = this.loandata.Loan_Documents.filter(p => p.Document_Type_ID == 75);
    } else {
      this.Documents = [];
     }

  }

  // tslint:disable-next-line: no-use-before-declare
  public popmodel: verifymodel_ins = new verifymodel_ins();

  ngOnInit() {
    this.popmodel.Docresult = false;
  }

  onNoClick() {
    this.popmodel.Docresult = false;
    this.dialogRef.close(this.popmodel);
  }

  onyesClick() {
    this.popmodel.Docresult = true;
    this.dialogRef.close(this.popmodel);
  }


}


export class verifymodel_ins {
  public DocID: string;
  public Docresult: boolean;
}

export class Loan_Verification_Document_Details {
  public DocID: string;
  public DocDate: string;
}
