import { distinctUntilChanged, debounceTime } from 'rxjs/operators';
import { Loan_Association, AssociationTypeCode } from './../../../models/loanmodel';
//Refrerences
import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';
import { environment } from "@env/environment.prod";
import { CropAutoCompleteEditor } from '@lenda/aggridcolumns/crop-auto-complete-editor/crop-auto-complete-editor.component';
import { DeleteButtonRenderer } from '@lenda/aggridcolumns/deletebuttoncolumn';
import { verifyPrimaryHeadergroup } from '@lenda/aggridcolumns/headerComponents/verifyprimaryHeadergroup';
import { CheckboxCellComponent } from '@lenda/aggridfilters/checkbox';
import { getApplicableProperty } from '@lenda/components/insurance/policies/policiescolumnalias';
import { MasterService } from '@lenda/master/master.service';
import { LoanStatus, loan_model } from '@lenda/models/loanmodel';
import { Insurance_Policy_Settings, Insurance_Policy_Tabs, Loansettings, Loan_Key_Visibilty } from '@lenda/models/loansettings';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';
import { Chevrons, LoanSettings, TableAddress, TableState } from '@lenda/preferences/models/loan-settings/index.model';
import { InsurancePlanSettings } from "@lenda/preferences/models/loan-settings/insurance-plan.model";
import { SettingsService } from "@lenda/preferences/settings/settings.service";
import { LoanAuditTrailService } from '@lenda/services/audit-trail.service';
import { Get_Policy_Key, RKS_RMAFind } from '@lenda/services/common-utils';
import { DataService } from '@lenda/services/data.service';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { PubSubService } from '@lenda/services/pubsub.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { lookupUOM } from '@lenda/Workers/utility/aggrid/cropboxes';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';
import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import { ChipsListEditor } from '../../../aggridcolumns/chipscelleditor';
import { AgGridTooltipComponent } from '../../../aggridcolumns/tooltip/tooltip.component';
import { calculatecolumnwidthsins, cellclassmaker, CellType, headerclassmaker, ischeckboxeditable, ischeckboxeditablewfrp, isgrideditable, isgrideditableinsurancepoilcytable } from '../../../aggriddefinations/aggridoptions';
import { BooleanEditor } from '../../../aggridfilters/booleanaggrid.';
import { EmptyEditor } from '../../../aggridfilters/emptybox';
import { NumericEditor } from '../../../aggridfilters/numericaggrid';
import { SelectEditor } from '../../../aggridfilters/selectbox';
import { BudgetFormatter, CountyidtonameFormatter, currencyFormatter, EmptyFormatter, percentageFormatter, StateidtonameFormatter } from '../../../aggridformatters/valueformatters';
import { AlertifyService } from '../../../alertify/alertify.service';
import { Chevronkeys, errormodel, VerificationStages } from '../../../models/commonmodels';
import { Loan_Policy } from '../../../models/insurancemodel';
import { Sync_Status } from '../../../models/syncstatusmodel';
import { LoanApiService } from '../../../services/loan/loanapi.service';
import { LoggingService } from '../../../services/Logs/logging.service';
import { PublishService } from '../../../services/publish.service';
import { ValidationService } from '../../../Workers/calculations/validation.service';
import { numberValueSetter } from '../../../Workers/utility/aggrid/numericboxes';
import { extractStateValues, getfilteredcountiesAPH, lookupCountyValueFromPassedRefData, lookupStateCodeFromPassedRefObject, Statevaluesetter } from '../../../Workers/utility/aggrid/stateandcountyboxes';
import { PoliciesVerificationHelper } from './policies.verify';
import { Page } from '@lenda/models/page.enum';
import { getlastfilledrow, getColumnsToCopy, copyrow, gettogglename, lookupStateValue, lookupCountyValue, CompareandgetSubPolicy, getcropnamebycropid, CompareandgetSubPolicyCustom, moveVerificationColumntolast, getsubtypeforinsurance, getoptionsforinsurance, getunitsforinsurance, getLoanSettingFromMyPreferences, getpossiblecombinationsforpolicy, getformatterforcolumn, assignAssocDetails } from './policies.helpers';
import { PolicyValidationHelper } from './policy-validation.helper';

declare function setmodifiedallIns(array, keyword): any;

//Refrerences END here

@Component({
  selector: 'app-policies',
  templateUrl: './policies.component.html',
  styleUrls: ['./policies.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PoliciesComponent implements OnInit, OnDestroy {
  // Local Objects and variables
  @Input() insurancePlanSettings: InsurancePlanSettings;
  public pinnedBottomRowData;
  public hrInclusion: boolean = false;
  private insuranceValidationData;
  private subscription: ISubscription;
  public syncInsuranceStatus: Sync_Status;
  public wfrpEnabled = true;
  private units: any[] = [
    { key: 'EU', value: 'EU' },
    { key: 'BU', value: 'BU' },
    { key: 'EP', value: 'EP' },
    { key: 'OU', value: 'OU' }
  ];
  public errorlist: Array<errormodel> = new Array<errormodel>();
  public currentPageName: Page = Page.insurance;
  public loankeySettings: Loan_Key_Visibilty;
  public columnsDisplaySettings: ColumnsDisplaySettings = new ColumnsDisplaySettings();

  //Verification Object
  public verificationHelper: PoliciesVerificationHelper;
  public showwfrpmessage = false;
  public methodfromcheckboxinvoked = false;
  //Properties
  private refdata: RefDataModel;
  private gridApi;
  private columnApi;
  rowData = [];
  public frameworkcomponents;
  public context;
  public components;
  public loanobj: loan_model;
  public rowClassRules;
  public Insurancetoggles = { MPCI: true, STAX: false, SCO: false, HMAX: false, RAMP: false, ICE: false, SELECT: false, PCI: false, CROPHAIL: false, WFRP: false };

  public defaultColDef = {};
  style = {
    marginTop: '10px',
    width: '98%'
  };
  public loanmodel: loan_model = null;
  public columnDefs: any[];
  private agents = [];
  private ProposedAips;
  private agencies = [];
  private sortstrings = ["Custom", "FC_StateCode", "FC_County"];
  private Loankeys = ["Croptype", 'Practice'];
  public insurancekeys: any[] = ["State_ID", "County_ID", "CropName", "Croptype", "Practice", "CropingPractice"];

  //Last Edited Row
  lastEditedRowid = "";
  // Local Objects declarations end here
  retrieveErrors() {
    this.errorlist = (this.localstorage.retrieve(environment.errorbase) as Array<errormodel>).filter(p => p.chevron == Chevronkeys.InsurancePolices);
    if (environment.isDebugModeActive) {
      console.time('Highlight Validation - InsuranceValidation');
    }
    this.errorlist = (this.localstorage.retrieve(environment.errorbase) as Array<errormodel>).filter(p => p.chevron == Chevronkeys.InsurancePolices);
    this.validationservice.highlighErrorCells(this.errorlist, 'Table_Policies');

    if (environment.isDebugModeActive) {
      console.timeEnd('Highlight Validation - InsuranceValidation');
    }
  }
  planTypes(): any {
    return { values: ['MPCI', 'HMAX', 'STAX'] };
  }
  //validations
  toggleHRinclusion($event) {
    if (environment.isDebugModeActive) {
      console.time('toggleHRinclusion start Time');
    }
    try {
      if ($event.checked) {
        let _columns = this.columnApi.getAllColumns().find(p => p.colDef.headerName == "HR");
        this.columnApi.setColumnVisible(_columns.colId, true);
      } else {
        let _columns = this.columnApi.getAllColumns().find(p => p.colDef.headerName == "HR");
        this.columnApi.setColumnVisible(_columns.colId, false);
        this.unCheckHRData();
      }
      let _obj = (this.loanmodel.LoanMaster.Loan_Settings == "" ? new Loansettings() : JSON.parse(this.loanmodel.LoanMaster.Loan_Settings) as Loansettings);
      this.hrInclusion = $event.checked;
      _obj.HrInclusion = $event.checked;
      this.loanmodel.LoanMaster.Loan_Settings = JSON.stringify(_obj);
      //Uncheck everything in HR Rows

      this.getgriddata(false);

      window.localStorage.setItem("ng2-webstorage|currentselectedloan", JSON.stringify(this.loanmodel));
    } catch (ex) {

    }
    if (environment.isDebugModeActive) {
      console.timeEnd('toggleHRinclusion start Time');
    }
  }

  unCheckHRData() {
    if (environment.isDebugModeActive) {
      console.time('unCheckHRData start Time');
    }
    let _keys = [];
    this.loanmodel.LoanPolicies.filter(p => p.HR_Exclusion_Ind == true && p.Ins_Plan == 'MPCI').forEach(policy => {
      policy.Select_Ind = false;
      _keys.push(policy.Policy_Key);
    });
    try {
      _.remove(this.loanmodel.LoanPolicies.filter(p => p.Ins_Plan != 'MPCI'), (p => _keys.includes(p.Policy_Key)));
    } catch {

    }
    if (environment.isDebugModeActive) {
      console.timeEnd('unCheckHRData start Time');
    }
  }

  onFilterChanged(value) {
    this.gridApi.setQuickFilter(value);
  }
  //Faster Formatters
  onBtExport() {
    const _params = {
      skipHeader: false,
      columnGroups: true,
      skipFooters: false,
      skipGroups: false,
      skipPinnedTop: false,
      skipPinnedBottom: false,
      allColumns: true,
      fileName: `Insurance_Policies_${this.loanobj.Loan_Full_ID}_${this.dtss.getTimeStamp()}.xlxs`,
      sheetName: 'Sheet 1'
    };
    // Export Data to Excel file
    this.gridApi.exportDataAsExcel(_params);
  }


  // async checkAIPStatusofPolicies() {
  //   let policies = _.uniq(this.loanmodel.LoanPolicies.map(p => p.Ins_Plan));
  //   policies.forEach( element => {
  //     ;
  //     let num = this.checkforAIP(element);
  //     if (num == -2) {
  //       // wait(5000);
  //     }
  //    ;
  //   });
  //   ;
  //   return true;
  // }
  declarecoldefs() {
    if (environment.isDebugModeActive) {
      console.time("declarecoldefs start Time");
    }
    this.defaultColDef = {
      suppressMovable: true,
      // menuTabs: [],
      pinnedRowCellRenderer: function (params) {

        if (params.colDef.field.includes('wfrp')) {
          if (params.colDef.field == "wfrp_aip") {
            try {
              if (params.value == 0) {
                return "";
              }
              return (params.context.componentParent.loanmodel as loan_model).Association.find(p => p.Assoc_ID == params.value).Assoc_Name;
            } catch (ex) {
              return "";
            }
          }
          if (params.colDef.field == "wfrp_Agency_Id") {
            try {
              if (params.value == 0) {
                return "";
              }
              return (params.context.componentParent.loanmodel as loan_model).Association.find(p => p.Assoc_ID == params.value).Assoc_Name;
            } catch (ex) {
              return "";
            }
          }
          if (params.colDef.field == "wfrp_Agent_Id") {
            try {
              if (params.value == 0) {
                return "";
              }
              return (params.context.componentParent.loanmodel as loan_model).Association.find(p => p.Assoc_ID == params.value).Assoc_Name;
            } catch (ex) {
              return "";
            }
          }


          return params.value == undefined ? "" : params.value;
        } else {
          if (params.colDef.field == 'State_ID') {
            return "All";
          }
          if (params.context.componentParent.insurancekeys.includes(params.colDef.pickfield)) {
            return "All";
          } else {
            return "";
          }
        }
      }
    };
    let mpcivisib = !this.Insurancetoggles.MPCI;
    this.columnDefs = [
      {
        headerName: 'State', field: 'State_ID', pickfield: 'State_ID', pinned: "left",
        cellClass: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          if (params.data.Custom == true) {
            return cellclassmaker(CellType.Text, true, '', Loan_Status);
          }
        }, cellRenderer: function (params) {
          if (params.data.Custom == true) {
            return params.valueFormatted + '<i class="ins-pol-star tiny material-icons icon-yellow">add</i>';
          } else {
            return params.valueFormatted;
          }
        },
        cellEditor: 'selectEditor',
        cellEditorParams: {
          values: extractStateValues(this.refdata.StateList)
        },
        valueFormatter: StateidtonameFormatter,
        valueSetter: Statevaluesetter,
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          return isgrideditable((params.data.Custom), Loan_Status);
        },
        width: 90
      },
      {
        headerName: 'County', field: 'FC_County', pickfield: 'FC_County', pinned: "left",
        cellClass: function (params) {
          if (params.data.Custom == true) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return cellclassmaker(CellType.Text, true, '', Loan_Status);
          }
        },
        cellEditor: 'selectEditor',
        cellEditorParams: getfilteredcountiesAPH,
        valueFormatter: function (params) {
          return params.value;
        },
        valueSetter: function (params) {
          params.data.County_ID = parseInt(params.newValue, 10);
          try {
            params.data.FC_County =
              params.context.componentParent.refdata.CountyList.find(p => p.County_ID == parseInt(params.newValue, 10)).County_Name;
          } catch (ex) {
            params.data.FC_County = null;
          }
        },
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          return isgrideditable((params.data.Custom), Loan_Status);
        },
        width: 90,
        minWidth: 90
      },
      {
        headerName: 'Crop',
        field: 'CropName', pickfield: 'CropName', pinned: "left",
        tooltip: params => params.data['CropName'],
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          return isgrideditable((params.data.Custom), Loan_Status);
        }, width: 90,
        cellClass: function (params) {
          if (params.data.Custom == true) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return cellclassmaker(CellType.Text, true, '', Loan_Status);
          }
        },
        minWidth: 90,
        maxWidth: 90,
        cellEditor: 'autocompleteEditor',
        valueFormatter: params => {
          return params.data['Crop_Name'];
        },
      },
      {
        headerName: 'Crop Type',
        headerTooltip: 'Crop Type',
        width: 90, pinned: "left",
        field: 'Croptype', pickfield: 'Croptype',
        tooltip: params => params.data['Croptype'],
        hide: !this.columnsDisplaySettings.cropType
      },
      {
        headerName: 'Irr Prac',
        headerTooltip: 'Irr Practice',
        width: 90, pinned: "left",
        field: 'Practice', pickfield: 'Practice',
        tooltip: params => params.data['Practice'],
        hide: !this.columnsDisplaySettings.irrPrac,
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          return isgrideditable((params.data.Custom), Loan_Status);
        }, cellEditor: 'selectEditor',
        cellEditorParams: {
          values: [{ key: 'IRR', value: 'IRR' }, { key: 'NIR', value: 'NIR' }]
        },

        cellClass: function (params) {
          if (params.data.Custom == true) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return cellclassmaker(CellType.Text, true, '', Loan_Status);
          }
        }
      },
      {
        headerName: 'HR',
        headerTooltip: 'Highly Rated',
        width: 90, pinned: "left",
        //cellRenderer: 'columnTooltip',
        field: 'HighlyRated', pickfield: 'HighlyRated',
        editable: false,
        hide: true,
        cellClass: function (params) {
          if (params.data.Rated == 0) {
            return 'grayedcell';
          } else {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return cellclassmaker(CellType.Text, false, '', Loan_Status);
          }
        },
        cellEditor: "booleaneditor",
        valueFormatter: function (params) {

          if (params.data.Rated == 0) {
            return "";
          } else {
            return "HR";
          }
        }
      },
      {
        headerName: 'MPCI', headerGroupComponentFramework: verifyPrimaryHeadergroup,
        headerGroupComponentParams: {
          menuIcon: "verified_user", isverify: true
        },
        children: [
          {
            headerName: '', hide: mpcivisib, width: 40, pickfield: "MPCI_Enabled", field: 'MPCI_Enabled', cellRenderer: "checkbox", suppressSorting: true,
            cellRendererParams: { edit: ischeckboxeditable() }
          },
          {
            headerName: 'Agency', suppressSorting: true,
            field: 'Agency_Id_MPCI',
            pickfield: 'AgencyId_MPCI',
            hide: mpcivisib,
            cellClass: function (params) {
              if (params.data.MPCI_Enabled == true) {
                return 'editable-color';
              } else {
                return "grayedcell";
              }
            },
            //cellRenderer: 'columnTooltip',
            headerTooltip: 'Agency',
            width: 90,
            editable: function (params) {
              if (params.data.MPCI_Enabled == true) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                if (Loan_Status == 'W') {
                  params.colDef.cellEditorParams = params.context.componentParent.getAgencies();
                }
                return isgrideditableinsurancepoilcytable(params, true, Loan_Status);
              } else { return false; }

            },
            cellEditor: "selectEditor",
            cellEditorParams: this.getAgencies(),
            valueFormatter: function (params) {
              try {
                if (params.value == 0 || params.data.MPCI_Enabled != true) {
                  return "";
                }
                return (params.context.componentParent.loanmodel as loan_model).Association.find(p => p.Assoc_ID == params.value).Assoc_Name;
              } catch (ex) {
                return "";
              }
            }
          },
          {
            headerName: 'AIP', suppressSorting: true,
            field: 'ProposedAIP_MPCI',
            pickfield: 'ProposedAIP_MPCI',
            headerTooltip: 'ProposedAIP',
            hide: mpcivisib,
            //cellRenderer: 'columnTooltip',
            cellClass: function (params) {
              if (params.data.MPCI_Enabled == true) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                return cellclassmaker(CellType.Text, true, '', Loan_Status);
              } else {
                return "grayedcell";
              }
            },
            editable: function (params) {
              if (params.data.MPCI_Enabled == true) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                if (Loan_Status == 'W') {
                  params.colDef.cellEditorParams = params.context.componentParent.getAIPs();
                }
                return isgrideditableinsurancepoilcytable(params, true, Loan_Status);
              } else {
                return false;
              }
            },
            width: 90,
            cellEditor: "selectEditor",
            cellEditorParams: this.getAIPs(),
            valueFormatter: function (params) {
              try {
                if (params.value == 0 || params.data.MPCI_Enabled != true) {
                  return "";
                }
                return (params.context.componentParent.loanmodel as loan_model).Association.find(p => p.Assoc_ID == params.value).Assoc_Name;
              } catch (ex) {
                return "";
              }
            }
          },
          {
            headerName: 'Plan Type', suppressSorting: true,
            headerTooltip: 'Type',
            field: 'Subtype_MPCI',
            hide: mpcivisib,
            pickfield: 'Subtype_MPCI',
            valueFormatter: (params) => {
              try {
                if (params.data.MPCI_Enabled != true) {
                  return "";
                } else {
                  return params.value;
                }
              } catch {
                return params.value;
              }
            },
            //cellRenderer: 'columnTooltip',
            cellClass: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforClass('Subtype_MPCI', params);
            },
            editable: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforEdit('Subtype_MPCI', params);
            },
            width: 90,
            cellEditor: "agSelectCellEditor",
            cellEditorParams: PoliciesComponent.GetMPCIPlanSubType('MPCI')
          },
          {
            headerName: 'Unit', suppressSorting: true,
            headerTooltip: 'Unit',
            field: 'Unit_MPCI',
            hide: mpcivisib,
            pickfield: 'Unit_MPCI',
            width: 90,
            minWidth: 90,

            // //cellRenderer: 'columnTooltip',
            cellClass: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforClass('Unit_MPCI', params);
            },
            editable: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforEdit('Unit_MPCI', params);
            },
            cellEditor: "selectEditor",
            cellEditorParams: {
              values: this.units
            },
            valueFormatter: (params) => {
              if (params.data.MPCI_Enabled != true) {
                return "";
              }
              const selected = params.context.componentParent.units.filter(u => u.key === (params.value));
              return selected.length > 0 ? selected[0].value : null;
            }
          },
          {
            headerName: 'Option', suppressSorting: true,
            field: 'Option_MPCI',
            pickfield: 'Option_MPCI',
            Section: "MPCI",
            hide: mpcivisib,
            //cellRenderer: 'columnTooltip',
            headerTooltip: 'Option',
            width: 90,
            valueFormatter: (params) => {
              try {
                let result = params.context.componentParent.getstatusofeditibilty(params);
                if (params.data.MPCI_Enabled == true && result) {
                  return params.value;
                } else {
                  return "";
                }
              } catch {
                return params.value;
              }
            },
            editable: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforEdit('Option_MPCI', params);
            },
            cellEditorSelector: function () {

              return {
                component: "agSelectCellEditor"
              };


            },
            cellEditorParams: getoptionsforinsurance("MPCI"),
            cellClass: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforClass('Option_MPCI', params);
            }
          },
          {
            headerName: 'Level %', suppressSorting: true,
            headerTooltip: 'Level Percent',
            field: 'Level_MPCI',
            pickfield: 'Level_MPCI',
            hide: mpcivisib,
            width: 90,
            headerClass: headerclassmaker(CellType.Integer, ""),
            cellClass: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforClass('Level_MPCI', params);
            },
            editable: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforEdit('Level_MPCI', params);
            },
            cellEditor: "numericCellEditor",
            cellEditorParams: {
              decimals: 1
            },
            valueFormatter: function (params) {
              if (params.data.MPCI_Enabled == true) {
                return percentageFormatter(params, 0);
              } else {
                return "";
              }
            },
            valueSetter: function (params) {
              numberValueSetter(params);
              if (params.newValue) {
                params.data['Rentperc'] = 100 - parseFloat(params.newValue);
              }
              return true;
            }
          },
          {
            headerName: 'Price', suppressSorting: true,
            headerTooltip: 'Price',
            field: 'Price_MPCI',
            hide: mpcivisib,
            pickfield: 'Price_MPCI',
            width: 90,
            //cellRenderer: 'columnTooltip',
            headerClass: headerclassmaker(CellType.Integer, ""),
            cellClass: function (params) {
              if (params.data.MPCI_Enabled == true) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                return cellclassmaker(CellType.Integer, false, '', Loan_Status);
              } else {
                return "grayedcell";
              }
            },
            editable: function (params) {
              if (params.data.MPCI_Enabled == true) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                return isgrideditableinsurancepoilcytable(params, false, Loan_Status);
              } else {
                return false;
              }
            },
            cellEditor: "numericCellEditor",
            cellEditorParams: {
              decimals: 4
            },
            valueSetter: numberValueSetter,
            valueFormatter: function (params) {
              if (params.data.MPCI_Enabled == true) {
                return currencyFormatter(params, 4);
              } else {
                return "";
              }

            },
          },
          {
            headerName: 'Yield %', suppressSorting: true,
            headerTooltip: 'Yield %',
            field: 'YieldProt_Pct_MPCI',
            pickfield: 'YieldProt_Pct_MPCI',
            hide: mpcivisib,
            width: 90,
            headerClass: headerclassmaker(CellType.Integer, ""),
            cellClass: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforClass('YieldProt_Pct_MPCI', params);
            },
            editable: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforEdit('YieldProt_Pct_MPCI', params);
            },
            cellEditorParams: {
              decimals: 2
            },
            cellEditorSelector: function (params) {
              if (params.data.Subtype_MPCI != "ARH") {
                return {
                  component: 'emptyeditor'
                };
              } else {
                return {
                  component: 'numericCellEditor'
                };
              }

            },
            valueSetter: numberValueSetter,
            valueFormatter: function (params) {
              if (params.data.MPCI_Enabled != true) {
                return "";
              } else {
                return percentageFormatter(params, 0);
              }
            }
          },
          {
            headerName: 'Price %', suppressSorting: true,
            headerTooltip: 'Price %',
            field: 'PriceProt_Pct_MPCI',
            pickfield: 'PriceProt_Pct_MPCI',
            hide: mpcivisib,
            width: 90,
            headerClass: headerclassmaker(CellType.Integer, ""),
            cellClass: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforClass('PriceProt_Pct_MPCI', params);
            },
            editable: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforEdit('PriceProt_Pct_MPCI', params);

            },
            cellEditorParams: {
              decimals: 2
            },
            cellEditorSelector: function (params) {
              if (params.data.Subtype_MPCI != "ARH") {
                return {
                  component: 'emptyeditor'
                };
              } else {
                return {
                  component: 'numericCellEditor'
                };
              }

            },
            valueSetter: numberValueSetter,
            valueFormatter: function (params) {
              if (params.data.MPCI_Enabled != true) {
                return "";
              } else {
                return percentageFormatter(params, 0);
              }
            }
          },
          {
            headerName: 'Premium', suppressSorting: true,
            headerTooltip: 'Premium',
            field: 'Premium_MPCI',
            pickfield: 'Premium_MPCI',
            hide: mpcivisib,
            width: 90,
            headerClass: headerclassmaker(CellType.Integer, ""),
            cellClass: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforClass('Premium_MPCI', params);
            },
            editable: function (params) {
              return params.context.componentParent.verificationHelper.MPCIColumnAgentforEdit('Premium_MPCI', params);
            },
            cellEditorParams: {
              decimals: 2
            },
            cellEditor: "numericCellEditor",
            valueSetter: numberValueSetter,
            valueFormatter: function (params) {
              if (params.data.MPCI_Enabled == true) {
                return currencyFormatter(params, 2);
              } else {
                return "";
              }
            }
          },
          {
            headerName: 'Plan Type (Verify)', suppressSorting: true,
            headerTooltip: 'Type',

            field: 'Subtype_MPCI_Verify',
            pickfield: 'Subtype_MPCI_Verify',
            valueFormatter: (params) => {
              try {
                if (params.data.MPCI_Enabled != true) {
                  return "";
                } else {
                  return params.value;
                }
              } catch {
                return params.value;
              }
            },
            cellRenderer: function (params) {

              if (params.data.Verified_MPCI == true) {
                return '<i class="tiny material-icons icon-yellow">done</i>';
              } else {
                return params.value;
              }
            },
            hide: true,
            cellClass: function (params) {
              if (params.data.MPCI_Enabled == true) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                return cellclassmaker(CellType.Text, true, '', Loan_Status);
              } else {
                return "grayedcell";
              }
            },
            editable: function (params) {
              if (params.data.Verified_MPCI == true) {
                return false;
              }
              if (params.data.MPCI_Enabled == true) {
                return true;
              } else {
                return false;
              }
            },
            width: 120,
            cellEditor: "agSelectCellEditor",
            cellEditorParams: PoliciesComponent.GetMPCIPlanSubType('MPCI')
          },
          {
            headerName: 'Unit (Verify)', suppressSorting: true,
            headerTooltip: 'Unit',
            field: 'Unit_MPCI_Verify',
            pickfield: 'Unit_MPCI_Verify',
            width: 120,
            hide: true,
            minWidth: 90,
            cellClass: function (params) {
              if (params.data.MPCI_Enabled == true) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                return cellclassmaker(CellType.Text, true, '', Loan_Status);
              } else {
                return "grayedcell";
              }
            },
            cellRenderer: function (params) {

              if (params.data.Verified_MPCI == true) {
                return '<i class="tiny material-icons icon-yellow">done</i>';
              } else {
                return params.value;
              }
            },
            editable: function (params) {
              if (params.data.MPCI_Enabled == true) {
                return true;
              } else {
                return false;
              }
            },
            cellEditor: "selectEditor",
            cellEditorParams: {
              values: this.units
            },
            valueFormatter: (params) => {
              if (params.data.MPCI_Enabled != true) {
                return "";
              }
              const selected = params.context.componentParent.units.filter(u => u.key === (params.value));
              return selected.length > 0 ? selected[0].value : null;
            }
          },
          {
            headerName: 'Option (Verify)', suppressSorting: true,
            field: 'Option_MPCI_Verify',
            pickfield: 'Option_MPCI_Verify',
            Section: "MPCI",
            headerTooltip: 'Option',
            width: 120,
            hide: true,
            cellRenderer: function (params) {

              if (params.data.Verified_MPCI == true) {
                return '<i class="tiny material-icons icon-yellow">done</i>';
              } else {
                return params.value;
              }
            },
            valueFormatter: (params) => {
              try {
                let result = params.context.componentParent.getstatusofeditibilty(params);
                if (params.data.MPCI_Enabled == true && result) {
                  return params.value;
                } else {
                  return "";
                }
              } catch {
                return params.value;
              }
            },
            editable: function (params) {
              if (params.data.Verified_MPCI == true) {
                return false;
              }
              if (params.data.MPCI_Enabled == true) {
                return true;
              } else {
                return false;
              }
            },
            cellEditorSelector: function () {

              return {
                component: "agSelectCellEditor"
              };


            },
            cellEditorParams: getoptionsforinsurance("MPCI"),
            cellClass: function (params) {

              if (params.data.MPCI_Enabled == true) {
                return cellclassmaker(CellType.Text, true);
              } else {
                return "grayedcell";
              }
            }
          },
          {
            headerName: 'Level % (Verify)', suppressSorting: true,
            headerTooltip: 'Level Percent',
            field: 'Level_MPCI_Verify',
            pickfield: 'Level_MPCI_Verify',
            width: 120,
            hide: true,
            headerClass: headerclassmaker(CellType.Integer, ""),
            cellClass: function (params) {
              let Loan_Status = params.context.componentParent.Loan_Status;
              if (params.data.MPCI_Enabled == true) {
                return cellclassmaker(CellType.Integer, true, '', Loan_Status);
              } else {
                return cellclassmaker(CellType.Integer, false, "grayedcell", Loan_Status);
              }
            },
            cellRenderer: function (params) {

              if (params.data.Verified_MPCI == true) {
                return '<i class="tiny material-icons icon-yellow">done</i>';
              } else {
                return params.value;
              }
            },
            editable: function (params) {
              if (params.data.Verified_MPCI == true) {
                return false;
              }
              if (params.data.MPCI_Enabled == true) {
                return true;
              } else {
                return false;
              }
            },
            cellEditor: "numericCellEditor",
            cellEditorParams: {
              decimals: 1
            },
            valueFormatter: function (params) {
              if (params.data.MPCI_Enabled == true) {
                return percentageFormatter(params, 0);
              } else {
                return "";
              }
            },
            valueSetter: function (params) {
              numberValueSetter(params);
              if (params.newValue) {
                params.data['Rentperc'] = 100 - parseFloat(params.newValue);
              }
              return true;
            }
          },
          {
            headerName: 'Yield % (Verify)', suppressSorting: true,
            headerTooltip: 'Yield Prot %',
            field: 'YieldProt_Pct_MPCI_Verify',
            pickfield: 'YieldProt_Pct_MPCI_Verify',
            width: 120,
            hide: true,
            headerClass: headerclassmaker(CellType.Integer, ""),
            cellClass: function (params) {
              let Loan_Status = params.context.componentParent.Loan_Status;
              if (params.data.MPCI_Enabled != true) {
                return "grayedcell rightaligned";
              } else {
                return cellclassmaker(CellType.Integer, true, '', Loan_Status);
              }
            },
            editable: function (params) {
              if (params.data.Verified_MPCI == true) {
                return false;
              }
              if (params.data.MPCI_Enabled == true) {
                return true;
              } else {
                return false;
              }
            },
            cellEditorParams: {
              decimals: 2
            },
            cellEditorSelector: function () {

              return {
                component: 'numericCellEditor'
              };

            },
            cellRenderer: function (params) {

              if (params.data.Verified_MPCI == true) {
                return '<i class="tiny material-icons icon-yellow">done</i>';
              } else {
                return params.value;
              }
            },
            valueSetter: numberValueSetter,
            valueFormatter: function (params) {
              if (params.data.MPCI_Enabled != true) {
                return "";
              } else {
                return percentageFormatter(params, 0);
              }
            }
          },
          {
            headerName: 'Price % (Verify)', suppressSorting: true,
            headerTooltip: 'Price Protection',
            field: 'PriceProt_Pct_MPCI_Verify',
            pickfield: 'PriceProt_Pct_MPCI_Verify',
            width: 120,
            hide: true,
            headerClass: headerclassmaker(CellType.Integer, ""),
            cellClass: function (params) {
              let Loan_Status = params.context.componentParent.Loan_Status;
              if (params.data.MPCI_Enabled != true) {
                return "grayedcell rightaligned";
              } else {
                return cellclassmaker(CellType.Integer, true, '', Loan_Status);
              }
            },
            editable: function (params) {
              if (params.data.Verified_MPCI == true) {
                return false;
              }
              if (params.data.MPCI_Enabled == true) {
                return true;
              } else {
                return false;
              }

            },
            cellEditorParams: {
              decimals: 2
            },
            cellRenderer: function (params) {

              if (params.data.Verified_MPCI == true) {
                return '<i class="tiny material-icons icon-yellow">done</i>';
              } else {
                return params.value;
              }
            },
            cellEditorSelector: function () {

              return {
                component: 'numericCellEditor'
                //component:editortype
              };

            },
            valueSetter: numberValueSetter,
            valueFormatter: function (params) {
              if (params.data.MPCI_Enabled != true) {
                return "";
              } else {
                return percentageFormatter(params, 0);
              }
            }
          },
          {
            headerName: 'Premium (Verify)', suppressSorting: true,
            headerTooltip: 'Premium',
            field: 'Premium_MPCI_Verify',
            pickfield: 'Premium_MPCI_Verify',
            width: 120,
            hide: true,
            headerClass: headerclassmaker(CellType.Integer, ""),
            cellRenderer: function (params) {

              if (params.data.Verified_MPCI == true) {
                return '<i class="tiny material-icons icon-yellow">done</i>';
              } else {
                return params.value;
              }
            },
            cellClass: function (params) {
              if (params.data.MPCI_Enabled == true) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                return cellclassmaker(CellType.Integer, true, '', Loan_Status);
              } else {
                return "grayedcell";
              }
            },
            editable: function (params) {
              if (params.data.Verified_MPCI == true) {
                return false;
              }
              if (params.data.MPCI_Enabled == true) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                return isgrideditableinsurancepoilcytable(params, true, Loan_Status);
              } else {
                return false;
              }
            },
            cellEditorParams: {
              decimals: 2
            },
            cellEditor: "numericCellEditor",
            valueSetter: numberValueSetter,
            valueFormatter: function (params) {
              if (params.data.MPCI_Enabled == true) {
                return currencyFormatter(params, 2);
              } else {
                return "";
              }
            }
          }
        ]
      }


    ];
    this.getgriddata();

    if (this.columnApi != undefined) {
      //STILL NOT FIXED SO UNSTABLE
      //autoSizeAll(this.columnApi);
    }
    if (environment.isDebugModeActive) { console.timeEnd("declarecoldefs start Time"); }
  }

  public static GetMPCIPlanSubType(type: string): any {
    if (type == "MPCI") {
      return { values: ['CAT', 'YP', 'RP-HPE', 'RP', 'ARH'] };
    }
  }
  applyalternateclassestogroups() {
    if (environment.isDebugModeActive) { console.time("applyalternateclassestogroups start Time"); }
    let _alternate = true;
    Object.keys(this.Insurancetoggles).forEach(_element => {
      if (this.Insurancetoggles[_element] == true) {
        if (_alternate) {
          if (this.columnApi.getAllColumns() != undefined) {
            let _columns = this.columnApi.getAllColumns().filter(p => p.parent != undefined && !p.colId.includes('Verify'));
            _element = gettogglename(_element);
            _columns = _columns.filter(p => p.parent.originalColumnGroup.colGroupDef.headerName == _element);
            _columns.forEach(col => {
              col.colDef.headerClass = "headerinsurance";
            });
            if (_columns.length > 0) {
              _columns[0].parent.originalColumnGroup.colGroupDef.headerClass = "headerinsurance";
            }
            _alternate = false;
          }
        } else {
          if (this.columnApi.getAllColumns() != undefined) {
            let _columns = this.columnApi.getAllColumns().filter(p => p.parent != undefined && !p.colId.includes('Verify'));
            _element = gettogglename(_element);
            _columns = _columns.filter(p => p.parent.originalColumnGroup.colGroupDef.headerName == _element);
            _columns.forEach(col => {
              col.colDef.headerClass = "";
            });
            if (_columns.length > 0) {
              _columns[0].parent.originalColumnGroup.colGroupDef.headerClass = "";
            }
            _alternate = true;
          }
        }
      }
    });

    this.gridApi.refreshHeader();
    if (environment.isDebugModeActive) { console.timeEnd("applyalternateclassestogroups start Time"); }
  }
  addNumericColumn(element: string, editortype: string, value: string, colorheader: boolean, ispercent?: boolean, hidden?: boolean) {
    if (environment.isDebugModeActive) { console.time("addNumericColumn start Time"); }
    let _header = element;
    let _headername = "";
    if (ispercent) {
      _headername = _header.replace('_', ' ').replace(value, '').replace('_', '').replace('%', '') + (ispercent ? " %" : "");
      _headername = _headername.replace(" Pct", "");
    } else {
      _headername = _header.replace('_', ' ').replace(value, '').replace('_', '');
    }
    //Yield to Area Yield Port  --Get in Section 1 After

    if (_headername.trim() == "Yield") {
      _headername = "Area Yield";
    }
    element = element.replace("%", "");

    // headername = getnameforheader(headerkey, value);

    ///
    let _headercol = "headerinsurance-" + value;
    let _column: any = {
      headerName: _headername, width: 90, pickfield: element, field: element, hide: hidden, suppressSorting: true,
      headerClass: function () {
        return headerclassmaker(CellType.Integer, colorheader ? _headercol : "");
      },
      editable: function (params) {
        let Loan_Status = params.context.componentParent.Loan_Status;
        let val = element == "Icc_PCI" ? false : true;
        let result = params.context.componentParent.getstatusofeditibilty(params);
        let vaslue = isgrideditableinsurancepoilcytable(params, (val && result), Loan_Status);
        if (params.context.componentParent.verificationHelper['verifyButtonStage' + value] == VerificationStages.Check) {
          return false;
        } else {
          return vaslue;
        }
      }, //only iccc field is non editable we can create an array if later needed
      cellEditorSelector: function (params) {
        let pos = params.colDef.pickfield.lastIndexOf("_") + 1;
        let policyname = params.colDef.pickfield.substr(pos, params.colDef.pickfield.length - pos);
        if (policyname.length > 0) {
          if (params.data.SecInsurance.includes(policyname)) {
            // We need to check and include columns here from ICE and CropHail

            if (policyname == "ICE") {
              if (params.colDef.field == "Price_Pct_ICE") {
                return {
                  component: 'emptyeditor'
                };
              }
            }
            if (policyname == "CROPHAIL") {
              //Check for ICC subtypehere
              if (params.data.Subtype_CROPHAIL == "Basic") {

                // /params.colDef.field == "Price_Pct_CROPHAIL" || params.colDef.field == "Yield_Pct_CROPHAIL"
                if (params.colDef.field == "Upper_Limit_CROPHAIL" || params.colDef.field == "Lower_Limit_CROPHAIL" || params.colDef.field == "Deduct_Pct_CROPHAIL") {
                  return {
                    component: 'emptyeditor'
                  };
                }
              } else
                if (params.data.Subtype_CROPHAIL == "Companion") {

                  if (params.colDef.field == "Liability_CROPHAIL") {
                    return {
                      component: 'emptyeditor'
                    };
                  }
                }
            }
            return {
              component: editortype
            };
          } else {
            return {
              component: 'emptyeditor'
              //component:editortype
            };
          }
        }

      },
      cellEditorParams: {
        decimals: 2
      },
      cellRenderer: function (params) {

        let pos = params.colDef.pickfield.lastIndexOf("_") + 1;
        let policyname = params.colDef.pickfield.substr(pos, params.colDef.pickfield.length - pos);
        if (policyname.length > 0) {
          if (params.data.SecInsurance.includes(policyname)) {
            if (policyname == "ICE") {
              if (params.colDef.field == "Price_Pct_ICE") {
                return '';
              }
            }
            if (policyname == "CROPHAIL") {
              //Check for ICC subtypehere
              if (params.data.Subtype_CROPHAIL == "Basic") {

                if (params.colDef.field == "Upper_Limit_CROPHAIL" || params.colDef.field == "Lower_Limit_CROPHAIL") {
                  return "";
                }
              } else
                if (params.data.Subtype_CROPHAIL == "Companion") {
                  if (params.colDef.field == "Liability_CROPHAIL") {
                    return "";
                  }
                }
            }
            return params.valueFormatted;
          } else {
            return "";
          }
        }
      },
      cellClass: function (params) {
        //
        try {

          let Loan_Status = params.context.componentParent.Loan_Status;
          let pos = params.colDef.pickfield.lastIndexOf("_") + 1;
          let policyname = params.colDef.pickfield.substr(pos, params.colDef.pickfield.length - pos);
          let result = params.context.componentParent.getstatusofeditibilty(params);
          if (policyname.length > 0) {
            if (!params.data.SecInsurance.includes(policyname)) {
              //return 'grayedcell rightaligned';
              return cellclassmaker(CellType.Integer, (false && result), '', Loan_Status);
            } else {
              if (params.context.componentParent.verificationHelper['verifyButtonStage' + value] == VerificationStages.Check && params.data.ispinned != true) {
                return cellclassmaker(CellType.Integer, false, "blackcell", Loan_Status);
              }
              if (params.colDef.pickfield.includes("Icc")) {
                //return "grayedcell rightaligned";
                return cellclassmaker(CellType.Integer, (false && result), '', Loan_Status);
              }
              if (policyname == "ICE") {
                if (params.colDef.field == "Price_Pct_ICE") {
                  //return 'grayedcell rightaligned';
                  return cellclassmaker(CellType.Integer, (false && result), '', Loan_Status);
                }
              }
              if (policyname == "CROPHAIL") {
                //Check for ICC subtypehere

                if (params.data.Subtype_CROPHAIL == "Basic") {

                  if (params.colDef.field == "Deduct_Pct_CROPHAIL") {
                    return "graycell";
                  }
                  if (params.colDef.field == "Upper_Limit_CROPHAIL" || params.colDef.field == "Lower_Limit_CROPHAIL" || params.colDef.field == "Price_Pct_CROPHAIL" || params.colDef.field == "Yield_Pct_CROPHAIL") {
                    //return 'grayedcell rightaligned';
                    return cellclassmaker(CellType.Integer, (false && result), '', Loan_Status);
                  }
                }
              } else
                if (params.data.Subtype_CROPHAIL == "Companion") {

                  if (params.colDef.field == "Liability_CROPHAIL") {
                    //return 'grayedcell rightaligned';
                    return cellclassmaker(CellType.Integer, (false && result), '', Loan_Status);
                  }
                }
              return cellclassmaker(CellType.Integer, (true && result), '', Loan_Status);
            }
          }
        } catch {
          return 'grayedcell';
        }

      }, valueFormatter: getformatterforcolumn(_header)
    };
    if (editortype == "booleaneditor") {
      _column.valueFormatter = function (params) {

        if (params.value == "1") {
          return "Yes";
        } else if (params.value == undefined) {
          return "";
        } else {
          return "No";
        }
      };
    }
    if (environment.isDebugModeActive) { console.timeEnd("addNumericColumn start Time"); }
    return _column;

  }

  addNumericColumnVerification(element: string, editortype: string, value: string, colorheader: boolean, ispercent?: boolean) {
    if (environment.isDebugModeActive) { console.time("addNumericColumnVerification start Time"); }
    let _header = element;
    let _headername = "";
    if (ispercent) {
      _headername = _header.replace('_', ' ').replace(value, '').replace('_', '').replace('%', '') + (ispercent ? " %" : "");
      _headername = _headername.replace(" Pct", "");
    } else {
      _headername = _header.replace('_', ' ').replace(value, '').replace('_', '');
    }
    //Yield to Area Yield Port  --Get in Section 1 After

    if (_headername.trim() == "Yield") {
      _headername = "Area Yield";
    }
    element = element.replace("%", "");

    // headername = getnameforheader(headerkey, value);

    ///
    let _headercol = "headerinsurance-" + value;
    let _column: any = {
      headerName: _headername + "(Verify)", width: 120, hide: true, pickfield: element + "_Verify", field: element + "_Verify", suppressSorting: true,
      headerClass: function () {

        return headerclassmaker(CellType.Integer, colorheader ? _headercol : "");
      },
      editable: function (params) {
        let policystrng = params.colDef.pickfield.replace("_Verify", '');
        let pos = policystrng.lastIndexOf("_") + 1;
        let policyname = policystrng.substr(pos, policystrng.length - pos);
        if (policyname.length > 0) {
          if (!params.data.SecInsurance.includes(policyname)) {
            return false;
          } else {
            return true;
          }
        }
        return true;
      }, //only iccc field is non editable we can create an array if later needed
      cellEditorSelector: function () {
        return {
          component: editortype
        };
      },
      cellEditorParams: {
        decimals: 2
      },
      cellRenderer: function (params) {

        if (params.data['Verified_' + value] == true) {
          return '<i class="tiny material-icons icon-yellow">done</i>';
        } else {
          try {
            let policystrng = params.colDef.pickfield.replace("_Verify", '');
            let pos = policystrng.lastIndexOf("_") + 1;
            let policyname = policystrng.substr(pos, policystrng.length - pos);
            if (policyname.length > 0) {
              if (!params.data.SecInsurance.includes(policyname)) {
                return "";
              } else {
                return params.valueFormatted;
              }

            }
          } catch {

          }
        }
      },
      cellClass: function (params) {

        try {
          let Loan_Status = params.context.componentParent.Loan_Status;
          let policystrng = params.colDef.pickfield.replace("_Verify", '');
          let pos = policystrng.lastIndexOf("_") + 1;
          let policyname = policystrng.substr(pos, policystrng.length - pos);
          let result = params.context.componentParent.getstatusofeditibilty(params);
          if (policyname.length > 0) {
            if (!params.data.SecInsurance.includes(policyname)) {
              //return 'grayedcell rightaligned';
              return cellclassmaker(CellType.Integer, (false && result), '', Loan_Status);
            } else {
              return cellclassmaker(CellType.Integer, (true && result), '', Loan_Status);
            }
          }

        } catch {
          return 'grayedcell';
        }

      }, valueFormatter: getformatterforcolumn(_header)
    };
    if (editortype == "booleaneditor") {
      _column.valueFormatter = function (params) {

        if (params.value == "1") {
          return "Yes";
        } else if (params.value == undefined) {
          return "";
        } else {
          return "No";
        }
      };
    }
    if (environment.isDebugModeActive) { console.timeEnd("addNumericColumnVerification start Time"); }
    return _column;

  }
  renderallcolumns() {
    if (environment.isDebugModeActive) { console.time("renderallcolumns start Time"); }
    let rendervalues = [];
    let value = "";

    value = "STAX";
    rendervalues = ['Yield_STAX', 'Upper_Limit%_STAX', 'Lower_Limit%_STAX', "Prot_Factor%_STAX", 'Premium_STAX'];

    this.makecolumns(value, rendervalues, true);
    value = "SCO"; //these values are Suffixed rather than prefixed
    //HMAX
    rendervalues = ['Yield_SCO', 'Upper_Limit%_SCO', 'Premium_SCO'];
    //HMAX
    this.makecolumns(value, rendervalues, true);
    value = "HMAX";  //these values are Suffixed rather than prefixed
    //HMAX
    rendervalues = ['Upper_Limit%_HMAX', 'Lower_Limit%_HMAX', 'Late_Deduct_HMAX', 'Price_Pct%_HMAX', 'Premium_HMAX', ];
    //HMAX
    this.makecolumns(value, rendervalues, true);
    value = "RAMP";
    rendervalues = ['Upper_Limit%_RAMP', 'Lower_Limit%_RAMP', 'Price_Pct%_RAMP', 'Liability_RAMP', 'Premium_RAMP', ];
    this.makecolumns(value, rendervalues, true);

    value = "ICE";
    rendervalues = ['Upper_Limit%_ICE', 'Lower_Limit%_ICE', 'Yield_Pct%_ICE', 'Premium_ICE'];
    this.makecolumns(value, rendervalues, true);

    value = "SELECT";
    rendervalues = ['Upper_Limit%_SELECT', 'Lower_Limit%_SELECT', 'Premium_SELECT'];
    this.makecolumns(value, rendervalues, true);

    value = "PCI";
    rendervalues = ['FCMC_PCI', 'Icc_PCI', 'Premium_PCI'];
    this.makecolumns(value, rendervalues, true);

    value = "CROPHAIL";
    rendervalues = ['Liability_CROPHAIL', 'Wind_CROPHAIL', 'Upper_Limit%_CROPHAIL', 'Price_Pct%_CROPHAIL', 'Yield_Pct%_CROPHAIL', 'Deduct_Pct%_CROPHAIL', 'Premium_CROPHAIL'];
    this.makecolumns(value, rendervalues, true);

    let wfrpvisibilty = !this.Insurancetoggles.WFRP;
    //wfrp Push
    this.columnDefs.push({
      headerName: 'WFRP', headerClass: "headerinsurance_wfrp", children: [{
        headerName: '', width: 40, pickfield: "Wfrp_Enabled", field: 'Wfrp_Enabled', hide: wfrpvisibilty, pinnedRowCellRenderer: "checkbox",
        pinnedRowCellRendererParams: { edit: ischeckboxeditablewfrp() }
      },
      {
        headerName: 'Agency',
        field: 'wfrp_Agency_Id',
        pickfield: 'wfrp_Agency_Id',
        //cellRenderer: 'columnTooltip',
        headerTooltip: 'Agency',
        width: 90,
        hide: wfrpvisibilty,
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          let _temp = params.context.componentParent.wfrpEnabled;
          if (Loan_Status == 'W' && _temp == true) {
            params.colDef.cellEditorParams = params.context.componentParent.getAgencies();
          }
          return isgrideditableinsurancepoilcytable(params, _temp, Loan_Status);
        },
        cellEditorSelector: function (params) {

          if (params.data.Wfrp_Enabled == true) {
            return { component: "selectEditor" };
          } else {
            return { component: "emptyeditor" };
          }
        },
        cellEditorParams: this.getAgencies(),
        cellClass: function (params) {
          if (params.data.ispinned == true && params.data.Wfrp_Enabled) {
            return "pinnededitable";
          } else {
            return "";
          }
        }
      },
      {
        headerName: 'AIP',
        field: 'wfrp_aip',
        pickfield: 'wfrp_aip',
        cellEditorParams: this.getAIPs(),
        cellClass: function (params) {
          if (params.data.ispinned == true && params.data.Wfrp_Enabled) {
            return "pinnededitable";
          } else {
            return "";
          }
        },
        hide: wfrpvisibilty,
        //cellRenderer: 'columnTooltip',
        width: 90,
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          let _temp = params.context.componentParent.wfrpEnabled;
          if (Loan_Status == 'W' && _temp == true) {
            params.colDef.cellEditorParams = params.context.componentParent.getAIPs();
          }
          return isgrideditableinsurancepoilcytable(params, _temp, Loan_Status);
        },
        cellEditorSelector: function (params) {

          if (params.data.Wfrp_Enabled == true) {
            return { component: "selectEditor" };
          } else {
            return { component: "emptyeditor" };
          }
        }
      },
      {
        headerName: 'Appr.Revenue',
        field: 'wfrp_revenue',
        pickfield: 'wfrp_revenue',
        cellEditor: "numericCellEditor",
        hide: wfrpvisibilty,
        //cellRenderer: 'columnTooltip',
        width: 90,
        cellClass: function (params) {
          if (params.data.ispinned == true && params.data.Wfrp_Enabled) {
            return "pinnededitable rightaligned";
          } else {
            return "";
          }
        },
        cellRenderer: function () {
          return "";
        },
        floatingCellRenderer: function (params) {
          if (params.data.Wfrp_Enabled) {
            return BudgetFormatter(params.value);
          } else {
            return "";
          }
        },
        editable: function (params) {

          let Loan_Status = params.context.componentParent.Loan_Status;
          let _temp = params.context.componentParent.wfrpEnabled;
          return isgrideditableinsurancepoilcytable(params, _temp, Loan_Status);
        }
      },
      {
        headerName: 'Level %',
        field: 'wfrp_level',
        pickfield: 'wfrp_level',
        hide: wfrpvisibilty,
        cellEditor: "numericCellEditor",
        //cellRenderer: 'columnTooltip',
        width: 90,
        cellClass: function (params) {
          if (params.data.ispinned == true && params.data.Wfrp_Enabled) {
            return "pinnededitable rightaligned";
          } else {
            return "";
          }
        },
        cellRenderer: function () {
          return "";
        },
        floatingCellRenderer: function (params) {

          if (params.data.Wfrp_Enabled) {
            return percentageFormatter(params);
          } else {
            return "";
          }

        },
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          let _temp = params.context.componentParent.wfrpEnabled;
          return isgrideditableinsurancepoilcytable(params, _temp, Loan_Status);
        }
      },
      {
        headerName: 'Premium',
        field: 'wfrp_premium',
        pickfield: 'wfrp_premium',
        hide: wfrpvisibilty,
        cellEditor: "numericCellEditor",
        //cellRenderer: 'columnTooltip',
        width: 90, cellClass: function (params) {
          if (params.data.ispinned == true && params.data.Wfrp_Enabled) {
            return "pinnededitable rightaligned";
          } else {
            return "";
          }
        },
        cellRenderer: function () {
          return "";
        },
        floatingCellRenderer: function (params) {
          if (params.data.Wfrp_Enabled) {
            return BudgetFormatter(params.value);
          } else {
            return "";
          }

        },
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          let _temp = params.context.componentParent.wfrpEnabled;
          return isgrideditableinsurancepoilcytable(params, _temp, Loan_Status);
        }
      }
      ]
    });
    if (environment.isDebugModeActive) { console.timeEnd("renderallcolumns start Time"); }
  }

  private makecolumns(value: string, rendervalues: any[], colorheader: boolean) {
    if (environment.isDebugModeActive) { console.time("makecolumns start Time"); }
    let _colheader: any;
    let _headerclass = "";


    //here hide feature
    let _hide = false;
    if (this.Insurancetoggles[value] == false) {
      _hide = true;
    }
    if (colorheader) {
      _colheader = {
        headerName: value, children: [], headerClass: _headerclass, headerGroupComponentFramework: verifyPrimaryHeadergroup,
        headerGroupComponentParams: {
          menuIcon: "verified_user", isverify: true
        }
      };
    } else {
      _colheader = {
        headerName: value, children: [], headerGroupComponentFramework: verifyPrimaryHeadergroup,
        headerGroupComponentParams: {
          menuIcon: "verified_user", isverify: true
        }
      };
    }


    //CROPHAIL
    if (value == "CROPHAIL") {
      _colheader.headerName = "CROP HAIL";
    }
    _colheader.children = [
      {
        headerName: '', width: 40, pickfield: value + "_Enabled", hide: _hide, field: value, suppressSorting: true, cellRendererSelector: function (params) {
          if (params.data.mainpolicyId == undefined && params.data.ispinned) {
            return {
              component: 'checkbox'
            };
          }
          if (params.context.componentParent.checkifcheckboxneeded(params.colDef.field, params.data.mainpolicyId)) {
            return {
              component: 'checkbox'
            };
          } else {
            return null;
          }
        },
        cellRendererParams: { edit: ischeckboxeditable() }, headerClass: (colorheader ? _headerclass : "")
      },
      {
        headerName: 'Agency', suppressSorting: true,
        field: 'Agency_Id_' + value,
        pickfield: 'Agency_Id_' + value,
        headerClass: colorheader ? _headerclass : "",
        //cellRenderer: 'columnTooltip',
        headerTooltip: 'Agency',
        width: 100,
        hide: _hide,
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          if (Loan_Status == 'W') {
            params.colDef.cellEditorParams = params.context.componentParent.getAgencies();
          }
          return isgrideditableinsurancepoilcytable(params, true, Loan_Status);
        },
        cellEditorSelector: function (params) {
          let pos = params.colDef.pickfield.lastIndexOf("_") + 1;
          let policyname = params.colDef.pickfield.substr(pos, params.colDef.pickfield.length - pos);
          if (policyname.length > 0) {
            if (params.data.SecInsurance.includes(policyname)) {
              return {
                component: "selectEditor"
              };
            } else {
              return {
                component: 'emptyeditor'
                //component:editortype
              };
            }
          }

        },
        cellEditorParams: this.getAgencies(),
        valueFormatter: function (params) {

          try {
            if (params.value == 0) {
              return "";
            }
            return (params.context.componentParent.loanmodel as loan_model).Association.find(p => p.Assoc_ID == params.value).Assoc_Name;
          } catch (ex) {
            return "";
          }
        },
        cellRenderer: function (params) {

          let pos = params.colDef.pickfield.lastIndexOf("_") + 1;
          let policyname = params.colDef.pickfield.substr(pos, params.colDef.pickfield.length - pos);
          if (policyname.length > 0) {
            if (params.data.SecInsurance.includes(policyname)) {
              return params.valueFormatted;
            } else {
              return "";
            }
          }
        },
        cellClass: function (params) {

          try {
            let pos = params.colDef.pickfield.lastIndexOf("_") + 1;
            let policyname = params.colDef.pickfield.substr(pos, params.colDef.pickfield.length - pos);
            if (policyname.length > 0) {
              if (!params.data.SecInsurance.includes(policyname)) {
                return 'grayedcell rightaligned';
                //return cellclassmaker(CellType.Integer, true)
              } else {
                let Loan_Status = params.context.componentParent.Loan_Status;
                return cellclassmaker(CellType.Text, true, '', Loan_Status);
              }

            }
          } catch {
            return 'grayedcell';
          }
        }
      },
      {
        headerName: 'AIP', suppressSorting: true,
        field: 'Aip_' + value,
        pickfield: 'Aip_' + value,
        headerTooltip: 'ProposedAIP',
        headerClass: colorheader ? _headerclass : "",
        //cellRenderer: 'columnTooltip',
        editable: function (params) {
          let Loan_Status = params.context.componentParent.Loan_Status;
          if (Loan_Status == 'W') {
            params.colDef.cellEditorParams = params.context.componentParent.getAIPs();
          }
          return isgrideditableinsurancepoilcytable(params, true, Loan_Status);
        },
        width: 100,
        hide: _hide,
        cellEditorSelector: function (params) {
          let pos = params.colDef.pickfield.lastIndexOf("_") + 1;
          let policyname = params.colDef.pickfield.substr(pos, params.colDef.pickfield.length - pos);
          if (policyname.length > 0) {
            if (params.data.SecInsurance.includes(policyname)) {
              return {
                component: "selectEditor"
              };
            } else {
              return {
                component: 'emptyeditor'
                //component:editortype
              };
            }
          }

        },
        valueFormatter: function (params) {

          try {
            if (params.value == 0) {
              return "";
            }
            return (params.context.componentParent.loanmodel as loan_model).Association.find(p => p.Assoc_ID == params.value && p.Assoc_Type_Code == "AIP").Assoc_Name;
          } catch (ex) {
            return "";
          }
        },
        cellEditorParams: this.getAIPs()
        ,
        cellRenderer: function (params) {

          let pos = params.colDef.pickfield.lastIndexOf("_") + 1;
          let policyname = params.colDef.pickfield.substr(pos, params.colDef.pickfield.length - pos);
          if (policyname.length > 0) {
            if (params.data.SecInsurance.includes(policyname)) {
              return params.valueFormatted;
            } else {
              return "";
            }
          }
        },
        cellClass: function (params) {

          try {
            let pos = params.colDef.pickfield.lastIndexOf("_") + 1;
            let policyname = params.colDef.pickfield.substr(pos, params.colDef.pickfield.length - pos);
            if (policyname.length > 0) {
              if (!params.data.SecInsurance.includes(policyname)) {
                return 'grayedcell rightaligned';
                //return cellclassmaker(CellType.Integer, true)
              } else {
                let Loan_Status = params.context.componentParent.Loan_Status;
                return cellclassmaker(CellType.Text, true, '', Loan_Status);
              }

            }
          } catch {
            return 'grayedcell';
          }
        }
      }
    ];
    //tarjeet
    if (getsubtypeforinsurance(value) != null) {
      _colheader.children.push(
        {
          headerName: 'Plan Type', suppressSorting: true,
          field: 'Subtype_' + value,
          pickfield: 'Subtype_' + value,
          Section: value,
          headerClass: colorheader ? _headerclass : "",
          //cellRenderer: 'columnTooltip',
          headerTooltip: 'Type',
          width: 100,
          hide: _hide,
          editable: function (params) {
            if (params.context.componentParent.verificationHelper['verifyButtonStage' + value] == VerificationStages.Check) {
              return false;
            } else {
              let Loan_Status = params.context.componentParent.Loan_Status;
              return isgrideditableinsurancepoilcytable(params, true, Loan_Status);
            }

          },
          cellEditorSelector: function (params) {
            let policyname = params.column.parent.originalColumnGroup.colGroupDef.headerName.replace(' ', ''); //case of Crop hail
            if (policyname.length > 0) {
              if (params.data.SecInsurance.includes(policyname)) {
                return {
                  component: "agSelectCellEditor"
                };
              } else {
                return {
                  component: 'emptyeditor'
                  //component:editortype
                };
              }
            }
          },
          cellEditorParams: getsubtypeforinsurance(value),
          cellClass: function (params) {

            let policyname = params.colDef.Section;
            if (policyname != undefined && params.data.SecInsurance != undefined && policyname.length > 0) {
              if (params.data.SecInsurance.includes(policyname.replace(' ', ''))) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                if (params.context.componentParent.verificationHelper['verifyButtonStage' + value] == VerificationStages.Check && params.data.ispinned != true) {
                  return cellclassmaker(CellType.Text, false, "blackcell", Loan_Status);
                }
                return cellclassmaker(CellType.Text, true, '', Loan_Status);
              } else {
                return "";
              }
            } else {
              return "";
            }
          }
        }
      );

      //push Verification One also
      _colheader.children.push(
        {
          headerName: 'Plan Type (Verify)', suppressSorting: true,
          field: 'Subtype_' + value + "_Verify",
          pickfield: 'Subtype_' + value + "_Verify",
          Section: value,
          headerClass: colorheader ? _headerclass : "",
          //cellRenderer: 'columnTooltip',
          headerTooltip: 'Type',
          width: 120,
          hide: true,
          editable: function () {
            return true;
          },
          cellEditorSelector: function (params) {
            let policyname = params.column.parent.originalColumnGroup.colGroupDef.headerName.replace(' ', ''); //case of Crop hail
            if (policyname.length > 0) {
              if (params.data.SecInsurance.includes(policyname)) {
                return {
                  component: "agSelectCellEditor"
                };
              } else {
                return {
                  component: 'emptyeditor'
                  //component:editortype
                };
              }
            }
          },
          cellEditorParams: getsubtypeforinsurance(value),
          cellClass: function (params) {

            // if (params.context.componentParent.verificationHelper['verifyButtonStage' + value] == VerificationStages.Check) {
            //   return cellclassmaker(CellType.Text,false,"blackcell");
            // }
            let policyname = params.colDef.Section;
            if (policyname != undefined && params.data.SecInsurance != undefined && policyname.length > 0) {
              if (params.data.SecInsurance.includes(policyname.replace(' ', ''))) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                return cellclassmaker(CellType.Text, true, Loan_Status);
              } else {
                return "";
              }
            } else {
              return "";
            }
          },
          cellRenderer: function (params) {

            if (params.data['Verified_' + value] == true) {
              return '<i class="tiny material-icons icon-yellow">done</i>';
            } else {
              return params.valueFormatted;
            }
          },
        }
      );
    }
    ///tarjeet
    if (getunitsforinsurance(value) != null) {
      _colheader.children.push(
        {
          headerName: 'Unit', suppressSorting: true,
          field: 'Unit_' + value,
          pickfield: 'Unit_' + value,
          Section: value,
          headerClass: colorheader ? _headerclass : "",
          //cellRenderer: 'columnTooltip',
          headerTooltip: 'Unit',
          width: 100,
          hide: _hide,
          editable: function (params) {

            if (params.context.componentParent.verificationHelper['verifyButtonStage' + value] == VerificationStages.Check) {
              return false;
            } else {
              let Loan_Status = params.context.componentParent.Loan_Status;
              return isgrideditableinsurancepoilcytable(params, true, Loan_Status);
            }
          },
          valueFormatter: (params) => {

            const selected = params.context.componentParent.units.filter(u => u.key === (params.value));
            return selected.length > 0 ? selected[0].value : null;
          },
          cellEditorSelector: function (params) {

            let policyname = params.column.parent.originalColumnGroup.colGroupDef.headerName.replace(' ', ''); //case of Crop hail
            if (policyname.length > 0) {
              if (params.data.SecInsurance.includes(policyname)) {
                return {
                  component: "agSelectCellEditor"
                };
              } else {
                return {
                  component: 'emptyeditor'
                  //component:editortype
                };
              }
            }
          },
          cellEditorParams: getunitsforinsurance(value),
          cellClass: function (params) {

            let policyname = params.colDef.Section;
            if (policyname != undefined && params.data.SecInsurance != undefined && policyname.length > 0) {
              if (params.data.SecInsurance.includes(policyname.replace(' ', ''))) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                if (params.context.componentParent.verificationHelper['verifyButtonStage' + value] == VerificationStages.Check && params.data.ispinned != true) {
                  return cellclassmaker(CellType.Text, false, "blackcell", Loan_Status);
                }
                let result = params.context.componentParent.getstatusofeditibilty(params);

                return cellclassmaker(CellType.Text, result, '', Loan_Status);
              } else {
                return "";
              }
            } else {
              return "";
            }
          }
        }
      );


      //push Verification One also
      _colheader.children.push(
        {
          headerName: 'Unit (Verify)', suppressSorting: true,
          field: 'Unit_' + value + "_Verify",
          pickfield: 'Unit_' + value + "_Verify",
          Section: value,
          headerClass: colorheader ? _headerclass : "",
          //cellRenderer: 'columnTooltip',
          headerTooltip: 'Unit',
          hide: true,
          width: 120,
          editable: function (params) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            let result = params.context.componentParent.getstatusofeditibilty(params);
            return isgrideditableinsurancepoilcytable(params, result, Loan_Status);
          },
          valueFormatter: (params) => {

            const selected = params.context.componentParent.units.filter(u => u.key === (params.value));
            return selected.length > 0 ? selected[0].value : null;
          },
          cellEditorSelector: function (params) {

            let policyname = params.column.parent.originalColumnGroup.colGroupDef.headerName.replace(' ', ''); //case of Crop hail
            if (policyname.length > 0) {
              if (params.data.SecInsurance.includes(policyname)) {
                return {
                  component: "agSelectCellEditor"
                };
              } else {
                return {
                  component: 'emptyeditor'
                  //component:editortype
                };
              }
            }
          },
          cellRenderer: function (params) {

            if (params.data['Verified_' + value] == true) {
              return '<i class="tiny material-icons icon-yellow">done</i>';
            } else {
              return params.valueFormatted;
            }
          },
          cellEditorParams: getunitsforinsurance(value),
          cellClass: function (params) {

            let policyname = params.colDef.Section;
            if (policyname != undefined && params.data.SecInsurance != undefined && policyname.length > 0) {
              if (params.data.SecInsurance.includes(policyname.replace(' ', ''))) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                let result = params.context.componentParent.getstatusofeditibilty(params);

                return cellclassmaker(CellType.Text, result, '', Loan_Status);
              } else {
                return "";
              }
            } else {
              return "";
            }
          }
        }
      );
    }

    if (getoptionsforinsurance(value) != null) {
      _colheader.children.push(
        {
          headerName: 'Option', suppressSorting: true,
          field: 'Option_' + value,
          pickfield: 'Option_' + value,
          Section: value,
          headerClass: colorheader ? _headerclass : "",
          //cellRenderer: 'columnTooltip',
          headerTooltip: 'Option',
          width: 100,
          hide: _hide,
          editable: function (params) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return isgrideditableinsurancepoilcytable(params, true, Loan_Status);
          },
          cellEditorSelector: function (params) {
            let policyname = params.column.parent.originalColumnGroup.colGroupDef.headerName;
            if (policyname.length > 0) {
              if (params.data.SecInsurance.includes(policyname)) {
                return {
                  component: "agSelectCellEditor"
                };
              } else {
                return {
                  component: 'emptyeditor'
                  //component:editortype
                };
              }
            }
          },
          cellEditorParams: getoptionsforinsurance(value),
          cellClass: function (params) {

            let policyname = params.colDef.Section;
            if (policyname != undefined && params.data.SecInsurance != undefined && policyname.length > 0) {
              if (params.data.SecInsurance.includes(policyname)) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                return cellclassmaker(CellType.Text, true, '', Loan_Status);
              } else {
                return "";
              }
            } else {
              return "";
            }
          }
        }
      );

      //push Verification One also

      _colheader.children.push(
        {
          headerName: 'Option (Verify)', suppressSorting: true,
          field: 'Option_' + value + "_Verify",
          pickfield: 'Option_' + value + "_Verify",
          Section: value,
          hide: true,
          headerClass: colorheader ? _headerclass : "",
          //cellRenderer: 'columnTooltip',
          headerTooltip: 'Option',
          width: 100,
          editable: function (params) {
            let Loan_Status = params.context.componentParent.Loan_Status;
            return isgrideditableinsurancepoilcytable(params, true, Loan_Status);
          },
          cellEditorSelector: function (params) {
            let policyname = params.column.parent.originalColumnGroup.colGroupDef.headerName;
            if (policyname.length > 0) {
              if (params.data.SecInsurance.includes(policyname)) {
                return {
                  component: "agSelectCellEditor"
                };
              } else {
                return {
                  component: 'emptyeditor'
                  //component:editortype
                };
              }
            }
          },
          cellEditorParams: getoptionsforinsurance(value),
          cellClass: function (params) {

            let policyname = params.colDef.Section;
            if (policyname != undefined && params.data.SecInsurance != undefined && policyname.length > 0) {
              if (params.data.SecInsurance.includes(policyname)) {
                let Loan_Status = params.context.componentParent.Loan_Status;
                return cellclassmaker(CellType.Text, true, '', Loan_Status);
              } else {
                return "";
              }
            } else {
              return "";
            }
          },
          cellRenderer: function (params) {

            if (params.data['Verified_' + value] == true) {
              return '<i class="tiny material-icons icon-yellow">done</i>';
            } else {
              return params.valueFormatted;
            }
          }
        }
      );
    }

    let inject = false;
    rendervalues.forEach(element => {
      if (this.columnDefs.find(p => p.pickfield == element) == undefined) {
        inject = true;
      }
      if (element.includes("Wind")) {
        _colheader.children.push(this.addNumericColumn(element, "booleaneditor", value, colorheader, false, _hide));
        _colheader.children.push(this.addNumericColumnVerification(element, "booleaneditor", value, colorheader, false));
        //verification column push here
      } else {
        let ispercent = element.includes("%");
        _colheader.children.push(this.addNumericColumn(element, "numericCellEditor", value, colorheader, ispercent, _hide));
        _colheader.children.push(this.addNumericColumnVerification(element, "numericCellEditor", value, colorheader, ispercent));
      }
    });
    if (inject) {
      this.columnDefs.push(_colheader);
    }
    if (environment.isDebugModeActive) { console.timeEnd("makecolumns start Time"); }
  }


  getwfrpmessagevis() {
    return !this.loanmodel.CalculatedVariables.FC_hasborrowerinc && this.loanmodel.Loanwfrp.Wfrp_Enabled;
  }

  methodFromcheckbox(event) {


    console.time('Insurance Validation start Time');
    let _result = this.checkifcheckboxneeded(event.col, event.id);
    if (_result == false) {
      event.value = false;
      if (environment.isDebugModeActive) { console.timeEnd("methodFromcheckbox start Time"); }
      return;
    }

    //check if policy is custom
    this.methodfromcheckboxinvoked = true;

    let _iscustom;
    let _policy = this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == event.id);
    if (_policy != undefined) {
      _iscustom = _policy.IsCustom;
    } else {
      _iscustom = false;
    }
    if (!_iscustom) {
      if (event.col != "Wfrp_Enabled" && event.col != "MPCI_Enabled") {

        if (event.value == true) {
          let _mainmpciPolicy = this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == event.id);
          let foundpolicy = this.loanmodel.LoanPolicies.find(p => p.Policy_Key == _mainmpciPolicy.Policy_Key && p.Ins_Plan == event.col && p.Select_Ind == false);
          if (foundpolicy != undefined || foundpolicy != null) {
            this.loanmodel.LoanPolicies.find(p => p.Policy_Key == _mainmpciPolicy.Policy_Key && p.Ins_Plan == event.col).Select_Ind = true;
          } else {
            let obj = new Loan_Policy();
            obj.Loan_Policy_ID = getRandomInt(Math.pow(10, 5), Math.pow(10, 7));
            obj.Ins_Plan = event.col;
            obj.FC_CanCopy = true;
            obj.Select_Ind = true;
            obj.AIP_ID = this.assignAiponcreate(event.col);
            obj.Loan_Full_ID = this.loanmodel.Loan_Full_ID;
            obj.State_ID = _mainmpciPolicy.State_ID;
            obj.County_ID = _mainmpciPolicy.County_ID;
            obj.Crop_Code = _mainmpciPolicy.Crop_Code;
            obj.Crop_Type_Code = _mainmpciPolicy.Crop_Type_Code;
            obj.Crop_Practice_Type_Code = _mainmpciPolicy.Crop_Practice_Type_Code;
            obj.Irr_Practice_Type_Code = _mainmpciPolicy.Irr_Practice_Type_Code;
            obj.HR_Exclusion_Ind = _mainmpciPolicy.HR_Exclusion_Ind;
            obj.Policy_Key = lookupStateValue(_mainmpciPolicy.State_ID, this.refdata) + "_" + lookupCountyValue(_mainmpciPolicy.County_ID, this.refdata) + "_" + _mainmpciPolicy.Crop_Code + "_" + (_mainmpciPolicy.Crop_Type_Code == "" ? "0" : _mainmpciPolicy.Crop_Type_Code)  + "_" + _mainmpciPolicy.Irr_Practice_Type_Code + '_' + _mainmpciPolicy.Crop_Practice_Type_Code + (_mainmpciPolicy.HR_Exclusion_Ind == true ? '_HR' : '');
            this.Set_Price(obj);
            obj.ActionStatus = 1;
            this.loanmodel.LoanPolicies.push(obj);
          }
        } else {
          //if id is null lets find by keys
          let MainPolicy = CompareandgetSubPolicy(event.id, event.col, this.loanmodel);
          if (MainPolicy.Loan_Policy_ID != null) {
            MainPolicy.ActionStatus = 3;

            _.remove(this.loanmodel.LoanPolicies, p => p.Loan_Policy_ID == MainPolicy.Loan_Policy_ID);
            this.loanmodel.LoanPolicies.push(MainPolicy);
          } else {

            _.remove(this.loanmodel.LoanPolicies, p => p == MainPolicy);
          }
        }
      } else {
        if (event.col == "Wfrp_Enabled") {
          this.wfrpEnabled = event.value;
          this.loanmodel.Loanwfrp.Wfrp_Enabled = event.value;
          if (event.value == false) {
            this.loanmodel.Loanwfrp.Approved_Revenue = 0;
            this.loanmodel.Loanwfrp.Level = 0;
            this.loanmodel.Loanwfrp.Premium = 0;
            this.loanmodel.Loanwfrp.Proposed_AIP = null;
            this.loanmodel.Loanwfrp.Agency_ID = null;
            this.loanmodel.Loanwfrp.Agent_ID = null;

          } else {

          }
        } else {
          let __policy = this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == event.id);
          this.Set_Price(__policy);
          __policy.Select_Ind = event.value;

          this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == event.id).FC_CanCopy = true;
          if (this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == event.id).ActionStatus != 1) {
            this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == event.id).ActionStatus = 2;
          }
        }

      }

    } else {
      if (event.col != "MPCI_Enabled") {
        if (event.value == true) {
          let _MainPolicy = this.loanmodel.LoanPolicies.find(p => Math.abs(p.Loan_Policy_ID) == Math.abs(event.id) && p.IsCustom == true);
          let _foundpolicy = this.loanmodel.LoanPolicies.find(p => p.Policy_Key == _MainPolicy.Policy_Key && p.Ins_Plan == event.col && p.Select_Ind == false);
          if (_foundpolicy != undefined || _foundpolicy != null) {
            this.loanmodel.LoanPolicies.find(p => p.Policy_Key == _MainPolicy.Policy_Key && p.Ins_Plan == event.col).Select_Ind = true;
          } else {
            let _obj = new Loan_Policy();
            _obj.Loan_Policy_ID = -Math.abs(this.loanmodel.LoanPolicies.length + 1);
            _obj.Ins_Plan = event.col;
            _obj.FC_CanCopy = true;
            _obj.Loan_Full_ID = this.loanmodel.Loan_Full_ID;
            _obj.AIP_ID = this.assignAiponcreate(event.col);
            _obj.State_ID = _MainPolicy.State_ID;
            _obj.County_ID = _MainPolicy.County_ID;
            _obj.Crop_Code = _MainPolicy.Crop_Code;
            _obj.Select_Ind = true;
            _obj.IsCustom = true;
            _obj.Crop_Practice_Type_Code = _MainPolicy.Crop_Practice_Type_Code;
            _obj.Irr_Practice_Type_Code = _MainPolicy.Crop_Practice_Type_Code;
            _obj.HR_Exclusion_Ind = _MainPolicy.HR_Exclusion_Ind;
            _obj.Policy_Key = lookupStateValue(_MainPolicy.State_ID, this.refdata) + "_" + lookupCountyValue(_MainPolicy.County_ID, this.refdata) + "_" + _MainPolicy.Crop_Code + "_" + (_MainPolicy.Crop_Type_Code == "" ? "0" : _MainPolicy.Crop_Type_Code)  + "_" + _MainPolicy.Crop_Practice_Type_Code + '_' + _MainPolicy.Crop_Practice_Type_Code + (_MainPolicy.HR_Exclusion_Ind == true ? '_HR' : '');
            _obj.Price = _MainPolicy.Price;
            _obj.ActionStatus = 1;
            this.loanmodel.LoanPolicies.push(_obj);
          }
        } else {
          //if id is null lets find by keys
          let _MainPolicy = CompareandgetSubPolicy(event.id, event.col, this.loanmodel);
          if (_MainPolicy.Loan_Policy_ID != null) {
            let _obj = new Loan_Policy();
            _obj.Loan_Policy_ID = null;
            _obj.Ins_Plan = event.col;
            _obj.Loan_Full_ID = this.loanmodel.Loan_Full_ID;
            _obj.State_ID = _MainPolicy.State_ID;
            _obj.Policy_Key = lookupStateValue(_MainPolicy.State_ID, this.refdata) + "_" + lookupCountyValue(_MainPolicy.County_ID, this.refdata) + "_" + _MainPolicy.Crop_Code + "_" + _MainPolicy.Crop_Practice_Type_Code + (_MainPolicy.HR_Exclusion_Ind == true ? '_HR' : '');
            _obj.County_ID = _MainPolicy.County_ID;
            _obj.AIP_ID = this.assignAiponcreate(event.col);
            _obj.Crop_Code = _MainPolicy.Crop_Code;
            _obj.IsCustom = true;
            this.Set_Price(_obj);
            _obj.Crop_Practice_Type_Code = _MainPolicy.Crop_Practice_Type_Code;
            _obj.Crop_Type_Code = _MainPolicy.Crop_Practice_Type_Code;
            _obj.HR_Exclusion_Ind = _MainPolicy.HR_Exclusion_Ind;
            _MainPolicy.ActionStatus = 3;

            _.remove(this.loanmodel.LoanPolicies, p => p.Loan_Policy_ID == _MainPolicy.Loan_Policy_ID);
            this.loanmodel.LoanPolicies.push(_MainPolicy);
          } else {

            _.remove(this.loanmodel.LoanPolicies, p => p == _MainPolicy);
          }
        }
      } else {
        this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == event.id).Select_Ind = event.value;
        if (this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == event.id).ActionStatus != 1) {
          this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == event.id).ActionStatus = 2;
        }
      }
    }
    //this.retrieveerrors();
    this.validationservice.highlighErrorCells(this.errorlist, 'Table_Policies');

    //this.getgriddata();
    this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);

    this.publishService.enableSync(Page.insurance);
    console.timeEnd('Insurance Validation start Time');
  }

  checkifcheckboxneeded(col: string, mainpolicyid: number) {
    let _cotton = "COT";
    let _insuranceValidationData = this.insuranceValidationData;

    let _mainPolicy = this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == mainpolicyid);
    if (_mainPolicy == undefined) {

    }
    if (col == "STAX") {
      if (_mainPolicy.Crop_Code != _cotton) {
        return false;
      }
    }

    if (col == "HMAX") {
      if (!(_insuranceValidationData.HMAX_STANDARD_ELLIGIBLESTATES.some(x => x.trim() == lookupStateValue(_mainPolicy.State_ID, this.refdata).trim()) &&
        _insuranceValidationData.HMAX_STANDARD_ELLIGIBLECROPS.some(x => x.trim() == _mainPolicy.Crop_Code.trim())
      )) {
        return false;
      }
    }

    if (col == "RAMP") {
      if (!(_insuranceValidationData.RAMP_RY_ELLIGIBLESTATES.some(x => x.trim() == lookupStateValue(_mainPolicy.State_ID, this.refdata).trim()) &&
        _insuranceValidationData.RAMP_RY_ELLIGIBLECROPS.some(x => x.trim() == _mainPolicy.Crop_Code.trim())
      )) {
        return false;
      }
    }

    if (col == "ICE") {
      if (!(_insuranceValidationData.ICE_BY_ELLIGIBLESTATES.some(x => x.trim() == lookupStateValue(_mainPolicy.State_ID, this.refdata).trim()) &&
        _insuranceValidationData.ICE_BY_ELLIGIBLECROPS.some(x => x.trim() == _mainPolicy.Crop_Code.trim())
      )) {
        return false;
      }
    }

    if (col == "SELECT") {
      if (!(_insuranceValidationData.SELECT_Yield_ELIGIBLE_STATES.some(x => x.trim() == lookupStateValue(_mainPolicy.State_ID, this.refdata).trim()) &&
        _insuranceValidationData.SELECT_Yield_ELLIGIBLECROPS.some(x => x.trim() == _mainPolicy.Crop_Code.trim())
      )) {
        return false;
      }
    }

    if (col == "PCI") {
      if (!(_insuranceValidationData.PCI_ELLIGIBLESTATES.some(x => x.trim() == lookupStateValue(_mainPolicy.State_ID, this.refdata).trim()) &&
        _insuranceValidationData.PCI_ELLIGIBLECROPS.some(x => x.trim() == _mainPolicy.Crop_Code.trim())
      )) {
        return false;
      }
    }

    if (col == "CROPHAIL") {
      if (!(_insuranceValidationData.CROPHAIL_BASIC_ELLIGIBLESTATES.some(x => x.trim() == lookupStateValue(_mainPolicy.State_ID, this.refdata).trim()) &&
        _insuranceValidationData.CROPHAIL_BASIC_ELLIGIBLECROPS.some(x => x.trim() == _mainPolicy.Crop_Code.trim())
      )) {
        return false;
      }
    }
    return true;
  }



  wfrpmessagewidth() {
    let _selectedelements = document.querySelectorAll('[class="ag-root-wrapper ag-layout-auto-height ag-ltr"]');
    _selectedelements.forEach(function (userItem) {
      if (userItem.parentElement.localName == 'ag-grid-angular') {
        return userItem.clientWidth;
      }
    });

    return '1360';
  }
  getAIPs(): any {
    let _ret = this.loanobj.Association.filter(p => p.ActionStatus != 3 && p.Assoc_Type_Code == "AIP");
    let obj: any[] = [];
    _ret.forEach((element: any) => {
      obj.push({ key: element.Assoc_ID, value: element.Assoc_Name ? element.Assoc_Name.toString() : "" });
    });
    this.ProposedAips = obj;
    return { values: obj };
  }
  getAgents(): any {

    let _ret = this.loanobj.Association.filter(p => p.ActionStatus != 3 && p.Assoc_Type_Code == "AGY");
    let obj: any[] = [];
    _ret.forEach((element: any) => {
      obj.push({ key: element.Assoc_ID, value: element.Assoc_Name ? element.Assoc_Name.toString() : "" });
    });
    this.agents = obj;

    return { values: obj };
  }
  toggleAgentsAipsAgency() {

    if (this.agencies.length == 1) {
      this.hidecolumnsbystringheader("Agency", false);
    } else {
      this.hidecolumnsbystringheader("Agency", true);
    }

    if (this.ProposedAips.length == 1) {
      this.hidecolumnsbystringheader("Aip", false);
      this.hidecolumnsbystringheader("AIP", false);
    } else {
      this.hidecolumnsbystringheader("Aip", true);
      this.hidecolumnsbystringheader("AIP", true);
    }

  }
  hidecolumnsbystringheader(header: string, show: boolean) {

    if (this.columnApi && this.columnApi.getAllColumns() != undefined) {
      let _columns = this.columnApi.getAllColumns();
      _columns = _columns.filter(p => p.colDef.pickfield.includes(header) || p.colDef.pickfield.includes(header.toLowerCase()));
      _columns.forEach(element => {
        try {
          if (show == true) {
            let headername = element.parent.originalColumnGroup.colGroupDef.headerName;
            headername = gettogglename(headername);
            if (this.Insurancetoggles[headername]) {
              this.columnApi.setColumnVisible(element.colId, true);
            }
          } else {
            this.columnApi.setColumnVisible(element.colId, show);
          }
        } catch (e) {

        }
      });
    }
  }

  getAgencies(): any {
    let _ret = this.loanobj.Association.filter(p => p.ActionStatus != 3 && p.Assoc_Type_Code == "AGY");
    let obj: any[] = [];
    _ret.forEach((element: any) => {
      obj.push({ key: element.Assoc_ID, value: element.Assoc_Name.toString() });
    });
    this.agencies = obj;
    return { values: obj };
  }
  public gridOptions = {
    getRowNodeId: function (data) {
      return "Ins_" + data.mainpolicyId;
    }
  };

  Get_Crops_Practices_List() {
    let _selection = [];

    let _existingCrops = this.loanmodel.CropYield.filter(p => p.ActionStatus != 3);

    this.refdata.CropList.forEach(c => {
      if (!_existingCrops.some(x => x.Crop_ID == c.Crop_And_Practice_ID)) {
        _selection.push({
          Crop_Code: c.Crop_Code,
          Crop_Name: c.Crop_Name,
          Crop_Type_Code: c.Crop_Type_Code,
          Irr_Prac_Code: c.Irr_Prac_Code,
          Crop_Prac_Code: c.Crop_Prac_Code,
          Crop_And_Practice_ID: c.Crop_And_Practice_ID,
          key: c.Crop_And_Practice_ID,
          value: `${c.Crop_Name} | ${c.Crop_Type_Code} | ${c.Irr_Prac_Code}`,
          InsUOM: lookupUOM(c.Crop_Code, this.refdata)
        });
      }
    });

    _selection = _.uniqBy(_selection, 'key');
    _selection = _.sortBy(_selection, ['Crop_Name', 'Crop_Type_Code', 'Irr_Prac_Code', 'Crop_Prac_Code']);

    return _selection;
  }

  constructor(
    private localstorage: LocalStorageService,
    private loancalculationservice: LoancalculationWorker,
    private validationservice: ValidationService,
    public logging: LoggingService,
    public alertify: AlertifyService,
    public loanapi: LoanApiService,
    private publishService: PublishService,
    private dataService: DataService,
    private settingsService: SettingsService,
    private dtss: DatetTimeStampService,
    public dialog: MatDialog,
    private pubsubService: PubSubService,
    private auditTrail: LoanAuditTrailService,
    public masterSvc: MasterService
  ) {

    this.frameworkcomponents = {
      chipeditor: ChipsListEditor,
      selectEditor: SelectEditor,
      numericCellEditor: NumericEditor,
      deletecolumn: DeleteButtonRenderer,
      booleaneditor: BooleanEditor,
      emptyeditor: EmptyEditor,
      columnTooltip: AgGridTooltipComponent,
      checkbox: CheckboxCellComponent,
      autocompleteEditor: CropAutoCompleteEditor
    };

    this.refdata = this.localstorage.retrieve(environment.referencedatakey);
    this.loanobj = this.localstorage.retrieve(environment.loankey);
    this.loanmodel = this.loanobj;
    this.loanobj.LoanPolicies.forEach(element => {

      element.FC_County = lookupCountyValueFromPassedRefData(element.County_ID, this.refdata);
      element.FC_StateCode = lookupStateCodeFromPassedRefObject(element.State_ID, this.refdata);
    });

    this.verificationHelper = new PoliciesVerificationHelper(this, dialog);
    this.context = { componentParent: this };
    // this.insuranceValidationData = this.validationservice.getAllValidationDataRules(this.refdata);
    this.insuranceValidationData = PolicyValidationHelper.getInsuranceValidationData(this.refdata);
    //Col defs

    // Ends Here

    // storage observer
    // TODO: Check this if it is needed, causing performance issue andcell gets blank
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      // settimeEnd("Editing Stopped//////////////////////////////////////////////")
      if (res != null) {


        this.loanmodel = res;
        this.hrInclusion = JSON.parse(this.loanmodel.LoanMaster.Loan_Settings).HrInclusion;
        //set filter here
        if (!this.columnDefs) { this.declarecoldefs(); } else {
          this.getgriddata(false);
        }

        //this.toggleHRinclusion({checked:this.HRinclusion});
        this.toggleAgentsAipsAgency();
        this.gridApi.redrawRows();
        // this.verificationHelper.seterrors();
        this.validationservice.highlighErrorCells(this.errorlist, 'Table_Policies');
        // settimeEnd("Processing Stopped//////////////////////////////////////////////")

        setTimeout(() => {
          setmodifiedallIns(this.localstorage.retrieve(environment.modifiedbase) as Array<String>, "Ins_");
        }, 1000);

      }

    });
  }
  saveloanobject(loanmodel) {
    this.loanmodel = loanmodel;
    //Experimental
    //this.dataService.setLoanObject(loanmodel);
  }
  private lasttoggledcolumn = "";
  private lasttoggledbool = null;
  hideUnhidetheColumns(event, _columnsname: string) {
try {
    let _columns = this.columnDefs;
    _columns = _columns.find(p => p.headerName == _columnsname);
    _columns['children'].filter(p => !p.field.includes('Verify')).forEach(element => {
      if (element.field != 'SecInsurance') {
        this.columnApi.setColumnVisible(element.field, event.checked);
      }
    });
    _columnsname = gettogglename(_columnsname);
    this.Insurancetoggles[_columnsname] = event.checked;
    let obj = (JSON.parse(this.loanmodel.LoanMaster.Loan_Settings) as Loansettings);
    if (obj.insurance_policy_Settings == undefined) {
      obj.insurance_policy_Settings = new Insurance_Policy_Settings();
    }
    obj.insurance_policy_Settings.policy_visibility_state[_columnsname] = event.checked;
    this.loanmodel.LoanMaster.Loan_Settings = JSON.stringify(obj);
    this.publishService.enableSync(Page.insurance);
    this.dataService.setLoanObjectwithoutsubscription(this.loanmodel);
    this.applyalternateclassestogroups();
    this.adjustToFitAgGrid();
    this.checkforAIP(_columnsname);
  } catch (ex) {
console.log("Columns Not Reasy Yet");
  }
  }

  loadinitialtabsfromcontext(settings) {

    let _objsettings = new Loansettings();
    if (settings != null && settings != "") {
      // if settings exist then use these
      _objsettings = JSON.parse(settings) as Loansettings;
    } else {
      // else load user's preferences and save in loan setting
      this.loanmodel.LoanMaster.Loan_Settings = JSON.stringify(
        getLoanSettingFromMyPreferences(_objsettings)
      );
    }
    /// takes 1 sec to load and gt column and grid api

    if (_objsettings.insurance_policy_Settings == undefined) {
      _objsettings.insurance_policy_Settings = new Insurance_Policy_Settings();
    }
    if (Object.keys(_objsettings.insurance_policy_Settings.policy_visibility_state).length == 0) {
      _objsettings.insurance_policy_Settings.policy_visibility_state = new Insurance_Policy_Tabs();
    }
    Object.keys(_objsettings.insurance_policy_Settings.policy_visibility_state).forEach(element => {
      this.Insurancetoggles[element] = _objsettings.insurance_policy_Settings.policy_visibility_state[element];
      //this.hideunhideexternally(element);
    });

    this.hrInclusion = JSON.parse(this.loanmodel.LoanMaster.Loan_Settings).HrInclusion;
  }
  ngOnInit() {

    this.loanmodel = this.loanobj;
    this.retrieveErrors();

    this.pubsubService.subscribeToEvent().pipe(debounceTime(1000)).subscribe((event) => {

      if (event.name === 'event.insurance.setting.updated') {
        this.hideUnhidetheColumns(event.data.checkboxEvent, event.data.value);
      }
      if (event.name === 'event.insurance.HR.updated') {
        this.toggleHRinclusion(event.data.checkboxEvent);
      }
    });

  }
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }


  getgriddata(rendercolumn: boolean = true) {

    this.getAgents();
    this.getAgencies();
    this.getAIPs();

    //Get localstorage first
    let _rowData = [];

    if (this.loanmodel != null) {

      //let hassync=-false;
      _rowData = this.preparedata();
      if (rendercolumn) {
        this.rowData = _rowData;
      } else {
        //
        if (this.methodfromcheckboxinvoked) {
          this.methodfromcheckboxinvoked = false;
          this.rowData = _rowData;
        } else {
          //when we select checkbox it will go to catch
          try {
            let rownode = this.gridApi.getRowNode(this.lastEditedRowid);
            rownode.setData(_rowData[rownode.rowIndex]);
          } catch {
            this.rowData = _rowData;
          }
        }
      }
      //get all custom rows to render

      let custompolicies = _.orderBy(this.loanmodel.LoanPolicies.filter(p => p.ActionStatus != 3 && p.IsCustom == true), this.sortstrings);
      if (custompolicies != undefined) {

        let mpciPolicies = custompolicies.filter(p => p.Ins_Plan == 'MPCI');
        mpciPolicies.forEach(element => {
          this.processCustomPolicy(element, custompolicies);
        });
      }
      if (rendercolumn) {
        this.renderallcolumns();
      }
    }
    this.getWfrpData();
    if (this.gridApi != undefined) {
      this.applyalternateclassestogroups();
    }
    if (this.columnApi != undefined) {
      //autoSizeAll(this.columnApi);
    }
    //here render the delete columns
    // this.columnDefs.push(
    //   {
    //     headerName: '', width: 60, pickfield: "delete_custom", field: 'delete_custom', cellRendererSelector: function (params) {
    //       if (params.data.Custom) {
    //         return {
    //           component: 'deletecolumn'
    //         };
    //       } else {

    //       }
    //     }
    //   });
    //
    this.retrieveErrors();
    this.validationservice.highlighErrorCells(this.errorlist, 'Table_Policies');
  }


  public DeleteClicked(rowindex) {
    this.alertify
      .confirm('Confirm', 'Do you Really Want to Delete this Record?')
      .subscribe(res => {
        if (res == true) {
          //lets find the row number wrt the loan policies
          let _actualindex = rowindex - this.rowData.filter(p => p.Custom != true).length;
          //keep order same
          let _policy = _.orderBy(this.loanmodel.LoanPolicies.filter(p => p.ActionStatus != 3 && p.IsCustom == true), this.sortstrings)[_actualindex];
          if (_policy.Loan_Policy_ID < 0) {
            let orderindex = (this.loanmodel.LoanPolicies.indexOf(_policy));
            this.loanmodel.LoanPolicies.splice(orderindex, 1);
          } else {
            _policy.ActionStatus = 3;
          }
          //using the same rowvaluechanged event here
          this.loanmodel.srccomponentedit = "PoliciesComponent";
          this.loanmodel.lasteditrowindex = rowindex;
          this.updateSyncStatus();
          this.loancalculationservice.performcalculationonloanobject(this.loanmodel);
          this.publishService.enableSync(Page.insurance);
        }
      });

  }


  private preparedata() {

    let _rowData = [];
    let _insurancepolicies = _.orderBy(this.loanmodel.LoanPolicies.filter(p => p.ActionStatus != 3 && p.IsCustom != true), this.sortstrings);

    //Get HR Inclusion here
    // if (this.HRinclusion != true) {
    //   insurancepolicies = insurancepolicies.filter(p => p.Rated == "1");
    // }
    let _cropUnits = _.uniqBy(this.loanmodel.LoanCropUnits, p => p.Crop_Unit_Key);
    _cropUnits.forEach(CU => {
      //Lets FInd the MPCI Policy for this Cropunit Record
      let HRiteratee = [];
      if (this.hrInclusion) {
        HRiteratee = [0, 1];
      } else {
        HRiteratee = [0];
      }
      HRiteratee.forEach(HR => {
        let item = _.orderBy(_insurancepolicies.filter(p => p.Policy_Key.includes(CU.Crop_Unit_Key) && p.Ins_Plan == "MPCI" && p.HR_Exclusion_Ind == HR), p => p.Select_Ind, 'desc')[0];
        if (item != undefined) {
          //Supolicies
          let Subpolicies = _.uniqBy(_insurancepolicies.filter(p => p.Policy_Key.includes(CU.Crop_Unit_Key) && p.Ins_Plan != "MPCI" && p.HR_Exclusion_Ind == HR), p => p.Ins_Plan);
          let row: any = {};
          row.MPCI_Enabled = item.Select_Ind;
          row.CUID = CU.Loan_CU_ID;
          row.mainpolicyId = item.Loan_Policy_ID;
          row.Rated = item.HR_Exclusion_Ind;
          row.FarmRated = CU.FC_Rating;
          row.Section = CU.FC_Section;
          row.HighlyRated = item.Eligible_Ind;
          if ((item.Agent_ID == 0 || item.Agent_ID == undefined) && this.agencies.length == 1) {
            item.Agent_ID = this.agencies[0].key;
          } else {
            if (this.agents.find(p => p.key == item.Agent_ID) == undefined && this.agents.length > 0) {
              item.Agent_ID = this.agents[0].key;
            }
            if (this.agents.find(p => p.key == item.Agent_ID) != undefined && this.agents.length > 0) {
              //item.Agency_Id=this.agencies[0].key;
            } else if (this.agents.find(p => p.key == item.Agent_ID) == undefined && this.agents.length == 0) {
              item.Agent_ID = 0;
            }
            //else
            // item.Agent_ID = 0;
          }
          row.Agent_Id_MPCI = item.Agent_ID;
          if ((item.Agency_ID == 0 || item.Agency_ID == undefined) && this.agencies.length == 1) {
            item.Agency_ID = this.agencies[0].key;
          } else {
            if (this.agencies.find(p => p.key == item.Agency_ID) == undefined && this.agencies.length > 0) {
              item.Agency_ID = this.agencies[0].key;
            }
            if (this.agencies.find(p => p.key == item.Agency_ID) != undefined && this.agencies.length > 0) {
              //item.Agency_Id=this.agencies[0].key;
            } else if (this.agencies.find(p => p.key == item.Agency_ID) == undefined && this.agencies.length == 0) {
              item.Agency_ID = 0;
            }
            //else
            //item.Agency_ID = 0;
          }
          row.Agency_Id_MPCI = item.Agency_ID;
          if ((item.AIP_ID == 0 || item.AIP_ID == undefined) && this.ProposedAips.length == 1) {
            item.AIP_ID = this.ProposedAips[0].key;
          } else {
            if (this.ProposedAips.find(p => p.key == item.AIP_ID) == undefined && this.ProposedAips.length > 0) {
              item.AIP_ID = this.ProposedAips[0].key;
            }

          }
          row.Custom = item.IsCustom == undefined ? false : item.IsCustom;
          //
          // let valueAPI = this.checkforAIP('MPCI');
          // if (valueAPI == -1) {
          row.ProposedAIP_MPCI = item.AIP_ID;
          row.Countyid = item.County_ID;
          row.ActionStatus = item.ActionStatus;
          row.State_ID = item.State_ID;
          row.County_ID = item.County_ID;
          row.FC_County = lookupCountyValueFromPassedRefData(row.County_ID, this.refdata);
          row.FC_StateCode = lookupStateCodeFromPassedRefObject(row.State_ID, this.refdata);
          row.CropName = getcropnamebycropid(item.Crop_Code, this.refdata);
          row.Croptype = item.Crop_Type_Code;
          row.Practice = item.Crop_Practice_Type_Code;
          row.Subtype_MPCI = item.Ins_Plan_Type;
          row.Subtype_MPCI_Verify = item['Ins_Plan_Type_Verify'] || null;
          row.SecInsurance = _.uniqBy(Subpolicies.filter(p => p.ActionStatus != 3), "Ins_Plan").map(p => p.Ins_Plan).join(',');
          row.Unit_MPCI = item.Ins_Unit_Type_Code;
          row.Unit_MPCI_Verify = item['Ins_Unit_Type_Code_Verify'] || null;
          row.Option_MPCI = item.Option;
          row.Option_MPCI_Verify = item['Option_Verify'] || null;
          row.YieldProt_Pct_MPCI = item.Yield_Percent;
          row.YieldProt_Pct_MPCI_Verify = item['Yield_Percent_Verify'] || null;
          row.PriceProt_Pct_MPCI = item.Price_Percent;
          row.PriceProt_Pct_MPCI_Verify = item['Price_Percent_Verify'] || null;
          row.Level_MPCI = item.Upper_Level;
          row.Verified_MPCI = item.isVerified;
          row.Level_MPCI_Verify = item['Upper_Level_Verify'] || null;
          if (item.Price == undefined) {
            row.Price_MPCI = 0;
          }
          // else {
          //   if(this.loanobj.LoanMaster.Crop_Year >= 2019) {
          // if(this.pricesyncneeded)
          //   this.Set_Price(item);
          row.Price_MPCI = item.Price;
          // }
          let loanitem = this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == item.Loan_Policy_ID);
          loanitem.Price = row.Price_MPCI;
          if (loanitem.ActionStatus != 1) {
            loanitem.ActionStatus = 2;
            // hassync=true;
            // }
          }
          // Object.keys(item).filter(p => p.includes('_Verify')).forEach(element => {
          // });
          Subpolicies.filter(p => p.ActionStatus != 3).forEach(policy => {
            {
              row["Subtype_" + policy.Ins_Plan.toString()] = policy.Ins_Plan_Type;
              row["Subtype_" + policy.Ins_Plan.toString() + "_Verify"] = policy['Ins_Plan_Type_Verify'];
            }

            let renderedvalues = getpossiblecombinationsforpolicy(policy.Ins_Plan);
            renderedvalues.forEach(element => {
              if (!element.includes("SubType")) {
                let tobindcol = getApplicableProperty(element.substr(0, element.lastIndexOf(policy.Ins_Plan) - 1));
                row[element] = policy[tobindcol];
                row[element + "_Verify"] = policy[tobindcol + "_Verify"];
                row.ActionStatus = policy.ActionStatus;
              }
            });
            row["Verified_" + policy.Ins_Plan] = policy.isVerified;
            if ((policy["Agent_ID"] == 0 || policy["Agent_ID"] == undefined) && this.agents.length == 1) {
              policy["Agent_ID"] = this.agents[0].key;
            } else {
              if (this.agents.find(p => p.key == policy["Agent_ID"]) == undefined && this.agents.length > 0) {
                policy["Agent_ID"] = this.agents[0].key;
              }
              if (this.agents.find(p => p.key == item.Agent_ID) != undefined && this.agents.length > 0) {
                //item.Agency_Id=this.agencies[0].key;
              } else if (this.agents.find(p => p.key == policy["Agent_ID"]) == undefined && this.agents.length == 0) {
                policy["Agent_ID"] = 0;
              }
              //else
              // policy["Agent_ID"] = 0;
            }
            row["Agent_Id_" + policy.Ins_Plan] = policy["Agent_ID"];
            if ((policy["Agency_ID"] == 0 || policy["Agency_ID"] == undefined) && this.agencies.length == 1) {
              policy["Agency_ID"] = this.agencies[0].key;
            } else {
              if (this.agencies.find(p => p.key == policy["Agency_ID"]) == undefined && this.agencies.length > 0) {
                policy["Agency_ID"] = this.agencies[0].key;
              }
              if (this.agencies.find(p => p.key == policy["Agency_ID"]) != undefined && this.agencies.length > 0) {
                //item.Agency_Id=this.agencies[0].key;
              } else if (this.agencies.find(p => p.key == policy["Agency_ID"]) == undefined && this.agencies.length == 0) {
                policy["Agency_ID"] = 0;
              }
              // else
              //  policy["Agency_ID"] = 0;
            }

            row["Agency_Id_" + policy.Ins_Plan] = policy["Agency_ID"];
            if ((policy["AIP_ID"] == 0 || policy["AIP_ID"] == undefined) && this.ProposedAips.length == 1) {
              policy["AIP_ID"] = this.ProposedAips[0].key;
            } else {
              if (this.ProposedAips.find(p => p.key == policy["AIP_ID"]) == undefined && this.ProposedAips.length > 0) {
                policy["AIP_ID"] = this.ProposedAips[0].key;
              }
              if (this.ProposedAips.find(p => p.key == policy["AIP_ID"]) != undefined && this.ProposedAips.length > 0) {
                //item.Agency_Id=this.agencies[0].key;
              } else if (this.ProposedAips.find(p => p.key == policy["AIP_ID"]) == undefined && this.ProposedAips.length == 0) {
                policy["AIP_ID"] = 0;
              }
              //else
              // policy["AIP_ID"] = 0;
            }
            row["Aip_" + policy.Ins_Plan] = policy["AIP_ID"];
            row["Unit_" + policy.Ins_Plan] = policy.Ins_Unit_Type_Code;
            row["Unit_" + policy.Ins_Plan + "_Verify"] = policy["Ins_Unit_Type_Code_Verify"];
            //row["Option_" + policy.Ins_Plan] = policy["Option"];
          });
          row.Premium_MPCI = item.Premium;
          row.Premium_MPCI_Verify = item['Premium_Verify'] || null;
          Subpolicies.forEach(element => {
            if (element.ActionStatus != 3) {
              row[element.Ins_Plan] = true;
            }
          });
          _rowData.push(row);
        } else {
        }
      });
    });

    let rows = _.orderBy(_rowData, this.sortstrings);
    return rows;
  }


  private needsaveonunique = false;
  //check for AIP in policy and Assign
  checkforAIP(policytype) {


    let _uniqueAipitem = this.refdata.Ref_Ins_Policy_Plans.find(p => p.Ins_Plan.toLowerCase() == policytype.toLowerCase());
    if (_uniqueAipitem != null && _uniqueAipitem.Unique_AIP_ID != null && _uniqueAipitem.Unique_AIP_ID != 0) {

      let item = this.loanmodel.Association.find(p => p.Ref_Assoc_ID == _uniqueAipitem.Unique_AIP_ID);
      if (item != null) {

        //return item.Assoc_ID;
        this.loanmodel.LoanPolicies.filter(p => p.Ins_Plan == policytype).forEach(element => {
          element.AIP_ID = item.Assoc_ID;
        });
        this.getgriddata();
        this.dataService.setLoanObjectwithoutsubscription(this.loanmodel);
        this.needsaveonunique = true;
      } else {

        //add here
        let toadditem = this.refdata.RefAssociations.find(p => p.Ref_Association_ID == _uniqueAipitem.Unique_AIP_ID);
        if (toadditem != null) {
          let itemtoadd = assignAssocDetails(toadditem);
          itemtoadd.ActionStatus = 1;
          itemtoadd.Loan_Full_ID = this.loanmodel.Loan_Full_ID;
          itemtoadd.Ref_Assoc_ID = _uniqueAipitem.Unique_AIP_ID;
          itemtoadd.Assoc_Name = toadditem.Assoc_Name;
          itemtoadd.Assoc_Type_Code = AssociationTypeCode.AIP;
          this.loanapi.SaveAssociationImmediate(itemtoadd).subscribe(res => {
            itemtoadd.Assoc_ID = res.Data;
            let toaddAIp = res.Data;
            if (this.loanmodel.Association.find(p => p.Ref_Assoc_ID == itemtoadd.Ref_Assoc_ID) == null) {
            this.loanmodel.Association.push(itemtoadd);
            } else {
              toaddAIp = this.loanmodel.Association.find(p => p.Ref_Assoc_ID == itemtoadd.Ref_Assoc_ID).Assoc_ID;
            }
            this.loanmodel.LoanPolicies.filter(p => p.Ins_Plan == policytype).forEach(element => {
              element.AIP_ID = toaddAIp;
            });
            this.needsaveonunique = true;
            this.dataService.setLoanObjectwithoutsubscription(this.loanmodel);
            this.getgriddata();
          });
          return -2;
        } else {
          return -1;
        }
      }
    } else {
      return -1;
    }

  }

  assignAiponcreate(policytype) {

    let _uniqueAipitem = this.refdata.Ref_Ins_Policy_Plans.find(p => p.Ins_Plan.toLowerCase() == policytype.toLowerCase());
    if (_uniqueAipitem != null && _uniqueAipitem.Unique_AIP_ID != null && _uniqueAipitem.Unique_AIP_ID != 0) {

      let item = this.loanmodel.Association.find(p => p.Ref_Assoc_ID == _uniqueAipitem.Unique_AIP_ID);
      if (item != null) {

        return item.Assoc_ID;

      } else {
        return null;
      }
  }
  return null;
}
  processCustomPolicy(item: Loan_Policy, insurancepolicies: Array<Loan_Policy>) {

    if (item != undefined) {
      //Supolicies
      let Subpolicies = _.uniqBy(insurancepolicies.filter(p => p.Policy_Key == item.Policy_Key && p.IsCustom == true && p.Ins_Plan != 'MPCI'), p => p.Ins_Plan);
      let row: any = {};
      row.MPCI_Enabled = item.Select_Ind;
      row.CUID = 0;
      row.mainpolicyId = item.Loan_Policy_ID;
      row.Rated = item.HR_Exclusion_Ind;
      row.FarmRated = null;
      row.Section = "";
      row.HighlyRated = false;
      if ((item.Agent_ID == 0 || item.Agent_ID == undefined) && this.agencies.length == 1) {
        item.Agent_ID = this.agencies[0].key;
      } else {
        if (this.agencies.find(p => p.key == item.Agent_ID) == undefined && this.agencies.length > 0) {
          item.Agent_ID = this.agencies[0].key;
        }
        if (this.agencies.find(p => p.key == item.Agent_ID) != undefined && this.agencies.length > 0) {
          //item.Agency_Id=this.agencies[0].key;
        } else if (this.agencies.find(p => p.key == item.Agent_ID) == undefined && this.agencies.length == 0) {
          item.Agent_ID = 0;
        } else {
          item.Agent_ID = 0;
        }
      }

      row.Agent_Id_MPCI = item.Agent_ID;

      if ((item.Agency_ID == 0 || item.Agency_ID == undefined) && this.agencies.length == 1) {
        item.Agency_ID = this.agencies[0].key;
      } else {
        if (this.agencies.find(p => p.key == item.Agency_ID) == undefined && this.agencies.length > 0) {
          item.Agency_ID = this.agencies[0].key;
        }

        if (this.agencies.find(p => p.key == item.Agency_ID) != undefined && this.agencies.length > 0) {
          //item.Agency_Id=this.agencies[0].key;
        } else if (this.agencies.find(p => p.key == item.Agency_ID) == undefined && this.agencies.length == 0) {
          item.Agency_ID = 0;
        } else {
          item.Agency_ID = 0;
        }
      }
      row.Agency_Id_MPCI = item.Agency_ID;

      if ((item.AIP_ID == 0 || item.AIP_ID == undefined) && this.ProposedAips.length == 1) {
        item.AIP_ID = this.ProposedAips[0].key;
      } else {
        if (this.ProposedAips.find(p => p.key == item.AIP_ID) == undefined && this.ProposedAips.length > 0) {
          item.AIP_ID = this.ProposedAips[0].key;
        }
        if (this.ProposedAips.find(p => p.key == item.AIP_ID) != undefined && this.ProposedAips.length > 0) {
          //item.Agency_Id=this.agencies[0].key;
        } else if (this.ProposedAips.find(p => p.key == item.AIP_ID) == undefined && this.ProposedAips.length == 0) {
          item.AIP_ID = 0;
        } else {
          item.AIP_ID = 0;
        }
      }
      row.Custom = item.IsCustom == undefined ? false : item.IsCustom;
      row.ProposedAIP_MPCI = item.AIP_ID;
      row.Countyid = item.County_ID;
      row.ActionStatus = item.ActionStatus;
      row.State_ID = item.State_ID;

      row.County_ID = item.County_ID;
      row.CropName = item.Crop_Code == "" ? "" : (getcropnamebycropid(item.Crop_Code, this.refdata));
      row.Croptype = item.Crop_Type_Code;
      row.Practice = item.Crop_Practice_Type_Code;
      row.Subtype_MPCI = item.Ins_Plan_Type;
      row.Subtype_MPCI_Verify = item['Ins_Plan_Type_Verify'] || null;
      row.FC_County = lookupCountyValueFromPassedRefData(row.County_ID, this.refdata);
      row.FC_StateCode = lookupStateCodeFromPassedRefObject(row.State_ID, this.refdata);
      row.SecInsurance = _.uniqBy(Subpolicies.filter(p => p.ActionStatus != 3), "Ins_Plan").map(p => p.Ins_Plan).join(',');
      row.Unit_MPCI = item.Ins_Unit_Type_Code;
      row.Unit_MPCI_Verify = item['Ins_Unit_Type_Code_Verify'] || null;
      row.Option_MPCI = item.Option;
      row.Option_MPCI_Verify = item['Option_Verify'] || null;
      row.YieldProt_Pct_MPCI = item.Yield_Percent;
      row.YieldProt_Pct_MPCI_Verify = item['Yield_Percent_Verify'] || null;
      row.PriceProt_Pct_MPCI = item.Price_Percent;
      row.PriceProt_Pct_MPCI_Verify = item['Price_Percent_Verify'] || null;
      row.Level_MPCI = item.Upper_Level;
      row.Verified_MPCI = item.isVerified;
      row.Level_MPCI_Verify = item['Upper_Level_Verify'] || null;
      if (item.Price == undefined) {
        row.Price_MPCI = 0;
      }
      this.Set_Price(item);
      row.price = item.Price;
      row.Price_MPCI = item.Price;

      let loanitem = this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == item.Loan_Policy_ID);
      if (loanitem) {
        loanitem.Price = row.Price;
        if (loanitem.ActionStatus != 1) {
          loanitem.ActionStatus = 2;
          // hassync=true;
        }
      }

      Object.keys(item).filter(p => p.includes('_Verify')).forEach(() => {

      });

      Subpolicies.filter(p => p.ActionStatus != 3).forEach(policy => {
        {
          row["Subtype_" + policy.Ins_Plan.toString()] = policy.Ins_Plan_Type;
          row["Subtype_" + policy.Ins_Plan.toString() + "_Verify"] = policy['Ins_Plan_Type_Verify'];
        }
        let renderedvalues = getpossiblecombinationsforpolicy(policy.Ins_Plan);

        renderedvalues.forEach(element => {
          if (!element.includes("SubType")) {
            let tobindcol = getApplicableProperty(element.substr(0, element.lastIndexOf(policy.Ins_Plan) - 1));
            row[element] = policy[tobindcol];
            row[element + "_Verify"] = policy[tobindcol + "_Verify"];
            row.ActionStatus = policy.ActionStatus;
          }
        });

        row["Verified_" + policy.Ins_Plan] = policy.isVerified;
        if ((policy["Agent_ID"] == 0 || policy["Agent_ID"] == undefined) && this.agencies.length == 1) {
          policy["Agent_ID"] = this.agencies[0].key;
        } else {
          if (this.agencies.find(p => p.key == policy["Agent_ID"]) == undefined && this.agencies.length > 0) {
            policy["Agent_ID"] = this.agencies[0].key;
          }
          if (this.agencies.find(p => p.key == item.Agent_ID) != undefined && this.agencies.length > 0) {
            //item.Agency_Id=this.agencies[0].key;
          } else if (this.agencies.find(p => p.key == policy["Agent_ID"]) == undefined && this.agencies.length == 0) {
            policy["Agent_ID"] = 0;
          } else {
            policy["Agent_ID"] = 0;
          }
        }
        row["Agent_Id_" + policy.Ins_Plan] = policy["Agent_ID"];

        if ((policy["Agency_ID"] == 0 || policy["Agency_ID"] == undefined) && this.agencies.length == 1) {
          policy["Agency_ID"] = this.agencies[0].key;
        } else {
          if (this.agencies.find(p => p.key == policy["Agency_ID"]) == undefined && this.agencies.length > 0) {
            policy["Agency_ID"] = this.agencies[0].key;
          }

          if (this.agencies.find(p => p.key == policy["Agency_ID"]) != undefined && this.agencies.length > 0) {
            //item.Agency_Id=this.agencies[0].key;
          } else if (this.agencies.find(p => p.key == policy["Agency_ID"]) == undefined && this.agencies.length == 0) {
            policy["Agency_ID"] = 0;
          } else {
            policy["Agency_ID"] = 0;
          }
        }
        row["Agency_Id_" + policy.Ins_Plan] = policy["Agency_ID"];

        if ((policy["AIP_ID"] == 0 || policy["AIP_ID"] == undefined) && this.ProposedAips.length == 1) {
          policy["AIP_ID"] = this.ProposedAips[0].key;
        } else {
          if (this.ProposedAips.find(p => p.key == policy["AIP_ID"]) == undefined && this.ProposedAips.length > 0) {
            policy["AIP_ID"] = this.ProposedAips[0].key;
          }
          if (this.ProposedAips.find(p => p.key == policy["AIP_ID"]) != undefined && this.ProposedAips.length > 0) {
            //item.Agency_Id=this.agencies[0].key;
          } else if (this.ProposedAips.find(p => p.key == policy["AIP_ID"]) == undefined && this.ProposedAips.length == 0) {
            policy["AIP_ID"] = 0;
          } else {
            policy["AIP_ID"] = 0;
          }
        }

        row["Aip_" + policy.Ins_Plan] = policy["AIP_ID"];
        row["Unit_" + policy.Ins_Plan] = policy.Ins_Unit_Type_Code;
        row["Unit_" + policy.Ins_Plan + "_Verify"] = policy["Ins_Unit_Type_Code_Verify"];
        //row["Option_" + policy.Ins_Plan] = policy["Option"];
      });
      row.Premium_MPCI = item.Premium;
      row.Premium_MPCI_Verify = item['Premium_Verify'] || null;

      Subpolicies.forEach(element => {
        if (element.ActionStatus != 3) {
          row[element.Ins_Plan] = true;
        }
      });

      this.rowData.push(row);
      return row;
    }
  }
  ///WFRP
  getWfrpData() {

    let rows = [];
    let row: any = {};
    row.ispinned = true;

    row.Wfrp_Enabled = this.loanmodel.Loanwfrp.Wfrp_Enabled;
    this.wfrpEnabled = row.Wfrp_Enabled;
    row.wfrp_revenue = this.loanmodel.Loanwfrp.Approved_Revenue;
    row.wfrp_level = this.loanmodel.Loanwfrp.Level;
    row.wfrp_premium = this.loanmodel.Loanwfrp.Premium;
    row.wfrp_aip = this.loanmodel.Loanwfrp.Proposed_AIP;
    row.wfrp_Agency_Id = this.loanmodel.Loanwfrp.Agency_ID;
    row.wfrp_Agent_Id = this.loanmodel.Loanwfrp.Agent_ID;
    rows.push(row);
    this.pinnedBottomRowData = rows;
  }

  //DB Operations
  updatelocalloanobject(event: any): any {

    //update the polices here
    if (event.data.Custom != true) {
      let isverify = false;
      let policy = event.column.colDef.pickfield;
      if (policy.includes('_Verify')) {
        isverify = true;
        policy = policy.replace('_Verify', '');
      }
      policy = policy.substr(policy.lastIndexOf('_') + 1, policy.length - policy.lastIndexOf('_'));
      let actualpolicy = CompareandgetSubPolicy(event.data.mainpolicyId, policy, this.loanmodel);
      if (actualpolicy != undefined) {
        let apppolicy = getApplicableProperty(event.column.colDef.pickfield.substr(0, event.column.colDef.pickfield.lastIndexOf(policy) - 1));
        if (isverify) {
          actualpolicy[apppolicy + '_Verify'] = event.value;
        } else {
          actualpolicy[apppolicy] = event.value;
          try {
            actualpolicy.isVerified = false;
            this.loanmodel.LoanMaster.AIP_Verified = false;
          } catch {
            //verify doesnt exist for policy
          }
        }
        if (actualpolicy.ActionStatus != 1) {
          actualpolicy.ActionStatus = 2;
        }


        this.validationservice.Validate_Insurance(event, this.loanmodel.LoanPolicies, this.insuranceValidationData);

        this.publishService.enableSync(Page.insurance);
        this.retrieveErrors();
      }
    } else {

      let property = event.column.colDef.pickfield;
      if (property == 'State_ID' || property == 'County_ID' || property == 'CropName' || property == 'Practice' || property == 'FC_County') {
        //Get all subpolicies and change keys for them also
        let mpcipolicy = this.loanmodel.LoanPolicies.find(p => p.Loan_Policy_ID == event.data.mainpolicyId);
        let subpolices = this.loanmodel.LoanPolicies.filter(p => p.Policy_Key == mpcipolicy.Policy_Key);
        switch (property) {
          case 'State_ID':
            mpcipolicy.State_ID = event.value;
            break;
          case 'County_ID':
            mpcipolicy.County_ID = event.value;
            break;
          case 'CropName':
            mpcipolicy.Crop_Code = event.value;
            break;
          case 'Practice':
            mpcipolicy.Crop_Practice_Type_Code = event.value;
            break;
          case 'FC_County':
            mpcipolicy.FC_County = event.value;
            mpcipolicy.County_ID = event.data.County_ID;
            break;
          default:
            //all other here
            break;
        }
        mpcipolicy.Policy_Key = lookupStateValue(mpcipolicy.State_ID, this.refdata) + "_" + lookupCountyValue(mpcipolicy.County_ID, this.refdata) + "_" + mpcipolicy.Crop_Code + "_" + mpcipolicy.Crop_Practice_Type_Code + (mpcipolicy.HR_Exclusion_Ind == true ? '_HR' : '');
        subpolices.forEach(element => {
          element.Policy_Key = mpcipolicy.Policy_Key;
        });
        if (mpcipolicy.ActionStatus != 1) {
          mpcipolicy.ActionStatus = 2;
        }
      } else {
        let policy = event.column.colDef.pickfield;
        policy = policy.substr(policy.lastIndexOf('_') + 1, policy.length - policy.lastIndexOf('_'));
        let actualpolicy = CompareandgetSubPolicyCustom(event.data.mainpolicyId, policy, this.loanmodel);
        let apppolicy = getApplicableProperty(event.column.colDef.pickfield.substr(0, event.column.colDef.pickfield.lastIndexOf(policy) - 1));
        actualpolicy[apppolicy] = event.value;
        if (actualpolicy.ActionStatus != 1) {
          actualpolicy.ActionStatus = 2;
        }
        try {
          actualpolicy.isVerified = false;
          this.loanmodel.LoanMaster.AIP_Verified = false;

        } catch {
          //verify doesnt exist for policy
        }
      }

    }
    this.gridApi.redrawRows();

  }

  syncenabled() {
    return false;
  }

  rowvaluechanged($event) {
    // settimeStart("Editing Started /////////////////////////////////////")
    this.lastEditedRowid = $event.node.id;
    // // if ($event.data.Custom==false&&$event.column.parent.originalColumnGroup.colGroupDef.headerName != "MPCI" && $event.column.parent.originalColumnGroup.colGroupDef.headerName != "WFRP" && !$event.data[$event.column.parent.originalColumnGroup.colGroupDef.headerName] && $event.column.colId != "HighlyRated") {
    // //   return;
    // // }
    let modifiedvalues = this.localstorage.retrieve(environment.modifiedbase) as Array<String>;

    if (!modifiedvalues.includes("Ins_" + $event.data.mainpolicyId + "_" + $event.colDef.field)) {
      modifiedvalues.push("Ins_" + $event.data.mainpolicyId + "_" + $event.colDef.field);
      this.localstorage.store(environment.modifiedbase, modifiedvalues);
    }
    if ($event.data.ispinned) {
      //update value here WFRP

      if ($event.colDef.pickfield == 'checkbox') {
        this.loanmodel.Loanwfrp.Wfrp_Enabled = $event.value;
      }
      if ($event.colDef.field == 'wfrp_revenue') {
        this.loanmodel.Loanwfrp.Approved_Revenue = parseFloat($event.value);
      }
      if ($event.colDef.field == 'wfrp_level') {
        this.loanmodel.Loanwfrp.Level = $event.value;
      }
      if ($event.colDef.field == 'wfrp_premium') {
        this.loanmodel.Loanwfrp.Premium = parseFloat($event.value);
      }
      if ($event.colDef.field == 'wfrp_aip') {
        this.loanmodel.Loanwfrp.Proposed_AIP = $event.value;
      }
      if ($event.colDef.field == 'wfrp_Agency_Id') {
        this.loanmodel.Loanwfrp.Agency_ID = $event.value;
      }
      if ($event.colDef.field == 'wfrp_Agent_Id') {
        this.loanmodel.Loanwfrp.Agent_ID = $event.value;
      }
      this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
      this.publishService.enableSync(Page.insurance);
    } else {


      // // Options


      // //MODIFIED YELLOW VALUES

      // //MODIFIED YELLOW VALUES




      this.updatelocalloanobject($event);
      this.retrieveErrors();
      this.loanmodel.srccomponentedit = "PoliciesComponent";
      this.loanmodel.lasteditrowindex = $event.rowIndex;
      this.updateSyncStatus();
      this.publishService.enableSync(Page.insurance);

      if ($event.data.Custom != true) {
        // settimeStart("Saving  Started /////////////////////////////////////")
        this.loancalculationservice.performcalculationonloanobject(this.loanmodel);
      } else {
        this.dataService.setLoanObjectwithoutsubscription(this.loanmodel);
      }

    }




  }

  onGridReady(params) {

    // settimeStart("OngridReady Started");
    if (environment.isDebugModeActive) { console.time('onGridReady start Time'); }
    this.gridApi = params.api;
    this.columnApi = params.columnApi;

    let preferences = this.settingsService.preferences;
    this.insurancePlanSettings = preferences.loanSettings.insurancePlanSettings;
    this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
    this.hideUnhideLoanKeys();
    this.loadinitialtabsfromcontext(this.loanmodel.LoanMaster.Loan_Settings);
    this.declarecoldefs();
    this.checkforAIP('MPCI');

    setmodifiedallIns(this.localstorage.retrieve(environment.modifiedbase), "Ins_");
    //
    this.verificationHelper.setapis(this.columnApi, this.gridApi);
    let that = this;
    this.gridApi.addEventListener('displayedColumnsChanged', function () {
      let state: any = that.columnApi.getColumnState();

      // if(state.filter(p=>p.hide==false).length>0)
      // {
      let obj = JSON.parse(that.loanmodel.LoanMaster.Loan_Settings) as LoanSettings;
      if (obj.Table_States == undefined || obj.Table_States == null) {
        obj.Table_States = new Array<TableState>();
      }
      let data = obj.Table_States.find(p => p.address.page == Page.insurance && p.address.chevron == Chevrons.insurance_policies);
      if (data != undefined) {
        data.data = state;
      } else {
        let farm_farmstate = new TableState();
        farm_farmstate.address = new TableAddress();
        farm_farmstate.address.page = Page.insurance;
        farm_farmstate.address.chevron = Chevrons.insurance_policies;
        farm_farmstate.data = state;
        obj.Table_States.push(farm_farmstate);
      }

      that.loanmodel.LoanMaster.Loan_Settings = JSON.stringify(obj);
      window.localStorage.setItem("ng2-webstorage|currentselectedloan", JSON.stringify(that.loanmodel));


    });
    let loansettingd = (JSON.parse(this.loanobj.LoanMaster.Loan_Settings) as LoanSettings);
    if (loansettingd.Table_States != undefined && loansettingd.Table_States != null) {
    }

    this.validationservice.Validate_Insurance(event, this.loanmodel.LoanPolicies, this.insuranceValidationData);

    setTimeout(() => {
      //this.toggleHRinclusion({ checked: this.HRinclusion });
      this.applyalternateclassestogroups();
      this.toggleAgentsAipsAgency();
      moveVerificationColumntolast(this.columnApi);
      this.retrieveErrors();
      setmodifiedallIns(this.localstorage.retrieve(environment.modifiedbase), "Ins_");
    }, 50);
    if (environment.isDebugModeActive) { console.timeEnd('onGridReady start Time'); }
  }

  adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidthsins(this.columnApi);
    }
  }

  displayColumnsChanged($event) {
    if (this.columnApi != undefined && $event != undefined) {
      //first One to save to Loan Settings
      let _loanSettings: Loansettings = JSON.parse(this.loanmodel.LoanMaster.Loan_Settings);
      if (_loanSettings == undefined || _loanSettings == null) {
        _loanSettings = new Loansettings();
      }
      if (_loanSettings.Loan_key_Settings == undefined || _loanSettings.Loan_key_Settings == null) {
        _loanSettings.Loan_key_Settings = new Loan_Key_Visibilty();
      }
      //Since we dont have column thats changed so lets see manually
      this.Loankeys.forEach(key => {
        let mainkey = "";
        switch (key) {
          case "Croptype":
            mainkey = "Crop_Type";
            break;
          case "Practice":
            mainkey = "Irr_Practice";
            break;
        }

        let column = this.columnApi.getColumn(key);
        _loanSettings.Loan_key_Settings[mainkey] = column.visible;
      });
      this.loanmodel.LoanMaster.Loan_Settings = JSON.stringify(_loanSettings);
      //window.localStorage.setItem("ng2-webstorage|currentselectedloan", JSON.stringify(this.loanmodel));
      this.dataService.setLoanObjectwithoutsubscription(this.loanmodel);
      // this.publishService.enableSync(this.currentPageName);
      //Second Execution
      this.retrieveErrors();
      this.adjustToFitAgGrid();
    }
  }

  hideUnhideLoanKeys() {

    let _loanSettings: Loansettings = JSON.parse(this.loanmodel.LoanMaster.Loan_Settings);
    if (_loanSettings != undefined || _loanSettings != null) {
      if (_loanSettings.Loan_key_Settings != undefined || _loanSettings.Loan_key_Settings != null) {
        this.loankeySettings = _loanSettings.Loan_key_Settings;

      } else {
        this.loankeySettings = new Loan_Key_Visibilty();
      }
    } else {
      this.loankeySettings = new Loan_Key_Visibilty();
    }
    this.columnsDisplaySettings.cropType = this.loankeySettings.Crop_Type == null ? this.columnsDisplaySettings.cropType : this.loankeySettings.Crop_Type;
    this.columnsDisplaySettings.irrPrac = this.loankeySettings.Irr_Practice == null ? this.columnsDisplaySettings.irrPrac : this.loankeySettings.Irr_Practice;
    this.columnsDisplaySettings.cropPrac = this.loankeySettings.Crop_Practice == null ? this.columnsDisplaySettings.cropPrac : this.loankeySettings.Crop_Practice;
  }

  displayedColumnsChanged() {
    this.adjustToFitAgGrid();
  }

  isLoanEditable() {
    return !this.loanobj || this.loanobj.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  //update Loan Status
  updateSyncStatus() {
    if (environment.isDebugModeActive) { console.time('updateSyncStatus start Time'); }
    if (this.checkforstatus([1, 3])) {
      this.syncInsuranceStatus = Sync_Status.ADDORDELETE;
    } else if (this.checkforstatus([2])) {
      this.syncInsuranceStatus = Sync_Status.EDITED;
    } else {
      this.syncInsuranceStatus = Sync_Status.NOCHANGE;
    }

    this.loanmodel.SyncStatus.Status_Insurance_Policies = this.syncInsuranceStatus;
    if (environment.isDebugModeActive) { console.timeEnd('updateSyncStatus start Time'); }
  }
  checkforstatus(statuscode: Array<number>) {


    let status = false;
    statuscode.forEach(element => {
      if (!status) {
        status = this.loanmodel.InsurancePolicies.filter(p => p.ActionStatus == element).length > 0;
        this.loanmodel.InsurancePolicies.forEach(element1 => {
          if (!status) {
            status = element1.Subpolicies.filter(p => p.ActionStatus == element).length > 0;
          }
        });
      }
    });
    return status;
  }

  //NEW FUNCTIONALITY
  //ADD ROWS
  addRow() {
    let row = new Loan_Policy();
    row.IsCustom = true;
    row.Ins_Plan = 'MPCI';
    row.Ins_Plan_Type = '';
    row.ActionStatus = 1;
    row.Crop_Code = "CRN";
    row.Crop_Type_Code = '-';
    row.Loan_Full_ID = this.loanmodel.Loan_Full_ID;
    row.Crop_Practice_Type_Code = 'Irr';
    row.Irr_Practice_Type_Code = 'Irr';
    row.State_ID = this.refdata.StateList[0].State_ID;
    row.County_ID = this.refdata.CountyList.filter(p => p.State_ID == row.State_ID)[0].County_ID;
    row.Loan_Policy_ID = -Math.abs(this.loanmodel.LoanPolicies.length + 1);
    row.Policy_Key = lookupStateValue(row.State_ID, this.refdata) + "_" + lookupCountyValue(row.County_ID, this.refdata) + "_" + row.Crop_Code + "_0_" + row.Irr_Practice_Type_Code + '_' + row.Crop_Practice_Type_Code + (row.HR_Exclusion_Ind == true ? '_HR' : '');
    this.Set_Price(row);
    this.loanmodel.LoanPolicies.push(row);
    //this.loancalculationservice.performcalculationonloanobject(this.loanmodel, false);
    this.dataService.setLoanObjectwithoutsubscription(this.loanmodel);

    let custompolicies = _.orderBy(this.loanmodel.LoanPolicies.filter(p => p.ActionStatus != 3 && p.IsCustom == true), this.sortstrings);
    if (custompolicies != undefined) {
      let item = this.processCustomPolicy(row, custompolicies);
      this.gridApi.updateRowData({ add: [item] });
    }


  }
  //FINISH HERE

  //Get editable value
  getstatusofeditibilty(params) {
    try {
      let pos = params.colDef.pickfield.lastIndexOf("_") + 1;
      let policyname = params.colDef.pickfield.substr(pos, params.colDef.pickfield.length - pos);
      let actualproperty = params.colDef.pickfield.replace('_' + policyname, '');
      let returnable = true;
      let policyrecord = CompareandgetSubPolicy(params.data.mainpolicyId, policyname, this.loanmodel);
      let property = getApplicableProperty(actualproperty);
      switch (policyname) {
        case 'MPCI':
          if (policyrecord.Ins_Plan_Type == 'CAT') {
            returnable = false;
          }
          break;
        case 'STAX':
          if (property == 'Price_Percent') {
            returnable = false;
          }
          break;
        case 'SCO':
          if (property == 'Price_Percent' || property == 'Yield_Percent' || property == 'Upper_Level' || property == 'Lower_Level') {
            returnable = false;
          }
          break;
        case 'HMAX':
          if (property == 'Yield_Percent' || property == 'Deductible_Percent') {
            returnable = false;
          }
          break;
        case 'RAMP':
          if (property == 'Yield_Percent') {
            returnable = false;
          }
          break;
        case 'ICE':
          if (property == 'Deductible_Units') {
            returnable = false;
          }
          break;
        case 'SELECT':
          if (property == 'Yield_Percent' || property == 'Price_Percent' || property == 'Lower_Level') {
            returnable = false;
          }
          break;
        case 'CROPHAIL':
          if (policyrecord.Ins_Plan_Type == "Basic") {
            if (property == 'Yield_Percent' || property == 'Price_Percent') {
              returnable = false;
            }
          }
          if (policyrecord.Ins_Plan_Type == "Companion") {
            if (property == 'Upper_Level' || property == 'Lower_Level' || property == 'Liability_Percent' || property == 'Ins_Unit_Type_Code') {
              returnable = false;
            }
          }
          break;


      }
      return returnable;
    } catch {
      return false;
    }

  }


  get Loan_Status() {
    if (this.loanmodel) {
      return this.loanmodel.LoanMaster.Loan_Status;
    }
    return '';
  }
  onCellClicked(event: any) {
    if (event.type == 'cellClicked' && !event.column.colDef.pickfield.includes('Enabled')) {
      let policy = event.column.colDef.pickfield;
      policy = policy.substr(policy.lastIndexOf('_') + 1, policy.length - policy.lastIndexOf('_'));
      let actualpolicy = CompareandgetSubPolicy(event.data.mainpolicyId, policy, this.loanmodel);
      let index = this.loanobj.LoanPolicies.indexOf(actualpolicy);
      if (actualpolicy && actualpolicy.FC_CanCopy) {
        actualpolicy.FC_CanCopy = false;
        let previous_row = getlastfilledrow(event.rowIndex, policy);
        let previouspolicy;
        if (previous_row != undefined) {
          previouspolicy = CompareandgetSubPolicy(previous_row.mainpolicyId, policy, this.loanmodel);
        }
        if (actualpolicy && !!previouspolicy) {

          let column: string = event.colDef.field;
          let columns_to_copy = getColumnsToCopy(this.columnDefs, column, policy);
          let current_row = this.rowData[event.rowIndex];

          current_row = copyrow(previous_row, current_row, columns_to_copy, index);
          this.rowData[event.rowIndex] = current_row;
          this.gridApi.updateRowData({ update: [current_row] });
          this.gridApi.stopEditing();
        }
      }
    }
  }

  private Set_Price(item: Loan_Policy) {
    console.log('called_Tarjeet singh test');
    let key = Get_Policy_Key(this.loanobj, item);
    let rma = RKS_RMAFind(key, item.State_ID, item.County_ID, this.refdata.RMA_Insurance_Offers);
    let price = rma.MPCI_Base_Price + Math.max(0, rma.MPCI_Harvest_Price - rma.MPCI_Base_Price) * rma.Price_Established / 100;
    // let adjustmentkey=Get_Policy_Adjustment_Key(this.loanobj, item);
    // let adjprice=RKS_RMAADJUSTMENTFind(adjustmentkey,this.refdata.Ref_Option_Adj_Lookups);
    // item.Price = price+adjprice;
    item.Price = price;
    const id = JSON.stringify(rma);
    const text = `Policy Price for - ${key}`;
    this.auditTrail.addAuditTrailAsync(text, 'RMA Price', id, this.loanobj);
  }
}


function wait(ms) {
  let start = new Date().getTime();
  let end = start;
  while (end < start + ms) {
    end = new Date().getTime();
  }
}
