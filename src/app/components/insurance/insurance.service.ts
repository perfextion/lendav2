import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { LoggingService } from '../../services/Logs/logging.service';
import { LoancalculationWorker } from '../../Workers/calculations/loancalculationworker';
import { LoanApiService } from '../../services/loan/loanapi.service';
import { ToasterService } from '../../services/toaster.service';
import { loan_model } from '../../models/loanmodel';
import { JsonConvert } from 'json2typescript';
import { GlobalService } from '../../services/global.service';
import { APH_Units, Loan_Crop_Unit } from '@lenda/models/cropmodel';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { environment } from '@env/environment.prod';
import {
  lookupCountyValueFromPassedRefData,
  lookupStateAbvRefValue
} from '@lenda/Workers/utility/aggrid/stateandcountyboxes';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';
import { RatedValues } from '../farm/farm.model';
@Injectable()
export class InsuranceService {
  refData: RefDataModel;

  constructor(
    public localstorageservice: LocalStorageService,
    public logging: LoggingService,
    public loanserviceworker: LoancalculationWorker,
    public loanapi: LoanApiService,
    public toasterService: ToasterService
  ) {
    this.refData = this.localstorageservice.retrieve(
      environment.referencedatakey
    );

    this.localstorageservice
      .observe(environment.referencedatakey)
      .subscribe(res => {
        this.refData = res;
      });
  }

  syncToDb(localloanobject: loan_model) {
    // Loan Condition Calculation
    localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);

    this.loanapi.syncloanobject(localloanobject).subscribe(res => {
      if (res.ResCode == 1) {
        this.localstorageservice.store(environment.modifiedbase, []);
        this.loanapi
          .getLoanById(localloanobject.Loan_Full_ID)
          .subscribe(res1 => {
            this.logging.checkandcreatelog(
              3,
              'Overview',
              'APi LOAN GET with Response ' + res1.ResCode
            );
            if (res1.ResCode == 1) {
              this.toasterService.success('Records Synced');
              let jsonConvert: JsonConvert = new JsonConvert();
              this.loanserviceworker.performcalculationonloanobject(
                jsonConvert.deserialize(res1.Data, loan_model)
              );
              this.loanserviceworker.saveValidationsToLocalStorage(res1.Data);
              this.updateLocalStorage();
            } else {
              this.toasterService.error('Could not fetch Loan Object from API');
            }
          });
      } else {
        this.toasterService.error(res.Message || 'Error in Sync');
      }
    });
  }

  updateLocalStorage() {
    new GlobalService(this.localstorageservice).updateLocalStorage('Ins_');
    new GlobalService(this.localstorageservice).updateLocalStorage('Aph_');
  }

  getColumnsToCopy(colDefs: any[], columnName: string) {
    let index = colDefs.findIndex(a => a.field == columnName);
    return colDefs.slice(0, index);
  }

  copyrow(from_aph: APH_Units, to_aph: APH_Units, columns: any[]) {
    columns
      .filter(a => !a.hide)
      .filter(a => a.field != 'Loan_CU_ID')
      .forEach(col => {
        let field = col.field;

        if (
          field == 'State_ID' ||
          field == 'County_ID' ||
          field == 'Prod_Perc'
        ) {
          to_aph[field] = from_aph[field];
          return;
        }

        if (to_aph[field] == null || String(to_aph[field]) === '') {
          to_aph[field] = from_aph[field];
        }
      });

    return to_aph;
  }

  getValidAPHs(data: APH_Units[], loan: loan_model) {
    const rows = [];

    data.forEach((row, index) => {
      let aph = this.validateAPH(row, index, loan, rows);
      if (aph) {
        rows.push(aph);
      }
    });

    const deleted_rows = this._deleted_APH(loan, rows);

    return {
      data: rows,
      addedRows: 0,
      deletedRows: deleted_rows.length
    };
  }

  private validateAPH(
    aph: APH_Units,
    index: number,
    loan: loan_model,
    aphs: APH_Units[]
  ) {
    try {
      let key = this._aph_key(aph);

      let lcu = loan.LoanCropUnits.find(a => a.Crop_Unit_Key == key);
      let aUnit = loan.AphUnits.find(a => this._aph_key(a) == key);

      if (!!lcu) {
        let prod = this.parse_Numeric(aph.Prod_Perc, 'Prod Percent');

        if (lcu.FC_ProdPerc != prod) {
          throw new Error('Production Percent value is not matching.');
        }

        lcu.ActionStatus = 2;
        lcu.Ins_APH = this.parse_Numeric(aph.Ins_APH, 'APH');
        lcu.Rate_Yield = this.parse_Numeric(aph.Rate_Yield, 'Rate Yield');

        return {
          row: lcu,
          isCU: true
        };
      } else if (!!aUnit) {
        aUnit.ActionStatus = 2;
        aUnit.Ins_APH = this.parse_Numeric(aUnit.Ins_APH, 'APH');
        aUnit.Rate_Yield = this.parse_Numeric(aUnit.Rate_Yield, 'Rate Yield');

        return {
          row: aUnit,
          isCU: false
        };
      } else {
        // validate state
        if (!this.refData.StateList.some(a => a.State_Abbrev == aph.FC_StateName)) {
          throw new Error('Invalid State Id');
        }

        // validate county
        if (
          !this.refData.CountyList.some(
            a => a.County_Name == aph.FC_CountyName && a.State_Abbrev == aph.FC_StateName
          )
        ) {
          throw new Error('Invalid County Id');
        }

        // Rated
        if (!RatedValues.some(a => a.key == String(aph.Rating))) {
          throw new Error('Invalid Rated');
        }

        let prod = parseFloat(String(aph.Prod_Perc));

        if (isNaN(prod)) {
          throw new Error('Invalid Production Percent Value');
        }

        if (prod > 100 || prod < 0) {
          throw new Error('Invalid Production Percent Value');
        }

        if (!['Yes', 'No'].some(a => a == String(aph.Added_Land))) {
          throw new Error('Invalid Added Land Value');
        }

        aph.Fc_Custom = true;
        aph.ActionStatus = 1;
        aph.Loan_CU_ID = getRandomInt(Math.pow(10, 8), Math.pow(10, 10));

        return {
          row: aph,
          isCU: false
        };
      }
    } catch (ex) {
      throw new Error(`Row ${index + 1} has error - ${ex.message}`);
    }
  }

  private parse_Numeric(val: number, name: string) {
    if(val) {
      let v = parseFloat(String(val));
      if (isNaN(v)) {
        throw new Error('Invalid ' + name + ' Value');
      }
      return v;
    } else {
      return 0;
    }
  }

  private _deleted_APH(loan: loan_model, aphs: APH_Units[]) {
    return [];
  }

  private _aph_key(cu: any) {
    return `${cu.FC_StateName}_${cu.FC_CountyName}_${cu.Crop_Code}_${this._crop_type(cu.Croptype)}_${cu.Crop_Practice_Type_Code}_${cu.Crop_Practice_Type_Code}`;
  }

  private _crop_type(crop_type: string) {
    if(crop_type == '' || crop_type == '-' || !crop_type) {
      return '0';
    }
    return crop_type;
  }
}
