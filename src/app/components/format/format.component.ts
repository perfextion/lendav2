import { Component, OnInit, Inject, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { loan_model } from '../../models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';
import { LoancalculationWorker } from '../../Workers/calculations/loancalculationworker';
import { LoggingService } from '../../services/Logs/logging.service';
import { environment } from '../../../environments/environment.prod';
import { CropapiService } from '../../services/crop/cropapi.service';
import { yieldValueSetter, getIntegerCellEditor, getNumericCellEditor, formatPhoneNumber, getPhoneCellEditor, formatPhoneNumberAsYouType } from '../../Workers/utility/aggrid/numericboxes';
import { extractCropValues, cropNameValueSetter, APHRoundValueSetter, CropPracticeValueSetter } from '../../Workers/utility/aggrid/cropboxes';
import { LoanApiService } from '../../services/loan/loanapi.service';
import { DeleteButtonRenderer } from '../../aggridcolumns/deletebuttoncolumn';
import { AlertifyService } from '../../alertify/alertify.service';
import { SelectEditor } from '../../aggridfilters/selectbox';
import { Sync_Status } from '../../models/syncstatusmodel';
import { isgrideditable, cellclassmaker, CellType, headerclassmaker, setgriddefaults } from '../../aggriddefinations/aggridoptions';
import { getAlphaNumericCellEditor } from '@lenda/Workers/utility/aggrid/alphanumericboxes';
import { currencyFormatter } from '@lenda/Workers/utility/aggrid/collateralboxes';
import { percentageFormatter, acresFormatter } from '@lenda/aggridformatters/valueformatters';
import { getDateCellEditor, formatDateValue, getDateValue } from '@lenda/Workers/utility/aggrid/dateboxes';

@Component({
  selector: 'app-format',
  templateUrl: './format.component.html',
  styleUrls: ['./format.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FormatComponent implements OnInit {

  public rowData=[];
  public columnDefs=[];
  public components;
  public gridApi;
  public columnApi;
  public deleteAction = false;

  public cropYear;
  context: any;
  public syncYieldStatus : Sync_Status = 0;

  frameworkcomponents;

  @ViewChild("myGrid") gridEl: ElementRef;

  defaultColDef = {
    enableValue: true,
    enableRowGroup: true,
    enablePivot: true
  };


  constructor(public localstorageservice:LocalStorageService,
  public loanserviceworker:LoancalculationWorker,
  public cropserviceapi:CropapiService,
  public logging:LoggingService,
  public loanapi:LoanApiService,
  public alertify:AlertifyService
  ) {


    this.components = { intCellEditor:getIntegerCellEditor(), numericCellEditor:getNumericCellEditor(), alphaNumeric: getAlphaNumericCellEditor(), phoneEditor: getPhoneCellEditor(),
                        dateCellEditor: getDateCellEditor(), datePicker: getDatePicker()};
    this.frameworkcomponents = {selectEditor: SelectEditor, deletecolumn: DeleteButtonRenderer};
    this.columnDefs = [
      {headerName: 'Numeric', field: 'Numeric', editable: true, cellEditor:"numericCellEditor"},
      {headerName: 'NoDecimal', field: 'NoDecimal', editable: true, cellEditor: "intCellEditor"},
      { headerName: 'Alphanumeric', field: 'Alphanumeric', editable: true,cellEditor: "alphaNumeric"},
      { headerName: 'Select', field: 'Select', editable: true,cellEditor: "selectEditor", cellEditorParams: {
        values: [{ 'key': 'No', 'value': 'No' }, { 'key': 'Yes', 'value': 'Yes' }]},},
      { headerName: 'Date', field: 'Date', editable: true,cellEditor: "dateCellEditor"},
      { headerName: 'Currency', field: 'Currency', editable: true, cellEditor:"numericCellEditor", valueFormatter: currencyFormatter},
      { headerName: 'Percent', field: 'Percent', editable: true, cellEditor:"numericCellEditor", valueFormatter: percentageFormatter},
      { headerName: 'Phone', field: 'Phone', editable: true, cellEditor:"phoneEditor",valueFormatter:formatPhoneNumberAsYouType},
      { headerName: 'Acres', field: 'Acres', editable: true, valueFormatter: acresFormatter},
      { headerName: 'RoundValue', field: 'RoundValue', editable: true, valueFormatter: APHRoundValueSetter},
    ];

    this.context = { componentParent: this };
  }

  ngOnInit() {
    this.rowData = [{Numeric: '',NoDecimal: '', Alphanumeric : '',Currency:'', Date:'1986-11-11T00:00:00', RoundValue:0},{Numeric: '',NoDecimal: '',Alphanumeric : '',Currency:'', Date:'1986-11-11T00:00:00', RoundValue:0}];
    this.getgridheight();

    //this.getdataforgrid();
  }



  onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    //these two methods remove side option toolbar , sets column , removes header menu
    setgriddefaults(this.gridApi,this.columnApi);
    params.api.sizeColumnsToFit();
    ///////////////////////////////////////////////////////////////////////////////////
    this.rowData = [{Numeric: '',NoDecimal: '', Alphanumeric : '',Currency:'', Date:'', RoundValue:0},{Numeric: '',NoDecimal: '',Alphanumeric : '',Currency:'', Date:'1986-11-11T00:00:00',RoundValue:0}];
  }

  rowvaluechanged(value:any){
  }

  addrow() {
  }

  DeleteClicked(rowIndex: any) {
  }

  getgridheight(){
    //this.style.height=(29*(this.rowData.length+2)).toString()+"px";
  }

  onGridSizeChanged(params) {
    //params.api.sizeColumnsToFit();
    params.api.resetRowHeights();
  }

  syncenabled() {
    if(this.rowData.filter(p => p.ActionStatus != undefined).length > 0 || this.deleteAction)
      return '';
    else
      return 'disabled';
  }
}

function getDatePicker() {
  function Datepicker() {}
  Datepicker.prototype.init = function(params) {
    this.eInput = document.createElement("input");
    this.eInput.value = params.value;
    // $(this.eInput).datepicker({ dateFormat: "dd/mm/yy" });
  };
  Datepicker.prototype.getGui = function() {
    return this.eInput;
  };
  Datepicker.prototype.afterGuiAttached = function() {
    this.eInput.focus();
    this.eInput.select();
  };
  Datepicker.prototype.getValue = function() {
    return this.eInput.value;
  };
  Datepicker.prototype.destroy = function() {};
  Datepicker.prototype.isPopup = function() {
    return false;
  };
  return Datepicker;
}
