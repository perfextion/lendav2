import { Component, OnInit, ViewChild, ViewEncapsulation, ElementRef, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatSort, MatSortable } from '@angular/material';

import { LocalStorageService } from 'ngx-webstorage';
import { JsonConvert } from 'json2typescript';
import { ToastrService } from 'ngx-toastr';
import * as XLSX from 'xlsx';
import * as _ from 'lodash';
import { ISubscription, Subscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { Page } from '@lenda/models/page.enum';

import { Preferences } from '@lenda/preferences/models/index.model';
import { loan_model, Loan_Cross_Collateral } from '@lenda/models/loanmodel';
import { ResponseModel } from '@lenda/models/resmodels/reponse.model';
import { LoanDetailService, LoanListRequestModel } from '@lenda/preferences/loan-list/list-settings/loan-detail/loan-detail.service';
import { LoanMasterColumn, LoanMasterSettings } from '@lenda/preferences/models/loan-list/list/loan-master/index.model';
import { ListSettings } from '@lenda/preferences/models/loan-list/list/list.model';
import { DisplayType, ColumnSettings, DataType } from '@lenda/preferences/models/base-entities/column/index.model';
import { LoanListSettings } from '@lenda/preferences/models/loan-list/index.model';
import { BasedUponCriteria } from '@lenda/preferences/models/base-entities/based-upon-criteria.enum';
import { NumberComparer, StringComparer } from './comparar-functions';
import { RefDataModel } from '@lenda/models/ref-data-model';

import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoanvalidatorService } from '@lenda/services/loanvalidator.service';
import { SharedService } from '@lenda/services/shared.service';
import { ApiService } from '@lenda/services';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { LayoutService } from '@lenda/shared/layout/layout.service';
import { PublishService } from '@lenda/services/publish.service';
import { PageInfoService } from '@lenda/services/page-info.service';
import { PubSubService } from '@lenda/services/pubsub.service';
import { MasterService } from '@lenda/master/master.service';
import { PendingActionGhostingService } from '@lenda/alertify/components/pending-action-ghosting/pending-action-ghosting.service';
import { EmailTemplateType } from '@lenda/models/pending-action/add-pending-action.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-loan-list',
  templateUrl: './loan-list.component.html',
  styleUrls: ['./loan-list.component.scss'],
  providers: [SharedService, LoanDetailService],
  encapsulation: ViewEncapsulation.None
})
export class LoanListComponent implements OnInit, OnDestroy {
  @ViewChild('TABLE') table: ElementRef;
  private loanid: string;
  private originalFilterPredicate: any;
  public headers: Array<string> = new Array<string>();
  public headersTitles: string[] = [];
  public loanlist = new MatTableDataSource<any>();
  public originalLoanList = new MatTableDataSource();
  public pageheader = 'Loan Listing';
  public refDataReceived = false;
  public refdata: RefDataModel;
  public loanListSettings: LoanListSettings;
  public listSettings: ListSettings;
  public isExportEnable: boolean = false;
  public isPendingActionEnabled: boolean = true;
  public footerItems = {
    totals: [],
    min: [],
    max: [],
    average: []
  };
  public isNumericColAvailable: boolean = false;
  public sortColumn: string = 'Pending_Action_Sort_Order';
  private pubsubSubscription: ISubscription;

  private ghostUserSubscription: ISubscription;
  private ghostUserID: number;

  private loanListColumnSettings: LoanListRequestModel;
  private originaldata: any;

  private prefSub: ISubscription;

  private blackListedColumns = [
    'Loan_Full_ID',
    'Loan_Status',
    'Loan_Tolerance_Level_Ind',
    'Loan_Pending_Action_Type_Code',
    'Is_Latest',
    'Reminder_Ind'
  ];

  private preferences: Preferences;

  private getComparor(column: string) {
    let col: ColumnSettings = LoanMasterSettings[column];

    if(col) {
      if(col.datatype == DataType.int || col.datatype == DataType.decimal) {
        return NumberComparer(column);
      }
      return StringComparer(column);
    }
    return StringComparer(column);
  }

  @ViewChild(MatSort) sort: MatSort;
  // @ViewChild(MatPaginator) pagintor: MatPaginator;
  @ViewChild('input') inputElRef: ElementRef;

  constructor(
    private loanService: LoanApiService,
    private route: Router,
    private activatedRoute: ActivatedRoute,
    private localstorageservice: LocalStorageService,
    private logging: LoggingService,
    private loancalculationservice: LoancalculationWorker,
    private loanvalidator: LoanvalidatorService,
    private loanservice: LoanApiService,
    private toaster: ToastrService,
    private settingsService: SettingsService,
    private sharedService: SharedService,
    private loanDetailService: LoanDetailService,
    private apiService: ApiService,
    private publishService: PublishService,
    private dtss: DatetTimeStampService,
    private layoutService: LayoutService,
    private pageInfoService: PageInfoService,
    private pubsubService: PubSubService,
    private masterService: MasterService,
    private pendingActionGhosting: PendingActionGhostingService
  ) { }

  ngOnInit() {
    //Clear Selected Loan
    this.localstorageservice.store(environment.loankey, null);

    this.loanservice.getFilter().subscribe(res => {
      res = res.trim(); // Remove whitespace
      res = res.toLowerCase(); // MatTableDataSource defaults to lowercase matches
      this.loanlist.filter = res;
    });

    let ghostUser = this.localstorageservice.retrieve(environment.ghostUserDetails);
    if(ghostUser) {
      this.ghostUserID  = ghostUser.UserID;
    }

    this.preferences = this.settingsService.preferences;

    // Update loan list of preferences are changed
    this.prefSub = this.settingsService.preferencesChange.subscribe((preference: Preferences) => {
      this.preferences = preference;
      if(this.ghostUserID > 0) {
        this.getGhostUserLoanList();
      } else {
        this.loadLoanList();
      }
    });

    // By default loan loan list
    // this.loadLoanList();

    this.localstorageservice.store(environment.currentpage, Page.loanlist);
    this.layoutService.toggleRightSidebar(false);
    this.getLOStats();

    this.getMasterBorrowerList();
    this.getMasterFarmerList();

    // If it is loan list, set the menu items
    this.activatedRoute.data.subscribe((data) => {
      if (data.page) {
        this.pageInfoService.setCurrentPage(data.page);
      }
    });

    this.ghostUserSubscription = this.pendingActionGhosting.ghostUser$.subscribe(res => {
      this.ghostUserID = res;
      this.getGhostUserLoanList();
    });

    // Subscribe to events from main header settings
    this.subscribeToPubsubEvents();
  }

  private getGhostUserLoanList() {
    this.settingsService.getPreferencesByUserId(this.ghostUserID).subscribe(res => {
      if(res.ResCode == 1 && res.Data) {
        this.preferences = JSON.parse(res.Data);
        this.loadLoanList();
      } else {
        this.loadLoanList();
      }
    });
  }
  keyupSub:  Subscription;
  // tslint:disable-next-line: use-life-cycle-interface
  ngAfterViewInit() {
    // this.ngzone.runOutsideAngular( () => {
      this.keyupSub = Observable.fromEvent(this.inputElRef.nativeElement, 'input')
        .debounceTime(500)
        .subscribe((keyboardEvent: KeyboardEvent) => {
          this.update(keyboardEvent.srcElement["value"]);
        });

    // });
  }
  ngOnDestroy() {
    if (this.pubsubSubscription) {
      this.pubsubSubscription.unsubscribe();
    }

    if (this.prefSub) {
      this.prefSub.unsubscribe();
    }

    if(this.ghostUserSubscription) {
      this.ghostUserSubscription.unsubscribe();
    }
  }

  getHeader(headerText: string) {
    try {
      if(headerText && headerText.includes(':')) {
        return headerText.split(':')[1].trim();
      }
      return headerText;
    } catch {
      return headerText;
    }
  }

  /**
   * Subscribe to pubsub bus for global settings action
   */
  private subscribeToPubsubEvents() {
    this.pubsubSubscription = this.pubsubService.subscribeToEvent().subscribe((ev) => {
      if (ev.name === 'event.loanlist.includereports.updated') {
        this.toggleIsIncludeMyReportsEnabledChange(ev.data.checkboxEvent);
      }
      if (ev.name === 'event.loanlist.IncludeAllVersions.updated') {
        this.onIncludeAllVersions(ev);
      }
      if (ev.name === 'event.loanlist.portfolioAnalysis.updated') {
        this.togglePortfolioAnalysisSettingsChange(ev.data.checkboxEvent);
      }
      if (ev.name === 'event.loanlist.includeInactive.updated') {
        this.onInacludeInactiveLoans(ev);
      }
    })
  }

  private getLOStats() {
    if (!this.localstorageservice.retrieve(environment.loanofficerstats)) {
      this.loanservice.getLoanOfficerStats().subscribe(res => {
        if (res.ResCode == 1) {
          this.localstorageservice.store(environment.loanofficerstats, res.Data);
        }
      });
    }
  }

  exportToExcel() {
    let exportData = JSON.parse(JSON.stringify(this.loanlist.data));
    exportData.forEach(function (el) {
      delete el.Loan_Pending_Action_Type_Code;
      delete el.Loan_Tolerance_Level_Ind;
      delete el.Is_Latest;
      delete el.Pending_Action_Sort_Order;
    });

    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(exportData);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();

    if(exportData && exportData.length > 0) {
      Object.keys(exportData[0]).forEach((key, index) =>  {
        let header = this.getHeader(LoanMasterColumn[key]);
        ws[XLSX.utils.encode_col(index) + '1'].v = header;
      });

      XLSX.utils.book_append_sheet(wb, ws, 'Loan List');
      XLSX.writeFile(wb, `Loan_List_${this.dtss.getTimeStamp()}.xlsx`);
    }
  }

  private loadLoanList() {
    this.loanListSettings = this.preferences.loanListSettings;
    this.listSettings = this.loanListSettings.listSettings;
    // Default sort column toggle from my preferences
    this.sortColumn = this.loanListSettings.listSettings.isPendingActionSortOrderEnabled ? 'Pending_Action_Sort_Order' : 'Loan_Pending_Action_Type_Code';
    // Get loan list query from my preferences
    // let querystring = this.loanDetailService.getLoanMasterQuery(this.listSettings.loanMasterSettings);
    let settings = this.loanDetailService.getLoanListColumnsSettings(this.listSettings);
    this.getloanlist(settings);
    this.sharedService.isLoanListActive(true);
    this.loanservice.setFilter('');
    //reference data api is taking time, so better to block the user untill it loaded and then display the list
    this.localstorageservice.observe(environment.referencedatakey).subscribe(refData => {
      if (refData) {
        this.refDataReceived = true;
        this.refdata = refData;
      }
    });
    let data = this.localstorageservice.retrieve(environment.referencedatakey);
    if (data) {
      this.refDataReceived = true;
      this.refdata = data;
    }
  }

  public togglePendingAction() {
    this.isPendingActionEnabled = !this.isPendingActionEnabled;
    let data = JSON.parse(this.originaldata);

    // If disabling, sort by next column
    if (!this.isPendingActionEnabled) {
      let columns = [];
      let orders = [];

      if(this.loanListColumnSettings && this.loanListColumnSettings.items) {
        let col = [...this.blackListedColumns, 'Pending_Action_Sort_Order'];

        const cols = this.loanListColumnSettings.items.filter(a => !col.includes(a.col))
        .sort((a, b) => a.colOrder - b.colOrder);

        columns = cols.map(a => this.getComparor(a.col));

        orders = cols.map(a => {
          if(a.sort == 0) {
            return 'asc';
          }
          return 'desc';
        });
      }

      data = _.orderBy(data, columns, orders);
      this.loanlist = new MatTableDataSource(data);

      this.sort.sort(<MatSortable>{
        id: '',
        start: 'asc'
      });

      this.sort.direction = '';
      this.loanlist.sort = this.sort;
      // this.loanlist.paginator = this.pagintor;
    } else {
      let columns = [];
      let orders = [];

      if(this.loanListColumnSettings && this.loanListColumnSettings.items) {
        const cols = this.loanListColumnSettings.items
        .filter(a => a.col != 'Is_Latest' && !this.blackListedColumns.includes(a.col))
        .sort((a, b) => a.colOrder - b.colOrder);

        columns = cols.map(a => this.getComparor(a.col));

        orders = cols.map(a => {
          if(a.sort == 0) {
            return 'asc';
          }
          return 'desc';
        });
      }

      data = _.orderBy(data, columns, orders);
      this.loanlist = new MatTableDataSource(data);

      this.sort.sort(<MatSortable>{
        id: '',
        start: 'asc'
      });

      this.sort.direction = '';
      this.loanlist.sort = this.sort;
      // this.loanlist.paginator = this.pagintor;
    }
  }

  private getColAfterPendingAction() {
    let data = this.loanlist.data;
    let settings: any = this.listSettings.loanMasterSettings;
    for (let index in settings) {
      // Find item with colorder 6
      // TODO: Check if rather than hardcoding the colorder, we can make it dynamic
      if (settings[index].colOrder === 6) {
        return index;
      }
    }
  }

  private getloanlist(settings: LoanListRequestModel) {
    this.loanListColumnSettings = settings;
    this.loanService.getLoanListByPost(settings, this.ghostUserID).subscribe(res => {
      if (res.ResCode == 1) {
        this.originaldata = res.Data;

        let data = JSON.parse(res.Data);

        let columns = [];
        let orders = [];

        if(this.loanListColumnSettings && this.loanListColumnSettings.items) {
          const cols = this.loanListColumnSettings.items
          .filter(a => a.col != 'Is_Latest' && !this.blackListedColumns.includes(a.col))
          .sort((a, b) => a.colOrder - b.colOrder);

          columns = cols.map(a => this.getComparor(a.col));

          orders = cols.map(a => {
            if(a.sort == 0) {
              return 'asc';
            }
            return 'desc';
          });
        }

        data = _.orderBy(data, columns, orders);

        this.loanlist = new MatTableDataSource(data);
        // Need to keep a copy to toggle the displayed data source
        this.originalLoanList = new MatTableDataSource(data);
        if (data.length > 0) {
          this.headers = Object.keys(data[0]);
          // custom iterator to change headings from mapping
          for (let header of this.headers) {
            if(header != 'Reminder_Ind') {
              this.headersTitles[header] = this.getHeader(LoanMasterColumn[header]);
            }
          }
        }

        this.loanlist.sort = this.sort;
        // this.loanlist.paginator = this.pagintor;
        this.isExportEnable = this.loanlist.data.length == 0;
        this.getFooterItems(data);
        this.originalFilterPredicate = this.loanlist.filterPredicate;
      }
    });
  }

  /**
   * Gets footer totals etc from API response
   * Sum, min, max and average calculations - if display type is currency or number
   * @param items  List of items sent by API
   */
  getFooterItems(items: Array<any>) {
    if (items && items.length > 0) {
      let item = items[0];
      this.isNumericColAvailable = false;
      Object.keys(item).forEach((key) => {
        if(this.listSettings.loanMasterSettings[key]) {
          let displayType = this.listSettings.loanMasterSettings[key].displayType;

          // If currency or numeric fields
          if (displayType === DisplayType.currency || displayType === DisplayType.acres) {
            // Get totals
            let arrayItems = items.map(a => a[key]);
            this.footerItems.totals[key] = arrayItems.reduce((a, b) => a + b);
            this.footerItems.min[key] = Math.min(...arrayItems);
            this.footerItems.max[key] = Math.max(...arrayItems);
            this.footerItems.average[key] = this.footerItems.totals[key] / arrayItems.length;
            // Mark numeric col to be true
            this.isNumericColAvailable = true;
          }

          // If percent or fee/interest field
          if (displayType === DisplayType.percent || displayType === DisplayType.interest || displayType === DisplayType.rating) {
            let arrayItems = items.map(a => a[key]);
            this.footerItems.min[key] = Math.min(...arrayItems);
            this.footerItems.max[key] = Math.max(...arrayItems);
            // Average  = (Sum of all% * #commit) / #totalcommit
            let selectedKey = this.getSelectedWeightedAverageOption();
            this.footerItems.average[key] = this.getPercentColAverage(items, selectedKey, key);
          }
        }
      });
    }
  }

  /**
   * Latest loan version checkbox toggle event
   * @param ev event of checkbox
   */
  onIncludeAllVersions(ev) {
    this.loanListColumnSettings.includeLatestVersion = !ev.data.checkboxEvent.checked;
    this.getloanlist(this.loanListColumnSettings);
  }

  /**
   * Include Active only toggled
   * @param ev checkbox event
   */
  onInacludeInactiveLoans(ev) {
    this.loanListColumnSettings.includeInactiveLoans = ev.data.checkboxEvent.checked;
    this.getloanlist(this.loanListColumnSettings);
  }

  /**
   * Gets percent col average for percent and fee columns
   */
  private getPercentColAverage(items, selectedKey, currentKey) {
    let sum = 0;
    for (let item of items) {
      sum += item[selectedKey] * item[currentKey];
    }
    return sum / this.getColTotal(items, selectedKey);
  }

  /**
   * Returns sum of selected col key
   */
  private getColTotal(items, col) {
    return items.map(a => a[col]).reduce((a, b) => a + b);
  }

  /**
   * Maps the option key by value of enum
   * Returns the saved total for key of weighted average %
   */
  private getSelectedWeightedAverageOption() {
    return Object.keys(BasedUponCriteria).find(key => BasedUponCriteria[key] === this.loanListSettings.portfolioAnalysisSettings.weightedAveragePercentBasedUpon)
    // return this.footerItems.totals[selectedKey];
  }

  public get hideFooter() {
    try {
      return (
        !this.loanListSettings.portfolioAnalysisSettings.isDetailRecordsEnabled ||
        this.loanlist.filteredData.length == 0 ||
        (
          !this.loanListSettings.portfolioAnalysisSettings.isSumRowEnabled &&
          !this.loanListSettings.portfolioAnalysisSettings.isMaxRowEnabled &&
          !this.loanListSettings.portfolioAnalysisSettings.isAverageRowEnabled &&
          !this.loanListSettings.portfolioAnalysisSettings.isMinRowEnabled
        )
      );
    } catch {
      return true;
    }
  }

  navigatetoloan(id: string) {
    this.layoutService.toggleRightSidebar(false);
    this.loanid = id
    this.localstorageservice.store(environment.loanidkey, id);
    this.localstorageservice.clear('CopyLoan');
    // Get default landing page from my preferences
    let preferences: Preferences = this.settingsService.preferences;
    //get loan here now
    let obj = this.localstorageservice.retrieve(environment.loankey);
    if ((obj == null || obj == undefined)) {
      this.localstorageservice.store(environment.loanGroup, []); //reset loan group if loan to be opened is not exist in local storage
      this.getLoanBasicDetails(preferences);
    }
    else {
      if (obj.Loan_Full_ID != this.loanid) {
        this.localstorageservice.store(environment.loanGroup, []);//reset loan group if loan to be opened is not exist in local storage
        this.getLoanBasicDetails(preferences);
      }
      else {
        this.localstorageservice.store(environment.currentpage, preferences.userSettings.landingPage);
        this.route.navigateByUrl('/home/loanoverview/' + id.replace('-', '/') + '/' + preferences.userSettings.landingPage);
      }
    }
  }

  private getLoanBasicDetails(preferences: Preferences) {
    if (this.loanid != null) {
      let loaded = false;
      try {
        this.localstorageservice.clear(environment.loankey);
        this.localstorageservice.clear(environment.loanGroup);
      } catch (e) {
        if (environment.isDebugModeActive) console.log(`trying to clear a non existing local storage key : ${environment.loankey}`);
      }
      this.loanservice.getLoanById(this.loanid).subscribe(res => {
        this.masterService.isMinimize = false;
        this.logging.checkandcreatelog(3, 'Overview', 'APi LOAN GET with Response ' + res.ResCode);
        if (res.ResCode == 1) {

          this.loanvalidator.validateloanobject(res.Data).then(errors => {
            if (errors && errors.length > 0) {

              this.toaster.error('Loan Data is invalid ... Rolling Back to list')
              this.route.navigateByUrl('/home/loans');


            } else {
              this.layoutService.toggleRightSidebar(false);
              let jsonConvert: JsonConvert = new JsonConvert();

              this.localstorageservice.store(environment.errorbase, []);
              this.loancalculationservice.saveValidationsToLocalStorage(res.Data);
              //we are making a copy of it also
              this.localstorageservice.store(environment.loankey_copy, res.Data);
              this.localstorageservice.store(environment.currentpage, preferences.userSettings.landingPage);

              //loan affiliated loan, currently static
              const route = '/api/Loans/GetLoanGroups?LoanFullID=' + (res.Data as loan_model).Loan_Full_ID;
              this.apiService.get(route).subscribe((resp: ResponseModel) => {
                if (resp) {
                  this.localstorageservice.store(environment.loanGroup, resp.Data);
                  this.loancalculationservice.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model), true, true);
                  this.route.navigateByUrl('/home/loanoverview/' + this.loanid.replace('-', '/') + '/' + preferences.userSettings.landingPage);
                }
              });

              this.localstorageservice.store(environment.loanCrossCollateral, []);
              this.loanservice.getDistinctLoanIDList(this.loanid).subscribe(res => {
                if (res && res.ResCode == 1) {
                  this.localstorageservice.store(environment.loanCrossCollateral, res.Data as Array<Loan_Cross_Collateral>);
                }
              });
            }
            this.localstorageservice.store(environment.exceptionStorageKey, []);
            this.localstorageservice.store(environment.modifiedbase, []);
            this.localstorageservice.store(environment.modifiedoptimizerdata, null);
            this.publishService.syncCompleted();
          });
        }
        else {
          this.toaster.error('Could not fetch Loan Object from API')
        }
        loaded = true;
      });

    }
    else {
      this.toaster.error('Something went wrong');
    }
  }

  update(value) {
    this.loanservice.setFilter(value);
    this.getFooterItems(this.loanlist.filteredData);
  }

  /**
   * Function to filter out columns that are marked as 'Do Not Display'
   * @param cols columns
   */
  public getDisplayedColumns(cols) {
    return cols.filter((col) => {
      if(this.listSettings.loanMasterSettings[col]) {
        return this.listSettings.loanMasterSettings[col].isDisplayEnabled;
      }
      return false;
    });
  }

  /**
   * Toggles the footer row enabled/disabled
   * @param ev event from toggle
   */
  public toggleIsPendingActionSortOrderEnabledChange(ev) {
    this.listSettings.isPendingActionSortOrderEnabled = ev.checked;
    this.sortColumn = this.loanListSettings.listSettings.isPendingActionSortOrderEnabled ? 'Pending_Action_Sort_Order' : 'Loan_Pending_Action_Type_Code';
    this.loadLoanList();
  }

  /**
   * Toggles the footer row enabled/disabled
   * @param ev event from toggle
   */
  public toggleIsIncludeMyReportsEnabledChange(ev) {
    this.listSettings.isIncludeMyReportsEnabled = ev.checked;
    let settings = this.loanDetailService.getLoanListColumnsSettings(this.listSettings);
    this.getloanlist(settings);
  }

  /**
   * Toggles the footer row enabled/disabled
   * @param ev event from toggle
   */
  public togglePortfolioAnalysisSettingsChange(ev) {
    this.loanListSettings.portfolioAnalysisSettings.isDetailRecordsEnabled = ev.checked;
  }

  private getMasterFarmerList() {
    if(!this.localstorageservice.retrieve(environment.masterfarmerlist)) {
      this.loanservice.getFarmerList().subscribe(res => {
        if (res.ResCode == 1) {
          this.localstorageservice.store(environment.masterfarmerlist, res.Data);
        } else {
          this.localstorageservice.store(environment.masterfarmerlist, []);
        }
      });
    }
  }

  private getMasterBorrowerList() {
    if(!this.localstorageservice.retrieve(environment.masterborrowerlist)) {
      this.loanservice.getBorrowerList().subscribe(res => {
        if (res.ResCode == 1) {
          this.localstorageservice.store(environment.masterborrowerlist, res.Data);
        } else {
          this.localstorageservice.store(environment.masterborrowerlist, []);
        }
      });
    }
  }

  public updateReminderInd(LoanFullID: string, event) {
    const model = {
      Loan_Full_ID: LoanFullID,
      Pending_Action: event,
      From_Loan_List: true
    };

    let EmailBody = {
      LoanFullID: LoanFullID,
      EmailTemplateType: EmailTemplateType.PendingActionReminder,
      EmailData: {
        PA_Code: event.PA_Group_Code,
        User_ID: this.localstorageservice.retrieve(environment.uid)
      }
    };

    this.loanservice.Update_Reminder_Ind(model).subscribe(res => {
      if(res.ResCode == 1) {
        this.toaster.success('Reminder Updated');
        this.loanService.sendEmailAndNotification(EmailBody).subscribe(res => {});
      } else {
        this.toaster.error('Failed');
      }
    },_ => {
      this.toaster.error('Failed');
    });
  }
}
