import { Component, OnInit, Input } from '@angular/core';

import { PortfolioAnalysisSettings } from '@lenda/preferences/models/loan-list/portfolio-analysis/index.model';

@Component({
  selector: 'app-footer-column',
  templateUrl: './footer-column.component.html'
})
export class FooterColumnComponent implements OnInit {
  @Input() footerSettings: PortfolioAnalysisSettings;

  @Input() sum: string;
  @Input() avg: string;
  @Input() max: string;
  @Input() min: string;

  constructor() {}

  ngOnInit() {}
}
