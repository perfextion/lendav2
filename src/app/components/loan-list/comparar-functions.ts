/**
 * @param propName Column Name
 */
export function StringComparer(propName: string) {
  return (a: any) => a[propName] || '';
}

/**
 * @param propName Column Name
 */
export function NumberComparer(propName: string) {
  return (a: any) => a[propName] || 0;
}
