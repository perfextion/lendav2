import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';
import { JsonConvert } from 'json2typescript';

import { environment } from '@env/environment.prod';

import { Page } from '@lenda/models/page.enum';

import { Preferences } from '@lenda/preferences/models/index.model';
import { loan_model, LoanGroup, LoanStatus } from '@lenda/models/loanmodel';
import { ResponseModel } from '@lenda/models/resmodels/reponse.model';
import { Get_Borrower_Name, Get_Formatted_Date, Get_Loan_Status } from '@lenda/services/common-utils';

import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { LayoutService } from '@lenda/shared/layout/layout.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoanvalidatorService } from '@lenda/services/loanvalidator.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { ApiService } from '@lenda/services';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-viewbalances',
  templateUrl: './viewbalances.component.html',
  styleUrls: ['./viewbalances.component.scss']
})
export class ViewBalancesComponent implements OnInit, OnDestroy {
  public loanlist: Array<any> = new Array<any>();
  public pageheader = 'Loan Listing';

  private loanid: any;

  private dataSubscription: ISubscription;

  constructor(
    private loanService: LoanApiService,
    private route: Router,
    private layoutService: LayoutService,
    private settingsService: SettingsService,
    private loanservice: LoanApiService,
    private logging: LoggingService,
    private loanvalidator: LoanvalidatorService,
    private toaster: ToastrService,
    private loancalculationservice: LoancalculationWorker,
    private apiService : ApiService,
    private localstorageservice:LocalStorageService,
    private dataService: DataService
  ) {
    this.localstorageservice.store(environment.currentpage, Page.viewBalances);
  }

  ngOnInit() {
    this.getloanlist();
    this.dataSubscription = this.dataService.getLoanObject().subscribe(res => {
      let currentPage = this.localstorageservice.retrieve(environment.currentpage);
      if(currentPage == 'viewBalances') {
        this.getloanlist();
      }
    });
  }

  ngOnDestroy() {
    if(this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  getBorrowerName(loanMaster) {
    return Get_Borrower_Name(loanMaster);
  }

  private getloanlist() {

    this.loanlist = [];

    //Checking on the Loan_Full_ID for now since incorrect data is stored in Loan_ID
    let currentLoan: loan_model = this.localstorageservice.retrieve(environment.loankey);

    let loanID: any = currentLoan.Loan_Full_ID;
    loanID = loanID.split('-')[0];
    loanID = Number(loanID);

    if(currentLoan.LoanMaster.Loan_Status == LoanStatus.Uploaded) {
      this.loanlist = [currentLoan];
    }

    let loans: Array<LoanGroup> = this.localstorageservice.retrieve(environment.loanGroup);

    let filteredLoans: Array<LoanGroup> = [];

    if(loans) {
      loans.forEach(a => {
        a.Loan_ID = a.LoanJSON.LoanMaster.Loan_ID;
        a.Loan_Seq_Number = a.LoanJSON.LoanMaster.Loan_Seq_num;
      });

      let latestUploadedLoan = loans.find(a => a.Loan_ID == currentLoan.LoanMaster.Loan_ID && a.LoanJSON.LoanMaster.Loan_Status == LoanStatus.Uploaded);;

      if(latestUploadedLoan) {
        filteredLoans.push(latestUploadedLoan);
      }

      loans.filter(a => a.IsAffiliated && a.LoanJSON.LoanMaster.Loan_Status == LoanStatus.Uploaded && a.Loan_ID != currentLoan.LoanMaster.Loan_ID).forEach(loan => {
        filteredLoans.push(loan);
      });
    }

    filteredLoans.forEach(loan => {
      this.loanlist.push(loan.LoanJSON);
    });
  }

  public getLoanStatus(LoanStatus: string) {
    return Get_Loan_Status(LoanStatus);
  }

  public navigatetoloan(id: string) {
    this.layoutService.toggleRightSidebar(false);
    this.loanid = id
    this.localstorageservice.store(environment.loanidkey, id);
    // Get default landing page from my preferences
    let preferences: Preferences = this.settingsService.preferences;
    //get loan here now
    this.localstorageservice.store(environment.loanGroup,[]);//reset loan group if loan to be opened is not exist in local storage
    this.getLoanBasicDetails(preferences);
  }

  private getLoanBasicDetails(preferences: Preferences) {
    if (this.loanid != null) {
      try {
        this.localstorageservice.clear(environment.loankey);
        this.localstorageservice.clear(environment.loanGroup);
      } catch (e) {
        if (environment.isDebugModeActive) console.log(`trying to clear a non existing local storage key : ${environment.loankey}`);
      }

      this.loanservice.getLoanById(this.loanid).subscribe(res => {
        this.logging.checkandcreatelog(3, 'Overview', "APi LOAN GET with Response " + res.ResCode);
        if (res.ResCode == 1) {

          this.loanvalidator.validateloanobject(res.Data).then(errors => {
            if (errors && errors.length > 0) {
              this.toaster.error("Loan Data is invalid ... Rolling Back to list")
              this.route.navigateByUrl("/home/loans");
            } else {
              this.layoutService.toggleRightSidebar(false);
              let jsonConvert: JsonConvert = new JsonConvert();

              this.loancalculationservice.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model), true, true);
              this.localstorageservice.store(environment.errorbase, []);
              this.loancalculationservice.saveValidationsToLocalStorage(res.Data);

              //we are making a copy of it also
              this.localstorageservice.store(environment.loankey_copy, res.Data);
              this.localstorageservice.store(environment.currentpage, preferences.userSettings.landingPage);

                //loan affiliated loan, currently static
              const route = '/api/Loans/GetLoanGroups?LoanFullID='+(res.Data as loan_model).Loan_Full_ID;
              this.apiService.get(route).subscribe((resLoan : ResponseModel) => {
                  if(resLoan){
                    let loanGrpups : Array<LoanGroup> = resLoan.Data;
                    this.localstorageservice.store(environment.loanGroup,resLoan.Data);
                    this.route.navigateByUrl("/home/loanoverview/" + this.loanid.replace("-", "/") + '/' + preferences.userSettings.landingPage);
                  } else {
                    this.route.navigateByUrl("/home/loanoverview/" + this.loanid.replace("-", "/") + '/' + preferences.userSettings.landingPage);
                  }
              }, _ => {
                this.route.navigateByUrl("/home/loanoverview/" + this.loanid.replace("-", "/") + '/' + preferences.userSettings.landingPage);
              });
            }
            this.localstorageservice.store(environment.exceptionStorageKey, []);
            this.localstorageservice.store(environment.modifiedbase, []);
            this.localstorageservice.store(environment.modifiedoptimizerdata, null);
          });
        } else {
          this.toaster.error("Could not fetch Loan Object from API")
        }
      });

    } else {
      this.toaster.error("Something went wrong");
    }
  }

  formateDate(strDate: any){
    if(strDate){
      try{
        let d = Get_Formatted_Date(strDate);
        if(d == '01/01/1900') {
          return '-';
        }
        return d;
      } catch {
        return '-';
      }
    } else {
      return '-';
    }
  }
}
