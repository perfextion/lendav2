import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { ISubscription } from 'rxjs/Subscription';
import { JsonConvert } from 'json2typescript';

import { environment } from '@env/environment.prod';

import { Page } from '@lenda/models/page.enum';

import { LoanGroup, loan_model, LoanStatus, Loan_Cross_Collateral } from '@lenda/models/loanmodel';
import { Get_Formatted_Date, Get_Borrower_Name, Get_Loan_Status } from '@lenda/services/common-utils';
import { Preferences } from '@lenda/preferences/models/index.model';
import { ChangeStatusModel, LoanStatusChange } from '@lenda/models/copy-loan/copyLoanModel';
import { ResponseModel } from '@lenda/models/resmodels/reponse.model';

import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoanvalidatorService } from '@lenda/services/loanvalidator.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { PublishService } from '@lenda/services/publish.service';
import { LayoutService } from '@lenda/shared/layout/layout.service';
import { MasterService } from '@lenda/master/master.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-loan-history',
  templateUrl: './loan-history.component.html',
  styleUrls: ['./loan-history.component.scss']
})
export class LoanHistoryComponent implements OnInit, OnDestroy {
  public loanlist: Array<any> = new Array<any>();
  public pageheader = 'Loan Listing';

  public isSyncRequired: boolean;

  private syncSubscription: ISubscription;
  private saveSubscription: ISubscription;

  public loanid: string;

  constructor(
    private router: Router,
    private localstorageservice:LocalStorageService,
    public dialog: MatDialog,
    private logging: LoggingService,
    private loancalculationservice: LoancalculationWorker,
    private loanvalidator: LoanvalidatorService,
    private loanservice: LoanApiService,
    private toaster: ToastrService,
    private settingsService: SettingsService,
    private publishService: PublishService,
    private layoutService: LayoutService,
    private masterService: MasterService,
    private alertify: AlertifyService,
    private dataservice: DataService
  ) {
    this.localstorageservice.store(environment.currentpage, Page.loanHistory);
  }

  ngOnInit() {
    this.getloanlist();

    this.syncSubscription = this.publishService.listenToSyncRequired().subscribe(res => {
      this.isSyncRequired = res && res.length > 0;
    });

    this.saveSubscription = this.dataservice.getLoanObject().subscribe(res => {
      let currentPage = this.localstorageservice.retrieve(environment.currentpage);
      if(currentPage == 'loanHistory') {
        this.getloanlist();
      }
    });
  }

  ngOnDestroy() {
    if(this.saveSubscription) {
      this.saveSubscription.unsubscribe();
    }

    if(this.syncSubscription) {
      this.syncSubscription.unsubscribe();
    }
  }

  getBorrowerName(loanMaster) {
    return Get_Borrower_Name(loanMaster);
  }

  private getloanlist() {
    this.loanlist = [];

    let currentLoan: loan_model = this.localstorageservice.retrieve(environment.loankey);
    this.loanlist.push(currentLoan);

    let filteredLoans = this.localstorageservice.retrieve(environment.loanGroup) as Array<LoanGroup>;

    if(filteredLoans) {
      filteredLoans.forEach(a => {
        a.Loan_ID = a.LoanJSON.LoanMaster.Loan_ID;
        a.Loan_Seq_Number = a.LoanJSON.LoanMaster.Loan_Seq_num;
      });

      filteredLoans = filteredLoans.filter(A => A.LoanJSON.LoanMaster.Loan_ID == currentLoan.LoanMaster.Loan_ID)

      filteredLoans = _.orderBy(filteredLoans, ['Loan_Seq_Number'], ['desc']);

      filteredLoans.forEach(loan => {
        this.loanlist.push(loan.LoanJSON);
      });
    }
  }

 formateDate(strDate: any){
    if(strDate){
      try{
        let d = Get_Formatted_Date(strDate);
        if(d == '01/01/1900') {
          return '-';
        }
        return d;
      } catch {
        return '-';
      }
    } else {
      return '-';
    }
  }

  public getLoanStatus(LoanStatus: string) {
    return Get_Loan_Status(LoanStatus);
  }

  public IsNotWorkingLoan(Loan: loan_model) {
    return this.getLoanStatus(Loan.LoanMaster.Loan_Status) != LoanStatus.Working;
  }

  public reviveCurrentLoan(Loan_Full_ID: string) {
    this.Check_Sync_And_Perform(() => {
      if (Loan_Full_ID) {
        this.alertify
        .confirm('Confirm', 'Are you sure you want to Revive/Copy this Loan?')
        .subscribe(res => {
          if(res) {
            const body = <ChangeStatusModel>{
              FromLoanFullID: Loan_Full_ID,
              ToStatus: LoanStatusChange.ReviveOrCopy
            };

            this.loanservice.changeStatus(body).subscribe(
              res => {
                if(res.ResCode == 1) {
                  this.toaster.success('Revive/Copy Loan Successful.');
                  this.navigatetoloan(res.Data);
                } else {
                  this.toaster.error('Revive/Copy Loan Failed.');
                }
              },
              error => {
                this.toaster.error('Revive/Copy Loan Failed.');
              }
            );
          }
        });
      }
    });
  }

  public navigatetoloan(loanid: string) {
    this.Check_Sync_And_Perform(() => {
      this.loanid = loanid;
      this.layoutService.toggleRightSidebar(false);
      let preferences: Preferences = this.settingsService.preferences;
      this.localstorageservice.store(environment.loanGroup,[]);
      this.getLoanBasicDetails(preferences);
    });
  }

  private getLoanBasicDetails(preferences: Preferences) {
    if (this.loanid != null) {
      try {
        this.localstorageservice.clear(environment.loankey);
        this.localstorageservice.clear(environment.loanGroup);
      } catch (e) {
        if (environment.isDebugModeActive) console.log(`trying to clear a non existing local storage key : ${environment.loankey}`);
      }

      this.loanservice.getLoanById(this.loanid).subscribe(res => {
        this.masterService.isMinimize = false;
        this.logging.checkandcreatelog(3, 'Overview', "APi LOAN GET with Response " + res.ResCode);
        if (res.ResCode == 1) {

          this.loanvalidator.validateloanobject(res.Data).then(errors => {
            if (errors && errors.length > 0) {

              this.toaster.error("Loan Data is invalid ... Rolling Back to list")
              this.router.navigateByUrl("/home/loans");


            } else {
              this.layoutService.toggleRightSidebar(false);
              let jsonConvert: JsonConvert = new JsonConvert();

              this.localstorageservice.store(environment.errorbase, []);
              this.loancalculationservice.saveValidationsToLocalStorage(res.Data);
              //we are making a copy of it also
              this.localstorageservice.store(environment.loankey_copy, res.Data);
              this.localstorageservice.store(environment.currentpage, preferences.userSettings.landingPage);

              //loan affiliated loan, currently static
              this.loanservice.getLoanGroups(this.loanid).subscribe((resp: ResponseModel) => {
                if (resp) {
                  this.localstorageservice.store(environment.loanGroup, resp.Data);
                  this.localstorageservice.store(environment.loanidkey, this.loanid);
                  this.loancalculationservice.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model));
                  this.router.navigateByUrl("/home/loanoverview/" + this.loanid.replace("-", "/") + '/' + preferences.userSettings.landingPage);
                } else {
                  this.router.navigateByUrl("/home/loanoverview/" + this.loanid.replace("-", "/") + '/' + preferences.userSettings.landingPage);
                }
              }, error => {
                this.router.navigateByUrl("/home/loanoverview/" + this.loanid.replace("-", "/") + '/' + preferences.userSettings.landingPage);
              });

              this.localstorageservice.store(environment.loanCrossCollateral, []);
              this.loanservice.getDistinctLoanIDList(this.loanid).subscribe(res => {
                if (res && res.ResCode == 1) {
                  this.localstorageservice.store(environment.loanCrossCollateral, res.Data as Array<Loan_Cross_Collateral>);
                }
              });
            }
            this.localstorageservice.store(environment.exceptionStorageKey, []);
            this.localstorageservice.store(environment.modifiedbase, []);
            this.localstorageservice.store(environment.modifiedoptimizerdata, null);
            this.publishService.syncCompleted();
          });
        } else {
          this.toaster.error("Could not fetch Loan Object from API")
        }
      });

    } else {
      this.toaster.error("Something went wrong");
    }
  }

  public Check_Sync_And_Perform(onSuccess: () => void) {
    if(this.isSyncRequired) {
      this.alertify.confirm(
        'Confirm',
        environment.unsavedChangesWarningMessage
      ).subscribe(res => {
        if(res) {
          this.publishService.syncCompleted();
          onSuccess();
        }
      });
    } else {
      onSuccess();
    }
  }
}
