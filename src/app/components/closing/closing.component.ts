import { Component, OnInit, OnDestroy } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { DataService } from '@lenda/services/data.service';
import { ISubscription } from 'rxjs/Subscription';
import { loan_farmer, loan_model, Loan_Condition } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { PublishService } from '@lenda/services/publish.service';
import { Page } from '@lenda/models/page.enum';
import { ClosingService } from '@lenda/components/closing/closing.service';

@Component({
  selector: 'app-closing',
  templateUrl: './closing.component.html',
  styleUrls: ['./closing.component.scss'],
  providers : [ClosingService]
})
export class ClosingComponent implements OnInit, OnDestroy {

  private subscription : ISubscription;
  private localLoanObject : loan_model;
  loanPreReqConditions : Array<Loan_Condition> = [];
  loanClosingConditions : Array<Loan_Condition> = [];
  currentPageName : Page = Page.closing;



  constructor(private localStorageService : LocalStorageService,
  private dataService : DataService,
  private loanCalculationService : LoancalculationWorker,
  private publishService : PublishService,
  private closingService : ClosingService) { }

  ngOnInit() {

    this.subscription = this.dataService.getLoanObject().subscribe((res : loan_model)=>{
      if(res){
        this.localLoanObject = res;
        if(this.localLoanObject.LoanConditions){
          this.loanPreReqConditions = this.localLoanObject.LoanConditions.filter(lc=>lc.Condition_Level == 1 && lc.Action_Status != 3);
          this.loanClosingConditions = this.localLoanObject.LoanConditions.filter(lc=>lc.Condition_Level == 2 && lc.Action_Status != 3);
        }

      }
    });

    this.localLoanObject = this.localStorageService.retrieve(environment.loankey);
    if(this.localLoanObject && this.localLoanObject.LoanConditions){
      this.loanPreReqConditions = this.localLoanObject.LoanConditions.filter(lc=>lc.Condition_Level == 1 && lc.Action_Status != 3);
      this.loanClosingConditions = this.localLoanObject.LoanConditions.filter(lc=>lc.Condition_Level == 2 && lc.Action_Status != 3);
    }

  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  onChange(condition : Loan_Condition){
    let existingCondition: Loan_Condition;

    if(condition.Loan_Condition_ID){
      existingCondition = this.localLoanObject.LoanConditions.find(lc=>lc.Loan_Condition_ID == condition.Loan_Condition_ID);
      existingCondition.Action_Status = 2;
    }else{
      existingCondition = this.localLoanObject.LoanConditions.find(lc => lc.Condition_Key == condition.Condition_Key);
      existingCondition.Action_Status = 1;
    }
    existingCondition.Suggestion_Only_Ind = condition.Suggestion_Only_Ind ? 1 : 0;

    this.loanCalculationService.performcalculationonloanobject(this.localLoanObject, false);
    this.publishService.enableSync(Page.closing);
  }

  /**
   * Sync to database - publish button event
   */
  synctoDb() {
    setTimeout(()=> {
      // waiting for 0.5 sec to get the blur events excecuted meanwhile
      this.publishService.syncCompleted();
      this.closingService.syncToDb(this.localStorageService.retrieve(environment.loankey));
     }, 500);

  }
}
