import { Component, OnInit } from '@angular/core';
import { loan_farmer, loan_model, BorrowerEntityType, borrower_model, LoanGroup, Loan_Type_Codes, Loantype_dsctext } from '../../models/loanmodel';
import { LoanApiService } from '../../services/loan/loanapi.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment.prod';
import { LoancalculationWorker } from '../../Workers/calculations/loancalculationworker';
import { JsonConvert } from 'json2typescript';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { ResponseModel } from '@lenda/models/resmodels/reponse.model';
import { ApiService } from '@lenda/services';
import { RefDataModel, LoanMaster } from '@lenda/models/ref-data-model';
import { IDType } from '../borrower/borrower-info/borrower-info-form/borrower-info-form.component';
import { RefDataService } from '@lenda/services/ref-data.service';

@Component({
  selector: 'app-create-loan',
  templateUrl: './create-loan.component.html',
  styleUrls: ['./create-loan.component.scss']
})
export class CreateLoanComponent implements OnInit {

  public farmerParamsObj: farmer_params = new farmer_params();
  farmerSuccessCallback;
  useFarmer;
  userBorrower = false;
  public borrowerParamsObj: borrower_params = new borrower_params();
  borrowerInfo : borrower_model = new borrower_model();
  farmerInfo : loan_farmer = new loan_farmer();
  cropYear : number;

  refData: RefDataModel;

  btnEnabled = true;

  constructor(
    private loanApiService: LoanApiService,
    private toaster: ToastrService,
    private route : Router,
    private loancalculationservice: LoancalculationWorker,
    private localstorageservice: LocalStorageService,
    private apiService : ApiService,
    private refDataService: RefDataService,
    private sessionstorage: SessionStorageService
  ) {
      this.refData = this.localstorageservice.retrieve(environment.referencedatakey);
    }

  ngOnInit() {

    let localLoanObject = this.localstorageservice.retrieve(environment.loankey);
    if(localLoanObject){
      this.localstorageservice.clear(environment.loankey);
    }
    this.borrowerInfo.Borrower_Entity_Type_Code = BorrowerEntityType.Individual;
    this.cropYear =  (new Date()).getFullYear();
  }
  onFarmerFormValueChange(data) {
    this.farmerParamsObj = Object.assign(new farmer_params(), data);

  }

  onBorrowerFormValueChange(data) {
    this.borrowerParamsObj = Object.assign(new borrower_params(), data);
    this.borrowerInfo = this.borrowerParamsObj.value;
  }

  useFarmerChange(e) {
    if (e && this.farmerParamsObj.value) {
      this.borrowerParamsObj.value.Borrower_First_Name = this.farmerParamsObj.value.Farmer_First_Name;
      this.borrowerParamsObj.value.Borrower_MI = this.farmerParamsObj.value.Farmer_MI;
      this.borrowerParamsObj.value.Borrower_Last_Name = this.farmerParamsObj.value.Farmer_Last_Name;
      this.borrowerParamsObj.value.Borrower_SSN_Hash = this.farmerParamsObj.value.Farmer_SSN_Hash;
      this.borrowerParamsObj.value['Borrower_DL_State'] = this.farmerParamsObj.value.Farmer_DL_State;
      this.borrowerParamsObj.value.Borrower_Dl_Num = this.farmerParamsObj.value.Farmer_DL_Num;
      this.borrowerParamsObj.value.Borrower_Address = this.farmerParamsObj.value.Farmer_Address;
      this.borrowerParamsObj.value.Borrower_City = this.farmerParamsObj.value.Farmer_City;
      this.borrowerParamsObj.value['Borrower_State_Abbrev'] = this.farmerParamsObj.value.Farmer_State;
      this.borrowerParamsObj.value.Borrower_Zip = this.farmerParamsObj.value.Farmer_Zip;
      this.borrowerParamsObj.value.Borrower_Phone = this.farmerParamsObj.value.Farmer_Phone;
      this.borrowerParamsObj.value['Borrower_Email'] = this.farmerParamsObj.value.Farmer_Email;
      this.borrowerParamsObj.value.Borrower_DOB = this.farmerParamsObj.value.Farmer_DOB;
      this.borrowerParamsObj.value.Borrower_Preferred_Contact_Ind = this.farmerParamsObj.value.Farmer_Preferred_Contact_Ind;
      this.borrowerParamsObj.value.Borrower_Entity_Type_Code = BorrowerEntityType.Individual;
      this.borrowerParamsObj.value.Borrower_ID_Type = IDType.SSN;

      this.borrowerInfo = Object.assign(new borrower_model(), this.borrowerParamsObj.value);
      this.borrowerParamsObj.isValid = Object.assign(this.borrowerParamsObj.isValid, this.farmerParamsObj.isValid);
    }
  }

  useBorrowerChange(e) {
    if (e && this.borrowerParamsObj.value) {
      this.farmerParamsObj.value.Farmer_First_Name = this.borrowerParamsObj.value.Borrower_First_Name;
      this.farmerParamsObj.value.Farmer_MI = this.borrowerParamsObj.value.Borrower_MI;
      this.farmerParamsObj.value.Farmer_Last_Name = this.borrowerParamsObj.value.Borrower_Last_Name;
      this.farmerParamsObj.value.Farmer_SSN_Hash = this.borrowerParamsObj.value.Borrower_SSN_Hash;
      this.farmerParamsObj.value.Farmer_Address = this.borrowerParamsObj.value.Borrower_Address;
      this.farmerParamsObj.value.Farmer_City = this.borrowerParamsObj.value.Borrower_City;
      this.farmerParamsObj.value.Farmer_State = this.borrowerParamsObj.value.Borrower_State_ID;
      this.farmerParamsObj.value.Farmer_Zip = this.borrowerParamsObj.value.Borrower_Zip;
      this.farmerParamsObj.value.Farmer_DL_Num = this.borrowerParamsObj.value.Borrower_Dl_Num;
      this.farmerParamsObj.value.Farmer_DL_State = this.borrowerParamsObj.value.Borrower_DL_state;
      this.farmerParamsObj.value.Farmer_Phone = this.borrowerParamsObj.value.Borrower_Phone;
      this.farmerParamsObj.value.Farmer_Email = this.borrowerParamsObj.value.Borrower_email;
      this.farmerParamsObj.value.Farmer_DOB = this.borrowerParamsObj.value.Borrower_DOB;
      this.farmerParamsObj.value.Farmer_Preferred_Contact_Ind = this.borrowerParamsObj.value.Borrower_Preferred_Contact_Ind;

      this.farmerInfo = Object.assign(new loan_farmer(), this.farmerParamsObj.value);
      this.farmerParamsObj.isValid = Object.assign(this.farmerParamsObj.isValid, this.farmerParamsObj.isValid);
    }
  }

  onSave(event:any) {

    let userID = this.localstorageservice.retrieve(environment.uid);
    let loanObj = Object.assign({}, this.farmerParamsObj.value, this.borrowerParamsObj.value,{Crop_Year : this.cropYear}, {User_ID : userID});
    if (this.farmerParamsObj.isValid && this.borrowerParamsObj.isValid && this.cropYear) {
      this.btnEnabled = false;
      this.Validate_Date(loanObj);
      this.Set_Office_And_Region_ID(loanObj);
      this.set_DL_State(loanObj);
      this.setDefaultDiscounts(loanObj);

      this.loanApiService.createLoan_New(loanObj).subscribe((successResponse) => {
        if(successResponse.ResCode == 1) {
          this.toaster.success("Details saved successfully, navigating to Loan Dashboard...");
          this.loanApiService.getLoanById(successResponse.Data).subscribe(res => {

            if (res.ResCode == 1) {

              let jsonConvert: JsonConvert = new JsonConvert();
              this.loancalculationservice.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model), true, true);
              this.loancalculationservice.saveValidationsToLocalStorage(res.Data);

              //we are making a copy of it also
              this.localstorageservice.store(environment.loankey_copy, res.Data);
              this.route.navigateByUrl("/home/loanoverview/"+successResponse.Data.replace("-","/")+"/borrower");

                this.localstorageservice.clear(environment.loanGroup);
                const route = '/api/Loans/GetLoanGroups?LoanFullID='+(res.Data as loan_model).Loan_Full_ID;
                this.apiService.get(route).subscribe((res : ResponseModel) => {
                    if(res){
                      let loanGrpups : Array<LoanGroup> = res.Data;
                      this.localstorageservice.store(environment.loanGroup,res.Data);
                    }
                });
            }
            else {
              this.toaster.error("Some Error Occured.Could not fetch Loan Object from API");
            }
          });
        } else {
          this.btnEnabled = true;
          this.toaster.error("Some Error Occured.Could not create new Loan.");
        }

      }, (errorResponse) => {
        this.toaster.error("Error Occurered while saving Farmer details");

      });
      //console.log(Object.assign({}, this.farmerParamsObj.value, this.borrowerParamsObj.value));
    } else {
      this.toaster.error("The data doesn't seem to have data in correct format, please correct them before saving.");
    }
  }


  private setDefaultDiscounts(loanObj: any) {
    // balance sheet
    loanObj['Current_Assets_Disc_Percent'] = this.getDiscount('CURRENT_DISC');
    loanObj['Inter_Assets_Disc_Percent'] = this.getDiscount('INTER_DISC');
    loanObj['Fixed_Assets_Disc_Percent'] = this.getDiscount('FIXED_DISC');

    // terms
    loanObj['Origination_Fee_Percent'] = this.getDiscount('ORG_PCT');
    loanObj['Service_Fee_Percent'] = this.getDiscount('SERVICE_PCT');
    loanObj['Rate_Percent'] = this.getDiscount('INTEREST_PCT');

    // Maturity Date
    let d = new Date();
    loanObj['Maturity_Date'] = this.refDataService.getDefaultMaturityDateString(d.getFullYear());
    // Loan_Type_Code
    loanObj['Loan_Type_Code'] = Loan_Type_Codes.AllIn;
    loanObj['Loan_Type_Name'] = Loantype_dsctext(Loan_Type_Codes.AllIn);
  }

  private set_DL_State(loanObj) {
    if(!loanObj['Farmer_DL_State']) {
      loanObj['Farmer_DL_State'] = loanObj['Farmer_State'];
    }

    if(!loanObj['Borrower_DL_State']) {
      loanObj['Borrower_DL_State'] = loanObj['Borrower_State_Abbrev'];
    }
  }

  private Set_Office_And_Region_ID(loanObj: LoanMaster) {
    let user = this.sessionstorage.retrieve('UID');
    if(user) {
      loanObj.Office_ID = user.Office_ID;
      loanObj.Region_ID = user.Region_ID;
    }
  }

  private Validate_Date(loanObj: LoanMaster) {
    let d = new Date(loanObj.Farmer_DOB);

    if(isNaN(+d)) {
      loanObj.Farmer_DOB = null;
    } else {
      if(d.getFullYear() < 1800) {
        loanObj.Farmer_DOB = null;
      }
    }

    d = new Date(loanObj.Borrower_DOB);

    if(isNaN(+d)) {
      loanObj.Borrower_DOB = null;
    } else {
      if(d.getFullYear() < 1800) {
        loanObj.Borrower_DOB = null;
      }
    }
  }

  private getDiscount(key: string){
    try {
      let disc = this.refData.Discounts.find(a => a.Discount_Key == key);
      return disc.Discount_Value;
    } catch {
      return 0;
    }
  }

}

class farmer_params {
  isValid: boolean = false;
  value: loan_farmer = new loan_farmer();
  successCallback: Function;
}

class borrower_params {
  isValid: boolean = false;
  value: borrower_model = new borrower_model();
  successCallback: Function;
}
