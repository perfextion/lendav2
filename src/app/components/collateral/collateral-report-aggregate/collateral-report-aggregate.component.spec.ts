import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollateralReportAggregateComponent } from './collateral-report-aggregate.component';

describe('CollateralReportAggregateComponent', () => {
  let component: CollateralReportAggregateComponent;
  let fixture: ComponentFixture<CollateralReportAggregateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollateralReportAggregateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollateralReportAggregateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
