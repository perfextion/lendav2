import { Component, OnInit } from '@angular/core';
import { loan_model } from '@lenda/models/loanmodel';
import { environment } from '@env/environment';
import { LocalStorageService } from 'ngx-webstorage';
import { CollateralReportAggregateService } from './collateral-report-aggregate.service';

@Component({
  selector: 'app-collateral-report-aggregate',
  templateUrl: './collateral-report-aggregate.component.html',
  styleUrls: ['./collateral-report-aggregate.component.scss']
})
export class CollateralReportAggregateComponent implements OnInit {
  private refdata: any;
  public localloanobject: loan_model = new loan_model();
  public columnDefs = [];
  public rowGroupPanelShow;
  rowData = [];

  constructor(
    private collateralReportAggregateService: CollateralReportAggregateService,
    private localstorageservice: LocalStorageService) { }

  ngOnInit() {
    this.columnDefs = this.collateralReportAggregateService.columnDefs;
    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);
    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
    this.rowGroupPanelShow = "always";
    this.rowData = this.collateralReportAggregateService.getGridData(this.localloanobject, this.refdata);
  }
}
