import { Injectable } from "@angular/core";
import { loan_model } from "@lenda/models/loanmodel";
import { lookupCountyValueFromPassedRefData, lookupStateCodeFromPassedRefObject } from '@lenda/Workers/utility/aggrid/stateandcountyboxes';
import { lookupCropTypeWithRefData, lookupCropPracticeTypeWithRefData } from '@lenda/Workers/utility/aggrid/cropboxes';
import { RefDataModel } from "@lenda/models/ref-data-model";

@Injectable()
export class CollateralReportAggregateService {
    private rowData = [];

    public columnDefs = [
        {
            field: "State",
            enableRowGroup: true
        },
        {
            field: "County",
            enableRowGroup: true
        },
        {
            field: "FSN",
            enableRowGroup: true
        },
        {
            field: "Prod_Perc",
            enableRowGroup: true
        },
        {
            field: "Landlord",
            enableRowGroup: true,
            pivot: true,
            enablePivot: true
        },
        {
            field: "Section",
            enableRowGroup: true
        },
        {
            field: "Rated",
            enableRowGroup: true
        },
        {
            field: "Crop",
            enableRowGroup: true,
            rowGroup: true
        },
        {
            field: "Crop_Type",
            enableRowGroup: true
        },
        {
            field: "Irr_Prac",
            enableRowGroup: true,
            rowGroup: true
        },
        {
            field: "Crop_Prac",
            enableRowGroup: true
        },
        {
            field: "Ins_Plan",
            enableRowGroup: true
        },
        {
            field: "Ins_Type",
            enableRowGroup: true
        },
        {
            field: "Market_Value",
            enableRowGroup: true,
            aggFunc: "sum",
        },
    ];

    getGridData(localloanobject: loan_model, refdata) {
        let objects = localloanobject.LoanCropUnits.filter(p => p.CU_Acres > 0);
        for (let i = 0; i < objects.length; i++) {

            let farm = localloanobject.Farms.find(p => p.Farm_ID == objects[i].Farm_ID);
            let row: any = {};
            row.Category = "Crop"
            row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, refdata);
            row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, refdata);
            row.Prod_Perc = (100 - objects[i].FC_Rent_Percent) + "%";;
            row.Landlord = farm.Landowner;
            row.FSN = farm.FSN;
            row.Section = farm.Section;
            row.Rated = farm.Rated
            row.Crop = this.getcropname(refdata, objects[i].Crop_Code);
            row.Acres = objects[i].CU_Acres;
            row.Perc_Ins = (farm.Permission_To_Insure ? 100 : farm.Percent_Prod) + "%";
            row.Crop_Type = lookupCropTypeWithRefData(objects[i].Crop_Practice_ID, refdata);
            row.Irr_Prac = objects[i].Crop_Practice_Type_Code;
            row.Crop_Prac = lookupCropPracticeTypeWithRefData(objects[i].Crop_Practice_ID, refdata);
            row.Ins_Plan = "";
            row.Ins_Type = "";
            row.Settings = false;
            row.Arm_Budget = "$ " + this.normalizevalue(objects[i].Arm_Budget_Sum || 0);
            row.Dist_Budget = "$ " + this.normalizevalue(objects[i].Dist_Budget_Sum || 0);
            row.Thirparty_Budget = "$ " + this.normalizevalue(objects[i].Third_Budget_Sum || 0);
            row.Market_Value = this.normalizevalue(objects[i].Mkt_Value || 0);
            row.Disc_Market_Value = this.normalizevalue(objects[i].Disc_Mkt_Value || 0);
            row.Ins_Value = "-"
            row.Disc_Ins_Value = "-"
            row.CEI = "$ " + this.normalizevalue(objects[i].CEI_Value || 0);
            row.CF = "$ " + this.normalizevalue(objects[i].FC_Cashflow || 0);
            row.RC = "$ " + this.normalizevalue(objects[i].FC_RiskCushion || 0);
            row.Margin = "$ " + this.normalizevalue(objects[i].FC_Margin || 0);
            this.rowData.push(row);

            localloanobject.CropUnitPolicies.filter(p => p.Status != 3 && p.Loan_Crop_Unit_ID == objects[i].Loan_CU_ID).forEach(element => {
                let row: any = {};
                row.Category = "Crop"
                row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, refdata);
                row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, refdata);
                row.Prod_Perc = (100 - objects[i].FC_Rent_Percent) + "%";
                row.Landlord = farm.Landowner;
                row.FSN = farm.FSN;
                row.Section = farm.Section;
                row.Rated = farm.Rated
                row.Crop = this.getcropname(refdata, objects[i].Crop_Code)
                row.Crop_Type = "-"
                row.Irr_Prac = objects[i].Crop_Practice_Type_Code;
                row.Crop_Prac = objects[i].Crop_Practice_Type_Code;

                row.Ins_Plan = element.Ins_Plan_Code;
                row.Ins_Type = element.Ins_Plan_Type_Code == "" ? '-' : element.Ins_Plan_Type_Code;
                row.Settings = false;
                row.Arm_Budget = "-";
                row.Dist_Budget = "-";
                row.Thirparty_Budget = "-";
                row.Market_Value = "-";
                row.Disc_Market_Value = "-";
                row.Ins_Value = "$ " + this.normalizevalue(element.Ins_Value || 0);
                row.Disc_Ins_Value = "$ " + this.normalizevalue(element.Disc_Ins_Value || 0);
                row.CEI = "-";
                row.CF = "-";
                row.RC = "-";
                row.Margin = "-";
                this.rowData.push(row);
            });
        }
        return this.rowData;
    }

    normalizevalue(amount) {
        if (isNaN(amount) || amount == null || amount == "") {
            return 0;
        }
        else
            return amount;
    }

    getcropname(refdata: RefDataModel, Code) {
        try {
            return refdata.CropList.find(p => p.Crop_Code == Code).Crop_Name;
        }
        catch{
            return '';
        }
    }
}
