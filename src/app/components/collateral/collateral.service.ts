import { Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { JsonConvert } from 'json2typescript';

import { loan_model, Loan_Collateral, Loan_Marketing_Contract } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import CollateralSettings from './collateral-types.model';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';

import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { ToasterService } from '@lenda/services/toaster.service';
import { FsaService } from './fsa/fsa.service';
import { GlobalService } from '@lenda/services/global.service';

/**
 * Shared service for collateral
 */
@Injectable()
export class CollateralService {
  public deleteAction = false;

  constructor(
    public localstorageservice: LocalStorageService,
    public logging: LoggingService,
    public loanserviceworker: LoancalculationWorker,
    public alertify: AlertifyService,
    public loanapi: LoanApiService,
    public toasterService: ToasterService,
    public fsaService: FsaService
  ) {
  }

  getRowData(localloanobject: loan_model, categoryCode: string, source: string, sourceKey: string) {
    if(localloanobject){
      if (sourceKey === '') {
        // Marketing Contracts
        return localloanobject[source] !== null ?
          localloanobject[source].filter(lc => {
            return categoryCode ? lc.ActionStatus !== 3 && categoryCode === lc.Collateral_Category_Code : lc.ActionStatus !== 3;
          }) : [];
      }
    }
  }

  addRow(localloanobject: loan_model, gridApi, rowData, newItemCategoryCode, source, sourceKey, collateralCatCode?: string) {
    let newItem;

    if (localloanobject[source] == null) {
      localloanobject[source] = [];
    }

    if (newItemCategoryCode === CollateralSettings.marketingContracts.key) {
      newItem = new Loan_Marketing_Contract();
      newItem.Z_Loan_ID = localloanobject.LoanMaster.Loan_ID;
      newItem.Z_Loan_Seq_Num = localloanobject.LoanMaster.Loan_Seq_num;
      newItem.Contract_ID = getRandomInt(Math.pow(10,8), Math.pow(10,10));
      newItem.Contract = '';
      newItem.Contract_Type_Code = '';
      newItem.ActionStatus = 1;
      newItem.Crop_Code = '';
      newItem.Assoc_ID = '';
      newItem.Assoc_Type_Code = 'BUY';
      newItem.Quantity = '';
      newItem.Price = '';
      newItem.Status = 1;
    } else {
      newItem = new Loan_Collateral();
      newItem.Collateral_ID = getRandomInt(Math.pow(10, 8), Math.pow(10,10));
      newItem.Collateral_Category_Code = collateralCatCode;

      if (sourceKey && sourceKey !== '') {
        newItem[sourceKey] = newItemCategoryCode;
        newItem.Disc_Value = 50;
        newItem.ActionStatus = 1
      }
    }

    newItem.Loan_Full_ID = localloanobject.Loan_Full_ID;

    let res = rowData.push(newItem);
    // if (newItemCategoryCode !== CollateralSettings.marketingContracts.key) {
    localloanobject[source].push(newItem);
    // }
    gridApi.setRowData(rowData);
    if (newItemCategoryCode === CollateralSettings.marketingContracts.key) {
      gridApi.startEditingCell({
        rowIndex: rowData.length - 1,
        colKey: CollateralSettings.marketingContracts.colKey
      });
    } else {
      gridApi.startEditingCell({
        rowIndex: rowData.length - 1,
        colKey: CollateralSettings.fsa.colKey
      });
    }


  }

  deleteClicked(rowIndex: any, localloanobject: loan_model, rowData, source, uniqueKey, onSuccess = () => {}) {
    this.alertify.deleteRecord().subscribe(res => {
      if (res == true) {
        let obj = rowData[rowIndex] as Loan_Collateral;
        if (obj && obj[uniqueKey] == 0) {
          rowData.splice(rowIndex, 1);
          localloanobject[source].splice(localloanobject[source].indexOf(obj), 1);
        } else {
          this.deleteAction = true;
          if(obj) obj.ActionStatus = 3;
          // localloanobject[source].splice(localloanobject[source].indexOf(obj), 1);
        }

        if(obj.Income_Ind == 1) {
          let other_incomes = localloanobject.LoanOtherIncomes.filter(a => a.ActionStatus != 3 && (a.Loan_Collateral_ID == obj.Collateral_ID || a.Loan_Other_Income_ID == obj.Loan_Other_Income_ID));
          other_incomes.forEach(other_income => {
            if(other_income.ActionStatus == 1) {
              localloanobject.LoanOtherIncomes.splice(localloanobject.LoanOtherIncomes.indexOf(other_income), 1);
            } else {
              other_income.ActionStatus = 3;
            }
          });
        }
        onSuccess();
        this.loanserviceworker.performcalculationonloanobject(localloanobject);
      }
    })
  }

  syncToDb(localloanobject: loan_model) {
    // Loan Condition Calculation
    localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);

    this.loanapi.syncloanobject(localloanobject).subscribe(res => {
      if (res.ResCode == 1) {
        this.localstorageservice.store(environment.modifiedbase, []);
        this.deleteAction = false;
        this.loanapi.getLoanById(localloanobject.Loan_Full_ID).subscribe(res1 => {
          this.logging.checkandcreatelog(3, 'Overview', "APi LOAN GET with Response " + res1.ResCode);
          if (res1.ResCode == 1) {
            this.toasterService.success("Records Synced");
            let jsonConvert: JsonConvert = new JsonConvert();
            this.loanserviceworker.performcalculationonloanobject(jsonConvert.deserialize(res1.Data, loan_model));
            this.loanserviceworker.saveValidationsToLocalStorage(res1.Data);
            // new GlobalService(this.localstorageservice).updateLocalStorage('MktCon_');
            new GlobalService(this.localstorageservice).updateLocalStorage('ADCOL_');

          }
          else {
            this.toasterService.error("Could not fetch Loan Object from API")
          }
        });
      }
      else {
        this.toasterService.error(res.Message || "Error in Sync");
      }
    });
  }

  /**
   * Helper methods
   */


  public adjustgrid(gridApi) {
    try {
      gridApi.sizeColumnsToFit();
    }
    catch (ex) {
    }
  }

}
