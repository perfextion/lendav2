import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import * as _ from 'lodash'
import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { Loansettings } from '@lenda/models/loansettings';
import { Loan_Key_Visibilty } from '@lenda/models/loansettings';

import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';
import { loan_model, Loan_Collateral, LoanStatus, Collateral_Category_Code } from '@lenda/models/loanmodel';
import CollateralSettings from './../collateral-types.model';
import { Page } from '@lenda/models/page.enum';
import { RefDataModel, RefChevron } from '@lenda/models/ref-data-model';
import { errormodel } from '@lenda/models/commonmodels';

import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { CropapiService } from '@lenda/services/crop/cropapi.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { CollateralService } from '../collateral.service';
import { FsaService } from './fsa.service';
import { PublishService } from "@lenda/services/publish.service";
import { DataService } from '@lenda/services/data.service';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';

import { getNumericCellEditor } from '@lenda/Workers/utility/aggrid/numericboxes';
import { DeleteButtonRenderer } from '@lenda/aggridcolumns/deletebuttoncolumn';
import { SelectEditor } from '@lenda/aggridfilters/selectbox';
import { getAlphaNumericCellEditor } from '@lenda/Workers/utility/aggrid/alphanumericboxes';
import { EmptyEditor } from '@lenda/aggridfilters/emptybox';
import { BlankCellRenderer } from '@lenda/aggridcolumns/tooltip/aggridrenderers';
import { CheckboxCellRenderer } from '@lenda/aggridcolumns/checkbox-cell-editor/checkbox-cell-rendere';

declare function setmodifiedall(array, keyword): any;
declare function setmodifiedsingle(obj, keyword): any;

@Component({
  selector: 'app-fsa',
  templateUrl: './fsa.component.html',
  styleUrls: ['./fsa.component.scss'],
  providers: [FsaService, CollateralService, PublishService]
})
export class FSAComponent implements OnInit, OnDestroy {
  private subscription : ISubscription;
  @Input() expanded: boolean = true;
  public refdata: any = {};
  public columnDefs = [];
  public localloanobject: loan_model;
  public rowData = [];
  public components;
  public context;
  public frameworkcomponents;
  public editType;
  private gridApi;
  private columnApi;
  public deleteAction = false;
  public pinnedBottomRowData;
  public rowClassRules;
  defaultColDef: { menuTabs: any[]; };
  static SELECTED_CAT_CODE: string = '';
  public getRowStyle;
  public collateralChevron: RefChevron = <RefChevron>{};
  private currenterrors: any[];
  public columnsDisplaySettings: ColumnsDisplaySettings = new ColumnsDisplaySettings();

  public title: string;

  public syncRequired: boolean = false;
  private preferencesChangeSub: ISubscription;

  constructor(
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public cropunitservice: CropapiService,
    public logging: LoggingService,
    public alertify: AlertifyService,
    public loanapi: LoanApiService,
    public collateralService: CollateralService,
    public fsaService: FsaService,
    private validationService: ValidationService,
    private settingsService: SettingsService,
    private dataService : DataService,
    private dialogRef: MatDialogRef<FSAComponent>
  ) {
    this.components = {
      blankcell: BlankCellRenderer,
      numericCellEditor: getNumericCellEditor(),
      alphaNumeric: getAlphaNumericCellEditor() ,
      emptyeditor: EmptyEditor
    };

    this.frameworkcomponents = {
      selectEditor: SelectEditor,
      deletecolumn: DeleteButtonRenderer,
      checkboxCellRenderer: CheckboxCellRenderer
    };

    this.context = { componentParent: this };

    this.defaultColDef = {
      menuTabs: []
    };
  }

  ngOnInit() {
    this.subscribeToChanges();
    this.getUserPreferences();
    this.syncRequired = false;

    this.rowData = this.collateralService.getRowData(this.localloanobject, FSAComponent.SELECTED_CAT_CODE, CollateralSettings.fsa.source,'');
    this.fsaService.computeTotal(this.localloanobject);

    if(this.refdata){
      this.collateralChevron = (this.refdata as RefDataModel).RefChevrons.find(c => c.Chevron_ID == 38);
    } else {
      this.collateralChevron = <RefChevron>{};
    }
  }

  getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;
    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe((preferences) => {
      this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
      this.hideUnhideLoanKeys();
      this.columnDefs = this.fsaService.getColumnDefs(this.localloanobject, FSAComponent.SELECTED_CAT_CODE);
      this.columnDefs[2]['hide'] = FSAComponent.SELECTED_CAT_CODE == Collateral_Category_Code.StoredCrop ? !this.columnsDisplaySettings.cropType : true;
    });
    this.hideUnhideLoanKeys();
    this.columnDefs = this.fsaService.getColumnDefs(this.localloanobject, FSAComponent.SELECTED_CAT_CODE);
    this.columnDefs[2]['hide'] = FSAComponent.SELECTED_CAT_CODE == Collateral_Category_Code.StoredCrop ? !this.columnsDisplaySettings.cropType : true;
  }

  //Hide Columns Based ON Loan Settings
  private LoankeySettings:Loan_Key_Visibilty;
  hideUnhideLoanKeys() {
    let LoanSettings: Loansettings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
    if (LoanSettings != undefined || LoanSettings != null) {
      if (LoanSettings.Loan_key_Settings != undefined || LoanSettings.Loan_key_Settings != null) {
        this.LoankeySettings=LoanSettings.Loan_key_Settings
      }
      else
      {
        this.LoankeySettings=new Loan_Key_Visibilty();
      }
    }
    else
    {
      this.LoankeySettings=new Loan_Key_Visibilty();
    }
    this.columnsDisplaySettings.cropType=this.LoankeySettings.Crop_Type==null?this.columnsDisplaySettings.cropType:this.LoankeySettings.Crop_Type;
  }

  //An Array of properties that Represent Keys in the same table
  public Loankeys = ["Crop_Type"]
  displayColumnsChanged($event) {

    if (this.columnApi != undefined && $event!=undefined) {

      //first One to save to Loan Settings
      let LoanSettings: Loansettings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
      if (LoanSettings == undefined || LoanSettings == null) {
        LoanSettings = new Loansettings();
      }
      if (LoanSettings.Loan_key_Settings == undefined || LoanSettings.Loan_key_Settings == null) {
        LoanSettings.Loan_key_Settings = new Loan_Key_Visibilty();
      }
      //Since we dont have column thats changed so lets see manually
      this.Loankeys.forEach(key => {

        let column = this.columnApi.getColumn(key);
        LoanSettings.Loan_key_Settings[key] = column.visible;
      });
      this.localloanobject.LoanMaster.Loan_Settings = JSON.stringify(LoanSettings);
      this.dataService.setLoanObjectwithoutsubscription(this.localloanobject);
    }
  }

  ngAfterViewInit() {
    setTimeout(()=>{
      setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), this.rowId);
      this.setFsaValidations();
      this.getCurrentErrors();
    },100);
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  subscribeToChanges() {
    this.subscription = this.dataService.getLoanObject().subscribe(res=>{
      this.localloanobject=res;

      this.rowData = this.collateralService.getRowData(res, FSAComponent.SELECTED_CAT_CODE, CollateralSettings.fsa.source,'');
    setTimeout(()=>{
      setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), this.rowId);
      this.setFsaValidations();
      this.getCurrentErrors();
    },5);
    });
  }


  onGridSizeChanged(Event: any) {
    this.adjustgrid();
  }

  private adjustgrid() {
    try {
      this.columnApi.columnController.autoSizeAllColumns("contextMenu")
    }
    catch (ex) {
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;

    this.adjustgrid();
    this.setFsaValidations();
    this.getCurrentErrors();
    this.hideBuyer();
  }

  public gridOptions = {
    getRowNodeId: (data) => {

      return this.rowId + data.Collateral_ID;
    }
  }

  hideBuyer() {
    let hide  = FSAComponent.SELECTED_CAT_CODE === Collateral_Category_Code.StoredCrop;
    this.columnApi.setColumnsVisible(["Assoc_ID", "Contract", "Contract_Type_Code", "Qty", "Price"], hide);
  }

  //Grid Events
  addrow() {
    if(this.isLoanEditable) {
      this.fsaService.addRow(this.localloanobject, this.gridApi, this.rowData, CollateralSettings.fsa.source, CollateralSettings.fsa.sourceKey, FSAComponent.SELECTED_CAT_CODE);
      this.setFsaValidations();
      this.getCurrentErrors();
    }
  }

  cellValueChanged(value: any){

    //If no value change dont record it.
    if(value && value.oldValue == value.value) return;

    if(!value && !value.column && !value.column.colId) return;

    this.syncRequired = true;

    let modifiedvalues = this.localstorageservice.retrieve(environment.modifiedbase) as Array<String>;

    if (!modifiedvalues.includes(this.rowId + value.data.Collateral_ID + "_" + value.colDef.field)) {
      modifiedvalues.push(this.rowId + value.data.Collateral_ID + "_" + value.colDef.field);
      this.localstorageservice.store(environment.modifiedbase, modifiedvalues); +
        setmodifiedsingle(this.rowId + value.data.Collateral_ID + "_" + value.colDef.field, this.rowId);
    }

    if(value.column.colId === "Crop_Detail"){
      //Update Price Based on Crop Detail and only if Crop detail is updated else retain
      if(value.newValue !== value.oldValue){
        let cropPrice = this.fsaService.CROP_LIST.find(crop => crop.Crop_Code == value.data.Crop_Detail);
        value.data.Price = cropPrice && cropPrice.Crop_Price >= 0 ? cropPrice.Crop_Price : null;
      }
    }

    if(value.column.colId === "Measure_Code" || value.column.colId == 'Contract_Type_Code') {
      let obj: Loan_Collateral = value.data;
      this.fsaService.Assign_Disc_Percent(obj);
    }

    const eventDelegations = value.column.colDef.eventDelegations;
    if (eventDelegations && eventDelegations.validations) {
      this.setFsaValidations();
    }
    this.getCurrentErrors();
    this.fsaService.rowValueChanged(value, this.localloanobject,"AdditionalCollateral", CollateralSettings.fsa.source, CollateralSettings.fsa.pk);
  }

  DeleteClicked(rowIndex: any) {
    this.collateralService.deleteClicked(
      rowIndex,
      this.localloanobject,
      this.rowData,
      CollateralSettings.fsa.source,
      CollateralSettings.fsa.pk,
      () => {
        this.syncRequired = true;
      }
    );
    this.setFsaValidations();
    this.getCurrentErrors();
  }

  /**
   * Method to close the NgbActiveModal: FSACompoenet.
   */
  closeModal(){
    // this.activeModal.dismiss();
    this.dialogRef.close();
  }

  get isLoanEditable(){
    return !this.localloanobject || this.localloanobject.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  get tableId() {
    return 'Table_' + this.collateralChevron.Chevron_Code;
  }

  get rowId(){
    return this.collateralChevron.Chevron_Code + '_';
  }

  private setFsaValidations(){
    this.validationService.validateTableFields<Loan_Collateral>(
      this.rowData,
      this.columnDefs,
      Page.collateral,
      this.rowId,
      FSAComponent.SELECTED_CAT_CODE,
      'Collateral_ID'
    );
  }

  private getCurrentErrors(){
    this.currenterrors = (this.localstorageservice.retrieve(environment.errorbase) as Array<errormodel>).filter(
      p => p.chevron == FSAComponent.SELECTED_CAT_CODE
    );

    this.validationService.highlighErrorCells(this.currenterrors, this.tableId);
  }

  MethodFromCheckbox(params: any, type: string, $event: any) {
    if(type == 'Insured_Flag') {
      if ($event.srcElement.checked) {
        params.data['Insured_Flag'] = 1;
      } else {
        params.data['Insured_Flag'] = 0;
      }

      params.api.refreshCells({
        columns: ['Insured_Flag']
      });
    }

    if(type == 'Income_Ind') {
      if ($event.srcElement.checked) {
        params.data['Income_Ind'] = 1;
      } else {
        params.data['Income_Ind'] = 0;
      }

      params.api.refreshCells({
        columns: ['Income_Ind']
      });
    }

    if (params.data.ActionStatus != 1) {
      params.data.ActionStatus = 2;
    }

    this.syncRequired = true;

    this.localloanobject.srccomponentedit = "AdditionalCollateral";
    this.localloanobject.lasteditrowindex = params.rowIndex;

    this.loanserviceworker.performcalculationonloanobject(this.localloanobject, true);

    const eventDelegations = params.column.colDef.eventDelegations;
    if (eventDelegations && eventDelegations.validations) {
      this.setFsaValidations();
    }
  }

  onOkClick() {
    if(this.isLoanEditable) {
      this.dialogRef.close(true);
    }
  }

  onCancelClick() {
    this.dialogRef.close(false);
  }
}
