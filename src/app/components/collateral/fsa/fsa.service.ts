import { Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { Loan_Collateral, loan_model, Collateral_Category_Code } from '@lenda/models/loanmodel';
import { RefDataModel, RefMeasurementType, RefCollateralMktDisc, RefCollateralCategory } from '@lenda/models/ref-data-model';

import {
  currencyFormatter,
  calculatedNumberFormatter
} from '@lenda/aggridformatters/valueformatters';
import {
  isgrideditable,
  cellclassmaker,
  CellType,
  headerclassmaker,
  IParam
} from '@lenda/aggriddefinations/aggridoptions';
import { getNumberEmptyDefault, numberValueSetter } from '@lenda/Workers/utility/aggrid/numericboxes';
import {
  BorrowerAndContractValidation,
  LienValidation
} from '@lenda/Workers/utility/validation-functions';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';

import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';

import { FSAComponent } from './fsa.component';

type CParams = IParam<FSAComponent, Loan_Collateral>;

/**
 * Shared service for FSA
 */
@Injectable()
export class FsaService {
  /**List of Stored Crop. */
  public readonly CROP_LIST = this.getCropList();

  public localloanobject: loan_model;
  private refdata: RefDataModel;
  private components;

  private measureItems: RefMeasurementType[] = [];
  private discountitems: RefCollateralMktDisc[] = [];
  private collateralCategory: RefCollateralCategory[] = [];
  private collateralInsDisc: any = [];

  constructor(
    public loanserviceworker: LoancalculationWorker,
    public localst: LocalStorageService
  ) {
    this.localloanobject = this.localst.retrieve(environment.loankey);
    this.refdata = this.localst.retrieve(environment.referencedatakey);

    if (this.refdata) {
      this.measureItems = this.refdata.MeasurementType || [];
      this.discountitems = this.refdata.CollateralMktDisc || [];
      this.collateralCategory = this.refdata.CollateralCategory || [];
      this.collateralInsDisc = this.refdata.CollateralInsDisc || [];
    }

    this.components = {
      numberEmptyDefault: getNumberEmptyDefault()
    };
  }

  public getColumnDefs(loanobj: loan_model, categoryCode: string) {
    this.localloanobject = loanobj;
    let valuesToReturn = [
      {
        headerName: 'Category',
        field: 'Collateral_Category_Code',
        editable: false,
        valueFormatter: params => {
          let contractValues: any[] = this.getCollateralCategory().values;
          if (params.value) {
            let selectedValue = contractValues.find(
              data => data.key == params.value
            );
            return selectedValue ? selectedValue.value : '';
          } else {
            return '';
          }
        },
        cellEditorSelector: function(params) {
          return {
            component: 'selectEditor'
          };
        },
        cellEditorParams: this.getCollateralCategory.bind(this),
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Crop',
        field: 'Crop_Detail',
        editable: function(params) {
          let val = false;
          if (params.data.Collateral_Category_Code == Collateral_Category_Code.StoredCrop) {
            val = true;
          } else val = false;

          return isgrideditable(val);
        },
        cellEditorSelector: function(params) {
          if (params.data.Collateral_Category_Code == Collateral_Category_Code.StoredCrop) {
            return {
              component: 'selectEditor'
            };
          } else {
            return null;
          }
        },
        cellClass: function(params) {
          if (!(params.data.Collateral_Category_Code == Collateral_Category_Code.StoredCrop)) {
            return 'grayedcell';
          } else {
            return cellclassmaker(CellType.Text, true);
          }
        },
        valueFormatter: params => {
          let cropValues: any[] = this.cropListSelection().values;

          if (params.value) {
            let selectedValue = cropValues.find(
              data => data.key == params.value
            );
            return selectedValue ? selectedValue.value : '';
          } else {
            return '';
          }
        },
        cellEditorParams: this.cropListSelection(),
        width: 90,
        minWidth: 90,
        maxWidth: 90,
        hide: categoryCode != Collateral_Category_Code.StoredCrop,
        suppressToolPanel: categoryCode != Collateral_Category_Code.StoredCrop
      },

      {
        headerName: 'Crop Type',
        field: 'Crop_Type',
        editable: function(params) {
          let val = false;
          if (params.data.Collateral_Category_Code == Collateral_Category_Code.StoredCrop) {
            val = true;
          } else val = false;

          return isgrideditable(val);
        },
        cellEditorSelector: function(params) {
          if (params.data.Collateral_Category_Code == Collateral_Category_Code.StoredCrop) {
            return {
              component: 'alphaNumeric'
            };
          } else {
            return null;
          }
        },
        cellClass: function(params) {
          if (!(params.data.Collateral_Category_Code == Collateral_Category_Code.StoredCrop)) {
            return 'grayedcell';
          } else {
            return cellclassmaker(CellType.Text, true);
          }
        },
        width: 90,
        minWidth: 90,
        maxWidth: 90,
        hide: categoryCode != Collateral_Category_Code.StoredCrop,
        suppressToolPanel: categoryCode != Collateral_Category_Code.StoredCrop
      },

      {
        headerName: 'Description',
        field: 'Collateral_Description',
        editable: isgrideditable(true),
        cellEditor: 'alphaNumeric',
        cellClass: isgrideditable(true) ? ['editable-color'] : [],
        width: 180,
        minWidth: 180,
        maxWidth: 180
      },
      {
        headerName: 'Meas Code',
        field: 'Measure_Code',
        editable: isgrideditable(true),
        cellClass: isgrideditable(true) ? ['editable-color'] : [],
        cellEditor: 'selectEditor',
        cellEditorParams: params => this.getMeasureItems(params.data.Collateral_Category_Code),
        valueFormatter: params => {
          if(!params.data.Measurement_Type_Name) {
            let measureItem = this.refdata.MeasurementType.find(a => a.Measurement_Type_Code == params.data.Measure_Code);
            if(measureItem) {
              params.data.Measurement_Type_Name = measureItem.Measurement_Type_Name;
            }
          }
          return params.data.Measurement_Type_Name;
        },
        valueSetter: params => {
          let measureItem = this.refdata.MeasurementType.find(a => a.Measurement_Type_Code == params.newValue);
          if(measureItem) {
            params.data.Measure_Code = measureItem.Measurement_Type_Code;
            params.data.Measurement_Type_Name = measureItem.Measurement_Type_Name;
            params.data.Exception_Ind = measureItem.Exception_Ind;
          } else {
            params.data.Measure_Code = '';
            params.data.Measurement_Type_Name = '';
            params.data.Exception_Ind = '';
          }
          return true;
        },
        width: 180,
        minWidth: 180,
        maxWidth: 180
      },
      {
        headerName: 'Buyer',
        field: 'Assoc_ID',
        cellClass: cellclassmaker(CellType.Text, true),
        editable: isgrideditable(true),
        cellEditor: 'selectEditor',
        hide: true,
        cellEditorParams: this.getBuyersValue.bind(this),
        valueFormatter: params => {
          let cropValues: any[] = this.getBuyersValue(params).values;
          if (params.value) {
            let selectedValue = cropValues.find(
              data => data.key == params.value
            );
            return selectedValue ? selectedValue.value : '';
          } else {
            return '';
          }
        },
        validations: params => {
          return BorrowerAndContractValidation(params, 'Assoc_ID');
        },
        eventDelegations: { validations: true },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Contract ID',
        field: 'Contract',
        hide: true,
        cellClass: cellclassmaker(CellType.Text, true),
        editable: isgrideditable(true),
        validations: params => {
          return BorrowerAndContractValidation(params, 'Contract');
        },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Contract Type',
        field: 'Contract_Type_Code',
        hide: true,
        editable: isgrideditable(true),
        cellClass: cellclassmaker(CellType.Text, true),
        cellEditor: 'selectEditor',
        cellEditorParams: this.getContractTypeValue.bind(this),
        valueFormatter: params => {
          let contractValues: any[] = this.getContractTypeValue().values;
          if (params.value) {
            let selectedValue = contractValues.find(
              data => data.key == params.value
            );
            return selectedValue ? selectedValue.value : '';
          } else {
            return '';
          }
        },
        validations: params => {
          return BorrowerAndContractValidation(params, 'Contract_Type_Code');
        },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Contract Qty',
        field: 'Qty',
        hide: true,
        editable: isgrideditable(true),
        cellEditor: this.components.numberEmptyDefault,
        cellClass: cellclassmaker(CellType.Integer, true),
        valueFormatter: function(params) {
          return params.value && !isNaN(params.value)
            ? calculatedNumberFormatter(params, 0)
            : '';
        },
        valueSetter: numberValueSetter,
        validations: params => {
          return BorrowerAndContractValidation(params, 'Qty');
        },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Contract Price',
        field: 'Price',
        hide: true,
        editable: isgrideditable(true),
        valueFormatter: function(params) {
          return params.value && !isNaN(params.value)
            ? currencyFormatter(params, 4)
            : '';
        },
        cellEditor: this.components.numberEmptyDefault,
        validations: params => {
          return BorrowerAndContractValidation(params, 'Price');
        },
        cellClass: cellclassmaker(CellType.Integer, true),
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Pre-Lien Value',
        field: 'Market_Value',
        headerClass: headerclassmaker(CellType.Integer),
        cellEditor: 'numericCellEditor',
        valueFormatter: currencyFormatter,
        valueParser: function numberParser(params) {
          return Number(params.newValue);
        },
        valueSetter: numberValueSetter,
        cellClass: function(params) {
          if (!(params.data.Collateral_Category_Code != Collateral_Category_Code.StoredCrop)) {
            return 'grayedcell rightaligned';
          } else {
            return cellclassmaker(CellType.Integer, true);
          }
        },
        editable: function(params) {
          let val = false;
          if (params.data.Collateral_Category_Code != Collateral_Category_Code.StoredCrop) {
            val = true;
          } else val = false;

          return isgrideditable(val);
        },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Lienholder',
        field: 'Lien_Holder',
        editable: isgrideditable(true),
        cellClass: cellclassmaker(CellType.Text, true),
        cellEditor: 'selectEditor',
        cellEditorParams: params => {
          return this.getLienholders();
        },
        validations: params => {
          return LienValidation(params, 'Lien_Holder');
        },
        eventDelegations: { validations: true },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Lien Amount',
        field: 'Prior_Lien_Amount',
        editable: isgrideditable(true),
        // cellEditor: "numericCellEditor",
        valueFormatter: function(params) {
          return params.value && !isNaN(params.value)
            ? currencyFormatter(params, 0)
            : '';
        },
        valueSetter: numberValueSetter,
        validations: params => {
          return LienValidation(params, 'Prior_Lien_Amount');
        },
        eventDelegations: { validations: true },
        cellClass: cellclassmaker(CellType.Integer, true),
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Income',
        field: 'Income_Ind',
        minWidth: 90, width: 90, maxWidth: 90,
        cellClass: params => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Text, true, '', status);
        },
        cellRenderer: 'checkboxCellRenderer',
        cellRendererParams: (params: CParams) => {
          return {
            type: 'Income_Ind',
            edit: params.context.componentParent.localloanobject.LoanMaster.Loan_Status == 'W'
          }
        }
      },
      {
        headerName: 'Mkt Value',
        field: 'Net_Market_Value',
        headerClass: headerclassmaker(CellType.Integer),
        cellEditor: 'numericCellEditor',
        valueFormatter: currencyFormatter,
        valueParser: function numberParser(params) {
          return Number(params.newValue);
        },
        cellClass: cellclassmaker(CellType.Integer, false),
        editable: isgrideditable(false),
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Disc Mkt Value',
        field: 'Disc_Value',
        editable: false,
        cellEditor: 'numericCellEditor',
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: cellclassmaker(CellType.Integer, false),
        valueFormatter: currencyFormatter,
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Insured',
        field: 'Insured_Flag',
        cellClass: params => {
          let val = cellclassmaker(CellType.Text, true);

          let found = this.collateralCategory.find(
            cc =>
              cc.Collateral_Category_Code ==
              params.data.Collateral_Category_Code
          );
          if (found) {
            val = cellclassmaker(CellType.Text, found.Protect_Insured_Ind != '1');
          }
          return val;
        },
        cellRenderer: 'checkboxCellRenderer',
        cellRendererParams: params => {
          let val = false;

          let found = this.collateralCategory.find(
            cc =>
              cc.Collateral_Category_Code ==
              params.data.Collateral_Category_Code
          );
          if (found) {
            val = found.Protect_Insured_Ind == '1' ? false : true;
          };

          return {
            type: 'Insured_Flag',
            edit: isgrideditable(val)
          }
        },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Ins Value',
        field: 'Ins_Value',
        editable: false,
        valueFormatter: currencyFormatter,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: cellclassmaker(CellType.Integer, false),
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Disc Ins Value',
        field: 'Disc_Ins_Value',
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: cellclassmaker(CellType.Integer, false),
        editable: false,
        valueFormatter: currencyFormatter,
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: '',
        field: 'value',
        cellRenderer: 'deletecolumn',
        cellClass: isgrideditable(true) ? [''] : ['disable'],
        pinnedRowCellRenderer: function() {
          return ' ';
        },
        width: 60,
        minWidth: 60,
        maxWidth: 60,
        suppressToolPanel: true
      }
    ];

    // if(categoryCode === Collateral_Category_Code.StoredCrop){
    //   valuesToReturn = this.getColumnDefsCont(valuesToReturn);
    // }

    return valuesToReturn;
  }

  public computeTotal(loanobject) {
    let total = [];
    try {
      let footer = new Loan_Collateral();
      footer.Collateral_Category_Code = 'Total';
      footer.Market_Value = loanobject.LoanMaster.FC_Market_Value_FSA;
      footer.Prior_Lien_Amount = loanobject.LoanMaster.FC_FSA_Prior_Lien_Amount;
      footer.Lien_Holder = '';
      footer.Net_Market_Value = loanobject.LoanMaster.Net_Market_Value_FSA;
      footer.Disc_Pct = 0;
      footer.Disc_Mkt_Value = loanobject.LoanMaster.Disc_value_FSA;
      total.push(footer);
      return total;
    } catch {
      // Means that Calculations have not Ended
      return total;
    }
  }

  //add new row

  //this.collateralService.addRow(this.localloanobject, this.gridApi, this.rowData, CollateralSettings.fsa.key, CollateralSettings.fsa.source, CollateralSettings.fsa.sourceKey);

  public addRow(
    localloanobject: loan_model,
    gridApi,
    rowData,
    source,
    sourceKey,
    collateralCatCode?: string
  ) {
    let newItem: Loan_Collateral;

    if (localloanobject[source] == null) {
      localloanobject[source] = [];
    }

    let collateralDefaults = this.collateralCategory.find(
      crop => crop.Collateral_Category_Code == collateralCatCode
    );

    newItem = new Loan_Collateral();
    newItem.Disc_Pct = 0;
    newItem.ActionStatus = 1;
    newItem.Collateral_ID = getRandomInt(Math.pow(10, 4), Math.pow(10, 6));
    newItem.Collateral_Category_Code = collateralCatCode;
    newItem.Loan_Full_ID = localloanobject.Loan_Full_ID;
    newItem.Loan_ID = localloanobject.LoanMaster.Loan_ID;
    newItem.Loan_Seq_Num = localloanobject.LoanMaster.Loan_Seq_num;
    newItem.Loan_Other_Income_ID = 0;
    newItem.Market_Value = 0;
    newItem.Income_Ind = 1;
    newItem.Qty = 0;
    newItem.Price = 0;
    newItem.Status = 1;

    let mktItem = this.refdata.CollateralMktDisc.find(mkt => mkt.Collateral_Category_Code == collateralCatCode);
    newItem.Disc_Pct = mktItem && mktItem.Disc_Percent ? mktItem.Disc_Percent : 0;

    if (collateralDefaults) {
      if(collateralDefaults.Default_Insured_Ind == '0') {
        newItem.Insured_Flag = 0;
      } else if (collateralDefaults.Default_Insured_Ind == '1') {
        newItem.Insured_Flag = 1;
      }
    } else {
      newItem.Insured_Flag = null;
    }

    newItem.Prior_Lien_Amount = 0;
    newItem.Assoc_ID = null;
    newItem.Contract_Type_Code = null;

    rowData.push(newItem);
    localloanobject[source].push(newItem);

    gridApi.setRowData(rowData);
    gridApi.startEditingCell({
      rowIndex: rowData.length - 1,
      colKey: sourceKey
    });
    this.loanserviceworker.performcalculationonloanobject(localloanobject);
  }

  public rowValueChanged(
    value: any,
    localloanobject: loan_model,
    component,
    source,
    uniqueKey
  ) {
    let obj: Loan_Collateral = value.data;
    if (obj[uniqueKey] == 0) {
      let lastIndex = localloanobject[source].length - 1;
      obj.ActionStatus = 1;
      localloanobject[source][lastIndex] = value.data;
    } else {
      let rowindex = localloanobject[source].findIndex(
        lc => lc[uniqueKey] == obj[uniqueKey]
      );
      if (obj.ActionStatus != 1) obj.ActionStatus = 2;
      localloanobject[source][rowindex] = obj;
    }

    if(obj.Income_Ind == 1) {
      this.UpdateOtherIncomes(obj, localloanobject, value.column.colId);
    }

    // this shall have the last edit
    localloanobject.srccomponentedit = component;
    localloanobject.lasteditrowindex = value.rowIndex;
    this.loanserviceworker.performcalculationonloanobject(localloanobject);
  }

  private UpdateOtherIncomes(obj: Loan_Collateral, localloanobject: loan_model, column: string) {
    let other_incomes = localloanobject.LoanOtherIncomes.filter(a => a.ActionStatus != 3 && (a.Loan_Collateral_ID == obj.Collateral_ID || a.Loan_Other_Income_ID == obj.Loan_Other_Income_ID));
    other_incomes.forEach(other_income => {
      if(obj.Collateral_Category_Code == Collateral_Category_Code.StoredCrop) {
        obj.Market_Value = parseFloat(obj.Price as any) * parseFloat(obj.Qty as any);
        other_income.Crop_Code = obj.Crop_Detail;
      }
      obj.Net_Market_Value = parseFloat(Math.max((Number(obj.Market_Value) - Number(obj.Prior_Lien_Amount)), 0).toFixed(2));
      other_income.Amount = obj.Net_Market_Value;
      other_income.Other_Description_Text = obj.Collateral_Description;

      if(other_income.ActionStatus != 1) {
        other_income.ActionStatus = 2;
      }
    });
  }

  /**
   * Method to get crop list from local storage.
   */
  private getCropList() {
    return this.localst.retrieve(environment.referencedatakey).Crops;
  }

  private cropListSelection() {
    let tempHolderIDs = [];

    this.CROP_LIST.map(cp => {
      tempHolderIDs.push(cp.Crop_Code);
    });

    tempHolderIDs = _.uniq(tempHolderIDs);

    let cropValues = _.uniqBy(this.mapCropValues(tempHolderIDs), 'key');
    return { values: cropValues };
  }

  private mapCropValues(cropIDs: Array<number>): Array<Object> {
    if (!this.CROP_LIST) return [];

    let cropValues = [];

    if (cropIDs && cropIDs.length > 0) {
      cropIDs.map(cpi => {
        this.CROP_LIST.map(cl => {
          if (cl.Crop_Code == cpi) {
            cropValues.push({ key: cl.Crop_Code, value: cl.Crop_Name });
          }
        });
      });
    } else {
      this.CROP_LIST.map(cl => {
        cropValues.push({ key: cl.Crop_Code, value: cl.Crop_Name });
      });
    }

    return cropValues;
  }

  private getBuyersValue(params) {
    let buyersValue = [];
    if (
      this.localloanobject.Association &&
      this.localloanobject.Association.length > 0
    ) {
      this.localloanobject.Association.filter(
        as => as.Assoc_Type_Code === 'BUY'
      ).map(buyer => {
        buyersValue.push({ key: buyer.Assoc_ID, value: buyer.Assoc_Name });
      });
      buyersValue = _.uniqBy(buyersValue, 'key');
      return { values: buyersValue };
    } else {
      return { values: [] };
    }
  }

  private getLienholders() {
    let values = [{ value: '', key: '' }];
    this.localloanobject.Association.filter(p => p.Assoc_Type_Code == 'LEI' && p.ActionStatus != 3).forEach(
      element => {
        values.push({ value: element.Assoc_Name, key: element.Assoc_Name });
      }
    );
    return { values: values };
  }

  private getCollateralCategory() {
    let collaterals = [];
    if (this.collateralCategory) {
      this.collateralCategory.map(cc => {
        collaterals.push({
          key: cc.Collateral_Category_Code,
          value: cc.Collateral_Category_Name
        });
      });
      collaterals = _.uniqBy(collaterals, 'key');
    }
    return collaterals && collaterals.length > 0
      ? { values: collaterals }
      : { values: [] };
  }

  private getMeasureItems(Collateral_Category_Code: string) {
    let measures = [];
    if (this.measureItems) {
      measures.push({ key: '', value: '' });
      this.measureItems.filter(a => a.Collateral_Category == Collateral_Category_Code).map(cc => {
        measures.push({
          key: cc.Measurement_Type_Code,
          value: cc.Measurement_Type_Name
        });
      });
      measures = _.uniqBy(measures, 'key');
    }
    return measures && measures.length > 0
      ? { values: measures }
      : { values: [] };
  }

  private getContractTypeValue() {
    let contractValue = [];

    this.refdata.ContractType.map(ct => {
      contractValue.push({
        key: ct.Contract_Type_Code,
        value: ct.Contract_Type_Name
      });
    });

    contractValue = _.uniqBy(contractValue, 'key');
    return contractValue && contractValue.length > 0
      ? { values: contractValue }
      : { values: [] };
  }

  public Assign_Disc_Percent(params: Loan_Collateral) {
    //Check PriorLien Indicator to See if will Include Amount in Calculation
    let collateral = this.collateralCategory.find(col => col.Collateral_Category_Code == params.Collateral_Category_Code);
    let disc_percent = 0;

    //Get Measure Discount
    if(params.Measure_Code) {
      let measItem = this.measureItems.find(mea => mea.Measurement_Type_Code == params.Measure_Code);
      disc_percent = measItem && measItem.Disc_Percent ? measItem.Disc_Percent : null;

      if(params.Contract_Type_Code) {
        let contractItem =  this.refdata.ContractType.find(item => item.Contract_Type_Code == params.Contract_Type_Code);
        disc_percent = contractItem && contractItem.Disc_Adj ? Number(disc_percent) + Number(contractItem.Disc_Adj) : Number(disc_percent);
      }
    } else {
      let mktItem = this.refdata.CollateralMktDisc.find(mkt => mkt.Collateral_Category_Code == params.Collateral_Category_Code);
      disc_percent = mktItem && mktItem.Disc_Percent ? mktItem.Disc_Percent : null;
    }

    //Get Disc % to Use : 0 as default
    disc_percent = disc_percent ? disc_percent : 100;

    if(collateral){
      disc_percent = collateral.Prior_Lien_Max_Disc_Ind == '1' && params.Prior_Lien_Amount && params.Prior_Lien_Amount > 0 ? 100 : disc_percent;
    }

    params.Disc_Pct = disc_percent;
  }
}
