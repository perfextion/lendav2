import { Injectable, OnInit, OnDestroy } from '@angular/core';

import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment';
import { loan_model } from '@lenda/models/loanmodel';
import { errormodel } from '@lenda/models/commonmodels';
import { RefChevron, RefDataModel } from '@lenda/models/ref-data-model';

import { numberValueSetter, priceValueSetter } from '@lenda/Workers/utility/aggrid/numericboxes';
import { getNumberEmptyDefault } from '@lenda/Workers/utility/aggrid/numericboxes';
import {
  calculatedNumberFormatter,
  currencyFormatter,
  percentageFormatter
} from '@lenda/aggridformatters/valueformatters';
import {
  isgrideditable,
  cellclassmaker,
  CellType,
  headerclassmaker
} from '@lenda/aggriddefinations/aggridoptions';
import {
  RequiredValidation,
  MarketingContractValidation
} from '@lenda/Workers/utility/validation-functions';

import { MarketingcontractcalculationService } from '@lenda/Workers/calculations/marketingcontractcalculation.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { DataService } from '@lenda/services/data.service';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';

/**
 * Shared service for Marketing Contracts
 */
@Injectable()
export class MarketingContractsService implements OnInit, OnDestroy {
  public localloanobject: loan_model;
  private refdata: RefDataModel;
  private components;
  private subscription: ISubscription;

  constructor(
    public localStorageService: LocalStorageService,
    public marketingCalculationService: MarketingcontractcalculationService,
    public loanserviceworker: LoancalculationWorker,
    private validationService: ValidationService,
    private dataService: DataService
  ) {
    this.localloanobject = this.localStorageService.retrieve(
      environment.loankey
    );

    this.refdata = this.localStorageService.retrieve(
      environment.referencedatakey
    );

    this.components = {
      numberEmptyDefault: getNumberEmptyDefault()
    };

    if (this.refdata) {
      this.mktContractsChevron = (this
        .refdata as RefDataModel).RefChevrons.find(c => c.Chevron_ID == 40);
    } else {
      this.mktContractsChevron = <RefChevron>{};
    }
  }

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localloanobject = res;
      this.refdata = this.localStorageService.retrieve(
        environment.referencedatakey
      );
    });
  }

  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getColumnDefs() {
    return [
      {
        headerName: 'Crop',
        field: 'Crop_Code',
        cellClass: cellclassmaker(CellType.Text, true),
        editable: isgrideditable(true),
        cellEditor: 'selectEditor',
        cellEditorParams: this.getCropValues.bind(this),
        valueFormatter: params => {
          let crop = this.refdata.CropList.find(
            a => a.Crop_Code == params.value
          );

          if (crop) {
            return crop.Crop_Name;
          } else {
            return '';
          }
        },
        valueSetter: params => {
          let Crop_And_Practice_ID = parseFloat(params.newValue);
          let crop = this.refdata.CropList.find(
            a => a.Crop_And_Practice_ID == Crop_And_Practice_ID
          );

          if (crop) {
            params.data.Crop_Code = crop.Crop_Code;
            params.data.Crop_Type_Code = crop.Crop_Type_Code;
            params.data.UoM = crop.MPCI_UOM;
          }

          return true;
        },
        validations: { Crop_Code: RequiredValidation },
        eventDelegations: { validations: true },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Crop Type',
        field: 'Crop_Type_Code',
        editable: false,
        cellClass: cellclassmaker(CellType.Text, false),
        width: 90,
        minWidth: 90,
        maxWidth: 90,
        visible: true
      },
      {
        headerName: 'Buyer',
        field: 'Assoc_ID',
        cellClass: cellclassmaker(CellType.Text, true),
        editable: isgrideditable(true),
        cellEditor: 'selectEditor',
        cellEditorParams: this.getBuyersValue.bind(this),
        valueFormatter: params => {
          let cropValues: any[] = this.getBuyersValue().values;
          if (params.value) {
            let selectedValue = cropValues.find(
              data => data.key == params.value
            );
            return selectedValue ? selectedValue.value : '';
          } else {
            return '';
          }
        },
        validations: params => {
          return MarketingContractValidation(params, 'Assoc_ID');
        },
        eventDelegations: { validations: true },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Contract ID',
        field: 'Contract',
        cellClass: cellclassmaker(CellType.Text, true),
        editable: isgrideditable(true),
        validations: params => {
          return MarketingContractValidation(params, 'Contract');
        },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Contract Type',
        field: 'Contract_Type_Code',suppressSorting: true,sortable:false,
        editable: isgrideditable(true),
        cellClass: cellclassmaker(CellType.Text, true),
        cellEditor: 'selectEditor',
        cellEditorParams: this.getContractTypeValue.bind(this),
        valueFormatter: params => {
          let contractValues: any[] = this.getContractTypeValue().values;
          if (params.value) {
            let selectedValue = contractValues.find(
              data => data.key == params.value
            );
            return selectedValue ? selectedValue.value : '';
          } else {
            return '';
          }
        },
        validations: params => {
          params.refdata = this.refdata.ContractType;
          return MarketingContractValidation(params, 'Contract_Type_Code');
        },
        eventDelegations: { validations: true },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },

      {
        headerName: 'Contract Qty',
        field: 'Quantity',suppressSorting: true,sortable:false,
        headerClass: headerclassmaker(CellType.Integer),
        editable: isgrideditable(true),
        cellEditor: this.components.numberEmptyDefault,
        cellClass: cellclassmaker(CellType.Integer, true),
        valueSetter: numberValueSetter,
        valueFormatter: function(params) {
          return params && params.value > 0
            ? calculatedNumberFormatter(params, 0)
            : '';
        },
        validations: params => {
          return MarketingContractValidation(params, 'Quantity');
        },
        eventDelegations: { validations: true },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },

      {
        headerName: 'Contract Price',
        field: 'Price',suppressSorting: true,sortable:false,
        headerClass: headerclassmaker(CellType.Integer),
        editable: isgrideditable(true),
        cellEditor: this.components.numberEmptyDefault,
        cellClass: cellclassmaker(CellType.Integer, true),
        valueSetter: priceValueSetter,
        valueFormatter: function(params) {
          return params && params.value > 0 ? currencyFormatter(params, 4) : '';
        },
        validations: params => {
          return MarketingContractValidation(params, 'Price');
        },
        eventDelegations: { validations: true },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Contract Value',
        field: 'Market_Value',suppressSorting: true,sortable:false,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: cellclassmaker(CellType.Integer, false),
        // valueFormatter: currencyFormatter,
        valueFormatter: function(params) {
          return params && params.value >= 0
            ? currencyFormatter(params, 0)
            : '';
        },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: 'Contract %',
        field: 'Contract_Per',suppressSorting: true,sortable:false,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: cellclassmaker(CellType.Integer, false),
        // valueFormatter: percentageFormatter,
        valueFormatter: function(params) {
          return params && params.value >= 0
            ? percentageFormatter(params, 1)
            : '';
        },
        width: 90,
        minWidth: 90,
        maxWidth: 90
      },
      {
        headerName: '',suppressSorting: true,sortable:false,
        field: 'value',
        cellRenderer: 'deletecolumn',
        width: 60,
        minWidth: 60,
        suppressToolPanel: true,
        maxWidth: 60
      }
    ];
  }

  rowvaluechanged(value: any, localloanobject: loan_model) {
    this.localloanobject.srccomponentedit = 'MarketingContractsComponent';
    this.localloanobject.lasteditrowindex = value.rowIndex;

    if (value.data.ActionStatus != 1) {
      value.data.ActionStatus = 2;
    }

    this.loanserviceworker.performcalculationonloanobject(localloanobject);
  }

  private getContractTypeValue() {
    let contractValue = [];

    this.refdata.ContractType.map(ct => {
      contractValue.push({
        key: ct.Contract_Type_Code,
        value: ct.Contract_Type_Name
      });
    });

    contractValue = _.uniqBy(contractValue, 'key');

    return contractValue && contractValue.length > 0
      ? { values: contractValue }
      : { values: [] };
  }

  getBuyersValue() {
    let buyersValue = this.getBuyersValueCont();

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localloanobject = res;
      buyersValue = this.getBuyersValueCont();
    });

    return buyersValue;
  }

  private getBuyersValueCont() {
    let buyersValue = [];
    if (
      this.localloanobject.Association &&
      this.localloanobject.Association.length > 0
    ) {
      this.localloanobject.Association.filter(
        as => as.Assoc_Type_Code === 'BUY' && as.ActionStatus != 3
      ).map(buyer => {
        buyersValue.push({ key: buyer.Assoc_ID, value: buyer.Assoc_Name });
      });

      buyersValue = _.uniqBy(buyersValue, 'key');
      return { values: buyersValue };
    } else {
      return { values: [] };
    }
  }

  getCropValues(params) {
    let cropValues = [];

    if (
      this.localloanobject.CropYield &&
      this.localloanobject.CropYield.length > 0
    ) {
      this.localloanobject.CropYield.forEach(cy => {
        let cl = this.refdata.CropList.find(
          a => a.Crop_And_Practice_ID == cy.Crop_ID
        );

        if (cl) {
          cropValues.push({
            key: cl.Crop_And_Practice_ID,
            Crop_Code: cl.Crop_Code,
            Crop_Type_Code: cl.Crop_Type_Code,
            value: `${cl.Crop_Name} | ${cl.Crop_Type_Code}`
          });
        }
      });
    }

    cropValues = _.uniqBy(cropValues, a => a.value);
    return { values: cropValues };
  }

  mktContractsChevron: RefChevron = <RefChevron>{};

  get tableId() {
    return 'Table_' + this.mktContractsChevron.Chevron_Code;
  }

  get rowId() {
    return this.mktContractsChevron.Chevron_Code + '_';
  }

  private currenterrors: Array<errormodel>;

  getcurrenterrors() {
    this.currenterrors = (this.localStorageService.retrieve(
      environment.errorbase
    ) as Array<errormodel>).filter(
      p => p.chevron == this.mktContractsChevron.Chevron_Code
    );

    this.validationService.highlighErrorCells(this.currenterrors, this.tableId);
  }
}
