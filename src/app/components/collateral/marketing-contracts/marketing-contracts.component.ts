import { Component, OnInit, Input, OnDestroy, ViewEncapsulation } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';
import { ISubscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

import { Loansettings } from '@lenda/models/loansettings';
import { Loan_Key_Visibilty } from '@lenda/models/loansettings';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';
import { loan_model, Loan_Marketing_Contract, LoanStatus } from '@lenda/models/loanmodel';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { CropapiService } from '@lenda/services/crop/cropapi.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { getNumericCellEditor} from '@lenda/Workers/utility/aggrid/numericboxes';
import { environment } from '@env/environment.prod';
import { SelectEditor } from '@lenda/aggridfilters/selectbox';
import { DeleteButtonRenderer } from '@lenda/aggridcolumns/deletebuttoncolumn';
import { MarketingcontractcalculationService } from '@lenda/Workers/calculations/marketingcontractcalculation.service';
import { getAlphaNumericCellEditor } from '@lenda/Workers/utility/aggrid/alphanumericboxes';
import { CollateralService } from '../collateral.service';
import { MarketingContractsService } from './marketing-contracts.service';
import CollateralSettings from './../collateral-types.model';
import { PublishService } from "@lenda/services/publish.service";
import { Page } from '@lenda/models/page.enum';
import { calculatecolumnwidths, autoSizeAll } from '@lenda/aggriddefinations/aggridoptions';
import { DataService } from '@lenda/services/data.service';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { errormodel } from '@lenda/models/commonmodels';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { lookupUOM } from '@lenda/Workers/utility/aggrid/cropboxes';

declare function setmodifiedall(array, keyword): any;
declare function setmodifiedsingle(obj, keyword): any;

@Component({
  selector: 'app-marketing-contracts',
  templateUrl: './marketing-contracts.component.html',
  styleUrls: ['./marketing-contracts.component.scss'],
  providers: [CollateralService, MarketingContractsService],
  encapsulation: ViewEncapsulation.None
})
export class MarketingContractsComponent implements OnInit, OnDestroy {
  private subscription : ISubscription;
  @Input() expanded: boolean = true;
  public refdata: any = {};
  public columnDefs = [];
  private localloanobject: loan_model = new loan_model();

  public rowData = [];
  public components;
  public context;
  public frameworkcomponents;
  public editType;
  public gridApi;
  public columnApi;
  public pinnedBottomRowData;

  public columnsDisplaySettings: ColumnsDisplaySettings = new ColumnsDisplaySettings();

  tableId = this.marketingContractsService.tableId

  currenterrors: Array<errormodel>;
  style: any = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };
  private preferencesChangeSub: ISubscription;

  // mktContractsChevron: RefChevron;

  constructor(public localstorageservice: LocalStorageService,
    private toaster: ToastrService,
    public loanserviceworker: LoancalculationWorker,
    public cropunitservice: CropapiService,
    public logging: LoggingService,
    public alertify: AlertifyService,
    public loanapi: LoanApiService,
    private marketingContractCalculationService: MarketingcontractcalculationService,
    public collateralService: CollateralService,
    public marketingContractsService: MarketingContractsService,
    public publishService: PublishService,
    private dtss: DatetTimeStampService,
    public validationservice: ValidationService,
    private settingsService: SettingsService,
    private dataService : DataService
  ) {
    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);
    this.components = { numericCellEditor: getNumericCellEditor(), alphaNumeric: getAlphaNumericCellEditor() };
    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
    this.frameworkcomponents = { selectEditor: SelectEditor, deletecolumn: DeleteButtonRenderer };
    this.context = { componentParent: this };
    this.columnDefs = this.marketingContractsService.getColumnDefs();
    this.getUserPreferences();
  }

  ngOnInit() {
    this.subscribeToChanges();
    this.rowData = this.collateralService.getRowData(this.localloanobject, CollateralSettings.marketingContracts.key, CollateralSettings.marketingContracts.source, CollateralSettings.marketingContracts.sourceKey);
    this.rowData = _.orderBy(this.rowData, ['Crop_Code', 'Crop_Type_Code', 'Assoc_ID', 'Contract'], ['asc', 'asc', 'asc', 'asc']);

    this.collateralService.adjustgrid(this.gridApi);

    setTimeout(()=>{
      this.setValidationErrorForMktContracts();
      this.getCurrentErrors();
      setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), this.marketingContractsService.mktContractsChevron.Chevron_Code + '_');
    },5);
  }

  getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;
    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe((preferences) => {
      this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
      this.hideUnhideLoanKeys();
      this.declareColumns();
    });
    // this.declareColumns();
  }

  declareColumns() {
    this.columnDefs[1]['hide'] = !this.columnsDisplaySettings.cropType;
  }

  //Hide Columns Based ON Loan Settings
  private LoankeySettings:Loan_Key_Visibilty;
  hideUnhideLoanKeys() {
    let LoanSettings: Loansettings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
    if (LoanSettings != undefined || LoanSettings != null) {
      if (LoanSettings.Loan_key_Settings != undefined || LoanSettings.Loan_key_Settings != null) {
        this.LoankeySettings=LoanSettings.Loan_key_Settings
      }
      else
      {
        this.LoankeySettings=new Loan_Key_Visibilty();
      }
    }
    else
    {
      this.LoankeySettings=new Loan_Key_Visibilty();
    }
    this.columnsDisplaySettings.cropType=this.LoankeySettings.Crop_Type==null?this.columnsDisplaySettings.cropType:this.LoankeySettings.Crop_Type;

  }

  public Loankeys = ["Crop_Type_Code"];
  displayColumnsChanged($event) {
    if (this.columnApi != undefined && $event!=undefined) {

      //first One to save to Loan Settings
      let LoanSettings: Loansettings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
      if (LoanSettings == undefined || LoanSettings == null) {
        LoanSettings = new Loansettings();
      }
      if (LoanSettings.Loan_key_Settings == undefined || LoanSettings.Loan_key_Settings == null) {
        LoanSettings.Loan_key_Settings = new Loan_Key_Visibilty();
      }
      //Since we dont have column thats changed so lets see manually
      this.Loankeys.forEach(key => {
        let mainkey = "";

        switch (key) {
          case "Crop_Type_Code":
            mainkey = "Crop_Type"
            break;
        }

        let column = this.columnApi.getColumn(key);
        if(column){
          LoanSettings.Loan_key_Settings[mainkey] = column.visible;
        }
      });
      this.localloanobject.LoanMaster.Loan_Settings = JSON.stringify(LoanSettings);
      this.dataService.setLoanObjectwithoutsubscription(this.localloanobject);
      //Second Execution
      this.adjustToFitAgGrid();
    }
    this.adjustToFitAgGrid();
  }

  subscribeToChanges() {
    this.subscription = this.dataService.getLoanObject().subscribe(res=>{

      this.localloanobject = res;
      if (res.LoanMarketingContracts && res.srccomponentedit == 'MarketingContractsComponent') {
        this.rowData[res.lasteditrowindex] = this.localloanobject.LoanMarketingContracts.filter(p => p.ActionStatus != 3)[res.lasteditrowindex];
        this.localloanobject.srccomponentedit = undefined;
        this.localloanobject.lasteditrowindex = undefined;
      } else if (res.LoanMarketingContracts) {
        this.rowData = this.localloanobject.LoanMarketingContracts.filter(p => p.ActionStatus != 3);
      } else {
        this.rowData = [];
      }

      if(this.gridApi) {
        this.gridApi.refreshCells({
          columns: ['Market_Value', 'Contract_Per']
        });
      }

      setTimeout(()=>{
        this.setValidationErrorForMktContracts();
        this.getCurrentErrors();
        setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), this.marketingContractsService.mktContractsChevron.Chevron_Code + '_');
      },5);
    });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    //these two methods remove side option toolbar , sets column , removes header menu
    //setgriddefaults(this.gridApi,this.columnApi);
    autoSizeAll(this.columnApi);
    params.api.sizeColumnsToFit();
    ///////////////////////////////////////////////////////////////////////////////////
    this.adjustToFitAgGrid();
    this.getCurrentErrors();
  }

  public gridOptions = {
    getRowNodeId: (data) => {
      return this.marketingContractsService.rowId + data.Contract_ID;
    }
  }

  //Grid Events
  addrow() {
    if(!this.isLoanEditable()) {
      return;
    }

    let hasBlankRow = this.rowData.find(a => !a.Crop_Code);

    if(hasBlankRow) {
      this.alertify.alert('Alert', 'You already have a blank row without Crop/Crop Type.');
    } else {
      this.collateralService.addRow(this.localloanobject, this.gridApi, this.rowData, CollateralSettings.marketingContracts.key, CollateralSettings.marketingContracts.source, CollateralSettings.marketingContracts.sourceKey);
      this.publishService.enableSync(Page.crop);
      this.setValidationErrorForMktContracts();
      this.getCurrentErrors();
    }
  }

  rowvaluechanged(value: any) {
        //---------Modified yellow values-------------
    //MODIFIED YELLOW VALUES

    if(value && (value.oldValue == value.value || (!value.oldValue && !value.value))) return;

    if(!value && !value.column && !value.column.colId) return;

    if(value.column.colId == 'Crop_Code') {
      value.data.UoM = lookupUOM(value.data.Crop_Code, this.refdata);
    }

    let id = value.data.Contract_ID;
    let modifiedvalues = this.localstorageservice.retrieve(environment.modifiedbase) as Array<String>;

    if (!modifiedvalues.includes(this.marketingContractsService.rowId + id + "_" + value.colDef.field)) {
      modifiedvalues.push(this.marketingContractsService.rowId + id + "_" + value.colDef.field);
      this.localstorageservice.store(environment.modifiedbase, modifiedvalues); +
        setmodifiedsingle(this.marketingContractsService.rowId + id + "_" + value.colDef.field, this.marketingContractsService.rowId);
    }

    this.marketingContractsService.rowvaluechanged(value, this.localloanobject);
    // const eventDelegations = value.column.colDef.eventDelegations;
    // if (eventDelegations && eventDelegations.validations) {
    this.setValidationErrorForMktContracts();
    this.publishService.enableSync(Page.crop);
    this.getCurrentErrors();
  }

  private setValidationErrorForMktContracts(){
    // this.validationservice.validateMktContTableFields<Loan_Marketing_Contract>(this.rowData);
    this.validationservice.validateTableFields<Loan_Marketing_Contract>(
      this.rowData,
      this.columnDefs,
      Page.crop,
      this.marketingContractsService.rowId,
      // this.mktContractsChevron.Chevron_Code,
      this.marketingContractsService.mktContractsChevron.Chevron_Code,
      'Contract_ID'
    );
  }


  DeleteClicked(rowIndex: any) {
    this.alertify.confirm("Confirm", "Do you Really Want to Delete this Record?").subscribe(res => {
      if (res == true) {

        let obj = this.rowData[rowIndex];
        if (obj && CollateralSettings.marketingContracts.pk == '0') {
          this.rowData.splice(rowIndex, 1);
          this.localloanobject.LoanMarketingContracts.splice(this.localloanobject.LoanMarketingContracts.indexOf(obj), 1);
        } else {
          if(obj) obj.ActionStatus = 3;
        }
        this.publishService.enableSync(Page.crop);
        this.setValidationErrorForMktContracts();
        this.getCurrentErrors();
        this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
      }
    });
  }

  deleteClicked(rowIndex: any, localloanobject: loan_model, rowData, source, uniqueKey) {

  }

  sortBySetter() {
    let sort = [

      {
        colId: "Crop_Code",
        sort: "asc"
      },
      {
        colId: "Crop_Type_Code",
        sort: "asc"
      },
      {
        colId: "Assoc_ID",
        sort: "asc"
      },
      {
        colId: "Contract",
        sort: "asc"
      }

    ];
    this.gridApi.setSortModel(sort);
  }

  onBtExport() {
    const params = {
      skipHeader: false,
      columnGroups: true,
      skipFooters: false,
      skipGroups: false,
      skipPinnedTop: false,
      skipPinnedBottom: false,
      allColumns: true,
      fileName: `Marketing_Contracts_${this.localloanobject.Loan_Full_ID}_${this.dtss.getTimeStamp()}.xlxs`,
      sheetName: 'Sheet 1'
    };
    // Export Data to Excel file
    this.gridApi.exportDataAsExcel(params);
  }

  onGridSizeChanged(params) {
    this.adjustToFitAgGrid();
  }

  adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  displayedColumnsChanged(params){
    this.adjustToFitAgGrid();
  }

  isLoanEditable(){
    return !this.localloanobject || this.localloanobject.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  private getCurrentErrors() {
    this.currenterrors = (this.localstorageservice.retrieve(
      environment.errorbase
    ) as Array<errormodel>).filter(p => p.chevron == this.marketingContractsService.mktContractsChevron.Chevron_Code);

    this.validationservice.highlighErrorCells(this.currenterrors, this.marketingContractsService.tableId);
  }
}
