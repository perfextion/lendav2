import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { environment } from '../../../../environments/environment.prod';
import { CrossCollateralDetailService } from './cross-collateral.service';
import { loan_model, LoanStatus, Cross_Collateralized_Loans, Loan_Cross_Collateral } from '../../../models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { PublishService } from '@lenda/services/publish.service';
import { Page } from '@lenda/models/page.enum';
import { ISubscription } from 'rxjs/Subscription';
import { DataService } from '@lenda/services/data.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { MasterService } from '@lenda/master/master.service';

@Component({
  selector: 'app-cross-collateral-detail',
  templateUrl: './cross-collateral.component.html',
  styleUrls: ['./cross-collateral.component.scss'],
  providers: [CrossCollateralDetailService]
})
export class CrossCollateralDetailComponent implements OnInit, OnDestroy {

  /**The loan_model instance */
  public localloanobject: loan_model = new loan_model();
  public collateralizeItems: Array<Cross_Collateralized_Loans> = [];
  private loanList: Array<Loan_Cross_Collateral> = [];
  public loanListCopy: Array<Loan_Cross_Collateral> = [];

  showSelection: boolean = false;

  private subscription: ISubscription;

  /**Dropdown Selection for Master. */
  isMaster =  [{key: 1, name: 'Yes'}, {key: 0, name: 'No'}];

  constructor(
    private loanApiService: LoanApiService,
    private localstorageservice: LocalStorageService,
    private dataService: DataService,
    private loanserviceworker: LoancalculationWorker,
    public alertify: AlertifyService,
    private publishService: PublishService,
    public masterSvc: MasterService
  ) {

    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);
    this.loanList = this.localstorageservice.retrieve(environment.loanCrossCollateral);
    this.collateralizeItems = this.localloanobject.Cross_Collateralized_Loans;
    //this.collateralizeItems = this.localloanobject.Cross_Collateralized_Loans;
  }

  ngOnInit() {

    this.prepareLoanList();
    this.sortSelection();

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res != null) {

        this.localloanobject = res;
        this.collateralizeItems = this.localloanobject.Cross_Collateralized_Loans;
        this.sortSelection();
      }
    })
  }

  /**Method to set Farmer and Borrower Name. */
  private prepareLoanList(){
    this.loanList.forEach(loan => {
      loan.Farmer = loan.Farmer_Last_Name + ', ' + loan.Farmer_First_Name + ' ' + loan.Farmer_MI + ' ',
      loan.Borrower = loan.Borrower_First_Name + ' ' + loan.Borrower_MI + ' ' + loan.Borrower_Last_Name,
      loan.View   = loan.Loan_ID + '   |   ' + loan.Farmer + '   |   ' + loan.Borrower
    })

    this.loanListCopy = this.loanList;
  }

  /**
   * Filter Loan Selection to show only
   * @param existingIDs
   */
  sortSelection(existingIDs?: any){

    if(!existingIDs){
      existingIDs = this.collateralizeItems.filter(loan => loan.ActionStatus != 3).map(loan => loan.Loan_ID);
    }
    this.loanListCopy = this.loanList.filter(ll => !existingIDs.includes(ll.Loan_ID) && ll.Loan_ID != this.localloanobject.LoanMaster.Loan_ID);
  }

  /**
   * Method to add new Row to Cross Collateral table.
   */
  addrow(){
    let isAnyRowWithoutLoanID = this.collateralizeItems.some(a => !a.Loan_Full_ID);

    if(isAnyRowWithoutLoanID) {
      this.alertify.alert('Alert', 'Warning: You already have a blank row without Loan ID/Loan Full ID.');
    } else {
      let item = new Cross_Collateralized_Loans();
      item.ActionStatus = 1;
      item.Status = 1;

      this.collateralizeItems.push(item);
    }
  }

  showSelectionChange(event, index){

    let allLoanSelection = document.querySelectorAll('[id^="loan-id"]');
    Array.from(allLoanSelection).forEach(ls => {
      ls.classList.remove("selection-active");
    })

    let element = document.getElementById('loan-id' + index);
    element.classList.add("selection-active");

    allLoanSelection = document.querySelectorAll('[id^="loanSelection"]');
    Array.from(allLoanSelection).forEach(ls => {
      ls.classList.remove("loan-selection-active");
    })

    element = document.getElementById('loanSelection' + index);
    element.classList.add("loan-selection-active");
  }

  mouseLeave(index){
    let element = document.getElementById('loanSelection' + index);
    element.classList.remove("loan-selection-active");
    element = document.getElementById('loan-id' + index);
    element.classList.remove("selection-active");
  }

  showMasterSelection(index){
    let allLoanSelection = document.querySelectorAll('[id^="Loan_Master_Cross_Ind-display"]');
    Array.from(allLoanSelection).forEach(ls => {
      ls.classList.remove("selection-active");
    })

    let element = document.getElementById('Loan_Master_Cross_Ind-display' + index);
    element.classList.add("selection-active");

    allLoanSelection = document.querySelectorAll('[id^="Loan_Master_Cross_Ind-selection"]');
    Array.from(allLoanSelection).forEach(ls => {
      ls.classList.remove("loan-selection-active");
    })
    element = document.getElementById('Loan_Master_Cross_Ind-selection' + index);
    element.classList.add("loan-selection-active");
  }

  hideMasterSelection(index){
    let element = document.getElementById('Loan_Master_Cross_Ind-display' + index);
    element.classList.remove("selection-active");
    element = document.getElementById('Loan_Master_Cross_Ind-selection' + index);
    element.classList.remove("loan-selection-active");
  }

  getMasterIndicatorValue(master){
    if(master != 0 && master != 1) return '';
    return this.isMaster.find(ma => ma.key == master).name;
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  /**Check is Loan is Editable or no. */
  isLoanEditable(){
    return !this.localloanobject || this.localloanobject.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  /**
   * Update Row Value based on selected row ID.
   * @param event The new Loan Object Selected.
   * @param index The CrossCollateralize Object INdex.
   */
  updateSelection(event, index){

      this.collateralizeItems[index].Loan_ID = event && event.Loan_ID ? event.Loan_ID : '';
      this.collateralizeItems[index].Farmer = event && event.Loan_ID ? event.Farmer : '';
      this.collateralizeItems[index].Borrower =  event && event.Loan_ID ? event.Borrower : '';
      this.collateralizeItems[index].Cross_Collateral_Group_ID =  event && event.Loan_ID ? event.Cross_Collateral_Group_ID : '';
      // this.collateralizeItems[index].Loan_Master_Cross_Ind =  event && event.Loan_ID ? event.Loan_Master_Cross_Ind : 0;

      let existingIDs = this.collateralizeItems.filter(loan => loan.ActionStatus != 3).map(loan => loan.Loan_ID);

      if(event && event.Cross_Collateral_Group_ID > 0){
        let relatedLoans = this.loanList.filter(
          loan =>
            (loan.Cross_Collateral_Group_ID == event.Cross_Collateral_Group_ID) &&
            (loan.Loan_ID != event.Loan_ID && !existingIDs.includes(loan.Loan_ID))
        );

        if(relatedLoans && relatedLoans.length > 0) {
          let rel: Array<Cross_Collateralized_Loans> = [];

          relatedLoans.forEach(rl => {
            let index = this.collateralizeItems.findIndex(a => a.Loan_ID == rl.Loan_ID);

            if(index == -1) {
              let item = new Cross_Collateralized_Loans();

              item.Cross_Collateral_Group_ID  = rl.Cross_Collateral_Group_ID;
              item.Loan_ID =  rl.Loan_ID;
              item.Farmer = rl.Farmer;
              item.Borrower = rl.Borrower;
              item.ActionStatus = 1;

              rel.push(item);
            }
          });

          this.collateralizeItems =  this.collateralizeItems.concat(rel);
        }
      }

      this.localloanobject.Cross_Collateralized_Loans = this.collateralizeItems;


      this.sortSelection();
      this.loanserviceworker.performcalculationonloanobject(this.localloanobject, false);
      this.publishService.enableSync(Page.collateral);
  }

  /**
   * Remove row row CrossCollateralize Array.
   * @param index The CrossCollateralize Object INdex.
   */
  removeItem(index){

    this.alertify.deleteRecord().subscribe(res => {
      if (res) {
        let item = this.collateralizeItems[index];

        if(item.ActionStatus != 1) {
          this.collateralizeItems[index].ActionStatus = 3;
        } else {
          this.collateralizeItems.splice(index, 1);
        }

        this.localloanobject.Cross_Collateralized_Loans = this.collateralizeItems;
        this.sortSelection();
        this.loanserviceworker.performcalculationonloanobject(this.localloanobject, false);
        this.publishService.enableSync(Page.collateral);
      }
    })
  }

  /**
   *
   */
  noActiveCollateral(){
    let result = this.collateralizeItems.filter(col => col.ActionStatus != 3);
    return result && result.length > 0 ? false : true;
  }

  crossCollAffLoans(){
    this.loanserviceworker.crossCollAffLoans();
  }
}
