import { Component, OnInit, ViewChild, ElementRef, HostListener, ViewEncapsulation, Input } from '@angular/core';
import { loan_model, LoanStatus, Collateral_Category_Code } from '@lenda/models/loanmodel';
import { MatSlider, MatDialog } from '@angular/material';
import { ISubscription } from 'rxjs/Subscription';
import { dollarformat, PracticeTranslator, formatacres } from '@lenda/aggridformatters/valueformatters';
import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';
import { DataService } from '@lenda/services/data.service';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { environment } from '@env/environment';
import { lookupCountyValue, lookupStateRefValue, lookupStateNameFromPassedRefObject, lookupCountyValueFromPassedRefData, lookupStateCodeFromPassedRefObject } from '@lenda/Workers/utility/aggrid/stateandcountyboxes';
import { Loan_Key_Visibilty } from '@lenda/models/loansettings';
import { SharedService } from '@lenda/services/shared.service';
import { lookupCropTypeWithRefData, lookupCropPracticeTypeWithRefData } from '@lenda/Workers/utility/aggrid/cropboxes';
import { Helper } from '@lenda/services/math.helper';
import { CollateralHelper } from '../collateral-helper.service';
import { RefDataModel } from '@lenda/models/ref-data-model';

@Component({
  selector: 'app-loan-detail',
  templateUrl: './loan-detail.component.html',
  styleUrls: ['./loan-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoanDetailComponent implements OnInit {

  //Properties
  public exampleset = [1, 2, 3, 4, 5];
  @ViewChild('table') el: ElementRef;
  @ViewChild(MatSlider) slider: MatSlider;
  private subscription: ISubscription;
  public sliderwidth: number = 0;
  max = 585;
  min = 0;
  step = 45;
  sliderValue = 0;
  private localloanobj: loan_model;
  private refdata: RefDataModel;
  rowData: any[];
  changed(event) {
    this.sliderValue = event.value;
    this.getgriddata();
  }
  //
  isLoanEditable() {
    return !this.localloanobj || this.localloanobj.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  @Input() expanded = true;

  tableColToDisplay: any = null;
  constructor(private localstorageservice: LocalStorageService,
    private dtss: DatetTimeStampService,
    private sharedservice: SharedService,
    private dataService: DataService, private dialog: MatDialog) {

    let tctd = this.localstorageservice.retrieve(environment.loankey);
    if (!_.isEmpty(tctd)) {
      this.tableColToDisplay = JSON.parse(tctd.LoanMaster.Loan_Settings).Loan_key_Settings as Loan_Key_Visibilty;
      if (this.tableColToDisplay) {
        Object.keys(this.tableColToDisplay).forEach(keyname => {
          switch (keyname) {
            case 'Crop_Type':
              this.availablecolumns.find(p => p.currentproperty == 'Crop Type').visible = this.tableColToDisplay[keyname] == null ?
                false : this.tableColToDisplay[keyname];
              break;
            case 'Crop_Practice':
              this.availablecolumns.find(p => p.currentproperty == 'Crop Prac').visible = this.tableColToDisplay[keyname] == null ?
                false : this.tableColToDisplay[keyname];
              break;
            case 'Irr_Practice':
              this.availablecolumns.find(p => p.currentproperty == 'Irr Prac').visible = this.tableColToDisplay[keyname] == null ?
                false : this.tableColToDisplay[keyname];
              break;
            case 'Section':
              this.availablecolumns.find(p => p.currentproperty == 'Section').visible = this.tableColToDisplay[keyname] == null ?
                false : this.tableColToDisplay[keyname];
              break;
            case 'Rated':
              this.availablecolumns.find(p => p.currentproperty == 'Rated').visible = this.tableColToDisplay[keyname] == null ?
                false : this.tableColToDisplay[keyname];
              break;
          }
        });
      }
      //now load data

      let currentvalue = 0;
      this.availablecolumns.filter(p => p.visible == true && p.isslider == true).forEach(columns => {
        columns.slidervalue = currentvalue;
        currentvalue = currentvalue + 45;
      });
      this.localloanobj = this.localstorageservice.retrieve(environment.loankey);
      this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
      this.sharedservice.isLoanDetailExtra$.subscribe(result => {
        this.availablecolumns.forEach(element => {
          if(element.optional)
          {
            element.visible=result;
          }
        });
        this.getgriddata();
      });
      this.subscribeToChanges();
      this.getgriddata();
      setTimeout(() => {
        this.sliderwidthcalculator();
      }, 500);
    }
  }

  ngOnInit() {

  }

  private subscribeToChanges() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localloanobj = res;
      this.getgriddata();

    });
  }

  sliderstyle = {
    width: '0px'
  }
  //Utility Functions
  @HostListener('window:resize', ['$event'])
  onResize() {

    this.sliderwidthcalculator();
  }
  private sliderwidthcalculator() {
    let sliderwidth = 0;
    this.availablecolumns.filter(p => p.visible == true && p.isslider == true).forEach((element, index) => {
      sliderwidth = sliderwidth + 45;

    });
    this.max = sliderwidth - 45;
    let length = this.max / 45;
    sliderwidth = 0;
    let e = (this.el.nativeElement as HTMLTableElement).rows[0].cells;
    Array.from(e).forEach((element, index) => {
      if (index < length)
        sliderwidth = sliderwidth + element.clientWidth;
    });
    this.sliderstyle.width = sliderwidth + "px";
  }

  formatamounts(amount, parse: boolean) {
    if (!parse) {
      if (amount != '-') {

        if (amount == "$ NaN" || amount == null || amount == "") {
          return "$ 0";
        }
        else {
          amount = amount.replace('$ ', '');
          let value = dollarformat(parseFloat(amount), '', 0)

          return value;
        }
      }
      else {
        return '-';
      }
    }
    else {
      return amount;
    }
  }

  getgriddata() {

    let isrefershneeded = true;
    let column = this.availablecolumns.find(a => a.slidervalue == this.sliderValue);

    if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "ST").slidervalue) {
      this.rowData = [];
      let objects = _.uniqBy(this.localloanobj.LoanCropUnits.filter(p=>p.CU_Acres>0), "FC_StateID");
      for (let i = 0; i < objects.length; i++) {

        let farm = this.localloanobj.Farms.find(p => p.Farm_ID == objects[i].Farm_ID);
        let row: any = {};
        row.Category = "Crop"
        row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata) ;
        row.County = "-";
        row.Landlord = "-";
        row.FSN = "-";
        row.farm_acres = _.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_StateID == objects[i].FC_StateID).map(p => p.CU_Acres));
        row.Acres=formatacres(row.farm_acres);
        row.Section = "-";
        row.Prod_Perc =this.getvalueforprodpercbystate(farm.Farm_State_ID)+ "%";
        row.Prod_Perc_key = row.Prod_Perc;
        row.Perc_Ins = this.getvalueforinspercbystate(farm.Farm_State_ID) + "%";
        row.Rated = "-";
        row.Crop = "-";
        row.Crop_Type = "-"
        row.Irr_Prac = "-";
        row.Crop_Prac = "-";
        row.Ins_Plan = "-";
        row.Ins_Type = "-";
        row.Settings = false;
        row.Arm_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_StateID == objects[i].FC_StateID).map(p => p.Arm_Budget_Sum || 0))));
        row.Dist_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_StateID == objects[i].FC_StateID).map(p => p.Dist_Budget_Sum || 0))));
        row.Thirparty_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_StateID == objects[i].FC_StateID).map(p => p.Third_Budget_Sum || 0))));
        row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_StateID == objects[i].FC_StateID).map(p => p.Mkt_Value || 0))));
        row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_StateID == objects[i].FC_StateID).map(p => p.Disc_Mkt_Value || 0))));
        row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_StateID == objects[i].FC_StateID).map(p => p.Ins_Value || 0))));
        row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_StateID == objects[i].FC_StateID).map(p => p.Disc_Ins_value || 0))));
        row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_StateID == objects[i].FC_StateID).map(p => p.Disc_CEI_Value || 0))));
        row.CF = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_StateID == objects[i].FC_StateID).map(p => p.FC_Cashflow || 0))));
        row.RC = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_StateID == objects[i].FC_StateID).map(p => p.FC_RiskCushion || 0))));
        row.Margin = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_StateID == objects[i].FC_StateID).map(p => p.FC_Margin || 0))));
        this.rowData.push(row);
      }
      this.generateWfrp();
    } else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "County").slidervalue) {
      this.rowData = [];
      let objects = _.uniqBy(this.localloanobj.LoanCropUnits.filter(p=>p.CU_Acres>0), "FC_CountyID");
      for (let i = 0; i < objects.length; i++) {

        let farm = this.localloanobj.Farms.find(p => p.Farm_ID == objects[i].Farm_ID);
        let row: any = {};
        row.Category = "Crop"
        row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata) ;
        row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
        row.Landlord = "-";
        row.FSN = "-";
        row.Prod_Perc =this.getvalueforprodpercbycounty(farm.Farm_County_ID)+ "%";
        row.Prod_Perc_key = row.Prod_Perc;
        row.Perc_Ins = this.getvalueforinspercbycounty(farm.Farm_County_ID) + "%";
        row.Section = "-";
        row.Rated = "-";
        row.Crop = "-";
        row.farm_acres = _.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_CountyID == objects[i].FC_CountyID).map(p => p.CU_Acres));
        row.Acres = formatacres(row.farm_acres);
        row.Crop_Type = "-"
        row.Irr_Prac = "-";
        row.Crop_Prac = "-";
        row.Ins_Plan = "-";
        row.Ins_Type = "-";
        row.Settings = false;
        row.Arm_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_CountyID == objects[i].FC_CountyID).map(p => p.Arm_Budget_Sum || 0))));
        row.Dist_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_CountyID == objects[i].FC_CountyID).map(p => p.Dist_Budget_Sum || 0))));
        row.Thirparty_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_CountyID == objects[i].FC_CountyID).map(p => p.Third_Budget_Sum || 0))));
        row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_CountyID == objects[i].FC_CountyID).map(p => p.Mkt_Value || 0))));
        row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_CountyID == objects[i].FC_CountyID).map(p => p.Disc_Mkt_Value || 0))));
        row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_CountyID == objects[i].FC_CountyID).map(p => p.Ins_Value || 0))));
        row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_CountyID == objects[i].FC_CountyID).map(p => p.Disc_Ins_value || 0))));
        row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_CountyID == objects[i].FC_CountyID).map(p => p.Disc_CEI_Value || 0))));
        row.CF = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_CountyID == objects[i].FC_CountyID).map(p => p.FC_Cashflow || 0))));
        row.RC = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_CountyID == objects[i].FC_CountyID).map(p => p.FC_RiskCushion || 0))));
        row.Margin = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.FC_CountyID == objects[i].FC_CountyID).map(p => p.FC_Margin || 0))));
        this.rowData.push(row);
      }
      this.generateWfrp();
    }
    else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "FSN").slidervalue) {
      this.rowData = [];
      let objects = _.uniqBy(this.localloanobj.LoanCropUnits.filter(p=>p.CU_Acres>0), "Farm_ID");
      for (let i = 0; i < objects.length; i++) {

        let farm = this.localloanobj.Farms.find(p => p.Farm_ID == objects[i].Farm_ID);
        let row: any = {};
        row.Category = "Crop"
        row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata) ;
        row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
        row.Landlord = "-";
        row.Prod_Perc = this.getvalueforprodpercbystate(farm.Farm_State_ID)+ "%";;
        row.Prod_Perc_key=farm.Percent_Prod + "%";
        row.Perc_Ins = this.getvalueforinspercbycounty(farm.Farm_County_ID) + "%";
        row.farm_acres = farm.FC_Total_Acres
        row.Acres = formatacres(row.farm_acres);
        row.FSN = farm.FSN
        row.Section = '-';
        row.Rated = '-';
        row.Crop = "-";
        row.Crop_Type = "-"
        row.Irr_Prac = "-";
        row.Crop_Prac = "-";
        row.Ins_Plan = "-";
        row.Ins_Type = "-";
        row.Settings = false;
        row.Arm_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Arm_Budget_Sum || 0))));
        row.Dist_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Dist_Budget_Sum || 0))));
        row.Thirparty_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Third_Budget_Sum || 0))));
        row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Mkt_Value || 0))));
        row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Disc_Mkt_Value || 0))));
        row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Ins_Value || 0))));
        row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Disc_Ins_value || 0))));
        row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.CEI_Value || 0))));
        row.CF = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_Cashflow || 0))));
        row.RC = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_RiskCushion || 0))));
        row.Margin = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_Margin || 0))));
        this.rowData.push(row);
      }
      this.generateWfrp();
    }
    else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Prod %").slidervalue) {
      this.rowData = [];
      let objects = _.uniqBy(this.localloanobj.LoanCropUnits.filter(p=>p.CU_Acres>0), "Farm_ID");
      for (let i = 0; i < objects.length; i++) {

        let farm = this.localloanobj.Farms.find(p => p.Farm_ID == objects[i].Farm_ID);
        let row: any = {};
        row.Category = "Crop"
        row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata);
        row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
        row.Prod_Perc = this.getvalueforprodpercbystate(farm.Farm_State_ID)+ "%";;
        row.Prod_Perc_key=farm.Percent_Prod + "%";
        row.Perc_Ins = this.getvalueforinspercbycounty(farm.Farm_County_ID) + "%";
        row.Landlord = "-";
        row.FSN = farm.FSN
        row.Section = '-';
        row.Rated = '-';
        row.farm_acres = farm.FC_Total_Acres
        row.Acres = formatacres(row.farm_acres);
        row.Crop = "-";
        row.Crop_Type = "-"
        row.Irr_Prac = "-";
        row.Crop_Prac = "-";
        row.Ins_Plan = "-";
        row.Ins_Type = "-";
        row.Settings = false;
        row.Arm_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Arm_Budget_Sum || 0))));
        row.Dist_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Dist_Budget_Sum || 0))));
        row.Thirparty_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Third_Budget_Sum || 0))));
        row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Mkt_Value || 0))));
        row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Disc_Mkt_Value || 0))));
        row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Ins_Value || 0))));
        row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Disc_Ins_value || 0))));
        row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.CEI_Value || 0))));
        row.CF = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_Cashflow || 0))));
        row.RC = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_RiskCushion || 0))));
        row.Margin = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_Margin || 0))));
        this.rowData.push(row);
      }
      this.generateWfrp();
    }
    else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Acres").slidervalue) {
      this.rowData = [];
      let objects = _.uniqBy(this.localloanobj.LoanCropUnits.filter(p=>p.CU_Acres>0), "Farm_ID");
      for (let i = 0; i < objects.length; i++) {

        let farm = this.localloanobj.Farms.find(p => p.Farm_ID == objects[i].Farm_ID);
        let row: any = {};
        row.Category = "Crop"
        row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata) ;
        row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
        row.Prod_Perc =this.getvalueforprodpercbystate(farm.Farm_State_ID)+ "%";
        row.Prod_Perc_key=farm.Percent_Prod + "%";
        row.Perc_Ins = this.getvalueforinspercbystate(farm.Farm_State_ID) + "%";
        row.Landlord = "";
        row.FSN = farm.FSN
        row.Section = farm.Section;
        row.Rated = '-';
        row.farm_acres = farm.FC_Total_Acres
        row.Acres = formatacres(row.farm_acres);
        row.Crop = "-";
        row.Crop_Type = "-"
        row.Irr_Prac = "-";
        row.Crop_Prac = "-";
        row.Ins_Plan = "-";
        row.Ins_Type = "-";
        row.Settings = false;
        row.Arm_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Arm_Budget_Sum || 0))));
        row.Dist_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Dist_Budget_Sum || 0))));
        row.Thirparty_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Third_Budget_Sum || 0))));
        row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Mkt_Value || 0))));
        row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Disc_Mkt_Value || 0))));
        row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Ins_Value || 0))));
        row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Disc_Ins_value || 0))));
        row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.CEI_Value || 0))));
        row.CF = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_Cashflow || 0))));
        row.RC = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_RiskCushion || 0))));
        row.Margin = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_Margin || 0))));
        this.rowData.push(row);
      }
      this.generateWfrp();
    }
    else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Ins %").slidervalue) {
      this.rowData = [];
      let objects = _.uniqBy(this.localloanobj.LoanCropUnits.filter(p=>p.CU_Acres>0), "Farm_ID");
      for (let i = 0; i < objects.length; i++) {

        let farm = this.localloanobj.Farms.find(p => p.Farm_ID == objects[i].Farm_ID);
        let row: any = {};
        row.Category = "Crop"
        row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata) ;
        row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
        row.Prod_Perc =this.getvalueforprodpercbystate(farm.Farm_State_ID)+ "%";
        row.Prod_Perc_key=farm.Percent_Prod + "%";
        row.Perc_Ins = this.getvalueforinspercbystate(farm.Farm_State_ID) + "%";
        row.Landlord = '';
        row.FSN = farm.FSN
        row.Section = farm.Section;
        row.Rated = '-';
        row.farm_acres = farm.FC_Total_Acres
        row.Acres = formatacres(row.farm_acres);
        row.Crop = "-";
        row.Crop_Type = "-"
        row.Irr_Prac = "-";
        row.Crop_Prac = "-";
        row.Ins_Plan = "-";
        row.Ins_Type = "-";
        row.Settings = false;
        row.Arm_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Arm_Budget_Sum || 0))));
        row.Dist_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Dist_Budget_Sum || 0))));
        row.Thirparty_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Third_Budget_Sum || 0))));
        row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Mkt_Value || 0))));
        row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Disc_Mkt_Value || 0))));
        row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Ins_Value || 0))));
        row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Disc_Ins_value || 0))));
        row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.CEI_Value || 0))));
        row.CF = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_Cashflow || 0))));
        row.RC = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_RiskCushion || 0))));
        row.Margin = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_Margin || 0))));
        this.rowData.push(row);
      }
      this.generateWfrp();
    }
    else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Landlord").slidervalue) {
      this.rowData = [];
      let objects = _.uniqBy(this.localloanobj.LoanCropUnits.filter(p=>p.CU_Acres>0), "Farm_ID");
      for (let i = 0; i < objects.length; i++) {

        let farm = this.localloanobj.Farms.find(p => p.Farm_ID == objects[i].Farm_ID);
        let row: any = {};
        row.Category = "Crop"
        row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata) ;
        row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
        row.Prod_Perc =this.getvalueforprodpercbystate(farm.Farm_State_ID)+ "%";
        row.Prod_Perc_key=farm.Percent_Prod + "%";
        row.Perc_Ins = this.getvalueforinspercbystate(farm.Farm_State_ID) + "%";
        row.Landlord = farm.Landowner;
        row.farm_acres = farm.FC_Total_Acres
        row.Acres = formatacres(row.farm_acres);
        row.FSN = farm.FSN
        row.Section = '-';
        row.Rated = '-';
        row.Crop = "-";
        row.Crop_Type = "-"
        row.Irr_Prac = "-";
        row.Crop_Prac = "-";
        row.Ins_Plan = "-";
        row.Ins_Type = "-";
        row.Settings = false;
        row.Arm_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Arm_Budget_Sum || 0))));
        row.Dist_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Dist_Budget_Sum || 0))));
        row.Thirparty_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Third_Budget_Sum || 0))));
        row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Mkt_Value || 0))));
        row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Disc_Mkt_Value || 0))));
        row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Ins_Value || 0))));
        row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Disc_Ins_value || 0))));
        row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.CEI_Value || 0))));
        row.CF = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_Cashflow || 0))));
        row.RC = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_RiskCushion || 0))));
        row.Margin = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_Margin || 0))));
        this.rowData.push(row);
      }
      this.generateWfrp();
    }
    else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Rated").slidervalue || this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Section").slidervalue) {
      this.rowData = [];
      let objects = _.uniqBy(this.localloanobj.LoanCropUnits.filter(p=>p.CU_Acres>0), "Farm_ID");
      for (let i = 0; i < objects.length; i++) {

        let farm = this.localloanobj.Farms.find(p => p.Farm_ID == objects[i].Farm_ID);
        let row: any = {};
        row.Category = "Crop"
        row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata) ;
        row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
        row.Prod_Perc =this.getvalueforprodpercbystate(farm.Farm_State_ID)+ "%";
        row.Perc_Ins = this.getvalueforinspercbystate(farm.Farm_State_ID) + "%";
        row.Landlord = farm.Landowner;
        row.Prod_Perc_key=farm.Percent_Prod + "%";
        row.FSN = farm.FSN
        row.Section = farm.Section;
        row.Rated = farm.Rated;
        if(this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Section").slidervalue)  {
          row.Rated = '-';
        }
        row.Crop = "-";
        row.Crop_Type = "-"
        row.farm_acres = farm.FC_Total_Acres
        row.Acres = formatacres(row.farm_acres);
        row.Irr_Prac = "-";
        row.Crop_Prac = "-";
        row.Ins_Plan = "-";
        row.Ins_Type = "-";
        row.Settings = false;
        row.Arm_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Arm_Budget_Sum || 0))));
        row.Dist_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Dist_Budget_Sum || 0))));
        row.Thirparty_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Third_Budget_Sum || 0))));
        row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Mkt_Value || 0))));
        row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Disc_Mkt_Value || 0))));
        row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Ins_Value || 0))));
        row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.Disc_Ins_value || 0))));
        row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.CEI_Value || 0))));
        row.CF = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_Cashflow || 0))));
        row.RC = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_RiskCushion || 0))));
        row.Margin = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == objects[i].Farm_ID).map(p => p.FC_Margin || 0))));
        this.rowData.push(row);
      }
      this.generateWfrp();
    }
    else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Ins Plan").slidervalue) {
      this.rowData = [];
      let objects = this.localloanobj.LoanCropUnits.filter(p=>p.CU_Acres>0);
      for (let i = 0; i < objects.length; i++) {

        let farm = this.localloanobj.Farms.find(p => p.Farm_ID == objects[i].Farm_ID);
        let row: any = {};
        row.Category = "Crop"
        row.State =lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata) ;
        row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
        row.Prod_Perc =this.getvalueforprodpercbystate(farm.Farm_State_ID)+ "%";
        row.Perc_Ins = this.getvalueforinspercbystate(farm.Farm_State_ID) + "%";
        row.Landlord = farm.Landowner;
        row.FSN = farm.FSN;
        row.Section = farm.Section;
        row.Rated = farm.Rated
        row.Prod_Perc_key=farm.Percent_Prod + "%";
        row.Crop = this.getcropname(objects[i].Crop_Code);
        row.farm_acres = objects[i].CU_Acres;
        row.Acres = formatacres(row.farm_acres);
        row.Crop_Type = lookupCropTypeWithRefData(objects[i].Crop_Practice_ID, this.refdata);
        row.Irr_Prac = objects[i].Crop_Practice_Type_Code;
        row.Crop_Prac = lookupCropPracticeTypeWithRefData(objects[i].Crop_Practice_ID, this.refdata);
        row.Ins_Plan = "";
        row.Ins_Type = "";
        row.Settings = false;
        row.Arm_Budget = "$ " + this.normalizevalue(objects[i].Arm_Budget_Sum || 0);
        row.Dist_Budget = "$ " + this.normalizevalue(objects[i].Dist_Budget_Sum || 0);
        row.Thirparty_Budget = "$ " + this.normalizevalue(objects[i].Third_Budget_Sum || 0);
        row.Market_Value = "$ " + this.normalizevalue(objects[i].Mkt_Value || 0);
        row.Disc_Market_Value = "$ " + this.normalizevalue(objects[i].Disc_Mkt_Value || 0);
        row.Ins_Value = "-"
        row.Disc_Ins_Value = "-"
        row.CEI = "$ " + this.normalizevalue(objects[i].CEI_Value || 0);
        row.CF = "$ " + this.normalizevalue(objects[i].FC_Cashflow || 0);
        row.RC = "$ " + this.normalizevalue(objects[i].FC_RiskCushion || 0);
        row.Margin = "$ " + this.normalizevalue(objects[i].FC_Margin || 0);
        this.rowData.push(row);

        this.localloanobj.CropUnitPolicies.filter(p => p.Status != 3 && p.Loan_Crop_Unit_ID == objects[i].Loan_CU_ID).forEach(element => {
          let row: any = {};
          row.type = 'Ins Plan';
          row.Category = "Crop"
          row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata);
          row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
          row.Prod_Perc =this.getvalueforprodpercbystate(farm.Farm_State_ID)+ "%";
          row.Perc_Ins = this.getvalueforinspercbystate(farm.Farm_State_ID) + "%";
          row.Landlord = farm.Landowner;
          row.FSN = farm.FSN;
          row.Section = farm.Section;
          row.Rated = farm.Rated
          row.Crop = this.getcropname(objects[i].Crop_Code)
          row.Crop_Type = "-"
          row.Irr_Prac = objects[i].Crop_Practice_Type_Code;
          row.Crop_Prac = objects[i].Crop_Practice_Type_Code;
          row.Prod_Perc_key=farm.Percent_Prod + "%";
          row.Ins_Plan = element.Ins_Plan_Code;
          row.Ins_Type = element.Ins_Plan_Type_Code == "" ? '-' : element.Ins_Plan_Type_Code;
          row.Settings = false;
          row.Arm_Budget = "-";
          row.Dist_Budget = "-";
          row.Thirparty_Budget = "-";
          row.Market_Value = "-";
          row.Disc_Market_Value = "-";
          row.Ins_Value = "$ " + this.normalizevalue(element.Ins_Value || 0);
          row.Disc_Ins_Value = "$ " + this.normalizevalue(element.Disc_Ins_Value || 0);
          row.CEI = "-";
          row.CF = "-";
          row.RC = "-";
          row.Margin = "-";
          this.rowData.push(row);
        });
      }
      this.generateWfrp();
      //this.generateCollaterals();
    }
    else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Crop").slidervalue) {
      //
      this.rowData = [];
      let farms = _.uniqBy(this.localloanobj.LoanCropUnits, "Farm_ID");
      for (let i = 0; i < farms.length; i++) {
        //means crop level
        let objects = _.uniqBy(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == farms[i].Farm_ID && p.CU_Acres>0), "Crop_Code");
        for (let j = 0; j < objects.length; j++) {
          let farm = this.localloanobj.Farms.find(p => p.Farm_ID == objects[j].Farm_ID);
          let row: any = {};
          row.Category = "Crop"
          row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata);
          row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
          row.Prod_Perc =this.getvalueforprodpercbystate(farm.Farm_State_ID)+ "%";
          row.Perc_Ins = this.getvalueforinspercbystate(farm.Farm_State_ID) + "%";
          row.Landlord = farm.Landowner;
          row.FSN = farm.FSN;
          row.farm_acres = _.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == farms[i].Farm_ID).map(p => p.CU_Acres));
          row.Acres = formatacres(row.farm_acres);
          row.Section = farm.Section;
          row.Prod_Perc_key=farm.Percent_Prod + "%";
          row.Rated = farm.Rated
          row.Crop = this.getcropname(objects[j].Crop_Code);
          row.Crop_Type = "-";
          row.Irr_Prac = "-";
          row.Crop_Prac = "-";
          row.Ins_Plan = "-";
          row.Ins_Type = "-";
          row.Settings = false;
          row.Arm_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Arm_Budget_Sum || 0))));
          row.Dist_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Dist_Budget_Sum || 0))));
          row.Thirparty_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Third_Budget_Sum || 0))));
          row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Mkt_Value || 0))));
          row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Disc_Mkt_Value || 0))));
          row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Ins_Value || 0))));
          row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Disc_Ins_value || 0))));
          row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.CEI_Value || 0))));
          row.CF = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.FC_Cashflow || 0))));
          row.RC = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.FC_RiskCushion || 0))));
          row.Margin = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.FC_Margin || 0))));
          this.rowData.push(row);
        }

      }

      this.generateWfrp();
      //this.generateCollaterals();
    } else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Crop Type").slidervalue) {
      //
      this.rowData = [];
      let farms = _.uniqBy(this.localloanobj.LoanCropUnits, "Farm_ID");
      for (let i = 0; i < farms.length; i++) {
        //means crop level
        let objects = _.uniqBy(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == farms[i].Farm_ID && p.CU_Acres>0),c=>this.compareter(c));
        for (let j = 0; j < objects.length; j++) {
          let farm = this.localloanobj.Farms.find(p => p.Farm_ID == objects[j].Farm_ID);
          let row: any = {};
          row.Category = "Crop"
          row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata) ;
          row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
          row.Prod_Perc =this.getvalueforprodpercbystate(farm.Farm_State_ID)+ "%";
          row.Prod_Perc_key=farm.Percent_Prod + "%";
          row.Perc_Ins = this.getvalueforinspercbystate(farm.Farm_State_ID) + "%";
          row.Landlord = farm.Landowner;
          row.FSN = farm.FSN;
          row.farm_acres = _.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.CU_Acres));
          row.Acres = formatacres(row.farm_acres);
          row.Section = farm.Section;
          row.Rated = farm.Rated
          row.Crop = this.getcropname(objects[j].Crop_Code);
          row.Crop_Type = lookupCropTypeWithRefData(objects[j].Crop_Practice_ID, this.refdata);
          row.Irr_Prac = "-";
          row.Crop_Prac = "-";
          row.Ins_Plan = "-";
          row.Ins_Type = "-";
          row.Settings = false;
          row.Arm_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Arm_Budget_Sum || 0))));
          row.Dist_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Dist_Budget_Sum || 0))));
          row.Thirparty_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Third_Budget_Sum || 0))));
          row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Mkt_Value || 0))));
          row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Disc_Mkt_Value || 0))));
          row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Ins_Value || 0))));
          row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.Disc_Ins_value || 0))));
          row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.CEI_Value || 0))));
          row.CF = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.FC_Cashflow || 0))));
          row.RC = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.FC_RiskCushion || 0))));
          row.Margin = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == objects[j].Crop_Code && p.Farm_ID == objects[j].Farm_ID).map(p => p.FC_Margin || 0))));
          this.rowData.push(row);
        }

      }

      this.generateWfrp();
      //this.generateCollaterals();
    } else if (this.availablecolumns.filter(p => p.currentproperty == "Crop Prac" || p.currentproperty == "Irr Prac").filter(p => p.visible == true).map(p => p.slidervalue).includes(this.sliderValue)) //Crop practice
    {
      this.rowData = [];
      let farms = _.uniqBy(this.localloanobj.LoanCropUnits, "Farm_ID");
      for (let i = 0; i < farms.length; i++) {
        //means crop level
        let objects = _.uniqBy(this.localloanobj.LoanCropUnits.filter(p => p.Farm_ID == farms[i].Farm_ID && p.CU_Acres>0), "Crop_Practice_ID");
        for (let j = 0; j < objects.length; j++) {
          let farm = this.localloanobj.Farms.find(p => p.Farm_ID == objects[j].Farm_ID);
          let row: any = {};
          row.Category = "Crop"
          row.State = lookupStateCodeFromPassedRefObject(farm.Farm_State_ID, this.refdata) ;
          row.County = lookupCountyValueFromPassedRefData(farm.Farm_County_ID, this.refdata);
          row.Prod_Perc_key=farm.Percent_Prod + "%";
          row.Prod_Perc =this.getvalueforprodpercbystate(farm.Farm_State_ID)+ "%";
          row.Perc_Ins = this.getvalueforinspercbystate(farm.Farm_State_ID) + "%";
          row.Landlord = farm.Landowner;
          row.FSN = farm.FSN;
          row.farm_acres = _.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == objects[j].Crop_Practice_ID && p.Farm_ID == objects[j].Farm_ID).map(p => p.CU_Acres));
          row.Acres = formatacres(row.farm_acres);
          row.Section = farm.Section;
          row.Rated = farm.Rated
          row.Crop = this.getcropname(objects[j].Crop_Code);
          row.Crop_Type = lookupCropTypeWithRefData(objects[j].Crop_Practice_ID, this.refdata);
          row.Irr_Prac = objects[j].Crop_Practice_Type_Code

          if (column.mappedproperty == 'Crop_Prac') {
            row.Crop_Prac = lookupCropPracticeTypeWithRefData(objects[j].Crop_Practice_ID, this.refdata);
          } else {
            row.Crop_Prac = '-';
          }

          row.Ins_Plan = "-";
          row.Ins_Type = "-";
          row.Settings = false;
          row.Arm_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == objects[j].Crop_Practice_ID && p.Farm_ID == objects[j].Farm_ID).map(p => p.Arm_Budget_Sum || 0))));
          row.Dist_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == objects[j].Crop_Practice_ID && p.Farm_ID == objects[j].Farm_ID).map(p => p.Dist_Budget_Sum || 0))));
          row.Thirparty_Budget = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == objects[j].Crop_Practice_ID && p.Farm_ID == objects[j].Farm_ID).map(p => p.Third_Budget_Sum || 0))));
          row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == objects[j].Crop_Practice_ID && p.Farm_ID == objects[j].Farm_ID).map(p => p.Mkt_Value || 0))));
          row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == objects[j].Crop_Practice_ID && p.Farm_ID == objects[j].Farm_ID).map(p => p.Disc_Mkt_Value || 0))));
          row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == objects[j].Crop_Practice_ID && p.Farm_ID == objects[j].Farm_ID).map(p => p.Ins_Value || 0))));
          row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == objects[j].Crop_Practice_ID && p.Farm_ID == objects[j].Farm_ID).map(p => p.Disc_Ins_value || 0))));
          row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == objects[j].Crop_Practice_ID && p.Farm_ID == objects[j].Farm_ID).map(p => p.CEI_Value || 0))));
          row.CF = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == objects[j].Crop_Practice_ID && p.Farm_ID == objects[j].Farm_ID).map(p => p.FC_Cashflow || 0))));
          row.RC = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == objects[j].Crop_Practice_ID && p.Farm_ID == objects[j].Farm_ID).map(p => p.FC_RiskCushion || 0))));
          row.Margin = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == objects[j].Crop_Practice_ID && p.Farm_ID == objects[j].Farm_ID).map(p => p.FC_Margin || 0))));
          this.rowData.push(row);
        }

      }
      this.generateWfrp();
      //this.generateCollaterals();
    } else {
      isrefershneeded = false;
    }
    this.rowData=_.chain(this.rowData)
    .sortBy('State')
    .sortBy('County')
    .sortBy('Crop')
    .sortBy('FSN')

    .value();

    if (isrefershneeded) {
      this.generateTotals();
    }
  }
  compareter(elem) {
    return [elem.Crop_Code, lookupCropTypeWithRefData(elem.Crop_Practice_ID, this.refdata)].join();
  }
  generateTotals(): any {

    let row: any = {};
    row.Category = "Totals"
    row.State = "Totals";
    row.County = "";
    row.Prod_Perc = "";
    row.Landlord = "";
    row.FSN = "";
    let totalacres = _.sumBy(this.rowData.filter(a => a.type != 'Ins Plan'), r => parseFloat(r.farm_acres));
    row.Acres = formatacres(totalacres);

    let totalprodperc = _.sumBy(this.rowData.filter(a => a.type != 'Ins Plan'), r => parseFloat(r.farm_acres) * parseFloat(r.Prod_Perc));
    row.Prod_Perc =  Helper.divide(totalprodperc, parseFloat(totalacres as any)).toFixed(1) + '%';

    let totalprodins = _.sumBy(this.rowData.filter(a => a.type != 'Ins Plan'), r => parseFloat(r.farm_acres) * parseFloat(r.Perc_Ins));
    row.Perc_Ins = Helper.divide(totalprodins, parseFloat(totalacres as any)).toFixed(1) + '%';

    row.Section = "";
    row.Rated = "";
    row.Crop = "";
    row.Crop_Type = "";
    row.Irr_Prac = "";
    row.Crop_Prac = "";
    row.Ins_Plan = "";
    row.Ins_Type = "";
    row.Settings = false;
    row.Arm_Budget = '$ ' + _.sum(this.rowData.filter(p => p.Arm_Budget != "-").map(p => Number(p.Arm_Budget == undefined ? 0 : p.Arm_Budget.replace('$ ', ''))));
    row.Dist_Budget = '$ ' + _.sum(this.rowData.filter(p => p.Dist_Budget != "-").map(p => Number(p.Dist_Budget == undefined ? 0 : p.Dist_Budget.replace('$ ', ''))));
    row.Thirparty_Budget = '$ ' + _.sum(this.rowData.filter(p => p.Thirparty_Budget != "-").map(p => Number(p.Thirparty_Budget == undefined ? 0 : p.Thirparty_Budget.replace('$ ', ''))));
    row.Market_Value = '$ ' + _.sum(this.rowData.filter(p => p.Market_Value != "-").map(p => Number(p.Market_Value == undefined ? 0 : p.Market_Value.replace('$ ', ''))));
    row.Disc_Market_Value = '$ ' + _.sum(this.rowData.filter(p => p.Disc_Market_Value != "-").map(p => Number(p.Disc_Market_Value == undefined ? 0 : p.Disc_Market_Value.replace('$ ', ''))));
    row.Ins_Value = '$ ' + _.sum(this.rowData.filter(p => p.Ins_Value != "-").map(p => Number(p.Ins_Value == undefined ? 0 : p.Ins_Value.replace('$ ', ''))));
    row.Disc_Ins_Value = '$ ' + _.sum(this.rowData.filter(p => p.Disc_Ins_Value != "-").map(p => Number(p.Disc_Ins_Value == undefined ? 0 : p.Disc_Ins_Value.replace('$ ', ''))));
    row.CEI = '$ ' + _.sum(this.rowData.filter(p => p.CEI != "-").map(p => Number(p.CEI == undefined ? 0 : p.CEI.replace('$ ', ''))));
    row.CF = '$ ' + _.sum(this.rowData.filter(p => p.CF != "-").map(p => Number(p.CF == undefined ? 0 : p.CF.replace('$ ', ''))));
    row.RC = '$ ' + _.sum(this.rowData.filter(p => p.RC != "-").map(p => Number(p.RC == undefined ? 0 : p.RC.replace('$ ', ''))));
    row.Margin = '$ ' + _.sum(this.rowData.filter(p => p.Margin != "-").map(p => Number(p.Margin == undefined ? 0 : p.Margin.replace('$ ', ''))));
    this.rowData.push(row);
  }

  onBtnExport() { }


  generateWfrp() {

    let row: any = {};
    row.Category = "All"
    row.State = "All"
    row.County = "All";
    row.Prod_Perc = "All"
    row.Landlord = "All"
    row.FSN = "All"
    row.Section = "All"
    row.Rated = "All"
    row.Crop = "All";
    row.Crop_Type = "All"
    row.Irr_Prac = "All"
    row.Crop_Prac = "All"
    row.Ins_Plan = "WFRP";
    row.Ins_Type = "";
    row.Settings = false;
    row.Arm_Budget = '-';
    row.Dist_Budget = '-';
    row.Thirparty_Budget = '-';
    row.Market_Value =  '$ '+ this.localloanobj.Loanwfrp.Wfrp_Value;
    row.Disc_Market_Value =  '$ '+ this.localloanobj.Loanwfrp.Wfrp_Disc_Value;
    row.Ins_Value ='$ ' + this.localloanobj.Loanwfrp.Wfrp_Ins_Value;
    row.Disc_Ins_Value = '$ '+ this.localloanobj.Loanwfrp.Wfrp_Disc_Ins_Value;
    row.CEI = '-';
    row.CF = '-';
    row.RC = '-';
    row.Margin = '-';
    if(this.localloanobj.Loanwfrp.Wfrp_Enabled==true && (this.localloanobj.LoanCrops.filter(a => a.ActionStatus != 3).length > 2))
    this.rowData.push(row);
  }

  private CollateralCategories = CollateralHelper.CollateralCategories;

    generateCollaterals() {
    //Collaterals at the End
    let collaterals = this.localloanobj.LoanCollateral;
    for (let f = 0; f < this.CollateralCategories.length; f++) {
      let foundrecords = collaterals.filter(p => p.Collateral_Category_Code == this.CollateralCategories[f] && p.ActionStatus != 3);
      if (foundrecords.length > 0) {

        let row: any = {};
        row.Category = CollateralHelper.getCollateralAlisas(this.CollateralCategories[f]);
        row.State = "";
        row.County = "";
        row.Prod_Perc = "";
        row.Landlord = "";
        row.FSN = "";
        row.Section = "";
        row.Rated = "";
        row.Crop = this.CollateralCategories[f] == Collateral_Category_Code.StoredCrop ? foundrecords[0].Crop_Detail : "";
        row.Crop_Type = "";
        row.Irr_Prac = "";
        row.Crop_Prac = "";
        row.Ins_Plan = "";
        row.Ins_Type = "";
        row.Settings = false;
        row.Arm_Budget = '-';
        row.Dist_Budget = '-';
        row.Thirparty_Budget = '-';
        row.Market_Value = "$ " + Math.round(_.sum(foundrecords.map(p => Number(p.Market_Value))));
        row.Disc_Market_Value = "$ " + Math.round(_.sum(foundrecords.map(p => Number(p.Disc_Mkt_Value))));
        row.Ins_Value = "$ " + Math.round(_.sum(foundrecords.map(p => Number(p.Ins_Value))));
        row.Disc_Ins_Value = "$ " + Math.round(_.sum(foundrecords.map(p => Number(p.Disc_Ins_Value))));
        let cievalue = (Math.round(_.sum(foundrecords.map(p => Number(p.Disc_Mkt_Value)))) - Math.round(_.sum(foundrecords.map(p => Number(p.Disc_Ins_Value)))));
        row.CEI = "$ " + (cievalue < 0 ? 0 : cievalue);
        row.CF = '-';
        row.RC = '-';
        row.Margin = '-';
        this.rowData.push(row);
        // }
      }
      else {
        //default Row
        let row: any = {};
        row.Category_Code = this.CollateralCategories[f];
        row.Category = CollateralHelper.getCollateralAlisas(this.CollateralCategories[f]);
        row.Crop_Name = "-";
        row.Crop_Type = "-"
        row.Settings = true;
        row.Crop_Practice = "-"
        row.Ins_Plan = "-";
        row.Plan_Type = "-";
        row.Market_Value_Pre = "-";
        row.Prior_Lien = "-";
        row.is_Insured = "No";
        row.Market_Value = row.Market_Value_Pre;
        row.Disc_Market_Value = "-";
        row.Ins_Value = "-";
        row.Disc_Ins_Value = "-";
        row.CEI = "-";
        this.rowData.push(row);
      }
    }
  }

  //this is the list which will generate the columns and set vivbilty and slider value
  availablecolumns = [
    { currentproperty: 'ST', mappedproperty: 'State', visible: true, slidervalue: 0, isslider: true, format: false, optional: false,isnumber:false },
    { currentproperty: 'County', mappedproperty: 'County', visible: true, slidervalue: 0, isslider: true, format: false, optional: false,isnumber:false },
    { currentproperty: 'FSN', mappedproperty: 'FSN', visible: true, slidervalue: 0, isslider: true, format: false, optional: false,isnumber:false },
    { currentproperty: 'Prod %', mappedproperty: 'Prod_Perc_key', visible: true, slidervalue: 0, isslider: true, format: false, optional: false,isnumber:true },
    { currentproperty: 'Landlord', mappedproperty: 'Landlord', visible: true, slidervalue: 0, isslider: true, format: false, optional: false,isnumber:false },
    { currentproperty: 'Section', mappedproperty: 'Section', visible: true, slidervalue: 0, isslider: true, format: false, optional: false,isnumber:false },
    { currentproperty: 'Rated', mappedproperty: 'Rated', visible: true, slidervalue: 0, isslider: true, format: false, optional: false,isnumber:false },
    { currentproperty: 'Crop', mappedproperty: 'Crop', visible: true, slidervalue: 0, isslider: true, format: false, optional: false,isnumber:false },
    { currentproperty: 'Crop Type', mappedproperty: 'Crop_Type', visible: true, slidervalue: 0, isslider: true, format: false, optional: false,isnumber:false },
    { currentproperty: 'Irr Prac', mappedproperty: 'Irr_Prac', visible: true, slidervalue: 0, isslider: true, format: false, optional: false,isnumber:false },
    { currentproperty: 'Crop Prac', mappedproperty: 'Crop_Prac', visible: true, slidervalue: 0, isslider: true, format: false, optional: false,isnumber:false },
    { currentproperty: 'Ins Plan', mappedproperty: 'Ins_Plan', visible: true, slidervalue: 0, isslider: true, format: false, optional: false,isnumber:false },
    { currentproperty: 'Plan Type', mappedproperty: 'Ins_Type', visible: true, slidervalue: -1, isslider: false, format: false, optional: false ,isnumber:false},
    { currentproperty: 'Mkt Value', mappedproperty: 'Market_Value', visible: true, slidervalue: -1, isslider: false, format: true, optional: false ,isnumber:true},
    { currentproperty: 'Disc Mkt Value', mappedproperty: 'Disc_Market_Value', visible: true, slidervalue: -1, isslider: false, format: true ,isnumber:true},
    { currentproperty: 'Ins Value', mappedproperty: 'Ins_Value', visible: true, slidervalue: -1, isslider: false, format: true ,isnumber:true},
    { currentproperty: 'Disc Ins Value', mappedproperty: 'Disc_Ins_Value', visible: true, slidervalue: -1, isslider: false, format: true, optional: false,isnumber:true },
    { currentproperty: 'Acres', mappedproperty: 'Acres', visible: false, slidervalue: -1, isslider: false, format: false, optional: true ,isnumber:true},
    { currentproperty: 'Prod %', mappedproperty: 'Prod_Perc', visible: false, slidervalue: -1, isslider: false, format: false, optional: true ,isnumber:true},
    { currentproperty: 'Ins %', mappedproperty: 'Perc_Ins', visible: false, slidervalue: -1, isslider: false, format: false, optional: true ,isnumber:true},
    { currentproperty: 'CEI', mappedproperty: 'CEI', visible: false, slidervalue: -1, isslider: false, format: true, optional: true ,isnumber:true},
    { currentproperty: 'CF', mappedproperty: 'CF', visible: false, slidervalue: -1, isslider: false, format: true, optional: true ,isnumber:true},
    { currentproperty: 'RC', mappedproperty: 'RC', visible: false, slidervalue: -1, isslider: false, format: true, optional: true ,isnumber:true},
    { currentproperty: 'Margin', mappedproperty: 'Margin', visible: false, slidervalue: -1, isslider: false, format: true, optional: true ,isnumber:true},
    { currentproperty: 'ARM Budget', mappedproperty: 'Arm_Budget', visible: false, slidervalue: -1, isslider: false, format: true, optional: true ,isnumber:true},
    { currentproperty: 'Dist Budget', mappedproperty: 'Dist_Budget', visible: false, slidervalue: -1, isslider: false, format: true, optional: true ,isnumber:true},
    { currentproperty: '3rd Party', mappedproperty: 'Thirparty_Budget', visible: false, slidervalue: -1, isslider: false, format: true, optional: true ,isnumber:true},
  ]


  normalizevalue(amount) {
    if (isNaN(amount) || amount == null || amount == "") {
      return 0;
    } else {
      return amount;
    }
  }

  getcropname(Code){
    try {
      return this.refdata.CropList.find(p=>p.Crop_Code==Code).Crop_Name;
    } catch {
      return '';
    }
  }

  getvalueforprodpercbycounty(countyid)
  {
    let totalvalue=0;
    let totalacres=0;
    this.localloanobj.Farms.filter(p=>p.Farm_County_ID==countyid).map(p=>{
      totalvalue=totalvalue+p.Percent_Prod *p.FC_Total_Acres;
      totalacres=totalacres+p.FC_Total_Acres;
    });
    return (totalvalue/totalacres).toFixed(1);

  }

  getvalueforinspercbycounty(countyid)
  {
    let totalvalue=0;
    let totalacres=0;
    this.localloanobj.Farms.filter(p=>p.Farm_County_ID==countyid).map(p=>{
      totalvalue=totalvalue+((p.Permission_To_Insure ? 100 : p.Percent_Prod) *p.FC_Total_Acres);
      totalacres=totalacres+p.FC_Total_Acres;
    });
    return (totalvalue/totalacres).toFixed(1);
  }

  getvalueforprodpercbystate(stateid)
  {
    let totalvalue=0;
    let totalacres=0;
    this.localloanobj.Farms.filter(p=>p.Farm_State_ID==stateid).map(p=>{
      totalvalue=totalvalue+p.Percent_Prod *p.FC_Total_Acres;
      totalacres=totalacres+p.FC_Total_Acres;
    });
    return (totalvalue/totalacres).toFixed(1);
  }

  getvalueforinspercbystate(stateid)
  {
    let totalvalue=0;
    let totalacres=0;
    this.localloanobj.Farms.filter(p=>p.Farm_State_ID==stateid).map(p=>{
      totalvalue=totalvalue+((p.Permission_To_Insure ? 100 : p.Percent_Prod) *p.FC_Total_Acres);
      totalacres=totalacres+
      p.FC_Total_Acres;
    });
    return (totalvalue/totalacres).toFixed(1);
  }
}


