import { Component, OnInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';
import CollteralSettings from './collateral-types.model';
import { Page } from '@lenda/models/page.enum';
import { Chevron } from '@lenda/models/loan-response.model';
import { loan_model } from '@lenda/models/loanmodel';

import { FsaService } from './fsa/fsa.service';
import { CollateralService } from './collateral.service';
import { PublishService } from '@lenda/services/publish.service';
import { CollateralReportAggregateService } from './collateral-report-aggregate/collateral-report-aggregate.service';
import { MasterService } from '@lenda/master/master.service';

@Component({
  selector: 'app-collateral',
  templateUrl: './collateral.component.html',
  styleUrls: ['./collateral.component.scss'],
  providers: [FsaService, CollateralService, CollateralReportAggregateService]
})

export class CollateralComponent implements OnInit {
  public currentPageName: string = Page.collateral;
  public expanded: boolean = true;

  public categories: any = [];
  public loanFullID: string;
  public collateralRows: any = {};
  public isSyncEnabled: boolean = false;
  public Chevron: typeof Chevron = Chevron;

  // Chevrons Open Flags
  public isGurantorOpen: boolean = false;
  public isBorrowerOpen: boolean = false;
  public isLineHolderOpen: boolean = false;
  public isSBIOpen: boolean = false;

  public localloanobject: loan_model = new loan_model();

  // Default settings of collateral tables
  public pageSettings = {
    lineHolder: {
      isActive: true,
      isDisabled: true
    },
    borrower: {
      isActive: true,
      isDisabled: true
    },
    questions: {
      isActive: true,
      isDisabled: false
    },
    buyer: {
      isActive: true,
      isDisabled: true
    },
    guarantor: {
      isActive: true,
      isDisabled: true
    },
    sbi: {
      isActive: true,
      isDisabled: true
    },
    collaterlData: {
      isActive: true,
      isDisabled: true
    },
    fsa: {
      isActive: true,
      isDisabled: true
    },
    livestock: {
      isActive: false,
      isDisabled: false
    },
    storedCrop: {
      isActive: false,
      isDisabled: false
    },
    equipment: {
      isActive: false,
      isDisabled: false
    },
    realestate: {
      isActive: false,
      isDisabled: false
    },
    other: {
      isActive: false,
      isDisabled: false
    },
    contacts: {
      isActive: true,
      isDisabled: true
    },
    crossCollateral: {
      isActive: true,
      isDisabled: true
    }
  };

  constructor(
    private localstorageservice: LocalStorageService,
    public collateralService: CollateralService,
    public publishService: PublishService,
    public masterSvc: MasterService
  ) {
    this.loanFullID = this.localstorageservice.retrieve(environment.loanidkey);
  }

  ngOnInit() {
    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);
    if(this.localloanobject && this.localloanobject !== null){
      this.pageSettings = this.localstorageservice.retrieve(environment.collateralTables) || this.pageSettings;
      this.getCollateralTypeRows();
    }
  }


  onToggleSettings(item) {
    item.isActive = !item.isActive;
    this.localstorageservice.store(environment.collateralTables, this.pageSettings);
  }

  getCollateralTypeRows() {
    for (let index in CollteralSettings) {
      let item = CollteralSettings[index];
      this.collateralRows[index] = this.getRowsForType(item.key, item.source, item.sourceKey);
    }
  }

  getRowsForType(type: string, src: string, srcType: string) {
    let items = this.localstorageservice.retrieve(environment.loankey)[src];
    if (!items) {
      return 0;
    }
    if (!srcType) {
      return items.length;
    }
    return items
      .filter(lc => {
        return lc[srcType] === type
      }).length;
  }

  /**
   * Sync to database - publish button event
   */
  synctoDb() {
    this.publishService.syncCompleted();
    this.collateralService.syncToDb(this.localstorageservice.retrieve(environment.loankey));
  }
}


