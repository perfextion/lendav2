import {
  AssociationTypeCode,
  Collateral_Category_Code
} from '../../models/loanmodel';

export default {
  lineHolder: {
    key: AssociationTypeCode.LienHolder,
    source: 'Association',
    sourceKey: 'Assoc_Type_Code'
  },
  buyer: {
    key: AssociationTypeCode.Buyer,
    source: 'Association',
    sourceKey: 'Assoc_Type_Code'
  },
  guarantor: {
    key: AssociationTypeCode.Guarantor,
    source: 'Association',
    sourceKey: 'Assoc_Type_Code'
  },
  collaterlData: {
    key: 'TBD3',
    source: 'LoanCollateral',
    sourceKey: 'TBD'
  },
  fsa: {
    key: Collateral_Category_Code.FSA,
    source: 'LoanCollateral',
    sourceKey: 'Collateral_Category_Code',
    component: 'FSAComponent',
    pk: 'Collateral_ID',
    colKey: 'Collateral_Description'
  },
  livestock: {
    key: Collateral_Category_Code.Livestock,
    source: 'LoanCollateral',
    sourceKey: 'Collateral_Category_Code',
    component: 'LivestockComponent',
    pk: 'Collateral_ID',
    colKey: 'Collateral_Description'
  },
  storedCrop: {
    key: Collateral_Category_Code.StoredCrop,
    source: 'LoanCollateral',
    sourceKey: 'Collateral_Category_Code',
    component: 'StoredCropComponent',
    pk: 'Collateral_ID',
    colKey: 'Collateral_Description'
  },
  equipment: {
    key: Collateral_Category_Code.Equipemt,
    source: 'LoanCollateral',
    sourceKey: 'Collateral_Category_Code',
    component: 'EquipmentComponent',
    pk: 'Collateral_ID',
    colKey: 'Collateral_Description'
  },
  realestate: {
    key: Collateral_Category_Code.RealEState,
    source: 'LoanCollateral',
    sourceKey: 'Collateral_Category_Code',
    component: 'RealEstateComponent',
    pk: 'Collateral_ID',
    colKey: 'Collateral_Description'
  },
  other: {
    key: Collateral_Category_Code.Other,
    source: 'LoanCollateral',
    sourceKey: 'Collateral_Category_Code',
    component: 'OthersComponent',
    pk: 'Collateral_ID',
    colKey: 'Collateral_Description'
  },
  contacts: {
    key: 'TBD4',
    source: '',
    sourceKey: 'TBD'
  },
  marketingContracts: {
    key: '',
    source: 'LoanMarketingContracts',
    sourceKey: '',
    component: 'MarketingContractsComponent',
    pk: 'Contract_ID',
    colKey: 'Category'
  }
};
