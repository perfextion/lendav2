import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { GridOptions } from 'ag-grid';

import {
  RefVCAFieldsWorkerService,
  VCA_Field
} from '@lenda/Workers/calculations/refvcafieldsworker.service';
import { loan_model } from '@lenda/models/loanmodel';
import { DataService } from '@lenda/services/data.service';
import { environment } from '@env/environment.prod';
import {
  headerclassmaker,
  CellType,
  calculatecolumnwidths
} from '@lenda/aggriddefinations/aggridoptions';
import { creditvalueformatter } from '@lenda/aggridformatters/valueformatters';

@Component({
  selector: 'app-credit-agreement-fields',
  templateUrl: './credit-agreement-fields.component.html',
  styleUrls: ['./credit-agreement-fields.component.scss']
})
export class CreditAgreementFieldsComponent implements OnInit, OnDestroy {
  @Input() expanded: boolean = false;

  localloanobj: loan_model;

  private subscription: ISubscription;

  columnDefs: any[];
  rowData: Array<VCA_Field>;
  style: any = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };

  defaultColDef = {
    enableValue: true,
    enableRowGroup: true,
    enablePivot: true
  };

  gridApi: any;
  columnApi: any;
  gridOptions: GridOptions;
  context: any;
  frameworkcomponents: any;
  components: any;

  constructor(
    private localstorage: LocalStorageService,
    private vcaFields: RefVCAFieldsWorkerService,
    private dataservice: DataService
  ) {
    this.context = { componentParent: this };
    this.components = {};
    this.frameworkcomponents = {};
  }

  ngOnInit() {
    this.subscription = this.dataservice.getLoanObject().subscribe(res => {
      this.localloanobj = res;
      this.getdataforgrid();
    });

    this.localloanobj = this.localstorage.retrieve(environment.loankey);
    this.getdataforgrid();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  declareColumnDefs() {
    this.columnDefs = [
      {
        headerName: 'CA#',
        field: 'CAF_No',
        headerClass: headerclassmaker(CellType.Text),
        editable: false,
        minWidth: 120,
        width: 120,
        maxWidth: 120,
        cellClass: 'default-color'
      },
      {
        headerName: 'Field Name',
        field: 'Name',
        headerClass: headerclassmaker(CellType.Text),
        editable: false,
        minWidth: 200,
        width: 200,
        maxWidth: 200,
        cellClass: 'default-color'
      },
      {
        headerName: 'Value',
        field: 'Value',
        headerClass: headerclassmaker(CellType.Integer),
        editable: false,
        minWidth: 150,
        width: 150,
        maxWidth: 150,
        cellClass: 'default-color rightaligned',
        valueFormatter: creditvalueformatter
      }
    ];
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;

    this.adjustToFitAgGrid();
    this.getdataforgrid();
  }

  adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  getdataforgrid() {
    this.declareColumnDefs();
    this.rowData = this.vcaFields.Get_VCA_Fields_Data(this.localloanobj);
  }

  get hasData() {
    try {
      return (
        this.localloanobj.LoanMaster.CAF1 > 0  ||
        this.localloanobj.LoanMaster.CAF2 > 0  ||
        this.localloanobj.LoanMaster.CAF3 > 0  ||
        this.localloanobj.LoanMaster.CAF4 > 0
      )
    } catch {
      return false;
    }
  }
}
