import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { dollarformat, PracticeTranslator } from '@lenda/aggridformatters/valueformatters';
import { MatSlider, MatDialog, MatDialogConfig } from "@angular/material";
import { loan_model, LoanStatus, Collateral_Category_Code } from '@lenda/models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment';
import * as _ from 'lodash';
import { FSAComponent } from '../fsa/fsa.component';
import { ISubscription } from 'rxjs/Subscription';
import { DataService } from '@lenda/services/data.service';
import * as XLSX from 'xlsx';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { Loan_Key_Visibilty } from '@lenda/models/loansettings';
import { debug } from 'util';
import { lookupCropPracticeTypeWithRefData, lookupCropTypeWithRefData } from '@lenda/Workers/utility/aggrid/cropboxes';
import { MasterService } from '@lenda/master/master.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { PublishService } from '@lenda/services/publish.service';
import { Page } from '@lenda/models/page.enum';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { CollateralHelper } from '../collateral-helper.service';

@Component({
  selector: 'app-collateral-report-collateral',
  templateUrl: './collateral-report.component.html',
  styleUrls: ['./collateral-report.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CollateralReportSliderComponent implements OnInit {
  //>>>>>> SLIDER OPTIONS
  public exampleset = [1, 2, 3, 4, 5];
  @ViewChild('table') el: ElementRef;
  public sliderwidth: number = 0;
  max = 270;
  min = 0;
  step = 45;
  get tickInterval(): number | 'auto' {
    return this._tickInterval;
  }
  set tickInterval(v) {

    this._tickInterval = Number(v);
  }
  private _tickInterval = 45;
  //<<<<<<<<<<

  private localloanobj: loan_model;
  private loanObjBackup: string;
  private refdata: RefDataModel;
  private cropLIst: any;
  //>>>>>> GRID PROPERTIES
  rowData = [];
  columnDefs = [];
  tableColToDisplay: any = null;
  //<<<<<<<<
  //>>>>>> EVENTS

  sliderValue = 0;
  changed(event) {
    this.sliderValue = event.value;
    this.getgriddata();
  }

  isLoanEditable() {
    return !this.localloanobj || this.localloanobj.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  onBtnExport() {
    let exportData = JSON.parse(JSON.stringify(this.rowData));
    exportData.forEach(function (el) {
      delete el.Category_Code;
    });

    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(exportData)
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    ws[XLSX.utils.encode_col(0) + "1"].v = 'Category';
    ws[XLSX.utils.encode_col(1) + "1"].v = 'Crop';
    ws[XLSX.utils.encode_col(2) + "1"].v = 'Crop Type';
    ws[XLSX.utils.encode_col(3) + "1"].v = 'Irr Prac';
    ws[XLSX.utils.encode_col(4) + "1"].v = 'Crop Prac';
    ws[XLSX.utils.encode_col(5) + "1"].v = 'Ins Plan';
    ws[XLSX.utils.encode_col(6) + "1"].v = 'Plan Type';
    ws[XLSX.utils.encode_col(7) + "1"].v = 'Mkt Value';
    ws[XLSX.utils.encode_col(8) + "1"].v = 'Disc Mkt Val';
    ws[XLSX.utils.encode_col(9) + "1"].v = 'Ins Value';
    ws[XLSX.utils.encode_col(10) + "1"].v = 'Disc Ins Val';
    ws[XLSX.utils.encode_col(11) + "1"].v = 'CEI';

    XLSX.utils.book_append_sheet(wb, ws, 'Sheet 1');
    XLSX.writeFile(wb, `Collateral_${this.localloanobj.Loan_Full_ID}_${this.dtss.getTimeStamp()}.xlsx`);
  }

  // onGridReady(params) {
  //   this.gridApi = params.api;
  //   this.columnApi = params.columnApi;
  //   //these two methods remove side option toolbar , sets column , removes header menu
  //   setgriddefaults(this.gridApi, this.columnApi);
  //   params.api.sizeColumnsToFit();
  //   ///////////////////////////////////////////////////////////////////////////////////
  // }
  //<<<<<<<<

  @ViewChild(MatSlider) slider: MatSlider;
  private subscription: ISubscription;

  ngAfterViewInit() {
    //  this.slider.
  }
  constructor(
    private localstorageservice: LocalStorageService,
    private dtss: DatetTimeStampService,
    private dataService: DataService,
    private dialog: MatDialog,
    public masterSvc: MasterService,
    private loancalculation: LoancalculationWorker,
    private publishService: PublishService
  ) {
    this.localloanobj = this.localstorageservice.retrieve(environment.loankey);
    let tctd = this.localloanobj;

    if (!_.isEmpty(tctd)) {
      this.tableColToDisplay = JSON.parse(tctd.LoanMaster.Loan_Settings).Loan_key_Settings as Loan_Key_Visibilty;
      if(this.tableColToDisplay) {
        Object.keys(this.tableColToDisplay).forEach(keyname => {
          switch (keyname) {
            case 'Crop_Type':
              this.availablecolumns.find(p => p.currentproperty == 'Crop Type').visible = this.tableColToDisplay[keyname] == null ?
              false : this.tableColToDisplay[keyname];
              break;
            case 'Crop_Practice':
              this.availablecolumns.find(p => p.currentproperty == 'Crop Prac').visible = this.tableColToDisplay[keyname] == null ?
              false : this.tableColToDisplay[keyname];
              break;
            case 'Irr_Practice':
              this.availablecolumns.find(p => p.currentproperty == 'Irr Prac').visible = this.tableColToDisplay[keyname] == null ?
              false : this.tableColToDisplay[keyname];
              break;
          }
        });
      }
      //now load data

      let currentvalue = 0;
      this.availablecolumns.filter(p => p.visible == true && p.isslider == true).forEach(columns => {
        columns.slidervalue = currentvalue;
        currentvalue = currentvalue + 45;
      });

      this.localloanobj = this.localstorageservice.retrieve(environment.loankey);
      this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
      this.cropLIst = this.refdata.CropList;

      this.subscribeToChanges();
      this.getgriddata();

      setTimeout(() => {
        this.sliderwidthcalculator();
      }, 500);
    }
  }

  ngOnInit() {


  }

  private subscribeToChanges() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localloanobj = res;
      this.getgriddata();
      // setTimeout(()=>{
      //   setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), "Fsa_");
      // },5);
    });
  }

  sliderstyle = {
    width: '0px'
  }

  private CollateralCategories = CollateralHelper.CollateralCategories;

  @HostListener('window:resize', ['$event'])
  onResize() {

    this.sliderwidthcalculator();
  }

  private sliderwidthcalculator() {
    let sliderwidth = 0;
    this.availablecolumns.filter(p => p.visible == true && p.isslider == true).forEach((element, index) => {
      sliderwidth = sliderwidth + 45;

    });
    this.max = sliderwidth - 45;
    let length = this.max / 45;
    sliderwidth = 0;
    let e = (this.el.nativeElement as HTMLTableElement).rows[0].cells;
    Array.from(e).forEach((element, index) => {
      if (index < length)
        sliderwidth = sliderwidth + element.clientWidth;
    });
    this.sliderstyle.width = 60 + sliderwidth + "px";

  }

  getgriddata() {

    let isrefershneeded = true;
    if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Category").slidervalue) {
      this.rowData = [];
      // for (let i = 0; i < crops.length; i++) {
      let row: any = {};
      row.Category = "Crop"
      row.Crop_Name = "-";
      row.Crop_Type = "-"
      row.Crop_Practice = "-"
      row.Irr_Practice = '-';
      row.Ins_Plan = "-";
      row.Plan_Type = "-";
      // row.Market_Value_Pre ="$ "+ Math.round(_.sum(this.localloanobj.LoanCropUnits.map(p => p.Mkt_Value)));
      // row.Prior_Lien = "-";
      row.Settings = false;
      // row.is_Insured = "-";
      row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.map(p => p.Mkt_Value || 0))));
      row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.map(p => p.Disc_Mkt_Value || 0))));
      row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.map(p => p.Ins_Value || 0))));
      row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.map(p => p.Disc_Ins_value || 0))));
      row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.map(p => p.CEI_Value || 0))));
      this.rowData.push(row);
      // }
    }
    else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Crop").slidervalue) {
      this.rowData = [];
      //means crop level
      let crops = _.uniqBy(this.localloanobj.LoanCropUnits, "Crop_Code");
      for (let i = 0; i < crops.length; i++) {
        let row: any = {};
        row.Category = "Crop"
        row.Crop_Name = this.refdata.CropList.find(p => p.Crop_Code == crops[i].Crop_Code).Crop_Name;
        row.Crop_Type = "-"
        row.Crop_Practice = "-"
        row.Irr_Practice = '-';
        row.Ins_Plan = "-";
        row.Plan_Type = "-";
        row.Market_Value_Pre = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == crops[i].Crop_Code).map(p => p.Mkt_Value || 0 ))));
        row.Prior_Lien = "-";
        row.Settings = false;
        row.is_Insured = "-";
        row.Market_Value = row.Market_Value_Pre;
        row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == crops[i].Crop_Code).map(p => p.Disc_Mkt_Value|| 0 ))));
        row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == crops[i].Crop_Code).map(p => p.Ins_Value|| 0 ))));
        row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == crops[i].Crop_Code).map(p => p.Disc_Ins_value|| 0 ))));
        row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == crops[i].Crop_Code).map(p => p.CEI_Value|| 0 ))));
        if(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == crops[i].Crop_Code).map(p => p.CU_Acres|| 0 ))!=0)
        {
          this.rowData.push(row);
        }

      }
    }
    else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Crop Type").slidervalue) {
      this.rowData = [];
      //means crop level
      let cropGroups = _.groupBy(this.localloanobj.LoanCropUnits, "Crop_Code");
      for(let crop in cropGroups) {
        // crop type level
        let crops = _.uniqBy(cropGroups[crop], c => lookupCropTypeWithRefData(c.Crop_Practice_ID, this.refdata));
        for (let i = 0; i < crops.length; i++) {
          let crop_type = lookupCropTypeWithRefData(crops[i].Crop_Practice_ID, this.refdata);;
          let crop_units = this.localloanobj.LoanCropUnits.filter(p => p.Crop_Code == crops[i].Crop_Code && lookupCropTypeWithRefData(p.Crop_Practice_ID, this.refdata) == crop_type);

          let row: any = {};
          row.Category = "Crop"
          row.Crop_Name = this.refdata.CropList.find(p => p.Crop_Code == crops[i].Crop_Code).Crop_Name;
          row.Crop_Type = crop_type;
          row.Crop_Practice = "-"
          row.Irr_Practice = '-';
          row.Ins_Plan = "-";
          row.Plan_Type = "-";
          row.Market_Value_Pre = "$ " + this.normalizevalue(Math.round(_.sum(crop_units.map(p => p.Mkt_Value || 0))));
          row.Prior_Lien = "-";
          row.Settings = false;
          row.is_Insured = "-";
          row.Market_Value = row.Market_Value_Pre;
          row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(crop_units.map(p => p.Disc_Mkt_Value || 0))));
          row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(crop_units.map(p => p.Ins_Value || 0))));
          row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(crop_units.map(p => p.Disc_Ins_value || 0))));
          row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(crop_units.map(p => p.CEI_Value || 0))));
          if(_.sum(crop_units.map(p => p.CU_Acres || 0))!=0)
          {
            this.rowData.push(row);
          }

        }
      }
    } else if (this.availablecolumns.filter(p => p.currentproperty == "Crop Prac" || p.currentproperty == 'Irr Prac').map(p => p.slidervalue).includes(this.sliderValue)) {
      this.rowData = [];
      let cropandprac = _.uniqBy(this.localloanobj.LoanCropUnits, 'Crop_Practice_ID');
      for (let i = 0; i < cropandprac.length; i++) {
        let row: any = {};
        row.Category = "Crop"
        row.Crop_Name = this.refdata.CropList.find(p => p.Crop_Code == cropandprac[i].Crop_Code).Crop_Name;
        row.Crop_Type = lookupCropTypeWithRefData(cropandprac[i].Crop_Practice_ID, this.refdata);
        row.Crop_Practice = '-';
        if(this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Crop Prac").slidervalue) {
          row.Crop_Practice = lookupCropPracticeTypeWithRefData(cropandprac[i].Crop_Practice_ID, this.refdata);
        }
        row.Irr_Practice = PracticeTranslator(cropandprac[i].Crop_Practice_Type_Code);
        row.Ins_Plan = "-";
        row.Plan_Type = "-";
        row.Market_Value_Pre = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.Mkt_Value|| 0 ))));
        row.Prior_Lien = "-";
        row.Settings = false;
        row.is_Insured = "-";
        row.Market_Value = row.Market_Value_Pre;
        row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.Disc_Mkt_Value|| 0 ))));
        row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.Ins_Value|| 0 ))));
        row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.Disc_Ins_value|| 0 ))));
        row.CEI = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.CEI_Value|| 0 ))));
        if(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.CU_Acres|| 0 ))!=0)
        {
          this.rowData.push(row);
        }
        //this.rowData.push(row);
      }
    }
    else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Ins Plan").slidervalue) {

      this.rowData = [];
      let cropandprac =  _.uniqBy(this.localloanobj.LoanCropUnits, 'Crop_Practice_ID');

      for (let i = 0; i < cropandprac.length; i++) {

        let row: any = {};
        row.Category = "Crop"
        row.Crop_Name = this.refdata.CropList.find(p => p.Crop_Code == cropandprac[i].Crop_Code).Crop_Name;
        row.Crop_Type = lookupCropTypeWithRefData(cropandprac[i].Crop_Practice_ID, this.refdata);
        row.Crop_Practice = lookupCropPracticeTypeWithRefData(cropandprac[i].Crop_Practice_ID, this.refdata);
        row.Irr_Practice = PracticeTranslator(cropandprac[i].Crop_Practice_Type_Code);
        row.Ins_Plan = "-";
        row.Plan_Type = "-";
        row.Market_Value_Pre = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.Mkt_Value|| 0 ))));
        row.Prior_Lien = "-";
        row.Settings = false;
        row.is_Insured = "Yes";
        row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.Mkt_Value|| 0 ))))
        row.Disc_Market_Value ="$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.Disc_Mkt_Value|| 0 ))))
        row.Disc_Ins_Value = "-";
        // row.CEI = "$ "+Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.CEI_Value)));
        row.CEI = "$ " + Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.CEI_Value|| 0 )));
        if(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.CU_Acres|| 0 ))!=0)
        this.rowData.push(row);
        let Cropunitfulls=this.localloanobj.LoanCropUnits.filter(p=>p.Crop_Practice_ID==cropandprac[i].Crop_Practice_ID);
        let cropunits=Cropunitfulls.map(p=>p.Loan_CU_ID);
        let actualpolicies=this.localloanobj.CropUnitPolicies.filter(p=>cropunits.includes(p.Loan_Crop_Unit_ID)&& p.Status!=3);
        let CUs=_.uniqBy(actualpolicies, function(elem) { return elem.Ins_Plan_Code });
        if(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.CU_Acres|| 0 ))!=0)
        {
        CUs.forEach(element => {

          let rowMPCI: any = {};
          rowMPCI.Category = "Crop"
          rowMPCI.Crop_Name = this.refdata.CropList.find(p => p.Crop_Code == cropandprac[i].Crop_Code).Crop_Name;
          rowMPCI.Crop_Type = lookupCropTypeWithRefData(cropandprac[i].Crop_Practice_ID, this.refdata);
          rowMPCI.Crop_Practice = lookupCropPracticeTypeWithRefData(cropandprac[i].Crop_Practice_ID, this.refdata);
          rowMPCI.Irr_Practice = PracticeTranslator(cropandprac[i].Crop_Practice_Type_Code);
          rowMPCI.Ins_Plan = element.Ins_Plan_Code;
          rowMPCI.Plan_Type = "";
          rowMPCI.Market_Value_Pre = "-";
          rowMPCI.Prior_Lien = "-";
          rowMPCI.Settings = false;
          rowMPCI.is_Insured = "Yes";
          rowMPCI.Market_Value ='';
          rowMPCI.Disc_Market_Value ='';
          rowMPCI.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(actualpolicies.filter(z=>z.Ins_Plan_Code==element.Ins_Plan_Code).map(p=>p.Ins_Value))));
          rowMPCI.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(actualpolicies.filter(z=>z.Ins_Plan_Code==element.Ins_Plan_Code).map(p=>p.Disc_Ins_Value))));
          // row.CEI = "$ "+Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.CEI_Value)));
          rowMPCI.CEI = "-";
          this.rowData.push(rowMPCI);
          ////ENDS Plan

        });
        }
      }
    }
    else if (this.sliderValue == this.availablecolumns.find(p => p.currentproperty == "Plan Type").slidervalue) {
      this.rowData = [];
      let cropandprac =  _.uniqBy(this.localloanobj.LoanCropUnits, 'Crop_Practice_ID');

      for (let i = 0; i < cropandprac.length; i++) {
        let row: any = {};
        row.Category = "Crop"
        row.Crop_Name = this.refdata.CropList.find(p => p.Crop_Code == cropandprac[i].Crop_Code).Crop_Name;
        row.Crop_Type = lookupCropTypeWithRefData(cropandprac[i].Crop_Practice_ID, this.refdata);
        row.Crop_Practice = lookupCropPracticeTypeWithRefData(cropandprac[i].Crop_Practice_ID, this.refdata);
        row.Irr_Practice = PracticeTranslator(cropandprac[i].Crop_Practice_Type_Code);
        row.Ins_Plan = "-";
        row.Plan_Type = "-";
        row.Market_Value_Pre = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.Mkt_Value|| 0 ))));
        row.Prior_Lien = "-";
        row.Settings = false;
        row.is_Insured = "Yes";
        row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.Mkt_Value|| 0 ))))
        row.Disc_Market_Value ="$ " + this.normalizevalue(Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.Disc_Mkt_Value|| 0 ))))
        row.Disc_Ins_Value = "-";
        // row.CEI = "$ "+Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.CEI_Value)));
        row.CEI = "$ " + Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.CEI_Value|| 0 )));
        if(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.CU_Acres|| 0 ))!=0)
        this.rowData.push(row);
        let Cropunitfulls=this.localloanobj.LoanCropUnits.filter(p=>p.Crop_Practice_ID==cropandprac[i].Crop_Practice_ID);
        let cropunits=Cropunitfulls.map(p=>p.Loan_CU_ID);
        let actualpolicies=this.localloanobj.CropUnitPolicies.filter(p=>cropunits.includes(p.Loan_Crop_Unit_ID)&& p.Status!=3);
        let CUs=_.uniqBy(actualpolicies, function(elem) { return [elem.Ins_Plan_Code,elem.Ins_Plan_Type_Code].join() });
        if(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.CU_Acres|| 0 ))!=0)
        {
        CUs.forEach(element => {

          let rowMPCI: any = {};
          rowMPCI.Category = "Crop"
          rowMPCI.Crop_Name = this.refdata.CropList.find(p => p.Crop_Code == cropandprac[i].Crop_Code).Crop_Name;
          rowMPCI.Crop_Type = lookupCropTypeWithRefData(cropandprac[i].Crop_Practice_ID, this.refdata);
          rowMPCI.Crop_Practice = lookupCropPracticeTypeWithRefData(cropandprac[i].Crop_Practice_ID, this.refdata);
          rowMPCI.Irr_Practice = PracticeTranslator(cropandprac[i].Crop_Practice_Type_Code);
          rowMPCI.Ins_Plan = element.Ins_Plan_Code;
          rowMPCI.Plan_Type = element.Ins_Plan_Type_Code;
          rowMPCI.Market_Value_Pre = "-";
          rowMPCI.Prior_Lien = "-";
          rowMPCI.Settings = false;
          rowMPCI.is_Insured = "Yes";
          rowMPCI.Market_Value ='';
          rowMPCI.Disc_Market_Value ='';
          rowMPCI.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(actualpolicies.filter(z=>z.Ins_Plan_Code==element.Ins_Plan_Code && z.Ins_Plan_Type_Code==element.Ins_Plan_Type_Code).map(p=>p.Ins_Value))));
          rowMPCI.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(actualpolicies.filter(z=>z.Ins_Plan_Code==element.Ins_Plan_Code && z.Ins_Plan_Type_Code==element.Ins_Plan_Type_Code).map(p=>p.Disc_Ins_Value))));
          // row.CEI = "$ "+Math.round(_.sum(this.localloanobj.LoanCropUnits.filter(p => p.Crop_Practice_ID == cropandprac[i].Crop_Practice_ID).map(p => p.CEI_Value)));
          rowMPCI.CEI = "-";
          this.rowData.push(rowMPCI);
          ////ENDS Plan

        });
        }
      }
    }
    else {
      //this.rowData = [];
      isrefershneeded = false;
    }

    if (isrefershneeded) {

      //Wfrp here
      if (this.localloanobj.Loanwfrp.Wfrp_Enabled==true) {
        let row: any = {};
        row.Category = "Crop"
        row.Crop_Name = "All";
        row.Crop_Type = "-"
        row.Crop_Practice = "-"
        row.Ins_Plan = "WFRP";
        row.Plan_Type = "-";
        row.Settings = false;
        // row.Market_Value_Pre ="$ "+ Math.round(foundrecords[s].Market_Value);
        // row.Prior_Lien = "$ "+ Math.round(foundrecords[s].Prior_Lien_Amount);
        // row.is_Insured = Math.round(foundrecords[s].Insured_Flag)==1?"Yes":"No";
        row.Market_Value = "$ " +this.normalizevalue( Math.round((this.localloanobj.Loanwfrp.Wfrp_Value)));
        row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round((this.localloanobj.Loanwfrp.Wfrp_Disc_Value)));
        row.Ins_Value = "$ " + this.normalizevalue(Math.round((this.localloanobj.Loanwfrp.Wfrp_Ins_Value)));
        row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round((this.localloanobj.Loanwfrp.Wfrp_Disc_Ins_Value)));
        row.CEI ="$ " +(this.localloanobj.Loanwfrp.FC_Wfrp_CEI);
        this.rowData.push(row);
      }

      //Collaterals at the End
      let collaterals = this.localloanobj.LoanCollateral;
      for (let f = 0; f < this.CollateralCategories.length; f++) {
        let foundrecords = collaterals.filter(p => p.Collateral_Category_Code == this.CollateralCategories[f] && p.ActionStatus != 3);
        if (foundrecords.length > 0) {
          // for(let s=0;s<foundrecords.length;s++)
          // {
          let row: any = {};
          row.Category_Code = this.CollateralCategories[f];
          row.Category = CollateralHelper.getCollateralAlisas(this.CollateralCategories[f]);
          row.Crop_Name = this.CollateralCategories[f] == Collateral_Category_Code.StoredCrop ? this.getCropName(foundrecords[0].Crop_Detail) : "-";


          row.Crop_Type = "-"
          row.Crop_Practice = "-"
          row.Ins_Plan = "-";
          row.Plan_Type = "-";
          row.Settings = true;
          // row.Market_Value_Pre ="$ "+ Math.round(foundrecords[s].Market_Value);
          // row.Prior_Lien = "$ "+ Math.round(foundrecords[s].Prior_Lien_Amount);
          // row.is_Insured = Math.round(foundrecords[s].Insured_Flag)==1?"Yes":"No";
          let values = foundrecords.map(p => p.Market_Value);
          //Display net Marketing Value
          row.Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(foundrecords.map(p => Number(p.Net_Market_Value)))));
          row.Disc_Market_Value = "$ " + this.normalizevalue(Math.round(_.sum(foundrecords.map(p => Number(p.Disc_Mkt_Value)))));
          row.Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(foundrecords.map(p => Number(p.Ins_Value)))));
          row.Disc_Ins_Value = "$ " + this.normalizevalue(Math.round(_.sum(foundrecords.map(p => Number(p.Disc_Ins_Value)))));
          let cievalue = this.normalizevalue((Math.round(_.sum(foundrecords.map(p => Number(p.Disc_Mkt_Value)))) - Math.round(_.sum(foundrecords.map(p => Number(p.Disc_Ins_Value))))));
          //CEI is Only for crop
          row.CEI = "-";
          this.rowData.push(row);
          // }
        } else {
          //default Row
          let row: any = {};
          row.Category_Code = this.CollateralCategories[f];
          row.Category = CollateralHelper.getCollateralAlisas(this.CollateralCategories[f]);
          row.Crop_Name = "-";
          row.Crop_Type = "-"
          row.Settings = true;
          row.Crop_Practice = "-"
          row.Irr_Practice = "-"
          row.Ins_Plan = "-";
          row.Plan_Type = "-";
          row.Market_Value_Pre = "-";
          row.Prior_Lien = "-";
          row.is_Insured = "No";
          row.Market_Value = row.Market_Value_Pre;
          row.Disc_Market_Value = "-";
          row.Ins_Value = "-";
          row.Disc_Ins_Value = "-";
          row.CEI = "-";
          this.rowData.push(row);
        }
      }
      this.generateTotals();
      // let cropdata=
    }
  }

  getCropName(crop_code: string) {
    if(this.refdata) {
      let crop = this.refdata.CropList.find(A => A.Crop_Code == crop_code);
      if(crop) {
        return crop.Crop_Name;
      } else {
        return '-';
      }
    }
    return '-';
  }

  getsubpolicyalias(alias: string) {
    switch (alias.toLowerCase()) {
      case "hmax":
        return "Hmax"
      case "ramp":
        return "Ramp"
      case "stax":
        return "Stax"
      case "sco":
        return "Sco"
      case "ice":
        return "Ice"
      case "abc":
        return "Abc"
      case "pci":
        return "Pci"
      case "crophail":
        return "Crophail"
      default:
        break;
    }
  }
  generateTotals() {

    let row: any = {};
    row.Category = "Totals";
    row.Crop_Name = "";
    row.Crop_Type = ""
    row.Settings = false;
    row.Crop_Practice = ""
    row.Ins_Plan = "";
    row.Plan_Type = "";
    // row.Market_Value_Pre = '$ '+_.sum(this.rowData.filter(p=>p.Market_Value_Pre!="-").map(p=>Number(p.Market_Value_Pre.replace('$ ',''))));
    // row.Prior_Lien ='$ '+_.sum(this.rowData.filter(p=>p.Prior_Lien!="-").map(p=>Number(p.Prior_Lien.replace('$ ',''))));
    // row.is_Insured = "";
    row.Market_Value = '$ ' + _.sum(this.rowData.filter(p => p.Market_Value_Pre != "-").map(p => Number(p.Market_Value.replace('$ ', ''))));



    row.Disc_Market_Value = '$ ' + _.sum(this.rowData.filter(p => p.Disc_Market_Value != "-").map(p => Number(p.Disc_Market_Value.replace('$ ', ''))));
    row.Ins_Value = '$ ' + _.sum(this.rowData.filter(p => p.Ins_Value != "-").map(p => Number((p.Ins_Value||'$ 0').replace('$ ', ''))));
    row.Disc_Ins_Value = '$ ' + _.sum(this.rowData.filter(p => p.Disc_Ins_Value != "-").map(p => Number(p.Disc_Ins_Value.replace('$ ', ''))));
    row.CEI = '$ ' + _.sum(this.rowData.filter(p => p.CEI != "-").map(p => Number(p.CEI.replace('$ ', ''))));
    this.rowData.push(row);
  }

  hasValue(code: string) {
    let rows = this.localloanobj.LoanCollateral.filter(
      a =>
        a.Collateral_Category_Code == code &&
        a.ActionStatus != 3 &&
        (a.Net_Market_Value > 0 || a.Market_Value > 0)
    ).length;
    return rows > 0;
  }


  formatamounts(amount, parse: boolean) {
    if (!parse) {
      if (amount != '-') {

        if(amount=="$ NaN" || amount==null || amount=="")
        {
          return "-";
        }
        else
        {
        amount = amount.replace('$ ', '');
        let value = dollarformat(parseFloat(amount), '', 0)
         if(value=="$0")
         {
          return "";
         }
         else
        return value;
        }
      }
      else {
        return '-';
      }
    }
    else {
      return amount;
    }
  }

  /**
   * Used to open the FSA Component in a modal.
   * Passes the selected category code to FSA component.
   * @param categoryCode {string} The Collateral Category Code Selected.
   */
  open(categoryCode: string) {

    FSAComponent.SELECTED_CAT_CODE = categoryCode;
    this.loanObjBackup = JSON.stringify(this.localloanobj);

    let ref = this.dialog.open(FSAComponent, {
      disableClose: true,
      autoFocus: false,
      panelClass: 'fsa-dialog'
    });

    ref.componentInstance.title = CollateralHelper.getCollateralAlisas(categoryCode) || '' + ' Collateral Detail';
    ref.componentInstance.localloanobject = this.localloanobj;
    ref.componentInstance.refdata = this.refdata;

    ref.afterClosed().subscribe(res => {
      if(this.isLoanEditable()) {
        if(!res) {
          let loan = JSON.parse(this.loanObjBackup) as loan_model;
          this.loancalculation.performcalculationonloanobject(loan, true);
        } else {
          this.publishService.enableSync(Page.collateral);
        }
      }
    });
  }

  getCropDetailLabel(Crop_Code) {
    let crop = this.cropLIst.find(data => data.Crop_Code == Crop_Code);
    return crop ? crop.Crop_Name : '';
  }


  //this is the list which will generate the columns and set vivbilty and slider value
  availablecolumns = [
    { currentproperty: 'Category', mappedproperty: 'Category', visible: true, slidervalue: 0, isslider: true,format:false },
    { currentproperty: 'Crop', mappedproperty: 'Crop_Name', visible: true, slidervalue: 0, isslider: true ,format:false},
    { currentproperty: 'Crop Type', mappedproperty: 'Crop_Type', visible: true, slidervalue: 0, isslider: true,format:false },
    { currentproperty: 'Irr Prac', mappedproperty: 'Irr_Practice', visible: true, slidervalue: 0, isslider: true ,format:false},
    { currentproperty: 'Crop Prac', mappedproperty: 'Crop_Practice', visible: true, slidervalue: 0, isslider: true ,format:false},
    { currentproperty: 'Ins Plan', mappedproperty: 'Ins_Plan', visible: true, slidervalue: 0, isslider: true ,format:false},
    { currentproperty: 'Plan Type', mappedproperty: 'Plan_Type', visible: true, slidervalue: 0, isslider: true ,format:false},
    { currentproperty: 'Mkt Value', mappedproperty: 'Market_Value', visible: true, slidervalue: -1, isslider: false ,format:true},
    { currentproperty: 'Disc Mkt Val', mappedproperty: 'Disc_Market_Value', visible: true, slidervalue: -1, isslider: false,format:true },
    { currentproperty: 'Ins Value', mappedproperty: 'Ins_Value', visible: true, slidervalue: -1, isslider: false ,format:true},
    { currentproperty: 'Disc Ins Val', mappedproperty: 'Disc_Ins_Value', visible: true, slidervalue: -1, isslider: false,format:true },
    { currentproperty: 'CEI', mappedproperty: 'CEI', visible: true, slidervalue: -1, isslider: false ,format:true}

  ]

  normalizevalue(amount)
  {
    if(isNaN(amount) || amount==null || amount=="")
    {
      return 0;
    }
    else
    return amount;
  }
}

export class propertyChild {
  currentproperty: string
  mappedproperty: string;
  visible: boolean;
  slidervalue: number;
}
