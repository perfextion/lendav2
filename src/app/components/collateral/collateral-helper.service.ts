import { Collateral_Category_Code } from '@lenda/models/loanmodel';

export class CollateralHelper {
  public static readonly CollateralCategories = [
    Collateral_Category_Code.FSA,
    Collateral_Category_Code.Livestock,
    Collateral_Category_Code.StoredCrop,
    Collateral_Category_Code.Equipemt,
    Collateral_Category_Code.RealEState,
    Collateral_Category_Code.Other
  ];

  public static readonly CollateralItems = [
    { value: 'FSA', key: Collateral_Category_Code.FSA },
    { value: 'Livestock', key: Collateral_Category_Code.Livestock },
    { value: 'Stored Crop', key: Collateral_Category_Code.StoredCrop },
    { value: 'Equipment', key: Collateral_Category_Code.Equipemt },
    { value: 'Real Estate', key: Collateral_Category_Code.RealEState },
    { value: 'Other', key: Collateral_Category_Code.Other }
  ];

  public static getCollateralAlisas(code: string) {
    let CollateralItem = CollateralHelper.CollateralItems.find(
      a => a.key == code
    );

    if (CollateralItem) {
      return CollateralItem.value;
    }
    return code;
  }
}
