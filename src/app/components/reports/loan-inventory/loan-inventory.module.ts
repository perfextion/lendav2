import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';
import { ReportsSharedModule } from '../shared/reports-shared.module';

import { LoanInventoryComponent } from './loan-inventory.component';
import { InventoryBorrowerComponent } from './borrower/borrower.component';
import { InventoryBudgetComponent } from './budget/budget.component';
import { InventoryCollateralComponent } from './collateral/collateral.component';
import { InventoryCommitteeComponent } from './committee/committee.component';
import { InventoryConditionsComponent } from './conditions/conditions.component';
import { InventoryCropComponent } from './crop/crop.component';
import { InventoryFarmComponent } from './farm/farm.component';
import { InventoryInsuranceComponent } from './insurance/insurance.component';
import { InventoryOptimizerComponent } from './optimizer/optimizer.component';

@NgModule({
  imports: [CommonModule, SharedModule, MaterialModule, UiComponentsModule, ReportsSharedModule],
  declarations: [
    LoanInventoryComponent,
    InventoryBorrowerComponent,
    InventoryBudgetComponent,
    InventoryCollateralComponent,
    InventoryCommitteeComponent,
    InventoryConditionsComponent,
    InventoryCropComponent,
    InventoryFarmComponent,
    InventoryInsuranceComponent,
    InventoryOptimizerComponent
  ],
  exports: [LoanInventoryComponent]
})
export class LoanInventoryReportsModule {}
