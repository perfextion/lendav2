import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { TableStatus } from '../loan-inventory.model';

import { DataService } from '@lenda/services/data.service';
import { LoanInventoryService } from '../loan-inventory.service';

@Component({
  selector: 'app-inventory-borrower',
  templateUrl: './borrower.component.html',
  styleUrls: ['./borrower.component.scss']
})
export class InventoryBorrowerComponent implements OnInit, OnDestroy {
  private loanData: loan_model = new loan_model();

  public TableStatus: typeof TableStatus = TableStatus;
  public borrowerInv: any;

  @Input() public showDetail = false;
  @Input() public ID: string = '';
  @Input() public expanded = true;

  private subscription: ISubscription;

  constructor(
    public localstorageservice: LocalStorageService,
    private loanInventoryService: LoanInventoryService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.loanData = this.localstorageservice.retrieve(environment.loankey);
    this.borrowerInv = this.loanInventoryService.prepareBorrowerInventory(
      this.loanData
    );

    this.subscription = this.dataService.getLoanObject().subscribe(loan => {
      this.loanData = loan;
      this.borrowerInv = this.loanInventoryService.prepareBorrowerInventory(
        this.loanData
      );
    });
  }

  public prepareStatus(status: number) {
    return status == 0 ? 'Need Information' : 'Completed';
  }

  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
