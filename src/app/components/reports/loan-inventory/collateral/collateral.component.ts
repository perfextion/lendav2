import { Component, OnInit, Input } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { TableStatus } from '../loan-inventory.model';

import { LoanInventoryService } from '../loan-inventory.service';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-inventory-collateral',
  templateUrl: './collateral.component.html',
  styleUrls: ['./collateral.component.scss']
})
export class InventoryCollateralComponent implements OnInit {
  private loanData: loan_model = new loan_model();

  @Input() public showDetail = false;
  @Input() public ID: string = '';
  @Input() public expanded = true;

  public TableStatus: typeof TableStatus = TableStatus;
  public colInv: any;

  private subscription: ISubscription;

  constructor(
    private localstorageservice: LocalStorageService,
    private loanInventoryService: LoanInventoryService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.loanData = this.localstorageservice.retrieve(environment.loankey);

    this.colInv = this.loanInventoryService.prepareCollateralInventory(
      this.loanData
    );

    this.subscription = this.dataService.getLoanObject().subscribe(loan => {
      this.loanData = loan;
      this.colInv = this.loanInventoryService.prepareCollateralInventory(
        this.loanData
      );
    });
  }

  prepareStatus(status: number) {
    return status == 0 ? 'Need Information' : 'Completed';
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
