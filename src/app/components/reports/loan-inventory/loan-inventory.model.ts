import { Loan_Condition } from '@lenda/models/loanmodel';
import { Loan_Document } from '@lenda/models/loan/loan_document';

export enum TableStatus {
  NeedInformation = ' Need Information',
  Completed = 'Completed'
}

export class LoanInventory {
  borrower = new BorrowerInventory();
  budget = new BudgetInventory();
  collateral = new CollateralInventory();
  committee = new CommitteeInventory();
  conditions = new LoanDocumentsInventory();
  crop = new CropInventory();
  farm = new FarmInventory();
  insurance = new InsuranceInventory();
  optimizer = new OptimizerInventory();
}

export class BorrowerInventory {
  isAnyIncomplete: boolean = true;
}

export class BudgetInventory {
  isAnyIncomplete: boolean = true;

  distributerTableTotalCells: number = 0;
  distributerTableFilledCells: number = 0;
  thirdPartyTableTotalCells: number = 0;
  thirdPartyTableFilledCells: number = 0;
  harvesterTableTotalCells: number = 0;
  harvesterTableFilledCells: number = 0;
  equipmentyTableTotalCells: number = 0;
  equipmentTableFilledCells: number = 0;
  totalBudgetTableTotalCells: number = 0;
  totalBudgetTableFilledCells: number = 0;
  fixedExpensesTableTotalCells: number = 0;
  fixedExpensesTableFilledCells: number = 0;

  distributerTableStatus: TableStatus = TableStatus.NeedInformation;
  thirdPartyTableStatus: TableStatus = TableStatus.NeedInformation;
  harvesterTableStatus: TableStatus = TableStatus.NeedInformation;
  equipmentyTableStatus: TableStatus = TableStatus.NeedInformation;
  totalBudgetTableStatus: TableStatus = TableStatus.NeedInformation;
  fixedExpensesTableStatus: TableStatus = TableStatus.NeedInformation;

  cropPracticeRows: Array<TableStatusRow> = [];
}

export class CollateralInventory {
  isAnyIncomplete: boolean = true;

  lienholderTableTotalCells: number = 0;
  lienholderTableFilledCells: number = 0;
  buyerTableTotalCells: number = 0;
  buyerTableFilledCells: number = 0;
  guarantorTableTotalCells: number = 0;
  guarantorTableFilledCells: number = 0;
  cropCollateralTableTotalCells: number = 0;
  cropCollateralTableFilledCells: number = 0;
  additionalCollateralTableTotalCells: number = 0;
  additionalCollateralTableFilledCells: number = 0;
  marketingContractsTableTotalCells: number = 0;
  marketingContractsTableFilledCells: number = 0;
  crossCollateralTableTotalCells: number = 0;
  crossCollateralTableFilledCells: number = 0;

  lienholderTableStatus: TableStatus = TableStatus.NeedInformation;
  buyerTableStatus: TableStatus = TableStatus.NeedInformation;
  guarantorTableStatus: TableStatus = TableStatus.NeedInformation;
  cropCollateralTableStatus: TableStatus = TableStatus.NeedInformation;
  additionalCollateralTableStatus: TableStatus = TableStatus.NeedInformation;
  marketingContractsTableStatus: TableStatus = TableStatus.NeedInformation;
  crossCollateralTableStatus: TableStatus = TableStatus.NeedInformation;
}

export class CommitteeInventory {
  isAnyIncomplete: boolean = true;

  termsTableTotalCells: number = 0;
  termsTableFilledCells: number = 0;
  exceptionsTableTotalCells: number = 0;
  exceptionsTableFilledCells: number = 0;
  loanCommentsTableTotalCells: number = 0;
  loanCommentsTableFilledCells: number = 0;

  termsTableStatus: TableStatus = TableStatus.NeedInformation;
  exceptionsTableStatus: TableStatus = TableStatus.NeedInformation;
  loanCommentsTableStatus: TableStatus = TableStatus.NeedInformation;
}

export class LoanDocumentsInventory {
  isAnyIncomplete: boolean = true;

  loanUnderWritingDocuments: Array<Loan_Document> = [];
  loanPreReqDocuments: Array<Loan_Document> = [];
  loanClosingDocuments: Array<Loan_Document> = [];
  cropMonitoringDocuments: Array<Loan_Document> = [];

  loanUnderWritingTotalDocuments: number = 0;
  loanUnderWritingFilledDocuments: number = 0;

  loanPreReqTotalDocuments: number = 0;
  loanPreReqFilledDocuments: number = 0;

  loanClosingTotalDocuments: number = 0;
  loanClosingFilledDocuments: number = 0;

  cropMonitoringTotalDocuments: number = 0;
  cropMonitoringFilledDocuments: number = 0;
}

export class CropInventory {
  isAnyIncomplete: boolean = true;

  rebatorTableTotalCells: number = 0;
  rebatorTableFilledCells: number = 0;
  yieldTableTotalCells: number = 0;
  yieldTableFilledCells: number = 0;
  priceTableTotalCells: number = 0;
  priceTableFilledCells: number = 0;
  otherIncomeTableTotalCells: number = 0;
  otherIncomeTableFilledCells: number = 0;

  rebatorTableStatus: TableStatus = TableStatus.NeedInformation;
  yieldTableStatus: TableStatus = TableStatus.NeedInformation;
  priceTableStatus: TableStatus = TableStatus.NeedInformation;
  otherIncomeTableStatus: TableStatus = TableStatus.NeedInformation;
}

export class FarmInventory {
  isAnyIncomplete: boolean = true;

  landlordTableTotalCells: number = 0;
  landlordTableFilledCells: number = 0;
  farmTableTotalCells: number = 0;
  farmTableFilledCells: number = 0;

  landlordTableStatus: TableStatus = TableStatus.NeedInformation;
  farmTableStatus: TableStatus = TableStatus.NeedInformation;
}

export class InsuranceInventory {
  isAnyIncomplete: boolean = true;

  agentTableTotalCells: number = 0;
  agentTableFilledCells: number = 0;
  agencyTableTotalCells: number = 0;
  agencyTableFilledCells: number = 0;
  aipTableTotalCells: number = 0;
  aipTableFilledCells: number = 0;
  policyTableTotalCells: number = 0;
  policyTableFilledCells: number = 0;
  wfrpTableTotalCells: number = 0;
  wfrpTableFilledCells: number = 0;
  aphTableTotalCells: number = 0;
  aphTableFilledCells: number = 0;

  agentTableStatus: TableStatus = TableStatus.NeedInformation;
  agencyTableStatus: TableStatus = TableStatus.NeedInformation;
  aipTableStatus: TableStatus = TableStatus.NeedInformation;
  policyTableStatus: TableStatus = TableStatus.NeedInformation;
  wfrpTableStatus: TableStatus = TableStatus.NeedInformation;
  aphTableStatus: TableStatus = TableStatus.NeedInformation;
}

export class OptimizerInventory {
  isAnyIncomplete: boolean = true;

  optimizerIRRTableTotalCells: number = 0;
  optimizerIRRTableFilledCells: number = 0;
  optimizerNIRTableTotalCells: number = 0;
  optimizerNIRableFilledCells: number = 0;

  optimizerIRRTableStatus: TableStatus = TableStatus.NeedInformation;
  optimizerNIRableStatus: TableStatus = TableStatus.NeedInformation;
}

export class TableStatusRow {
  crop: string = '';
  practiceType: string = '';
  filledFields: number = 0;
  totalFields: number = 0;
  status: TableStatus = TableStatus.NeedInformation;
}
