import { Component, OnInit, Input } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { TableStatus } from '../loan-inventory.model';

import { DataService } from '@lenda/services/data.service';
import { LoanInventoryService } from '../loan-inventory.service';

@Component({
  selector: 'app-inventory-budget',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.scss']
})
export class InventoryBudgetComponent implements OnInit {
  private loanData: loan_model = new loan_model();

  @Input() public showDetail = false;
  @Input() public ID: string = '';
  @Input() public expanded = true;

  public TableStatus: typeof TableStatus = TableStatus;
  public budgetInv: any;

  private subscription: ISubscription;

  constructor(
    public localstorageservice: LocalStorageService,
    private loanInventoryService: LoanInventoryService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.loanData = this.localstorageservice.retrieve(environment.loankey);
    this.budgetInv = this.loanInventoryService.prepareBudgetInventory(
      this.loanData
    );

    this.subscription = this.dataService.getLoanObject().subscribe(loan => {
      this.loanData = loan;
      this.budgetInv = this.loanInventoryService.prepareBudgetInventory(
        this.loanData
      );
    });
  }

  prepareStatus(status: number) {
    return status == 0 ? 'Need Information' : 'Completed';
  }

  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
