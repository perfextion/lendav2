import { Injectable, EventEmitter } from '@angular/core';
import { loan_model } from '@lenda/models/loanmodel';
import * as _ from 'lodash';
import { TableStatus, LoanDocumentsInventory } from './loan-inventory.model';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class LoanInventoryService {
  completedPerc = 0;

  loanInventoryStatusChange = new EventEmitter<number>(true);

  constructor(private localStorage: LocalStorageService) {
    this.completedPerc = 0;
    try {
      this.completedPerc = parseFloat(
        this.localStorage.retrieve(environment.completedPercentage)
      );

      if (isNaN(this.completedPerc)) {
        this.completedPerc = 0;
      }
    } catch {
      this.completedPerc = 0;
    }
  }

  getTableStatus(filled: number, total: number) {
    if (total > 0) {
      return total == filled
        ? TableStatus.Completed
        : TableStatus.NeedInformation;
    } else {
      return TableStatus.NeedInformation;
    }
  }

  countFields(inventory, type) {
    let total = 0;

    Object.keys(inventory)
      .filter(key => key.includes(type))
      .forEach(key => {
        total += inventory[key] || 0;
      });

    return total;
  }

  prepareCountandSum(model: any) {
    return {
      Length: Object.keys(model).length,
      Total: this.sum(model)
    };
  }

  prepareCountandSumConditions(model: any) {
    //
    return {
      Length:
        model.cropMonitoringTotalDocuments +
        model.loanClosingTotalDocuments +
        model.loanUnderWritingTotalDocuments +
        model.loanPreReqTotalDocuments,
      Total:
        model.cropMonitoringFilledDocuments +
        model.loanClosingFilledDocuments +
        model.loanPreReqFilledDocuments +
        model.loanUnderWritingFilledDocuments
    };
  }

  sum(model) {
    let sum = 0;
    Object.keys(model).forEach(element => {
      sum = sum + model[element];
    });
    return sum;
  }

  prepareInventoryInformation(loanData: loan_model) {
    if (environment.isDebugModeActive) console.time('Loan Inventory');

    const borrower = this.prepareCountandSum(
      this.prepareBorrowerInventory(loanData)
    );

    const budget = this.prepareCountandSum(
      this.prepareBudgetInventory(loanData)
    );

    const collateral = this.prepareCountandSum(
      this.prepareCollateralInventory(loanData)
    );

    const committee = this.prepareCountandSum(
      this.prepareCommitteeInventory(loanData)
    );

    const condition = this.prepareCountandSumConditions(this.prepareConditionsInventory(loanData));
    //
    const crop = this.prepareCountandSum(this.prepareCropInventory(loanData));

    const farm = this.prepareCountandSum(this.prepareFarmInventory(loanData));

    const insurance = this.prepareCountandSum(
      this.prepareInsuranceInventory(loanData)
    );

    const opt = this.prepareCountandSum(
      this.prepareOptimizerInventory(loanData)
    );

    this.completedPerc =
      ((borrower.Total +
        budget.Total +
        collateral.Total +
        committee.Total +
        condition.Total +
        crop.Total +
        farm.Total +
        insurance.Total +
        opt.Total) /
        (borrower.Length +
          budget.Length +
          collateral.Length +
          committee.Length +
          condition.Length +
          crop.Length +
          farm.Length +
          insurance.Length +
          opt.Length)) *
      100;

    this.completedPerc = parseFloat(this.completedPerc.toFixed(2));

    this.loanInventoryStatusChange.next(this.completedPerc);

    if (environment.isDebugModeActive) console.timeEnd('Loan Inventory');

    return this.completedPerc;
  }

  prepareBorrowerInventory(loanData: loan_model) {
    //Borrower Fields
    return {
      Borrower:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 64 && p.Tab_ID == 3
        ) == undefined
          ? 1
          : 0,

      ReferredFrom:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 49 && p.Tab_ID == 3
        ) == undefined
          ? 1
          : 0,

      CreditRererrence:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 50 && p.Tab_ID == 3
        ) == undefined
          ? 1
          : 0,

      FarmerAffliate:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 86 && p.Tab_ID == 3
        ) == undefined
          ? 1
          : 0,

      CreditScore:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 56 && p.Tab_ID == 3
        ) == undefined
          ? 1
          : 0,

      BorrowerIncomeHistory:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 51 && p.Tab_ID == 3
        ) == undefined
          ? 1
          : 0,

      Balancesheet:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 52 && p.Tab_ID == 3
        ) == undefined
          ? 1
          : 0
    };
  }
  prepareCropInventory(loanData: loan_model) {
    //Borrower Fields
    return {
      Crop:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 2 && p.Tab_ID == 4
        ) == undefined
          ? 1
          : 0,

      Rebator:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 18 && p.Tab_ID == 4
        ) == undefined
          ? 1
          : 0,

      Yield:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 19 && p.Tab_ID == 4
        ) == undefined
          ? 1
          : 0,

      Price:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 20 && p.Tab_ID == 4
        ) == undefined
          ? 1
          : 0,

      OtherIncome:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 86 && p.Tab_ID == 4
        ) == undefined
          ? 1
          : 0
    };
  }

  prepareBudgetInventory(loanData: loan_model) {
    return {
      Distributor:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 28 && p.Tab_ID == 7
        ) == undefined
          ? 1
          : 0,

      ThirdParty:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 29 && p.Tab_ID == 7
        ) == undefined
          ? 1
          : 0,

      Harvestor:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 30 && p.Tab_ID == 7
        ) == undefined
          ? 1
          : 0,

      EquipmentProvider:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 70 && p.Tab_ID == 7
        ) == undefined
          ? 1
          : 0,

      TotalBudget:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 53 && p.Tab_ID == 7
        ) == undefined
          ? 1
          : 0,

      FixedExpense:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 34 && p.Tab_ID == 7
        ) == undefined
          ? 1
          : 0
    };
  }

  prepareCollateralInventory(loanData: loan_model) {
    return {
      Lienholder:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 35 && p.Tab_ID == 14
        ) == undefined
          ? 1
          : 0,
      Buyer:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 36 && p.Tab_ID == 14
        ) == undefined
          ? 1
          : 0,
      Guarantor:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 37 && p.Tab_ID == 14
        ) == undefined
          ? 1
          : 0,
      CropCollateral:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 7 && p.Tab_ID == 14
        ) == undefined
          ? 1
          : 0,
      AdditionalCollateral:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 38 && p.Tab_ID == 14
        ) == undefined
          ? 1
          : 0,
      Contracts:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 40 && p.Tab_ID == 14
        ) == undefined
          ? 1
          : 0
    };
  }

  prepareCommitteeInventory(loanData: loan_model) {
    return {
      Terms:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 41 && p.Tab_ID == 15
        ) == undefined
          ? 1
          : 0,
      Exceptions:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 42 && p.Tab_ID == 15
        ) == undefined
          ? 1
          : 0,
      LoanComments:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 43 && p.Tab_ID == 15
        ) == undefined
          ? 1
          : 0
    };
  }

  prepareConditionsInventory(loanData: loan_model) {
    let documentsInv = new LoanDocumentsInventory();

    let documents = _.sortBy(
      loanData.Loan_Documents.filter(lc => lc.ActionStatus != 3),
      'Sort_Order'
    );

    // Pre Requisite conditions
    documentsInv.loanUnderWritingDocuments = documents.filter(
      lc => lc.Document_Type_Level == 1
    );

    documentsInv.loanUnderWritingTotalDocuments +=
      documentsInv.loanUnderWritingDocuments.length;
    documentsInv.loanUnderWritingFilledDocuments += documentsInv.loanUnderWritingDocuments.filter(
      lc => !!lc.Upload_User_ID
    ).length;

    // Closing conditions
    documentsInv.loanPreReqDocuments = documents.filter(
      lc => lc.Document_Type_Level == 2
    );

    documentsInv.loanPreReqTotalDocuments +=
      documentsInv.loanPreReqDocuments.length;
    documentsInv.loanPreReqFilledDocuments += documentsInv.loanPreReqDocuments.filter(
      lc => !!lc.Upload_User_ID
    ).length;

    // Underwriting conditions
    documentsInv.loanClosingDocuments = documents.filter(
      lc => lc.Document_Type_Level == 3
    );

    documentsInv.loanClosingTotalDocuments +=
      documentsInv.loanClosingDocuments.length;
    documentsInv.loanClosingFilledDocuments += documentsInv.loanClosingDocuments.filter(
      lc => !!lc.Upload_User_ID
    ).length;

    documentsInv.cropMonitoringDocuments = documents.filter(
      lc => lc.Document_Type_Level == 4
    );

    documentsInv.cropMonitoringTotalDocuments +=
      documentsInv.cropMonitoringDocuments.length;
    documentsInv.cropMonitoringFilledDocuments += documentsInv.cropMonitoringDocuments.filter(
      lc => !!lc.Upload_User_ID
    ).length;

    return documentsInv;
  }

  prepareFarmInventory(loanData: loan_model) {
    return {
      Landlord:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 21 && p.Tab_ID == 5
        ) == undefined
          ? 1
          : 0,
      Farm:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 3 && p.Tab_ID == 5
        ) == undefined
          ? 1
          : 0
    };
  }
  prepareInsuranceInventory(loanData: loan_model) {
    return {
      Agent:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 22 && p.Tab_ID == 6
        ) == undefined
          ? 1
          : 0,
      Agency:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 23 && p.Tab_ID == 6
        ) == undefined
          ? 1
          : 0,
      AIP:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 24 && p.Tab_ID == 6
        ) == undefined
          ? 1
          : 0,
      Policy:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 25 && p.Tab_ID == 6
        ) == undefined
          ? 1
          : 0,
      Wfrp:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 25 && p.Tab_ID == 6
        ) == undefined
          ? 1
          : 0,
      APH:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 27 && p.Tab_ID == 6
        ) == undefined
          ? 1
          : 0
    };
  }

  prepareOptimizerInventory(loanData: loan_model) {
    return {
      OptimizerIrr:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 6 && p.Tab_ID == 13
        ) == undefined
          ? 1
          : 0,
      OptimizerNir:
        loanData.Loan_Exceptions.find(
          p => p.Exception_ID_Level == 2 && p.Chevron_ID == 66 && p.Tab_ID == 13
        ) == undefined
          ? 1
          : 0
    };
  }

  // Copied from Collateral Service ( pasted here to resolve circular dependency )
  private getRowData(localloanobject, categoryCode, source, sourceKey) {
    if (localloanobject) {
      if (sourceKey === '') {
        // Marketing Contracts
        return localloanobject[source] !== null
          ? localloanobject[source].filter(lc => {
              return categoryCode
                ? lc.ActionStatus !== 3 &&
                    categoryCode === lc.Collateral_Category_Code
                : lc.ActionStatus !== 3;
            })
          : [];
      }
    }
  }
}
