import { Component, OnInit, Input } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { TableStatus } from '../loan-inventory.model';

import { DataService } from '@lenda/services/data.service';
import { LoanInventoryService } from '../loan-inventory.service';

@Component({
  selector: 'app-inventory-crop',
  templateUrl: './crop.component.html',
  styleUrls: ['./crop.component.scss']
})
export class InventoryCropComponent implements OnInit {
  private loanData: loan_model = new loan_model();

  @Input() public showDetail = false;
  @Input() public ID: string = '';
  @Input() public expanded = true;

  public TableStatus: typeof TableStatus = TableStatus;

  public cropInventory: any;

  private subscription: ISubscription;

  constructor(
    private localstorageservice: LocalStorageService,
    private loanInventoryService: LoanInventoryService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.loanData = this.localstorageservice.retrieve(environment.loankey);
    this.cropInventory = this.loanInventoryService.prepareCropInventory(
      this.loanData
    );

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.loanData = res;
      this.cropInventory = this.loanInventoryService.prepareCropInventory(
        this.loanData
      );
    });
  }

  prepareStatus(status: number) {
    return status == 0 ? 'Need Information' : 'Completed';
  }

  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
