import { Component, OnInit } from '@angular/core';

import { Loansettings } from '@lenda/models/loansettings';

import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { MasterService } from '@lenda/master/master.service';

@Component({
  selector: 'app-loan-inventory-report',
  templateUrl: './loan-inventory.component.html',
  styleUrls: ['./loan-inventory.component.scss']
})
export class LoanInventoryComponent implements OnInit {
  public showDetail: boolean = false;
  public loanSetting: Loansettings = new Loansettings();

  constructor(
    private settingsService: SettingsService,
    public masterSvc: MasterService
  ) {}

  ngOnInit() {
    this.loanSetting = this.settingsService.userLoanSettings;
  }

  onDetailChange(detailsState) {
    this.showDetail = detailsState;
  }
}
