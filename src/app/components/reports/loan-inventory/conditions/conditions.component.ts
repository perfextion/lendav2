import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { LoanDocumentsInventory } from '../loan-inventory.model';
import { Loan_Document } from '@lenda/models/loan/loan_document';

import { DataService } from '@lenda/services/data.service';
import { LoanInventoryService } from '../loan-inventory.service';

@Component({
  selector: 'app-inventory-conditions',
  templateUrl: './conditions.component.html',
  styleUrls: ['./conditions.component.scss']
})
export class InventoryConditionsComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  private localLoanObject: loan_model;

  @Input() public ID: string = '';
  @Input() public expanded = true;

  conditionInv = new LoanDocumentsInventory();

  constructor(
    private localStorageService: LocalStorageService,
    private loanInventory: LoanInventoryService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.localLoanObject = this.localStorageService.retrieve(
      environment.loankey
    );

    this.conditionInv = this.loanInventory.prepareConditionsInventory(
      this.localLoanObject
    );

    this.subscription = this.dataService.getLoanObject().subscribe(loan => {
      this.localLoanObject = loan;
      this.conditionInv = this.loanInventory.prepareConditionsInventory(
        this.localLoanObject
      );
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getStatus(Doc: Loan_Document) {
    if (!Doc.Upload_Date_Time) {
      return 'Need Information';
    } else {
      return 'Completed';
    }
  }
}
