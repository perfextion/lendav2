import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';
import { loan_model, LoanGroup } from '@lenda/models/loanmodel';
import { Get_Borrower_Name } from '@lenda/services/common-utils';
import { LoanMaster } from '@lenda/models/ref-data-model';

import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-related-loan-list',
  templateUrl: './related-loan-list.component.html',
  styleUrls: ['./related-loan-list.component.scss']
})
export class RelatedLoanListComponent implements OnInit,OnDestroy {
  @Input()
  public type : RelatedLoanListType;

  @Input()
  public showARMDetails : boolean = true;

  @Input()
  public ID : string ='';

  @Input()
  public expanded = true;

  public header : string;
  public loanList : Array<LoanMaster> = [];
  public localLoanObject : loan_model;

  private subscription : ISubscription;
  private loanGroupSubscription : ISubscription;

  public totalRow : RelatedLoansTotalRow = new RelatedLoansTotalRow();
  public RelatedLoanListType : typeof RelatedLoanListType = RelatedLoanListType;

  public loanGroup: Array<LoanGroup>;
  public openChevron: boolean = true;

  constructor(
    private localStorageService : LocalStorageService,
    private dataService : DataService
  ) { }

  getBorrowerName(lm: LoanMaster) {
    return Get_Borrower_Name(lm);
  }

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res=>{
      if(res){
        this.localLoanObject = res;
        this.prepareLoanList();
      }
    })

    this.loanGroupSubscription = this.localStorageService.observe(environment.loanGroup).subscribe(res=>{
      if(res){
        this.prepareLoanList();
      }
    })

    this.localLoanObject = this.localStorageService.retrieve(environment.loankey);
    this.loanGroup = this.localStorageService.retrieve(environment.loanGroup);

    this.prepareLoanList();

    this.checkLoanGroup();
    this.checkCrossCollateralizedLoans();
    this.checkIfAffiliated();
  }

  private checkLoanGroup() {
    if(this.type == 1) {
      this.openChevron = !_.isEmpty(this.loanGroup);
    }
  }

  private checkCrossCollateralizedLoans() {
    if(this.type == 3) {
      this.openChevron = !_.isEmpty(this.localLoanObject.Cross_Collateralized_Loans);
    }
  }

  private checkIfAffiliated() {
    if(this.type == 2) {
      if(!_.isEmpty(this.loanGroup)) {
        let IsAffiliated = _.find(this.loanGroup, (el) => {
          return el.IsAffiliated == true;
        });

        this.openChevron = !_.isEmpty(IsAffiliated);
      } else {
        this.openChevron = false;
      }
    }
  }

  private prepareLoanList(){
    this.loanList = [this.localLoanObject.LoanMaster];
    let localGroups : Array<LoanGroup> = this.localStorageService.retrieve(environment.loanGroup) || [];

    let borrowerName = Get_Borrower_Name(this.localLoanObject.LoanMaster as any)
    let farmerName = this.localLoanObject.LoanMaster.Farmer_Last_Name + ', ' + this.localLoanObject.LoanMaster.Farmer_First_Name + ' ' + this.localLoanObject.LoanMaster.Farmer_MI;

    if(this.type == RelatedLoanListType.Affiliated){
      this.loanList = [];
      this.header = `Affiliated Loans of Farmer: ${farmerName}`

      localGroups.filter(group=>group.IsAffiliated).filter(loan=>{
        this.loanList.push(loan.LoanJSON.LoanMaster);
      });
    }

    if(this.type == RelatedLoanListType.CrossCollateralized){
      this.loanList = [];
      this.header = `Cross Collateralized Loans of Borrower: ${borrowerName}`

      localGroups.filter(group=>group.IsCrossCollateralized).filter(loan=>{
        this.loanList.push(loan.LoanJSON.LoanMaster);
      });
    }

    if(this.type == RelatedLoanListType.Current){
      this.header = `Current Loan History of Borrower: ${borrowerName}`

      localGroups.filter(group=>group.IsCurrentYearLoan).filter(loan=>{
        this.loanList.push(loan.LoanJSON.LoanMaster);
      });

      this.loanList = _.orderBy(this.loanList, ['Loan_Full_ID'], ['desc']);
    }

    if(this.type == RelatedLoanListType.Previous){
      this.loanList = [];//previous loans should not include the currently selected loan (all otehr type should include)
      this.header = `Previous Loan History of Borrower: ${borrowerName}`
      localGroups.filter(group=>group.IsPreviousYearLoan).filter(loan=>{
        this.loanList.push(loan.LoanJSON.LoanMaster);
      });

      this.loanList = _.orderBy(this.loanList, ['Loan_Full_ID'], ['desc']);
    }

    if([RelatedLoanListType.Affiliated,RelatedLoanListType.CrossCollateralized].includes(this.type)){
      this.totalRow.Acres = _.sumBy(this.loanList,(loan)=> loan.Total_Crop_Acres ||0);
      this.totalRow.ARMCommit = _.sumBy(this.loanList,(loan)=> loan.ARM_Commitment ||0);
      this.totalRow.DistCommit = _.sumBy(this.loanList,(loan)=> loan.Dist_Commitment ||0);
      this.totalRow.TotalCommit = _.sumBy(this.loanList,(loan)=> loan.Total_Commitment ||0);
      this.totalRow.OrgiFee = _.sumBy(this.loanList,(loan)=> loan.Origination_Fee_Amount ||0);
      this.totalRow.ServiceFee = _.sumBy(this.loanList,(loan)=> loan.Service_Fee_Amount ||0);
      this.totalRow.ExtensionFee = _.sumBy(this.loanList,(loan)=> loan.Extension_Fee_Amount ||0);
      this.totalRow.TotalFee = _.sumBy(this.loanList,(loan)=> (loan.Total_ARM_Fees_And_Interest ||0));
      this.totalRow.Outstanding = _.sumBy(this.loanList,(loan)=> loan.Outstanding_Balance ||0);
      this.totalRow.RiskCusion = _.sumBy(this.loanList,(loan)=> loan.Risk_Cushion_Percent ||0);
      this.totalRow.Return = _.sumBy(this.loanList,(loan)=> loan.Return_Percent ||0);
    }
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
    this.loanGroupSubscription.unsubscribe();
  }

  formateDate(strDate){
    if(strDate){
      let date = new Date(strDate);
      if(date.getFullYear() < 2000){
        return '-';
      }else{
        return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
      }
    }else{
      return '-';
    }
  }

}

export enum RelatedLoanListType{
  Current,
  Previous,
  Affiliated,
  CrossCollateralized

}
class RelatedLoansTotalRow {
  Acres : number;
  ARMCommit : number;
  DistCommit : number;
  TotalCommit : number;
  OrgiFee: number;
  ServiceFee : number;
  ExtensionFee : number;
  TotalFee : number;
  Outstanding : number;
  RiskCusion : number;
  Return : number;
}
