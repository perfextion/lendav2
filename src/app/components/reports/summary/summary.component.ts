import { Component, OnInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { Loansettings } from '@lenda/models/loansettings';

import { RelatedLoanListType } from '@lenda/components/reports/summary/related-loan-list/related-loan-list.component';

import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { MasterService } from '@lenda/master/master.service';
import { CommitmentType } from '@lenda/components/reports/cross-collateral/commitment.service';

@Component({
  selector: 'app-summary-report',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryReportComponent implements OnInit {
  public CommitmentType: typeof CommitmentType = CommitmentType;
  public RelatedLoanListType: typeof RelatedLoanListType = RelatedLoanListType;
  public showARMDetails: boolean = true;
  public showDiscValues: boolean = true;

  public tableColToDisplay: any = null;

  public loanSetting: Loansettings = new Loansettings();

  //Declared Variable to enable building the project
  //Kindly recheck
  public SummaryLoanComment: any;

  constructor(
    private localstorageservice: LocalStorageService,
    private settingsService: SettingsService,
    public masterSvc: MasterService
  ) {
    let tctd = this.localstorageservice.retrieve(environment.loankey);
    if (!_.isEmpty(tctd)) {
      this.tableColToDisplay = JSON.parse(
        tctd.LoanMaster.Loan_Settings
      ).Loan_key_Settings;
    }

    this.loanSetting = this.settingsService.userLoanSettings;
  }

  ngOnInit() {}
}
