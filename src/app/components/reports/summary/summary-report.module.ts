import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';
import { ReportsSharedModule } from '../shared/reports-shared.module';
import { SummaryReportsSharedModule } from '../summary-shared/summary-report-shared.module';

import { SummaryReportComponent } from './summary.component';
import { CollateralValueReportComponent } from './collateral-value/collateral-value.component';
import { CommitmentReportComponent } from './commitment/commitment.component';
import { ExceptionReportComponent } from './exception-report/exception-report.component';
import { LoanCommentReportComponent } from './loan-comment/loan-comment.component';
import { RelatedLoanListComponent } from './related-loan-list/related-loan-list.component';
import { ValidationReportComponent } from './validation-report/validation-report.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    UiComponentsModule,
    ReportsSharedModule,
    SummaryReportsSharedModule
  ],
  declarations: [
    SummaryReportComponent,
    CollateralValueReportComponent,
    CommitmentReportComponent,
    ExceptionReportComponent,
    LoanCommentReportComponent,
    RelatedLoanListComponent,
    ValidationReportComponent
  ],
  exports: [SummaryReportComponent]
})
export class SummaryReportsModule {}
