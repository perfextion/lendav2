import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { Loan_Exception } from '@lenda/models/loan/loan_exceptions';

import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-exception-report',
  templateUrl: './exception-report.component.html',
  styleUrls: ['./exception-report.component.scss']
})
export class ExceptionReportComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;

  public Loan_Exceptions: Array<Loan_Exception>;

  public exceptions: Array<ExceptionGroupedList> = [];
  public refExceptions: Array<ExceptionGroupedList> = [];

  public localLoanObject: loan_model;

  @Input() ID: string = '';
  @Input() showDetails: boolean = true;
  @Input() expanded = true;

  private refdata: RefDataModel;
  private refdataSub: ISubscription;

  constructor(
    private localStorageService: LocalStorageService,
    private dataService: DataService
  ) {
    this.refdata = this.localStorageService.retrieve(
      environment.referencedatakey
    );

    this.refdataSub = this.localStorageService
      .observe(environment.referencedatakey)
      .subscribe(refdata => {
        this.refdata = refdata;
      });
  }

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res) {
        this.localLoanObject = res;
        this.prepareData();
      }
    });

    this.localLoanObject = this.localStorageService.retrieve(
      environment.loankey
    );
    if (this.localLoanObject) {
      this.prepareData();
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.refdataSub) {
      this.refdataSub.unsubscribe();
    }
  }

  private prepareData() {
    this.Loan_Exceptions = this.localLoanObject.Loan_Exceptions.filter(
      ex => ex.ActionStatus != 3
    );
    let exceptionDictionary = _.groupBy(this.Loan_Exceptions, 'Tab_ID');

    this.exceptions = [];
    for (let tab in exceptionDictionary) {
      let refTab = this.refdata.RefTabs.find(t => t.Tab_ID.toString() == tab);
      this.exceptions.push({
        tab: refTab.Tab_Name,
        exceptions: _.sortBy(exceptionDictionary[tab], e => e.Sort_Order)
      });
    }
  }
}

class ExceptionGroupedList {
  tab: string = '';
  exceptions: Array<Loan_Exception> = [];
}
