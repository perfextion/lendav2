import { Component, OnInit, Input } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { LoanMaster } from '@lenda/models/ref-data-model';

import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-loan-comment',
  templateUrl: './loan-comment.component.html',
  styleUrls: ['./loan-comment.component.scss']
})
export class LoanCommentReportComponent implements OnInit {
  private subscription: ISubscription;
  public loanMaster: LoanMaster;

  @Input() public ID: string = '';
  @Input() public expanded = true;

  constructor(
    private localStorageService: LocalStorageService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res) {
          this.loanMaster = res.LoanMaster;
        }
      });

    let loanObj = this.localStorageService.retrieve(
      environment.loankey
    ) as loan_model;
    if (loanObj) {
      this.loanMaster = loanObj.LoanMaster;
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
