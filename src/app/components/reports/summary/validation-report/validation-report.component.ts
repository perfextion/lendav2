import { Component, OnInit, Input, OnDestroy, ViewEncapsulation } from '@angular/core';

import { ISubscription } from 'rxjs/Subscription';
import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { ValidationGroup } from '@lenda/components/committee/loan-validations/loan-validations.component';

import { loan_model } from '@lenda/models/loanmodel';
import { RefDataModel, RefValidation } from '@lenda/models/ref-data-model';
import { errormodel } from '@lenda/models/commonmodels';

@Component({
  selector: 'app-validation-report',
  templateUrl: './validation-report.component.html',
  styleUrls: ['./validation-report.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ValidationReportComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;

  public validationGroups: Array<ValidationGroup> = [];
  public refValidationGroups: Array<ValidationGroup> = [];

  public validations: Array<errormodel>;
  public uniqueValidations: Array<RefValidation> = [];

  public localLoanObject: loan_model;

  @Input()
  public ID: string = '';

  @Input()
  public showDetails: boolean = true;

  @Input()
  public expanded = true;

  private refdata: RefDataModel;

  constructor(private localstorage: LocalStorageService) {
    this.validations = this.localstorage.retrieve(environment.errorbase);
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);

    this.getData();

    this.subscription = this.localstorage
      .observe(environment.errorbase)
      .subscribe(errors => {
        this.validations = errors;
        this.getData();
      });
  }

  ngOnInit() {}

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private getData() {
    this.validations = _.orderBy(this.validations.filter(a => !a.ignore), v => this.getTabId(v.tab));
    let groups = _.groupBy(this.validations, v => v.tab);

    this.validationGroups = [];

    for (let group in groups) {
      this.validationGroups.push({
        tab: group,
        validations: _.sortBy(groups[group], v => v.level)
      });
    }
  }

  private getTabId(tabname: string) {
    try {
      let tab = this.refdata.RefTabs.find(
        a => a.Tab_Name.toUpperCase() == tabname.toUpperCase()
      );
      return tab.Tab_ID;
    } catch {
      return 0;
    }
  }
}
