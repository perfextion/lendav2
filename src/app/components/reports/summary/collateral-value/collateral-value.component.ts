import { Component, OnInit, Input, OnDestroy, OnChanges } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';

import {
  CollateralReportService,
  CollateralReport,
  CollateralValueKey
} from '../../collateral-report.service';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-collateral-value',
  templateUrl: './collateral-value.component.html',
  styleUrls: ['./collateral-value.component.scss']
})
export class CollateralValueReportComponent implements OnInit, OnDestroy, OnChanges {
  private subscription: ISubscription;
  public localLoanObject: loan_model;
  public collateralReport: CollateralReport = new CollateralReport();
  public allValueHeader = [];
  public CollateralValueKey: typeof CollateralValueKey = CollateralValueKey;
  public collateralValueKeys: Array<CollateralValueKey> = [];

  /**
   * To pass the information of whether the ARM details slider is on or off in the summary (parent) component,
   * accordingly the collateral value will hide/show some of the data
   */
  @Input() public showARMDetails: boolean = true;

  /**
   * To pass the information of whether the Disc Value slider is on or off in the summary (parent) component,
   * accordingly the collateral value will hide/show some of the data
   */
  @Input() public showDiscValues: boolean = true;

  /**
   * To give the table wrapper html id for the PDF export purpose
   */
  @Input() public ID: string = '';

  @Input() public expanded = true;

  constructor(
    private localStorageServce: LocalStorageService,
    private collateralReportService: CollateralReportService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res) {
          this.localLoanObject = res;
          this.prepareData();
        }
      });

    this.localLoanObject = this.localStorageServce.retrieve(
      environment.loankey
    );

    this.prepareData();
  }

  ngOnChanges() {
    if (this.showDiscValues) {
      this.allValueHeader = [
        'Mkt Value',
        'Disc Mkt Value',
        'Ins Value',
        'Disc Ins Value'
      ];

      this.collateralValueKeys = [
        CollateralValueKey.Market,
        CollateralValueKey.DiscMarket,
        CollateralValueKey.Insurance,
        CollateralValueKey.DiscInsurance
      ];
    } else {
      this.allValueHeader = ['Mkt Value', 'Ins Value'];
      this.collateralValueKeys = [
        CollateralValueKey.Market,
        CollateralValueKey.Insurance
      ];
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private prepareData() {
    if (this.localLoanObject) {
      if (this.localLoanObject.LoanMaster) {
        this.collateralReport = this.collateralReportService.getCollateralValue(
          this.localLoanObject,
          this.collateralValueKeys
        );
      }
    }
  }

  getValue(object: any, key: any) {
    if (object && !isNaN(object[key])) {
      return parseFloat(object[key]);
    } else {
      return '0';
    }
  }
}
