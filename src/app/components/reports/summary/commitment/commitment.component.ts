import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { Get_Borrower_Name } from '@lenda/services/common-utils';

import {
  CommitmentService,
  CommitmentSummary,
  CommitmentType
} from '../../cross-collateral/commitment.service';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-commitment',
  templateUrl: './commitment.component.html',
  styleUrls: ['./commitment.component.scss']
})
export class CommitmentReportComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  public localLoanObject: loan_model;
  public commitmentSummary: CommitmentSummary = new CommitmentSummary();

  @Input() public isCCReport = false;
  @Input() public type: CommitmentType = CommitmentType.ARMDIST;
  @Input() public displayWeightedPercent = false;
  @Input() public displayReturnInDetail = false;
  @Input() public ID: string = '';
  @Input() public expanded = true;

  public CommitmentType: typeof CommitmentType = CommitmentType;
  public borrowerName: string = '';

  constructor(
    private localStorageServce: LocalStorageService,
    private commitmentService: CommitmentService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res) {
          this.localLoanObject = res;
          this.prepareData();
        }
      });

    this.localLoanObject = this.localStorageServce.retrieve(
      environment.loankey
    );

    this.prepareData();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  prepareData() {
    if (this.localLoanObject && this.localLoanObject.LoanMaster) {
      this.borrowerName = Get_Borrower_Name(this.localLoanObject.LoanMaster);
      if (this.type === CommitmentType.RETURN) {
        this.commitmentSummary = this.commitmentService.getReturnCommitments(
          this.localLoanObject.LoanMaster
        );
      } else {
        this.commitmentSummary = this.commitmentService.getArmDistCommitments(
          this.localLoanObject.LoanMaster
        );
      }
    }
  }
}
