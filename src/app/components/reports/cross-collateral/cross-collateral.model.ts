export class Crop {
  name: string;
  cropType: string;
  practiceType: string;
  cropPracticeType: string;
  value: number;
  perValue: number;
  isPracticeRow: boolean;
}

export class CCCrop {
  name: string;
  cropType: string;
  practiceType: string;
  cropPracticeType: string;
  values: Array<number>;
  perValue: number;
  isPracticeRow: boolean;
  totalValue: number;
}

export class TotalRow {
  name: string = 'Total';
  value: number;
  perValues: number;
  shouldHighlight: boolean;
}

export class CCTotalRow {
  name: string = 'Total';
  values: Array<number> = [];
  perValue: number;
  totalValue: number;
  shouldHighlight: boolean;
  shouldHighlightSecondary: boolean;
  bolderFont: boolean;
  totalRow: boolean;
}

export class Heading {
  itemHeading: string;
  loanBorrower: Array<string> = [];
  totalHeading: string = 'Total';
  percentHeading: string = 'Percent';
}
