import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model, LoanGroup } from '@lenda/models/loanmodel';
import { CommitmentService, CommitmentSummary, CommitmentType, Commitment } from '@lenda/components/reports/cross-collateral/commitment.service';
import { Get_Borrower_Name } from '@lenda/services/common-utils';
import { Helper } from '@lenda/services/math.helper';

import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-cc-commitment',
  templateUrl: './cc-commitment.component.html',
  styleUrls: ['./cc-commitment.component.scss']
})
export class CCCommitmentComponent implements OnInit, OnDestroy {
  private subscription : ISubscription;
  public localLoanObject: loan_model;
  public ccReport : CCCommitmentSummary = new CCCommitmentSummary();
  public CommitmentType : typeof CommitmentType = CommitmentType;

  @Input() public type :CommitmentType = CommitmentType.ARMDIST;
  @Input() public ID : string = '';
  @Input() public expanded = true;

  constructor(
    private localStorageServce: LocalStorageService,
    private commitmentService : CommitmentService,
    private dataService : DataService
  ) { }

  ngOnInit() {
    this.subscription =  this.dataService.getLoanObject().subscribe((res: loan_model)=>{
      if (res) {
        this.localLoanObject = res;
        this.prepareData();
      }
    });

    this.localLoanObject = this.localStorageServce.retrieve(environment.loankey);
    this.prepareData();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private prepareData() {

    let loanCommitmentSummaries: Array<CommitmentSummary> = [];

    if (this.localLoanObject && this.localLoanObject.LoanMaster ) {

      this.ccReport.borrowers.push(Get_Borrower_Name(this.localLoanObject.LoanMaster));
      if(this.type === CommitmentType.RETURN) {
        loanCommitmentSummaries.push(this.commitmentService.getReturnCommitments(this.localLoanObject.LoanMaster));
      } else {
        loanCommitmentSummaries.push(this.commitmentService.getArmDistCommitments(this.localLoanObject.LoanMaster));
      }

      let loanGroups : Array<LoanGroup>= this.localStorageServce.retrieve(environment.loanGroup) || [];
      let crossCollateralLoans : Array<LoanGroup>= loanGroups.filter(lg=>lg.IsCrossCollateralized);

      crossCollateralLoans.forEach(loan => {
        this.ccReport.borrowers.push(Get_Borrower_Name(loan.LoanJSON.LoanMaster));
        if(this.type === CommitmentType.RETURN) {
          loanCommitmentSummaries.push(this.commitmentService.getReturnCommitments(loan.LoanJSON.LoanMaster));
        } else {
          loanCommitmentSummaries.push(this.commitmentService.getArmDistCommitments(loan.LoanJSON.LoanMaster));
        }
      })

      let allRows :Array<Commitment> = [];
      loanCommitmentSummaries.forEach(loanSum => {
        allRows = allRows.concat(loanSum.commitments);
      });

      allRows = _.uniqBy(allRows,'name');

      allRows.forEach(row => {
        let ccRow : CCCommitmentRow = new CCCommitmentRow();
        ccRow.name = row.name;
        loanCommitmentSummaries.forEach(loan => {
          let matchingRow = loan.commitments.find(com=>com.name == ccRow.name);
          if(matchingRow) {
            ccRow.values.push(matchingRow.value);
          } else {
            ccRow.values.push(0);
          }
        })

        ccRow.totalValue = _.sum(ccRow.values);
        this.ccReport.rows.push(ccRow);
      });

      let daysSumProduct  = 0;
      loanCommitmentSummaries.forEach(loan => {
        this.ccReport.totalRow.values.push(loan.totalCommitment || 0);

        this.ccReport.days.values.push(loan.days ||0);
        daysSumProduct +=((loan.totalCommitment || 0) * (loan.days || 0));
        this.ccReport.returnPer.values.push((loan.return_per || 0));

      })

      this.ccReport.totalRow.totalValue = _.sum(this.ccReport.totalRow.values);
      this.ccReport.days.totalValue = daysSumProduct/this.ccReport.totalRow.totalValue;

      //calculation of total return
      if(this.type === CommitmentType.RETURN) {
        let totalArmCommit = _.sumBy(loanCommitmentSummaries, c => c.armcommit);
        const total_return_arm = _.sumBy(loanCommitmentSummaries, a => a.returnArmSum);
        this.ccReport.returnPer.totalValue = Helper.divide(total_return_arm, totalArmCommit);
        this.storeReturnPercentToLoanScratch();
      }


      this.ccReport.rows.forEach(row => {
        row.perValue = row.totalValue / this.ccReport.totalRow.totalValue * 100;
      });

      this.ccReport.totalRow.perValue = 100;
    }
  }

  private storeReturnPercentToLoanScratch() {
    let lg = this.localStorageServce.retrieve(environment.loanGroup);

    if(!lg) {
      lg = [];
    }

    lg[0].returnPercent = this.ccReport.returnPer.totalValue;
    this.localStorageServce.store(environment.loanGroup, lg);
  }
}

export class CCCommitmentSummary{
  borrowers : Array<string> = [];
  rows : Array<CCCommitmentRow> = [];
  totalRow : CCCommitmentRow = new CCCommitmentRow();
  days : CCCommitmentRow = new CCCommitmentRow();
  returnPer : CCCommitmentRow = new CCCommitmentRow();
}

export class CCCommitmentRow{
  name : string;
  values : Array<number> = [];
  totalValue : number;
  perValue : number;
}
