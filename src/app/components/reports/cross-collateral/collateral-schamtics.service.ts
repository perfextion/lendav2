import { Injectable } from '@angular/core';

import * as _ from 'lodash';

import { loan_model, LoanGroup } from '@lenda/models/loanmodel';
import {
  CCCollateralReport,
  CCCollateralRow
} from './cc-collateral-value/cc-collateral-value.component';
import { Get_Borrower_Name } from '@lenda/services/common-utils';
import {
  CollateralReportService,
  CollateralReport,
  CollateralValueKey,
  CollateralType
} from '../collateral-report.service';
import {
  CommitmentSummary,
  CommitmentService,
  Commitment
} from './commitment.service';
import {
  CCCommitmentSummary,
  CCCommitmentRow
} from './cc-commitment/cc-commitment.component';
import { Helper } from '@lenda/services/math.helper';

@Injectable({
  providedIn: 'root'
})
export class CollateralSchematicsService {
  constructor(
    private collateralReportService: CollateralReportService,
    private commitmentService: CommitmentService
  ) {}

  prepareRC(
    localLoanObject: loan_model,
    loanGroup: Array<LoanGroup>
  ) {
    let ccCollateralReport = new CCCollateralReport();
    let collateralReports: Array<CollateralReport> = [];
    let collateralValueKey: CollateralValueKey = CollateralValueKey.DiscInsurance;

    if (localLoanObject) {

      if (localLoanObject.LoanMaster) {
        ccCollateralReport.borrowers.push(Get_Borrower_Name(localLoanObject.LoanMaster));
        collateralReports.push(
          this.collateralReportService.getCollateralValue(localLoanObject, [ collateralValueKey ])
        );
      }

      let crossCollateralized: Array<LoanGroup> = loanGroup.filter(
        lg => lg.IsCrossCollateralized
      );
      crossCollateralized.forEach(loan => {
        ccCollateralReport.borrowers.push(Get_Borrower_Name(loan.LoanJSON.LoanMaster));
        collateralReports.push(this.collateralReportService.getCollateralValue(loan.LoanJSON, [collateralValueKey])
        );
      });

      let allCollteralTypes: Array<CollateralType> = [];

      collateralReports.forEach(loanReport => {
        allCollteralTypes = allCollteralTypes.concat(loanReport.collateralTypes);
      });

      allCollteralTypes = _.uniqBy(allCollteralTypes, 'typeName');

      allCollteralTypes.forEach(clType => {
        let ccClTypeRow: CCCollateralRow = new CCCollateralRow();
        ccClTypeRow.name = clType.typeName;

        collateralReports.forEach(loanReport => {
          let loanClType = loanReport.collateralTypes.find(clType => clType.typeName == ccClTypeRow.name);
          if (loanClType) {
            ccClTypeRow.values.push(loanClType.value[collateralValueKey]);
          } else {
            ccClTypeRow.values.push(0);
          }
        });

        ccClTypeRow.totalValue = _.sum(ccClTypeRow.values);
        ccCollateralReport.collateralTypes.push(ccClTypeRow);
      });

      collateralReports.forEach(loanReport => {
        ccCollateralReport.totalCollateral.values.push(loanReport.totalCollateral[collateralValueKey] || 0);
        ccCollateralReport.armMargin.values.push(loanReport.armMargin[collateralValueKey] || 0);
        ccCollateralReport.totalMargin.values.push(loanReport.totalMargin[collateralValueKey] || 0);
      });

      ccCollateralReport.armMargin.totalValue = _.sum(ccCollateralReport.armMargin.values);
      ccCollateralReport.totalMargin.totalValue = _.sum(ccCollateralReport.totalMargin.values);

      ccCollateralReport.RCValue = (ccCollateralReport.armMargin.totalValue / localLoanObject.LoanMaster.ARM_Commitment) * 100;
    }

    return ccCollateralReport.RCValue;
  }

  prepeareReturnPercent(
    localLoanObject: loan_model,
    loanGroups: Array<LoanGroup>
  ) {
    let loanCommitmentSummaries: Array<CommitmentSummary> = [];
    let ccReport: CCCommitmentSummary = new CCCommitmentSummary();

    if (localLoanObject && localLoanObject.LoanMaster) {
      ccReport.borrowers.push(Get_Borrower_Name(localLoanObject.LoanMaster));
      loanCommitmentSummaries.push(
        this.commitmentService.getReturnCommitments(localLoanObject.LoanMaster)
      );

      let crossCollateralLoans: Array<LoanGroup> = loanGroups.filter(
        lg => lg.IsCrossCollateralized
      );
      crossCollateralLoans.forEach(loan => {
        ccReport.borrowers.push(Get_Borrower_Name(loan.LoanJSON.LoanMaster));
        loanCommitmentSummaries.push(
          this.commitmentService.getReturnCommitments(loan.LoanJSON.LoanMaster)
        );
      });

      let allRows: Array<Commitment> = [];
      loanCommitmentSummaries.forEach(loanSum => {
        allRows = allRows.concat(loanSum.commitments);
      });

      allRows = _.uniqBy(allRows, 'name');

      allRows.forEach(row => {
        let ccRow: CCCommitmentRow = new CCCommitmentRow();
        ccRow.name = row.name;
        loanCommitmentSummaries.forEach(loan => {
          let matchingRow = loan.commitments.find(com => com.name == ccRow.name);
          if (matchingRow) {
            ccRow.values.push(matchingRow.value);
          } else {
            ccRow.values.push(0);
          }
        });

        ccRow.totalValue = _.sum(ccRow.values);
        ccReport.rows.push(ccRow);
      });

      let daysSumProduct = 0;
      loanCommitmentSummaries.forEach(loan => {
        ccReport.totalRow.values.push(loan.totalCommitment || 0);

        ccReport.days.values.push(loan.days || 0);
        daysSumProduct += (loan.totalCommitment || 0) * (loan.days || 0);
        ccReport.returnPer.values.push(loan.return_per || 0);
      });

      ccReport.totalRow.totalValue = _.sum(ccReport.totalRow.values);
      ccReport.days.totalValue = daysSumProduct / ccReport.totalRow.totalValue;

      //calculation of total return
      let totalArmCommit = _.sumBy(loanCommitmentSummaries, c => c.armcommit);
      const total_return_arm = _.sumBy(loanCommitmentSummaries, a => a.returnArmSum);
      ccReport.returnPer.totalValue = Helper.divide(total_return_arm, totalArmCommit);
    }

    return ccReport.returnPer.totalValue;
  }
}
