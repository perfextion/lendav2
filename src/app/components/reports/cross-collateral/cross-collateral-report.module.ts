import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';
import { ReportsSharedModule } from '../shared/reports-shared.module';

import { CrossCollateralReportComponent } from './cross-collateral.component';
import { AcresComponent } from './acres/acres.component';
import { CCBudgetReportComponent } from './cc-budget-report/cc-budget-report.component';
import { CCCollateralValueComponent } from './cc-collateral-value/cc-collateral-value.component';
import { CCCommitmentComponent } from './cc-commitment/cc-commitment.component';
import { CCProjectedincomeComponentnt } from './cc-projectedincome/cc-projectedincome.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    UiComponentsModule,
    ReportsSharedModule
  ],
  declarations: [
    CrossCollateralReportComponent,
    AcresComponent,
    CCBudgetReportComponent,
    CCCollateralValueComponent,
    CCCommitmentComponent,
    CCProjectedincomeComponentnt
  ],
  exports: [CrossCollateralReportComponent]
})
export class CrossCollateralReportsModule {}
