import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import {
  CCProjectedIncomeService,
  CCProjectedIncomeReport
} from '@lenda/components/reports/cross-collateral/cc-projectedincome/ccProjectedIncome.service';
import { ReportsSettings } from '@lenda/models/loansettings';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';

import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { DataService } from '@lenda/services/data.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';

@Component({
  selector: 'app-cc-projectedincome',
  templateUrl: './cc-projectedincome.component.html',
  styleUrls: ['./cc-projectedincome.component.scss']
})
export class CCProjectedincomeComponentnt implements OnInit, OnDestroy {
  private subscription: ISubscription;
  private localloanobject: loan_model = new loan_model();
  public allDataFetched = false;
  public displayPracticeRows: boolean = false;
  public prReport: CCProjectedIncomeReport = new CCProjectedIncomeReport();

  @Input() public ID: string = '';
  @Input() public colToDisplay: any = null;
  @Input() public expanded = true;

  //to track whether the storing the json attempted or not, otehrwise it will go in infinite look with observer
  //as we have to call the performcalculationonloanobject function in the routine of the preparedData which is being called from
  //observer
  public isStorageAttempted: boolean = false;

  public reportSettings: ReportsSettings;
  public columnsDisplaySettings: ColumnsDisplaySettings;

  private preferencesubscription: ISubscription;

  constructor(
    public localstorageservice: LocalStorageService,
    public logging: LoggingService,
    public loanCalculationsService: LoancalculationWorker,
    private dataService: DataService,
    private ccProjectedIncomeService: CCProjectedIncomeService,
    private settingsService: SettingsService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res != undefined && res != null) {
          this.localloanobject = res;
          this.allDataFetched = true;
          this.prepareData(this.localloanobject);
        }
      });

    this.getdataforgrid();

    this.getUserPreferences();
  }

  getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;
    this.reportSettings = this.settingsService.userLoanSettings.reportsSettings;

    this.preferencesubscription = this.settingsService.preferencesChange.subscribe(
      preferences => {
        this.columnsDisplaySettings =
          preferences.loanSettings.columnsDisplaySettings;
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.preferencesubscription.unsubscribe();
  }

  private getdataforgrid() {
    let obj: any = this.localstorageservice.retrieve(environment.loankey);
    if (obj != null && obj != undefined) {
      this.localloanobject = obj;
      this.allDataFetched = true;
    }
    this.prepareData(this.localloanobject);
  }

  private prepareData(localloanobject: loan_model) {
    this.prReport = this.ccProjectedIncomeService.prepareData(
      localloanobject,
      [],
      {}
    );
  }

  onDisplayDetailsChange(event) {
    this.displayPracticeRows = event.checked;
  }
}
