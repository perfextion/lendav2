import { Injectable, Input } from "@angular/core";
import { loan_model, LoanGroup, Collateral_Category_Code } from "@lenda/models/loanmodel";
import { LocalStorageService } from "ngx-webstorage";
import { environment } from "@env/environment.prod";
import { ProjectedIncomeModel, CropRevenueModel } from "@lenda/components/reports/cross-collateral/cc-projectedincome/ccProjectedIncome.model";
import { Heading, CCCrop, CCTotalRow, TotalRow, Crop } from "@lenda/components/reports/cross-collateral/cross-collateral.model";
import * as _ from "lodash"
import { Get_Borrower_Name } from "@lenda/services/common-utils";
import { RefDataModel } from "@lenda/models/ref-data-model";
import { V_Crop_Price_Details } from "@lenda/models/cropmodel";
import { Helper } from "@lenda/services/math.helper";
@Injectable({
  providedIn: 'root'
})
export class CCProjectedIncomeService{

  constructor(private localstorageservice : LocalStorageService){

  }

  prepareData(localloanobject : loan_model, loanGroup : Array<LoanGroup>, refData) {
    let projectedIncomesOfLoans : Array<ProjectedIncomeModel> = [];

    projectedIncomesOfLoans.push(this.getProjectedIncomeModelOfLoan(localloanobject,refData));

    let crossCollateralizedLoans : Array<LoanGroup> = loanGroup.filter(lg=>lg.IsCrossCollateralized);

    crossCollateralizedLoans.forEach(loan=>{
      projectedIncomesOfLoans.push(this.getProjectedIncomeModelOfLoan(loan.LoanJSON,refData));
    });

    let ccProjectedIncomeReport : CCProjectedIncomeReport = new CCProjectedIncomeReport();

    //header
    ccProjectedIncomeReport.heading.itemHeading = 'Crop';
    ccProjectedIncomeReport.heading.loanBorrower = [];

    projectedIncomesOfLoans.forEach(loanReport =>{
      ccProjectedIncomeReport.heading.loanBorrower.push(loanReport.borrowerName);
    });

    ccProjectedIncomeReport.heading.totalHeading = 'Total';
    ccProjectedIncomeReport.heading.percentHeading = 'Revenue %';

    //crops

    let allCrops : Array<CropRevenueModel> = [];
    projectedIncomesOfLoans.forEach(loan=>{
      allCrops = allCrops.concat(loan.crops);
    });

    let distictCrops  = _.uniqBy(allCrops,(crop)=>crop.Name+'-'+crop.PracticeType);

    distictCrops.forEach(crop=>{
      let ccCrop : CCCrop = new CCCrop();
      ccCrop.name = crop.Name;
      ccCrop.cropType = crop.CropType;
      ccCrop.practiceType = crop.PracticeType;
      ccCrop.cropPracticeType = crop.CropPracticeType;
      ccCrop.isPracticeRow = crop.isPracticeRow;
      ccCrop.values = [];

      projectedIncomesOfLoans.forEach(loan=>{
        let loanCrop = loan.crops.find((crp)=>crp.Name == ccCrop.name && crp.PracticeType == ccCrop.practiceType);
        if(loanCrop) {
          ccCrop.values.push(loanCrop.Revenue);
        } else {
          ccCrop.values.push(0);
        }
      });

      ccCrop.totalValue = _.sum(ccCrop.values);
      ccProjectedIncomeReport.crops.push(ccCrop);
    });

    //other_incomes

    let other_incomes : Array<CropRevenueModel> = [];
    projectedIncomesOfLoans.forEach(loan=>{
      other_incomes = other_incomes.concat(loan.otherIncomes);
    });

    let distictother_incomes  = _.uniqBy(other_incomes, a => a.Name);

    distictother_incomes.forEach(crop => {
      let ccOT : CCCrop = new CCCrop();
      ccOT.name = crop.Name;
      ccOT.values = [];

      projectedIncomesOfLoans.forEach(loan=>{
        let income = loan.otherIncomes.find(i => i.Name == crop.Name);
        if(income) {
          ccOT.values.push(income.Revenue);
        } else {
          ccOT.values.push(0);
        }
      });

      ccOT.totalValue = _.sum(ccOT.values);
      ccProjectedIncomeReport.other_incomes.push(ccOT);
    });

    //Crop Revenue

    ccProjectedIncomeReport.cropRevenue.name = 'Crop Revenue';
    ccProjectedIncomeReport.cropRevenue.values = [];

    projectedIncomesOfLoans.forEach(loan=>{
      ccProjectedIncomeReport.cropRevenue.values.push(loan.NetCropRevenue);
    });

    ccProjectedIncomeReport.cropRevenue.totalValue = _.sum(ccProjectedIncomeReport.cropRevenue.values);


    let otherIncome : CCTotalRow = new CCTotalRow();
    otherIncome.name = 'Other Income';
    otherIncome.shouldHighlightSecondary = true;

    let totalAdditionalRevenue : CCTotalRow = new CCTotalRow();
    totalAdditionalRevenue.name = 'Subtotal Additional Revenue';
    totalAdditionalRevenue.shouldHighlightSecondary = true;
    totalAdditionalRevenue.totalRow = true;

    let totalRevenue : CCTotalRow = new CCTotalRow();
    totalRevenue.name = 'Total Revenue';
    totalRevenue.shouldHighlightSecondary = true;

    let totalExpenseBudget : CCTotalRow = new CCTotalRow();
    totalExpenseBudget.name = 'Total Budget Expense';

    let estimatedInterest : CCTotalRow = new CCTotalRow();
    estimatedInterest.name = 'Estimated Interest';

    let totalCashFlow : CCTotalRow = new CCTotalRow();
    totalCashFlow.shouldHighlight = true;
    totalCashFlow.name = 'Total Cashflow';

    projectedIncomesOfLoans.forEach(loan=>{
      otherIncome.values.push(loan.Net_Other_Income || 0);
      totalAdditionalRevenue.values.push(loan.Total_Additional_Revenue || 0);
      totalRevenue.values.push(loan.Total_Revenue || 0);
      totalExpenseBudget.values.push(loan.Total_Expense_Budget || 0);
      estimatedInterest.values.push(loan.Estimated_Interest || 0);
      totalCashFlow.values.push(loan.Total_CashFlow || 0);
    });

    otherIncome.totalValue = _.sum(otherIncome.values);
    totalAdditionalRevenue.totalValue = _.sum(totalAdditionalRevenue.values);
    totalRevenue.totalValue = _.sum(totalRevenue.values);
    totalExpenseBudget.totalValue = _.sum(totalExpenseBudget.values);
    estimatedInterest.totalValue = _.sum(estimatedInterest.values);
    totalCashFlow.totalValue = _.sum(totalCashFlow.values);

    ccProjectedIncomeReport.crops.forEach(crop=>{
      crop.perValue = crop.totalValue/totalRevenue.totalValue *100;
    });

    ccProjectedIncomeReport.other_incomes.forEach(income => {
      income.perValue = Helper.percent(income.totalValue , totalRevenue.totalValue)
    });

    ccProjectedIncomeReport.cropRevenue.perValue = ccProjectedIncomeReport.cropRevenue.totalValue / totalRevenue.totalValue*100;

    otherIncome.perValue = otherIncome.totalValue/totalRevenue.totalValue*100;
    totalAdditionalRevenue.perValue = totalAdditionalRevenue.totalValue/totalRevenue.totalValue*100;
    totalRevenue.perValue = totalRevenue.totalValue/totalRevenue.totalValue*100;
    totalExpenseBudget.perValue = totalExpenseBudget.totalValue/totalRevenue.totalValue*100;
    estimatedInterest.perValue = estimatedInterest.totalValue/totalRevenue.totalValue*100;
    totalCashFlow.perValue = totalCashFlow.totalValue/totalRevenue.totalValue*100;

    ccProjectedIncomeReport.totalRows.push(otherIncome);
    ccProjectedIncomeReport.totalRows.push(totalAdditionalRevenue);
    ccProjectedIncomeReport.totalRows.push(totalRevenue);
    ccProjectedIncomeReport.totalRows.push(totalExpenseBudget);
    ccProjectedIncomeReport.totalRows.push(estimatedInterest);
    ccProjectedIncomeReport.totalRows.push(totalCashFlow);

    projectedIncomesOfLoans.forEach(loan=>{
      ccProjectedIncomeReport.breakEven.push(loan.Breakeven);
    });

    return ccProjectedIncomeReport;
  }

  getProjectedIncomeModelOfLoan(loanObject : loan_model,refData){
    let ccPRModel = new ProjectedIncomeModel();

    if (loanObject && loanObject.LoanCrops  && loanObject.LoanMaster) {

      ccPRModel.borrowerName = Get_Borrower_Name(loanObject.LoanMaster);

      ccPRModel.crops = [];
      loanObject.LoanCrops.filter(a => a.ActionStatus != 3).forEach(crop => {
        let cropRevenue: CropRevenueModel = new CropRevenueModel();
        cropRevenue.Name = this.getCropName(crop.Crop_ID);
        cropRevenue.CropType = crop.Crop_Type_Code;
        cropRevenue.Revenue = crop.Revenue ? crop.Revenue : 0;
        ccPRModel.crops.push(cropRevenue);

        let cropPracticesIds = [];
        refData.CropList.filter(cl=>cl.Crop_Code == crop.Crop_Code).forEach(element => {
          cropPracticesIds.push(element.Crop_And_Practice_ID)
        });

        //add respective crops practie details
        let cropPractices = loanObject.LoanCropPractices.filter(cp=>cropPracticesIds.includes(cp.Crop_Practice_ID));

        cropPractices.forEach(cp=>{
          let cropRevenue: CropRevenueModel = new CropRevenueModel();

          let v_crop: V_Crop_Price_Details = this.getCrop(cp.Crop_Practice_ID, refData);
          cropRevenue.Name = v_crop.Crop_Name;
          cropRevenue.CropType = v_crop.Crop_Type_Code as string;
          cropRevenue.CropPracticeType = v_crop.Crop_Prac_Code as string;
          cropRevenue.PracticeType = v_crop.Irr_Prac_Code as string;

          cropRevenue.Revenue = cp.Market_Value ? cp.Market_Value : 0;
          cropRevenue.isPracticeRow = true;
          ccPRModel.crops.push(cropRevenue);
        });
      });

      ccPRModel.crops = ccPRModel.crops.filter(crop=>crop.Revenue);

      if (loanObject && loanObject.LoanMaster) {
        let loanMaster = loanObject.LoanMaster;

        ccPRModel.NetCropRevenue = loanMaster.Net_Market_Value_Crops ? parseInt(loanMaster.Net_Market_Value_Crops.toFixed(0)) : 0;

        ccPRModel.otherIncomes  = [];

        const _other_incomes = loanObject.LoanOtherIncomes || [];

        const other_income_data = _.sortBy(_other_incomes.filter(a => a.ActionStatus != 3), a => a.Sort_Order);
        let other_income_groups = _.groupBy(other_income_data, a => a.Other_Income_Name);

        for(let group in other_income_groups) {
          let incomes = other_income_groups[group];
          let revenuemodel = new CropRevenueModel();
          revenuemodel.Name = group;
          revenuemodel.Revenue = _.sumBy(incomes, a => a.Amount || 0);
          ccPRModel.otherIncomes.push(revenuemodel);
        }

        ccPRModel.Net_Other_Income = loanMaster.Net_Other_Income || 0;

        ccPRModel.Total_Additional_Revenue = ccPRModel.Net_Other_Income;
        ccPRModel.Total_Revenue = ccPRModel.NetCropRevenue + ccPRModel.Total_Additional_Revenue;
        ccPRModel.Total_Revenue = ccPRModel.Total_Revenue ? parseInt(ccPRModel.Total_Revenue.toFixed(0)) : 0;
        ccPRModel.Total_Expense_Budget = loanMaster.Loan_Total_Budget ? parseInt(loanMaster.Loan_Total_Budget.toFixed(0)) :0;
        ccPRModel.Estimated_Interest = loanMaster.Interest_Est_Amount ? parseInt(loanMaster.Interest_Est_Amount.toFixed(0)) : 0;
        ccPRModel.Total_CashFlow = parseInt(ccPRModel.Total_Revenue.toFixed(0)) - parseInt(ccPRModel.Total_Expense_Budget.toFixed(0)) - parseInt(ccPRModel.Estimated_Interest.toFixed(0));
        ccPRModel.Breakeven = Helper.percent(loanMaster.ARM_Commitment + loanMaster.Dist_Commitment, ccPRModel.Total_Revenue);

        ccPRModel.crops.forEach((crop)=>{
          crop.Percent = ccPRModel.Total_Revenue ? (crop.Revenue/ccPRModel.Total_Revenue*100) : 0;
        });

        ccPRModel.Per_NetCropRevenue = ccPRModel.Total_Revenue ? (ccPRModel.NetCropRevenue/ccPRModel.Total_Revenue*100) : 0;
        ccPRModel.Per_Net_Other_Income = ccPRModel.Net_Other_Income / ccPRModel.Total_Revenue *100;
        ccPRModel.Per_Total_Additional_Revenue = ccPRModel.Total_Additional_Revenue / ccPRModel.Total_Revenue *100;
        ccPRModel.Per_Total_Revenue = 100;
        ccPRModel.Per_Total_Expense_Budget = ccPRModel.Total_Expense_Budget / ccPRModel.Total_Revenue *100;
        ccPRModel.Per_Estimated_Interest = ccPRModel.Estimated_Interest / ccPRModel.Total_Revenue *100;
        ccPRModel.Per_Total_CashFlow = ccPRModel.Total_CashFlow / ccPRModel.Total_Revenue *100;

        ccPRModel.otherIncomes.forEach(income => {
          income.Percent = Helper.percent(income.Revenue, ccPRModel.Total_Revenue);
        });
      }
    }

    return ccPRModel;
  }

  getCropName(cropID){

    let refData = this.localstorageservice.retrieve(environment.referencedatakey);

    if(refData && refData.Crops && cropID){
      let crop = refData.Crops.find(c=>c.Crop_ID == cropID);
      if(crop){
        return crop.Crop_Name;
      }else{
        return '';
      }
    }else{
      return '';
    }
  }

  getCrop(Crop_And_Practice_ID: number,refData: RefDataModel) {
    let crop = refData.CropList.find(a => a.Crop_And_Practice_ID == Crop_And_Practice_ID);
    return crop || new V_Crop_Price_Details();
  }
}

export class CCProjectedIncomeReport{
  heading : Heading = new Heading();
  crops : Array<CCCrop> = [];
  other_incomes : Array<CCCrop> = [];
  cropRevenue : CCCrop = new CCCrop();
  totalRows : Array<CCTotalRow> = new Array<CCTotalRow>();
  breakEven : Array<number>= [];
}

enum ProjectedIncomeOtherVariables{
  Net_Market_Value_Livestock,
  Net_Market_Value_Stored_Crops,
  Net_Market_Value__Other
}
