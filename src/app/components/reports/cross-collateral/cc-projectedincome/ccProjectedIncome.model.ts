export class CropRevenueModel {
  Name: string;
  CropType: string;
  PracticeType: string;
  CropPracticeType: string;
  Revenue: number;
  Percent: number;
  isPracticeRow: boolean;
}

export class ProjectedIncomeModel {
  borrowerName: string;
  crops: Array<CropRevenueModel>;
  subtotalCropRevenue: number;
  NetCropRevenue: number;
  otherIncomes: Array<CropRevenueModel>;
  Net_Other_Income: number;
  Total_Additional_Revenue: number;
  Total_Revenue: number;
  Total_Expense_Budget: number;
  Estimated_Interest: number;
  Total_CashFlow: number;
  Breakeven: number;

  Per_NetCropRevenue: number;
  Per_Net_Other_Income: number;
  Per_Total_Additional_Revenue: number;
  Per_Total_Revenue: number;
  Per_Total_Expense_Budget: number;
  Per_Estimated_Interest: number;
  Per_Total_CashFlow: number;
}
