import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { RefDataModel } from '@lenda/models/ref-data-model';
import { loan_model, LoanGroup } from '@lenda/models/loanmodel';
import {
  AcresService,
  CCAcresReport
} from '@lenda/components/reports/cross-collateral/acres/acres.service';
import { ReportsSettings, Loan_Key_Visibilty } from '@lenda/models/loansettings';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';

import {
  CCProjectedIncomeService,
  CCProjectedIncomeReport
} from '../cc-projectedincome/ccProjectedIncome.service';
import { DataService } from '@lenda/services/data.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';

@Component({
  selector: 'app-acres-report',
  templateUrl: './acres.component.html',
  styleUrls: ['./acres.component.scss']
})
export class AcresComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  private preferencesubscription: ISubscription;

  @Input() public ID: string = '';
  @Input() public expanded = true;

  public localLoanObject: loan_model;
  public loanGroups: LoanGroup[];
  private refdata: RefDataModel;

  public ccAcresReport: CCAcresReport = new CCAcresReport();
  public prReport: CCProjectedIncomeReport = new CCProjectedIncomeReport();

  public reportSettings: ReportsSettings;
  public columnsDisplaySettings: ColumnsDisplaySettings;
  private LoankeySettings: Loan_Key_Visibilty;

  constructor(
    private localStorageServce: LocalStorageService,
    private dataService: DataService,
    private acresService: AcresService,
    private settingsService: SettingsService,
    private ccProjectedIncomeService: CCProjectedIncomeService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res) {
          this.localLoanObject = res;
          this.prepareData();
        }
      });

    this.localLoanObject = this.localStorageServce.retrieve(
      environment.loankey
    );

    this.loanGroups = this.localStorageServce.retrieve(environment.loanGroup);
    this.refdata = this.localStorageServce.retrieve(
      environment.referencedatakey
    );

    this.prepareData();
    this.getUserPreferences();
  }

  private getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;
    this.reportSettings = this.settingsService.userLoanSettings.reportsSettings;

    this.preferencesubscription = this.settingsService.preferencesChange.subscribe(
      preferences => {
        this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
        this.hideUnhideLoanKeys();
      }
    );
  }

  private hideUnhideLoanKeys() {
    try {
      let LoanSettings = JSON.parse(
        this.localLoanObject.LoanMaster.Loan_Settings
      );

      if (LoanSettings && LoanSettings.Loan_key_Settings) {
        this.LoankeySettings = LoanSettings.Loan_key_Settings;
      } else {
        this.LoankeySettings = new Loan_Key_Visibilty();
      }
    } catch {
      this.LoankeySettings = new Loan_Key_Visibilty();
    }

    this.columnsDisplaySettings.cropPrac = this.LoankeySettings.Crop_Practice == null ? this.columnsDisplaySettings.cropPrac : this.LoankeySettings.Crop_Practice;
    this.columnsDisplaySettings.cropType = this.LoankeySettings.Crop_Type == null ? this.columnsDisplaySettings.cropType : this.LoankeySettings.Crop_Type;
  }

  private prepareData() {
    this.ccAcresReport = this.acresService.prepareData(
      this.localLoanObject,
      this.loanGroups,
      this.refdata
    );

    this.prReport = this.ccProjectedIncomeService.prepareData(
      this.localLoanObject,
      this.loanGroups,
      this.refdata
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.preferencesubscription.unsubscribe();
  }
}
