import { Injectable } from '@angular/core';

import * as _ from 'lodash';

import { loan_model, LoanGroup, Loan_Crop } from '@lenda/models/loanmodel';
import {
  Crop,
  CCTotalRow,
  CCCrop,
  Heading
} from '@lenda/components/reports/cross-collateral/cross-collateral.model';
import { Get_Borrower_Name } from '@lenda/services/common-utils';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { V_Crop_Price_Details } from '@lenda/models/cropmodel';

@Injectable({
  providedIn: 'root'
})
export class AcresService {
  constructor() {}

  prepareData(
    localLoanObject: loan_model,
    loanGroups: Array<LoanGroup>,
    refData
  ) {
    let acresReports: Array<AcresReportModel> = [];
    acresReports.push(this.getAcreModelForLoan(localLoanObject, refData));

    let crossCollateralizedLoans: Array<LoanGroup> = loanGroups.filter(
      lg => lg.IsCrossCollateralized
    );

    if (crossCollateralizedLoans) {
      crossCollateralizedLoans.forEach(loan => {
        acresReports.push(this.getAcreModelForLoan(loan.LoanJSON, refData));
      });
    }

    return this.processAcresReportsModel(acresReports);
  }

  private processAcresReportsModel(acresReports: Array<AcresReportModel>) {
    let crops: Array<Crop> = [];
    acresReports.forEach(loan => {
      crops = crops.concat(loan.crops);
    });
    crops = _.uniqBy(crops, crop => crop.name + '-' + crop.practiceType);

    let ccReport: CCAcresReport = new CCAcresReport();
    //header
    ccReport.heading.itemHeading = 'Acres';
    ccReport.heading.loanBorrower = [];
    acresReports.forEach(loanReport => {
      ccReport.heading.loanBorrower.push(loanReport.borrowerName);
    });
    ccReport.heading.itemHeading = 'Total';
    ccReport.heading.percentHeading = 'Acres %';
    //crops
    crops.forEach(crop => {
      let ccCrop: CCCrop = new CCCrop();
      ccCrop.name = crop.name;
      ccCrop.practiceType = crop.practiceType;
      ccCrop.isPracticeRow = crop.isPracticeRow;
      ccCrop.cropType = crop.cropType;
      ccCrop.cropPracticeType = crop.cropPracticeType;
      ccCrop.values = [];
      acresReports.forEach(loan => {
        ccCrop.values.push(this.getAcresOfLoanCrop(loan, crop));
      });

      ccCrop.totalValue = _.sum(ccCrop.values);
      ccReport.crops.push(ccCrop);
    });

    //totals
    ccReport.totalRow.values = [];
    acresReports.forEach(loan => {
      ccReport.totalRow.values.push(loan.totalAcres);
    });
    ccReport.totalRow.totalValue = _.sum(ccReport.totalRow.values);
    ccReport.totalRow.perValue =
      (ccReport.totalRow.totalValue * 100) / ccReport.totalRow.totalValue;

    //crops percentage
    ccReport.crops.forEach(crop => {
      crop.perValue = (crop.totalValue * 100) / ccReport.totalRow.totalValue;
    });

    return ccReport;
  }

  private getAcresOfLoanCrop(loan: AcresReportModel, crop: Crop) {
    let cropFound = loan.crops.find(
      crp => crp.name == crop.name && crp.practiceType == crop.practiceType
    );
    if (cropFound) {
      return cropFound.value;
    } else {
      return 0;
    }
  }

  private getAcreModelForLoan(loanObject: loan_model, refData) {
    let acresReportModel: AcresReportModel = new AcresReportModel();

    if (loanObject) {
      if (loanObject.LoanMaster) {
        acresReportModel.borrowerName = Get_Borrower_Name(
          loanObject.LoanMaster
        );
      }
      acresReportModel.crops = [];
      loanObject.LoanCrops.filter(a => a.ActionStatus != 3).forEach(lc => {
        let crop = new Crop();
        crop.name = this.getCropName(lc, refData);
        crop.cropType = lc.Crop_Type_Code;
        crop.value = lc.Acres;
        acresReportModel.crops.push(crop);

        let cropPracticesIds = [];
        refData.CropList.filter(cl => cl.Crop_Code == lc.Crop_Code).forEach(
          element => {
            cropPracticesIds.push(element.Crop_And_Practice_ID);
          }
        );

        let cropPractices = loanObject.LoanCropPractices.filter(cp =>
          cropPracticesIds.includes(cp.Crop_Practice_ID)
        );

        cropPractices.forEach(cp => {
          let cropPractice = new Crop();
          cropPractice.name = crop.name;

          let v_crop: V_Crop_Price_Details = this.getCrop(
            cp.Crop_Practice_ID,
            refData
          );
          cropPractice.cropType = v_crop.Crop_Type_Code as string;
          cropPractice.practiceType = v_crop.Irr_Prac_Code as string;
          cropPractice.cropPracticeType = v_crop.Crop_Prac_Code as string;

          cropPractice.value = cp.LCP_Acres;
          cropPractice.isPracticeRow = true;
          acresReportModel.crops.push(cropPractice);
        });
      });

      //sum only crop items not crop practice items
      acresReportModel.totalAcres = _.sumBy(acresReportModel.crops, crop =>
        crop.practiceType ? 0 : crop.value
      );
      acresReportModel.totalPerAcres = 100;

      acresReportModel.crops.forEach(crop => {
        crop.perValue = (crop.value * 100) / acresReportModel.totalAcres;
      });
    }

    return acresReportModel;
  }

  private getCropName(lc:  Loan_Crop, refData) {
    if (refData && refData.CropList && lc) {
      let crop = refData.CropList.find(c => c.Crop_Code == lc.Crop_Code);
      if (crop) {
        return crop.Crop_Name;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  private getCrop(Crop_And_Practice_ID: number, refData: RefDataModel) {
    let crop = refData.CropList.find(
      a => a.Crop_And_Practice_ID == Crop_And_Practice_ID
    );
    return crop || new V_Crop_Price_Details();
  }
}

export class CCAcresReport {
  heading: Heading = new Heading();
  crops: Array<CCCrop> = [];
  totalRow: CCTotalRow = new CCTotalRow();
}

class AcresReportModel {
  borrowerName: string;
  crops: Array<Crop> = [];
  totalAcres: number;
  totalPerAcres: number;
}
