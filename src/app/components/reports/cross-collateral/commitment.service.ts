import { Injectable } from '@angular/core';

import * as _  from 'lodash';

import { LoanMaster } from '@lenda/models/ref-data-model';

@Injectable({
  providedIn: 'root'
})
export class CommitmentService {
  constructor() {}

  getArmDistCommitments(loanMaster: LoanMaster): CommitmentSummary {
    let commitmentSummary = new CommitmentSummary();
    let arm = new Commitment();

    arm.name = 'ARM';
    arm.value = loanMaster.ARM_Commitment;
    commitmentSummary.commitments.push(arm);

    let dist = new Commitment();
    dist.name = 'Distributer';
    dist.value = loanMaster.Dist_Commitment;
    commitmentSummary.commitments.push(dist);

    commitmentSummary =  this.popuateTotalCommitmentAndShare(commitmentSummary);

    commitmentSummary.totalPerCommitment = 100;

    return commitmentSummary;
  }

  private popuateTotalCommitmentAndShare(
    commitmentSummary: CommitmentSummary,
    loanMaster : any = {} ,
    type : CommitmentType = CommitmentType.ARMDIST
  ): CommitmentSummary {
    if(type== CommitmentType.RETURN){

      commitmentSummary.totalCommitment = loanMaster.Total_Return_Fee;
      commitmentSummary.commitments.forEach(com => {
        com.perValue = (100*com.value)/loanMaster.ARM_Commitment;
      });
      commitmentSummary.totalARMFeeValue = _.sumBy(commitmentSummary.commitments,'armFeeValue');
      commitmentSummary.totalDistFeeValue = _.sumBy(commitmentSummary.commitments,'distFeeValue');
    }else{
      commitmentSummary.totalCommitment = _.sumBy(commitmentSummary.commitments,'value');
      commitmentSummary.commitments.forEach(com => {
        com.perValue = (100*com.value)/commitmentSummary.totalCommitment;
      });
    }

    return commitmentSummary;
  }

  getReturnCommitments(loanMaster: LoanMaster): CommitmentSummary {
    let commitmentSummary = new CommitmentSummary();
    let armCommitment = loanMaster.ARM_Commitment;
    let distCommitment = loanMaster.Dist_Commitment;

    let org = new Commitment();
    org.name = 'Origination Fee';
    org.value = loanMaster.Origination_Fee_Amount;
    org.committeeFeePercent = loanMaster.Origination_Fee_Percent;
    org.armFeeValue = loanMaster.Origination_Fee_Percent;
    commitmentSummary.commitments.push(org);

    let srvs = new Commitment();
    srvs.name = 'Service Fee';
    srvs.value = loanMaster.Service_Fee_Amount;
    srvs.committeeFeePercent = loanMaster.Service_Fee_Percent;
    srvs.armFeeValue = loanMaster.Service_Fee_Amount;
    commitmentSummary.commitments.push(srvs);

    let extension = new Commitment();
    extension.name = 'Extension Fee';
    extension.value = loanMaster.Extension_Fee_Amount || 0;
    extension.committeeFeePercent = loanMaster.Extension_Fee_Percent || 0;
    extension.armFeeValue = loanMaster.Extension_Fee_Amount || 0;
    commitmentSummary.commitments.push(extension);

    let int = new Commitment();
    int.name = ' Est Interest';
    int.value = loanMaster.Interest_Est_Amount;
    int.committeeFeePercent = loanMaster.Interest_Percent;
    int.armFeeValue = loanMaster.ARM_Rate_Fee;
    int.distFeeValue = loanMaster.Dist_Rate_Fee;
    commitmentSummary.commitments.push(int);

    commitmentSummary =  this.popuateTotalCommitmentAndShare(commitmentSummary, loanMaster, CommitmentType.RETURN);

    commitmentSummary.totalPerCommitment = (commitmentSummary.totalCommitment / loanMaster.ARM_Commitment )*100;

    commitmentSummary.days = loanMaster.Estimated_Days;
    commitmentSummary.return_per = loanMaster.Return_Percent;
    commitmentSummary.returnArmSum = loanMaster.Return_Percent * armCommitment;
    commitmentSummary.armcommit = armCommitment;

    return commitmentSummary;
  }
}

export class Commitment {
  name: string;
  value: number;
  perValue: number;
  committeeFeePercent : number;
  armFeeValue : number;
  distFeeValue : number;
}

export class CommitmentSummary {
  commitments : Array<Commitment> = [];
  armcommit: number;
  totalCommitment : number;
  totalPerCommitment : number;
  totalARMFeeValue : number;
  totalDistFeeValue : number;
  //for Cross collateral Return
  days : number;
  return_per : number;
  returnArmSum: number = 0;
}

export enum CommitmentType{
  ARMDIST = 'ARMDIST',
  RETURN = 'RETURN'
}
