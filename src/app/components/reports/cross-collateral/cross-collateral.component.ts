import { Component, OnInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment';
import { Loansettings } from '@lenda/models/loansettings';

import { CollateralValueKey } from '../collateral-report.service';
import { CommitmentType } from '@lenda/components/reports/cross-collateral/commitment.service';
import { MasterService } from '@lenda/master/master.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';

@Component({
  selector: 'app-cross-collateral-report',
  templateUrl: './cross-collateral.component.html',
  styleUrls: ['./cross-collateral.component.scss']
})
export class CrossCollateralReportComponent implements OnInit {
  public CollateralValueKey : typeof CollateralValueKey = CollateralValueKey;
  public CommitmentType : typeof CommitmentType = CommitmentType;
  public showARMDetails : boolean = true;
  public showDiscValues : boolean = true;
  public tableColToDisplay: any = null;

  public loanSetting: Loansettings = new Loansettings();

  constructor(
    private localstorageservice: LocalStorageService,
    public masterSvc: MasterService,
    private settingsService: SettingsService
  ) {
    let localLoanObject = this.localstorageservice.retrieve(environment.loankey);
    if(!_.isEmpty(localLoanObject)) {
      this.tableColToDisplay = JSON.parse(localLoanObject.LoanMaster.Loan_Settings).Loan_key_Settings;
    }

    this.loanSetting = this.settingsService.userLoanSettings;
  }

  ngOnInit() { }

  public onARMDetailsChange(detailsState: boolean) {
    this.showARMDetails = detailsState;
  }

  public onDiscValuesChange(detailsState: boolean) {
    this.showDiscValues = detailsState;
  }
}
