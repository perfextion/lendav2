import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { Loan_Budget, loan_model, LoanGroup } from '@lenda/models/loanmodel';
import { Get_Borrower_Name } from '@lenda/services/common-utils';

import { DataService } from '@lenda/services/data.service';
import { BudgetHelperService } from '@lenda/components/budget/budget-helper.service';

@Component({
  selector: 'app-cc-budget-report',
  templateUrl: './cc-budget-report.component.html',
  styleUrls: ['./cc-budget-report.component.scss']
})
export class CCBudgetReportComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  budgets: Array<Loan_Budget>;
  borrowerName: string = '';
  ccBudgetReport: CCBudgetReport = new CCBudgetReport();
  @Input() withRevenue: boolean = false;
  @Input() withReportSubHeading: boolean = false;
  @Input() cropName: string = undefined;
  @Input() ID: string = '';
  @Input() expanded = true;

  constructor(
    public localstorageservice: LocalStorageService,
    public budgetService: BudgetHelperService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res != undefined && res != null) {
          let loanObject: loan_model = res;
          this.getData(loanObject);
        }
      });

    let loanObject: loan_model = this.localstorageservice.retrieve(
      environment.loankey
    );

    this.getData(loanObject);
  }

  private getData(loanObject: loan_model) {
    if (loanObject &&
      loanObject.LoanCropPractices &&
      loanObject.LoanBudget &&
      loanObject.LoanMaster) {
      this.prepareBudgetData(loanObject);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private prepareBudgetData(localLoanObject: loan_model): any {
    let loanBudgets: Array<Array<Loan_Budget>> = [];

    loanBudgets.push(this.getLoanBudget(localLoanObject));

    this.ccBudgetReport.borrowers.push(
      Get_Borrower_Name(localLoanObject.LoanMaster)
    );

    let loanGroups: Array<LoanGroup> = this.localstorageservice.retrieve(environment.loanGroup) || [];
    let crossCollateralized: Array<LoanGroup> = loanGroups.filter(
      lg => lg.IsCrossCollateralized
    );

    crossCollateralized.forEach(loan => {
      loanBudgets.push(this.getLoanBudget(loan.LoanJSON));
      this.ccBudgetReport.borrowers.push(
        Get_Borrower_Name(loan.LoanJSON.LoanMaster)
      );
    });

    let allBudgets: Array<{
      id: number;
      name: string;
      sort_order: number;
    }> = [];

    loanBudgets.forEach(loanBudgets => {
      loanBudgets.forEach(budget => {
        allBudgets.push({
          id: budget.Expense_Type_ID || budget.Loan_Budget_ID,
          name: budget.Expense_Type_Name,
          sort_order: budget.Sort_order || budget.Loan_Budget_ID
        });
      });
    });

    allBudgets = _.uniqBy(allBudgets, 'id');

    allBudgets = _.sortBy(allBudgets, a => a.sort_order);

    allBudgets.forEach(budget => {
      let budgetType: CCBudgetRow = new CCBudgetRow();
      budgetType.name = budget.name;
      budgetType.budgetTypeId = budget.id;

      loanBudgets.forEach(budgets => {
        let matchingBudget = budgets.find(b => {
          if (b.Expense_Type_ID) {
            return b.Expense_Type_ID == budgetType.budgetTypeId;
          }
          return b.Loan_Budget_ID == budgetType.budgetTypeId;
        });

        if (matchingBudget) {
          budgetType.budgets.push(matchingBudget.Total_Budget_Crop_ET);
        } else {
          budgetType.budgets.push(0);
        }
      });

      this.ccBudgetReport.budgetTypes.push(budgetType);
    });

    this.ccBudgetReport.budgetTypes.forEach(type => {
      type.totalBudget = _.sum(type.budgets);
    });

    this.ccBudgetReport.budgetTotal.budgets = [];
    loanBudgets.forEach(budgets => {
      this.ccBudgetReport.budgetTotal.budgets.push(
        _.sumBy(budgets, 'Total_Budget_Crop_ET')
      );
    });

    this.ccBudgetReport.budgetTotal.totalBudget = _.sum(
      this.ccBudgetReport.budgetTotal.budgets
    );

    this.ccBudgetReport.budgetTypes.forEach(budgetType => {
      budgetType.perBudget =
        (budgetType.totalBudget / this.ccBudgetReport.budgetTotal.totalBudget) *
        100;
    });

    this.ccBudgetReport.budgetTotal.perBudget = 100;

    //interest
    this.ccBudgetReport.feeAndInterest.budgets.push(
      localLoanObject.LoanMaster.Interest_Est_Amount || 0
    );

    crossCollateralized.forEach(loan => {
      this.ccBudgetReport.feeAndInterest.budgets.push(
        loan.LoanJSON.LoanMaster.Interest_Est_Amount || 0
      );
    });

    this.ccBudgetReport.feeAndInterest.totalBudget = _.sum(
      this.ccBudgetReport.feeAndInterest.budgets
    );

    this.ccBudgetReport.feeAndInterest.perBudget =
      (this.ccBudgetReport.feeAndInterest.totalBudget /
        this.ccBudgetReport.budgetTotal.totalBudget) *
      100;
  }

  getLoanBudget(loanObject: loan_model) {
    let preparedCP = this.budgetService.prepareCropPractice(
      loanObject.LoanCropPractices
    );

    this.budgets = this.budgetService.getTotalTableData(
      loanObject.LoanBudget,
      preparedCP,
      loanObject.Loan_Full_ID
    );

    this.budgets = _.sortBy(
      this.budgets,
      a => a.Sort_order || a.Expense_Type_ID || a.Loan_Budget_ID
    );

    return this.budgets.filter(bgt => bgt.Total_Budget_Crop_ET);
  }
}

export class CCBudgetReport {
  borrowers: Array<string> = [];
  budgetTypes: Array<CCBudgetRow> = [];
  budgetTotal: CCBudgetRow = new CCBudgetRow();
  feeAndInterest: CCBudgetRow = new CCBudgetRow();
}

export class CCBudgetRow {
  name: string;
  budgetTypeId: number;
  budgets: Array<number> = [];
  totalBudget: number;
  perBudget: number;
}
