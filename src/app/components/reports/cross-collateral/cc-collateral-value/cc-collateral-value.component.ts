import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { loan_model, LoanGroup } from '@lenda/models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';
import {
  CollateralReportService,
  CollateralReport,
  CollateralValueKey,
  Value,
  CollateralType
} from '@lenda/components/reports/collateral-report.service';
import { ISubscription } from 'rxjs/Subscription';
import { DataService } from '@lenda/services/data.service';
import * as _ from 'lodash';
import { Get_Borrower_Name } from '@lenda/services/common-utils';
import { Helper } from '@lenda/services/math.helper';
@Component({
  selector: 'app-cc-collateral-value',
  templateUrl: './cc-collateral-value.component.html',
  styleUrls: ['./cc-collateral-value.component.scss']
})
export class CCCollateralValueComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  localLoanObject: loan_model;
  collateralReports: Array<CollateralReport> = [];
  ccCollateralReport: CCCollateralReport = new CCCollateralReport();
  CollateralValueKey: typeof CollateralValueKey = CollateralValueKey;

  @Input() header: string = 'Collateral Value';
  @Input() showARMDetails: boolean = true;
  @Input() ID: string = '';
  @Input() expanded = true;

  reportValues: Array<CollateralValueReport> = [];
  CollateralKeys = [
    {
      key: CollateralValueKey.Market,
      header: 'Mkt Value'
    },
    {
      key: CollateralValueKey.DiscMarket,
      header: 'Disc Mkt Value'
    },
    {
      key: CollateralValueKey.Insurance,
      header: 'Ins Value'
    },
    {
      key: CollateralValueKey.DiscInsurance,
      header: 'Disc Ins Value'
    }
  ];

  constructor(
    private localStorageServce: LocalStorageService,
    private collateralReportService: CollateralReportService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res) {
          this.localLoanObject = res;
          this.prepareData();
        }
      });

    this.localLoanObject = this.localStorageServce.retrieve(environment.loankey);
    this.prepareData();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  prepareData() {
    let loanGroup: Array<LoanGroup> =
      this.localStorageServce.retrieve(environment.loanGroup) || [];

    this.reportValues = [];

    this.CollateralKeys.forEach(collateralKey => {
      this.reportValues.push({
        collateralValueKey: collateralKey.key,
        header: collateralKey.header,
        ccCollateralReport: this.getReportValue(loanGroup, collateralKey.key)
      });
    });
  }

  getReportValue(
    loanGroup: Array<LoanGroup>,
    colKey: CollateralValueKey
  ) {
    let ccCollateralReport = new CCCollateralReport();
    this.collateralReports = [];

    if (this.localLoanObject) {

      if (this.localLoanObject.LoanMaster) {
        ccCollateralReport.borrowers.push(Get_Borrower_Name(this.localLoanObject.LoanMaster));
        this.collateralReports.push(
          this.collateralReportService.getCollateralValue(this.localLoanObject,[colKey])
        );
      }

      let crossCollateralized: Array<LoanGroup> = loanGroup.filter(
        lg => lg.IsCrossCollateralized
      );
      crossCollateralized.forEach(loan => {
        ccCollateralReport.borrowers.push(
          Get_Borrower_Name(loan.LoanJSON.LoanMaster)
        );

        this.collateralReports.push(this.collateralReportService.getCollateralValue(loan.LoanJSON, [colKey ]));
      });

      let allCollteralTypes: Array<CollateralType> = [];

      this.collateralReports.forEach(loanReport => {
        allCollteralTypes = allCollteralTypes.concat(loanReport.collateralTypes);
      });

      allCollteralTypes = _.uniqBy(allCollteralTypes, 'typeName');

      allCollteralTypes.forEach(clType => {
        let ccClTypeRow: CCCollateralRow = new CCCollateralRow();
        ccClTypeRow.name = clType.typeName;

        this.collateralReports.forEach(loanReport => {
          let loanClType = loanReport.collateralTypes.find(
            clType => clType.typeName == ccClTypeRow.name
          );
          if (loanClType) {
            ccClTypeRow.values.push(loanClType.value[colKey]);
          } else {
            ccClTypeRow.values.push(0);
          }
        });

        ccClTypeRow.totalValue = _.sum(ccClTypeRow.values);
        ccCollateralReport.collateralTypes.push(ccClTypeRow);
      });

      this.collateralReports.forEach(loanReport => {
        ccCollateralReport.totalCollateral.values.push(loanReport.totalCollateral[colKey] || 0);
        ccCollateralReport.LTV.values.push(loanReport.LTV[colKey] || 0);
        ccCollateralReport.CEI.values.push(loanReport.CEI[colKey] || 0);
        ccCollateralReport.agPro.values.push(loanReport.agPro);
        ccCollateralReport.armCommit.values.push(loanReport.armCommit);
        ccCollateralReport.distCommit.values.push(loanReport.distCommit);
        ccCollateralReport.armEstInterest.values.push(loanReport.armEstInterest);
        ccCollateralReport.distEstInterest.values.push(loanReport.distEstInterest);
        ccCollateralReport.armMargin.values.push(loanReport.armMargin[colKey] || 0);
        ccCollateralReport.totalMargin.values.push(loanReport.totalMargin[colKey] || 0);
      });

      ccCollateralReport.totalCollateral.totalValue = _.sum(ccCollateralReport.totalCollateral.values);
      ccCollateralReport.LTV.totalValue = _.sum(ccCollateralReport.LTV.values);
      ccCollateralReport.CEI.totalValue = _.sum(ccCollateralReport.CEI.values);
      ccCollateralReport.agPro.totalValue = _.sum(ccCollateralReport.agPro.values);
      ccCollateralReport.armCommit.totalValue = _.sum(ccCollateralReport.armCommit.values);
      ccCollateralReport.distCommit.totalValue = _.sum(ccCollateralReport.distCommit.values);
      ccCollateralReport.armEstInterest.totalValue = _.sum(ccCollateralReport.armEstInterest.values);
      ccCollateralReport.distEstInterest.totalValue = _.sum(ccCollateralReport.distEstInterest.values);
      ccCollateralReport.armMargin.totalValue = _.sum(ccCollateralReport.armMargin.values);
      ccCollateralReport.totalMargin.totalValue = _.sum(ccCollateralReport.totalMargin.values);

      ccCollateralReport.LTVValue = Helper.percent(ccCollateralReport.armCommit.totalValue + ccCollateralReport.armEstInterest.totalValue, ccCollateralReport.totalCollateral.totalValue);

      if (colKey == CollateralValueKey.DiscInsurance) {
        ccCollateralReport.RCValue = (ccCollateralReport.armMargin.totalValue / this.localLoanObject.LoanMaster.ARM_Commitment) * 100;
        this.storeRisk();
      }
    }

    return ccCollateralReport;
  }

  private storeRisk() {
    let lg = this.localStorageServce.retrieve(environment.loanGroup);

    if (!lg) {
      lg = [];
    }

    lg[0].RC = this.ccCollateralReport.RCValue;
    this.localStorageServce.store(environment.loanGroup, lg);
  }

  getValue(object, key) {
    if (object && !isNaN(object[key])) {
      return parseFloat(object[key]).toFixed(0);
    } else {
      return '0';
    }
  }
}

export class CCCollateralReport {
  borrowers: Array<string> = [];
  collateralTypes: Array<CCCollateralRow> = [];
  totalCollateral: CCCollateralRow = new CCCollateralRow();
  armMargin: CCCollateralRow = new CCCollateralRow();
  LTV: CCCollateralRow = new CCCollateralRow();
  CEI: CCCollateralRow = new CCCollateralRow();
  totalMargin: CCCollateralRow = new CCCollateralRow();
  agPro: CCCollateralRow = new CCCollateralRow();
  RCValue: number;
  LTVValue: number;
  armCommit: CCCollateralRow = new CCCollateralRow();
  distCommit: CCCollateralRow = new CCCollateralRow();
  armEstInterest: CCCollateralRow = new CCCollateralRow();
  distEstInterest: CCCollateralRow = new CCCollateralRow();
}

export class CCCollateralRow {
  name: string;
  values: Array<number> = [];
  totalValue: number;
}

export class CollateralValueReport {
  collateralValueKey: CollateralValueKey;
  ccCollateralReport: CCCollateralReport;
  header: string;
}
