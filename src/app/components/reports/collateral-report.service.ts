import { Injectable } from '@angular/core';
import * as _ from 'lodash';

import { loan_model } from '../../models/loanmodel';

import { Helper } from '@lenda/services/math.helper';
import { CollateralHelper } from '../collateral/collateral-helper.service';

@Injectable({
  providedIn: 'root'
})
export class CollateralReportService {
  public readonly CollateralItems = CollateralHelper.CollateralItems;

  constructor() { }

  public getCollateralValue(
    localLoanObject : loan_model,
    collateralValueKeys : Array<CollateralValueKey>
  ) {
    let collateralReport = new CollateralReport();

    if(localLoanObject && localLoanObject.LoanCollateral && localLoanObject.LoanMaster){
      let collateralTypes = this.CollateralItems;
      let collateralTypeValues : Array<CollateralType> =[];

      //Non Collateral types: Crops , CEI
      collateralTypeValues.push({
        typeName : 'Crop',
        typeCode : 'Crop',
        value : new Value()
      });

      //collateral types
      collateralTypes.forEach(type => {
        let collateral = new CollateralType();
        collateral.typeName = type.value;
        collateral.typeCode = type.key;
        collateralTypeValues.push(collateral);
      });
      // collateralTypeValues.push({typeName : 'CEI',typeCode : 'CEI', value : new Value()});

      let nonZeroCollateralTypeValues : Array<CollateralType> =[];

      collateralTypeValues.forEach(collateral => {
        if(collateral.typeCode === 'Crop'){
          collateral.value.Mkt = localLoanObject.LoanMaster.Net_Market_Value_Crops || 0;
          collateral.value.DiscMkt = localLoanObject.LoanMaster.Disc_value_Crops || 0;
          collateral.value.Ins = localLoanObject.LoanMaster.Net_Market_Value_Insurance || 0;
          collateral.value.DiscIns = localLoanObject.LoanMaster.Disc_value_Insurance || 0;
        } else {
          let collaterals = localLoanObject.LoanCollateral.filter(col=>col.Collateral_Category_Code == collateral.typeCode);
          collateral.value = new Value();
          collateral.value.Mkt = _.sumBy(collaterals,(col)=> col.Net_Market_Value ? parseFloat(col.Net_Market_Value+'') : 0);
          collateral.value.Ins = _.sumBy(collaterals,(col)=>col.Insurance_Value ? parseFloat(col.Insurance_Value+'') : 0 );
          collateral.value.DiscMkt = _.sumBy(collaterals,(col)=>col.Disc_Mkt_Value ? parseFloat(col.Disc_Mkt_Value+'') : 0 );
          collateral.value.DiscIns = _.sumBy(collaterals,(col)=>col.Insurance_Disc_Value ? parseFloat(col.Insurance_Disc_Value+'') : 0 );
        }

        let isNonZero = false;
        collateralValueKeys.some(key => {
          if(collateral.value[key]){
            isNonZero = true;
            return true;
          }
        });

        if(isNonZero) {
          nonZeroCollateralTypeValues.push(collateral);
        }
      });

      //total collateral
      collateralReport.collateralTypes = nonZeroCollateralTypeValues;
      collateralReport.totalCollateral = new Value();
      collateralReport.totalCollateral.Mkt = _.sumBy(nonZeroCollateralTypeValues,'value.Mkt');
      collateralReport.totalCollateral.Ins = _.sumBy(nonZeroCollateralTypeValues,'value.Ins');
      collateralReport.totalCollateral.DiscMkt = _.sumBy(nonZeroCollateralTypeValues,'value.DiscMkt');
      collateralReport.totalCollateral.DiscIns = _.sumBy(nonZeroCollateralTypeValues,'value.DiscIns');

      //arm commitment
      let armCommitment = localLoanObject.LoanMaster.ARM_Commitment;
      let armInterest = localLoanObject.LoanMaster.ARM_Rate_Fee;

      //total commitment
      let totalCommitment = localLoanObject.LoanMaster.ARM_Commitment + localLoanObject.LoanMaster.Dist_Commitment;
      let totalInterest = localLoanObject.LoanMaster.Interest_Est_Amount;

      collateralReport.armCommit = armCommitment || 0;
      collateralReport.armEstInterest = armInterest;
      collateralReport.distCommit = localLoanObject.LoanMaster.Dist_Commitment || 0;
      collateralReport.distEstInterest = localLoanObject.LoanMaster.Dist_Rate_Fee || 0;

      const CEI_And_AG_PRO = Math.round((localLoanObject.LoanMaster.CEI_Value || 0) + (localLoanObject.LoanMaster.Ag_Pro_Requested_Credit || 0));

      //LTV
      // LTV = ARM Loan = ( ARM Commit + ARM Interest) / Collateral Value
      const totalLoan = armCommitment + armInterest;

      collateralReport.LTV = new Value();
      collateralReport.LTV.Mkt = Helper.percent(totalLoan, collateralReport.totalCollateral.Mkt);
      collateralReport.LTV.Ins = Helper.percent(totalLoan, collateralReport.totalCollateral.Ins);
      collateralReport.LTV.DiscMkt = Helper.percent(totalLoan, collateralReport.totalCollateral.DiscMkt);
      collateralReport.LTV.DiscIns = Helper.percent(totalLoan, collateralReport.totalCollateral.DiscIns);

      //CEI
      collateralReport.CEI = new Value();
      collateralReport.CEI.DiscIns = localLoanObject.LoanMaster.CEI_Value || 0;

      //ARM margin
      collateralReport.armMargin = new Value();
      collateralReport.armMargin.Mkt = collateralReport.totalCollateral.Mkt - (armCommitment + armInterest);
      collateralReport.armMargin.Ins = collateralReport.totalCollateral.Ins - (armCommitment + armInterest);
      collateralReport.armMargin.DiscMkt = collateralReport.totalCollateral.DiscMkt - (armCommitment + armInterest);
      collateralReport.armMargin.DiscIns = collateralReport.totalCollateral.DiscIns + CEI_And_AG_PRO - (armCommitment + armInterest);

      //total margin
      collateralReport.totalMargin = new Value();
      collateralReport.totalMargin.Mkt = collateralReport.totalCollateral.Mkt - (totalCommitment + totalInterest);
      collateralReport.totalMargin.Ins = collateralReport.totalCollateral.Ins - (totalCommitment + totalInterest);
      collateralReport.totalMargin.DiscMkt = collateralReport.totalCollateral.DiscMkt - (totalCommitment + totalInterest);
      collateralReport.totalMargin.DiscIns = collateralReport.totalCollateral.DiscIns + CEI_And_AG_PRO - (totalCommitment + totalInterest);

      collateralReport.RCValue =  (collateralReport.armMargin.DiscIns/armCommitment)*100;
      collateralReport.agPro = localLoanObject.LoanMaster.Ag_Pro_Requested_Credit || 0;

      if(collateralValueKeys.length>1){
        //if the single table needs to display all 4 kind of value (Mkt, DiscMkt, Ins, DisIns)
        collateralReport.LTVValue =  collateralReport.totalCollateral.Ins/localLoanObject.LoanMaster.ARM_Commitment  *100;
      } else {
        //if specific value is asked, use it' total collteral instead of Ins
        collateralReport.LTVValue =  collateralReport.totalCollateral[collateralValueKeys[0]]/localLoanObject.LoanMaster.ARM_Commitment  *100;
      }

      return collateralReport;
    } else {
      return undefined;
    }
  }

  public getCollateralValueForSchematics(
    localLoanObject : loan_model,
    collateralValueKeys : Array<CollateralValueKey>
  ) {
    let collateralReport = new CollateralReport();
    if(localLoanObject && localLoanObject.LoanCollateral && localLoanObject.LoanMaster){
      let collateralTypes = this.CollateralItems;
      let collateralTypeValues : Array<CollateralType> =[];

      //Non Collateral types: Crops , CEI
      collateralTypeValues.push({
        typeName : 'Crop',
        typeCode : 'Crop',
        value : new Value()
      });

      //collateral types
      collateralTypes.forEach(type => {
        let collateral = new CollateralType();
        collateral.typeName = type.value;
        collateral.typeCode = type.key;
        collateralTypeValues.push(collateral);
      });
      // collateralTypeValues.push({typeName : 'CEI',typeCode : 'CEI', value : new Value()});

      let nonZeroCollateralTypeValues : Array<CollateralType> =[];
      collateralTypeValues.forEach(collateral => {
        if(collateral.typeCode === 'Crop') {
          collateral.value.Mkt = _.sumBy(localLoanObject.LoanCropPractices, (cp)=>cp.Market_Value || 0);
          collateral.value.DiscMkt = _.sumBy(localLoanObject.LoanCropPractices, (cp)=>cp.Disc_Market_Value || 0);
          collateral.value.Ins = _.sumBy(localLoanObject.LoanCropPractices, (cp)=>cp.FC_LCP_Ins_Value || 0);
          collateral.value.DiscIns = _.sumBy(localLoanObject.LoanCropPractices, (cp)=>cp.FC_LCP_Disc_Ins_Value || 0);
        } else {
          let collaterals = localLoanObject.LoanCollateral.filter(col=>col.Collateral_Category_Code == collateral.typeCode);
          collateral.value = new Value();
          collateral.value.Mkt = _.sumBy(collaterals,(col)=> col.Net_Market_Value ? parseFloat(col.Net_Market_Value+'') : 0);
          collateral.value.Ins = _.sumBy(collaterals,(col)=>col.Insurance_Value ? parseFloat(col.Insurance_Value+'') : 0 );
          collateral.value.DiscMkt = _.sumBy(collaterals,(col)=>col.Disc_Mkt_Value ? parseFloat(col.Disc_Mkt_Value+'') : 0 );
          collateral.value.DiscIns = _.sumBy(collaterals,(col)=>col.Insurance_Disc_Value ? parseFloat(col.Insurance_Disc_Value+'') : 0 );
        }

        let isNonZero = false;
        collateralValueKeys.some(key => {
          if(collateral.value[key]){
            isNonZero = true;
            return true;
          }
        });

        if(isNonZero) {
          nonZeroCollateralTypeValues.push(collateral);
        }
      });

      //total collateral
      collateralReport.collateralTypes = nonZeroCollateralTypeValues;
      collateralReport.totalCollateral = new Value();
      collateralReport.totalCollateral.Mkt = _.sumBy(nonZeroCollateralTypeValues,'value.Mkt');
      collateralReport.totalCollateral.Ins = _.sumBy(nonZeroCollateralTypeValues,'value.Ins');
      collateralReport.totalCollateral.DiscMkt = _.sumBy(nonZeroCollateralTypeValues,'value.DiscMkt');
      collateralReport.totalCollateral.DiscIns = _.sumBy(nonZeroCollateralTypeValues,'value.DiscIns');

      //arm commitment
      let armCommitment = localLoanObject.LoanMaster.ARM_Commitment;
      let armFeesAndInsterest = localLoanObject.LoanMaster.ARM_Rate_Fee;

      //LTV
      collateralReport.LTV = new Value();
      collateralReport.LTV.Mkt =  collateralReport.totalCollateral.Mkt ? (100*(armCommitment + armFeesAndInsterest))/collateralReport.totalCollateral.Mkt : 0;
      collateralReport.LTV.Ins = collateralReport.totalCollateral.Ins ? (100*(armCommitment + armFeesAndInsterest))/collateralReport.totalCollateral.Ins : 0;
      collateralReport.LTV.DiscMkt = collateralReport.totalCollateral.DiscMkt ? (100*(armCommitment + armFeesAndInsterest))/collateralReport.totalCollateral.DiscMkt : 0;
      collateralReport.LTV.DiscIns = collateralReport.totalCollateral.DiscIns ? (100*(armCommitment + armFeesAndInsterest))/collateralReport.totalCollateral.DiscIns : 0;

      return collateralReport;
    } else {
      return undefined;
    }
  }
}

export class CollateralType{
  typeName: string;
  typeCode : string;
  value : Value
}

export class Value {
  Mkt: number = 0;
  Ins : number = 0;
  DiscMkt : number = 0;
  DiscIns : number = 0;
}

export class CollateralReport{
  collateralTypes : Array<CollateralType>;
  totalCollateral : Value;
  armMargin : Value;
  LTV : Value;
  CEI : Value;
  agPro: number;
  armCommit: number;
  armEstInterest: number;
  distCommit: number;
  distEstInterest: number;
  totalMargin : Value;
  RCValue : number;
  LTVValue : number;
}

export enum CollateralValueKey{
  Market = 'Mkt',
  Insurance = 'Ins',
  DiscMarket = 'DiscMkt',
  DiscInsurance = 'DiscIns'
}
