import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { Helper } from '@lenda/services/math.helper';
import { LoanMaster } from '@lenda/models/ref-data-model';
import { Get_Formatted_Date } from '@lenda/services/common-utils';

import { LoggingService } from '@lenda/services/Logs/logging.service';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-financials',
  templateUrl: './financials.component.html',
  styleUrls: ['./financials.component.scss']
})
export class FinancialsReportComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  private loanMaster: LoanMaster;
  public allDataFetched = false;
  public rowData = [];

  @Input() ID: string = '';
  @Input() showARMDetails: boolean = false;
  @Input() expanded = true;

  constructor(
    public localstorageservice: LocalStorageService,
    public logging: LoggingService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res != undefined && res != null) {
          this.loanMaster = res.LoanMaster;
          this.allDataFetched = true;
          this.prepareviewmodel();
        }
      });

    this.getdataforgrid();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private getdataforgrid() {
    let obj: any = this.localstorageservice.retrieve(environment.loankey);
    // this.logging.checkandcreatelog(1, 'financials', "LocalStorage retrieved");
    if (obj != null && obj != undefined) {
      this.loanMaster = obj.LoanMaster;
      this.allDataFetched = true;

      if (this.loanMaster != null) {
        this.prepareviewmodel();
      } else {
        this.rowData = [];
      }
    }
  }

  private prepareviewmodel() {
    // prepare three rows here

    if (this.loanMaster) {
      this.rowData = [];

      //1st Current Financial Row
      let currentobj = {
        Financials: 'Current',
        Assets: this.loanMaster.Current_Assets,
        Debt: this.loanMaster.Current_Liabilities,
        Equity: this.loanMaster.Current_Assets - this.loanMaster.Current_Liabilities,
        Ratios: this.getRatio(this.loanMaster.Current_Assets, this.loanMaster.Current_Liabilities),
        FICO: this.loanMaster.Credit_Score,
        Rating: this.loanMaster.Borrower_Rating || 0
      };

      this.rowData.push(currentobj);

      //1st Intermediate Financial Row
      let Intermediateobj = {
        Financials: 'Intermediate',
        Assets: this.loanMaster.Inter_Assets,
        Debt: this.loanMaster.Inter_Liabilities,
        Equity: this.loanMaster.Inter_Assets - this.loanMaster.Inter_Liabilities,
        Ratios: '',
        FICO: '',
        Rating: ''
      };
      this.rowData.push(Intermediateobj);

      //1st LongTerm Financial Row
      let LongTermobj = {
        Financials: 'Long Term',
        Assets: this.loanMaster.Fixed_Assets,
        Debt: this.loanMaster.Fixed_Liabilities,
        Equity: this.loanMaster.Fixed_Assets - this.loanMaster.Fixed_Liabilities,
        Ratios: '',
        FICO: '',
        Rating: ''
      };
      this.rowData.push(LongTermobj);

      //Last Aggregate Row
      let Aggregateobj = {
        Financials: 'Total Financials',
        Assets: this.loanMaster.Total_Assets,
        Debt: this.loanMaster.Total_Liabilities,
        Equity: this.loanMaster.Total_Assets - this.loanMaster.Total_Liabilities,
        Ratios: this.getRatio(this.loanMaster.Total_Liabilities, this.loanMaster.Total_Assets) + ' Debt/Equity',
        FICO: '',
        Rating: 'Financials as of ' + Get_Formatted_Date(this.loanMaster.Financials_Date)
      };

      //this.pinnedBottomRowData.push(Aggregateobj);
      this.rowData.push(Aggregateobj);
    }
  }

  private getRatio(Total_Assets: number, Total_Liabilities: number) {
    if(Total_Assets) {
      return Helper.percent(Total_Assets, Total_Liabilities).toFixed(1) + '%';
    } else {
      return '0%';
    }
  }
}
