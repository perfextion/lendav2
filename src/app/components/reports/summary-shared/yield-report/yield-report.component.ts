import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';
import { loan_model } from '@lenda/models/loanmodel';
import {
  lookupCropTypeWithRefData,
  lookupCropPracticeTypeWithRefData
} from '@lenda/Workers/utility/aggrid/cropboxes';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';
import { Loan_Key_Visibilty, Loansettings } from '@lenda/models/loansettings';

import { DataService } from '@lenda/services/data.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';

@Component({
  selector: 'app-yield-report',
  templateUrl: './yield-report.component.html',
  styleUrls: ['./yield-report.component.scss']
})
export class YieldReportComponent implements OnInit, OnDestroy {
  @Input() ID: string = '';
  @Input() expanded = true;
  @Input() showCropDetail: boolean = true;

  public columnsDisplaySettings: ColumnsDisplaySettings;
  public years: Array<number> = [];
  public cropYield: Array<any>;

  private cropYear: number;
  private refdata: RefDataModel;
  private localloanobject: loan_model;

  //Hide Columns Based ON Loan Settings
  private LoankeySettings: Loan_Key_Visibilty;

  //Subscriptions
  private subscription: ISubscription;
  private preferencesubscription: ISubscription;

  constructor(
    public localst: LocalStorageService,
    private dataService: DataService,
    private settingsService: SettingsService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localloanobject = res;
      if (res != undefined && res != null) {
        if (res && res.CropYield && res.LoanMaster) {
          this.prepareData(res);
        }
      }
    });

    this.localloanobject = this.localst.retrieve(environment.loankey);
    this.refdata = this.localst.retrieve(environment.referencedatakey);

    this.getUserPreferences();

    if (
      this.localloanobject &&
      this.localloanobject.CropYield &&
      this.localloanobject.LoanMaster
    ) {
      this.prepareData(this.localloanobject);
    }
  }

  private getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;

    this.preferencesubscription = this.settingsService.preferencesChange.subscribe(
      preferences => {
        this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
        this.hideUnhideLoanKeys();
      }
    );
  }

  hideUnhideLoanKeys() {
    try {
      let LoanSettings = JSON.parse(
        this.localloanobject.LoanMaster.Loan_Settings
      );

      if (LoanSettings && LoanSettings.Loan_key_Settings) {
        this.LoankeySettings = LoanSettings.Loan_key_Settings;
      } else {
        this.LoankeySettings = new Loan_Key_Visibilty();
      }
    } catch {
      this.LoankeySettings = new Loan_Key_Visibilty();
    }

    this.columnsDisplaySettings.cropPrac = this.LoankeySettings.Crop_Practice == null ? this.columnsDisplaySettings.cropPrac : this.LoankeySettings.Crop_Practice;
    this.columnsDisplaySettings.cropType = this.LoankeySettings.Crop_Type == null ? this.columnsDisplaySettings.cropType : this.LoankeySettings.Crop_Type;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.preferencesubscription.unsubscribe();
  }

  private prepareData(res: any) {
    this.cropYield = res.CropYield;
    this.cropYield = _.sortBy(this.cropYield, [
      'Crop',
      'cropType',
      'Practice',
      'Crop_Practice'
    ]);

    this.cropYear = res.LoanMaster.Crop_Year;

    this.years = [];
    for (let i = 1; i < 8; i++) {
      this.years.push(this.cropYear - i);
    }
  }

  cropType(cropId: number) {
    return lookupCropTypeWithRefData(cropId, this.refdata);
  }

  cropPracticeType(cropId: number) {
    return lookupCropPracticeTypeWithRefData(cropId, this.refdata);
  }
}
