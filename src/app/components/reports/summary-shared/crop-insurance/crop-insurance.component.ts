import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model, Loan_Crop_Practice, Collateral_Category_Code } from '@lenda/models/loanmodel';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';
import { Loan_Policy } from '@lenda/models/insurancemodel';
import { Loan_Crop_Unit, V_Crop_Price_Details } from '@lenda/models/cropmodel';
import { Helper } from '@lenda/services/math.helper';

import { DataService } from '@lenda/services/data.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { Loan_Key_Visibilty } from '@lenda/models/loansettings';

@Component({
  selector: 'app-crop-insurance',
  templateUrl: './crop-insurance.component.html',
  styleUrls: ['./crop-insurance.component.scss']
})
export class CropInsuranceReportComponent implements OnInit, OnDestroy {
  private subscription : ISubscription;
  private preferencesubscription: ISubscription;

  cropInsurances : Array<CropInsuranceReport> = [];
  nonCropInsurances : Array<CropInsuranceReport> = [];
  localLoanObject : loan_model;
  totalInsValue : number = 0;
  refData : any = {};
  displayDetail = true;

  @Input() ID : string ='';
  @Input() colToDisplay : any = null;
  @Input() showCropDetail : boolean = true;
  @Input() expanded = true;

  columnsDisplaySettings: ColumnsDisplaySettings;
  private LoankeySettings: Loan_Key_Visibilty;

  constructor(
    private localStorageService : LocalStorageService,
    private dataService : DataService,
    private settingsService: SettingsService
  ) { }

  ngOnInit() {
    this.refData = this.localStorageService.retrieve(environment.referencedatakey);

    this.subscription =  this.dataService.getLoanObject().subscribe((res: loan_model)=>{
      if(res){
        this.localLoanObject = res;
        this.prepareData();
      }
    });

    this.localLoanObject= this.localStorageService.retrieve(environment.loankey);
    if(this.localLoanObject){
      this.prepareData();
      this.getUserPreferences();
    }
  }

  private getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;

    this.preferencesubscription= this.settingsService.preferencesChange.subscribe((preferences) => {
      this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
      this.hideUnhideLoanKeys();
    });
  }

  hideUnhideLoanKeys() {
    try {
      let LoanSettings = JSON.parse(
        this.localLoanObject.LoanMaster.Loan_Settings
      );

      if (LoanSettings && LoanSettings.Loan_key_Settings) {
        this.LoankeySettings = LoanSettings.Loan_key_Settings;
      } else {
        this.LoankeySettings = new Loan_Key_Visibilty();
      }
    } catch {
      this.LoankeySettings = new Loan_Key_Visibilty();
    }

    this.columnsDisplaySettings.cropPrac = this.LoankeySettings.Crop_Practice == null ? this.columnsDisplaySettings.cropPrac : this.LoankeySettings.Crop_Practice;
    this.columnsDisplaySettings.cropType = this.LoankeySettings.Crop_Type == null ? this.columnsDisplaySettings.cropType : this.LoankeySettings.Crop_Type;
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
    this.preferencesubscription.unsubscribe();
  }

  private prepareData(){
    this.cropInsurances = [];
    let totalInsValue : number = 0;

    this.localLoanObject.LoanCrops.filter(a => a.ActionStatus != 3).forEach(crop => {
      let refCrop: V_Crop_Price_Details = this.refData.CropList.find(cl => cl.Crop_Code == crop.Crop_Code);

      if (refCrop){
        let cropPractices : Array<Loan_Crop_Practice> = this.localLoanObject.LoanCropPractices.filter(cp => cp.FC_CropName == refCrop.Crop_Name);
        let cropPracticeInsurance : Array<CropInsuranceReport> = [];

        cropPractices.forEach(cp => {
          let policies = this.localLoanObject.LoanPolicies.filter(
            a => a.ActionStatus != 3 &&
            a.Select_Ind &&
            this.compareType(a.Crop_Type_Code, cp.FC_Crop_Type as any) &&
            a.Crop_Code == cp.Fc_Crop_Code &&
            a.Crop_Practice_Type_Code == cp.FC_Crop_Practice_Type &&
            a.Irr_Practice_Type_Code == cp.FC_PracticeType
          );

          let policyGroups = _.groupBy(policies, p => p.Ins_Plan);

          for(let plan in policyGroups) {
            let policies = policyGroups[plan];
            if(policies && policies.length > 0) {
              let crpPracInsurance = this.getCropInsuranceRow(policies, true, cp);
              totalInsValue += crpPracInsurance.InsValue;
              cropPracticeInsurance.push(crpPracInsurance);
            }
          }
        });

        let policies = this.localLoanObject.LoanPolicies.filter(
          a => a.ActionStatus != 3 &&
          a.Select_Ind &&
          a.Crop_Code == refCrop.Crop_Code
        );

        if(policies.length > 0) {
          let crpPracInsurance = this.getCropInsuranceRow(policies, false, {
            FC_CropName: refCrop.Crop_Name,
            FC_PracticeType: '',
            FC_Crop_Type: '',
            FC_Crop_Practice_Type: ''
          });

          cropPracticeInsurance.push(crpPracInsurance);
        }

        cropPracticeInsurance.forEach(cpi =>{
          if(!!cpi.InsValue){
            this.cropInsurances.push(cpi);
          }
        });
      }
    });

    this.nonCropInsurances = [];

    if(this.localLoanObject.Loanwfrp){
      let wfrpInsurance = new CropInsuranceReport();
      wfrpInsurance.InsCategory = 'Crop';
      wfrpInsurance.CropName = 'All'
      wfrpInsurance.InsPlan = 'WFRP';
      wfrpInsurance.InsSharePer = 100;
      wfrpInsurance.InsValue = this.localLoanObject.Loanwfrp.Approved_Revenue;
      if(this.isNonZeroRow(wfrpInsurance)){
        totalInsValue += wfrpInsurance.InsValue || 0;
        this.nonCropInsurances.push(wfrpInsurance);
      }
    }

    if(this.localLoanObject.LoanCollateral) {
      let fsa = this.localLoanObject.LoanCollateral.filter(col=>col.Collateral_Category_Code == Collateral_Category_Code.FSA);
      let fsaInsurance = new CropInsuranceReport();
      fsaInsurance.InsCategory = 'FSA';
      fsaInsurance.InsSharePer = 100;
      fsaInsurance.InsValue = _.sumBy(fsa,'Insurance_Value');
      if(this.isNonZeroRow(fsaInsurance)){
        totalInsValue += fsaInsurance.InsValue;
        this.nonCropInsurances.push(fsaInsurance);
      }

      let equipments = this.localLoanObject.LoanCollateral.filter(col=>col.Collateral_Category_Code == Collateral_Category_Code.Equipemt);
      let equInsurance = new CropInsuranceReport();
      equInsurance.InsCategory = 'Equipments';
      equInsurance.InsSharePer = 100;
      equInsurance.InsValue = _.sumBy(equipments,'Insurance_Value');
      if(this.isNonZeroRow(equInsurance)){
        totalInsValue += equInsurance.InsValue;
        this.nonCropInsurances.push(equInsurance);
      }

      let realEstates = this.localLoanObject.LoanCollateral.filter(col=>col.Collateral_Category_Code == Collateral_Category_Code.RealEState);
      let reInsurance = new CropInsuranceReport();
      reInsurance.InsCategory = 'Real Estate';
      reInsurance.InsSharePer = 100;
      reInsurance.InsValue = _.sumBy(realEstates,'Insurance_Value');
      if(this.isNonZeroRow(reInsurance)){
        totalInsValue += reInsurance.InsValue;
        this.nonCropInsurances.push(reInsurance);
      }

      let livestaocks = this.localLoanObject.LoanCollateral.filter(col=>col.Collateral_Category_Code == Collateral_Category_Code.Livestock);
      let lsInsurance = new CropInsuranceReport();
      lsInsurance.InsCategory = 'Livestock';
      lsInsurance.InsSharePer = 100;
      lsInsurance.InsValue = _.sumBy(livestaocks,'Insurance_Value');
      if(this.isNonZeroRow(lsInsurance)){
        totalInsValue += lsInsurance.InsValue;
        this.nonCropInsurances.push(lsInsurance);
      }

      let storedCrops = this.localLoanObject.LoanCollateral.filter(col=>col.Collateral_Category_Code == Collateral_Category_Code.StoredCrop);
      let scpInsrance = new CropInsuranceReport();
      scpInsrance.InsCategory = 'Stored Crop';
      scpInsrance.InsSharePer = 100;
      scpInsrance.InsValue = _.sumBy(storedCrops,'Insurance_Value');
      if(this.isNonZeroRow(scpInsrance)){
        totalInsValue += scpInsrance.InsValue;
        this.nonCropInsurances.push(scpInsrance);
      }

      let others = this.localLoanObject.LoanCollateral.filter(col=>col.Collateral_Category_Code == Collateral_Category_Code.Other);
      let otherInsurance = new CropInsuranceReport();
      otherInsurance.InsCategory = 'Other';
      otherInsurance.InsValue = _.sumBy(others,'Insurance_Value');
      otherInsurance.InsSharePer = 100;
      if(this.isNonZeroRow(otherInsurance)){
        totalInsValue += otherInsurance.InsValue;
        this.nonCropInsurances.push(otherInsurance);
      }
    }

    this.totalInsValue = totalInsValue;
  }

  private compareType(cropUnitType: string, policyType: string) {
    if(cropUnitType == '') {
      return policyType == '' || policyType == '-';
    }
    return cropUnitType == policyType;
  }

  private getCropInsuranceRow(LoanPolicies: Array<Loan_Policy>, isPractieRecord: boolean, cp: any) {
    let a = LoanPolicies[0];

    let crpPracInsurance = new CropInsuranceReport();
    crpPracInsurance.InsCategory = 'Crop';
    crpPracInsurance.CropName = cp.FC_CropName;
    crpPracInsurance.Practice = cp.FC_PracticeType;
    crpPracInsurance.CropType = cp.FC_Crop_Type as any;
    crpPracInsurance.CropPrac = cp.FC_Crop_Practice_Type as any;

    let insValue = 0;
    let acres = 0;
    let aphsum = 0;
    let pricesum = 0;
    let totalinsshare = 0;

    LoanPolicies.forEach(a => {
      let cropUnits = this.localLoanObject.LoanCropUnits.filter(
        c => c.ActionStatus != 3 &&
        a.Policy_Key == c.Crop_Unit_Key
      );

      acres +=  _.sumBy(cropUnits,'CU_Acres') || 0;
      insValue += _.sumBy(cropUnits, c => this.getInsValue(a, c)) || 0;
      pricesum += _.sumBy(cropUnits, c => c.CU_Acres * a.Price);
      aphsum += _.sumBy(cropUnits, c => c.Ins_APH * c.CU_Acres);
      totalinsshare += _.sumBy(cropUnits, c => c.FC_Insurance_Share * c.CU_Acres );
    });

    crpPracInsurance.Acres = acres;
    crpPracInsurance.InsSharePer = Helper.divide(totalinsshare, acres);
    crpPracInsurance.InsValue = insValue;
    crpPracInsurance.isPractieRecord = isPractieRecord;
    crpPracInsurance.InsPlan = a.Ins_Plan;
    crpPracInsurance.InsType = a.Ins_Plan_Type;
    crpPracInsurance.APH = Helper.divide(aphsum, acres);
    crpPracInsurance.Price = Helper.divide(pricesum, acres);
    return crpPracInsurance;
  }

  private getInsValue(a: Loan_Policy, c: Loan_Crop_Unit) {
    switch (a.Ins_Plan.toUpperCase()) {
      case 'MPCI':
        return c.FC_MPCIvalue;
      case 'STAX':
        return c.FC_Staxvalue;
      case 'SCO':
        return c.FC_Scovalue;
      case 'HMAX':
        return c.FC_Hmaxvalue;
      case 'RAMP':
        return c.FC_Rampvalue;
      case 'ICE':
        return c.FC_Icevalue;
      case 'SELECT':
        return c.FC_Abcvalue;
      case 'PCI':
        return c.FC_Pcivalue;
      case 'CROPHAIL':
        return c.FC_Crophailvalue;
      default:
        return c.FC_MPCIvalue;
    }
  }

  isNonZeroRow(cropInsurance : CropInsuranceReport) {
    return (!!cropInsurance.InsValue)
  }

  onDisplayDetailsChange(event){
    this.displayDetail = event.checked;
  }
}

class CropInsuranceReport{
  CropName : string;
  CropType : string;
  CropPrac : string;
  Practice : string;
  InsCategory : string;
  InsPlan : string;
  InsType : string;
  Acres : number;
  APH: number = 0;
  Price: number = 0;
  InsSharePer : number;
  InsValue : number;
  isPractieRecord : boolean;

}
