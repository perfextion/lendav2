import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';
import { ReportsSharedModule } from '../shared/reports-shared.module';

import { BudgetReportComponent } from './budget-report/budget-report.component';
import { CropInsuranceReportComponent } from './crop-insurance/crop-insurance.component';
import { FinancialsReportComponent } from '../summary-shared/financials/financials.component';
import { ProjectedincomeReportComponent } from './projectedincome/projectedincome.component';
import { DocumentsReportComponent } from './documents-report/documents-report.component';
import { YieldReportComponent } from './yield-report/yield-report.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    UiComponentsModule,
    ReportsSharedModule
  ],
  declarations: [
    BudgetReportComponent,
    CropInsuranceReportComponent,
    DocumentsReportComponent,
    FinancialsReportComponent,
    ProjectedincomeReportComponent,
    YieldReportComponent
  ],
  exports: [
    BudgetReportComponent,
    CropInsuranceReportComponent,
    DocumentsReportComponent,
    FinancialsReportComponent,
    ProjectedincomeReportComponent,
    YieldReportComponent
  ]
})
export class SummaryReportsSharedModule {}
