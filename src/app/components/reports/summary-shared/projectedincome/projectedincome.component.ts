import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model, Loan_Crop } from '@lenda/models/loanmodel';
import { Helper } from '@lenda/services/math.helper';
import { LoanMaster, RefDataModel } from '@lenda/models/ref-data-model';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';

import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { DataService } from '@lenda/services/data.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { Loan_Key_Visibilty } from '@lenda/models/loansettings';

@Component({
  selector: 'app-projectedincome',
  templateUrl: './projectedincome.component.html',
  styleUrls: ['./projectedincome.component.scss']
})
export class ProjectedincomeReportComponent implements OnInit, OnDestroy {
  private subscription : ISubscription;
  private preferencesubscription: ISubscription;

  @Input() ID : string = '';
  @Input() colToDisplay : any = null;
  @Input() showCropDetail : boolean = true;
  @Input() expanded = true;

  private localloanobject: loan_model = new loan_model();
  public allDataFetched = false;
  public cropRevenue: Array<CropRevenueModel> = [];
  public totalAcres: string | number;
  public NetCropRevenue: number;
  public Per_NetCropRevenue : number;
  private refData : RefDataModel;
  public displayPracticeRows : boolean = false;


  public Total_Additional_Revenue: number = 0;
  public Total_Revenue : number;
  public Total_Expense_Budget: number = 0;
  public Estimated_Interest: number = 0;
  public Total_CashFlow: number = 0;

  public Per_Total_Additional_Revenue: number;
  public Per_Total_Revenue : number;
  public Per_Total_Expense_Budget: number;
  public Per_Estimated_Interest: number;
  public Per_Total_CashFlow: number;

  public Other_Income_Revenue: number;
  public Per_Other_Income_Revenue: number;

  public breakeven : number;

  public columnsDisplaySettings: ColumnsDisplaySettings;
  public other_incomes: Array<CropRevenueModel> = [];

  //to track whether the storing the json attempted or not, otehrwise it will go in infinite look with observer
  //as we have to call the performcalculationonloanobject function in the routine of the preparedData which is being called from
  //observer
  public isStorageAttempted :boolean=false;

  private LoankeySettings: Loan_Key_Visibilty;

  constructor(
    public localstorageservice: LocalStorageService,
    public logging: LoggingService,
    public loanCalculationsService: LoancalculationWorker,
    private dataService : DataService,
    private settingsService: SettingsService
  ) {}

  ngOnInit() {
    this.refData = this.localstorageservice.retrieve(environment.referencedatakey);

    this.subscription =  this.dataService.getLoanObject().subscribe((res: loan_model)=>{
      if (res != undefined && res != null) {
        this.localloanobject = res;
        this.allDataFetched = true;
        this.prepareData();
      }
    });

    this.getdataforgrid();
  }

  private getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;

    this.preferencesubscription= this.settingsService.preferencesChange.subscribe((preferences) => {
      this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
      this.hideUnhideLoanKeys();
    });
  }

  hideUnhideLoanKeys() {
    try {
      let LoanSettings = JSON.parse(
        this.localloanobject.LoanMaster.Loan_Settings
      );

      if (LoanSettings && LoanSettings.Loan_key_Settings) {
        this.LoankeySettings = LoanSettings.Loan_key_Settings;
      } else {
        this.LoankeySettings = new Loan_Key_Visibilty();
      }
    } catch {
      this.LoankeySettings = new Loan_Key_Visibilty();
    }

    this.columnsDisplaySettings.cropPrac = this.LoankeySettings.Crop_Practice == null ? this.columnsDisplaySettings.cropPrac : this.LoankeySettings.Crop_Practice;
    this.columnsDisplaySettings.cropType = this.LoankeySettings.Crop_Type == null ? this.columnsDisplaySettings.cropType : this.LoankeySettings.Crop_Type;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.preferencesubscription.unsubscribe();
  }

  private getdataforgrid() {
    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);

    if (this.localloanobject != null && this.localloanobject != undefined) {
      this.localloanobject = this.localloanobject;
      this.allDataFetched = true;
    }
    this.getUserPreferences();
    this.prepareData();
  }

  private prepareData() {
    if (this.localloanobject && this.localloanobject.LoanCrops) {
      this.cropRevenue = [];
      this.localloanobject.LoanCrops.filter(a => a.ActionStatus != 3).forEach(crop => {
        let cropRevenue: CropRevenueModel = new CropRevenueModel();
        cropRevenue.Name = this.getCropName(crop);
        cropRevenue.CropType = crop.Crop_Type_Code;
        cropRevenue.Acres = crop.Acres ? crop.Acres.toFixed(1) : '0.0';
        cropRevenue.CropYield = crop.W_Crop_Yield ? crop.W_Crop_Yield.toFixed(0) : '0';
        cropRevenue.Share = crop.LC_Share ? crop.LC_Share.toFixed(1) : '0.0';
        cropRevenue.Price = crop.Crop_Price ? crop.Crop_Price.toFixed(4) : '0.0000';
        cropRevenue.Basic_Adj = crop.Basic_Adj ? crop.Basic_Adj.toFixed(4) : '0.0000';
        cropRevenue.Marketing_Adj = crop.Marketing_Adj ? crop.Marketing_Adj.toFixed(4) : '0.0000';
        cropRevenue.Rebate_Adj = crop.Rebate_Adj ? crop.Rebate_Adj.toFixed(4) : '0.0000';
        cropRevenue.Revenue = crop.Revenue;
        cropRevenue.isPracticeRow = false;
        this.cropRevenue.push(cropRevenue);

        //add respective crops practie details
        let cropPractices = this.localloanobject.LoanCropPractices.filter(cp=> cp.FC_Crop_Type == crop.Crop_Type_Code && cp.Fc_Crop_Code == crop.Crop_Code);

        cropPractices.forEach(cp=>{
          let cropRevenue: CropRevenueModel = new CropRevenueModel();
          cropRevenue.Name = cp.FC_CropName;
          cropRevenue.CropType = cp.FC_Crop_Type;
          cropRevenue.PracticeType = cp.FC_PracticeType;
          cropRevenue.CropPracticeType = cp.FC_Crop_Practice_Type;
          cropRevenue.Acres = cp.LCP_Acres ? cp.LCP_Acres.toFixed(1) : '0.0';
          cropRevenue.CropYield = cp.FC_CropYield ? cp.FC_CropYield.toFixed(0) : '0';
          cropRevenue.Share = cp.FC_Share ? cp.FC_Share.toFixed(1) : '0.0';
          cropRevenue.Price = crop.Crop_Price ? crop.Crop_Price.toFixed(4) : '0.0000';
          cropRevenue.Basic_Adj = crop.Basic_Adj ? crop.Basic_Adj.toFixed(4) : '0.0000';
          cropRevenue.Marketing_Adj = crop.Marketing_Adj ? crop.Marketing_Adj.toFixed(4) : '0.0000';
          cropRevenue.Rebate_Adj = crop.Rebate_Adj ? crop.Rebate_Adj.toFixed(4) : '0.0000';
          cropRevenue.Revenue = cp.Market_Value;
          cropRevenue.isPracticeRow = true;
          this.cropRevenue.push(cropRevenue);
        });
      });

      if(this.localloanobject && this.localloanobject.LoanOtherIncomes) {
        this.Other_Income_Revenue = this.localloanobject.LoanMaster.Net_Other_Income || 0;
      }

      if (this.localloanobject && this.localloanobject.LoanMaster && this.localloanobject.LoanMaster) {
        let loanMaster: LoanMaster = this.localloanobject.LoanMaster;

        this.totalAcres = loanMaster.Total_Crop_Acres ? loanMaster.Total_Crop_Acres.toFixed(1) : 0;
        this.NetCropRevenue = loanMaster.Net_Market_Value_Crops ? parseInt(loanMaster.Net_Market_Value_Crops.toFixed(0)) : 0;

        this.other_incomes = [];

        const _other_incomes = this.localloanobject.LoanOtherIncomes || [];
        const other_income_data = _.sortBy(_other_incomes.filter(a => a.ActionStatus != 3), a => a.Sort_Order);
        let other_income_groups = _.groupBy(other_income_data, a => a.Other_Income_Name);

        for(let group in other_income_groups) {
          let incomes = other_income_groups[group];
          let revenuemodel = new CropRevenueModel();
          revenuemodel.Name = group;
          revenuemodel.Revenue = _.sumBy(incomes, a => a.Amount || 0);
          this.other_incomes.push(revenuemodel);
        }

        this.Total_Additional_Revenue = this.Other_Income_Revenue;
        this.Total_Revenue = this.NetCropRevenue + this.Total_Additional_Revenue;
        this.Total_Revenue = this.Total_Revenue ? parseInt(this.Total_Revenue.toFixed(0)) : 0;

        this.Total_Expense_Budget = loanMaster.Loan_Total_Budget ? parseInt(loanMaster.Loan_Total_Budget.toFixed(0)) :0;
        this.Estimated_Interest = loanMaster.Interest_Est_Amount || 0;

        this.Total_CashFlow = parseInt(this.Total_Revenue.toFixed(0)) - parseInt(this.Total_Expense_Budget.toFixed(0)) - parseInt(this.Estimated_Interest.toFixed(0));

        this.breakeven = Helper.percent(loanMaster.ARM_Commitment + loanMaster.Dist_Commitment,  this.Total_Revenue);
        this.breakeven = parseFloat(this.breakeven.toFixed(1));

        this.cropRevenue.forEach(cr=>{
          cr.Percentage = Helper.percent(cr.Revenue, this.Total_Revenue);
        });

        this.Per_NetCropRevenue = Helper.percent(this.NetCropRevenue, this.Total_Revenue);
        this.Per_Total_Additional_Revenue = Helper.percent(this.Total_Additional_Revenue, this.Total_Revenue);
        this.Per_Total_Revenue = Helper.percent(this.Total_Revenue, this.Total_Revenue);
        this.Per_Total_Expense_Budget = Helper.percent(this.Total_Expense_Budget, this.Total_Revenue);
        this.Per_Estimated_Interest = Helper.percent(this.Estimated_Interest, this.Total_Revenue);
        this.Per_Total_CashFlow = Helper.percent(this.Total_CashFlow, this.Total_Revenue);
        this.Per_Other_Income_Revenue = Helper.percent(this.Other_Income_Revenue, this.Total_Revenue);

        this.other_incomes.forEach(income => {
          income.Percentage = Helper.percent(income.Revenue, this.Total_Revenue);
        });
      }

      this.cropRevenue = _.sortBy(this.cropRevenue, ['Name', 'CropType', 'PracticeType', 'CropPracticeType'])
    }
  }

  private getCropName(lc: Loan_Crop){
    if(this.refData && this.refData.CropList && lc){
      let crop = this.refData.CropList.find(c=>c.Crop_Code == lc.Crop_Code);
      if(crop){
        return crop.Crop_Name;
      }else{
        return '';
      }
    }else{
      return '';
    }
  }

  public onDisplayDetailsChange(event){
    this.displayPracticeRows = event.checked;
  }
}

class CropRevenueModel {
  Name: string;
  CropType: String;
  PracticeType : string;
  CropPracticeType: String;
  Acres: string;
  CropYield: string;
  Share: string;
  Price: string;
  Basic_Adj: string;
  Marketing_Adj: string;
  Rebate_Adj: string;
  Revenue: number;
  Percentage: number;
  isPracticeRow : boolean;

  constructor() {
    this.isPracticeRow = false;
  }
}
