import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';

import { DataService } from '@lenda/services/data.service';
import { loan_model } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import { Loan_Document } from '@lenda/models/loan/loan_document';
import { RefDataModel } from '@lenda/models/ref-data-model';

import * as _ from 'lodash';

@Component({
  selector: 'app-documents-report',
  templateUrl: './documents-report.component.html',
  styleUrls: ['./documents-report.component.scss']
})
export class DocumentsReportComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  public localLoanObject: loan_model;
  private refData: RefDataModel;
  public documents: Array<string> = [];
  public refDocuments: Array<string> = [];

  @Input() public ID: string = '';
  @Input() public showDocuments: boolean = true;
  @Input() public expanded = true;

  constructor(
    private localStorageService: LocalStorageService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res) {
          this.localLoanObject = res;
          this.prepareDocuments();
        }
      });

    this.localLoanObject = this.localStorageService.retrieve(
      environment.loankey
    );

    this.refData = this.localStorageService.retrieve(
      environment.referencedatakey
    );

    if (this.localLoanObject) {
      this.prepareDocuments();
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private prepareDocuments() {
    let docs = _.sortBy(
      this.localLoanObject.Loan_Documents.filter(a => a.ActionStatus != 3),
      ['Document_Type_Level', 'Sort_Order']
    );

    docs.forEach(con => {
      this.documents.push(con.Document_Name);
    });

    let uniqConsditions: Array<Loan_Document> = _.uniqBy(
      docs,
      'Document_Type_ID'
    );

    uniqConsditions.forEach(condition => {
      let matchedCondition = this.refData.Ref_Document_Types.find(
        con => con.Document_Type_ID === condition.Document_Type_ID
      );

      if (matchedCondition) {
        this.refDocuments.push(matchedCondition.Document_Name_Override);
      }
    });
  }
}
