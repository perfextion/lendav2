import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { BudgetHelperService } from '@lenda/components/budget/budget-helper.service';
import { Loan_Budget, Loan_Crop_Practice, loan_model } from '@lenda/models/loanmodel';
import { Get_Distributor_Name, Get_Crop_Name_By_Crop_Practice_Without_Column_Settings } from '@lenda/services/common-utils';

import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-budget-report',
  templateUrl: './budget-report.component.html',
  styleUrls: ['./budget-report.component.scss']
})
export class BudgetReportComponent implements OnInit, OnDestroy {
  private subscription : ISubscription;

  public budgets: Array<any>;
  public isTotalBudgetReport : boolean = false;
  public loanCropPractices : Array<Loan_Crop_Practice> = [];
  public displayAdditionalNotes : boolean = false;

  /**
   * Should the budget-report display the percentage columns or not
   */
  @Input() withRevenue :boolean = false;

  /**
   * Should budget-report display heading before table (check scheduled budgets in borrower-summary report)
   */
  @Input() withReportSubHeading :boolean = false;

  /**
   * Determines  if the budget is for individual crop practice or not, and if it is, it will hold cropPracticeID
    */
  @Input() cropPracticeId : number = undefined;

  /**
   * Determines if the component should only render one total budget table or it should also display fixed budget and all crop practice tables*/
  @Input() withIndividualCropDetails = false;

  /**
   * Determines what will be the table heading, if there will be multiple tables in the single cheveron(ex. Schedule Budget),
   */
  @Input() tableHeading : string ='';

  /**
   * Determines whether to wrap the table in dedicated cheveron or not. For subtables on schedule budget this param should be off to just display table heading
   * Mostly used with `tableHeading`. So if no cheveron then what heading you want to display
   */
  @Input() withoutChevron = false;

  /**
   * Determines the is it a fixed budget table or not. If fixed budget we should display budgets with Budget_Type='F'
   */
  @Input() isFixedBudget : boolean = false;

  /**
   * Determines whether it is a ScheduleBudget or not. The schedule buudgets have different styling so moslty used to achieve required UI
   */
  @Input() isScheduleBudget = false;

  /**
   * To assign html ID to the table wrapper
   */
  @Input() ID : string = '';

  @Input() expanded = true;


  private localLoanObject: loan_model;
  public DistributorName: string;
  public DistributorNameTitle: string;

  constructor(
    public localstorageservice: LocalStorageService,
    public budgetService : BudgetHelperService,
    private dataService : DataService
  ) { }

  ngOnInit() {

    //if this is not a fixed budget or invidual crop budget then considering it's a total budget
    this.isTotalBudgetReport = !(this.isFixedBudget || this.cropPracticeId);

    this.subscription =  this.dataService.getLoanObject().subscribe((res: loan_model)=>{
      if(res!=undefined && res!=null)
      {
        this.localLoanObject = res;
        if(this.localLoanObject && this.localLoanObject.LoanCropPractices && this.localLoanObject.LoanBudget && this.localLoanObject.LoanMaster){
          this.prepareData(this.localLoanObject);
        }
      }
    });

    this.localLoanObject = this.localstorageservice.retrieve(environment.loankey);
    if(this.localLoanObject && this.localLoanObject.LoanCropPractices && this.localLoanObject.LoanBudget && this.localLoanObject.LoanMaster ){
      this.prepareData(this.localLoanObject);
    }
  }

  public getCPName(cp: Loan_Crop_Practice) {
    return Get_Crop_Name_By_Crop_Practice_Without_Column_Settings(cp);
  }

  public defaultAcresVal = 0;

  getAcres(cropPractice: Loan_Crop_Practice) {
    if(cropPractice.LCP_Acres) {
      return cropPractice.LCP_Acres.toLocaleString('en-US', {
        maximumFractionDigits: 1,
        minimumFractionDigits: 1
      });
    }
    return this.defaultAcresVal.toFixed(1);
  }

  ngOnDestroy(){
    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private prepareData(loanObject: loan_model) {
    this.DistributorName = Get_Distributor_Name(this.localLoanObject);
    this.DistributorNameTitle = Get_Distributor_Name(this.localLoanObject, false);

    this.loanCropPractices = loanObject.LoanCropPractices;

    if (this.cropPracticeId) {
      let lstBudget = loanObject.LoanBudget.filter(lb=>lb.Budget_Type != 'F' && lb.Crop_Practice_ID == this.cropPracticeId);
      let cropPractice = loanObject.LoanCropPractices.filter(cp => cp.Crop_Practice_ID == this.cropPracticeId );
      this.prepareBudgetData(cropPractice, lstBudget);
    } else {
      if(this.isFixedBudget){
        let lstBudget = loanObject.LoanBudget.filter(lb=>lb.Budget_Type == 'F');
        this.prepareBudgetData(loanObject.LoanCropPractices, lstBudget);
      }else if(this.isTotalBudgetReport){
        this.budgets = _.sortBy(
          loanObject.LoanBudget.filter(lb=>(!lb.Crop_Practice_ID)  && lb.Total_Budget_Crop_ET && lb.Budget_Type=='V'),
          (b: Loan_Budget) => b.Sort_order || b.Expense_Type_ID || b.Loan_Budget_ID
        );

        let total = this.calculateTotalAndPer();
        if(!this.isScheduleBudget){
          this.addFeeAndInterstRow(loanObject,total);
        }
      }
    }
    this.displayAdditionalNotes =  this.budgets.some(budget=>budget.Notes);
  }

  private prepareBudgetData(cropPractices : Array<Loan_Crop_Practice>, loanBudget : Array<Loan_Budget>): any {
    let preparedCP = this.budgetService.prepareCropPractice(cropPractices);
    if(this.isFixedBudget){
      this.budgets = _.sortBy(
        loanBudget.map((lb)=> {
          lb.Expense_Type_Name = lb.Expense_Type_Name || lb.Other_Description_Text;
          return lb;
        }),
        (b: Loan_Budget) => b.Sort_order || b.Expense_Type_ID || b.Loan_Budget_ID
      );
    }else{
      this.budgets = this.budgetService.getTotalTableData(loanBudget, preparedCP,(this.localstorageservice.retrieve(environment.loankey) as loan_model).Loan_Full_ID);
    }
    this.calculateTotalAndPer();
  }


  private calculateTotalAndPer() {
    this.budgets.push(this.budgetService.getTotals(this.budgets)[0]);
    let grandTotal_Budget_Crop_ET = this.budgets[this.budgets.length - 1].Total_Budget_Crop_ET;
    this.budgets.forEach(budget => {
      budget.Per_Revenue = (100 * budget.Total_Budget_Crop_ET) / grandTotal_Budget_Crop_ET;
      budget.Per_Revenue = parseFloat(budget.Per_Revenue.toFixed(1));
    });
    return  grandTotal_Budget_Crop_ET;
  }

  private addFeeAndInterstRow(loanObject : loan_model,total : number){
    let budget = {
      Expense_Type_Name: 'Estimated Interest',
      Other_Description_Text: '',
      ARM_Budget_Acre: 0,
      Third_Party_Budget_Acre:0,
      Distributor_Budget_Acre:0,
      Total_Budget_Acre:0,
      ARM_Budget_Crop: loanObject.LoanMaster.ARM_Rate_Fee,
      Third_Party_Budget_Crop: 0,
      Distributor_Budget_Crop: loanObject.LoanMaster.Dist_Rate_Fee,
      Total_Budget_Crop_ET: loanObject.LoanMaster.Interest_Est_Amount,
      Per_Revenue : (100 * loanObject.LoanMaster.Interest_Est_Amount) / total
    }
    this.budgets.push(budget);
  }
}
