import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';

import { BalancesReportsModule } from './balances/balances-report.module';
import { LoanInventoryReportsModule } from './loan-inventory/loan-inventory.module';
import { BorrowerSummaryReportsModule } from './borrower-summary/borrower-summary-report.module';
import { SummaryReportsModule } from './summary/summary-report.module';
import { CrossCollateralReportsModule } from './cross-collateral/cross-collateral-report.module';

import { ReportsComponent } from './reports.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    UiComponentsModule,
    BorrowerSummaryReportsModule,
    SummaryReportsModule,
    CrossCollateralReportsModule,
    LoanInventoryReportsModule,
    BalancesReportsModule
  ],
  declarations: [ReportsComponent],
  exports: [ReportsComponent]
})
export class ReportsModule {}
