import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model, Loantype_dsctext } from '@lenda/models/loanmodel';
import { LoanMaster, RefDataModel } from '@lenda/models/ref-data-model';
import { Get_Borrower_Name } from '@lenda/services/common-utils';

import { LoggingService } from '@lenda/services/Logs/logging.service';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-loan-information',
  templateUrl: './loan-information.component.html',
  styleUrls: ['./loan-information.component.scss']
})
export class LoanInformationComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  public loanMaster: LoanMaster = <LoanMaster>{};
  public allDataFetched=false;
  private refData: RefDataModel;
  public officeName: string = '';
  public FARMing: number = 0;
  public ARMing: number = 0;
  public coBorrowers: Array<string> = [];

  @Input() hideComment: boolean = false;
  @Input() ID: string = '';
  @Input() expanded = true;

  constructor(
    public localstorageservice:LocalStorageService,
    public logging:LoggingService,
    private dataService : DataService
  ) { }

  ngOnInit() {
    this.refData = this.localstorageservice.retrieve(environment.referencedatakey);

    this.subscription =  this.dataService.getLoanObject().subscribe((res: loan_model)=>{
      if (res != undefined && res != null) {
        this.prepareData(res);
      }
    })

    this.getdataforgrid();
  }

  private prepareData(res: loan_model) {
    this.loanMaster = res.LoanMaster;
    this.allDataFetched = true;
    this.officeName = this.getOfficeName(this.loanMaster.Office_ID);
    this.setFarmingAndArming();
    this.coBorrowers = res.CoBorrower.map(cb=> cb.Borrower_First_Name + ' '+ cb.Borrower_Last_Name);
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  getloantype(type:number){
    return Loantype_dsctext(type);
  }

  private getdataforgrid(){
    let obj: any = this.localstorageservice.retrieve(environment.loankey);
    if (obj != null && obj != undefined) {
      this.prepareData(obj);
    }
  }

  private setFarmingAndArming(){
    this.FARMing = this.loanMaster.Year_Begin_Farming ?  (new Date()).getFullYear() - (this.loanMaster.Year_Begin_Farming) : 0;
    this.ARMing = this.loanMaster.Year_Begin_Client ? (new Date()).getFullYear() - (this.loanMaster.Year_Begin_Client) : 0;
  }

  private getOfficeName(officeId : number) {
    if(this.refData && this.refData.OfficeList && officeId){
      let office = this.refData.OfficeList.find(o => o.Office_ID == officeId);
      if(office) {
        return office.Office_Name;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  public get BorrowerName() {
    return Get_Borrower_Name(this.loanMaster as any);
  }
}
