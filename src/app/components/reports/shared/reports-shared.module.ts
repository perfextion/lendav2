import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';

import { LoanInformationComponent } from './loan-information/loan-information.component';
import { ReportHeaderComponent } from './report-header/report-header.component';

@NgModule({
  imports: [CommonModule, SharedModule, MaterialModule, UiComponentsModule],
  declarations: [LoanInformationComponent, ReportHeaderComponent],
  exports: [LoanInformationComponent, ReportHeaderComponent]
})
export class ReportsSharedModule {}
