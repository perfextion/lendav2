import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import * as moment from 'moment';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { Get_Distributor_Name, Get_Borrower_Name } from '@lenda/services/common-utils';
import { LoanMaster } from '@lenda/models/ref-data-model';

import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-report-header',
  templateUrl: './report-header.component.html',
  styleUrls: ['./report-header.component.scss']
})
export class ReportHeaderComponent implements OnInit, OnDestroy {

  private subscription : ISubscription;
  loanMaster : LoanMaster;
  today  = new Date();

  @Input() reportName : string;
  @Input() subReportName : string;
  @Input() insideChevron : boolean = false;
  @Input() hideBorrowerDetails :boolean = false;
  /**
   * Will be used for server side report generation
   */
  @Input() ID : string = '';
  loanObject: loan_model;
  loanFullID: string = '';
  borrowerName: string = '';
  loanTypeAndDistCode: string = '';
  loanCropYear: number = 0;


  constructor(
    private localStorageService: LocalStorageService,
    private dataService: DataService
  ) {
    this.loanObject = this.localStorageService.retrieve(environment.loankey);
    this.loanFullID = this.loanObject.LoanMaster.Loan_Full_ID;
    this.loanCropYear = this.loanObject.LoanMaster.Crop_Year;
    this.loanTypeAndDistCode = `${this.loanObject.LoanMaster.Loan_Type_Name == 'Ag-Input' ? (this.loanObject.LoanMaster.Loan_Type_Name + ' (' + Get_Distributor_Name(this.loanObject, false) +')') : this.loanObject.LoanMaster.Loan_Type_Name}`;
  }

  ngOnInit() {
    this.subscription =  this.dataService.getLoanObject().subscribe((res: loan_model)=>{
      if(res){
        this.loanMaster = res.LoanMaster;
      }
    });
    this.getdataforgrid();
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  getdataforgrid(){

    let obj:loan_model=this.localStorageService.retrieve(environment.loankey);
    if(obj)
    {
      this.loanMaster=obj.LoanMaster;
    }
  }

  getCurrentDate(){
    return moment(this.loanMaster.Application_Date).fromNow();

  }

  get BorrowerName() {
    return Get_Borrower_Name(this.loanObject.LoanMaster as any);
  }
}
