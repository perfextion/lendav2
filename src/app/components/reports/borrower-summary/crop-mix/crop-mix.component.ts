import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { loan_model, Loan_Crop } from '@lenda/models/loanmodel';
import { Loan_Crop_Unit } from '@lenda/models/cropmodel';
import {
  lookupStateAbvRefValue,
  lookupCountyRefValue
} from '@lenda/Workers/utility/aggrid/stateandcountyboxes';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';
import { RefDataModel } from '@lenda/models/ref-data-model';

import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { DataService } from '@lenda/services/data.service';
import { Loan_Key_Visibilty } from '@lenda/models/loansettings';

@Component({
  selector: 'app-crop-mix',
  templateUrl: './crop-mix.component.html',
  styleUrls: ['./crop-mix.component.scss']
})
export class CropMixComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  private preferencesubscription: ISubscription;

  public CropMixArray: Array<CropMix> = [];
  public localLoanObject: loan_model;
  public refData: RefDataModel;

  @Input() colToDisplay: any = null;
  @Input() expanded = true;

  columnsDisplaySettings: ColumnsDisplaySettings;
  private LoankeySettings: Loan_Key_Visibilty;

  constructor(
    private localst: LocalStorageService,
    private dataService: DataService,
    private settingsService: SettingsService
  ) {}

  ngOnInit() {
    this.refData = this.localst.retrieve(environment.referencedatakey);

    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res) {
          this.localLoanObject = res;
          this.prepareData(
            this.localLoanObject.LoanCrops.filter(a => a.ActionStatus != 3),
            this.localLoanObject.LoanCropUnits
          );
        }
      });

    this.localLoanObject = this.localst.retrieve(environment.loankey);

    if (this.localLoanObject) {
      this.prepareData(
        this.localLoanObject.LoanCrops.filter(a => a.ActionStatus != 3),
        this.localLoanObject.LoanCropUnits
      );
    }

    this.getUserPreferences();
  }

  private getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;

    this.preferencesubscription = this.settingsService.preferencesChange.subscribe(
      preferences => {
        this.columnsDisplaySettings =
          preferences.loanSettings.columnsDisplaySettings;
        this.hideUnhideLoanKeys();
      }
    );
  }

  private hideUnhideLoanKeys() {
    try {
      let LoanSettings = JSON.parse(
        this.localLoanObject.LoanMaster.Loan_Settings
      );

      if (LoanSettings && LoanSettings.Loan_key_Settings) {
        this.LoankeySettings = LoanSettings.Loan_key_Settings;
      } else {
        this.LoankeySettings = new Loan_Key_Visibilty();
      }
    } catch {
      this.LoankeySettings = new Loan_Key_Visibilty();
    }

    this.columnsDisplaySettings.cropPrac = this.LoankeySettings.Crop_Practice == null ? this.columnsDisplaySettings.cropPrac : this.LoankeySettings.Crop_Practice;
    this.columnsDisplaySettings.cropType = this.LoankeySettings.Crop_Type == null ? this.columnsDisplaySettings.cropType : this.LoankeySettings.Crop_Type;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.preferencesubscription.unsubscribe();
  }

  private prepareData(
    loanCrops: Array<Loan_Crop>,
    cropUnits: Array<Loan_Crop_Unit>
  ) {
    loanCrops.forEach(crop => {
      let refCrop = this.refData.CropList.find(
        c => c.Crop_Code == crop.Crop_Code
      );

      let cropMix = new CropMix();
      let crpUnitsIRR = cropUnits.filter(
        cu =>
          cu.Crop_Code == crop.Crop_Code &&
          cu.Crop_Practice_Type_Code === PracticeType.IRR
      );

      this.populateCropMixVM(crpUnitsIRR, crop, cropMix.cropIRRMixArray);

      let irrSubtotal = new CropMixViewModel();
      irrSubtotal.crop = refCrop ? refCrop.Crop_Name : crop.Crop_Code;
      irrSubtotal.cropType = crop.Crop_Type_Code;
      irrSubtotal.practiceType = PracticeType.IRR;
      irrSubtotal.acres = _.sumBy(cropMix.cropIRRMixArray, 'acres');
      cropMix.totalIRRAcres = irrSubtotal;

      let crpUnitsNI = cropUnits.filter(
        cu =>
          cu.Crop_Code == crop.Crop_Code &&
          cu.Crop_Practice_Type_Code === PracticeType.NIR
      );

      this.populateCropMixVM(crpUnitsNI, crop, cropMix.cropNIRMixArray);

      let nirSubtotal = new CropMixViewModel();
      nirSubtotal.crop = refCrop ? refCrop.Crop_Name : crop.Crop_Code;
      nirSubtotal.cropType = crop.Crop_Type_Code;
      nirSubtotal.practiceType = PracticeType.NIR;
      nirSubtotal.acres = _.sumBy(cropMix.cropNIRMixArray, 'acres');
      cropMix.totalNIRAcres = nirSubtotal;

      cropMix.totalAcres =
        cropMix.totalIRRAcres.acres + cropMix.totalNIRAcres.acres;
      this.CropMixArray.push(cropMix);
    });
  }

  private populateCropMixVM(
    crpUnits: Loan_Crop_Unit[],
    crop: Loan_Crop,
    cropMixVM: Array<CropMixViewModel>
  ) {
    let refCrop = this.refData.CropList.find(
      c => c.Crop_Code == crop.Crop_Code
    );
    crpUnits.forEach(cu => {
      let farm = this.localLoanObject.Farms.find(f => f.Farm_ID == cu.Farm_ID);
      if (farm) {
        let cropMix = new CropMixViewModel();
        cropMix.practiceType = cu.Crop_Practice_Type_Code;

        cropMix.stateAbbrev = lookupStateAbvRefValue(
          farm.Farm_State_ID,
          this.refData
        );

        cropMix.county = lookupCountyRefValue(
          farm.Farm_County_ID,
          this.refData
        );

        cropMix.prodPer = farm.Percent_Prod.toFixed(1);
        cropMix.landlord = farm.Landowner;
        cropMix.fsn = farm.FSN;
        cropMix.section = farm.Section;
        cropMix.rated = farm.Rated;
        cropMix.crop = refCrop ? refCrop.Crop_Name : crop.Crop_Code;
        cropMix.cropType = crop.Crop_Type_Code;
        cropMix.acres = cu.CU_Acres || 0;
        cropMixVM.push(cropMix);
      }
    });
  }
}

class CropMixViewModel {
  practiceType: string;
  stateAbbrev: string;
  county: string;
  prodPer: string;
  landlord: string;
  fsn: string;
  section: string;
  rated: string;
  crop: string;
  cropType: string;
  acres: number;
}

enum PracticeType {
  IRR = 'Irr',
  NIR = 'NI'
}

class CropMix {
  cropIRRMixArray: Array<CropMixViewModel> = [];
  totalIRRAcres: CropMixViewModel;
  cropNIRMixArray: Array<CropMixViewModel> = [];
  totalNIRAcres: CropMixViewModel;
  totalAcres: number;
}
