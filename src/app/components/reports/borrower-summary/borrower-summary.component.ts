import { Component, OnInit, OnDestroy } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { Loansettings } from '@lenda/models/loansettings';

import { DataService } from '@lenda/services/data.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { MasterService } from '@lenda/master/master.service';

@Component({
  selector: 'app-borrower-summary-report',
  templateUrl: './borrower-summary.component.html',
  styleUrls: ['./borrower-summary.component.scss']
})
export class BorrowerSummaryReportComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  public cropNames: Array<string> = [];
  public tableColToDisplay: any = null;
  public loanSetting: Loansettings = new Loansettings();

  constructor(
    private localstorageservice: LocalStorageService,
    private dataService: DataService,
    private settingsService: SettingsService,
    public masterSvc: MasterService
  ) {
    let tctd = this.localstorageservice.retrieve(environment.loankey);
    if (!_.isEmpty(tctd)) {
      this.tableColToDisplay = JSON.parse(
        tctd.LoanMaster.Loan_Settings
      ).Loan_key_Settings;
    }
  }

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res != undefined && res != null) {
        let loanObject: loan_model = res;
        if (
          loanObject &&
          loanObject.LoanCropPractices &&
          loanObject.LoanBudget &&
          loanObject.LoanMaster
        ) {
          this.cropNames = _.uniqBy(loanObject.LoanCropPractices, 'FC_CropName').map(cp => cp.FC_CropName);
        }
      }
    });

    let loanObject: loan_model = this.localstorageservice.retrieve(
      environment.loankey
    );

    if (
      loanObject &&
      loanObject.LoanCropPractices &&
      loanObject.LoanBudget &&
      loanObject.LoanMaster
    ) {
      this.cropNames = _.uniqBy(loanObject.LoanCropPractices, 'FC_CropName').map(cp => cp.FC_CropName);
    }

    this.loanSetting = this.settingsService.userLoanSettings;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
