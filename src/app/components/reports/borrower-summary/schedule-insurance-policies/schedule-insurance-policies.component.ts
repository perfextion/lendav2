import { Component, OnInit, Input } from '@angular/core';

import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment';

import { loan_model } from '@lenda/models/loanmodel';
import { lookupStateValueFromPassedRefObject, lookupCountyValueFromPassedRefData, lookupCropValueFromPassedRefData } from '@lenda/Workers/utility/aggrid/stateandcountyboxes';
import { InsuranceReportModel, Loan_Policy } from '@lenda/models/insurancemodel';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';

import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { DataService } from '@lenda/services/data.service';
import { Loan_Key_Visibilty } from '@lenda/models/loansettings';

@Component({
  selector: 'app-schedule-insurance-policies',
  templateUrl: './schedule-insurance-policies.component.html',
  styleUrls: ['./schedule-insurance-policies.component.scss']
})
export class ScheduleInsurancePoliciesComponent implements OnInit {
  private subscription: ISubscription;
  private preferencesubscription: ISubscription;

  private rawloanPolicies: Array<Loan_Policy> = [];
  public loanPolicies: Array<InsuranceReportModel> = [];
  private loanPoliciesAfterSorting: Array<InsuranceReportModel> = [];

  @Input() public colToDisplay : any = null;
  @Input() public expanded = true;

  public columnsDisplaySettings: ColumnsDisplaySettings;
  private LoankeySettings: Loan_Key_Visibilty;
  private localloanobject: loan_model;

  constructor(
    private localStorageService: LocalStorageService,
    private dataService: DataService,
    private settingsService: SettingsService
  ) { }

  ngOnInit() {

    this.subscription = this.dataService.getLoanObject()
      .subscribe((res: loan_model) => {
        this.localloanobject = res;
        if (res) {
          this.rawloanPolicies = res.LoanPolicies;
          this.prepareData(res.LoanPolicies);
        }
      });

    this.getdataforgrid();
    this.getUserPreferences();
  }

  private getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;

    this.preferencesubscription= this.settingsService.preferencesChange.subscribe((preferences) => {
      this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
      this.hideUnhideLoanKeys();
    });
  }

  private hideUnhideLoanKeys() {
    try {
      let LoanSettings = JSON.parse(
        this.localloanobject.LoanMaster.Loan_Settings
      );

      if (LoanSettings && LoanSettings.Loan_key_Settings) {
        this.LoankeySettings = LoanSettings.Loan_key_Settings;
      } else {
        this.LoankeySettings = new Loan_Key_Visibilty();
      }
    } catch {
      this.LoankeySettings = new Loan_Key_Visibilty();
    }

    this.columnsDisplaySettings.cropPrac = this.LoankeySettings.Crop_Practice == null ? this.columnsDisplaySettings.cropPrac : this.LoankeySettings.Crop_Practice;
    this.columnsDisplaySettings.cropType = this.LoankeySettings.Crop_Type == null ? this.columnsDisplaySettings.cropType : this.LoankeySettings.Crop_Type;
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.preferencesubscription.unsubscribe();
  }

  private getdataforgrid() {
    this.localloanobject = this.localStorageService.retrieve(environment.loankey);
    if (this.localloanobject) {
      this.rawloanPolicies = this.localloanobject.LoanPolicies;
      this.prepareData( this.rawloanPolicies);
    }
  }

  private prepareData(loanPolicies: Array<Loan_Policy>) {
    let refData = this.localStorageService.retrieve(environment.referencedatakey);
      loanPolicies.filter(x=>x.ActionStatus!=3 && x.Select_Ind).forEach(ins => {
        let policy = new InsuranceReportModel();
        policy.County_Id = ins.County_ID;
        policy.State_Id = ins.State_ID;
        policy.Crop_Code = ins.Crop_Code;
        policy.Crop_Type_Code = ins.Crop_Type_Code;
        policy.Premium = ins.Premium;
        policy.Irr_Practice_Type_Code = ins.Irr_Practice_Type_Code;
        policy.Crop_Practice_Type_Code = ins.Crop_Practice_Type_Code;
        policy.HR_Exclusion_Ind = ins.HR_Exclusion_Ind;
        policy.Ins_Plan = ins.Ins_Plan;
        policy.Ins_Plan_Type = ins.Ins_Plan_Type;
        policy.Ins_Unit_Type_Code = ins.Ins_Unit_Type_Code;
        policy.Option = ins.Option;
        policy.Price = ins.Price;
        policy.Upper_Level = ins.Upper_Level;
        policy.Lower_Level = ins.Lower_Level;
        policy.Yield_Percent = ins.Yield_Percent;
        policy.Price_Percent = ins.Price_Percent;
        policy.Custom1 = ins.Ins_Plan=='PCI' ? ins.Custom1 : 0;
        policy.FC_StateAbbr = lookupStateValueFromPassedRefObject(policy.State_Id, refData);
        policy.FC_CountyName = lookupCountyValueFromPassedRefData(policy.County_Id, refData);
        policy.FC_CropName = lookupCropValueFromPassedRefData(policy.Crop_Code, refData);

        this.loanPolicies.push(policy);
      });

    this.loanPolicies.filter(y=>y.Ins_Plan=='MPCI').forEach(cp => {
      //get current MPCI item price
      let selectPrice = cp.Price;
      //Push this item to the top
      this.loanPoliciesAfterSorting.push(cp);

      //get all policies related to the above MPCI plan (as per group) and push in array + update price
      this.loanPolicies.filter(x => x.County_Id == cp.County_Id &&
        x.State_Id == cp.State_Id &&
        x.Irr_Practice_Type_Code == cp.Irr_Practice_Type_Code &&
        x.Crop_Practice_Type_Code == cp.Crop_Practice_Type_Code &&
        x.FC_CropName == cp.FC_CropName &&
        x.Ins_Plan != 'MPCI').forEach(p => {
          p.Price = selectPrice;
          this.loanPoliciesAfterSorting.push(p);
        }
        );
    });

    this.loanPolicies=this.loanPoliciesAfterSorting;
  }
}
