import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  ViewEncapsulation
} from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model, FarmOverrideModel } from '@lenda/models/loanmodel';
import { Loan_Farm } from '@lenda/models/farmmodel.';
import { RefDataModel } from '@lenda/models/ref-data-model';

import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-farms-report',
  templateUrl: './farms.component.html',
  styleUrls: ['./farms.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FarmsReportComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;

  public farms: Array<Loan_Farm> = [];
  public farmCrops: Array<FarmOverrideModel> = [];

  @Input() colToDisplay: any = null;
  @Input() expanded = true;

  constructor(
    private localStorageService: LocalStorageService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService
      .getLoanObject()
      .subscribe((res: loan_model) => {
        if (res) {
          this.farms = res.Farms;
          this.farmCrops = res.FarmCropOverride;
          this.prepareData(this.farms, this.farmCrops);
        }
      });

    this.getdataforgrid();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private getdataforgrid() {
    let obj: loan_model = this.localStorageService.retrieve(
      environment.loankey
    );
    if (obj) {
      this.farms = obj.Farms;
      this.farmCrops = obj.FarmCropOverride;
      this.prepareData(this.farms, this.farmCrops);
    }
  }

  private prepareData(
    farms: Array<Loan_Farm>,
    farmCrops: Array<FarmOverrideModel>
  ) {
    let refData: RefDataModel = this.localStorageService.retrieve(
      environment.referencedatakey
    );

    farmCrops.forEach(farmCrop => {
      let crop = refData.CropList.find(
        a => a.Crop_Full_Key.toLowerCase() == farmCrop.Crop_Code.toLowerCase()
      );

      if (crop) {
        farmCrop.crop = crop.Crop_Name;
        farmCrop.crop_prac = crop.Crop_Prac_Code as any;
        farmCrop.crop_type = crop.Crop_Type_Code as any;
        farmCrop.irr_prac = crop.Irr_Prac_Code as any;
      }
    });
  }

  public get colspanForRent() {
    if (this.colToDisplay) {
      if (this.colToDisplay.Section && this.colToDisplay.Rated) {
        return 4;
      }

      if (this.colToDisplay.Section || this.colToDisplay.Rated) {
        return 3;
      }

      return 2;
    }
    return 2;
  }
}
