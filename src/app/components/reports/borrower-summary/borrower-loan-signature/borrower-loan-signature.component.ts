import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-borrower-loan-signature',
  templateUrl: './borrower-loan-signature.component.html',
  styleUrls: ['./borrower-loan-signature.component.scss']
})
export class BorrowerLoanSignatureComponent implements OnInit {
  @Input() expanded = true;

  constructor() {}

  ngOnInit() {}
}
