import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';
import { ReportsSharedModule } from '../shared/reports-shared.module';
import { SummaryReportsSharedModule } from '../summary-shared/summary-report-shared.module';

import { BorrowerLoanSignatureComponent } from './borrower-loan-signature/borrower-loan-signature.component';
import { CropMixComponent } from './crop-mix/crop-mix.component';
import { FarmsReportComponent } from './farms/farms.component';
import { ScheduleInsurancePoliciesComponent } from './schedule-insurance-policies/schedule-insurance-policies.component';
import { BorrowerSummaryReportComponent } from './borrower-summary.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    UiComponentsModule,
    ReportsSharedModule,
    SummaryReportsSharedModule
  ],
  declarations: [
    BorrowerLoanSignatureComponent,
    CropMixComponent,
    FarmsReportComponent,
    ScheduleInsurancePoliciesComponent,
    BorrowerSummaryReportComponent
  ],
  exports: [BorrowerSummaryReportComponent]
})
export class BorrowerSummaryReportsModule {}
