import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatTabGroup } from '@angular/material';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/map';
import { saveAs } from 'file-saver/FileSaver';

import { environment } from '@env/environment.prod';
import { LoanGroup, loan_model } from '@lenda/models/loanmodel';

import { ApiService } from '@lenda/services/api.service';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';

import { CrossCollateralReportComponent } from './cross-collateral/cross-collateral.component';
import { SummaryReportComponent } from './summary/summary.component';
import { LoanMaster } from '@lenda/models/ref-data-model';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html'
})
export class ReportsComponent implements OnInit, OnDestroy {
  @ViewChild(SummaryReportComponent) child: SummaryReportComponent;
  @ViewChild(CrossCollateralReportComponent) child1: SummaryReportComponent;
  @ViewChild(MatTabGroup) matTabs: MatTabGroup;

  loanMaster: LoanMaster;
  loanGroups: Array<LoanGroup> = [];
  subscription: ISubscription;
  loanObject: loan_model;
  loanFullID: string = '';

  constructor(
    private localstorageservice: LocalStorageService,
    private apiservice: ApiService,
    private dtss: DatetTimeStampService
  ) {
    this.loanObject = this.localstorageservice.retrieve(environment.loankey);
  }

  ngOnInit() {
    this.subscription =  this.localstorageservice.observe(environment.loanGroup).subscribe(res=>{
      if (res) {
        this.loanGroups = res;
      }
    });

    this.loanGroups = this.localstorageservice.retrieve(environment.loanGroup) || [];
  }

  hasCrossCollatarizedLoan() {
    if (this.loanGroups) {
      return this.loanGroups.some(a => {
        return a.IsCrossCollateralized;
      });
    }
    return false;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public BlurCapture() {
    this.child.showARMDetails = false;
  }

  public BlurCapture1() {
    this.child1.showARMDetails = false;
  }

  public captureSummary() {
    let links = document.getElementsByTagName('link');
    for (let i = 0, l = links.length; i < l; i++) {
      links[i].href = links[i].href;
    }

    let data = document.getElementById('contentSummary').innerHTML.replace(/"/g, "'");
    let style = document.getElementsByTagName('head')[0].innerHTML.replace(/"/g, "'");

    let dd = {
      loanobj: data,
      style: style
    };

    this.apiservice.postpdf(dd).subscribe(fileData => {
      let b: any = new Blob([fileData], { type: 'application/pdf' });
      let filename = `Summary_${this.loanObject.LoanMaster.Loan_Full_ID}_${this.dtss.getTimeStamp()}.pdf`;
      saveAs(b, filename);
    });
  }

  public captureCrossCollateral() {
    let links = document.getElementsByTagName('link');
    for (let i = 0, l = links.length; i < l; i++) {
      links[i].href = links[i].href;
    }

    let data = document.getElementById('contentcrosscollateral').innerHTML.replace(/"/g, "'")
    let style = document.getElementsByTagName('head')[0].innerHTML.replace(/"/g, "'");

    let dd = {
      loanobj: data,
      style: style
    };

    this.apiservice.postpdf(dd).subscribe(fileData => {
      let b: any = new Blob([fileData], { type: 'application/pdf' });
      let filename = `CrossCollateral_${this.loanObject.LoanMaster.Loan_Full_ID}_${this.dtss.getTimeStamp()}.pdf`;
      saveAs(b, filename);
    });
  }

  public captureBorrowerSummary() {
    let links = document.getElementsByTagName('link');
    for (let i = 0, l = links.length; i < l; i++) {
      links[i].href = links[i].href;
    }

    let data = document.getElementById('contentBorrowerSummary').innerHTML.replace(/"/g, "'")
    let style = document.getElementsByTagName('head')[0].innerHTML.replace(/"/g, "'");

    let dd = {
      loanobj: data,
      style: style
    };

    this.apiservice.postpdf(dd).subscribe(fileData => {
      let b: any = new Blob([fileData], { type: 'application/pdf' });
      let filename = `BorrowerSummary_${this.loanObject.LoanMaster.Loan_Full_ID}_${this.dtss.getTimeStamp()}.pdf`;
      saveAs(b, filename);
    });
  }

  public captureLoanInventory() {
    let links = document.getElementsByTagName('link');
    for (let i = 0, l = links.length; i < l; i++) {
      links[i].href = links[i].href;
    }

    let data = document.getElementById('contentLoanInventorySummary').innerHTML.replace(/"/g, "'")
    let style = document.getElementsByTagName('head')[0].innerHTML.replace(/"/g, "'");

    let dd = {
      loanobj: data,
      style: style
    };

    this.apiservice.postpdf(dd).subscribe(fileData => {
      let b: any = new Blob([fileData], { type: 'application/pdf' });
      let filename = `LoanInventorySummary_${this.loanObject.LoanMaster.Loan_Full_ID}_${this.dtss.getTimeStamp()}.pdf`;
      saveAs(b, filename);
    });
  }

  public captureBalances() {
    let links = document.getElementsByTagName('link');
    for (let i = 0, l = links.length; i < l; i++) {
      links[i].href = links[i].href;
    }

    let data = document.getElementById('contentBalancesSummary').innerHTML.replace(/"/g, "'")
    let style = document.getElementsByTagName('head')[0].innerHTML.replace(/"/g, "'");

    let dd = {
      loanobj: data,
      style: style
    };

    this.apiservice.postpdf(dd).subscribe(fileData => {
      let b: any = new Blob([fileData], { type: 'application/pdf' });
      let filename = `BalancesSummary_${this.loanObject.LoanMaster.Loan_Full_ID}_${this.dtss.getTimeStamp()}.pdf`;
      saveAs(b, filename);
    });
  }

  public downloadReport(reportType: string) {
    return new Promise((res, rej) => {
      this.tryDownloadReport(reportType, res, rej);
    });
  }

  private tryDownloadReport(
    reportType: string,
    res: (value?: {} | PromiseLike<{}>) => void,
    rej: (reason?: any) => void
  ) {
    try {
      switch (reportType) {
        case 'Summary':
          this.captureSummary();
          break;

        case 'CrossCollateral':
          if (this.hasCrossCollatarizedLoan()) {
            this.matTabs.selectedIndex = 1;
            this.captureCrossCollateral();
          }
          break;

        case 'BorrowerSummary':
          if (this.hasCrossCollatarizedLoan()) {
            this.matTabs.selectedIndex = 2;
          }
          else {
            this.matTabs.selectedIndex = 1;
          }

          setTimeout(() => {
            this.captureBorrowerSummary();
          }, 1000);

          break;

        case 'LoanBalances':
          if (this.hasCrossCollatarizedLoan()) {
            this.matTabs.selectedIndex = 4;
          }
          else {
            this.matTabs.selectedIndex = 3;
          }

          setTimeout(() => {
            this.captureBalances();
          }, 1000);

          break;

        case 'LoanInventory':
          if (this.hasCrossCollatarizedLoan()) {
            this.matTabs.selectedIndex = 3;
          }
          else {
            this.matTabs.selectedIndex = 2;
          }

          setTimeout(() => {
            this.captureLoanInventory();
          }, 1000);

          break;
      }
      res();
    } catch (ex) {
      rej(ex.message);
    }
  }
}

export enum ReportType {
  Summary = 'Summary',
  CrossCollateral = 'Cross Collateral',
  BorrowerSummary = 'Borrower Summary'
}
