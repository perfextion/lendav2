import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';
import { ReportsSharedModule } from '../shared/reports-shared.module';

import { LoanBalanceSummaryComponent } from './loan-balance-summary/loan-balance-summary.component';
import { BudgetCommitmentDetailsComponent } from './budget-commitment-details/budget-commitment-details.component';
import { BalancesReportComponent } from './balances.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    UiComponentsModule,
    ReportsSharedModule
  ],
  declarations: [
    LoanBalanceSummaryComponent,
    BudgetCommitmentDetailsComponent,
    BalancesReportComponent
  ],
  exports: [BalancesReportComponent]
})
export class BalancesReportsModule {}
