import { Component, OnInit } from '@angular/core';

import { MasterService } from '@lenda/master/master.service';

@Component({
  selector: 'app-balances-report',
  templateUrl: './balances.component.html',
  styleUrls: ['./balances.component.scss']
})
export class BalancesReportComponent implements OnInit {
  constructor(public masterSvc: MasterService) {}

  ngOnInit() {}
}
