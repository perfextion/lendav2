import { Component, OnInit, Input } from '@angular/core';

import { ISubscription, Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { loan_model, LoanGroup, Loan_Budget } from '@lenda/models/loanmodel';
import { Get_Distributor_Name, Get_Borrower_Name, Get_Formatted_Date } from '@lenda/services/common-utils';

import { DataService } from '@lenda/services/data.service';

import { LoanSummaryBalance } from '../loan-balance-summary/loan-balance-summary.component';

@Component({
  selector: 'app-budget-commitment-details',
  templateUrl: './budget-commitment-details.component.html',
  styleUrls: ['./budget-commitment-details.component.scss']
})
export class BudgetCommitmentDetailsComponent implements OnInit {
  private subscription: ISubscription = new Subscription();

  @Input() ID : string = '';
  @Input() expanded = true;

  private localLoanObject: loan_model;
  public budgetCommitment : BudgetCommitmentVM = new BudgetCommitmentVM();
  private balances = new Array<any>();

  headerText : string = '';
  headersubText: string;

  constructor(
    private localStorageService: LocalStorageService,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res) {
        this.localLoanObject = res;
        this.prepareData();
      }
    });

    this.localLoanObject = this.localStorageService.retrieve(environment.loankey);
    this.prepareData();
    this.prepareBalancesData();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private prepareData() {
    let date = new Date();
    let today = Get_Formatted_Date(date);

    if(this.localLoanObject){
        // this.headerText = this.localLoanObject.LoanMaster.Crop_Year +' Crop Loan as of '+ today;
        this.headerText = 'As of '+ today;

        this.headersubText = this.localLoanObject.Loan_Full_ID +"; " + Get_Borrower_Name(this.localLoanObject.LoanMaster as any) +"; " +this.localLoanObject.LoanMaster.Crop_Year + " " +this.localLoanObject.LoanMaster.Loan_Type_Name;
        if(this.localLoanObject.LoanMaster.Loan_Type_Name == 'Ag-Input') {
          this.headersubText = this.headersubText + " (" + Get_Distributor_Name(this.localLoanObject, false)+")";
        }

        let loanBudgets = _.sortBy(
          this.localLoanObject.LoanBudget.filter(b=>b.Budget_Type === 'V' && b.Crop_Practice_ID ===0 && b.ARM_Budget_Crop),
          (bgt: Loan_Budget) => bgt.Sort_order || bgt.Expense_Type_ID
        );

        this.budgetCommitment = new BudgetCommitmentVM();
        this.budgetCommitment.total.expense = 'Total';

        loanBudgets.forEach(budget =>{
          let budgetCommitment : BudgetCommitment = new BudgetCommitment();
          budgetCommitment.loanid=budget.Loan_Full_ID;
          budgetCommitment.borrowername=this.localLoanObject.LoanMaster.Borrower_First_Name + " " + this.localLoanObject.LoanMaster.Borrower_Last_Name ;
          budgetCommitment.cropyear=this.localLoanObject.LoanMaster.Crop_Year;
          budgetCommitment.status=this.localLoanObject.LoanMaster.Loan_Status;
          budgetCommitment.expense = budget.Expense_Type_Name;
          budgetCommitment.commit = budget.ARM_Budget_Crop;
          budgetCommitment.usedPer = 80;
          budgetCommitment.used = budgetCommitment.commit * (budgetCommitment.usedPer/100);
          budgetCommitment.available = budgetCommitment.commit - budgetCommitment.used;
          budgetCommitment.outstanding= (this.localLoanObject.LoanMaster.Outstanding_Balance||0);
          budgetCommitment.maturitydate=this.localLoanObject.LoanMaster.Maturity_Date;
          this.budgetCommitment.budgetCommitments.push(budgetCommitment);
          this.budgetCommitment.total.commit += budgetCommitment.commit;
          this.budgetCommitment.total.used += budgetCommitment.used;
          this.budgetCommitment.total.available += budgetCommitment.available;
          this.budgetCommitment.total.outstanding+= budgetCommitment.outstanding;
        });
    }
  }

  private prepareBalancesData(){
    if(this.localLoanObject){
      //current balance
      let currentLoanBal : LoanSummaryBalance = new LoanSummaryBalance();
      currentLoanBal.headerText = this.localLoanObject.LoanMaster.Crop_Year ;

      currentLoanBal.commitment = this.localLoanObject.LoanMaster.Total_Commitment  || 0;
      currentLoanBal.used = currentLoanBal.commitment * 0.80;
      currentLoanBal.available = currentLoanBal.commitment - currentLoanBal.used;
      currentLoanBal.ballaneOutstanding = 0;
      currentLoanBal.userperc=(currentLoanBal.used /currentLoanBal.commitment *100).toFixed(1);
      currentLoanBal.maturityDate = this.localLoanObject.LoanMaster.Maturity_Date;
      currentLoanBal.overDueDays = Math.round((new Date().getTime() -  new Date(currentLoanBal.maturityDate).getTime())/(1000*60*60*24));
      this.balances.push(currentLoanBal);


      //previous balance
      let groupLoans : Array<LoanGroup> = this.localStorageService.retrieve(environment.loanGroup);
      if(groupLoans){
        let prevLoans = groupLoans.filter(gl=>gl.IsPreviousYearLoan);

        if(prevLoans.length){
          let latestPrevLoan = _.orderBy(prevLoans,['Loan_Full_ID'],['desc'])[0];

          let prevLoanBal : LoanSummaryBalance = new LoanSummaryBalance();
          prevLoanBal.headerText = latestPrevLoan.LoanJSON.LoanMaster.Crop_Year.toString() ;
          prevLoanBal.headersubtext='hahhaha';
          //prevLoanBal.headerText = '2017 Crop Loan as of '+ today; //todo
          prevLoanBal.commitment = latestPrevLoan.LoanJSON.LoanMaster.Total_Commitment  || 0;
          prevLoanBal.used = prevLoanBal.commitment * 0.80;
          prevLoanBal.available = prevLoanBal.commitment - prevLoanBal.used;
          prevLoanBal.ballaneOutstanding = 0;
          prevLoanBal.userperc=(currentLoanBal.used /currentLoanBal.commitment *100).toFixed(1);
          prevLoanBal.maturityDate = latestPrevLoan.LoanJSON.LoanMaster.Maturity_Date;
          //prevLoanBal.maturityDate = '03/15/2018';
          prevLoanBal.overDueDays = Math.round((new Date().getTime() -  new Date(prevLoanBal.maturityDate).getTime())/(1000*60*60*24));
          this.balances.push(prevLoanBal);
        }
      }
    }
  }
}

class BudgetCommitment {
  loanid:string='';
  status:string='';
  cropyear: number= 0;
  borrowername:string='';
  expense : string = '';
  commit : number = 0;
  used : number = 0;
  usedPer : number = 0;
  available: number =0;
  outstanding : number = 0;
  maturitydate: string = '';
}

class BudgetCommitmentVM{
  budgetCommitments : Array<BudgetCommitment> = [];
  total : BudgetCommitment = new BudgetCommitment();
}
