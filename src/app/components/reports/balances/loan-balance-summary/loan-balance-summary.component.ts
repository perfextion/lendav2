import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { ISubscription, Subscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { loan_model, LoanGroup } from '@lenda/models/loanmodel';
import { Get_Borrower_Name, Get_Formatted_Date, DateDiff } from '@lenda/services/common-utils';
import { Helper } from '@lenda/services/math.helper';

import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-loan-balance-summary',
  templateUrl: './loan-balance-summary.component.html',
  styleUrls: ['./loan-balance-summary.component.scss']
})
export class LoanBalanceSummaryComponent implements OnInit, OnDestroy {
  private subscription: ISubscription = new Subscription();
  private localLoanObject: loan_model;

  public balances: Array<LoanSummaryBalance> = [];

  @Input() public ID: string = '';
  @Input() public expanded = true;

  constructor(
    private localStorageService: LocalStorageService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res) {
        this.localLoanObject = res;
        this.prepareData();
      }
    });

    this.localLoanObject = this.localStorageService.retrieve(
      environment.loankey
    );
    this.prepareData();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private prepareData() {
    this.balances = [];
    if (this.localLoanObject) {
      let date = new Date();
      let today = Get_Formatted_Date(date);

      //current balance
      let currentLoanBal: LoanSummaryBalance = new LoanSummaryBalance();
      currentLoanBal.headerText = 'As of ' + today;
      currentLoanBal.commitment = this.localLoanObject.LoanMaster.Total_Commitment || 0;
      currentLoanBal.loanid = this.localLoanObject.Loan_Full_ID;
      currentLoanBal.status = this.localLoanObject.LoanMaster.Loan_Status;
      currentLoanBal.borrowername = Get_Borrower_Name(this.localLoanObject.LoanMaster);
      currentLoanBal.cropyear = this.localLoanObject.LoanMaster.Crop_Year.toString();
      currentLoanBal.used = currentLoanBal.commitment * 0.8;
      currentLoanBal.available = currentLoanBal.commitment - currentLoanBal.used;
      currentLoanBal.userperc = (100 - Helper.percent(currentLoanBal.available, currentLoanBal.commitment)).toFixed(2) + '%';
      currentLoanBal.ballaneOutstanding = 0;
      currentLoanBal.maturityDate = this.localLoanObject.LoanMaster.Maturity_Date;
      currentLoanBal.overDueDays = DateDiff(new Date(), currentLoanBal.maturityDate).days;
      this.balances.push(currentLoanBal);

      //previous balance
      let groupLoans: Array<LoanGroup> = this.localStorageService.retrieve(
        environment.loanGroup
      );
      if (groupLoans) {
        let prevLoans = groupLoans.filter(gl => gl.IsPreviousYearLoan);

        if (prevLoans.length) {
          let latestPrevLoan = _.orderBy(
            prevLoans,
            ['Loan_Full_ID'],
            ['desc']
          )[0];

          let prevLoanBal: LoanSummaryBalance = new LoanSummaryBalance();
          prevLoanBal.headerText = latestPrevLoan.LoanJSON.LoanMaster.Crop_Year.toString();
          prevLoanBal.loanid = this.localLoanObject.Loan_Full_ID;
          prevLoanBal.status = this.localLoanObject.LoanMaster.Loan_Status;
          prevLoanBal.borrowername = Get_Borrower_Name(this.localLoanObject.LoanMaster);
          prevLoanBal.cropyear = this.localLoanObject.LoanMaster.Crop_Year.toString();
          prevLoanBal.commitment = latestPrevLoan.LoanJSON.LoanMaster.Total_Commitment || 0;
          prevLoanBal.used = prevLoanBal.commitment * 0.8;
          prevLoanBal.available = prevLoanBal.commitment - prevLoanBal.used;
          prevLoanBal.ballaneOutstanding = 0;
          prevLoanBal.maturityDate = latestPrevLoan.LoanJSON.LoanMaster.Maturity_Date;
          prevLoanBal.overDueDays = DateDiff(new Date(), prevLoanBal.maturityDate).days;
          this.balances.push(prevLoanBal);
        }
      }
    }
  }
}

export class LoanSummaryBalance {
  loanid: string = '';
  status: string = '';
  headersubtext: string;
  cropyear: string | number = '';
  borrowername: string = '';
  headerText: string | number = '';
  commitment: number = 0;
  used: number = 0;
  userperc: string = '';
  available: number = 0;
  ballaneOutstanding: number = 0;
  maturityDate: string = '';
  overDueDays: number = 0;
}
