import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NortridgeUploadComponent } from './nortridge-upload.component';

describe('NortridgeUploadComponent', () => {
  let component: NortridgeUploadComponent;
  let fixture: ComponentFixture<NortridgeUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NortridgeUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NortridgeUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
