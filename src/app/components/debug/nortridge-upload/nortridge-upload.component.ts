import { Component, OnInit } from '@angular/core';

import { DataService } from '@lenda/services/data.service';
import { environment } from '@env/environment';
import { loan_model } from '@lenda/models/loanmodel';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { NgxXml2jsonService } from 'ngx-xml2json';

@Component({
  selector: 'app-nortridge-upload',
  templateUrl: './nortridge-upload.component.html',
  styleUrls: ['./nortridge-upload.component.scss']
})
export class NortridgeUploadComponent implements OnInit {
  loanid: string = '';
  xmltext = {};
  udftext = {};
  logs: [];
  uploaddate: string = "";
  myModel: string = "";
  startlimit:string="";
  endlimit:string="";
  rangeenabled: boolean;
  singleenabled: boolean;
  loanmask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  constructor(public apiservice: LoanApiService,
    public dataservice: DataService, private ngxXml2jsonService: NgxXml2jsonService) { }

  ngOnInit() {
    let loanmoadel = this.dataservice.localstorageservice.retrieve(environment.loankey) as loan_model;
    this.uploaddate = new Date(loanmoadel.LoanMaster.Nortridge_UploadDate).toDateString();
    this.loanid = loanmoadel.Loan_Full_ID;
    this.myModel = this.loanid;
  }

  UploadLoan() {
    if (this.singleenabled) {
      this.apiservice.UploadLoanToNortridge(this.myModel).subscribe((p) => {
        const parser = new DOMParser();
        const xml = parser.parseFromString(p.Data.LoanDetails, 'text/xml');
        this.xmltext = this.ngxXml2jsonService.xmlToJson(xml);
        const UDF = parser.parseFromString(p.Data.UDFs, 'text/xml');
        this.udftext = this.ngxXml2jsonService.xmlToJson(UDF);
        this.logs = p.Data.Logs.toString().split('_');
      })
    }
    else {
      let start=this.startlimit.split('-')[0];
      let end=this.endlimit.split('-')[0];
      this.uploadLoanSingleloop(start,end);
    }
  }

  uploadLoanSingleloop(startlimit,endlimit){

    let current=startlimit;
    this.apiservice.UploadLoanToNortridge(current + "-001").subscribe((p) => {
      const parser = new DOMParser();
      const xml = parser.parseFromString(p.Data.LoanDetails, 'text/xml');
      this.xmltext = this.ngxXml2jsonService.xmlToJson(xml);
      const UDF = parser.parseFromString(p.Data.UDFs, 'text/xml');
      this.udftext = this.ngxXml2jsonService.xmlToJson(UDF);
      this.logs = p.Data.Logs.toString().split('_');
        if(current!=endlimit)
        {
          this.uploadLoanSingleloop("00"+(parseInt(current)+1),endlimit);
        }
    })
  }

}

