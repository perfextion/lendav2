import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';
import { loan_model } from '@lenda/models/loanmodel';
import * as _ from 'lodash';
@Component({
  selector: 'app-croppractices',
  templateUrl: './croppractices.component.html',
  styleUrls: ['./croppractices.component.scss']
})
export class CroppracticesComponent implements OnInit {
  public records: loan_model;
  public refdata: any;

  constructor(private localstorageservice: LocalStorageService) {}

  ngOnInit() {
    this.records = this.localstorageservice.retrieve(environment.loankey);
    this.refdata = this.localstorageservice.retrieve(
      environment.referencedatakey
    );
  }

  sumbyvalue(property: string) {
    if (property == 'Market_Value') {
    }

    return _.sumBy(this.records.LoanCropPractices, p =>
      parseFloat(p[property] || 0)
    ).toFixed(2);
  }
}
