import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';

import { WorkInProgressComponent } from './work-in-progress.component';
import { AssociationRecordsComponent } from './associationrecords/associationrecords.component';
import { CollateralReportComponent } from './collateral-report/collateral-report.component';
import { ConditionsComponent } from './conditions/conditions.component';
import { CroppracticesComponent } from './croppractices/croppractices.component';
import { CropunitrecordsComponent } from './cropunitrecords/cropunitrecords.component';
import { CropUnitsInfoRecordsComponent } from './cropunitsinforecords/cropunitsinforecords.component';
import {
  ExceptionsComponent,
  SubmitLoanAlertComponent
} from './exceptions/exceptions.component';
import { FarmFinancialRecordsComponent } from './farmfinancialrecords/farm-financial-records.component';
import { FinanceStatsComponent } from './finance-stats/finance-stats.component';
import { LoanCropPracticePremiumComponent } from './loan-crop-practice-premium/loan-crop-practice-premium.component';
import { LoanCropUnitsPremiumComponent } from './loan-crop-units-premium/loan-crop-units-premium.component';
import { LoanCropsPremiumComponent } from './loan-crops-premium/loan-crops-premium.component';
import { LoanCollateralRecordsComponent } from './loancollateralrecords/loancollateralrecords.component';
import { LoanMarketingRecordsComponent } from './loanmarketingrecords/loanmarketingrecords.component';
import { SyncStatusComponent } from './syncstatus/syncstatus.component';
import { WfrprecordsComponent } from './wfrprecords/wfrprecords.component';
import { WorkInProgressStatsComponent } from './work-in-progress-stats/work-in-progress-stats.component';
import { FarmRecordsComponent } from './farmrecords/farmrecords.component';
import { LoanCropsRecordsComponent } from './loancroprecords/loancroprecords.component';

@NgModule({
  imports: [CommonModule, SharedModule, MaterialModule, UiComponentsModule],
  declarations: [
    WorkInProgressComponent,
    AssociationRecordsComponent,
    CollateralReportComponent,
    ConditionsComponent,
    CroppracticesComponent,
    CropunitrecordsComponent,
    CropUnitsInfoRecordsComponent,
    ExceptionsComponent,
    SubmitLoanAlertComponent,
    FarmFinancialRecordsComponent,
    FarmRecordsComponent,
    FinanceStatsComponent,
    LoanCropPracticePremiumComponent,
    LoanCropUnitsPremiumComponent,
    LoanCropsPremiumComponent,
    LoanCollateralRecordsComponent,
    LoanMarketingRecordsComponent,
    SyncStatusComponent,
    WfrprecordsComponent,
    WorkInProgressStatsComponent,
    LoanCropsRecordsComponent
  ],
  entryComponents: [SubmitLoanAlertComponent],
  exports: [WorkInProgressComponent]
})
export class WorkInProgressModule {}
