import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { loan_model } from '@lenda/models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';

import { RefDataModel } from '@lenda/models/ref-data-model';
import { environment } from '@env/environment.prod';

import * as _ from 'lodash';
import { Loan_Crop_Unit } from '@lenda/models/cropmodel';
@Component({
  selector: 'app-loan-crop-practice-premium',
  templateUrl: './loan-crop-practice-premium.component.html',
  encapsulation: ViewEncapsulation.None
})
export class LoanCropPracticePremiumComponent implements OnInit {
  public records: loan_model;
  public refdata: RefDataModel;

  constructor(private localstorageservice: LocalStorageService) {}

  ngOnInit() {
    this.records = this.localstorageservice.retrieve(environment.loankey);
    this.refdata = this.localstorageservice.retrieve(
      environment.referencedatakey
    );
  }

  sumbyvalue(property: string) {
    return _.sumBy(this.records.LoanCropPractices, p =>
      parseFloat(p[property] || 0)
    );
  }

  cropName(cropCode) {
    try {
      let crop = this.refdata.CropList.find(c => c.Crop_Code == cropCode);
      return crop.Crop_Name;
    } catch {
      return '';
    }
  }

  get loanData(): any {
    let d = _.groupBy(
      this.records.LoanCropUnits,
      (c: Loan_Crop_Unit) => c.Crop_Practice_ID
    );

    let data = [];

    for (let p in d) {
      let cp = d[p];

      let crop = this.records.LoanCropPractices.find(
        c => c.Crop_Practice_ID == cp[0].Crop_Practice_ID
      );

      let acres = _.sumBy(cp, c => c.CU_Acres);

      data.push({
        Loan_CU_ID: crop.Loan_Crop_Practice_ID,
        Crop_Practice_Type_Code: cp[0].Crop_Practice_Type_Code,
        CU_Acres: acres,
        Crop_Name: this.cropName(cp[0].Crop_Code),
        FC_MpciPremium: _.sumBy(cp, c => c.FC_MpciPremium || 0),
        FC_Premium_MPCI: _.sumBy(cp, c => c.FC_MpciPremium || 0) * acres,
        FC_HmaxPremium: _.sumBy(cp, c => c.FC_HmaxPremium || 0),
        FC_Premium_Hmax: _.sumBy(cp, c => c.FC_HmaxPremium || 0) * acres,
        FC_StaxPremium: _.sumBy(cp, c => c.FC_StaxPremium || 0),
        FC_Premium_Stax: _.sumBy(cp, c => c.FC_StaxPremium || 0) * acres,
        FC_ScoPremium: _.sumBy(cp, c => c.FC_ScoPremium || 0),
        FC_Premium_SCO: _.sumBy(cp, c => c.FC_ScoPremium || 0) * acres,
        FC_RampPremium: _.sumBy(cp, c => c.FC_RampPremium || 0),
        FC_Premium_Ramp: _.sumBy(cp, c => c.FC_RampPremium || 0) * acres,
        FC_IcePremium: _.sumBy(cp, c => c.FC_IcePremium || 0),
        FC_Premium_ICE: _.sumBy(cp, c => c.FC_IcePremium || 0) * acres,
        FC_AbcPremium: _.sumBy(cp, c => c.FC_AbcPremium || 0),
        FC_Premium_ABC: _.sumBy(cp, c => c.FC_AbcPremium || 0) * acres,
        FC_PciPremium: _.sumBy(cp, c => c.FC_PciPremium || 0),
        FC_Premium_PCI: _.sumBy(cp, c => c.FC_PciPremium || 0) * acres,
        FC_CrophailPremium: _.sumBy(cp, c => c.FC_CrophailPremium || 0),
        FC_Premium_CropHail: _.sumBy(cp, c => c.FC_CrophailPremium || 0) * acres
      });
    }

    return _.orderBy(data, d => d.Loan_CU_ID);
  }

  totalPremiumCrop(c) {
    return (
      (c.FC_Premium_MPCI || 0) +
      (c.FC_Premium_Hmax || 0) +
      (c.FC_Premium_Stax || 0) +
      (c.FC_Premium_SCO || 0) +
      (c.FC_Premium_Ramp || 0) +
      (c.FC_Premium_ICE || 0) +
      (c.FC_Premium_ABC || 0) +
      (c.FC_Premium_PCI || 0) +
      (c.FC_Premium_CropHail || 0)
    );
  }
}
