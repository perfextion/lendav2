import { Component, OnInit } from '@angular/core';
import { MatCheckboxChange } from '@angular/material';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { loan_model, LoanStatus } from '@lenda/models/loanmodel';

@Component({
  selector: 'app-work-in-progress',
  templateUrl: './work-in-progress.component.html',
  styleUrls: ['./work-in-progress.component.scss']
})
export class WorkInProgressComponent implements OnInit {
  public conditionList = [];
  public checked = false;

  private localLoanObject: loan_model;

  constructor(private localstorage: LocalStorageService) {}

  ngOnInit() {
    this.localLoanObject = this.localstorage.retrieve(environment.loankey);
    this.checked = this.localLoanObject.LoanMaster.Loan_Status != 'W';
  }

  onreadonlyChange(e: MatCheckboxChange) {
    if (e.checked == true) {
      this.localLoanObject.LoanMaster.Loan_Status = LoanStatus.Uploaded;
    } else {
      this.localLoanObject.LoanMaster.Loan_Status = LoanStatus.Working;
    }

    this.localstorage.store(environment.loankey, this.localLoanObject);
  }
}
