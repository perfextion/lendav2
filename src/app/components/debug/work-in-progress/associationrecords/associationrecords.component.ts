import { Component, OnInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

@Component({
  selector: 'app-associationrecords',
  templateUrl: './associationrecords.component.html',
  styleUrls: ['./associationrecords.component.scss']
})
export class AssociationRecordsComponent implements OnInit {
  public records = [];
  constructor(private localst: LocalStorageService) {}

  ngOnInit() {
    this.records = this.localst.retrieve(environment.loankey).Association;
    let groups = {};

    for (let i = 0; i < this.records.length; i++) {
      let groupName = this.records[i].Assoc_Type_Code;
      if (!groups[groupName]) {
        groups[groupName] = [];
      }
      groups[groupName].push(this.records[i]);
    }
  }

  changeColor(params) {
    if (params.ActionStatus === 3) {
      return { 'background-color': 'red' };
    } else if (params.ActionStatus === 2) {
      return { 'background-color': 'yellow' };
    } else if (params.ActionStatus === 1) {
      return { 'background-color': 'green' };
    }
  }
}
