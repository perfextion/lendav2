import { Component, OnInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

@Component({
  selector: 'app-cropunitrecords',
  templateUrl: './cropunitrecords.component.html',
  styleUrls: ['./cropunitrecords.component.scss']
})
export class CropunitrecordsComponent implements OnInit {
  public records: any;
  public refdata: any;
  constructor(private localstorageservice: LocalStorageService) {}

  ngOnInit() {
    this.records = this.localstorageservice.retrieve(environment.loankey);
    this.refdata = this.localstorageservice.retrieve(
      environment.referencedatakey
    );
  }

  changeColor(params) {
    if (params.ActionStatus === 3) {
      return { 'background-color': 'red' };
    } else if (params.ActionStatus === 2) {
      return { 'background-color': 'yellow' };
    } else if (params.ActionStatus === 1) {
      return { 'background-color': 'green' };
    }
  }
  returndecimalcapedvalue(value: any) {
    if (value == null || value == undefined || isNaN(value)) {
      value = 0;
    }
    return value.toFixed(2);
  }

  getCountynamefromid(id: any) {
    try {
      return this.refdata.CountyList.find(p => p.County_ID == id).County_Name;
    } catch {
      return 'NA';
    }
  }
  getprodpercfromfarmid(farmid: number) {
    return this.records.Farms.find(p => p.Farm_ID == farmid).Percent_Prod;
  }
  getinsurancerecordparameter(Id: number, param: string): any {
    let rec = this.records.LoanPolicies.find(p => p.Loan_Policy_ID == Id);
    if (rec == undefined) {
      return 'N/A';
    }
    return rec[param];
  }
  sumbyvalue(property: string) {
    return _.sumBy(this.records.LoanCropUnits, p => p[property] || 0).toFixed(
      2
    );
  }
  getsubpolicyparams(key: string, Type: String, param: string) {
    try {
      let rec = this.records.LoanPolicies.find(
        p => p.Policy_Key.includes(key) && p.Ins_Plan == Type
      );
      return rec[param];
    } catch (ex) {
      return 'N/A';
    }
  }
}
