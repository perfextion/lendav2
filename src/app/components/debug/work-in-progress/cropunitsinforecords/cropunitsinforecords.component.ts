import { Component, OnInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

@Component({
  selector: 'app-cropunitsinforecords',
  templateUrl: './cropunitsinforecords.component.html',
  styleUrls: ['./cropunitsinforecords.component.scss']
})
export class CropUnitsInfoRecordsComponent implements OnInit {
  public records = [];
  public refdata;
  constructor(private localst: LocalStorageService) {}

  ngOnInit() {
    this.records = this.localst.retrieve(environment.loankey).LoanCropUnits;
    this.refdata = this.localst.retrieve(environment.referencedatakey);
  }

  changeColor(params) {
    if (params.ActionStatus === 3) {
      return { 'background-color': 'red' };
    } else if (params.ActionStatus === 2) {
      return { 'background-color': 'yellow' };
    } else if (params.ActionStatus === 1) {
      return { 'background-color': 'green' };
    }
  }

  cropId(code) {
    let id = this.refdata.Crops.find(p => p.Crop_Code == code);
    return id.Crop_ID;
  }
}
