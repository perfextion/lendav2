import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';

@Component({
  selector: 'app-farmrecords',
  templateUrl: './farmrecords.component.html',
  styleUrls: ['./farmrecords.component.scss']
})
export class FarmRecordsComponent implements OnInit {
  public records = [];
  public refdata;
  constructor(
    private localstorageservice: LocalStorageService
  ) { }

  ngOnInit() {
    this.records = this.localstorageservice.retrieve(environment.loankey).Farms;
    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
  }

  changeColor(params){
    if(params.ActionStatus === 3){
      return { 'background-color': 'red'};
    }else if(params.ActionStatus === 2){
      return { 'background-color': 'yellow'};
    }else if(params.ActionStatus === 1){
      return { 'background-color': 'green'};
    }
  }

  farmCounty(id){
    let county = this.refdata.CountyList.find(p => p.County_ID == id);
    return county.County_Name;
  }

  farmState(id){
    let county = this.refdata.StateList.find(p => p.State_ID == id);
    return county.State_Name;
  }
}
