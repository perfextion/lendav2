import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import { DataService } from '@lenda/services/data.service';
import { loan_model } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import { LoanMasterCalculationWorkerService } from '@lenda/Workers/calculations/loan-master-calculation-worker.service';
import {
  calculatedNumberFormatter,
  currencyFormatter
} from '@lenda/aggridformatters/valueformatters';

@Component({
  selector: 'app-farm-financial-records',
  templateUrl: './farm-financial-records.component.html',
  styleUrls: ['./farm-financial-records.component.scss']
})
export class FarmFinancialRecordsComponent implements OnInit {
  private subscription: ISubscription;
  private localloanobj: loan_model;
  data = [];
  private ffStaticValue: any;

  headers = [
    'Borrower Rating',
    'Calculation Value',
    'Weight',
    'Rating',
    'Weight Sum'
  ];

  constructor(
    private dataService: DataService,
    private localst: LocalStorageService,
    private masterCalc: LoanMasterCalculationWorkerService
  ) {
    this.localloanobj = this.localst.retrieve(environment.loankey);
    this.bindData();
  }

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res != null) {
        this.localloanobj = res;
        this.bindData();
      }
    });
  }

  private bindData() {
    if (this.localloanobj && this.localloanobj.LoanMaster) {
      this.ffStaticValue = this.masterCalc.getFarmFinancialStaticValues(this.localloanobj.LoanMaster.FC_Owned);
      let loanMaster = this.localloanobj.LoanMaster;
      let farmFinancialRatingValues = this.getFormattedData(this.localloanobj);

      this.data = [
        {
          text: 'Current Ratio',
          value: calculatedNumberFormatter(
            farmFinancialRatingValues.currentRatio,
            2
          ),
          weight: this.getWeight(
            farmFinancialRatingValues.currentRatio_weight,
            2
          ),
          rating: calculatedNumberFormatter(
            farmFinancialRatingValues.currentRatio_rating,
            2
          ),
          weightSum: calculatedNumberFormatter(
            farmFinancialRatingValues.currentRatio_weight_sum,
            2
          ),
          caculation: [
            { text: 'Current Assets', value: loanMaster.Current_Assets },
            {
              text: 'Current Liabilities',
              value: loanMaster.Current_Liabilities
            }
          ]
        },
        {
          text: 'Working Capital',
          value: calculatedNumberFormatter(
            farmFinancialRatingValues.workingCapital,
            2
          ),
          weight: this.getWeight(
            farmFinancialRatingValues.workingCapital_weight,
            2
          ),
          rating: calculatedNumberFormatter(
            farmFinancialRatingValues.workingCapital_rating,
            2
          ),
          weightSum: calculatedNumberFormatter(
            farmFinancialRatingValues.workingCapital_weight_sum,
            2
          ),
          caculation: [
            { text: 'Current Assets', value: loanMaster.Current_Assets },
            {
              text: 'Current Liabilities',
              value: loanMaster.Current_Liabilities
            },
            {
              text: 'Revenue Threshold',
              value: this.masterCalc.getRevanueThresholdValue(this.localloanobj)
            }
          ]
        },
        {
          text: 'Debt/Assets',
          value: calculatedNumberFormatter(
            farmFinancialRatingValues.debtByAssets,
            2
          ),
          weight: this.getWeight(
            farmFinancialRatingValues.debtByAssets_weight,
            2
          ),
          rating: calculatedNumberFormatter(
            farmFinancialRatingValues.debtByAssets_rating,
            2
          ),
          weightSum: calculatedNumberFormatter(
            farmFinancialRatingValues.debtByAssets_weight_sum,
            2
          ),
          caculation: [
            { text: 'Totals Assets', value: loanMaster.Total_Assets },
            { text: 'Total Liabilities', value: loanMaster.Total_Liabilities }
          ]
        },
        {
          text: 'Equity/Assets',
          value: calculatedNumberFormatter(
            farmFinancialRatingValues.equityByAssets,
            2
          ),
          weight: this.getWeight(
            farmFinancialRatingValues.equityByAssets_weight,
            2
          ),
          rating: calculatedNumberFormatter(
            farmFinancialRatingValues.equityByAssets_rating,
            2
          ),
          weightSum: calculatedNumberFormatter(
            farmFinancialRatingValues.equityByAssets_weight_sum,
            2
          ),
          caculation: [
            { text: 'Totals Assets', value: loanMaster.Total_Assets },
            { text: 'Total Liabilities', value: loanMaster.Total_Liabilities }
          ]
        },
        {
          text: 'Debt/Equity',
          value: calculatedNumberFormatter(
            farmFinancialRatingValues.debtByEquity,
            2
          ),
          weight: this.getWeight(
            farmFinancialRatingValues.debtByEquity_weight,
            2
          ),
          rating: calculatedNumberFormatter(
            farmFinancialRatingValues.debtByEquity_rating,
            2
          ),
          weightSum: calculatedNumberFormatter(
            farmFinancialRatingValues.debtByEquity_weight_sum,
            2
          ),
          caculation: [
            { text: 'Totals Assets', value: loanMaster.Total_Assets },
            { text: 'Total Liabilities', value: loanMaster.Total_Liabilities }
          ]
        },
        {
          text: 'ROA',
          value: calculatedNumberFormatter(farmFinancialRatingValues.ROA, 2),
          weight: this.getWeight(farmFinancialRatingValues.ROA_weight),
          rating: calculatedNumberFormatter(
            farmFinancialRatingValues.ROA_rating,
            2
          ),
          weightSum: this.getWeightSum(
            calculatedNumberFormatter(
              farmFinancialRatingValues.ROA_weight_sum,
              2
            )
          ),
          caculation: [
            { text: 'Cash Flow Ammount', value: loanMaster.Cash_Flow_Amount },
            { text: 'Total Assets', value: loanMaster.Total_Assets }
          ]
        },
        {
          text: 'Operating Profit',
          value: this.getWeight(farmFinancialRatingValues.operatingProfit, 2),
          weight: this.getWeight(
            farmFinancialRatingValues.operatingProfit_weight
          ),
          rating: calculatedNumberFormatter(
            farmFinancialRatingValues.operatingProfit_rating,
            2
          ),
          weightSum: this.getWeightSum(
            calculatedNumberFormatter(
              farmFinancialRatingValues.operatingProfit_weight_sum,
              2
            )
          ),
          caculation: [
            { text: 'Cash Flow Ammount', value: loanMaster.Cash_Flow_Amount },
            {
              text: 'Revenue Threshold',
              value: this.masterCalc.getRevanueThresholdValue(this.localloanobj)
            }
          ]
        },
        {
          text: 'Operating Exp/Rev',
          value: calculatedNumberFormatter(
            farmFinancialRatingValues.operatingByExpRev,
            2
          ),
          weight: this.getWeight(
            farmFinancialRatingValues.operatingByExpRev_weight
          ),
          rating: calculatedNumberFormatter(
            this.getRating(
              farmFinancialRatingValues.operatingByExpRev,
              this.ffStaticValue.operatingByExpRev[0],
              this.ffStaticValue.operatingByExpRev[1]
            ),
            2
          ),
          weightSum: '-',
          caculation: [
            { text: 'Total Commitment', value: loanMaster.Total_Commitment },
            { text: 'Rate Fee Amount', value: loanMaster.Interest_Est_Amount },
            {
              text: 'Revenue Threshold',
              value: this.masterCalc.getRevanueThresholdValue(this.localloanobj)
            }
          ]
        },
        {
          text: 'Interest/Cashflow',
          value: calculatedNumberFormatter(
            farmFinancialRatingValues.interestByCashFlow,
            2
          ),
          weight: this.getWeight(
            farmFinancialRatingValues.interestByCashFlow_weight
          ),
          rating: calculatedNumberFormatter(
            this.getRating(
              farmFinancialRatingValues.interestByCashFlow,
              this.ffStaticValue.interestByCashFlow[0],
              this.ffStaticValue.interestByCashFlow[1]
            ),
            2
          ),
          weightSum: '-',
          caculation: [
            { text: 'Cash Flow Ammount', value: loanMaster.Cash_Flow_Amount },
            { text: 'Rate Fee Amount', value: loanMaster.Interest_Est_Amount }
          ]
        },
        {
          text: 'Total Farm Financial',
          value:
            calculatedNumberFormatter(
              loanMaster.Borrower_Farm_Financial_Rating
                ? loanMaster.Borrower_Farm_Financial_Rating
                : 0,
              1
            ) + '%',
          weight: calculatedNumberFormatter(
            farmFinancialRatingValues.totalFarmFinacialRating_weight,
            2
          ),
          rating: '',
          weightSum: calculatedNumberFormatter(
            farmFinancialRatingValues.weightSum,
            2
          ),
          caculation: []
        }
      ];
    }
  }

  private getFormattedData(loanObject: loan_model) {
    return this.masterCalc.getFarmFinancialRating(loanObject);
  }

  private getRevanueThresholdValue(loanObject) {
    return this.masterCalc.getRevanueThresholdValue(loanObject) || 0;
  }

  private getRating(input: number, strong: number, stable: number) {
    return (100 + ((input - strong) * (100 - 0)) / (strong - stable)) / 100;
  }

  private getWeightSum(weight: string) {
    if (parseFloat(weight) == 0) {
      return '-';
    }
    return weight;
  }

  getWeight(weight: number, x = null) {
    let v = calculatedNumberFormatter(weight, 2);

    if (parseFloat(v) == 0) {
      return '-';
    }
    return v;
  }
}

export class FarmFinacialValueParams {
  currentRatio: number;
  workingCapital: number;
  debtByAssets: number;
  debtByEquity: number;
  equityByAssets: number;
  ROA: number;
  operatingProfit: number;
  operatingByExpRev: number;
  interestByCashFlow: number;
  totalFarmFinacialRating: number;

  currentRatio_rating: number;
  workingCapital_rating: number;
  debtByAssets_rating: number;
  debtByEquity_rating: number;
  equityByAssets_rating: number;
  ROA_rating: number;
  operatingProfit_rating: number;
  operatingByExpRev_rating: number;
  interestByCashFlow_rating: number;
  totalFarmFinacialRating_rating: number;

  currentRatio_weight: number = 3;
  workingCapital_weight: number = 3;
  debtByAssets_weight: number = 3;
  debtByEquity_weight: number = 3;
  equityByAssets_weight: number = 3;
  ROA_weight: number = 1;
  operatingProfit_weight: number = 1;
  operatingByExpRev_weight: number;
  interestByCashFlow_weight: number;
  totalFarmFinacialRating_weight: number = 17;

  currentRatio_weight_sum: number;
  workingCapital_weight_sum: number;
  debtByAssets_weight_sum: number;
  debtByEquity_weight_sum: number;
  equityByAssets_weight_sum: number;
  ROA_weight_sum: number;
  operatingProfit_weight_sum: number;
  operatingByExpRev_weight_sum: number;
  interestByCashFlow_weight_sum: number;
  totalFarmFinacialRating_weight_sum: number;

  weightSum: number;
}
