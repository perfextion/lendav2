import { Component, OnInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';

@Component({
  selector: 'app-wfrprecords',
  templateUrl: './wfrprecords.component.html',
  styleUrls: ['./wfrprecords.component.scss']
})
export class WfrprecordsComponent implements OnInit {
  private localobj: loan_model;

  constructor(private localstorageservice: LocalStorageService) {}

  ngOnInit() {
    this.localobj = this.localstorageservice.retrieve(environment.loankey);
  }

  getvalue(property: string) {
    return this.localobj.Loanwfrp[property];
  }
}
