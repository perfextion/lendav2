import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { loan_model } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import { Loan_Crop_Unit } from '@lenda/models/cropmodel';

@Component({
  selector: 'app-loan-crop-units-premium',
  templateUrl: './loan-crop-units-premium.component.html',
  styleUrls: ['./loan-crop-units-premium.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoanCropUnitsPremiumComponent implements OnInit {
  public localLoanObject: loan_model;
  public refdata: RefDataModel;

  constructor(private localstorageservice: LocalStorageService) {}

  ngOnInit() {
    this.localLoanObject = this.localstorageservice.retrieve(
      environment.loankey
    );
    this.refdata = this.localstorageservice.retrieve(
      environment.referencedatakey
    );
  }

  getCountynamefromid(id: any) {
    return this.refdata.CountyList.find(p => p.County_ID == id).County_Name;
  }

  getprodpercfromfarmid(farmid: number) {
    return this.localLoanObject.Farms.find(p => p.Farm_ID == farmid)
      .Percent_Prod;
  }

  totalPremiumCrop(c: Loan_Crop_Unit) {
    return (
      (c.FC_MpciPremium || 0) +
      (c.FC_HmaxPremium || 0) +
      (c.FC_StaxPremium || 0) +
      (c.FC_ScoPremium || 0) +
      (c.FC_RampPremium || 0) +
      (c.FC_IcePremium || 0) +
      (c.FC_AbcPremium || 0) +
      (c.FC_PciPremium || 0) +
      (c.FC_CrophailPremium || 0)
    );
  }


  toDecimal(x: number){
    if(isNaN(x)){
      return 'NA';
    }

    return x;
  }
}
