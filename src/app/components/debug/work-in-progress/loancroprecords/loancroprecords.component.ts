import { Component, OnInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod'

import { Loan_Crop_Unit } from '@lenda/models/cropmodel';
import { loan_model } from '@lenda/models/loanmodel';
import { RefDataModel } from '@lenda/models/ref-data-model';

@Component({
  selector: 'app-loancroprecords',
  templateUrl: './loancroprecords.component.html',
  styleUrls: ['./loancroprecords.component.scss']
})
export class LoanCropsRecordsComponent implements OnInit {
  public records = [];
  public cropunits: Array<Loan_Crop_Unit>;
  localstorage: loan_model;
  refdata: RefDataModel;

  constructor(private localstorageservice: LocalStorageService) {}

  ngOnInit() {
    this.localstorage = this.localstorageservice.retrieve(
      environment.loankey
    ) as loan_model;
    this.refdata = this.localstorageservice.retrieve(
      environment.referencedatakey
    );
    this.cropunits = this.localstorage.LoanCropUnits as Array<Loan_Crop_Unit>;
    this.records = (_.uniqBy(
      this.localstorageservice.retrieve(environment.loankey).LoanCropUnits,
      'Crop_Code'
    ) as [any]).map(p => p.Crop_Code);
  }

  changeColor(params) {
    if (params.ActionStatus === 3) {
      return { 'background-color': 'red' };
    } else if (params.ActionStatus === 2) {
      return { 'background-color': 'yellow' };
    } else if (params.ActionStatus === 1) {
      return { 'background-color': 'green' };
    }
  }

  sumby(cropcode: string, property: string) {
    if (cropcode != 'ALL') {
      return (_.sumBy(
        this.cropunits.filter(p => p.Crop_Code == cropcode),
        p => p[property] || 0
      )).toFixed(2);
    } else {
      return (_.sumBy(this.cropunits, p => p[property] || 0)).toFixed(2);
    }
  }

  sumbyforBudget(cropcode: string, property: string) {
    if (cropcode != 'ALL') {
      let cropname = this.refdata.CropList.find(p => p.Crop_Code == cropcode)
        .Crop_Name;
      return _.sumBy(
        this.localstorage.LoanCropPractices.filter(
          p => p.FC_CropName == cropname
        ),
        p => p[property] || 0
      ).toFixed(2);
    } else {
      return _.sumBy(
        this.localstorage.LoanCropPractices,
        p => p[property] || 0
      ).toFixed(2);
    }
  }
}
