import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  EventEmitter,
  AfterViewChecked,
  ViewEncapsulation
} from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model, LoanException, LoanStatus } from '@lenda/models/loanmodel';
import { Page } from '@lenda/models/page.enum';
import { Loan_Exception } from '@lenda/models/loan/loan_exceptions';

import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { PublishService } from '@lenda/services/publish.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { ToasterService } from '@lenda/services/toaster.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-exceptions',
  templateUrl: './exceptions.component.html',
  styleUrls: ['./exceptions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ExceptionsComponent
  implements OnInit, OnDestroy, AfterViewChecked {
  private subscription: ISubscription;
  exceptions: Array<ExceptionGroupedList> = [];
  localLoanObject: loan_model;

  @Input() displayHeader = true;
  @Input() askForJudgement = false;
  loanStatus: string;
  LoanStatus: typeof LoanStatus = LoanStatus;

  recommendButtonClicked: boolean;
  errorsList: any = [];

  constructor(
    private localStorageService: LocalStorageService,
    private loanCalcultionService: LoancalculationWorker,
    private toasterService: ToasterService,
    private publishService: PublishService,
    private loanApi: LoanApiService,
    private logging: LoggingService,
    private dataService: DataService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.recommendButtonClicked = false;

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res) {
        this.localLoanObject = res;
        this.prepareData();
      }
    });
    this.localLoanObject = this.localStorageService.retrieve(
      environment.loankey
    );
    if (this.localLoanObject) {
      this.prepareData();
    }
  }

  checkErrors() {
    this.errorsList = this.localStorageService.retrieve(environment.errorbase);

    let errors = [];

    this.exceptions.forEach(execGroup => {
      execGroup.exceptions.forEach(exec => {
        if (exec.Mitigation_Text == '' && exec.Exception_ID_Level == 2) {
          errors.push({
            chevron: 'Exception',
            details: exec,
            tab: this.localStorageService.retrieve(environment.currentpage),
            level: exec.Exception_ID_Level
          });
        }
      });
    });

    if (_.isEmpty(this.errorsList)) {
      this.errorsList = errors;
    } else {
      errors.forEach(execErrors => {
        let hasAlreadyErr = _.find(this.errorsList, obj => {
          return (
            execErrors.details.Loan_Exception_ID ==
            obj.details.Loan_Exception_ID
          );
        });

        if (_.isEmpty(hasAlreadyErr)) {
          this.errorsList.push(execErrors);
        }
      });
    }

    if (!_.isEmpty(this.localLoanObject.LoanMaster['Loan_Settings'])) {
      let loanSettings = JSON.parse(
        this.localLoanObject.LoanMaster.Loan_Settings
      );
      loanSettings.validation_errors[
        this.localStorageService.retrieve(environment.currentpage)
      ] = {
        exceptions: {
          level1: 0,
          level2: 0
        },
        validations: {
          level1: 0,
          level2: errors.length
        }
      };

      this.localLoanObject.LoanMaster['Loan_Settings'] = JSON.stringify(
        loanSettings
      );
      this.localStorageService.store(environment.loankey, this.localLoanObject);
    }
  }

  ngAfterViewChecked() {
    this.checkErrors();
  }

  private prepareData() {
    let exceptions = this.localLoanObject.Loan_Exceptions.filter(
      ex => ex.ActionStatus != 3
    );
    this.loanStatus = this.localLoanObject.LoanMaster.Loan_Status;

    let tabIDs = [];

    exceptions.forEach(e => {
      if (!tabIDs.includes(e.Tab_ID)) {
        tabIDs.push(e.Tab_ID);
      }
    });

    this.exceptions = [];
    let refData = this.dataService.getRefData();

    if (refData && refData.RefTabs.length) {
      tabIDs.forEach(tab => {
        let refTab = refData.RefTabs.find(t => t.Tab_ID == tab);
        if (refTab) {
          this.exceptions.push({
            tab: refTab.Tab_Name,
            exceptions: exceptions.filter(e => e.Tab_ID == tab)
          });
        }
      });
    }

    this.checkErrors();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onBlur(exception: LoanException) {
    this.publishService.enableSync(Page.committee);
    exception.Action_Status = 2;
    this.loanCalcultionService.performcalculationonloanobject(
      this.localLoanObject,
      false
    );
  }

  isLoanEditable() {
    return (
      !this.localLoanObject ||
      this.localLoanObject.LoanMaster.Loan_Status === LoanStatus.Working
    );
  }
}

class ExceptionGroupedList {
  tab: string = '';
  exceptions: Array<Loan_Exception> = [];
}

@Component({
  selector: 'submit-loan-dialog',
  templateUrl: 'submit-loan-dialog.component.html',
  styleUrls: ['./../../../../alertify/components/alertify.style.scss']
})
export class SubmitLoanAlertComponent implements OnInit {
  responseReceived = new EventEmitter<boolean>();
  constructor(public dialogRef: MatDialogRef<SubmitLoanAlertComponent>) {}

  ngOnInit() {}

  onNoClick(event): void {
    this.responseReceived.emit(false);
  }

  onSubmitClick(event): void {
    this.responseReceived.emit(true);
  }
}
