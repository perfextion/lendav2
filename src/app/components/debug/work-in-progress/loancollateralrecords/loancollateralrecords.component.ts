import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod'
import * as _ from 'lodash';

@Component({
  selector: 'app-loancollateralrecords',
  templateUrl: './loancollateralrecords.component.html',
  styleUrls: ['./loancollateralrecords.component.scss']
})
export class LoanCollateralRecordsComponent implements OnInit {
  public records = [];
  computeRec: any = [];
  CollateralCategoryCodeTotals: any = [];

  private measureItems;
  private collateralMktDisc;
  private collateralCategory;
  private contractType;
  private collateralInsDisc;
  private refdata;

  constructor(
    private localstorageservice: LocalStorageService
  ) {
    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);

    if(this.refdata){
      this.measureItems = this.refdata.MeasurementType || [];
      this.collateralMktDisc = this.refdata.CollateralMktDisc || [];
      this.collateralCategory = this.refdata.CollateralCategory || [];
      this.contractType = this.refdata.ContractType || [];
      this.collateralInsDisc = this.refdata.CollateralInsDisc || [];
    }
  }

  ngOnInit() {
    this.records = this.localstorageservice.retrieve(environment.loankey).LoanCollateral;
    this.records.sort((e1, e2) => {
      let comp = (e1.Collateral_Category_Code > e2.Collateral_Category_Code) ? 1 : -1;
      return comp;
    });

    this.getCollateralCategoryCodeTotals();

    this.CollateralCategoryCodeTotals.forEach(element => {
      this.computeRec.push(_.filter(this.records, ['Collateral_Category_Code', element.CCCode]));
    });
  }

  changeColor(params){
    if(params.ActionStatus === 3){
      return { 'background-color': 'red'};
    }else if(params.ActionStatus === 2){
      return { 'background-color': 'yellow'};
    }else if(params.ActionStatus === 1){
      return { 'background-color': 'green'};
    }
  }

  getCollateralCategoryCodeTotals() {
    let CCC = null;
    let CCCTempTotals = null;
    let CCCTotals = [];

    this.records.map(function(el) {
      if (CCC === null || CCC != el.Collateral_Category_Code) {
        CCC = el.Collateral_Category_Code;
        CCCTempTotals = {
          CCCode: CCC,
          Prior_Lien_Amount: 0,
          Market_Value: 0,
          Net_Market_Value: 0,
          Disc_Value: 0,
          Insurance_Value: 0,
          Disc_Ins_Value: 0,
          Measure_Code: el.Measure_Code
        };

        if (CCCTempTotals != null) {
          CCCTotals.push(CCCTempTotals);
        }
      }
      if (CCC === el.Collateral_Category_Code) {
        CCCTempTotals.Prior_Lien_Amount = CCCTempTotals.Prior_Lien_Amount + el.Prior_Lien_Amount;
        CCCTempTotals.Market_Value = CCCTempTotals.Market_Value + el.Market_Value;
        CCCTempTotals.Net_Market_Value = parseFloat(CCCTempTotals.Net_Market_Value) + parseFloat(el.Net_Market_Value);
        CCCTempTotals.Disc_Value = CCCTempTotals.Disc_Value + el.Disc_Value;
        CCCTempTotals.Insurance_Value = CCCTempTotals.Insurance_Value + el.Insurance_Value;
        CCCTempTotals.Disc_Ins_Value = CCCTempTotals.Disc_Ins_Value + el.Disc_Ins_Value;
      }
    });

    this.CollateralCategoryCodeTotals = CCCTotals;
  }

  private getInsuredValue(value){
    return value == 0 ? 'No' : 'Yes';
  }

  getMktDisc(collaterCategoryCode: string){
    let mktValue = this.collateralMktDisc.find( c => c.Collateral_Category_Code == collaterCategoryCode);
    return mktValue ? mktValue.Disc_Percent.toFixed(1) + '%' : '-';
  }

  getMeasDisc(measCode: string){
    let measValue = this.measureItems.find( c => c.Measurement_Type_Code == measCode);
    return measValue ? measValue.Disc_Percent.toFixed(1) + '%' : '-';
  }

  getMeasName(measCode: string){
    let measValue = this.measureItems.find( c => c.Measurement_Type_Code == measCode);
    return measValue ? measValue.Measurement_Type_Name : '-';
  }

  getContractDisc(contractTypeCode: string){
    let measValue = this.contractType.find( c => c.Contract_Type_Code == contractTypeCode);
    return measValue ? measValue.Disc_Adj.toFixed(1) + '%' : '-';
  }

}
