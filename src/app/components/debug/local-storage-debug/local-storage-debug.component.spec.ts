import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalStorageDebugComponent } from './local-storage-debug.component';

describe('LocalStorageDebugComponent', () => {
  let component: LocalStorageDebugComponent;
  let fixture: ComponentFixture<LocalStorageDebugComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalStorageDebugComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalStorageDebugComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
