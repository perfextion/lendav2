import { Component, OnInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment';

import { loan_model, LoanException } from '@lenda/models/loanmodel';
import { Loan_Exception } from '@lenda/models/loan/loan_exceptions';

@Component({
  selector: 'app-exceptions-debug-info',
  templateUrl: './exceptions-debug-info.component.html',
  styleUrls: ['./exceptions-debug-info.component.scss']
})
export class ExceptionsDebugInfoComponent implements OnInit {
  public loanExceptions: Loan_Exception[];
  constructor(private localst: LocalStorageService) {}

  ngOnInit() {
    let currentLoan: loan_model = this.localst.retrieve(environment.loankey);
    this.loanExceptions = currentLoan.Loan_Exceptions;
  }

  /**
   * Gets exceptions
   * @param tabName name of tab
   * TODO: Change to tabid and map with ref data
   */
  getExceptionsInTab(Tab_ID: number) {
    return this.loanExceptions.filter((exception: Loan_Exception) => {
      if (exception.Tab_ID) {
        return exception.Tab_ID == Tab_ID;
      }
      return false;
    });
  }
}
