import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageExceptionsComponent } from './page-exceptions.component';

describe('PageExceptionsComponent', () => {
  let component: PageExceptionsComponent;
  let fixture: ComponentFixture<PageExceptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageExceptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageExceptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
