import { Component, OnInit, Input } from '@angular/core';
import { LoanException } from '@lenda/models/loanmodel';

@Component({
  selector: 'app-page-exceptions',
  templateUrl: './page-exceptions.component.html',
  styleUrls: ['./page-exceptions.component.scss']
})
export class PageExceptionsComponent implements OnInit {
  @Input() tabName: string = 'Undefined';
  displayedColumns: string[] = ['Loan_Exception_ID'];
  _exceptions: LoanException[] = [];
  constructor() {}

  ngOnInit() {}

  get exceptions() {
    return this._exceptions;
  }

  @Input()
  set exceptions(val: LoanException[]) {
    this._exceptions = val;
    if (val && val[0]) {
      this.displayedColumns = Object.keys(val[0]);
    }
  }
}
