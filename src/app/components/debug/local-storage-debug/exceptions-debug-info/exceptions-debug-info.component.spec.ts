import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceptionsDebugInfoComponent } from './exceptions-debug-info.component';

describe('ExceptionsDebugInfoComponent', () => {
  let component: ExceptionsDebugInfoComponent;
  let fixture: ComponentFixture<ExceptionsDebugInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceptionsDebugInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceptionsDebugInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
