import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';
import { Page } from '@lenda/models/page.enum';

@Component({
  selector: 'app-local-storage-debug',
  templateUrl: './local-storage-debug.component.html',
  styleUrls: ['./local-storage-debug.component.scss']
})
export class LocalStorageDebugComponent implements OnInit {
  constructor(private localstorageservice: LocalStorageService) {}

  ngOnInit() {
    this.localstorageservice.store(environment.currentpage, Page.debug);
  }
}
