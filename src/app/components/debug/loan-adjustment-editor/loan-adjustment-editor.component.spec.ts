import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanAdjustmentEditorComponent } from './loan-adjustment-editor.component';

describe('LoanAdjustmentEditorComponent', () => {
  let component: LoanAdjustmentEditorComponent;
  let fixture: ComponentFixture<LoanAdjustmentEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanAdjustmentEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanAdjustmentEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
