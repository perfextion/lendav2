import { DataService } from '@lenda/services/data.service';

import { loan_model, UpdateMigrationAdjustmentModel } from './../../../models/loanmodel';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { variable } from '@angular/compiler/src/output/output_ast';
import { ToasterService } from '@lenda/services/toaster.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';

@Component({
  selector: 'app-loan-adjustment-editor',
  templateUrl: './loan-adjustment-editor.component.html',
  styleUrls: ['./loan-adjustment-editor.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoanAdjustmentEditorComponent implements OnInit {
  public code: string = "";
  public localLoanObject: loan_model;
  public refdata: RefDataModel;

  selectedOfficeId = 4;

  constructor(
    private localstorageservice: LocalStorageService,
    public ApiService: LoanApiService,
    public alert: ToasterService,
    public dataservice: DataService,
    private alertify: AlertifyService,
    private loancalc: LoancalculationWorker
  ) {
    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
  }

  ngOnInit() {
    this.localLoanObject = this.localstorageservice.retrieve(environment.loankey);
    if (this.localLoanObject.LoanMaster.Legacy_Loan_ID > 0) {
      this.code = JSON.stringify(JSON.parse(this.localLoanObject.LoanMaster.Migrated_Loan_Adjustments), null, 2);
    }
  }

  update() {
    if (this.localLoanObject.LoanMaster.Legacy_Loan_ID > 0) {
      let vari = new UpdateMigrationAdjustmentModel();
      vari.LoanFullID = this.localLoanObject.Loan_Full_ID;
      vari.PayLoad_Json = this.code;
      this.ApiService.UpdateMigrationAdjustment(vari).subscribe(_res => {
        if (_res.Data == true) {
          this.alert.success("Field has been Updated Successfully");
          this.localLoanObject.LoanMaster.Migrated_Loan_Adjustments = this.code;
          this.loancalc.performcalculationonloanobject(this.localLoanObject, true, true);
        } else {
          this.alert.error("Operation Failed");
        }
      },
      () => {
        this.alert.error("Operation Failed");
      });
    }
  }

  generateAdj() {
    let message = '';
    if(!this.selectedOfficeId) {
      message ='Please select Office.';
      this.alert.error(message);
      return;
    }

    this.ApiService.GenerateMigratedAdjustmentsForLoans().subscribe(_res => {
      if (_res.Data == true) {
        this.alert.success("Field has been Updated Successfully");
        this.getloan();
      } else {
      this.alert.error("Operation Failed!try again");
      }
   });
  }

  generateMktAdj() {
    let message = '';

    if(!this.selectedOfficeId) {
      message ='Please select Office.';
      this.alert.error(message);
      return;
    }

    this.ApiService.GenerateMktAndInsValueAdjustment(this.selectedOfficeId).subscribe(_res => {
      if (_res.ResCode == 1) {
        this.alert.success( _res.Data + ' Loans updated.');
        this.getloan();
      } else {
      this.alert.error("Operation Failed!try again");
      }
   });
  }

  generateCFandRCAdj() {
    let message = '';
    if(!this.selectedOfficeId) {
      message ='You have not selected any office. It will affect all office loans';
      this.alert.error(message);
      return;
    }

    this.ApiService.GenerateCashflowAndRiskCushionAdjustment(this.selectedOfficeId).subscribe(_res => {
      if (_res.ResCode == 1) {
        this.alert.success( _res.Data + ' Loans updated.');
        this.getloan();
      } else {
      this.alert.error("Operation Failed!try again");
      }
   });
  }

  clearAdj() {
    let message = 'Are you sure you want to clear adjusments?';

    if(!this.selectedOfficeId) {
      message +=' (You have not selected any office. It will affect all office loans.)'
    }

    this.alertify.confirm('Confirm', message).subscribe(res => {
      if(res) {
        this.ApiService.ClearMigratedAdjustmentsForLoans(this.selectedOfficeId).subscribe(_res => {
          if (_res.ResCode == 1) {
            this.alert.success( _res.Data + ' Loans updated.');
            this.getloan();
          } else {
          this.alert.error("Operation Failed!try again");
          }
       });
      }
    });
  }

  getloan() {
    this.ApiService.getLoanById(this.localLoanObject.Loan_Full_ID).subscribe(res => {
      this.dataservice.setLoanObjectwithoutsubscription(res.Data);
      this.localLoanObject = res.Data;
      if (this.localLoanObject.LoanMaster.Legacy_Loan_ID > 0) {
        this.code = JSON.stringify(JSON.parse(this.localLoanObject.LoanMaster.Migrated_Loan_Adjustments), null, 2);
      }
    });
  }
}
