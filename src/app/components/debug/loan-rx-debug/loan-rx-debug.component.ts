import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { LoanRxCalculationWorker } from '@lenda/Workers/calculations/loanrxworker.service';
import { loan_model } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import { Page } from '@lenda/models/page.enum';

@Component({
  selector: 'app-loan-rx-debug',
  templateUrl: './loan-rx-debug.component.html',
  styleUrls: ['./loan-rx-debug.component.scss']
})
export class LoanRxDebugComponent implements OnInit {
  loan_obj: loan_model;
  original_loan_obj_string: string;

  constructor(
    private localstorage: LocalStorageService,
    private loanrxworker: LoanRxCalculationWorker
  ) {
    this.loan_obj = this.localstorage.retrieve(environment.loankey);
    this.original_loan_obj_string = JSON.stringify(this.loan_obj);
    this.localstorage.store(environment.currentpage, Page.debug);
  }

  ngOnInit() {}

  moveAllAcresToIrr() {
    this.loan_obj = this.loanrxworker.moveAllAcresToIrr(this.original_loan_obj_string);
  }

  moveAllAcresToNI() {
    this.loan_obj = this.loanrxworker.moveAllAcresToNI(this.original_loan_obj_string);
  }

  maxCF(isIRR: boolean) {
    this.loan_obj = this.loanrxworker.maxCF(this.original_loan_obj_string, isIRR);
  }

  maxRC(isIRR: boolean) {
    this.loan_obj = this.loanrxworker.maxRC(this.original_loan_obj_string, isIRR);
  }

  maxMargin(isIRR: boolean) {
    this.loan_obj = this.loanrxworker.maxMargin(this.original_loan_obj_string, isIRR);
  }

  maximizerMPCILevelPercent() {
    this.loan_obj = this.loanrxworker.maximizerMPCILevelPercent(this.original_loan_obj_string);
  }
}
