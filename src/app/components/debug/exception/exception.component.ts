import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { loan_model, AssociationTypeCode } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import { RegEx } from '@lenda/shared/shared.constants';

@Component({
  selector: 'app-exception-hyperfield',
  templateUrl: './exception.component.html',
  styleUrls: ['./exception.component.scss']
})
export class ExceptionComponent implements OnInit {
  associcationType = '';
  field = '';
  operator = 'IS';
  action = '';

  localloanobject: loan_model;

  Table_Name: string;
  Field_Name: string;
  isArray = false;
  Item_ID: number;
  Primary_Key: string;

  exceptionResult: any;
  exceptionTime: number;

  result = [];

  constructor(private localstorage: LocalStorageService) {}

  get exp() {
    return `${this.associcationType}.${this.field} ${this.operator} ${
      this.action
    }`;
  }

  ngOnInit() {
    this.localloanobject = this.localstorage.retrieve(environment.loankey);
  }

  calculate() {
    let associations = this.getAssociation(this.associcationType);

    this.result = associations.map(assoc => {
      let val = assoc[this.field];

      let res = this.getResult(this.action, val);

      return {
        result: res,
        level: 1
      };
    });
  }

  getResult(action: string, value: any) {
    switch (action) {
      case 'ValidEmail':
        return ValidEmail(value);

      case 'ValidPhone':
        return ValidPhone(value);
    }
  }

  getAssociation(associcationType: string) {
    switch (associcationType) {
      case 'LienHolder':
        return this.localloanobject.Association.filter(
          a => a.Assoc_Type_Code == AssociationTypeCode.LienHolder
        );

      case 'Agency':
        return this.localloanobject.Association.filter(
          a => a.Assoc_Type_Code == AssociationTypeCode.Agency
        );

      case 'Distributor':
        return this.localloanobject.Association.filter(
          a => a.Assoc_Type_Code == AssociationTypeCode.Distributor
        );

      case 'Rebator':
        return this.localloanobject.Association.filter(
          a => a.Assoc_Type_Code == AssociationTypeCode.Rebator
        );

      case 'LandLord':
        return this.localloanobject.Association.filter(
          a => a.Assoc_Type_Code == AssociationTypeCode.Landlord
        );
    }
  }

  calculateException() {
    console.time('calculateException');

    let start = new Date().getTime();

    for(let i = 0; i < 1000; i ++) {
      let obj = <LoanConditionDetail>{
      Field_Name: this.Field_Name,
      Is_Array: this.isArray,
      Item_ID: this.Item_ID,
      Primary_Key: this.Primary_Key,
      Table_Name: this.Table_Name
      };

      try {
        this.exceptionResult = this.getValue(obj);
      } catch {
        this.exceptionResult = '';
      }
    }

    let end = new Date().getTime();
    this.exceptionTime = start - end;

    console.timeEnd('calculateException');
  }

  getValue(detail: LoanConditionDetail) {
    let exceptionResult = '';

    if(!detail.Is_Array) {
      if(detail.Table_Name && detail.Field_Name) {
        let v = eval(`this.localloanobject.${detail.Table_Name}.${detail.Field_Name}`);
        exceptionResult = v;
      }
    } else {
      if(
        detail.Field_Name &&
        detail.Item_ID &&
        detail.Primary_Key &&
        detail.Table_Name
      ) {
        let expression = `this.localloanobject.${detail.Table_Name}.find(item => item['${detail.Primary_Key}'] == ${detail.Item_ID})`;
        let item = eval(expression);

        exceptionResult = item[detail.Field_Name];
      }
    }

    return exceptionResult;
  }
}

function ValidEmail(email: string) {
  return RegEx.Email.test(email);
}

function ValidPhone(Phone: string) {
  return RegEx.Phone.test(Phone);
}

export class LoanConditionDetail {
  Table_Name: string;
  Field_Name: string;

  Is_Array: boolean;
  Primary_Key: string;
  Item_ID: number;
}
