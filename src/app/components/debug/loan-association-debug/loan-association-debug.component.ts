import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { Loan_Association, loan_model } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';

@Component({
  selector: 'app-loan-association-debug',
  templateUrl: './loan-association-debug.component.html',
  styleUrls: ['./loan-association-debug.component.scss']
})
export class LoanAssociationDebugComponent implements OnInit {
  Associations: Array<Loan_Association>;
  private localLoanObject: loan_model;

  constructor(private localstorage: LocalStorageService) {
    this.localLoanObject = this.localstorage.retrieve(environment.loankey);
    this.Associations = this.localLoanObject.Association;
  }

  ngOnInit() {}
}
