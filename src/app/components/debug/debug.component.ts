import { currencyFormatter, numberFormatter, percentageFormatter, acresFormatter, BudgetFormatter } from '@lenda/aggridformatters/valueformatters';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoanApiService } from '../../services/loan/loanapi.service';
import { ValidationHelper } from '../../services/validation.helper.service';

import { loan_model, Add_Loan_Comments } from '@lenda/models/loanmodel';
import { CommentTypeMock, PriorityMock } from '@lenda/models/comment-type/mock-comment-type';
import { UserApiService } from '../../services/user/user.service';
import { User } from '../../models/user';
import { MatSnackBar } from '@angular/material';
import { Page } from '@lenda/models/page.enum';
import { ToastrService } from 'ngx-toastr';
import { EmailTemplates, EmailTemplate, EmailModel } from '@lenda/models/email.model';
import * as _ from 'lodash';
import { RKSFind } from '@lenda/services/common-utils';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { GetAllUsers } from '../committee/members/member.model';

@Component({
  selector: 'app-debug',
  templateUrl: './debug.component.html',
  styleUrls: ['./debug.component.scss']
})
export class DebugComponent implements OnInit {

  public commentForm: FormGroup;

  // Value Formatter form group
  public valueFormatterForm: FormGroup;

  private localloanobject: loan_model = new loan_model();

  commentTypeList: any;
  priorityList: any;

  userForms = new FormControl();
  users: GetAllUsers[];
  selectedUsers: any;
  templates: any[];
  selectedTemplate: any;
  loan: loan_model;

  // Value Formatter Output
  currencyFormatOutput: any = 0;
  numberFormatOutput: any = 0;
  acresFormatOutput: any = 0;
  percentageFormatOutput: any = 0;
  budgetFormatOutput: any = 0;

  loan_number: string;
  template: EmailTemplate;

  refData: RefDataModel;

  discounts: {[key: string]: number} = {};

  constructor(private fb: FormBuilder, private loanapi: LoanApiService, public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker, private userService: UserApiService,
    public snackBar: MatSnackBar, private toaster: ToastrService) {
    this.commentForm = this.fb.group({
      Loan_Full_ID: ['', [Validators.required]],
      User_ID: ['', [Validators.required, ValidationHelper.numberValidator]],
      Comment_Type_Code: ['', [Validators.required, ValidationHelper.numberValidator]],
      Comment_Type_Level_Code: ['', [Validators.required, ValidationHelper.numberValidator]],
      Comment_Text: ['', [Validators.required]],
      Parent_Emoji_ID: ['', [ValidationHelper.numberValidator]],
      Comment_Emoji_ID: ['', [ValidationHelper.numberValidator]]
    });

    // Value Formatte Form Controls
    this.valueFormatterForm = this.fb.group({
      budgetFormatterFC: ['', []],
      currencyFormatterFC: ['', []], //ValidationService.numberValidator
      currencyFormatterDecimalFC: ['', []],
      numberFormatterFC: ['', []],
      acresFormatterFC: ['', []],
      percentageFormatterFC: ['', []], // ValidationService.numberValidator
      percentageFormatterDecimalFC: ['', []],
    });

    this.users = new Array<GetAllUsers>();
    // this.templates = [{ text: 'Loan Approved Email', id: 0 }, { text: 'User Registration Email', id: 1 }];
    this.templates = EmailTemplates;

    this.refData = this.localstorageservice.retrieve(environment.referencedatakey);
    this.refData.Discounts.forEach(disc => {
      this.discounts[disc.Discount_Key] = disc.Discount_Value;
    });

    this.localstorageservice.store(environment.currentpage, Page.debug);
  }

  ngOnInit() {

    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);

    this.commentTypeList = CommentTypeMock;
    this.priorityList = PriorityMock;

    this.userService.getUsers().subscribe(res => {
      for (let i = 0; i <= res.Data.length - 1; i++) {
        this.users.push(res.Data[i]);
      }
    });
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  public OnAddCommentBtn() {
    if (this.commentForm.dirty && this.commentForm.valid) {
      this.doAddComment();
    } else {
      this.validateAllFormFields(this.commentForm);
    }
  }

  public doAddComment() {
    const loanComment = new Add_Loan_Comments();
    loanComment.Loan_Full_ID = this.commentForm.value.Loan_Full_ID;
    loanComment.User_ID = this.commentForm.value.User_ID;
    loanComment.Comment_Text = this.commentForm.value.Comment_Text;
    loanComment.Parent_Emoji_ID = this.commentForm.value.Parent_Emoji_ID;
    loanComment.Comment_Emoji_ID = this.commentForm.value.Comment_Emoji_ID;
    loanComment.Comment_Read_Ind = 0;
    loanComment.Comment_Type_Code = this.commentForm.value.Comment_Type_Code;
    loanComment.Comment_Type_Level_Code = this.commentForm.value.Comment_Type_Level_Code;

    this.loanapi.addComment(loanComment).subscribe((res) => {
      if (res.ResCode == 1) {
        // Successfully added the comment
        // Now fetch the latest comments
        this.loanapi.getComments(loanComment.Loan_Full_ID, this.commentForm.value.User_ID).subscribe((com) => {
          this.localloanobject.LoanComments = com.Data;
          // Save to Local Storage
          this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
        });
        this.commentForm.controls['Loan_Full_ID'].setValue('');
        this.commentForm.controls['User_ID'].setValue('');
        this.commentForm.controls['Comment_Type_Code'].setValue('');
        this.commentForm.controls['Comment_Type_Level_Code'].setValue('');
        this.commentForm.controls['Comment_Text'].setValue('');
        this.commentForm.controls['Parent_Emoji_ID'].setValue('');
        this.commentForm.controls['Comment_Emoji_ID'].setValue('');
      } else {
        // Error while adding the comment

      }
    });
  }

  sendEmail() {
    this.loan = this.localstorageservice.retrieve(environment.loankey);
    this.userService.sendUsersEmail(this.selectedUsers, this.loan.Loan_Full_ID, this.selectedTemplate.id)
    .subscribe((res) => {

      if (res) {
        this.snackBar.open('Email Sent', 'successfully', {
          verticalPosition: 'top',
          duration: 2000
        });
      }
    });
  }

  sendEmailToCommittee(){
    let body = <EmailModel>{
      emailTemplateType: this.template.id,
      loanNumber : this.loan_number
    };

    this.userService.sendEmailToCommittee(body).subscribe(res => {
      if(res.Data) {
        this.toaster.success('Email sent successfully.','Success');
      } else {
        this.toaster.error('Email sent failed','Error');
      }
    });
  }


  // VALUE FORMATTER TEST FUCNTIONS
  formatToBudget() {
    this.budgetFormatOutput = BudgetFormatter(this.valueFormatterForm.controls.budgetFormatterFC.value);
  }

  formatToCurrency() {
    let decimalPlace = this.valueFormatterForm.controls.currencyFormatterDecimalFC.value;
    this.currencyFormatOutput = currencyFormatter(this.valueFormatterForm.controls.currencyFormatterFC,
      decimalPlace == null ? 0 : decimalPlace);
  }

  formatToNumber() {
    this.numberFormatOutput = numberFormatter(this.valueFormatterForm.controls.numberFormatterFC);
  }

  formatToAcres() {
    this.acresFormatOutput = acresFormatter(this.valueFormatterForm.controls.acresFormatterFC);
  }

  formatToPercentage() {
    let decimalPlace = this.valueFormatterForm.controls.percentageFormatterDecimalFC.value;
    this.percentageFormatOutput = percentageFormatter(this.valueFormatterForm.controls.percentageFormatterFC,
      decimalPlace == null ? 0 : decimalPlace);
  }
  // /VALUE FORMATTER TEST FUCNTIONS


  findDisc(key: string, default_value){
    if(!default_value || default_value == ''){
      default_value = 100;
    }
    return RKSFind(key, this.discounts, default_value);
  }
}
