import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-exception-playground',
  templateUrl: './exception-playground.component.html',
  styleUrls: ['./exception-playground.component.scss']
})
export class ExceptionPlaygroundComponent implements OnInit {
  evalExpression: string;
  operator: string;
  level1: string;
  level2: string;
  message: string;
  exceptionForm: FormGroup;
  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.exceptionForm = this.fb.group({
      EvalField: ['', Validators.required],
      ExceptionDesc: ['', Validators.required],
      Operator: ['', Validators.required],
      Level1: [''],
      Level2: ['']
    });
  }

  checkForAllException() {
    //this.exceptionService.processAllExceptions();
  }

  runExceptionLogic() {
    // let exceptionObj : Exception = Object.assign(new Exception(), this.exceptionForm.getRawValue());
    // exceptionObj.Type = ExceptionType.Attribute;
    //this.exceptionService.processException(exceptionObj);
  }
}
