import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { RefDataModel } from '@lenda/models/ref-data-model';
import { loan_model } from '@lenda/models/loanmodel';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment';
import * as _ from 'lodash';

@Component({
  selector: 'app-loan-exception-debug',
  templateUrl: './loan-exception-debug.component.html',
  styleUrls: ['./loan-exception-debug.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoanExceptionDebugComponent implements OnInit {
  refdata: RefDataModel;
  localloanobject: loan_model;

  constructor(private localstorage: LocalStorageService) {
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);
    this.localloanobject = this.localstorage.retrieve(environment.loankey);
  }

  ngOnInit() {}

  get Exceptions() {
    return _.sortBy(this.localloanobject.Loan_Exceptions, 'Sort_Order');
  }
}
