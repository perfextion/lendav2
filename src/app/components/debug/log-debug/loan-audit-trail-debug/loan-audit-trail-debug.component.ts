import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { LoanAuditTrail } from '@lenda/models/audit-trail/audit-trail';

@Component({
  selector: 'app-loan-audit-trail-debug',
  templateUrl: './loan-audit-trail-debug.component.html',
  styleUrls: ['./loan-audit-trail-debug.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoanAuditTrailDebugComponent implements OnInit {
  auditTrails: Array<LoanAuditTrail>;
  totalAuditTrail: Array<LoanAuditTrail>;

  auditTrailTypes: Array<{code; value}>;
  selectedAuditTrailType: string = '';

  constructor(
    private loanApiService: LoanApiService,
    private localStorage: LocalStorageService,
    private toastr: ToastrService
  ) {
    this.auditTrailTypes = [];
  }

  ngOnInit() {
    let loanFullID = this.localStorage.retrieve(environment.loanidkey);
    this.loanApiService.getAuditTrails(loanFullID).subscribe(
      res => {
        if (res.ResCode == 1) {
          this.auditTrails = _.orderBy(res.Data, ['Loan_Audit_Trail_ID'], ['desc']);
          this.totalAuditTrail = JSON.parse(JSON.stringify(this.auditTrails));
          this.auditTrailTypes = _.uniqBy(this.auditTrails, a => a.Audit_Trail_Type).map(a => {
            return {
              code: a.Audit_Trail_Type,
              value: a.Audit_Trail_Type
            };
          });

          this.auditTrailTypes.unshift({
            code: '',
            value: 'Select All'
          });
        } else {
          this.toastr.error('Error during fetching Audit Trails');
        }
      },
      error => {
        this.toastr.error('Error during fetching Audit Trails');
      }
    );
  }

  selectionChange() {
    if(this.selectedAuditTrailType) {
      this.auditTrails = this.totalAuditTrail.filter(a => a.Audit_Trail_Type == this.selectedAuditTrailType);
    } else {
      this.auditTrails = this.totalAuditTrail;
    }
  }

  deleteAuditTrails() {
    let loanFullID = this.localStorage.retrieve(environment.loanidkey);
    this.loanApiService.deleteAuditTrails(loanFullID,  this.selectedAuditTrailType).subscribe(
      res => {
        if (res.ResCode == 1) {
          this.toastr.success('Successfully deleted audit trails');
          _.remove(this.totalAuditTrail, a => this.auditTrails.some(y => y.Loan_Audit_Trail_ID == a.Loan_Audit_Trail_ID));
          this.auditTrails = [];
        } else {
          this.toastr.error('Error during deleting Audit Trails');
        }
      },
      error => {
        this.toastr.error('Error during deleting Audit Trails');
      }
    );
  }
}
