import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PageEvent, Sort } from '@angular/material';

import { LocalStorageService } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';

import { environment } from '@env/environment.prod';

import { LogsRequestParams, LogResponseModel, LogListModel } from './logs.model';

import { LoanApiService } from '@lenda/services/loan/loanapi.service';

@Component({
  selector: 'app-loan-logs-debug',
  templateUrl: './loan-logs-debug.component.html',
  styleUrls: ['./loan-logs-debug.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoanLogsDebugComponent implements OnInit {
  public requestParams: LogsRequestParams;
  public logResponse: LogResponseModel;

  public expandedElement: LogListModel;

  constructor(
    private loanApiService: LoanApiService,
    private localst: LocalStorageService,
    private toastr: ToastrService
  ) {
    this.requestParams = new LogsRequestParams();
  }

  ngOnInit() {
    this.requestParams.Loan_Full_ID = this.localst.retrieve(
      environment.loanidkey
    );
    this.getData();
  }

  private getData() {
    this.loanApiService.getLogs(this.requestParams).subscribe(
      res => {
        if (res.ResCode == 1) {
          this.logResponse = res.Data;
        } else {
          this.toastr.error('Error Occured');
        }
      },
      _ => {
        this.toastr.error('Error Occured');
      }
    );
  }

  public searchLogs() {
    this.getData();
  }

  changePage(event: PageEvent) {
    this.requestParams.PageNumber = event.pageIndex + 1;
    this.getData();
  }

  sortData(event: Sort) {
    this.requestParams.Sort_Column = event.active;
    this.requestParams.Sort_Order = event.direction == 'asc' ? 1 : 0;
    this.getData();
  }
}
