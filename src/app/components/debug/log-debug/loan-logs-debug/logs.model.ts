export class LogListModel {
  Log_Id: number;
  Log_Section: string;
  Log_Message: string;
  Log_datetime: string;
  Exec_Time: number;
  Severity: number;
  Sql_Error: string;
  userID: number;
  Loan_Full_ID: string;
  Batch_ID: string;
  SourceName: string;
  SourceDetail: string;
  Status: number;
}

export class LogsRequestParams {
  PageNumber: number;
  Sort_Column: string;
  Sort_Order: number;
  Loan_Full_ID: string;

  Severity: string;
  SourceName: string;
  Log_Section: string;

  constructor() {
    this.Sort_Column = 'Log_Id';
    this.Sort_Order = 0;
    this.PageNumber = 1;
    this.Log_Section = '';
    this.Severity = '';
    this.SourceName = '';
  }
}

export class PageInfo {
  Count: number;
}

export class LogResponseModel {
  PageInfo: PageInfo;
  Data: Array<LogListModel>;
}
