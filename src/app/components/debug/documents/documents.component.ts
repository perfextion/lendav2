import { Component, OnInit } from '@angular/core';
import {
  Loan_Document,
  Loan_Document_Action
} from '@lenda/models/loan/loan_document';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {
  Documents: Array<Loan_Document> = [];

  Loan_Document: Loan_Document;

  Borrower_Name: string;
  Borrower_Email: string;
  Borrower_Address: string;

  Document_Type_ID = 100;
  Document_Template_ID = 10;
  Exception_ID = 2;
  Borrower_ID = 124;
  constructor() {}

  ngOnInit() {
    this.Documents = [];
  }

  generateDocument() {
    if (this.Borrower_Name && this.Borrower_Email && this.Borrower_Address) {
      let details = {
        Exception_ID: this.Exception_ID,
        Borrower_Name: this.Borrower_Name,
        Borrower_Email: this.Borrower_Email,
        Borrower_Address: this.Borrower_Address,
        Borrower_ID: this.Borrower_ID
      };

      let doc = new Loan_Document();
      doc.Loan_Document_ID = getRandomInt(100, 10000);

      doc.Document_Name = `Document for ${this.Borrower_Name}, ${
        this.Borrower_Email
      }, ${this.Borrower_Address}`;

      doc.ActionStatus = 1;
      doc.Document_Details = JSON.stringify(details);
      doc.Document_Type_ID = this.Document_Type_ID;
      doc.Document_Template_ID = this.Document_Template_ID;
      doc.Action_Ind = Loan_Document_Action.RequireUpload;
      doc.Document_Key = this.Borrower_ID.toString();

      doc.Upload_Date_Time = null;

      this.Loan_Document = doc;

      this.addIfNotExists(doc.Document_Type_ID, doc.Document_Key);
    }
  }

  addIfNotExists(Document_Type_ID: number, Field_ID: string) {
    let index = this.Documents.findIndex(
      a =>
        a.Document_Type_ID == Document_Type_ID &&
        a.Document_Key == Field_ID &&
        a.Action_Ind != Loan_Document_Action.Archived
    );

    if (index == -1) {
      this.Documents.push(this.Loan_Document);
    } else {
      let doc = this.Documents[index];

      if (
        doc.Action_Ind == Loan_Document_Action.RequireReupload ||
        doc.Action_Ind == Loan_Document_Action.RequireUpload
      ) {
        let ID = this.Documents[index].Loan_Document_ID;

        this.Documents[index] = this.Loan_Document;
        this.Documents[index].Loan_Document_ID = ID;
        this.Documents[index].ActionStatus = 2;
      } else {
        try {
          if (
            doc.Document_Details.toLowerCase() !=
            this.Loan_Document.Document_Details.toLowerCase()
          ) {
            doc.Action_Ind = Loan_Document_Action.Archived;

            this.Loan_Document.Action_Ind =
              Loan_Document_Action.RequireReupload;

            this.Loan_Document.Prev_Loan_Document_ID = doc.Loan_Document_ID;

            this.Documents.push(this.Loan_Document);
          }
        } catch (ex) {
          console.log(ex);
        }
      }
    }
  }

  uploadDocument() {
    let index = this.Documents.findIndex(
      a =>
        a.Document_Type_ID == this.Document_Type_ID &&
        a.Document_Key == this.Borrower_ID.toString() &&
        a.Action_Ind != Loan_Document_Action.Archived
    );

    if (index > -1) {
      let doc = this.Documents[index];

      if (doc.Action_Ind == Loan_Document_Action.Uploaded) {
        return;
      } else {
        doc.Action_Ind = Loan_Document_Action.Uploaded;
        doc.Upload_Date_Time = new Date().toISOString();
        doc.Upload_User_ID = getRandomInt(100, 1000);
        doc.KL_Document_ID = getRandomInt(1000, 10000);
        doc.ActionStatus = 3;
      }
    }
  }
}
