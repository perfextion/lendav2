import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { loan_model } from '@lenda/models/loanmodel';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { environment } from '@env/environment.prod';
import { Loan_Document_Action } from '@lenda/models/loan/loan_document';
import * as _ from 'lodash';

@Component({
  selector: 'app-loan-documents-debug',
  templateUrl: './loan-documents-debug.component.html',
  styleUrls: ['./loan-documents-debug.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoanDocumentsDebugComponent implements OnInit {
  localloanobject: loan_model;
  refdata: RefDataModel;

  constructor(private localstorage: LocalStorageService) {
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);
    this.localloanobject = this.localstorage.retrieve(environment.loankey);
  }

  ngOnInit() {}

  get Document() {
    return _.sortBy(this.localloanobject.Loan_Documents, ['Document_Type_Level', 'Sort_Order']);
  }

  getActionInd(Action_Ind: number) {
    if (Action_Ind == Loan_Document_Action.Archived) {
      return 'Deactivated';
    }

    if (Action_Ind == Loan_Document_Action.RequireReupload) {
      return 'Reupload';
    }

    if (Action_Ind == Loan_Document_Action.RequireUpload) {
      return 'Upload';
    }

    if (Action_Ind == Loan_Document_Action.Uploaded) {
      return 'Uploaded';
    }

    return 'Deactivated';
  }
}
