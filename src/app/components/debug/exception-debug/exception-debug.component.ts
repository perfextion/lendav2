import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { RefDataModel, Exception_Detail } from '@lenda/models/ref-data-model';
import { environment } from '@env/environment.prod';
import { loan_model } from '@lenda/models/loanmodel';

@Component({
  selector: 'app-ref-exception-debug',
  templateUrl: './exception-debug.component.html',
  styleUrls: ['./exception-debug.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ExceptionDebugComponent implements OnInit {
  refdata: RefDataModel;
  localloanobject: loan_model;

  constructor(private localstorage: LocalStorageService) {
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);
    this.localloanobject = this.localstorage.retrieve(environment.loankey);
  }

  ngOnInit() {}

  get Exceptions() {
    return this.refdata.RefExceptions || [];
  }

  getDetails(details: Exception_Detail) {
    let detail = '';

    try {
      detail += 'Added - ' + details.Documents.length + '\n';
      detail += 'Existing - ' + details.Exisiting_Documents.length + '\n';
      detail += 'Deleted - ' + details.Deleted_Documents.length + '\n';
      return detail;
    } catch {
      return detail;
    }
  }
}
