import { Pipe, PipeTransform } from '@angular/core';
import { Ref_Exception } from '@lenda/models/ref-data-model';

@Pipe({
  name: 'exceptionHyperocde'
})
export class ExceptionHyperocdePipe implements PipeTransform {
  transform(ex: Ref_Exception, level: number): any {
    if (ex) {
      if (level == 1) {
        if (ex.Level_1_Hyper_Code) {
          if (ex.Exception_Details && ex.Exception_Details.Level_1_Value) {
            return ex.Level_1_Hyper_Code + ' | ' + 'Yes';
          }
          return ex.Level_1_Hyper_Code + ' | ' + 'No';
        }
        return '';
      }

      if (ex.Level_2_Hyper_Code) {
        if (ex.Exception_Details && ex.Exception_Details.Level_2_Value) {
          return ex.Level_2_Hyper_Code + ' | ' + 'Yes';
        }
        return ex.Level_2_Hyper_Code + ' | ' + 'No';
      }
    }
    return '';
  }
}
