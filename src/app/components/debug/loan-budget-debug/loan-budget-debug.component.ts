import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';

import { loan_model } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';

import * as _ from 'lodash';

@Component({
  selector: 'app-loan-budget-debug',
  templateUrl: './loan-budget-debug.component.html',
  styleUrls: ['./loan-budget-debug.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoanBudgetDebugComponent implements OnInit {
  localloanobject: loan_model;

  constructor(private localStorage: LocalStorageService) {}

  ngOnInit() {
    this.localloanobject = this.localStorage.retrieve(environment.loankey);
  }

  get Budgets() {
    return _.sortBy(
      this.localloanobject.LoanBudget.filter(a => !!a.ActionStatus),
      b => b.Crop_Practice_ID
    );
  }
}
