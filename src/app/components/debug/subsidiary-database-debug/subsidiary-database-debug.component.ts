import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubsidiaryDatabaseDebugService } from './subsidiary-database-debug.service';
import { ColumnApi, GridApi } from 'ag-grid';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';
import { Page } from '@lenda/models/page.enum';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-subsidiary-database-debug',
  templateUrl: './subsidiary-database-debug.component.html',
  styleUrls: ['./subsidiary-database-debug.component.scss'],
  providers: [SubsidiaryDatabaseDebugService]
})
export class SubsidiaryDatabaseDebugComponent implements OnInit, OnDestroy {
  database: any;
  private gridApi: GridApi;
  private gridColumnApi: ColumnApi;

  public columnDefs = [];
  public defaultColDef;
  public getRowNodeId;
  public rowData: [];
  public rowGroupPanelShow = 'always';
  public sideBar = 'filters';

  private getSubsidiaryDatabaseSub: ISubscription;

  constructor(
    private subsidiaryDatabaseDebugService: SubsidiaryDatabaseDebugService,
    private localstorageservice: LocalStorageService
  ) {
    this.localstorageservice.store(environment.currentpage, Page.debug);
  }

  ngOnInit() {
    this.getSubsidiaryDatabaseSub = this.subsidiaryDatabaseDebugService
      .getSubsidiaryDatabase()
      .subscribe(resp => {
        // Create column definitions based on data
        this.initColumnDefs(resp.Data);
        this.database = resp.Data;
      });

    this.defaultColDef = {
      enableRowGroup: true
    };
  }

  ngOnDestroy() {
    if (this.getSubsidiaryDatabaseSub) {
      this.getSubsidiaryDatabaseSub.unsubscribe();
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  initColumnDefs(data) {
    let item = data[0];
    for (let key in item) {
      if (item.hasOwnProperty(key)) {
        this.columnDefs.push({
          headerName: key,
          field: key
        });
      }
    }
    this.gridApi.setColumnDefs(this.columnDefs);
  }
}
