import { Injectable } from '@angular/core';
import { ApiService } from '@lenda/services';
import { environment } from '@env/environment';

@Injectable()
export class SubsidiaryDatabaseDebugService {
  constructor(private apiService: ApiService) {}

  /**
   * Gets subsidiary database
   */
  public getSubsidiaryDatabase() {
    return this.apiService.get(
      environment.apiEndpoints.userEndpoints.subsidiaryDatabase
    );
  }
}
