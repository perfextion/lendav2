import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NortridgeFunctionsComponent } from './nortridge-functions.component';

describe('NortridgeFunctionsComponent', () => {
  let component: NortridgeFunctionsComponent;
  let fixture: ComponentFixture<NortridgeFunctionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NortridgeFunctionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NortridgeFunctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
