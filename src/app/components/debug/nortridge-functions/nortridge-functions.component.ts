import { ResponseModel } from '@lenda/models/resmodels/reponse.model';
import { AlertifyService } from './../../../alertify/alertify.service';
import { Component, OnInit } from '@angular/core';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { ToasterService } from '@lenda/services/toaster.service';
import { NgxXml2jsonService } from 'ngx-xml2json';

@Component({
  selector: 'app-nortridge-functions',
  templateUrl: './nortridge-functions.component.html',
  styleUrls: ['./nortridge-functions.component.scss']
})
export class NortridgeFunctionsComponent implements OnInit {

  public _loanid: string = "";
  public xmltext={};
  public xmltext2={};
  constructor(public loanapiservice: LoanApiService,
    public alertservice: ToasterService, private ngxXml2jsonService: NgxXml2jsonService) { }

  ngOnInit() {
  }

  public nortridgesync_full() {
    this.loanapiservice.SyncLoantoNortridge_full(this._loanid).subscribe((result: ResponseModel) => {
       if (result.ResCode == 1) {

         let str = "";
         let str2 = "";
         if (typeof result.Data == "object") {
           str = (result.Data as string[])[0];
           try{
           str2 = (result.Data as string[])[1];
           }
           catch{

           }
         } else {
         str = result.Data;
         }
         const parser = new DOMParser();
         const xml = parser.parseFromString(str, 'text/xml');
         this.xmltext = this.ngxXml2jsonService.xmlToJson(xml);
         const xml2 = parser.parseFromString(str2, 'text/xml');
         this.xmltext2 = this.ngxXml2jsonService.xmlToJson(xml2);
        this.alertservice.success("Process Done Successfully");
       } else {
        this.alertservice.error("Process geneated fault..Please check Logs");
       }
    });
  }


  public nortridgesync_CreditLines() {
    this.loanapiservice.SyncLoantoNortridge_Creditlines(this._loanid).subscribe((result: ResponseModel) => {
      if (result.ResCode == 1) {

        let str = "";
        let str2 = "";
        if (typeof result.Data == "object") {
          str = (result.Data as string[])[0];
          try{
          str2 = (result.Data as string[])[1];
          }
          catch{

          }
        } else {
        str = result.Data;
        }
        const parser = new DOMParser();
        const xml = parser.parseFromString(str, 'text/xml');
        this.xmltext = this.ngxXml2jsonService.xmlToJson(xml);
        const xml2 = parser.parseFromString(str2, 'text/xml');
        this.xmltext2 = this.ngxXml2jsonService.xmlToJson(xml2);
       this.alertservice.success("Process Done Successfully");
      } else {
       this.alertservice.error("Process geneated fault..Please check Logs");
      }
    });
  }


  public nortridgesync_Balances() {
    this.loanapiservice.SyncLoantoNortridge_Balances(this._loanid).subscribe((result: ResponseModel) => {
      if (result.ResCode == 1) {

        let str = "";
        let str2 = "";
        if (typeof result.Data == "object") {
          str = (result.Data as string[])[0];
          try{
          str2 = (result.Data as string[])[1];
          }
          catch{

          }
        } else {
        str = result.Data;
        }
        const parser = new DOMParser();
        const xml = parser.parseFromString(str, 'text/xml');
        this.xmltext = this.ngxXml2jsonService.xmlToJson(xml);
        const xml2 = parser.parseFromString(str2, 'text/xml');
        this.xmltext2 = this.ngxXml2jsonService.xmlToJson(xml2);
       this.alertservice.success("Process Done Successfully");
      } else {
       this.alertservice.error("Process geneated fault..Please check Logs");
      }
    });
  }
}
