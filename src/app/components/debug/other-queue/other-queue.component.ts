import { Component, OnInit, Inject } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import {
  loan_model,
  Risk_Other_Queue,
  AssociationCodeNameMapping
} from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import { DataService } from '@lenda/services/data.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { ToasterService } from '@lenda/services/toaster.service';
import { JsonConvert } from 'json2typescript';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';

@Component({
  selector: 'app-other-queue',
  templateUrl: './other-queue.component.html',
  styleUrls: ['./other-queue.component.scss']
})
export class OtherQueueComponent implements OnInit {
  AssociationCodeNameMapping = AssociationCodeNameMapping;
  RiskOtherDatabase: typeof RiskOtherDatabase = RiskOtherDatabase;
  localLoanObject: loan_model;

  associations: Array<{[key: string]: string}>;
  selectedKey: string = '';

  constructor(
    private localStorageService: LocalStorageService,
    private dataService: DataService,
    public dialog: MatDialog,
    private loanApi: LoanApiService,
    private logging: LoggingService,
    private toasterService: ToasterService,
    private loanCalculationService: LoancalculationWorker
  ) {}

  ngOnInit() {
    this.dataService.getLoanObject().subscribe(res => {
      if (res) {
        this.localLoanObject = res;
      }
    });

    this.localLoanObject = this.localStorageService.retrieve(
      environment.loankey
    );

    this.setAssociations();
  }

  setAssociations() {
    this.associations = [
      {
        key: 'AGY',
        value: 'Agency'
      },
      {
        key: 'AIP',
        value: 'AIP'
      },
      {
        key: 'BUY',
        value: 'Buyer'
      },
      {
        key: 'DIS',
        value: 'Distributer'
      }
    ];
  }

  onEditQueue(queue: Risk_Other_Queue) {
    const dialogRef = this.dialog.open(OtherQueueEditComponent, {
      data: queue
    });

    dialogRef.afterClosed().subscribe((queue: Risk_Other_Queue) => {
      if (queue) {
        this.updateQueueData(queue);
      }
    });
  }

  onApproveQueue(queue: Risk_Other_Queue) {
    queue.Response_Ind = ResponseInd.Approve;
    this.updateQueueData(queue);
  }

  updateQueueData(queue: Risk_Other_Queue) {
    this.loanApi.approveOrRejectQueue(queue).subscribe(
      res => {
        if (res.ResCode == 1) {
          this.loanApi
            .getLoanById(this.localLoanObject.Loan_Full_ID)
            .subscribe(res => {
              this.logging.checkandcreatelog(
                3,
                'Overview',
                'APi LOAN GET with Response ' + res.ResCode
              );
              if (res.ResCode == 1) {
                this.toasterService.success('Records Synced');

                let jsonConvert: JsonConvert = new JsonConvert();
                this.loanCalculationService.performcalculationonloanobject(
                  jsonConvert.deserialize(res.Data, loan_model)
                );
              } else {
                this.toasterService.error(
                  'Could not fetch Loan Object from API'
                );
              }
            });
        } else {
          this.toasterService.error('Error in Sync');
        }
      },
      error => {}
    );
  }
}

@Component({
  selector: 'other-queue-edit-dialog',
  templateUrl: 'other-queue-edit.component.html',
  styles: [`
    .roq-input mat-radio-button {
      margin-right: 25px;
    }
  `]
})
export class OtherQueueEditComponent implements OnInit {
  associations = [];
  expenses = [];
  refData: any;
  ResponseInd: typeof ResponseInd = ResponseInd;
  RiskOtherDatabase: typeof RiskOtherDatabase = RiskOtherDatabase;

  model = {
    Response_Ind: null,
    Other_Text: '',
    Suggested_Ref_ID: null,
    Ref_Database_ID: null
  };

  constructor(
    public dialogRef: MatDialogRef<OtherQueueEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Risk_Other_Queue,
    private localStorageService: LocalStorageService
  ) {}

  ngOnInit() {
    this.refData = this.localStorageService.retrieve(
      environment.referencedatakey
    );

    if (this.data.Ref_Database_ID == RiskOtherDatabase.LoanAssociation) {
      this.associations = this.refData.RefAssociations.filter(
        assoc => assoc.Assoc_Type_Code == this.data.Field_ID
      );
    } else if (this.data.Ref_Database_ID == RiskOtherDatabase.LoanBudget) {
      this.expenses = this.refData.BudgetExpenseType;
    }
    this.model.Ref_Database_ID = this.data.Ref_Database_ID;
    this.model.Response_Ind = ResponseInd.ModifyAndApprove; // by default selecting modify
    this.model.Other_Text = this.data.Other_Text;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  approve() {
    if (this.model.Response_Ind == ResponseInd.ModifyAndApprove) {
      this.data.Other_Text = this.model.Other_Text;
      this.data.Response_Ind = this.model.Response_Ind;
    } else if (this.model.Response_Ind == ResponseInd.SuggestAndApprove) {
      this.data.Suggested_Ref_ID = this.model.Suggested_Ref_ID;
      this.data.Response_Ind = this.model.Response_Ind;
    } else if (this.model.Response_Ind == ResponseInd.SuggestOtherAndApprove) {
      //SuggestOtherAndApprove and SuggestAndApprove just we are not asking admin which to choose
      let otherAssoc = this.refData.RefAssociations.find(
        assoc => assoc.Assoc_Type_Code == this.data.Field_ID && assoc.IsOther
      );
      this.data.Suggested_Ref_ID = otherAssoc.Ref_Association_ID;
      this.data.Response_Ind = ResponseInd.SuggestAndApprove;
    }
    this.dialogRef.close(this.data);
    //call API
  }
}

enum ResponseInd {
  NoAction = 0,
  Approve = 1,
  ModifyAndApprove = 2,
  SuggestAndApprove = 3,
  SuggestOtherAndApprove = 4
}

enum RiskOtherDatabase {
  LoanAssociation = 1,
  LoanBudget = 2
}
