import { Component, OnInit, ViewEncapsulation, ViewChild, OnDestroy } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { environment } from '@env/environment.prod';
import { MatSelectionList } from '@angular/material';
import { LoanvalidatorService } from '@lenda/services/loanvalidator.service';
import { JsonConvert } from 'json2typescript';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { loan_model } from '@lenda/models/loanmodel';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '@lenda/services/data.service';
import { Page } from '@lenda/models/page.enum';
import { ISubscription } from 'rxjs/Subscription';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-loan-sync-debug',
  templateUrl: './loan-sync-debug.component.html',
  styleUrls: ['./loan-sync-debug.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoanSyncDebugComponent implements OnInit, OnDestroy {
  loans: Array<any>;

  @ViewChild(MatSelectionList) selectionList: MatSelectionList;

  query: string;

  private loanSyncSub: ISubscription;
  private getDataSub: ISubscription;

  _current_index = 0;
  private _total_loans = 0;
  getLoanListForSyncSub: ISubscription;
  getLoanByIdSub: any;

  constructor(
    private localstorageservice: LocalStorageService,
    private loanservice: LoanApiService,
    private loanvalidator: LoanvalidatorService,
    private loancalculationservice: LoancalculationWorker,
    private toaster: ToastrService,
    private dataservice: DataService
  ) {
    this.query = "Select Loan_Full_ID, Legacy_Loan_ID, Crop_Year, Convert(varchar, Last_Sync_Date, 111) as [Last_Sync_Date] From Loan_Master Where Crop_Year = 2018";
    this.LoanLoans();
    this.localstorageservice.store(environment.currentpage, Page.debug);
  }

  ngOnInit() {
    this.getDataSub = this.dataservice.getLoanObject().subscribe(res => {
      if(document.location.href.includes('loan-sync-debug')) {
        this.syncToDb(res);
      }
    });
  }

  LoanLoans() {
    this.getLoanListForSyncSub = this.loanservice.getLoanListForSync(this.query).subscribe(res => {
      if(res.ResCode == 1) {
        this.loans = res.Data;
        this._current_index = 0;
        this.loanservice.Log_Out({}).subscribe(res => {});
      } else {
        this.toaster.error(res.Message);
        this.loans = [];
      }
    }, error => {
      this.toaster.error('Some Error Occured');
      this.loans = [];
    });
  }

  selectAll($event) {
    if($event.checked) {
      if(this.selectionList) {
        this.selectionList.selectAll();
      }
    } else {
      if(this.selectionList) {
        this.selectionList.deselectAll();
      }
    }
  }


  private _selectedLoans: any[];

  performSync() {
    this._current_index = 0;
    this._selectedLoans = this.selectionList.selectedOptions.selected.map(a => a.value);
    this._total_loans = this._selectedLoans.length;

    this.syncLoan(this._current_index);
  }

  syncLoan(index: number) {
    try {
      if(index <= this._total_loans - 1) {
        let loan = this._selectedLoans[index];
        if(loan) {
          this.getLoanBasicDetails(loan.Loan_Full_ID);
        }
      }
    } catch {
      this.syncNextLoan();
    }
  }

  syncNextLoan() {
    this._current_index = this._current_index + 1;
    this.syncLoan(this._current_index);
  }

   getLoanBasicDetails(Loan_Full_ID: string) {
    if (Loan_Full_ID) {
      this.getLoanByIdSub = this.loanservice.getLoanById(Loan_Full_ID).subscribe(res => {
        if (res.ResCode == 1) {
          this.loanvalidator.validateloanobject(res.Data).then(errors => {
            if (errors && errors.length > 0) {
            } else {
              this.localstorageservice.store(environment.errorbase, []);
              this.loancalculationservice.saveValidationsToLocalStorage(res.Data);
              this.getLoanGroups(res.Data, Loan_Full_ID);
            }
            this.localstorageservice.store(environment.exceptionStorageKey, []);
            this.localstorageservice.store(environment.modifiedbase, []);
            this.localstorageservice.store(environment.modifiedoptimizerdata, null);
          });
        }
      });
    } else {
      this.toaster.error("Something went wrong");
    }
  }

  getLoanGroups(localloanobject: loan_model, Loan_Full_ID: string) {
    this.loanservice.getLoanGroups(Loan_Full_ID).subscribe(res => {
      if(res.ResCode == 1) {
        this.localstorageservice.store(environment.loanGroup, res.Data);
      }
      let jsonConvert: JsonConvert = new JsonConvert();
      this.loancalculationservice.performcalculationonloanobject(jsonConvert.deserialize(localloanobject, loan_model), true, true);
    });
  }

  syncToDb(localloanobject) {
    localloanobject = this.loancalculationservice.setValidationErrorsBeforeSave(localloanobject);
    localloanobject=this.loancalculationservice.WaiveofftempdisburseErrors(localloanobject);

    this.loanSyncSub = this.loanservice.syncloanobject(localloanobject)
    .pipe(
      finalize(() => {
        this.syncNextLoan();
      })
    )
    .subscribe(respone => {
      if (respone.ResCode == 1) {
        let loan = this.loans.find(a => a.Loan_Full_ID === localloanobject.Loan_Full_ID);
        if(loan) {
          loan.Sync_Done = true;
        }
      } else {
        this.toaster.error(respone.Message || 'Error in Sync');
      }
    }, error => {
      this.toaster.error('Error in Sync - ' +  localloanobject.Loan_Full_ID);
    });
  }

  ngOnDestroy() {
    if(this.loanSyncSub) {
      this.loanSyncSub.unsubscribe();
    }

    if(this.getDataSub) {
      this.getDataSub.unsubscribe();
    }

    if(this.getLoanListForSyncSub) {
      this.getLoanListForSyncSub.unsubscribe();
    }

    if(this.getLoanByIdSub) {
      this.getLoanByIdSub.unsubscribe();
    }
  }
}
