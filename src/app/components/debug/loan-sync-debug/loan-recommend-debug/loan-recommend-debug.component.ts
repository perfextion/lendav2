import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { loan_model, LoanStatus } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { Loan_Exception } from '@lenda/models/loan/loan_exceptions';
import { errormodel, validationlevel } from '@lenda/models/commonmodels';
import * as _ from 'lodash';
import { MembersService } from '@lenda/components/committee/members/members.service';
import { forkJoin } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Loan_Document } from '@lenda/models/loan/loan_document';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-loan-recommend-debug',
  templateUrl: './loan-recommend-debug.component.html',
  styleUrls: ['./loan-recommend-debug.component.scss']
})
export class LoanRecommendDebugComponent implements OnInit {
  public currentSelectedLoan: loan_model;
  public refdata: RefDataModel;

  exceptions: Array<Loan_Exception>;
  validations: Array<errormodel>;

  loanstatus: typeof LoanStatus = LoanStatus;

  remainingUnderwritingDocs: Array<Loan_Document> = [];

  constructor(
    private localstorage: LocalStorageService,
    private loanAPI: LoanApiService,
    private memberService: MembersService,
    private toastr: ToastrService,
    private dataService: DataService
  ) {
    this.currentSelectedLoan = this.localstorage.retrieve(environment.loankey);
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);
  }

  ngOnInit() {
    this.exceptions = this.currentSelectedLoan.Loan_Exceptions.filter(a => !a.Complete_Ind && a.ActionStatus != 3);
    let errors = this.localstorage.retrieve(environment.errorbase) as Array<errormodel>;

    this.validations = errors.filter(a => a.level == validationlevel.level2);
    this.validations = _.uniqBy(this.validations, v => v.Validation_ID_Text);
    this.setRemainingDocs();
  }

  setRemainingDocs() {
    this.remainingUnderwritingDocs = this.currentSelectedLoan.Loan_Documents.filter(a => a.Document_Type_Level == 1 && !a.Upload_Date_Time && a.ActionStatus != 3);
  }

  uploadUnderwritingDocs() {
    let date = new Date().toUTCString();
    this.remainingUnderwritingDocs.forEach(d => {
      let doc = this.currentSelectedLoan.Loan_Documents.find(a => a.Loan_Document_ID == d.Loan_Document_ID);
      if(doc) {
        if(doc.ActionStatus != 1) {
          doc.ActionStatus = 2;
        }
        doc.Request_Date_Time = date;
        doc.Request_User_ID = 7;
        doc.Upload_User_ID = 7;
        doc.Upload_Date_Time = date;
        doc.Document_URL_Executed = 'https://armlend365.sharepoint.com/sites/loansdev/PreClosing/2227/Subordination%20Agreement.docx';
      }
    });

    this.loanAPI.UploadDocuments(this.currentSelectedLoan).subscribe(loanRes => {
      if(loanRes.ResCode == 1) {
        this.currentSelectedLoan.Loan_Documents = loanRes.Data;
        this.setRemainingDocs();
        this.dataService.setLoanObjectwithoutsubscription(this.currentSelectedLoan);
        this.toastr.success('Uploaded document successfully.');
      } else {
        this.toastr.error('Upload Document Failed');
      }
    }, _ => {
      this.toastr.error('Upload Document Failed');
    });
  }

  performExceptions() {
    this.exceptions.forEach(a => {
      if(!a.Mitigation_Text) {
        a.Mitigation_Text = 'Test Mitigation Text for Recommend';
      }

      a.ActionStatus = 2;
    });
  }

  performRecommend() {

  }

  performDecline() {

  }
}
