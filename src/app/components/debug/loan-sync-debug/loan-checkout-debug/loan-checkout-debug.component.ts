import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { MembersService } from '@lenda/components/committee/members/members.service';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '@lenda/services/data.service';
import { Loan_Document } from '@lenda/models/loan/loan_document';
import { environment } from '@env/environment.prod';
import { loan_model, LoanStatus, Loan_Committee } from '@lenda/models/loanmodel';
import { RefDataModel, LoanMaster, RefCommitteeLevel } from '@lenda/models/ref-data-model';
import { Loansettings } from '@lenda/models/loansettings';
import { CommitteeStatusMock } from '@lenda/models/committee-status/mock-committee-status';
import { forkJoin } from 'rxjs';
import { JsonConvert } from 'json2typescript';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';

@Component({
  selector: 'app-loan-checkout-debug',
  templateUrl: './loan-checkout-debug.component.html',
  styleUrls: ['./loan-checkout-debug.component.scss']
})
export class LoanCheckoutDebugComponent implements OnInit {
  public currentSelectedLoan: loan_model;
  public refdata: RefDataModel;

  remainingDocuments: Array<Loan_Document> = [];

  isVoteRequired: boolean;
  voteRequiredMembers: Array<Loan_Committee> = [];

  constructor(
    private localstorage: LocalStorageService,
    private loanAPI: LoanApiService,
    private memberService: MembersService,
    private toastr: ToastrService,
    private dataService: DataService,
    private loancalculationworker: LoancalculationWorker
  ) {
    this.currentSelectedLoan = this.localstorage.retrieve(environment.loankey);
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);
  }

  ngOnInit() {
    this.setRemainingDocs();
    this.setVoteStatus();
  }

  get isCheckoutEnabled() {
    if(this.currentSelectedLoan) {
      let status = this.currentSelectedLoan.LoanMaster.Loan_Status;
      return status == LoanStatus.Recommended || status == LoanStatus.Approved;
    }
    return false;
  }

  setVoteStatus() {
    let committeeLevel = this.getCommitteeLevel(this.currentSelectedLoan.LoanMaster);
    if(committeeLevel){
      let minimumApprovalPerc = committeeLevel.Approval / 100;

      let approvedVotePerc = this.getApprovalPerc();
      let declinedVotePerc = this.getDeclinePerc();

      if(approvedVotePerc >= minimumApprovalPerc) {
        this.isVoteRequired = false;
      } else if (declinedVotePerc > ( 1 - minimumApprovalPerc)) {
        this.isVoteRequired = false;
      } else {
        this.isVoteRequired = true;
        this.voteRequiredMembers = this.currentSelectedLoan.LoanCommittees.filter(a => a.ActionStatus != 3 && !a.Voted_Date && a.CM_Role == 1);
      }
    }
  }

  public recordVote() {
    try {
      let committeeStatus = CommitteeStatusMock[0];
      let innerCommitteeStatus = committeeStatus.innerStatus[0];

      const voteReq = this.voteRequiredMembers.filter(A => A.CM_Role == 1).map(a => {
        return this.memberService.voteLoan(
          this.currentSelectedLoan.Loan_Full_ID,
          a.User_ID,
          committeeStatus.id,
          innerCommitteeStatus.id,
          '',
          innerCommitteeStatus.title
        )
      });

      forkJoin(voteReq).subscribe(
        res => {
          if (res[res.length - 1].ResCode == 1) {
            this.toastr.success('Successfully Recorded Vote');
            this.getLatestLoan();
          } else {
            this.toastr.error('Error occured while recording voting');
          }
        },
        error => {
          this.toastr.error('Error occured while recording voting');
        }
      );
    } catch {
      this.toastr.error('Error occured while recording voting');
    }
  }

  private getLatestLoan() {
    this.loanAPI
      .getLoanById(this.currentSelectedLoan.Loan_Full_ID)
      .subscribe(res => {
        if (res.ResCode == 1) {
          this.toastr.success('Loan synced successfully');
          this.currentSelectedLoan = res.Data;
          this.voteRequiredMembers = this.currentSelectedLoan.LoanCommittees.filter(a => a.ActionStatus != 3 && !a.Voted_Date && a.CM_Role == 1);
          this.dataService.setLoanObjectwithoutsubscription(this.currentSelectedLoan);
        } else {
          this.toastr.error('Could not fetch latest Loan from API');
        }
      });
  }

  setRemainingDocs() {
    this.remainingDocuments = this.currentSelectedLoan.Loan_Documents.filter(a => a.ActionStatus != 3 && !a.Upload_Date_Time);
  }

  uploadRemainingDocs() {
    let date = new Date().toUTCString();
    this.remainingDocuments.forEach(d => {
      let doc = this.currentSelectedLoan.Loan_Documents.find(a => a.Loan_Document_ID == d.Loan_Document_ID);
      if(doc) {
        if(doc.ActionStatus != 1) {
          doc.ActionStatus = 2;
        }
        doc.Request_Date_Time = date;
        doc.Request_User_ID = 7;
        doc.Upload_User_ID = 7;
        doc.Upload_Date_Time = date;
        doc.Document_URL_Executed = 'https://armlend365.sharepoint.com/sites/loansdev/PreClosing/2227/Subordination%20Agreement.docx';
      }
    });

    this.loanAPI.UploadDocuments(this.currentSelectedLoan).subscribe(loanRes => {
      if(loanRes.ResCode == 1) {
        this.currentSelectedLoan.Loan_Documents = loanRes.Data;
        this.setRemainingDocs();
        this.dataService.setLoanObjectwithoutsubscription(this.currentSelectedLoan);
        this.toastr.success('Uploaded document successfully.');
      } else {
        this.toastr.error('Upload Document Failed');
      }
    }, _ => {
      this.toastr.error('Upload Document Failed');
    });
  }

  private getCommitteeLevel(master: LoanMaster) {
    let loanSettings: Loansettings;
    try {
      loanSettings = JSON.parse(master.Loan_Settings);
    }
    catch {
      loanSettings = new Loansettings();
    }

    try {
      let committeeLevel = this.refdata.CommitteeLevels.find(l => l.Level_Code == loanSettings.committeeLevel);
      return committeeLevel;
    } catch {
      return <RefCommitteeLevel>{};
    }
  }

  private getApprovalPerc() {
    let committee = this.currentSelectedLoan.LoanCommittees.filter(a => a.CM_Role == 1);
    let approvedVote = committee.filter(a => !!a.Voted_Date && a.Vote == 1);
    let approvedVotePerc = approvedVote.length / committee.length;
    return approvedVotePerc;
  }

  private getDeclinePerc(newVote = 0) {
    let committee = this.currentSelectedLoan.LoanCommittees.filter(a => a.CM_Role == 1);
    let declinedVote = committee.filter(a => a.CM_Role == 1 && !!a.Voted_Date && a.Vote == 2);
    let declinedVotePerc = (declinedVote.length + newVote) / committee.length;
    return declinedVotePerc;
  }

}
