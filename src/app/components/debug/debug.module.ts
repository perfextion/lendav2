import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { AgGridModule } from 'ag-grid-angular';

import { DebugComponent } from './debug.component';
import { DocumentsComponent } from './documents/documents.component';
import { ExceptionComponent } from './exception/exception.component';
import { ExceptionDebugComponent } from './exception-debug/exception-debug.component';
import { ExceptionHyperocdePipe } from './exception-debug/hypercode.pipe';
import { LoanDocumentsDebugComponent } from './loan-documents-debug/loan-documents-debug.component';
import { LoanExceptionDebugComponent } from './loan-exception-debug/loan-exception-debug.component';
import { LoanAssociationDebugComponent } from './loan-association-debug/loan-association-debug.component';
import { LoanBudgetDebugComponent } from './loan-budget-debug/loan-budget-debug.component';
import { NortridgeUploadComponent } from './nortridge-upload/nortridge-upload.component';
import { LoanSyncDebugComponent } from './loan-sync-debug/loan-sync-debug.component';
import { LoanRecommendDebugComponent } from './loan-sync-debug/loan-recommend-debug/loan-recommend-debug.component';
import { LoanCheckoutDebugComponent } from './loan-sync-debug/loan-checkout-debug/loan-checkout-debug.component';
import { LoanAuditTrailDebugComponent } from './log-debug/loan-audit-trail-debug/loan-audit-trail-debug.component';
import { LogDebugComponent } from './log-debug/log-debug.component';
import { LoanLogsDebugComponent } from './log-debug/loan-logs-debug/loan-logs-debug.component';
import { WorkInProgressModule } from './work-in-progress/work-in-progress.module';
import { CustomentryComponent } from './customentry/customentry.component';
import { ExceptionPlaygroundComponent } from './customentry/exception-playground/exception-playground.component';
import { LocalStorageDebugComponent } from './local-storage-debug/local-storage-debug.component';
import { ExceptionsDebugInfoComponent } from './local-storage-debug/exceptions-debug-info/exceptions-debug-info.component';
import { PageExceptionsComponent } from './local-storage-debug/exceptions-debug-info/page-exceptions/page-exceptions.component';
import {
  OtherQueueComponent,
  OtherQueueEditComponent
} from './other-queue/other-queue.component';
import { SubsidiaryDatabaseDebugComponent } from './subsidiary-database-debug/subsidiary-database-debug.component';
import { NortridgeFunctionsComponent } from './nortridge-functions/nortridge-functions.component';
import { LoanAdjustmentEditorComponent } from './loan-adjustment-editor/loan-adjustment-editor.component';
import { AceEditorModule } from 'ng2-ace-editor';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    UiComponentsModule,
    NgxJsonViewerModule,
    AgGridModule,
    AceEditorModule
  ],
  declarations: [
    DebugComponent,
    DocumentsComponent,
    ExceptionComponent,
    ExceptionDebugComponent,
    ExceptionHyperocdePipe,
    LoanDocumentsDebugComponent,
    LoanExceptionDebugComponent,
    LoanAssociationDebugComponent,
    LoanBudgetDebugComponent,
    NortridgeUploadComponent,
    LoanSyncDebugComponent,
    LoanRecommendDebugComponent,
    LoanCheckoutDebugComponent,
    LoanAuditTrailDebugComponent,
    LogDebugComponent,
    LoanLogsDebugComponent,
    CustomentryComponent,
    ExceptionPlaygroundComponent,
    LocalStorageDebugComponent,
    ExceptionsDebugInfoComponent,
    PageExceptionsComponent,
    OtherQueueComponent,
    OtherQueueEditComponent,
    SubsidiaryDatabaseDebugComponent,
    NortridgeFunctionsComponent,
    LoanAdjustmentEditorComponent
  ],
  entryComponents: [OtherQueueEditComponent],
  exports: [
    DebugComponent,
    DocumentsComponent,
    ExceptionComponent,
    ExceptionDebugComponent,
    LoanSyncDebugComponent,
    LoanRecommendDebugComponent,
    LoanCheckoutDebugComponent,
    LogDebugComponent,
    WorkInProgressModule,
    CustomentryComponent,
    LocalStorageDebugComponent,
    SubsidiaryDatabaseDebugComponent
  ]
})
export class DebugModule {}
