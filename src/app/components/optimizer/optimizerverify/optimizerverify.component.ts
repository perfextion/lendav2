import { Component, OnInit, Inject, NgZone, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DisbursepopupComponent } from '@lenda/aggridcolumns/disbursepopup/disbursepopup.component';
import { loan_model } from '@lenda/models/loanmodel';
import { AlertifyService } from '@lenda/alertify/alertify.service';

@Component({
  selector: 'app-optimizerverify',
  templateUrl: './optimizerverify.component.html',
  styleUrls: ['./optimizerverify.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OptimizerverifyComponent implements OnInit {

  private loandata: loan_model;
  public errorText = "";
  constructor(public dialogRef: MatDialogRef<OptimizerverifyComponent>,
    public alerts: AlertifyService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private ngZone: NgZone) {
    this.loandata = data;
  }

  public _documents: Array<any> = new Array<any>();
  // tslint:disable-next-line: no-use-before-declare
  public popmodel: verifymodel_OPT = new verifymodel_OPT();
  ngOnInit() {
    this.popmodel.Verifytype = 2;
    this.populateDocs(2);
    try {
    let objAIP = JSON.parse(this.loandata.LoanMaster.Verf_AIP_Data) as Loan_Verification_Document_Details;
    if (objAIP != null && objAIP.DocID != null) {
      this.popmodel.DocID = objAIP.DocID;
    }
  } catch {

  }
  }
  onNoClick() {
    this.dialogRef.close();
  }
  onyesClick() {

    this.dialogRef.close(this.popmodel);
  }
  radioButtonSelected($event) {
    this.populateDocs($event.value);
    switch (this.popmodel.Verifytype) {
      case 2:
        try {
          let objAIP = JSON.parse(this.loandata.LoanMaster.Verf_AIP_Data) as Loan_Verification_Document_Details;
          if (objAIP != null && objAIP.DocID != null) {
            this.popmodel.DocID = objAIP.DocID;
          }
        } catch {
          this.popmodel.DocID = null;
        }
        break;
      case 1:
        try {
          let objFSA = JSON.parse(this.loandata.LoanMaster.Verf_FSA_Data) as Loan_Verification_Document_Details;
          if (objFSA != null && objFSA.DocID != null) {
            this.popmodel.DocID = objFSA.DocID;
          }
        } catch {
          this.popmodel.DocID = null;
        }
        break;
      default:
        break;
    }
  }


  populateDocs(type) {
    switch (type) {
      case 2: //AIP
        this._documents = this.loandata.Loan_Documents.filter(p => p.Document_Type_ID == 74).map((p) => {
          return { id: p.Loan_Document_ID, text: p.Document_Name };
        });
        break;
      case 1: //FSA
        this._documents = this.loandata.Loan_Documents.filter(p => p.Document_Type_ID == 76).map((p) => {
          return { id: p.Loan_Document_ID, text: p.Document_Name };
        });
        break;
      default:
        break;
    }
    if (this._documents.length == 0) {
      //this.alerts.alert("Warning", "No Documents !! Please Upload to Continue Verification")
      this.errorText = "Please upload Verify source documents";
    }
  }
}


// tslint:disable-next-line: class-name
export class verifymodel_OPT {
  public Verifytype: any;
  public DocID: string;
  public DocDate: string;
}

// tslint:disable-next-line: class-name
export class Loan_Verification_Document_Details {
  public DocID: string;
  public DocDate: string;
}
