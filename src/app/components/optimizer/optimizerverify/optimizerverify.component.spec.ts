import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptimizerverifyComponent } from './optimizerverify.component';

describe('OptimizerverifyComponent', () => {
  let component: OptimizerverifyComponent;
  let fixture: ComponentFixture<OptimizerverifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptimizerverifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptimizerverifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
