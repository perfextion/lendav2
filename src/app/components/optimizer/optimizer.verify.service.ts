import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { LoanApiService } from '../../services/loan/loanapi.service';
import { VerificationStages, TableId } from '@lenda/models/commonmodels';
import { environment } from '@env/environment';
import { OptimizerverifyComponent, Loan_Verification_Document_Details } from './optimizerverify/optimizerverify.component';
import { CropapiService } from '@lenda/services/crop/cropapi.service';
import { MatDialog } from '@angular/material';
import { InsuranceapiService } from '@lenda/services/insurance/insuranceapi.service';
import { DataService } from '@lenda/services/data.service';
import { loan_model } from '@lenda/models/loanmodel';

declare function setverificationerroralloptimizer(array, keyword): any;

@Injectable()
export class OptimizerVerifyService {

  constructor( private localstorage: LocalStorageService,
    public loanapi: LoanApiService,
    public apiservice: CropapiService,
    public dialog: MatDialog,
    private insapiservice: InsuranceapiService, ) { }

    //Other TEMP VALUES
    public loanmodel: loan_model;
    private gridApiNIR;
    private columnApiNIR;
    private gridApiIIR;
    private columnApiIIR;
    public currentopenedAcresVeriftTab: string;
    public currentopenedAcresVeriftTabNIR: string;

    //Verify Stages indiacators
    public verifyButtonStageIIR = VerificationStages.Verify;
    public verifyButtonStageNIR = VerificationStages.Verify;
    public verifyButtonStageIIRAPH = VerificationStages.Verify;
    public verifyButtonStageNIRAPH = VerificationStages.Verify;



    //VErification Error Bases
    public verificationdataIIRAPH = [];
    public verificationdataNIRAPH = [];
    public verificationdataIIR = [];
    public verificationdataNIR = [];

    public currentIRRVerification: number;
    public currentNIRVerification: number;

    public parent: any;

    //MISC
    public currentmodeldata: any;
    public dataservice: DataService;
   //CUSTOM CONNSTTRUCTOR

    MyConstructor(gridApiIIR: any, columnApiIIR: any, columnApiNIR: any, gridApiNIR: any, dataservice: DataService, comp) {

        this.gridApiIIR = gridApiIIR;
        this.columnApiIIR = columnApiIIR;
        this.columnApiNIR = columnApiNIR;
        this.gridApiNIR = gridApiNIR;
        this.dataservice = dataservice;
        this.parent = comp;
    }


    verifyIIRClicked(rowdata: any, dialog: any, loanmodel: any) {

          if (this.verifyButtonStageIIR == VerificationStages.Verify) {

            const dialogRef = dialog.open(OptimizerverifyComponent, {
              width: '600px',
              data: loanmodel
            });
            dialogRef.afterClosed().subscribe(rs => {

              if (rs != undefined) {
                this.currentmodeldata = rs;
              this.currentopenedAcresVeriftTab = rs.Verifytype;
                //Check and Fill the Docid and DoscDate
                if (rs.DocID != undefined || rs.DocDate != undefined) {
                  //means dta has been fileld
                  //now lets convert it into Json
                  let objtosave = new Loan_Verification_Document_Details();
                  objtosave.DocDate = rs.DocDate;
                  objtosave.DocID = rs.DocID;

                  if (rs.Verifytype == 2) {
                    loanmodel.LoanMaster.Verf_AIP_Data = JSON.stringify(objtosave);
                  } else {
                    loanmodel.LoanMaster.Verf_FSA_Data = JSON.stringify(objtosave);
                  }
                }
                let result = rs.Verifytype;
             let columns: any;
             this.currentIRRVerification = result;



          if (result == 1) {
            columns = this.columnApiIIR.getAllColumns().filter(p => p.colId.includes("Verf_FSA"));
          } else
          if (result == 2) {
            columns = this.columnApiIIR.getAllColumns().filter(p => p.colId.includes("Verf_AIP"));
          }

          columns.forEach(element => {
            this.columnApiIIR.setColumnVisible(element.colId, true);
          });

          this.verifyButtonStageIIR = VerificationStages.Check;
          // this.
          this.gridApiIIR.redrawRows();

          //this.setModifiedAPHTable();
          this.setVerificationError();
        }
        });
        } else
        if (this.verifyButtonStageIIR == VerificationStages.Check) {
          if (this.verifyColumnvaluesIIR(rowdata, loanmodel)) {

          this.verifyButtonStageIIR = VerificationStages.Check2;
          }
        } else {
          if (this.verifyColumnvaluesIIR(rowdata, loanmodel)) {
            this.verifyButtonStageIIR = VerificationStages.Check2;
          }
        }

        this.gridApiIIR.redrawRows();
        ;
        //this.setModifiedAPHTable();

        setTimeout(() => {
        this.setVerificationError();
        }, 500);






    }
    verifyNIRClicked(rowdata: any, dialog: any, loanmodel: any) {

           if (this.verifyButtonStageNIR == VerificationStages.Verify) {

             const dialogRef = dialog.open(OptimizerverifyComponent, {
               width: '600px',
               data: loanmodel
             });
             dialogRef.afterClosed().subscribe(rs => {
               if (rs != undefined) {
                 this.currentmodeldata = rs;
               this.currentopenedAcresVeriftTabNIR = rs.Verifytype;
                 //Check and Fill the Docid and DoscDate
                 if (rs.DocID != undefined || rs.DocDate != undefined) {
                   //means dta has been fileld
                   //now lets convert it into Json
                   let objtosave = new Loan_Verification_Document_Details();
                   objtosave.DocDate = rs.DocDate;
                   objtosave.DocID = rs.DocID;

                   if (rs.Verifytype == 2) {
                     loanmodel.LoanMaster.Verf_AIP_Data = JSON.stringify(objtosave);
                   } else {
                     loanmodel.LoanMaster.Verf_FSA_Data = JSON.stringify(objtosave);
                   }
                 }
                 let result = rs.Verifytype;
              let columns: any;
              this.currentNIRVerification = result;



           if (result == 1) {
             columns = this.columnApiNIR.getAllColumns().filter(p => p.colId.includes("Verf_FSA"));
           } else
           if (result == 2) {
             columns = this.columnApiNIR.getAllColumns().filter(p => p.colId.includes("Verf_AIP"));
           }

           columns.forEach(element => {
             this.columnApiNIR.setColumnVisible(element.colId, true);
           });

           this.verifyButtonStageNIR = VerificationStages.Check;
           this.gridApiNIR.redrawRows(); ;
           this.gridApiNIR.refreshHeader();
           //this.setModifiedAPHTable();
           this.setVerificationError();
         }
         });
         } else
         if (this.verifyButtonStageNIR == VerificationStages.Check) {
           if (this.verifyColumnvaluesNIR(rowdata, loanmodel)) {

           this.verifyButtonStageNIR = VerificationStages.Check2;
           }
         } else {
           if (this.verifyColumnvaluesNIR(rowdata, loanmodel)) {
             this.verifyButtonStageNIR = VerificationStages.Check2;
           }
         }

         this.gridApiNIR.redrawRows(); ;
         this.gridApiNIR.refreshHeader();
         //this.setModifiedAPHTable();
         this.setVerificationError();
     }
    verifyColumnvaluesIIR(rowDataIIR: any, loanmodel: any) {

      this.verificationdataIIR = [];
      let haserror = false;
      if (this.currentIRRVerification == 1) {
      rowDataIIR.filter(p => p.Verf_FSA_Acres_touched = true && p.Verf_FSA_Acres != null).forEach(element => {
       if (element.Acres != element.Verf_FSA_Acres) {
         haserror = true;
         this.verificationdataIIR.push(TableId.OPT + "IRR" + "_" + element.ID + "_" + "Verf_FSA_Acres");
       } else {
         element.Verf_Acres_Status = true;
         loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == element.ID).Verf_FSA_Acres_Status = true;
         this.updateAcresVerification(element.ID, true);
       }
     });
    } else {
      rowDataIIR.filter(p => p.Verf_AIP_Acres_touched = true && p.Verf_AIP_Acres != null).forEach(element => {
        if (element.Acres != element.Verf_AIP_Acres) {
          haserror = true;
          this.verificationdataIIR.push(TableId.OPT + "IRR" + "_" + element.ID + "_" + "Verf_AIP_Acres");
        } else {
          element.Verf_Acres_Status = true;
          loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == element.ID).Verf_AIP_Acres_Status = true;
          this.updateAcresVerification(element.ID, false);
        }
      });
    }

    if (loanmodel.LoanCropUnits.filter(p => p.Verf_FSA_Acres_Status != true).length == 0) {
      loanmodel.LoanMaster.FSA_Acres_Verified = true;
    }
    if (loanmodel.LoanCropUnits.filter(p => p.Verf_AIP_Acres_Status != true).length == 0) {
      loanmodel.LoanMaster.AIP_Acres_Verified = true;
    }
     this.localstorage.store(environment.verificationbase, this.verificationdataIIR);
     this.dataservice.setLoanObject(loanmodel);
     //this.parent.loanmodel=loanmodel;
     return haserror;
    }
    verifyColumnvaluesNIR(rowDataNIR: any, loanmodel: any) {
      //
      this.verificationdataNIR = [];
      let haserror = false;
      if (this.currentNIRVerification == 1) {
        rowDataNIR.filter(p => p.Verf_FSA_Acres_touched = true && p.Verf_FSA_Acres != null).forEach(element => {
       if (element.Acres != element.Verf_FSA_Acres) {
         haserror = true;
         this.verificationdataNIR.push(TableId.OPT + "NI_" + element.ID + "_" + "Verf_FSA_Acres");
       } else {
         element.Verf_Acres_Status = true;
         loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == element.ID).Verf_FSA_Acres_Status = true;
         this.updateAcresVerification(element.ID, true);
       }
     });
    } else {
      rowDataNIR.filter(p => p.Verf_AIP_Acres_touched = true && p.Verf_AIP_Acres != null).forEach(element => {
        if (element.Acres != element.Verf_AIP_Acres) {
          haserror = true;
          this.verificationdataNIR.push(TableId.OPT + "NI_" + element.ID + "_" + "Verf_AIP_Acres");
        } else {
          element.Verf_Acres_Status = true;
          loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == element.ID).Verf_AIP_Acres_Status = true;
          this.updateAcresVerification(element.ID, false);
        }
      });
    }

    if (loanmodel.LoanCropUnits.filter(p => p.Verf_FSA_Acres_Status != true).length == 0) {
      loanmodel.LoanMaster.FSA_Acres_Verified = true;
    }
    if (loanmodel.LoanCropUnits.filter(p => p.Verf_AIP_Acres_Status != true).length == 0) {
      loanmodel.LoanMaster.AIP_Acres_Verified = true;
    }
     this.localstorage.store(environment.verificationbase, this.verificationdataNIR);
     this.dataservice.setLoanObjectwithoutsubscription(loanmodel);
     //this.parent.loanmodel=loanmodel;
     return haserror;
    }
    verifyColumnvaluesIIRAPH(rowDataIIR: any, loanmodel: any) {

      this.verificationdataIIRAPH = [];
      let haserror = false;
      rowDataIIR.filter(p => p.Verf_APH_touched = true && p.Verf_APH != null).forEach(element => {
       if (element.Aph != element.Verf_APH) {
         haserror = true;
         this.verificationdataIIRAPH.push(TableId.OPT + "IRR_" + element.ID + "_" + "Verf_APH");
       } else {
         element.Verf_APH_Status = true;
         loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == element.ID).Verf_Ins_APH_Status = true;
         this.updateAPHVerification(element.ID, true);
       }
     });

     this.localstorage.store(environment.verificationbase, this.verificationdataIIRAPH);
     this.dataservice.setLoanObjectwithoutsubscription(loanmodel);
     //this.parent.loanmodel=loanmodel;
     return haserror;
    }
    verifyColumnvaluesNIRAPH(rowDataNIR: any, loanmodel: any) {
      //
      this.verificationdataNIRAPH = [];
      let haserror = false;
      rowDataNIR.filter(p => p.Verf_APH_touched = true && p.Verf_APH != null).forEach(element => {
       if (element.Aph != element.Verf_APH) {

         haserror = true;
         this.verificationdataNIRAPH.push(TableId.OPT + "NI_" + element.ID + "_" + "Verf_APH");
       } else {
         element.Verf_APH_Status = true;
         loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == element.ID).Verf_Ins_APH_Status = true;
         this.updateAPHVerification(element.ID, true);
       }
     });

     this.localstorage.store(environment.verificationbase, this.verificationdataNIRAPH);
     this.dataservice.setLoanObjectwithoutsubscription(loanmodel);
     //this.parent.loanmodel=loanmodel;
     return haserror;
    }
    updateAcresVerification(id, isFSA) {

      this.apiservice.updateVerification(id, isFSA).subscribe(() => {
      });
    }
    updateAPHVerification(id, iscropunit) {
        let that = this;
        this.insapiservice.updateAPHVerification(id, iscropunit).subscribe(p => {

        });
      }
    setVerificationError() {

      setverificationerroralloptimizer(
        this.verificationdataIIR,
        TableId.OPT
      );
      setverificationerroralloptimizer(
        this.verificationdataNIR,
        TableId.OPT
      );
    }
    setVerificationErrorAPH() {

      setverificationerroralloptimizer(
        this.verificationdataIIRAPH,
        TableId.OPT
      );
      setverificationerroralloptimizer(
        this.verificationdataNIRAPH,
        TableId.OPT
      );
    }
    //APH VERIFICATION
    verifyIIRAPH(rowdata: any, loanmodel: any) {

      if (this.verifyButtonStageIIRAPH == VerificationStages.Verify) {
      let columns = this.columnApiIIR.getAllColumns().filter(p => p.colId.includes("Verf_APH"));
      columns.forEach(element => {
        this.columnApiIIR.setColumnVisible(element.colId, true);
      });
      this.verifyButtonStageIIRAPH = VerificationStages.Check;
    } else
    if (this.verifyButtonStageIIRAPH == VerificationStages.Check) {
      if (this.verifyColumnvaluesIIRAPH(rowdata, loanmodel)) {

      this.verifyButtonStageIIRAPH = VerificationStages.Check2;
      }
    } else {
      if (this.verifyColumnvaluesIIRAPH(rowdata, loanmodel)) {
        this.verifyButtonStageIIRAPH = VerificationStages.Check2;
      }
    }
    this.gridApiIIR.redrawRows();
    this.gridApiIIR.refreshHeader();
    setTimeout(() => {
        this.setVerificationErrorAPH();
    }, 500);

    }
    verifyNIRAPH(rowdata: any, loanmodel: any) {
      //
      if (this.verifyButtonStageNIRAPH == VerificationStages.Verify) {
      let columns = this.columnApiNIR.getAllColumns().filter(p => p.colId.includes("Verf_APH"));
      columns.forEach(element => {
        this.columnApiNIR.setColumnVisible(element.colId, true);
      });
      this.verifyButtonStageNIRAPH = VerificationStages.Check;
    } else
    if (this.verifyButtonStageNIRAPH == VerificationStages.Check) {
      if (this.verifyColumnvaluesNIRAPH(rowdata, loanmodel)) {

      this.verifyButtonStageNIRAPH = VerificationStages.Check2;
      }
    } else {
      if (this.verifyColumnvaluesNIRAPH(rowdata, loanmodel)) {
        this.verifyButtonStageNIRAPH = VerificationStages.Check2;
      }
    }
    this.gridApiNIR.redrawRows(); ;
    this.gridApiNIR.refreshHeader();
    setTimeout(() => {
        this.setVerificationErrorAPH();
    }, 2000);
    }

    resetVerification() {
        let columns = this.columnApiIIR.getAllColumns().filter(p => p.colId.includes("Verf"));
         columns.forEach(element => {
           this.columnApiIIR.setColumnVisible(element.colId, false);
         });
         let columnsa = this.columnApiNIR.getAllColumns().filter(p => p.colId.includes("Verf"));
         columns.forEach(element => {
           this.columnApiNIR.setColumnVisible(element.colId, false);
         });
         this.verifyButtonStageIIR = VerificationStages.Verify;
         this.verifyButtonStageIIRAPH = VerificationStages.Verify;
         this.verifyButtonStageNIR = VerificationStages.Verify;
         this.verifyButtonStageNIRAPH = VerificationStages.Verify;
         this.gridApiIIR.redrawRows();
         this.gridApiNIR.redrawRows();
         this.gridApiIIR.refreshHeader();
         this.gridApiNIR.refreshHeader();
       }
}
