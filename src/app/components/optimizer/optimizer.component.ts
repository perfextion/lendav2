import { Crop } from './../reports/cross-collateral/cross-collateral.model';
import { Loan_Key_Visibilty, Loansettings } from '@lenda/models/loansettings';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';
import {
  cellclassmaker,
  cellclassmakerverify
} from '@lenda/aggriddefinations/aggridoptions';
import { Component, OnInit, OnDestroy, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../environments/environment.prod';
import { loan_model, LoanStatus } from '../../models/loanmodel';
import * as _ from 'lodash';
import {
  lookupStateAbvRefValue,
  lookupCountyRefValue
} from '../../Workers/utility/aggrid/stateandcountyboxes';
import { LoancalculationWorker } from '../../Workers/calculations/loancalculationworker';
import { LoggingService } from '../../services/Logs/logging.service';
import { AlertifyService } from '../../alertify/alertify.service';
import { LoanApiService } from '../../services/loan/loanapi.service';
import { EmptyEditor } from '../../aggridfilters/emptybox';
import {
  calculatecolumnwidths,
  headerclassmaker,
  CellType,
  isgrideditable
} from '../../aggriddefinations/aggridoptions';
import { NumericEditor } from '../../aggridfilters/numericaggrid';
import { PublishService } from '../../services/publish.service';
import { Page } from '@lenda/models/page.enum';
import { OptimizerService } from './optimizer.service';
import {
  acresFormatter,
  currencyFormatter,
  numberFormatter,
  percentageFormatter,
  formatacres,
  formatcurrency
} from '../../aggridformatters/valueformatters';
import { ISubscription } from 'rxjs/Subscription';
import { DataService } from '@lenda/services/data.service';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { VerificationStages, TableId, errormodel } from '@lenda/models/commonmodels';
import { numberValueSetterwithdash } from '@lenda/Workers/utility/aggrid/numericboxes';
import { MatDialog } from '@angular/material';
import { CropapiService } from '@lenda/services/crop/cropapi.service';
import { verifyPrimaryHeader } from '@lenda/aggridcolumns/headerComponents/verifyprimaryHeader';
import { OptimizerVerifyService } from './optimizer.verify.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { OptimizerTotalUsedAcres, OptimizerTotalUsedAcresExceedingFarmIrrAcres, OptimizerTotalUsedAcresExceedingFarmNIAcres, OptimizerTotalUsedAcresExceedingTotalNI, OptimizerTotalUsedAcresExceedingTotalIRR } from '@lenda/Workers/utility/validation-functions';
import { Loan_Crop_Unit } from '@lenda/models/cropmodel';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { Chevron } from '@lenda/models/loan-response.model';
import { lookupCropPracticeTypeWithRefData, lookupCropTypeWithRefData } from '@lenda/Workers/utility/aggrid/cropboxes';
import { PubSubService } from '@lenda/services/pubsub.service';
import { MasterService } from '@lenda/master/master.service';
import { FileSaverService } from 'ngx-filesaver';
import * as XLSX from 'xlsx';
import { Helper } from '@lenda/services/math.helper';
import { CheckboxCellRenderer } from '@lenda/aggridcolumns/checkbox-cell-editor/checkbox-cell-rendere';

declare function setmodifiedall(array, keyword): any;

@Component({
  selector: 'app-optimizer',
  templateUrl: './optimizer.component.html',
  styleUrls: ['./optimizer.component.scss'],
  providers: [OptimizerService],
  encapsulation: ViewEncapsulation.None
})
export class OptimizerComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  private errorSub: ISubscription;

  frameworkComponents = { agColumnHeader: verifyPrimaryHeader };
  // Max Undo buttons
  enableMaxIRR: boolean;
  enableMaxNI: boolean;
  public AcresMapped = false;

  private gridApiNIR;
  private columnApiNIR;
  private gridApiIIR;
  private columnApiIRR;

  rowDataNIR = [];
  rowDataIIR = [];
  public frameworkcomponents;
  public loading = false;
  public context;
  public rowClassRules;
  private refdata: RefDataModel;
  currentPageName: Page = Page.optimizer;

  defaultColDef = {
    cellClass: function (params) {
      if (params.data.ID == undefined || params.data == undefined) {
        return 'aggregatecol';
      }
    }
  };

  styleIRR = {
    marginTop: '10px',
    width: '97%',
    boxSizing: 'border-box'
  };

  styleNIR = {
    marginTop: '10px',
    width: '97%',
    boxSizing: 'border-box'
  };

  columnDefsNIR: any = [];
  columnDefsIRR: any = [];

  public Chevron: typeof Chevron = Chevron;
  public columnsDisplaySettingsNIR: ColumnsDisplaySettings = new ColumnsDisplaySettings();
  public columnsDisplaySettingsIRR: ColumnsDisplaySettings = new ColumnsDisplaySettings();

  public loanmodel: loan_model;

  public gridOptionsIIR = {
    getRowNodeId: data => `${TableId.OPT}${'IRR'}_${data.ID}`
  };

  public gridOptionsNIR = {
    getRowNodeId: data => `${TableId.OPT}${'NI'}_${data.ID}`
  };

  errors: errormodel[];
  pinnedBottomRowDataNI = [];
  pinnedBottomRowData = [];
  private preferencesChangeSub1: ISubscription;
  private preferencesChangeSub2: ISubscription;

  //CONSTRUCTOR
  constructor(
    private localstorage: LocalStorageService,
    private optimizerverifyService: OptimizerVerifyService,
    private loancalculationservice: LoancalculationWorker,
    public logging: LoggingService,
    public alertify: AlertifyService,
    public pubsubService: PubSubService,
    public loanapi: LoanApiService,
    private publishService: PublishService,
    private optimizerService: OptimizerService,
    private dataService: DataService,
    private dtss: DatetTimeStampService,
    public dialog: MatDialog,
    public apiservice: CropapiService,
    private settingsService: SettingsService,
    private validationservice: ValidationService,
    public masterSvc: MasterService,
    private _FileSaverService: FileSaverService
  ) {
    this.frameworkcomponents = {
      emptyeditor: EmptyEditor,
      numericCellEditor: NumericEditor,
      checkboxCellRenderer: CheckboxCellRenderer
    };

    this.context = { componentParent: this };
    // change row class contextually here
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);
    this.loanmodel = this.localstorage.retrieve(environment.loankey);
    this.rowClassRules = {
      aggregaterow: function (params) {
        return params.data.ID == undefined;
      }
    };


    this.tabToNextCell = this.tabToNextCell.bind(this);
    // storage observer
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      //

      if (
        res != null &&
        res.srccomponentedit != undefined &&
        res.srccomponentedit != 'optimizercomponent'
      ) {
        this.loanmodel = res;

        this.getgriddata();
      } else {
        this.loanmodel = res;
        this.getgriddata();
        this.gridApiIIR.redrawRows();
        this.gridApiNIR.redrawRows();

        this.gridApiIIR.startEditingCell({
          rowIndex: this.loanmodel.lasteditrowindex,
          colKey: 'Acres'
        });

        this.gridApiNIR.startEditingCell({
          rowIndex: this.loanmodel.lasteditrowindex,
          colKey: 'Acres'
        });
      }
      this.enableDisableUndoMaxButtons();
      let AcresMappedquestion = this.loanmodel.LoanQResponse.find(p => p.Question_ID == 72);
      this.AcresMapped = AcresMappedquestion == undefined ? false : (AcresMappedquestion.Response_Detail == "Yes" ? true : false);

      this.setModifiedAll();
    });

    this.declareColumnsIRR();
    this.declareColumnsNIR();

    this.setModifiedAll();
  }

  private setModifiedAll() {
    setTimeout(() => {
      setmodifiedall(this.localstorage.retrieve(environment.modifiedbase), 'OPTIRR_');
    }, 5);

    setTimeout(() => {
      setmodifiedall(this.localstorage.retrieve(environment.modifiedbase), 'OPTNI_');
    }, 5);
  }


  //Rent Detail Columns
  private rentdetailcolumns = ['Rent_perc', 'Perm_Insure', 'Waiver', 'Ins_Perc', 'Rent'];


  showhiderentdetails(visible: boolean) {
    if (visible) {
      this.columnApiIRR.setColumnsVisible(this.rentdetailcolumns, true);
      this.columnApiNIR.setColumnsVisible(this.rentdetailcolumns, true);
    } else {
      this.columnApiIRR.setColumnsVisible(this.rentdetailcolumns, false);
      this.columnApiNIR.setColumnsVisible(this.rentdetailcolumns, false);
    }
  }
  //ENDS HERE
  //Generic Functions and validations
  validateacresvalue(id, newvalue: number) {
    try {
      let Cropunit = this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == id);
      if (Cropunit != undefined) {
        let farmid = Cropunit.Farm_ID;
        let farm = this.loanmodel.Farms.find(p => p.Farm_ID == Cropunit.Farm_ID);

        let sumofcurrentvalues = _.sumBy(
          this.loanmodel.LoanCropUnits.filter(
            p =>
              p.Farm_ID == farmid &&
              p.Crop_Practice_Type_Code &&
              p.Crop_Practice_Type_Code == Cropunit.Crop_Practice_Type_Code &&
              p.Loan_CU_ID != Cropunit.Loan_CU_ID
          ),
          function (o) {
            return o.CU_Acres;
          }
        );

        let acres = 0;
        if (Cropunit.Crop_Practice_Type_Code.toUpperCase() == 'IRR') {
          acres = farm.Irr_Acres;
        } else {
          acres = farm.NI_Acres;
        }
        if (sumofcurrentvalues + newvalue > acres) {
          return false;
        } else {
          return true;
        }
      }
    } catch (ex) {
      return false;
    }
  }

  gettotalacresonfarmbycropunitid(id: number) {
    try {
      let Cropunit = this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == id);
      if (Cropunit != undefined) {
        let farm = this.loanmodel.Farms.find(p => p.Farm_ID == Cropunit.Farm_ID);
        if (Cropunit.Crop_Practice_Type_Code && Cropunit.Crop_Practice_Type_Code.toUpperCase() == 'IRR') {
          return farm.Irr_Acres;
        } else {
          return farm.NI_Acres;
        }
      } else {
        return 0;
      }
    } catch (ex) {
      return 0;
    }
  }

  ngOnInit() {
    this.localstorage.store(environment.currentpage, Page.optimizer);

    this.pubsubService.subscribeToEvent().subscribe((event) => {
      if (event.name === 'event.optimizer.rentdetail.updated') {
        this.showhiderentdetails(event.data.value);
      }
    });

    let AcresMappedquestion = this.loanmodel.LoanQResponse.find(p => p.Question_ID == 72);
    this.AcresMapped = AcresMappedquestion == undefined ? false : (AcresMappedquestion.Response_Detail == "Yes" ? true : false);
    this.getgriddata();

    this.enableDisableUndoMaxButtons();

    this.getUserPreferencesIRR();
    this.getUserPreferencesNIR();

    this.getInitialValidation();

    this.errorSub = this.localstorage.observe(environment.errorbase).subscribe(res => {
      this.errors = res;
      this.getCurrentErrors();
    });
  }

  getCurrentErrors() {
    setTimeout(() => {
      this.getCurrentErrorsIRR();
      this.getCurrentErrorsNI();
    }, 100);
  }

  private getInitialValidation() {
    this.setValidationErrorForOptimizerIRR();
    this.setValidationErrorForOptimizerNI();
  }

  getUserPreferencesIRR() {
    this.columnsDisplaySettingsNIR = this.settingsService.preferences.loanSettings.columnsDisplaySettings;
    this.preferencesChangeSub1 = this.settingsService.preferencesChange.subscribe(preferences => {
      this.columnsDisplaySettingsNIR = preferences.loanSettings.columnsDisplaySettings;
      this.hideUnhideLoanKeysIIR();
      this.declareColumnsIRR();
    });
    this.hideUnhideLoanKeysIIR();
    this.declareColumnsIRR();
  }

  //Hide Columns Based ON Loan Settings
  private LoankeySettingsIIR: Loan_Key_Visibilty;

  hideUnhideLoanKeysIIR() {

    let LoanSettings: Loansettings = JSON.parse(this.loanmodel.LoanMaster.Loan_Settings);

    if (LoanSettings != undefined || LoanSettings != null) {
      if (
        LoanSettings.Loan_key_Settings != undefined ||
        LoanSettings.Loan_key_Settings != null
      ) {
        this.LoankeySettingsIIR = LoanSettings.Loan_key_Settings;
      } else {
        this.LoankeySettingsIIR = new Loan_Key_Visibilty();
      }
    } else {
      this.LoankeySettingsIIR = new Loan_Key_Visibilty();
    }

    this.columnsDisplaySettingsIRR.cropPrac = this.LoankeySettingsIIR.Crop_Practice == null ? this.columnsDisplaySettingsIRR.cropPrac : this.LoankeySettingsIIR.Crop_Practice;
    this.columnsDisplaySettingsIRR.irrPrac = this.LoankeySettingsIIR.Irr_Practice == null ? this.columnsDisplaySettingsIRR.irrPrac : this.LoankeySettingsIIR.Irr_Practice;
    this.columnsDisplaySettingsIRR.cropType = this.LoankeySettingsIIR.Crop_Type == null ? this.columnsDisplaySettingsIRR.cropType : this.LoankeySettingsIIR.Crop_Type;
    this.columnsDisplaySettingsIRR.rated = this.LoankeySettingsIIR.Rated == null ? this.columnsDisplaySettingsIRR.rated : this.LoankeySettingsIIR.Rated;
    this.columnsDisplaySettingsIRR.section = this.LoankeySettingsIIR.Section == null ? this.columnsDisplaySettingsIRR.section : this.LoankeySettingsIIR.Section;
    this.columnsDisplaySettingsIRR.rateYield = this.LoankeySettingsIIR.RateYield == null ? this.columnsDisplaySettingsIRR.rateYield : this.LoankeySettingsIIR.RateYield;
    this.columnsDisplaySettingsIRR.aph = this.LoankeySettingsIIR.APH == null ? this.columnsDisplaySettingsIRR.aph : this.LoankeySettingsIIR.APH;
  }

  declareColumnsIRR() {
    this.columnDefsIRR = [
      {
        headerName: 'ST | County',
        field: 'StateCounty',
        editable: false,
        width: 120
      },
      {
        headerName: 'Prod %',
        width: 90,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: 'rightaligned aggregatecol',
        valueFormatter: function (params) {
          if (!params.data.isAllTotal) { return percentageFormatter(params, 1); } else { return ''; }
        },
        field: 'Prodpercentage',
        editable: false
      },
      { headerName: 'Landlord', field: 'Landlord', tooltip: params => params.data['Landlord'], editable: false, width: 90 },
      {
        headerName: 'FSN',
        field: 'FSN',
        editable: false,
        cellClass: 'aggregatecol',
        width: 90
      },
      {
        headerName: 'Section',
        field: 'Section',
        editable: false,
        width: 90,
        hide: !this.columnsDisplaySettingsIRR.section
      },
      {
        headerName: 'Rated',
        field: 'Rated',
        editable: false,
        width: 90,
        valueFormatter: function (params) {
          if (params.value == 'NA' || params.value == 'NR') {
            return '';
          }
        },
        hide: !this.columnsDisplaySettingsIRR.rated
      },
      {
        headerName: 'Rent %',
        field: 'Rent_perc',
        editable: false,
        hide: true,
        width: 90,
        cellClass: 'rightaligned aggregatecol',
        valueFormatter: function (params) {
          if (!params.data.istotal) { return percentageFormatter(params, 1); } else { return ''; }
        }
      },
      {
        headerName: 'PTI',
        field: 'Perm_Insure',
        editable: false,
        hide: true,
        width: 90,
        cellRendererSelector: params => {
          if (!params.data.istotal && !params.data.isAllTotal) {
            return {
              component: 'checkboxCellRenderer'
            };
          }
          return null;
        },
        cellRendererParams: params => {
          return {
            type: 'PTI_IRR',
            edit: false
          };
        }
      },
      {
        headerName: 'Ins %',
        field: 'Ins_Perc',
        editable: false,
        hide: true,
        width: 90,
        cellClass: 'rightaligned aggregatecol',
        valueFormatter: function (params) {
          if (!params.data.istotal) { return percentageFormatter(params, 1); } else { return ''; }
        }
      },
      {
        headerName: 'Rent $',
        field: 'Rent',
        editable: false,
        hide: true,
        width: 90,
        cellClass: 'rightaligned aggregatecol',
        valueFormatter: function (params) {
          if (params.data.isAllTotal) {
            return "";
          }
          if (!params.data.istotal) { return currencyFormatter(params, 2); } else { return ''; }
        }
      },
      {
        headerName: 'Waiver $',
        field: 'Waiver',
        editable: false,
        hide: true,
        width: 90,
        cellClass: 'rightaligned aggregatecol',
        valueFormatter: function (params) {
          if (params.data.isAllTotal) {
            return "";
          }
          if (!params.data.istotal) { return currencyFormatter(params, 2); } else { return ''; }
        }
      },
      { headerName: 'CropunitRecord', field: 'ID', hide: true, suppressToolPanel: true },
      {
        headerName: 'Crop',
        field: 'Crop',
        editable: false,
        width: 100,
        tooltip: params => params.data['Crop'],
        cellClassRules: {
          'rightaligned': (params => params.data.istotal || params.data.isAllTotal)
        }
      },
      {
        headerName: 'Crop Type',
        field: 'Crop_Type',
        tooltip: params => params.data['Crop_Type'],
        editable: false,
        width: 90,
        hide: !this.columnsDisplaySettingsIRR.cropType
      },
      {
        headerName: 'Irr Prac',
        field: 'Practice',
        tooltip: params => params.data['Practice'],
        editable: false,
        width: 100,
        hide: !this.columnsDisplaySettingsIRR.irrPrac,
        cellClassRules: {
          'rightaligned': (params => params.data.istotal || params.data.isAllTotal)
        }
      },
      {
        headerName: 'Crop Prac',
        field: 'Crop_Practice',
        tooltip: params => params.data['Crop_Practice'],
        editable: false,
        width: 90,
        hide: !this.columnsDisplaySettingsIRR.cropPrac
      },
      {
        headerName: 'Rate Yield',
        field: 'Rate_Yield',
        minWidth: 90,
        width: 90,
        maxWidth: 90,
        hide: true,
        // headerClass: headerclassmaker(CellType.Integer),
        cellClass: function (params) {
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return cellclassmaker(CellType.Integer, false, 'aggregatecol', status);
          }
          return cellclassmaker(CellType.Integer, true, '', status);
        },
        cellEditorSelector: function (params) {
          if (params.data.ID == undefined) {
            return {
              component: 'emptyeditor'
            };
          } else {
            return {
              component: 'numericCellEditor'
            };
          }
        },
        editable: function (params) {
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return false;
          }
          return isgrideditable(true, status);
        },
        // valueSetter: numberValueSetter,
        valueFormatter: function (params) {
          if (params.data.isAllTotal) {
            return "";
          }
          if (!params.data.istotal) {
            return numberFormatter(params);
          } else { return ''; }
        }
      },
      {
        headerName: 'APH',
        headerComponentFramework: verifyPrimaryHeader,
        headerComponentParams: {
          menuIcon: 'verified_user',
          isverify: true,
          isAPH: true, alreadyverifiedhook: 'APH_IIR'
        },
        hide: !this.columnsDisplaySettingsIRR.aph,
        minWidth: 90,
        headerClass: headerclassmaker(CellType.Integer),
        width: 90,
        field: 'Aph',
        cellClass: function (params) {
          //
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return 'aggregatecol';
          }
          let invoker = params.data.Practice == "NI" ? params.context.componentParent.optimizerverifyService.verifyButtonStageNIRAPH : params.context.componentParent.optimizerverifyService.verifyButtonStageIIRAPH;
          if (invoker == VerificationStages.Verify) {
            return cellclassmaker(CellType.Integer, true, '', status);
          } else
            if (invoker == VerificationStages.Check) {
              if (params.data.Verf_APH_Status != true) {
                return cellclassmaker(CellType.Integer, false, 'redactedcell', status);
              } else { return cellclassmaker(CellType.Integer, false, '', status); }
            } else {
              if (params.data.Verf_APH_touched == true && params.data.Verf_APH != null) {
                return cellclassmaker(CellType.Integer, true, '', status);
              } else if (params.data.Verf_APH_Status != true) {
                return cellclassmaker(CellType.Integer, false, 'redactedcell', status);
                   } else {
                return cellclassmaker(CellType.Integer, false, '', status);
                   }
            }
        },
        cellEditorSelector: function (params) {
          if (params.data.ID == undefined) {
            return {
              component: 'emptyeditor'
            };
          } else {
            return {
              component: 'numericCellEditor'
            };
          }
        },
        editable: function (params) {
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          let editfactor = params.data.ID == undefined ? false : true;
          let invoker = params.data.Practice == "Irr" ? params.context.componentParent.optimizerverifyService.verifyButtonStageIIRAPH : params.context.componentParent.optimizerverifyService.verifyButtonStageNIRAPH;
          if (editfactor == false) {
            return false;
          }
          if (invoker == VerificationStages.Verify) {
            return isgrideditable(true, status);
          } else
            if (invoker == VerificationStages.Check) {
              return isgrideditable(false, status);
            } else {
              if (params.data.Verf_APH_touched == true && params.data.Verf_APH != null) {
                return isgrideditable(true, status);
              } else if (params.data.Verf_APH_Status != true) {
                return isgrideditable(false, status);
                   } else {
                return isgrideditable(false, status);
                   }
            }
        },
        valueFormatter: function (params) {
          if (params.data.isAllTotal) {
            return "";
          }
          if (!params.data.istotal) {
            return numberFormatter(params);
          } else { return ''; }
        }
      },
      {
        headerName: 'APH Verify',
        field: 'Verf_APH',
        headerComponentFramework: verifyPrimaryHeader,
        headerComponentParams: {
          menuIcon: 'cancel',
          isverify: false,
        },
        suppressToolPanel: true,
        supressExcel: true,
        editable: function (params) {
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return false;
          }
          let editparam = params.data.Verf_APH_Status == undefined ? false : params.data.Verf_APH_Status;
          return isgrideditable(!editparam, status);
        },
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: function (params) {
          if (params.data.istotal) {
            return 'aggregatecol';
          }
          let editparam = params.data.Verf_APH_Status == undefined ? false : params.data.Verf_APH_Status;
          return cellclassmakerverify(CellType.Integer, !editparam);
        },
        cellEditorSelector: function (params) {
          if (params.data.ID == undefined) {
            return {
              component: 'emptyeditor'
            };
          } else {
            return {
              component: 'numericCellEditor'
            };
          }
        },
        cellEditorParams: params => {
          return { value: params.data.Verf_APH || 0 };
        },

        valueSetter: numberValueSetterwithdash,
        width: 120,
        minWidth: 90,
        hide: true,
        cellRenderer: function (params) {
          let editparam = params.data.Verf_APH_Status == undefined ? false : params.data.Verf_APH_Status;
          if (editparam) {
            return '<i class="tiny material-icons icon-yellow">done</i>';
          } else {
            return params.value;
          }
        }
      },
      {
        headerName: 'Acres',
        headerComponentFramework: verifyPrimaryHeader,
        headerComponentParams: {
          menuIcon: 'verified_user',
          isverify: true,
          alreadyverifiedhook: 'Acres_IIR'
        },
        width: 90,
        headerClass: headerclassmaker(CellType.Integer),
        field: 'Acres',
        validations: { acres: OptimizerTotalUsedAcres, farm_acres: OptimizerTotalUsedAcresExceedingFarmIrrAcres, exceedingTotal: OptimizerTotalUsedAcresExceedingTotalIRR },
        cellClass: function (params) {

          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return cellclassmaker(CellType.Integer, false, 'aggregatecol');
          }
          let invoker = params.data.Practice == "NI" ? params.context.componentParent.optimizerverifyService.verifyButtonStageNIR : params.context.componentParent.optimizerverifyService.verifyButtonStageIIR;
          let field = params.context.componentParent.optimizerverifyService.currentopenedAcresVeriftTab == "1" ? "Verf_FSA_Acres_Status" : "Verf_AIP_Acres_Status";

          if (invoker == VerificationStages.Verify) {
            return cellclassmaker(CellType.Integer, true, '', status);
          } else
            if (invoker == VerificationStages.Check) {

              if (params.data[field] != true) {
                return cellclassmaker(CellType.Integer, false, 'redactedcell', status);
              } else { return cellclassmaker(CellType.Integer, false, '', status); }
            } else {
              if (params.data[field.toString().replace('Status', 'touched')] == true && params.data[field.toString().replace('_Status', '')] != null) {
                return cellclassmaker(CellType.Integer, true, '', status);
              } else if (params.data[field] != true) {
                return cellclassmaker(CellType.Integer, false, 'redactedcell', status);
                   } else {
                return cellclassmaker(CellType.Integer, false, '', status);
                   }
            }
        },
        cellEditorSelector: function (params) {
          if (params.data.ID == undefined) {
            return {
              component: 'emptyeditor'
            };
          } else {
            return {
              component: 'numericCellEditor'
            };
          }
        },
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          let editfactor = params.data.ID == undefined ? false : true;
          let invoker = params.data.Practice == "NI" ? params.context.componentParent.optimizerverifyService.verifyButtonStageNIR : params.context.componentParent.optimizerverifyService.verifyButtonStageIIR;
          let field = params.context.componentParent.optimizerverifyService.currentopenedAcresVeriftTab == "1" ? "Verf_FSA_Acres_Status" : "Verf_AIP_Acres_Status";

          if (editfactor == false) {
            return false;
          }
          if (invoker == VerificationStages.Verify) {
            return isgrideditable(true, status);
          } else
            if (invoker == VerificationStages.Check) {
              return isgrideditable(false, status);
            } else {
              if (params.data[field.toString().replace('Status', 'touched')] == true && params.data[field.toString().replace('_Status', '')] != null) {
                return isgrideditable(true, status);
              } else if (params.data[field] != true) {
                return isgrideditable(false, status);
                   } else {
                return isgrideditable(false, status);
                   }
            }
        },
        cellEditorParams: {
          decimals: 1
        },
        valueFormatter: function (params) {
          // if (params.data.isAllTotal) {
          //   return "";
          // }

          return acresFormatter(params);

        },
        valueSetter: function (value) {
          let result = value.context.componentParent.validateacresvalue(value.data.ID, parseFloat(value.newValue));
          value.data.Acres = parseFloat(value.newValue);
          return true;
        }
      },
      {
        headerName: 'Acres (FSA)',
        field: 'Verf_FSA_Acres',
        headerComponentFramework: verifyPrimaryHeader,
        headerComponentParams: {
          menuIcon: 'cancel',
          isverify: false
        },
        suppressToolPanel: true,
        supressExcel: true,
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let editparam = params.data.Verf_FSA_Acres_Status == undefined ? false : params.data.Verf_FSA_Acres_Status;
          return isgrideditable(!editparam, params.context.componentParent.loanmodel.LoanMaster.Loan_Status);
        },
        cellEditorSelector: function (params) {
          if (params.data.ID == undefined) {
            return {
              component: 'emptyeditor'
            };
          } else {
            return {
              component: 'numericCellEditor'
            };
          }
        },
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: function (params) {
          if (params.data.istotal) {
            return 'aggregatecol';
          }
          let editparam = params.data.Verf_FSA_Acres_Status == undefined ? false : params.data.Verf_FSA_Acres_Status;
          return cellclassmakerverify(CellType.Integer, !editparam, params.context.componentParent.loanmodel.LoanMaster.Loan_Status);
        },
        cellEditorParams: params => {
          return { value: params.data.Verf_FSA_Acres || 0, decimals: 1 };
        },

        valueSetter: function (params) {
          if (params.data.isAllTotal) {
            return "";
          }
          params.data.Verf_FSA_Acres = parseFloat(
            parseFloat(params.newValue).toFixed(1)
          );
        },
        width: 120,
        minWidth: 90,
        hide: true,
        valueFormatter: acresFormatter,
        cellRenderer: function (params) {
          let editparam = params.data.Verf_FSA_Acres_Status == undefined ? false : params.data.Verf_FSA_Acres_Status;
          if (editparam) {
            return '<i class="tiny material-icons icon-yellow">done</i>';
          } else {
            return params.value;
          }
        }
      },
      {
        headerName: 'Acres (AIP)',
        field: 'Verf_AIP_Acres',
        headerComponentFramework: verifyPrimaryHeader,
        headerComponentParams: {
          menuIcon: 'cancel',
          isverify: false
        },
        suppressToolPanel: true,
        supressExcel: true,
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let editparam = params.data.Verf_AIP_Acres_Status == undefined ? false : params.data.Verf_AIP_Acres_Status;
          return isgrideditable(!editparam, params.context.componentParent.loanmodel.LoanMaster.Loan_Status);
        },
        cellEditorSelector: function (params) {
          if (params.data.ID == undefined) {
            return {
              component: 'emptyeditor'
            };
          } else {
            return {
              component: 'numericCellEditor'
            };
          }
        },
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: function (params) {

          if (params.data.istotal) {
            return 'aggregatecol';
          }
          let editparam = params.data.Verf_AIP_Acres_Status == undefined ? false : params.data.Verf_AIP_Acres_Status;
          return cellclassmakerverify(CellType.Integer, !editparam, params.context.componentParent.loanmodel.LoanMaster.Loan_Status);
        },
        cellEditorParams: params => {
          return { value: params.data.Verf_AIP_Acres || 0, decimals: 1 };
        },

        valueSetter: function (params) {
          if (params.data.isAllTotal) {
            return "";
          }
          params.data.Verf_AIP_Acres = parseFloat(
            parseFloat(params.newValue).toFixed(1)
          );
        },
        width: 120,
        minWidth: 90,
        valueFormatter: acresFormatter,
        hide: true,
        cellRenderer: function (params) {

          let editparam = params.data.Verf_AIP_Acres_Status == undefined ? false : params.data.Verf_AIP_Acres_Status;
          if (editparam) {
            return '<i class="tiny material-icons icon-yellow">done</i>';
          } else {
            return params.value;
          }
        }
      },
      {
        headerName: 'CF',
        minWidth: 60,
        width: 90,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: 'rightaligned aggregatecol',
        field: 'CF',
        editable: false,
        valueFormatter: params => {
          if (params.data.isAllTotal) {
            return "";
          }
          if (!params.data.istotal) { return currencyFormatter(params, 2); } else { return params.value; }
        }
      },
      {
        headerName: 'RC',
        minWidth: 60,
        width: 90,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: 'rightaligned aggregatecol',
        field: 'RC',
        editable: false,
        valueFormatter: params => {

          if (params.data.isAllTotal) {
            return "";
          }
          if (!params.data.istotal) { return currencyFormatter(params, 2); } else { return params.value; }
        }
      },
      {
        headerName: 'Margin',
        minWidth: 60,
        width: 90,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: 'rightaligned aggregatecol',
        field: 'ExcessIns',
        editable: false,
        valueFormatter: params => {
          if (params.data.isAllTotal) {
            return "";
          }
          if (!params.data.istotal) { return currencyFormatter(params, 2); } else { return ''; }
        }
      }
    ];
  }

  getUserPreferencesNIR() {
    this.columnsDisplaySettingsNIR = this.settingsService.preferences.loanSettings.columnsDisplaySettings;

    this.preferencesChangeSub2 = this.settingsService.preferencesChange.subscribe(preferences => {
      this.columnsDisplaySettingsNIR = preferences.loanSettings.columnsDisplaySettings;
      this.hideUnhideLoanKeysNIR();
      this.declareColumnsNIR();
    });

    this.hideUnhideLoanKeysNIR();
    this.declareColumnsNIR();
  }

  //Hide Columns Based ON Loan Settings
  private LoankeySettingsNIR: Loan_Key_Visibilty;
  hideUnhideLoanKeysNIR() {

    let LoanSettings: Loansettings = JSON.parse(this.loanmodel.LoanMaster.Loan_Settings);
    if (LoanSettings != undefined || LoanSettings != null) {
      if (
        LoanSettings.Loan_key_Settings != undefined ||
        LoanSettings.Loan_key_Settings != null
      ) {
        this.LoankeySettingsNIR = LoanSettings.Loan_key_Settings;
      } else {
        this.LoankeySettingsNIR = new Loan_Key_Visibilty();
      }
    } else {
      this.LoankeySettingsNIR = new Loan_Key_Visibilty();
    }
    this.columnsDisplaySettingsNIR.cropPrac = this.LoankeySettingsNIR.Crop_Practice == null ? this.columnsDisplaySettingsNIR.cropPrac : this.LoankeySettingsNIR.Crop_Practice;
    this.columnsDisplaySettingsNIR.irrPrac = this.LoankeySettingsNIR.Irr_Practice == null ? this.columnsDisplaySettingsNIR.irrPrac : this.LoankeySettingsNIR.Irr_Practice;
    this.columnsDisplaySettingsNIR.cropType = this.LoankeySettingsNIR.Crop_Type == null ? this.columnsDisplaySettingsNIR.cropType : this.LoankeySettingsNIR.Crop_Type;
    this.columnsDisplaySettingsNIR.rated = this.LoankeySettingsNIR.Rated == null ? this.columnsDisplaySettingsNIR.rated : this.LoankeySettingsNIR.Rated;
    this.columnsDisplaySettingsNIR.section = this.LoankeySettingsNIR.Section == null ? this.columnsDisplaySettingsNIR.section : this.LoankeySettingsNIR.Section;
    this.columnsDisplaySettingsNIR.rateYield = this.LoankeySettingsNIR.RateYield == null ? this.columnsDisplaySettingsNIR.rateYield : this.LoankeySettingsNIR.RateYield;
    this.columnsDisplaySettingsNIR.aph = this.LoankeySettingsNIR.APH == null ? this.columnsDisplaySettingsNIR.aph : this.LoankeySettingsNIR.APH;
  }
  declareColumnsNIR() {
    this.columnDefsNIR = [
      {
        headerName: 'ST | County',
        field: 'StateCounty',
        editable: false,
        width: 120
      },
      {
        headerName: 'Prod %',
        width: 90,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: 'rightaligned aggregatecol',
        valueFormatter: function (params) {
          if (!params.data.isAllTotal) { return percentageFormatter(params, 1); } else { return ''; }
        },
        field: 'Prodpercentage',
        editable: false
      },
      { headerName: 'Landlord', field: 'Landlord', tooltip: params => params.data['Landlord'], editable: false, width: 90 },
      {
        headerName: 'FSN',
        field: 'FSN',
        tooltip: params => params.data['FSN'],
        editable: false,
        cellClass: 'aggregatecol',
        width: 90
      },
      {
        headerName: 'Section',
        field: 'Section',
        editable: false,
        width: 90,
        hide: !this.columnsDisplaySettingsNIR.section
      },
      {
        headerName: 'Rated',
        field: 'Rated',
        editable: false,
        width: 90,
        valueFormatter: function (params) {
          if (params.value == 'NA' || params.value == 'NR') {
            return '';
          }
        },
        hide: !this.columnsDisplaySettingsNIR.rated
      },
      {
        headerName: 'Rent %',
        field: 'Rent_perc',
        editable: false,
        hide: true,
        width: 90,
        cellClass: 'rightaligned aggregatecol',
        valueFormatter: function (params) {
          if (!params.data.istotal) { return percentageFormatter(params, 1); } else { return ''; }
        }
      },
      {
        headerName: 'PTI',
        field: 'Perm_Insure',
        editable: false,
        hide: true,
        width: 90,
        cellRendererSelector: params => {
          if (!params.data.istotal && !params.data.isAllTotal) {
            return {
              component: 'checkboxCellRenderer'
            };
          }
          return null;
        },
        cellRendererParams: params => {
          return {
            type: 'PTI_NI',
            edit: false
          };
        }
      },
      {
        headerName: 'Ins %',
        field: 'Ins_Perc',
        editable: false,
        hide: true,
        width: 90,
        cellClass: 'rightaligned aggregatecol',
        valueFormatter: function (params) {
          if (!params.data.istotal) {
            return percentageFormatter(params, 1);
          } else { return ''; }
        }
      },
      {
        headerName: 'Rent $',
        field: 'Rent',
        editable: false,
        hide: true,
        width: 90,
        cellClass: 'rightaligned aggregatecol',
        valueFormatter: function (params) {
          if (!params.data.istotal) { return currencyFormatter(params, 2); } else { return ''; }
        }
      },
      {
        headerName: 'Waiver $',
        field: 'Waiver',
        editable: false,
        hide: true,
        width: 90,
        cellClass: 'rightaligned aggregatecol',
        valueFormatter: function (params) {
          if (!params.data.istotal) { return currencyFormatter(params, 2); } else { return ''; }
        }
      },
      { headerName: 'CropunitRecord', field: 'ID', hide: true, suppressToolPanel: true },
      {
        headerName: 'Crop',
        field: 'Crop',
        editable: false,
        width: 90,
        tooltip: params => params.data['Crop'],
        cellClassRules: {
          'rightaligned': (params => params.data.istotal || params.data.isAllTotal)
        }
      },
      {
        headerName: 'Crop Type',
        field: 'Crop_Type',
        tooltip: params => params.data['Crop_Type'],
        editable: false,
        width: 90,
        hide: !this.columnsDisplaySettingsNIR.cropType
      },
      {
        headerName: 'Irr Prac',
        field: 'Practice',
        tooltip: params => params.data['Practice'],
        editable: false,
        width: 90,
        hide: !this.columnsDisplaySettingsNIR.irrPrac,
        cellClassRules: {
          'rightaligned': (params => params.data.istotal || params.data.isAllTotal)
        }
      },
      {
        headerName: 'Crop Prac',
        field: 'Crop_Practice',
        tooltip: params => params.data['Crop_Practice'],
        editable: false,
        width: 90,
        hide: !this.columnsDisplaySettingsNIR.cropPrac
      },
      {
        headerName: 'Rate Yield',
        field: 'Rate_Yield',
        minWidth: 90,
        width: 90,
        maxWidth: 90,
        hide: true,
        // headerClass: headerclassmaker(CellType.Integer),
        cellClass: function (params) {
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return cellclassmaker(CellType.Integer, false, 'aggregatecol', status);
          }
          return cellclassmaker(CellType.Integer, true, '', status);
        },
        cellEditorSelector: function (params) {
          if (params.data.ID == undefined) {
            return {
              component: 'emptyeditor'
            };
          } else {
            return {
              component: 'numericCellEditor'
            };
          }
        },
        editable: function (params) {
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return false;
          }
          return isgrideditable(true, status);
        },
        // valueSetter: numberValueSetter,
        valueFormatter: function (params) {

          if (params.data.isAllTotal) {
            return "";
          }
          if (!params.data.istotal) {
            return numberFormatter(params);
          } else { return ''; }
        }
      },
      {
        headerName: 'APH',
        headerComponentFramework: verifyPrimaryHeader,
        headerComponentParams: {
          menuIcon: 'verified_user',
          isverify: true,
          isAPH: true
          , alreadyverifiedhook: 'APH_NIR'
        },
        minWidth: 90,
        width: 90,
        field: 'Aph',
        hide: !this.columnsDisplaySettingsNIR.aph,
        cellClass: function (params) {
          //
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return 'aggregatecol';
          }
          let invoker = params.data.Practice == "NI" ? params.context.componentParent.optimizerverifyService.verifyButtonStageNIRAPH : params.context.componentParent.optimizerverifyService.verifyButtonStageIIRAPH;
          if (invoker == VerificationStages.Verify) {
            return cellclassmaker(CellType.Integer, true, '', status);
          } else
            if (invoker == VerificationStages.Check) {
              if (params.data.Verf_APH_Status != true) {
                return cellclassmaker(CellType.Integer, false, 'redactedcell', status);
              } else { return cellclassmaker(CellType.Integer, false, '', status); }
            } else {
              if (params.data.Verf_APH_touched == true && params.data.Verf_APH != null) {
                return cellclassmaker(CellType.Integer, true, '', status);
              } else if (params.data.Verf_APH_Status != true) {
                return cellclassmaker(CellType.Integer, false, 'redactedcell', status);
                   } else {
                return cellclassmaker(CellType.Integer, false, '', status);
                   }
            }
        },
        cellEditorSelector: function (params) {
          if (params.data.ID == undefined) {
            return {
              component: 'emptyeditor'
            };
          } else {
            return {
              component: 'numericCellEditor'
            };
          }
        },
        editable: function (params) {
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          let editfactor = params.data.ID == undefined ? false : true;
          let invoker = params.data.Practice == "Irr" ? params.context.componentParent.optimizerverifyService.verifyButtonStageIIRAPH : params.context.componentParent.optimizerverifyService.verifyButtonStageNIRAPH;
          if (editfactor == false) {
            return false;
          }
          if (invoker == VerificationStages.Verify) {
            return isgrideditable(true, status);
          } else
            if (invoker == VerificationStages.Check) {
              return isgrideditable(false, status);
            } else {
              if (params.data.Verf_APH_touched == true && params.data.Verf_APH != null) {
                return isgrideditable(true, status);
              } else if (params.data.Verf_APH_Status != true) {
                return isgrideditable(false, status);
                   } else {
                return isgrideditable(false, status);
                   }
            }
        },
        valueFormatter: function (params) {
          if (params.data.isAllTotal) {
            return "";
          }
          if (!params.data.istotal) {
            return numberFormatter(params);
          } else { return ''; }
        }
      },
      {
        headerName: 'APH Verify',
        field: 'Verf_APH',
        headerComponentFramework: verifyPrimaryHeader,
        headerComponentParams: {
          menuIcon: 'cancel',
          isverify: false
        },
        suppressToolPanel: true,
        supressExcel: true,
        editable: function (params) {
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return false;
          }
          let editparam = params.data.Verf_APH_Status == undefined ? false : params.data.Verf_APH_Status;
          return isgrideditable(!editparam, status);
        },
        cellEditorSelector: function (params) {
          if (params.data.ID == undefined) {
            return {
              component: 'emptyeditor'
            };
          } else {
            return {
              component: 'numericCellEditor'
            };
          }
        },
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: function (params) {
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return 'aggregatecol';
          }
          let editparam = params.data.Verf_APH_Status == undefined ? false : params.data.Verf_APH_Status;
          return cellclassmakerverify(CellType.Integer, !editparam, status);
        },
        cellEditorParams: params => {
          return { value: params.data.Verf_APH || 0 };
        },

        valueSetter: numberValueSetterwithdash,
        width: 120,
        minWidth: 90,
        hide: true,
        cellRenderer: function (params) {
          let editparam = params.data.Verf_APH_Status == undefined ? false : params.data.Verf_APH_Status;
          if (editparam) {
            return '<i class="tiny material-icons icon-yellow">done</i>';
          } else {
            return params.value;
          }
        }
      },
      {
        headerName: 'Acres',
        headerComponentFramework: verifyPrimaryHeader,
        headerComponentParams: {
          menuIcon: 'verified_user',
          isverify: true
          , alreadyverifiedhook: 'Acres_NIR'
        },
        width: 90,
        headerClass: headerclassmaker(CellType.Integer),
        field: 'Acres',
        validations: { acres: OptimizerTotalUsedAcres, farm_acres: OptimizerTotalUsedAcresExceedingFarmNIAcres, exceedingTotal: OptimizerTotalUsedAcresExceedingTotalNI },
        cellClass: function (params) {

          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return cellclassmaker(CellType.Integer, false, 'aggregatecol');
          }
          let invoker = params.data.Practice == "NI" ? params.context.componentParent.optimizerverifyService.verifyButtonStageNIR : params.context.componentParent.optimizerverifyService.verifyButtonStageIIR;
          let field = params.context.componentParent.optimizerverifyService.currentopenedAcresVeriftTabNIR == "1" ? "Verf_FSA_Acres_Status" : "Verf_AIP_Acres_Status";

          if (invoker == VerificationStages.Verify) {
            return cellclassmaker(CellType.Integer, true, '', status);
          } else
            if (invoker == VerificationStages.Check) {

              if (params.data[field] != true) {
                return cellclassmaker(CellType.Integer, false, 'redactedcell', status);
              } else { return cellclassmaker(CellType.Integer, false, '', status); }
            } else {
              if (params.data[field.toString().replace('Status', 'touched')] == true && params.data[field.toString().replace('_Status', '')] != null) {
                return cellclassmaker(CellType.Integer, true, '', status);
              } else if (params.data[field] != true) {
                return cellclassmaker(CellType.Integer, false, 'redactedcell', status);
                   } else {
                return cellclassmaker(CellType.Integer, false, '', status);
                   }
            }
        },
        cellEditorSelector: function (params) {
          if (params.data.ID == undefined) {
            return {
              component: 'emptyeditor'
            };
          } else {
            return {
              component: 'numericCellEditor'
            };
          }
        },
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          let editfactor = params.data.ID == undefined ? false : true;
          let invoker = params.data.Practice == "NI" ? params.context.componentParent.optimizerverifyService.verifyButtonStageNIR : params.context.componentParent.optimizerverifyService.verifyButtonStageIIR;
          let field = params.context.componentParent.optimizerverifyService.currentopenedAcresVeriftTabNIR == "1" ? "Verf_FSA_Acres_Status" : "Verf_AIP_Acres_Status";

          if (editfactor == false) {
            return false;
          }
          if (invoker == VerificationStages.Verify) {
            return isgrideditable(true, status);
          } else
            if (invoker == VerificationStages.Check) {
              return isgrideditable(false, status);
            } else {
              if (params.data[field.toString().replace('Status', 'touched')] == true && params.data[field.toString().replace('_Status', '')] != null) {
                return isgrideditable(true, status);
              } else if (params.data[field] != true) {
                return isgrideditable(false, status);
                   } else {
                return isgrideditable(false, status);
                   }
            }
        },
        cellEditorParams: {
          decimals: 1
        },
        valueFormatter: function (params) {
          // if (params.data.isAllTotal) {
          //   return "";
          // }

          return acresFormatter(params);

        },
        valueSetter: function (value) {
          let result = value.context.componentParent.validateacresvalue(
            value.data.ID,
            parseFloat(value.newValue)
          );
          value.data.Acres = parseFloat(value.newValue);
          return true;

        }
      },
      {
        headerName: 'Acres (FSA)',
        field: 'Verf_FSA_Acres',
        headerComponentFramework: verifyPrimaryHeader,
        headerComponentParams: {
          menuIcon: 'cancel',
          isverify: false
        },
        suppressToolPanel: true,
        supressExcel: true,
        editable: function (params) {
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return false;
          }
          let editparam = params.data.Verf_FSA_Acres_Status == undefined ? false : params.data.Verf_FSA_Acres_Status;
          return isgrideditable(!editparam, status);
        },
        cellEditorSelector: function (params) {
          if (params.data.ID == undefined) {
            return {
              component: 'emptyeditor'
            };
          } else {
            return {
              component: 'numericCellEditor'
            };
          }
        },
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: function (params) {

          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return 'aggregatecol';
          }
          let editparam = params.data.Verf_FSA_Acres_Status == undefined ? false : params.data.Verf_FSA_Acres_Status;
          return cellclassmakerverify(CellType.Integer, !editparam, status);
        },
        cellEditorParams: params => {
          return { value: params.data.Verf_FSA_Acres || 0, decimals: 1 };
        },

        valueSetter: function (params) {
          if (params.data.isAllTotal) {
            return "";
          }
          params.data.Verf_FSA_Acres = parseFloat(
            parseFloat(params.newValue).toFixed(1)
          );
        },
        width: 120,
        minWidth: 90,
        hide: true,
        valueFormatter: acresFormatter,
        cellRenderer: function (params) {
          let editparam = params.data.Verf_FSA_Acres_Status == undefined ? false : params.data.Verf_FSA_Acres_Status;
          if (editparam) {
            return '<i class="tiny material-icons icon-yellow">done</i>';
          } else {
            return params.value;
          }
        }
      },
      {
        headerName: 'Acres (AIP)',
        field: 'Verf_AIP_Acres',
        headerComponentFramework: verifyPrimaryHeader,
        headerComponentParams: {
          menuIcon: 'cancel',
          isverify: false
        },
        suppressToolPanel: true,
        supressExcel: true,
        editable: function (params) {
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return false;
          }
          let editparam = params.data.Verf_AIP_Acres_Status == undefined ? false : params.data.Verf_AIP_Acres_Status;
          return isgrideditable(!editparam, status);
        },
        cellEditorSelector: function (params) {
          if (params.data.ID == undefined) {
            return {
              component: 'emptyeditor'
            };
          } else {
            return {
              component: 'numericCellEditor'
            };
          }
        },
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: function (params) {
          let status = params.context.componentParent.loanmodel.LoanMaster.Loan_Status;
          if (params.data.istotal) {
            return 'aggregatecol';
          }
          let editparam = params.data.Verf_AIP_Acres_Status == undefined ? false : params.data.Verf_AIP_Acres_Status;
          return cellclassmakerverify(CellType.Integer, !editparam, status);
        },
        cellEditorParams: params => {
          return { value: params.data.Verf_AIP_Acres || 0, decimals: 1 };
        },

        valueSetter: function (params) {
          if (params.data.isAllTotal) {
            return "";
          }
          params.data.Verf_AIP_Acres = parseFloat(
            parseFloat(params.newValue).toFixed(1)
          );
        },
        width: 120,
        minWidth: 90,
        valueFormatter: acresFormatter,
        hide: true,
        cellRenderer: function (params) {
          let editparam = params.data.Verf_AIP_Acres_Status == undefined ? false : params.data.Verf_AIP_Acres_Status;
          if (editparam) {
            return '<i class="tiny material-icons icon-yellow">done</i>';
          } else {
            return params.value;
          }
        }
      },
      {
        headerName: 'CF',
        minWidth: 60,
        width: 90,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: 'rightaligned aggregatecol',
        field: 'CF',
        editable: false,
        valueFormatter: params => {
          if (params.data.isAllTotal) {
            return "";
          }
          if (!params.data.istotal) { return currencyFormatter(params, 2); } else { return params.value; }
        }
      },
      {
        headerName: 'RC',
        minWidth: 60,
        width: 90,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: 'rightaligned aggregatecol',
        field: 'RC',
        editable: false,
        valueFormatter: params => {
          if (params.data.isAllTotal) {
            return "";
          }
          if (!params.data.istotal) { return currencyFormatter(params, 2); } else { return params.value; }
        }
      },
      {
        headerName: 'Margin',
        minWidth: 60,
        width: 90,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: 'rightaligned aggregatecol',
        field: 'ExcessIns',
        editable: false,
        valueFormatter: params => {
          if (params.data.isAllTotal) {
            return "";
          }
          if (!params.data.istotal) { return currencyFormatter(params, 2); } else { return ''; }
        }
      }
    ];
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.errorSub) {
      this.errorSub.unsubscribe();
    }

    if (this.preferencesChangeSub1) {
      this.preferencesChangeSub1.unsubscribe();
    }

    if (this.preferencesChangeSub2) {
      this.preferencesChangeSub2.unsubscribe();
    }
  }

  getverifiedstatusIIR(key: string) {
    switch (key) {
      case 'APH':
        return this.loanmodel.LoanCropUnits.filter(p => p.Crop_Practice_Type_Code.toUpperCase() == 'IRR' && p.Verf_Ins_APH_Status != true).length == 0;
        break;
      case 'Acres':
        return this.loanmodel.LoanCropUnits.filter(p => p.Crop_Practice_Type_Code.toUpperCase() == 'IRR' && p.Verf_FSA_Acres_Status != true && p.Verf_AIP_Acres_Status != true).length == 0;
        break;
      default:
        false;
    }
  }
  getverifiedstatusNIR(key: string) {
    switch (key) {
      case 'APH':
        return this.loanmodel.LoanCropUnits.filter(p => p.Crop_Practice_Type_Code.toUpperCase() == 'NI' && p.Verf_Ins_APH_Status != true).length == 0;
        break;
      case 'Acres':
        return this.loanmodel.LoanCropUnits.filter(p => p.Crop_Practice_Type_Code.toUpperCase() == 'NI' && p.Verf_FSA_Acres_Status != true && p.Verf_AIP_Acres_Status != true).length == 0;
        break;
      default:
        false;
    }
  }
  //Grid Functions
  getgriddata() {
    //Get localstorage first
    this.rowDataIIR = [];
    this.rowDataNIR = [];
    if (this.loanmodel != null) {
      //Start making Rows here for IRR
      this.loanmodel.Farms.filter(f => f.Irr_Acres > 0).forEach(farm => {
        //get distinct crops for the farm
        let temparray = [];
        let StateCounty = lookupStateAbvRefValue(farm.Farm_State_ID, this.refdata) + ' | ' + lookupCountyRefValue(farm.Farm_County_ID, this.refdata);

        let distinctrows = this.loanmodel.LoanCropUnits.filter(
          a => a.ActionStatus != 3 && !a.FC_Disabled
        ).filter(
          p =>
            p.Farm_ID == farm.Farm_ID &&
            p.Crop_Practice_Type_Code && p.Crop_Practice_Type_Code.toUpperCase() == 'IRR'
        );

        try {
          distinctrows.forEach(crop => {
            let row: any = {};
            row.ID = crop.Loan_CU_ID;
            row.Practice = 'Irr';
            row.Crop_Practice = lookupCropPracticeTypeWithRefData(crop.Crop_Practice_ID, this.refdata);
            row.Crop_Type = lookupCropTypeWithRefData(crop.Crop_Practice_ID, this.refdata);
            row.StateCounty = StateCounty;
            row.Prodpercentage = crop.FC_ProdPerc;
            row.FC_State_Name = farm.FC_State_Code;
            row.FC_County_Name = farm.FC_County_Name;
            row.Landlord = farm.Landowner;
            row.FSN = farm.FSN;
            row.FC_FSN = farm.FSN;
            row.Section = farm.Section;
            row.Rated = farm.Rated;
            row.Rent_perc = crop.FC_Rent_Percent || (100 - farm.Percent_Prod);
            row.Rate_Yield = crop.Rate_Yield;
            row.Perm_Insure = farm.Permission_To_Insure;
            row.Ins_Perc = crop.FC_Insurance_Share;
            row.Rent = farm.Cash_Rent_Per_Acre;
            row.Waiver = farm.Cash_Rent_Waived;
            row.Crop = this.getCropName(crop.Crop_Code);
            row.CF = crop.FC_Cashflow_Acre;
            row.RC = crop.FC_RiskCushion_Acre;
            row.ExcessIns = crop.FC_Margin_Acre;
            row.Acres = crop.CU_Acres;
            row.FC_Total_Used_Acres = farm.FC_Total_Used_Acres;
            row.FC_Total_Used_Irr_Acres = farm.FC_Total_Used_Irr_Acres;
            row.FC_Irr_Acres = farm.Irr_Acres;
            row.Aph = crop.Ins_APH;
            row.Verf_AIP_Acres = crop.Verf_AIP_Acres;
            row.Verf_AIP_Acres_Status = crop.Verf_AIP_Acres_Status;
            row.Verf_AIP_Acres_touched = crop.Verf_AIP_Acres_touched;

            row.Verf_FSA_Acres = crop.Verf_FSA_Acres;
            row.Verf_FSA_Acres_Status = crop.Verf_FSA_Acres_Status;
            row.Verf_FSA_Acres_touched = crop.Verf_FSA_Acres_touched;

            row.Verf_APH = crop.Verf_Ins_APH;
            row.Verf_APH_Status = crop.Verf_Ins_APH_Status;
            row.Verf_APH_touched = crop.Verf_Ins_APH_touched;
            this.rowDataIIR.push(row);
            temparray.push(row);
            row.istotal = false;
          });

          if (this.rowDataIIR.length > 0) {
            let row: any = {};

            let cusum = _.sumBy(distinctrows, function (o) {
              return o.CU_Acres;
            });
            row.Acres =  (farm.Irr_Acres - cusum).toFixed(1) ;
            row.RC = '';
            row.istotal = true;
            row.StateCounty = StateCounty;
            row.Prodpercentage = farm.Percent_Prod;
            row.Landlord = farm.Landowner;
            row.FSN = farm.FSN;

            //row.Crop = formatacres(farm.Irr_Acres.toFixed(1));
            row.Crop = "";
            row.Practice = formatacres((farm.Irr_Acres -   parseFloat(row.Acres.replace(/,/g, ''))).toFixed(1));
            row.Is_Invalid = (farm.Irr_Acres - parseFloat(row.Acres.replace(/,/g, ''))) < 0;

            row.FC_State_Name = farm.FC_State_Name;
            row.FC_County_Name = farm.FC_County_Name;
            row.FC_FSN = farm.FSN;

            row.ExcessIns = '';
            row.Crop_Practice = 'Available';

            this.rowDataIIR.push(row);
          }
        } catch (Exception) {

        }
      });
      this.generatetotalsIIR();

      //NIR
      this.loanmodel.Farms.filter(f => f.NI_Acres > 0).forEach(farm => {
        //get distinct crops for the farm
        let StateCounty = lookupStateAbvRefValue(farm.Farm_State_ID, this.refdata) + ' | ' + lookupCountyRefValue(farm.Farm_County_ID, this.refdata);

        let distinctrows = this.loanmodel.LoanCropUnits.filter(
          a => a.ActionStatus != 3 && !a.FC_Disabled
        ).filter(
          p => p.Farm_ID == farm.Farm_ID && p.Crop_Practice_Type_Code && p.Crop_Practice_Type_Code.toUpperCase() == 'NI'
        );
        try {
          distinctrows.forEach(crop => {
            let row: any = {};
            row.ID = crop.Loan_CU_ID;
            row.Practice = 'NI';
            row.Crop_Practice = lookupCropPracticeTypeWithRefData(crop.Crop_Practice_ID, this.refdata);
            row.Crop_Type = lookupCropTypeWithRefData(crop.Crop_Practice_ID, this.refdata);
            row.StateCounty = StateCounty;
            row.Prodpercentage = crop.FC_ProdPerc;
            row.Landlord = farm.Landowner;
            row.FC_State_Name = farm.FC_State_Code;
            row.FC_County_Name = farm.FC_County_Name;
            row.FC_FSN = farm.FSN;
            row.FSN = farm.FSN;
            row.Section = farm.Section;
            row.Rated = farm.Rated;
            row.Rent_perc = crop.FC_Rent_Percent || (100 - farm.Percent_Prod);
            row.Rate_Yield = crop.Rate_Yield;
            row.Perm_Insure = farm.Permission_To_Insure;
            row.Ins_Perc = crop.FC_Insurance_Share;
            row.Rent = farm.Cash_Rent_Per_Acre;
            row.Waiver = farm.Cash_Rent_Waived;
            row.Crop = this.getCropName(crop.Crop_Code);
            row.CF = crop.FC_Cashflow_Acre;
            row.RC = crop.FC_RiskCushion_Acre;
            row.ExcessIns = crop.FC_Margin_Acre;
            row.Acres = crop.CU_Acres;
            row.FC_Total_Used_Acres = farm.FC_Total_Used_Acres;
            row.FC_Total_Used_NI_Acres = farm.FC_Total_Used_NI_Acres;
            row.FC_NI_Acres = farm.NI_Acres;
            row.istotal = false;
            row.Verf_AIP_Acres = crop.Verf_AIP_Acres;
            row.Verf_AIP_Acres_Status = crop.Verf_AIP_Acres_Status;
            row.Verf_AIP_Acres_touched = crop.Verf_AIP_Acres_touched;

            row.Verf_FSA_Acres = crop.Verf_FSA_Acres;
            row.Verf_FSA_Acres_Status = crop.Verf_FSA_Acres_Status;
            row.Verf_FSA_Acres_touched = crop.Verf_FSA_Acres_touched;

            row.Verf_APH = crop.Verf_Ins_APH;
            row.Verf_APH_Status = crop.Verf_Ins_APH_Status;
            row.Verf_APH_touched = crop.Verf_Ins_APH_touched;
            row.Aph = crop.Ins_APH;
            this.rowDataNIR.push(row);
            row.istotal = false;
          });

          if (this.rowDataNIR.length > 0) {

            let row: any = {};
            let cudsum = _.sumBy(distinctrows, function (o) {
              return o.CU_Acres;
            });
            row.Acres =  (farm.NI_Acres - cudsum).toFixed(1);
            row.RC = '';
            row.istotal = true;
            row.StateCounty = StateCounty;
            row.Prodpercentage = farm.Percent_Prod;
            row.Landlord = farm.Landowner;
            row.FSN = farm.FSN;

            //row.Crop = formatacres(farm.NI_Acres.toFixed(1));
             row.Crop = "";
            row.Practice = formatacres((farm.NI_Acres - parseFloat(row.Acres.replace(/,/g, ''))).toFixed(1));
            row.Is_Invalid = (farm.NI_Acres - parseFloat(row.Acres.replace(/,/g, ''))) < 0;

            row.FC_State_Name = farm.FC_State_Name;
            row.FC_County_Name = farm.FC_County_Name;
            row.FC_FSN = farm.FSN;

            if (isNaN(row.Practice)) {
            }
            row.Crop_Practice = 'Available';

            row.ExcessIns = '';
            this.rowDataNIR.push(row);
          }
        } catch (Exception) {

        }
      });
      //End rows here
      this.generatetotalsNI();
    }
  }

  getCropName(cropCode: string) {
    try {
      if (!this.refdata) {
        this.refdata = this.localstorage.retrieve(environment.referencedatakey);
      }

      let crop = this.refdata.CropList.find(a => a.Crop_Code == cropCode);
      if (crop) {
        return crop.Crop_Name;
      } else {
        return '';
      }
    } catch {
      return '';
    }
  }

  syncenabled() {
    if (
      this.loanmodel.LoanCropUnits.filter(p => p.ActionStatus == 2).length == 0
    ) {
      return 'disabled';
    } else { return ''; }
  }

  /**
   * Sync to database - publish button event
   */
  synctoDb() {
    this.publishService.syncCompleted();
    this.localstorage.store(environment.modifiedoptimizerdata, null);
    this.optimizerService.syncToDb(this.localstorage.retrieve(environment.loankey));
    this.getInitialValidation();
  }

  // private _tempeditindex = 0;
  // private _tempeditfield = "";
  rowvaluechangedirr($event) {

    // this._tempeditfield = $event.colDef.field;
    // this._tempeditindex = $event.rowIndex;
    if ($event.data.isAllTotal) {
      return;
    }
    if (isNaN(parseFloat($event.value))) {
      $event.value = 0;
    }

    if ($event.column.colId == "Verf_FSA_Acres") {
      let cu = this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == $event.data.ID && p.Crop_Practice_Type_Code.toUpperCase() == ($event.data.Practice == "NI" ? 'NI' : 'IRR'));
      cu.Verf_FSA_Acres = parseFloat($event.value);
      cu.Verf_FSA_Acres_touched = true;
      this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
      this.publishService.enableSync(Page.optimizer);
      return;
    }
    if ($event.column.colId == "Verf_AIP_Acres") {
      let cu = this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == $event.data.ID && p.Crop_Practice_Type_Code.toUpperCase() == ($event.data.Practice == "NI" ? 'NI' : 'IRR'));
      cu.Verf_AIP_Acres = parseFloat($event.value);
      cu.Verf_AIP_Acres_touched = true;
      // if (cu.CU_Acres == parseFloat($event.value)) {
      //   cu.Verf_AIP_Acres_Status = true;
      // }
      this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
      this.publishService.enableSync(Page.optimizer);
      return;
    }
    if ($event.column.colId == "Verf_APH") {
      let cu = this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == $event.data.ID && p.Crop_Practice_Type_Code.toUpperCase() == ($event.data.Practice == "NI" ? 'NI' : 'IRR'));
      cu.Verf_Ins_APH = parseFloat($event.value);
      cu.Verf_Ins_APH_touched = true;
      this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
      this.publishService.enableSync(Page.optimizer);
      return;
    }

    if ($event.data.ID != undefined || $event.data.ID != null) {
      let oldvalue = 0;
      if ($event.colDef.field == "Acres") {
        oldvalue = this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == $event.data.ID).CU_Acres;
      } else if ($event.colDef.field == "Aph") {
        oldvalue = this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == $event.data.ID).Ins_APH;
           } else {
        oldvalue = this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == $event.data.ID).Rate_Yield;
           }
      if (oldvalue != $event.value) {
        if ($event.colDef.field == "Aph") {
          let item = this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == $event.data.ID && p.Crop_Practice_Type_Code.toUpperCase() == ($event.data.Practice == "NI" ? 'NI' : 'IRR'));
          item.Ins_APH = parseFloat($event.value);
          if (item.Verf_Ins_APH_Status == true) {
            item.Verf_Ins_APH = 0;
            item.Verf_Ins_APH_Status = false;
          }
        } else if ($event.colDef.field == "Acres") {
          if (!this.enableMaxIRR) {
            let modified = this.localstorage.retrieve(environment.modifiedoptimizerdata);
            if (!modified) {
              modified = {};
            }

            modified['IRR'] = this.loanmodel.LoanCropUnits.filter(a => a.Crop_Practice_Type_Code == 'Irr').map(c => {
              return {
                ID: c.Loan_CU_ID,
                Acres: c.CU_Acres
              };
            });

            this.localstorage.store(environment.modifiedoptimizerdata, modified);
            this.enableMaxIRR = true;
          }
          let cu = this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == $event.data.ID && p.Crop_Practice_Type_Code.toUpperCase() == ($event.data.Practice == "NI" ? 'NI' : 'IRR'));
          cu.CU_Acres = parseFloat($event.value);
          if (cu.Verf_AIP_Acres_Status == true || cu.Verf_FSA_Acres_Status == true) {
            cu.Verf_AIP_Acres_Status = false;
            cu.Verf_FSA_Acres_Status = false;
            cu.Verf_AIP_Acres = 0;
            cu.Verf_FSA_Acres = 0;

          }
        } else {
          this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == $event.data.ID && p.Crop_Practice_Type_Code.toUpperCase() == ($event.data.Practice == "NI" ? 'NI' : 'IRR')).Rate_Yield = parseFloat($event.value);
             }
        this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == $event.data.ID && p.Crop_Practice_Type_Code.toUpperCase() == ($event.data.Practice == "NI" ? 'NI' : 'IRR')).ActionStatus = 2;
        this.loanmodel.srccomponentedit = "optimizercomponent";
        this.loanmodel.lasteditrowindex = $event.rowIndex;
        this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
        this.publishService.enableSync(Page.optimizer);

        if ($event.colDef.field == 'Acres') {
          this.setValidationErrorForOptimizerIRR();
          this.setValidationErrorForOptimizerNI();
        }

        //#region MODIFIED YELLOW VALUES

        let modifiedvalues = this.localstorage.retrieve(environment.modifiedbase) as Array<String>;

        if (!modifiedvalues.includes("OPTIRR_" + $event.data.ID + "_" + $event.colDef.field)) {
          modifiedvalues.push("OPTIRR_" + $event.data.ID + "_" + $event.colDef.field);
          this.localstorage.store(environment.modifiedbase, modifiedvalues);
        }

        //#endregion MODIFIED YELLOW VALUES
      }
    }
    setTimeout(() => {
      if (this.hastabbed) {
      this.gridApiIIR.startEditingCell({
        rowIndex:  this.tabbedrow,
        colKey:  "Acres"
    });
  }
   this.hastabbed = false;
    }, 500);
  }
private hastabbed = false;
private tabbedrow = 0;
  tabToNextCell(params) {

    let previousCell = params.previousCellDef;
    let lastRowIndex = previousCell.rowIndex;
    let nextRowIndex = params.backwards ? lastRowIndex - 1 : lastRowIndex + 1;
    let renderedRowCount = params.nextCellDef.column.gridApi.getModel().getRowCount();
    if (nextRowIndex < 0) {
      nextRowIndex = 0;
    }
    if (nextRowIndex >= renderedRowCount) {
      nextRowIndex = renderedRowCount - 1;
    }
    if (params.nextCellDef.column.gridApi.getModel().rowsToDisplay[nextRowIndex].data.istotal == true) {
      if (!params.backwards) {
      nextRowIndex = nextRowIndex + 1;
      } else {
      nextRowIndex = nextRowIndex - 1;
      }
    }
    let result = {
      rowIndex: nextRowIndex,
      column: previousCell.column,
      floating: previousCell.floating
    };
    this.hastabbed = true;
    this.tabbedrow = nextRowIndex;
    //return result;
  }
  rowvaluechangednir($event) {

    if ($event.data.isAllTotal) {
      return;
    }
    let cu = this.loanmodel.LoanCropUnits.find(p => p.Loan_CU_ID == $event.data.ID);

    if (isNaN(parseFloat($event.value))) {
      $event.value = 0;
    }

    if ($event.column.colId == 'Verf_FSA_Acres') {
      cu.Verf_FSA_Acres = parseFloat($event.value);
      cu.Verf_FSA_Acres_touched = true;

      this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
      this.publishService.enableSync(Page.optimizer);
      return;
    }

    if ($event.column.colId == 'Verf_AIP_Acres') {
      cu.Verf_AIP_Acres = parseFloat($event.value);
      cu.Verf_AIP_Acres_touched = true;

      this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
      this.publishService.enableSync(Page.optimizer);
      return;
    }

    if ($event.column.colId == 'Verf_APH') {
      cu.Verf_Ins_APH = parseFloat($event.value);
      cu.Verf_Ins_APH_touched = true;

      this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
      this.publishService.enableSync(Page.optimizer);
      return;
    }

    if ($event.data.ID != undefined || $event.data.ID != null) {
      if ($event.oldValue != $event.value) {
        if ($event.value == '') {
          $event.value = 0;
        }

        if ($event.colDef.field == 'Aph') {
          cu.Ins_APH = parseFloat($event.value);
          cu.Verf_Ins_APH = 0;
          cu.Verf_Ins_APH_Status = false;
        } else if ($event.colDef.field == 'Acres') {
          if (!this.enableMaxNI) {
            let modified = this.localstorage.retrieve(environment.modifiedoptimizerdata);
            if (!modified) {
              modified = {};
            }

            modified['NI'] = this.loanmodel.LoanCropUnits.filter(a => a.Crop_Practice_Type_Code == 'NI').map(c => {
              return {
                ID: c.Loan_CU_ID,
                Acres: c.CU_Acres
              };
            });

            this.localstorage.store(environment.modifiedoptimizerdata, modified);
            this.enableMaxNI = true;
          }

          cu.CU_Acres = parseFloat($event.value);
          if (cu.Verf_AIP_Acres_Status == true || cu.Verf_FSA_Acres_Status == true) {
            cu.Verf_AIP_Acres_Status = false;
            cu.Verf_FSA_Acres_Status = false;
            cu.Verf_AIP_Acres = 0;
            cu.Verf_FSA_Acres = 0;

          }
        } else { cu.Rate_Yield = parseFloat($event.value); }

        cu.ActionStatus = 2;

        this.loanmodel.srccomponentedit = 'optimizercomponent';
        this.loanmodel.lasteditrowindex = $event.rowIndex;
        this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
        this.publishService.enableSync(Page.optimizer);

        if ($event.colDef.field == 'Acres') {
          this.setValidationErrorForOptimizerIRR();
          this.setValidationErrorForOptimizerNI();
        }

        //#region MODIFIED YELLOW VALUES

        let modifiedvalues = this.localstorage.retrieve(environment.modifiedbase) as Array<String>;

        if (!modifiedvalues.includes("OPTNI_" + $event.data.ID + "_" + $event.colDef.field)) {
          modifiedvalues.push("OPTNI_" + $event.data.ID + "_" + $event.colDef.field);
          this.localstorage.store(environment.modifiedbase, modifiedvalues);
        }

        //#endregion MODIFIED YELLOW VALUES
      }
    }
    setTimeout(() => {
      if (this.hastabbed) {
      this.gridApiNIR.startEditingCell({
        rowIndex:  this.tabbedrow,
        colKey:  "Acres"
    });
  }
   this.hastabbed = false;
    }, 500);
  }

  onGridReadyNIR(params) {
    this.gridApiNIR = params.api;
    this.columnApiNIR = params.columnApi;
    //setgriddefaults(this.gridApi, this.columnApi);
    //this.style.width = calculatecolumnwidths(this.columnApi) + 40 + "px";

    //params.api.sizeColumnsToFit();//autoresizing
    this.adjustToFitAgGridNIR();
    //Now for NIR
    this.optimizerverifyService.MyConstructor(this.gridApiIIR, this.columnApiIRR, this.columnApiNIR, this.gridApiNIR, this.dataService, this);
  }

  onGridReadyIRR(params) {
    this.gridApiIIR = params.api;
    this.columnApiIRR = params.columnApi;
    //setgriddefaults(this.gridApi, this.columnApi);
    //this.style.width = calculatecolumnwidths(this.columnApi) + 40 + "px";

    //params.api.sizeColumnsToFit();//autoresizing
    this.adjustToFitAgGridIRR();
    this.optimizerverifyService.MyConstructor(this.gridApiIIR, this.columnApiIRR, this.columnApiNIR, this.gridApiNIR, this.dataService, this);
  }
  //Grid Functions End

  get isLoanEditable() {
    return !this.loanmodel || this.loanmodel.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  /**
   * Common Export method for Irrigated and Non-Irrigated Crops
   */
  onExport(type = 'IRR') {
    let hasData = false;

    if (type == 'IRR') {
      hasData = this.rowDataNIR && this.rowDataNIR.length > 0;
    } else {
      hasData = this.rowDataIIR && this.rowDataIIR.length > 0;
    }

    // if (hasData) {
    //   this.alertify.confirm('Confirm', 'Do you want to include both Irrigation Practices?').subscribe(res => {
    //     if (res) {
    //       this.onExportBothTogether();
    //     } else {
    //       if (type == 'IRR') {
    //         this.onBtExportIRR();
    //       } else {
    //         this.onBtExportNIR();
    //       }
    //     }
    //   });
    // } else {
    //   if (type == 'IRR') {
    //     this.onBtExportIRR();
    //   } else {
    //     this.onBtExportNIR();
    //   }
    // }

    this.onExportBothTogether();
  }

  /**
   * Export Both Irrigated and Non Irrigated
   */
  onExportBothTogether() {
    let columnKeys = this.columnDefsNIR.filter(a => !a.supressExcel);
    columnKeys = columnKeys.map(a => a.field);

    const params = {
      skipHeader: false,
      columnGroups: true,
      skipFooters: false,
      skipGroups: false,
      skipPinnedTop: false,
      skipPinnedBottom: false,
      allColumns: true,
      exportMode: 'xlsx',
      fileName: `Combined_Irrigated_Crops_${this.loanmodel.Loan_Full_ID}_${this.dtss.getTimeStamp()}.xlsx`,
      sheetName: 'Sheet 1',
      columnKeys: columnKeys,
      suppressTextAsCDATA: true,
      shouldRowBeSkipped: params => {
        let p = params;
        return params.node.data.istotal || params.node.data.isAllTotal;
      },
      processCellCallback: params => {
        if (params.column.colId == 'Rent' || params.column.colId == 'CF' || params.column.colId == 'RC' || params.column.colId == 'ExcessIns') {
          return formatcurrency(params.value, 2);
        }
        return params.value;
      }
    };

    let iirdata = this.gridApiIIR.getDataAsExcel(params);
    params.skipHeader = true;
    let nirdata = this.gridApiNIR.getDataAsExcel(params);

    let parser = new DOMParser();
    let xmlDocIIR = parser.parseFromString(iirdata, "text/xml");
    let xmlDocNIR = parser.parseFromString(nirdata, "text/xml");

    //get Nodes for NIR
    let rows = Array.from(xmlDocNIR.getElementsByTagName('Row'));
    rows.forEach(element => {
      xmlDocIIR.getElementsByTagName('Table')[0].appendChild(element);
    });

    let oSerializer = new XMLSerializer();
    let sXML = oSerializer.serializeToString(xmlDocIIR);

    let workbook = XLSX.read(sXML, {type: 'string'});
    XLSX.writeFile(workbook, `Combined_Irrigated_Crops_${this.loanmodel.Loan_Full_ID}_${this.dtss.getTimeStamp()}.xlsx`);
  }


  /**
   * Export Irrigated
   */
  onBtExportNIR() {
    if (this.isLoanEditable && this.rowDataNIR && this.rowDataNIR.length > 0) {
      let columnKeys = this.columnDefsNIR.filter(a => !a.supressExcel);
      columnKeys = columnKeys.map(a => a.field);

      const params = {
        skipHeader: false,
        columnGroups: true,
        skipFooters: false,
        skipGroups: false,
        skipPinnedTop: false,
        skipPinnedBottom: false,
        allColumns: true,
        fileName: `Non_Irrigated_Crops_${this.loanmodel.Loan_Full_ID}_${this.dtss.getTimeStamp()}.xls`,
        sheetName: 'Sheet 1',
        columnKeys: columnKeys,
        shouldRowBeSkipped: params => {
          let p = params;
          return params.node.data.istotal || params.node.data.isAllTotal;
        },
        processCellCallback: params => {
          if (params.column.colId == 'Rent' || params.column.colId == 'CF' || params.column.colId == 'RC' || params.column.colId == 'ExcessIns') {
            return formatcurrency(params.value, 2);
          }
          return params.value;
        }
      };
      // Export Data to Excel file
      this.gridApiNIR.exportDataAsExcel(params);
    }
  }

  /**
   * Export Irrigated
   */
  onBtExportIRR() {
    if (this.isLoanEditable && this.rowDataIIR && this.rowDataIIR.length > 0) {
      let columnKeys = this.columnDefsIRR.filter(a => !a.supressExcel);
      columnKeys = columnKeys.map(a => a.field);

      const params = {
        skipHeader: false,
        columnGroups: true,
        skipFooters: false,
        skipGroups: false,
        skipPinnedTop: false,
        skipPinnedBottom: false,
        allColumns: true,
        fileName: `Irrigated_Crops_${this.loanmodel.Loan_Full_ID}_${this.dtss.getTimeStamp()}.xls`,
        sheetName: 'Sheet 1',
        columnKeys: columnKeys,
        shouldRowBeSkipped: params => {
          let p = params;
          return params.node.data.istotal || params.node.data.isAllTotal;
        },
        processCellCallback: params => {
          if (params.column.colId == 'Rent' || params.column.colId == 'CF' || params.column.colId == 'RC' || params.column.colId == 'ExcessIns') {
            return formatcurrency(params.value, 2);
          }
          return params.value;
        }
      };
      // Export Data to Excel file
      this.gridApiIIR.exportDataAsExcel(params);
    }
  }

  LoankeysIRR = ['Crop_Type', 'Practice', 'Section', 'Rated', 'Crop_Practice', 'Aph'];

  displayColumnsChangedIRR($event) {
    if (this.columnApiIRR != undefined && $event != undefined) {
      //first One to save to Loan Settings
      let LoanSettings: Loansettings = JSON.parse(this.loanmodel.LoanMaster.Loan_Settings);

      if (LoanSettings == undefined || LoanSettings == null) {
        LoanSettings = new Loansettings();
      }

      if (
        LoanSettings.Loan_key_Settings == undefined ||
        LoanSettings.Loan_key_Settings == null
      ) {
        LoanSettings.Loan_key_Settings = new Loan_Key_Visibilty();
      }
      //Since we dont have column thats changed so lets see manually
      this.LoankeysIRR.forEach(key => {
        let mainkey = '';
        switch (key) {
          case 'Rated':
            mainkey = 'Rated';
            break;
          case 'Section':
            mainkey = 'Section';
            break;
          case 'Practice':
            mainkey = 'Irr_Practice';
            break;
          case 'Crop_Practice':
            mainkey = 'Crop_Practice';
            break;
          case 'Aph':
            mainkey = 'APH';
            break;
          case 'Crop_Type':
            mainkey = 'Crop_Type';
            break;
        }

        let column = this.columnApiIRR.getColumn(key);
        LoanSettings.Loan_key_Settings[mainkey] = column.visible;
      });
      this.loanmodel.LoanMaster.Loan_Settings = JSON.stringify(LoanSettings);
      this.dataService.setLoanObjectwithoutsubscription(this.loanmodel);
      if (this.isLoanEditable) {
        this.publishService.enableSync(this.currentPageName);
      }
      //Second Execution
      this.adjustToFitAgGridIRR();
      this.getCurrentErrors();
    }
  }


  generatetotalsIIR() {
    const totaldata = this.rowDataIIR.filter(p => p.istotal == true);
    this.pinnedBottomRowData = [];
    let row: any = {};
    row.ID = null;
    row.Practice = formatacres(_.sum(totaldata.map(p => Number(p.Practice.toString().replace(/,/g, '')))).toFixed(1));
    row.Crop_Practice = '';
    row.StateCounty = "Total";

    const totalAcres = _.sumBy(totaldata, a => Number(a.Acres));
    //row.Prodpercentage = Helper.divide(_.sumBy(totaldata.map(a => Number(a.Prodpercentage) * Number(a.Acres))), totalAcres);

    row.Landlord = "";
    row.FSN = "";
    row.Section = "";
    row.Rated = "";
    row.Rent_perc = "";
    row.Rate_Yield = "";
    row.Perm_Insure = "";

    const nontotaldata = this.rowDataIIR.filter(a => !a.istotal);
    const nontotalacres = _.sumBy(nontotaldata, a => Number(a.Acres));
    row.Ins_Perc = Helper.divide(_.sumBy(nontotaldata.map(a => (parseFloat(a.Ins_Perc) || 0) * (parseFloat(a.Acres) || 0 ))), nontotalacres);

    row.Rent = "";
    row.Waiver = "";
    //row.Crop = formatacres(_.sum(this.loanmodel.Farms.map(p => p.Irr_Acres)).toFixed(1));
    row.Crop = "";
    row.CF = "";
    row.RC = "";
    row.ExcessIns = "";
    row.Acres = _.sum(totaldata.map(p => Number(p.Acres))).toFixed(1); //crop.CU_Acres;
    row.FC_Total_Used_Acres = 0; //farm.FC_Total_Used_Acres;
    row.Aph = "";
    row.Verf_AIP_Acres = "";
    row.Verf_AIP_Acres_Status = "";
    row.Verf_AIP_Acres_touched = "";

    row.Verf_FSA_Acres = "";
    row.Verf_FSA_Acres_Status = "";
    row.Verf_FSA_Acres_touched = "";
    row.Crop_Practice = 'Available';
    row.Verf_APH = "";
    row.Verf_APH_Status = "";
    row.Verf_APH_touched = "";
    row.isAllTotal = true;
    this.pinnedBottomRowData.push(row);
  }

  generatetotalsNI() {
    const totaldata = this.rowDataNIR.filter(p => p.istotal == true);
    this.pinnedBottomRowDataNI = [];

    let row: any = {};
    row.ID = null;
    row.Practice = formatacres(_.sum(totaldata.map(p => Number(p.Practice.toString().replace(/,/g, '')))).toFixed(1));
    row.Crop_Practice = '';
    row.StateCounty = "Total";
    const totalAcres = _.sumBy(totaldata, a => Number(a.Acres));
    row.Prodpercentage = Helper.divide(_.sumBy(totaldata.map(a => Number(a.Prodpercentage) * Number(a.Acres))), totalAcres);
    row.Landlord = "";
    row.FSN = "";
    row.Section = "";
    row.Rated = "";
    row.Rent_perc = "";
    row.Rate_Yield = "";
    row.Perm_Insure = "";

    const nontotaldata = this.rowDataNIR.filter(a => !a.istotal);
    const nontotalacres = _.sumBy(nontotaldata, a => Number(a.Acres));
    row.Ins_Perc = Helper.divide(_.sumBy(nontotaldata.map(a => (parseFloat(a.Ins_Perc) || 0) * (parseFloat(a.Acres) || 0 ))), nontotalacres);
    row.Rent = "";
    row.Waiver = "";
    // row.Crop = formatacres(_.sum(this.loanmodel.Farms.map(p => p.NI_Acres)).toFixed(1));
    row.Crop = "";
    row.CF = "";
    row.RC = "";
    row.ExcessIns = "";
    row.Acres = _.sum(totaldata.map(p => Number(p.Acres))).toFixed(1); //crop.CU_Acres;
    row.FC_Total_Used_Acres = 0; //farm.FC_Total_Used_Acres;
    row.Aph = "";
    row.Verf_AIP_Acres = "";
    row.Verf_AIP_Acres_Status = "";
    row.Verf_AIP_Acres_touched = "";

    row.Verf_FSA_Acres = "";
    row.Verf_FSA_Acres_Status = "";
    row.Verf_FSA_Acres_touched = "";
    row.Crop_Practice = 'Available';
    row.Verf_APH = "";
    row.Verf_APH_Status = "";
    row.Verf_APH_touched = "";
    row.isAllTotal = true;
    this.pinnedBottomRowDataNI.push(row);
  }
  LoankeysNIR = ['Crop_Type', 'Practice', 'Section', 'Rated', 'Crop_Practice', 'Aph'];

  displayColumnsChangedNIR($event) {

    if (this.columnApiNIR != undefined && $event != undefined) {
      //first One to save to Loan Settings
      let LoanSettings: Loansettings = JSON.parse(this.loanmodel.LoanMaster.Loan_Settings);

      if (LoanSettings == undefined || LoanSettings == null) {
        LoanSettings = new Loansettings();
      }

      if (
        LoanSettings.Loan_key_Settings == undefined ||
        LoanSettings.Loan_key_Settings == null
      ) {
        LoanSettings.Loan_key_Settings = new Loan_Key_Visibilty();
      }
      //Since we dont have column thats changed so lets see manually
      this.LoankeysNIR.forEach(key => {
        let mainkey = '';
        switch (key) {
          case 'Rated':
            mainkey = 'Rated';
            break;
          case 'Section':
            mainkey = 'Section';
            break;
          case 'Practice':
            mainkey = 'Irr_Practice';
            break;
          case 'Crop_Practice':
            mainkey = 'Crop_Practice';
            break;
          case 'Aph':
            mainkey = 'APH';
            break;
          case 'Crop_Type':
            mainkey = 'Crop_Type';
            break;
        }

        let column = this.columnApiNIR.getColumn(key);
        LoanSettings.Loan_key_Settings[mainkey] = column.visible;
      });
      this.loanmodel.LoanMaster.Loan_Settings = JSON.stringify(LoanSettings);
      //window.localStorage.setItem("ng2-webstorage|currentselectedloan", JSON.stringify(this.loanmodel));
      this.dataService.setLoanObjectwithoutsubscription(this.loanmodel);
      if (this.isLoanEditable) {
        this.publishService.enableSync(this.currentPageName);
      }
      //Second Execution
      this.adjustToFitAgGridNIR();
      this.getCurrentErrors();
    }
  }

  adjustToFitAgGridNIR() {
    if (this.columnApiNIR != undefined) {
      this.styleNIR.width = calculatecolumnwidths(this.columnApiNIR);
    }
  }

  adjustToFitAgGridIRR() {
    if (this.columnApiIRR != undefined) {
      this.styleIRR.width = calculatecolumnwidths(this.columnApiIRR);
    }
  }

  // displayedColumnsChanged(params){
  //   this.adjustToFitAgGridNIR();
  //   this.adjustToFitAgGridIRR();
  // }

  //_________________________________________________________________________________
  // MAX calculation
  maxCalculation(pCode: string, cropProperty = 'FC_Cashflow_Acre') {
    let modified = this.localstorage.retrieve(environment.modifiedoptimizerdata) as {};

    let isIRR = pCode == 'IRR';
    let refreshRequired = false;

    if (!modified) {
      modified = {};
    }

    this.loanmodel.Farms.filter(f => {
      if (isIRR) {
        return f.Irr_Acres > 0;
      }
      return f.NI_Acres > 0;
    }).forEach(farm => {
      const cropUnits = this.loanmodel.LoanCropUnits.filter(c => c.Crop_Practice_Type_Code && c.Crop_Practice_Type_Code.toUpperCase() == pCode && c.Farm_ID == farm.Farm_ID);

      let maxValueCropUnit = _.maxBy(cropUnits, c => c[cropProperty]);

      if (!maxValueCropUnit) {
        return;
      }

      try {
        if (isIRR) {
          if (!this.enableMaxIRR) {
            this.enableMaxIRR = true;
            modified['IRR'] = this.rowDataIIR;
          }

          maxValueCropUnit.CU_Acres = farm.Irr_Acres;
        } else {
          if (!this.enableMaxNI) {
            this.enableMaxNI = true;
            modified['NI'] = this.rowDataNIR;
          }

          maxValueCropUnit.CU_Acres = farm.NI_Acres;
        }
        maxValueCropUnit.ActionStatus = 2;
        refreshRequired = true;

        cropUnits.forEach(lcu => {
          if (lcu.Crop_Practice_ID != maxValueCropUnit.Crop_Practice_ID) {
            lcu.CU_Acres = 0;
            lcu.ActionStatus = 2;
          }
        });
      } catch { }
    });

    if (refreshRequired) {
      this.localstorage.store(environment.modifiedoptimizerdata, modified);

      this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
      this.publishService.enableSync(Page.optimizer);

      this.getInitialValidation();
      this.getCurrentErrors();
    }
  }

  undoMaxCalculation(type: string = 'IRR') {
    let modified = this.localstorage.retrieve(environment.modifiedoptimizerdata);
    const rowData = modified[type];

    if (rowData) {
      try {
        rowData.forEach(cu => {
          if (!cu.istotal) {
            const cropUnit = this.loanmodel.LoanCropUnits.find(l => l.Loan_CU_ID == cu.ID);
            cropUnit.CU_Acres = cu.Acres;
            cropUnit.ActionStatus = 2;
          }
        });

        modified[type] = null;
        this[`enableMax${type}`] = false;

        this.localstorage.store(environment.modifiedoptimizerdata, modified);
        this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
        this.publishService.enableSync(Page.optimizer);
      } catch {
        modified[type] = rowData;
        this[`enableMax${type}`] = true;
        this.localstorage.store(environment.modifiedoptimizerdata, modified);
      }
    }

    this.getInitialValidation();
    this.getCurrentErrors();
  }

  private enableDisableUndoMaxButtons() {
    let modified = this.localstorage.retrieve(environment.modifiedoptimizerdata) as {};
    if (modified) {
      if (modified['IRR']) {
        this.enableMaxIRR = true;
      } else {
        this.enableMaxIRR = false;
      }

      if (modified['NI']) {
        this.enableMaxNI = true;
      } else {
        this.enableMaxNI = false;
      }
    } else {
      this.enableMaxIRR = false;
      this.enableMaxNI = false;
    }
  }

  //VERIFICATION CALLS TO SERVICE
  verifyIIRClicked() {
    this.optimizerverifyService.verifyIIRClicked(this.rowDataIIR, this.dialog, this.loanmodel );
  }

  verifyNIRClicked() {
    this.optimizerverifyService.verifyNIRClicked(this.rowDataNIR, this.dialog, this.loanmodel);
  }

  resetVerification() {
    this.optimizerverifyService.resetVerification();
  }

  verifyIIRAPH() {
    this.optimizerverifyService.verifyIIRAPH(this.rowDataIIR, this.loanmodel);
  }

  verifyNIRAPH() {
    this.optimizerverifyService.verifyNIRAPH(this.rowDataNIR, this.loanmodel);
  }
  //

  //Loan Master Saving two Checkbox
  updateAIPVerified(checked: boolean) {
    this.loanmodel.LoanMaster.AIP_Verified = checked;
    this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
  }

  updateAcresMapped(checked: boolean) {
    this.loanmodel.LoanMaster.Acres_Mapped = checked;
    this.loancalculationservice.performcalculationonloanobject(this.loanmodel, true);
  }

  //#region Validation

  setValidationErrorForOptimizerIRR() {
    if (environment.isDebugModeActive) { console.time('ValidationErrorForOptimizerIRR'); }

    this.validationservice.validateTableFields<Loan_Crop_Unit>(
      this.rowDataIIR,
      this.columnDefsIRR,
      this.currentPageName,
      `${TableId.OPT}${'IRR'}_`,
      `OPTIR`,
      'ID'
    );

    if (environment.isDebugModeActive) { console.timeEnd('ValidationErrorForOptimizerIRR'); }
  }

  getCurrentErrorsIRR() {
    let currenterrorsIRR = (this.localstorage.retrieve(
      environment.errorbase
    ) as Array<errormodel>).filter(p => p.chevron == 'OPTIR');

    this.validationservice.highlighErrorCells(currenterrorsIRR, `Optimizer_IRR`);
  }

  setValidationErrorForOptimizerNI() {
    if (environment.isDebugModeActive) { console.time('ValidationErrorForOptimizerNI'); }

    this.validationservice.validateTableFields<Loan_Crop_Unit>(
      this.rowDataNIR,
      this.columnDefsNIR,
      this.currentPageName,
      `${TableId.OPT}${'NI'}_`,
      `OPTNI`,
      'ID'
    );

    if (environment.isDebugModeActive) { console.timeEnd('ValidationErrorForOptimizerNI'); }
  }

  getCurrentErrorsNI() {
    let currenterrorsNIR = (this.localstorage.retrieve(
      environment.errorbase
    ) as Array<errormodel>).filter(p => p.chevron == 'OPTNI');

    this.validationservice.highlighErrorCells(currenterrorsNIR, `Optimizer_NI`);
  }

  //#endregion Validation
}
