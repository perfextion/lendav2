import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { LoggingService } from '../../services/Logs/logging.service';
import { LoancalculationWorker } from '../../Workers/calculations/loancalculationworker';
import { LoanApiService } from '../../services/loan/loanapi.service';
import { ToasterService } from '../../services/toaster.service';
import { loan_model } from '../../models/loanmodel';
import { JsonConvert } from 'json2typescript';
import { environment } from '@env/environment.prod';

@Injectable()
export class OptimizerService {

  constructor(
    public localstorageservice: LocalStorageService,
    public logging: LoggingService,
    public loanserviceworker: LoancalculationWorker,
    public loanapi: LoanApiService,
    public toasterService: ToasterService
  ) { }

  syncToDb(localloanobject: loan_model) {
    // Loan Condition Calculation
    localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);

    this.loanapi.syncloanobject(localloanobject).subscribe(res => {
      this.localstorageservice.store(environment.modifiedbase, []);

      if (res.ResCode == 1) {
        this.loanapi.getLoanById(localloanobject.Loan_Full_ID).subscribe(res1 => {
          this.logging.checkandcreatelog(3, 'Overview', "APi LOAN GET with Response " + res1.ResCode);
          if (res1.ResCode == 1) {
            this.toasterService.success("Records Synced");
            let jsonConvert: JsonConvert = new JsonConvert();
            this.loanserviceworker.performcalculationonloanobject(jsonConvert.deserialize(res1.Data, loan_model));
            this.localstorageservice.store(environment.modifiedoptimizerdata, null);
            this.loanserviceworker.saveValidationsToLocalStorage(res1.Data);
          }
          else {
            this.toasterService.error("Could not fetch Loan Object from API")
          }
        });
      }
      else {
        this.toasterService.error(res.Message || "Error in Sync");
      }
    });

  }

}
