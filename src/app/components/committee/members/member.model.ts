export class GetAllUsers {
  UserID: number;
  Username: string;
  FirstName: string;
  LastName: string;
  Job_Title_ID: number;
  Job_Title: string;
  Job_Title_Code: string;
}

export class AddNotifyPartyData {
  users: Array<GetAllUsers>;
  title: string;
}
