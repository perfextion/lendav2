import { Component, OnInit, OnDestroy, OnChanges, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Observable, of } from 'rxjs';
import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';
import { JsonConvert } from 'json2typescript';

import {
  loan_model,
  Loan_Committee,
  LoanGroup,
  LoanStatus,
  Committee_Meetings
} from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import {
  cellclassmaker,
  CellType,
  setgriddefaults,
  calculatecolumnwidths,
  IParam
} from '@lenda/aggriddefinations/aggridoptions';
import { Loansettings } from '@lenda/models/loansettings';
import { errormodel } from '@lenda/models/commonmodels';
import { SelectedCommitteeStatus } from '@lenda/models/committee-status/committee-status.model';
import { LoanMaster, RefDataModel, RefCommitteeLevel } from '@lenda/models/ref-data-model';
import { CommitteeStatusMock } from '@lenda/models/committee-status/mock-committee-status';

import { VoteButtonColumnRenderer } from '@lenda/aggridcolumns/vote-button-column/vote-button-column.component';
import { DeleteNotifyPartyButtonRenderer } from '@lenda/aggridcolumns/delete-notify-party/delete-notify-party-column';

import { PublishService, Sync } from '@lenda/services/publish.service';
import { ValidationErrorsCountService } from '@lenda/Workers/calculations/validationerrorscount.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { MembersService } from './members.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { formatDateValue } from '@lenda/Workers/utility/aggrid/dateboxes';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { ToasterService } from '@lenda/services/toaster.service';
import { DataService } from '@lenda/services/data.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';

export type IMemberParams = IParam<MembersComponent, Loan_Committee>;

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit, OnDestroy, OnChanges {
  private subscription: ISubscription;
  private errorsubscription: ISubscription;

  public refdata: RefDataModel = <RefDataModel>{};
  public isAdding: boolean = false;
  public isgriddirty: boolean;
  public indexsedit = [];
  public columnDefs = [];
  private localloanobject: loan_model = new loan_model();
  public loggedInUserId: number;
  public isAdmin: boolean;
  private errors: errormodel[];

  public style: any = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };

  //#region Aggrid
  public rowData: Array<Loan_Committee> = [];
  public components;
  public context;
  public frameworkcomponents;
  public editType;
  private gridApi;
  private columnApi: any;

  public isCrossCollaterized: boolean;
  public loanGroups: Array<LoanGroup>;

  // vote status
  public approved: boolean;
  public declined: boolean;

  public syncRequired: Sync[] = [];
  //#endregion Aggrid

  recommendButtonClicked: boolean;
  loanStatus: string;
  LoanStatus: typeof LoanStatus = LoanStatus;

  @Input() expanded: boolean = true;

  private valiationerrors;

  constructor(
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public alertify: AlertifyService,
    private loanApi: LoanApiService,
    private toaster: ToasterService,
    private dataService: DataService,
    public logging: LoggingService,
    private memberService: MembersService,
    private dialog: MatDialog,
    private publishservice: PublishService,
    private validationCount: ValidationErrorsCountService
  ) {
    this.frameworkcomponents = {
      votebutton: VoteButtonColumnRenderer,
      deletecolumn: DeleteNotifyPartyButtonRenderer
     };
    this.components = {};

    this.refdata = this.localstorageservice.retrieve(
      environment.referencedatakey
    );

    this.loggedInUserId = this.localstorageservice.retrieve(environment.uid);
    // this.loggedInUserId = 64;

    try {
      this.isAdmin = this.localstorageservice.retrieve('isadmin') == 1;
    } catch {
      this.isAdmin = false;
    }

    this.localloanobject = this.localstorageservice.retrieve(
      environment.loankey
    );
    this.loanStatus = this.localloanobject.LoanMaster.Loan_Status;
    this.errors = this.localstorageservice.retrieve(environment.errorbase) as errormodel[];
    this.localstorageservice.observe(environment.errorbase).subscribe((res: errormodel[]) => {
      this.errors = res;
    });


    this.valiationerrors = this.validationCount.getValidationErrors();

    this.errorsubscription = this.validationCount.latestValidationCount$.subscribe(valiationerrors => {
      this.valiationerrors = valiationerrors;
    });
  }

  ngOnInit() {
    this.prepareColDefs();
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res) {
        this.localloanobject = res;
        if (this.localloanobject.LoanCommittees) {
          if (this.localloanobject.srccomponentedit == 'LoanCommittee') {
            this.rowData[
              this.localloanobject.lasteditrowindex
            ] = this.localloanobject.LoanCommittees.filter(
              p => p.ActionStatus != 3
            )[this.localloanobject.lasteditrowindex];
          } else {
            this.rowData = this.localloanobject.LoanCommittees.filter(
              p => p.ActionStatus != 3
            );
          }
        }
        this.setApprovedDeclinedStatus();
        this.errors = this.localstorageservice.retrieve(environment.errorbase) as errormodel[];
      }
    });

    this.setCrossCollateralStatus();
    this.setApprovedDeclinedStatus();

    this.publishservice.listenToSyncRequired().subscribe(res => {
      this.syncRequired = res;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    if(this.errorsubscription) {
      this.errorsubscription.unsubscribe();
    }
  }

  ngOnChanges() {
    this.prepareColDefs();
  }

  prepareColDefs() {
    this.columnDefs = [
      {
        headerName: 'Status',
        field: 'CM_Role',
        editable: false,
        cellClass: cellclassmaker(CellType.Text, false),
        cellEditor: 'alphaNumericCellEditor',
        valueFormatter: (params: IMemberParams) => {
          if (params.data.CM_Role == 1) {
            // Voting Member
            if (params.data.Vote == 1) {
              return 'Approved';
            } else if (params.data.Vote == 2) {
              return 'Declined';
            } else {
              return 'Pending';
            }
          } else {
            return 'Notify Party';
          }
        },
        width: 120,
        maxWidth: 120
      },
      {
        headerName: 'User',
        field: 'Username',
        editable: false,
        cellClass: cellclassmaker(CellType.Text, false),
        cellEditor: 'alphaNumericCellEditor',
        valueFormatter: (params: IMemberParams) => {
          if(params.data.FirstName || params.data.LastName) {
            let name = params.data.LastName;
            if(name && name.length > 0) {
              name += ', ';
            }

            name += params.data.FirstName;

            return name;
          }
          return params.data.Username;
        },
        width: 180,
        maxWidth: 180
      },
      {
        headerName: 'User Role',
        field: 'Pool_Set_Name',
        editable: false,
        tooltip: (params: IMemberParams) => params.data.Job_Title,
        cellClass: cellclassmaker(CellType.Text, false),
        cellEditor: 'alphaNumericCellEditor',
        width: 180,
        maxWidth: 180,
        valueFormatter: (params: IMemberParams) => {
          return params.data.Pool_Set_Name || params.data.Job_Title;
        }
      },
      {
        headerName: 'Add Date',
        field: 'Added_Date',
        editable: false,
        cellClass: cellclassmaker(CellType.Text, false),
        cellEditor: 'alphaNumericCellEditor',
        valueFormatter: formatDateValue,
        width: 120,
        maxWidth: 120
      },
      {
        headerName: 'Vote Date',
        field: 'Voted_Date',
        editable: false,
        cellClass: cellclassmaker(CellType.Text, false),
        cellEditor: 'alphaNumericCellEditor',
        valueFormatter: formatDateValue,
        width: 120,
        maxWidth: 120
      },
      {
        headerName: 'Action',
        field: 'value',
        cellRenderer: 'votebutton',
        loggedInUser: this.loggedInUserId,
        isAdmin: this.isAdmin,
        width: 160,
        maxWidth: 170
      },
      {
        headerName: '',
        field: 'value',
        cellRenderer: 'deletecolumn',
        minWidth: 60, width: 60, maxWidth: 60,
        suppressToolPanel: true
      }
    ];

    ///
    this.context = { componentParent: this };

    this.getdataforgrid();
    this.editType = '';
  }

  public onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    setgriddefaults(this.gridApi, this.columnApi);
    params.api.sizeColumnsToFit();
    this.adjustToFitAgGrid();
  }

  public getdataforgrid() {
    if (this.localloanobject != null && this.localloanobject != undefined) {
      if (this.localloanobject.LoanCommittees)
        this.rowData = this.localloanobject.LoanCommittees.filter(
          p => p.ActionStatus != 3
        );
      else {
        this.rowData = [];
      }
    }
  }

  public adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  public displayedColumnsChanged(params) {
    this.adjustToFitAgGrid();
  }

  public emojiClicked(data: Loan_Committee, isCross: boolean) {
    this.alertify.openEmojiDialog('Vote by Emoji').subscribe(res => {
      if (res) {
        if(res.committeeStatusId == 2) {
          this.declineVoteAlert().subscribe(r => {
            if(r){
              this.recordVote(res, data, isCross);
            }
          });
        } else {
          this.recordVote(res, data, isCross);
        }
      }
    });
  }

  public recordVote(
    selectedCommitteStatus: SelectedCommitteeStatus,
    data: Loan_Committee,
    isCross: boolean
  ) {
    try {
      let committeeStatus = CommitteeStatusMock.find(
        s => s.id == selectedCommitteStatus.committeeStatusId
      );

      let innerCommitteeStatus = committeeStatus.innerStatus.find(
        i => i.id == selectedCommitteStatus.innerCommitteeStatusId
      );

      let crossColLoans: Array<string> = this.getCrossColLoanIds(isCross);

      let voteLoans = crossColLoans.map(loan_Id => {
        return this.memberService.voteLoan(
          loan_Id,
          data.User_ID,
          selectedCommitteStatus.committeeStatusId,
          selectedCommitteStatus.innerCommitteeStatusId,
          '',
          innerCommitteeStatus.title
        );
      });

      Observable.forkJoin(voteLoans).subscribe(
        res => {
          if (res[res.length - 1].ResCode == 1) {
            this.toaster.success('Successfully Recorded Vote');
            this.getLoanById();
          } else {
            this.toaster.error('Error occured while recording voting');
          }
        },
        error => {
          this.toaster.error('Error occured while recording voting');
        }
      );
    } catch {
      this.toaster.error('Error occured while recording voting');
    }
  }

  public rescindVote(
    committeeMemberId: number,
    UserId: number,
    isCrossCollaterized: boolean
  ) {
    try {
      let crossColLoans: Array<string> = this.getCrossColLoanIds(
        isCrossCollaterized
      );

      let recindVotes = crossColLoans.map(loan_Id => {
        return this.memberService.rescindVote(
          loan_Id,
          committeeMemberId,
          UserId,
          this.loggedInUserId,
          '',
          'Vote Rescinded'
        );
      });

      Observable.forkJoin(recindVotes).subscribe(
        res => {
          if (res[res.length - 1].ResCode == 1) {
            this.toaster.success('Successfully Rescinded Vote.');
            this.getLoanById();
          } else {
            this.toaster.error('Error occured while rescinding voting');
          }
        },
        error => {
          this.toaster.error('Error occured while rescinding voting');
        }
      );
    } catch {
      this.toaster.error('Error occured while rescinding voting');
    }
  }

  public getLoanById() {
    this.localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(this.localloanobject);
    this.loanApi
      .getLoanById(this.localloanobject.Loan_Full_ID)
      .subscribe(res => {
        this.logging.checkandcreatelog(
          3,
          'Overview',
          'APi LOAN GET with Response ' + res.ResCode
        );
        if (res.ResCode == 1) {
          this.toaster.success('Records Synced');
          let jsonConvert: JsonConvert = new JsonConvert();
          this.loanserviceworker.performcalculationonloanobject(
            jsonConvert.deserialize(res.Data, loan_model)
          );
          this.loanserviceworker.saveValidationsToLocalStorage(res.Data);
        } else {
          this.toaster.error('Could not fetch Loan Object from API');
        }
      });
  }

  public addrow() {
    this.loanApi.getAllUsers().subscribe(res => {
      this.logging.checkandcreatelog(
        3,
        'Overview',
        'APi USER GET with Response ' + res.ResCode
      );
      if (res.ResCode == 1) {
        let data = res.Data;
        data = data.filter(a => !this.localloanobject.LoanCommittees.some(A => A.User_ID == a.UserID));

        this.alertify
          .addNotifyParty('Add Notified Party', data)
          .subscribe(result => {
            if (result) {
              let user = {
                UserId: result.UserId,
                Loan_Full_ID: this.localloanobject.Loan_Full_ID,
                JobTitleId: result.JobTitleId
              };
              this.loanApi.addNotifyParty(user).subscribe(dat => {
                if (dat.ResCode == 1) {
                  this.localloanobject.LoanCommittees = dat.Data.Committee;
                  this.localloanobject.Meetings = dat.Data.Meetings;
                  // Save to Local Storage
                  this.dataService.setLoanObject(this.localloanobject);
                } else if (dat.ResCode == 0) {
                  this.toaster.error(dat.Message);
                }
              });
            }
          });
      } else {
        this.toaster.error('Could not fetch Users from API');
      }
    });
  }

  public DeleteNotifyParty(CM_ID: any): any {
    this.alertify
    .confirm('Confirm', 'Are you sure you want to delete notify party?')
    .subscribe(res => {
      if(res) {
        try {
          const notifyParty = {
            CM_ID: CM_ID,
            Loan_Full_ID: this.localloanobject.Loan_Full_ID
          };

          this.loanApi.removeNotifyParty(notifyParty).subscribe(dat => {
            if (dat.ResCode == 1) {
              this.localloanobject.LoanCommittees = dat.Data;
              this.localloanobject.Meetings = this.Get_Meetings(this.localloanobject.LoanCommittees);
              this.dataService.setLoanObject(this.localloanobject);
            } else if (dat.ResCode == 0) {
              this.toaster.error(dat.Message);
            }
          });
        } catch {
          this.toaster.error('Could not delete Notify Party from API');
        }
      }
    }, error => {
      this.toaster.error(error.ExceptionMessage);
    });
  }

  private Get_Meetings(committee: Array<Loan_Committee>) {
    let Meetings: Array<Committee_Meetings> = [];

    try {
      committee.forEach(c => {
        let m = <Committee_Meetings>{
          emailAddress:  {
            address: c.Email,
            name: `${c.LastName}, ${c.FirstName}`
          },
          type: 'Required'
        };
        Meetings.push(m);
      });
    } catch {
      Meetings = [];
    }

    return Meetings;
  }

  public setCrossCollateralStatus() {
    this.loanGroups = this.localstorageservice.retrieve(environment.loanGroup);
    // Need to observe as otherwise sometimes loangroup is not available
    this.localstorageservice.observe(environment.loanGroup).subscribe(res => {
      this.loanGroups = res;
      if (this.loanGroups) {
        this.isCrossCollaterized = this.loanGroups.some(
          lg => lg.IsCrossCollateralized
        );
      } else {
        this.isCrossCollaterized = false;
      }
    });

    if (this.loanGroups) {
      this.isCrossCollaterized = this.loanGroups.some(
        lg => lg.IsCrossCollateralized
      );
    } else {
      this.isCrossCollaterized = false;
    }
  }

  private getCrossColLoanIds(isCross: boolean) {
    let crossColLoans: Array<string> = [];

    if (isCross) {
      crossColLoans = this.loanGroups
        .filter(a => a.IsCrossCollateralized)
        .map(a => a.Loan_Full_ID);
    }

    crossColLoans.push(this.localloanobject.Loan_Full_ID);
    return crossColLoans;
  }

  public get isLoanInVotingCondition() {
    if (this.localloanobject) {
      let master: LoanMaster = this.localloanobject.LoanMaster;
      return (
        master.Loan_Status == 'R' ||
        master.Loan_Status == 'A' ||
        master.Loan_Status == 'Z'
      );
    }
    return false;
  }

  // add notify party
  public get canAddNotifyParty() {
    try {
      if(this.localloanobject){
        return (
          this.localloanobject.LoanMaster.Loan_Status == 'W' ||
          this.localloanobject.LoanMaster.Loan_Status == 'R'
        );
      }
      return false;
    } catch {
      return false;
    }
  }

  // recommend button

  public get isAllAnswered() {
    return this.localloanobject.Loan_Exceptions.every(
      a => !!a.Mitigation_Ind && !!a.Mitigation_Text
    );
  }

  public onSubmitClick() {
    if (this.loanStatus == LoanStatus.Working) {
      this.confirmSubmit();
    }
  }

  private confirmSubmit() {
    if (this.recommendButtonClicked) {
      return;
    }

    if(this.syncRequired && this.syncRequired.length > 0){
      this.alertify
      .confirm('Confirm', 'You have unsaved changes. Do you want to save and Proceed?')
      .subscribe(r => {
        if(r){
          this.loanApi.syncloanobject(this.localloanobject).subscribe(res => {
            if(res.ResCode == 1) {
              this.publishservice.syncCompleted();
              this._confirmRecommendLoan();
            } else {
              this.toaster.error('Loan Sync failed.');
            }
          }, _ => {
            this.toaster.error('Loan Sync failed.');
          });
        }
      });
    } else {
      this._confirmRecommendLoan();
    }
  }

  private _confirmRecommendLoan() {
    let errorsCount = this.getValidationErrors();

    this.alertify.openRecommendLoanDialog(
      errorsCount.validationCount,
      errorsCount.exceptionsCount
    ).subscribe(res => {
      if(res) {
        this.submitLoan();
      }
    });
  }

  private submitLoan() {

    if (this.localloanobject && this.localloanobject.Loan_Full_ID) {
      this.recommendButtonClicked = true;
      let committeeStatus = CommitteeStatusMock.find(
        s => s.id == 4
      );

      let innerCommitteeStatus = committeeStatus.innerStatus.find(
        i => i.id == 0
      );

      // this.loanApi.submitLoan(this.localloanobject.Loan_Full_ID, innerCommitteeStatus.title)
      this.loanApi.recommendLoan(
        this.localloanobject.Loan_Full_ID,
        this.localloanobject.LoanMaster.Crop_Year,
        innerCommitteeStatus.title,
        this.localloanobject.LoanMaster.Loan_Officer_ID
      )
      .subscribe(
        res => {
          if (res.ResCode == 1) {
            this.loanApi
              .getLoanById(this.localloanobject.Loan_Full_ID)
              .subscribe(res => {
                this.logging.checkandcreatelog(
                  3,
                  'Overview',
                  'APi LOAN GET with Response ' + res.ResCode
                );
                if (res.ResCode == 1) {
                  this.toaster.success('Loan submitted successfully');
                  let jsonConvert: JsonConvert = new JsonConvert();
                  this.loanserviceworker.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model), true, true);
                  this.loanserviceworker.saveValidationsToLocalStorage(res.Data);
                } else {
                  this.toaster.error('Could not fetch Loan Object from API');
                }
              });
          } else {
            this.toaster.error(res.Message);
            this.recommendButtonClicked = false;
          }
        },
        error => {
          this.recommendButtonClicked = false;
          this.toaster.error('Loan Recommendation failed.');
        }
      );
    }
  }

  public declineVoteAlert() {
    if(this.showDeclineAlert()){
      return this.alertify
      .confirm(
        'Confirm',
        'Loan will be denined based on your vote. Would you like to proceed?'
      );
    }

    return of(true);
  }

  public setApprovedDeclinedStatus() {
    let master: LoanMaster = this.localloanobject.LoanMaster;

    if(master.Loan_Status == LoanStatus.Approved){
      this.approved = true;
      return;
    }

    if(master.Loan_Status == LoanStatus.Declined){
      this.declined = true;
      return;
    }

    let committeeLevel = this.getCommitteeLevel(master);
    if(committeeLevel){
      let minimumApprovalPerc = committeeLevel.Approval / 100;

      let approvedVotePerc = this.getApprovalPerc();
      if(approvedVotePerc >= minimumApprovalPerc) {
        this.approved = true;
      }

      let declinedVotePerc = this.getDeclinePerc();
      if(declinedVotePerc > ( 1 - minimumApprovalPerc)) {
        this.declined = true;
      }
    }
  }

  private getApprovalPerc() {
    let committee = this.localloanobject.LoanCommittees.filter(a => a.CM_Role == 1);
    let approvedVote = committee.filter(a => !!a.Voted_Date && a.Vote == 1);
    let approvedVotePerc = approvedVote.length / committee.length;
    return approvedVotePerc;
  }

  private getDeclinePerc(newVote = 0) {
    let committee = this.localloanobject.LoanCommittees.filter(a => a.CM_Role == 1);
    let declinedVote = committee.filter(a => a.CM_Role == 1 && !!a.Voted_Date && a.Vote == 2);
    let declinedVotePerc = (declinedVote.length + newVote) / committee.length;
    return declinedVotePerc;
  }

  private showDeclineAlert() {
    let master: LoanMaster = this.localloanobject.LoanMaster;

    let committeeLevel = this.getCommitteeLevel(master);
    if(committeeLevel){
      let minimumApprovalPerc = committeeLevel.Approval / 100;

      let declinedVotePerc = this.getDeclinePerc(1);
      if(declinedVotePerc > ( 1 - minimumApprovalPerc)) {
        return true;
      }
    }
    return false;
  }

  private getCommitteeLevel(master: LoanMaster) {
    let loanSettings: Loansettings;
    try {
      loanSettings = JSON.parse(master.Loan_Settings);
    }
    catch {
      loanSettings = new Loansettings();
    }

    try {
      let committeeLevel = this.refdata.CommitteeLevels.find(l => l.Level_Code == loanSettings.committeeLevel);
      return committeeLevel;
    } catch {
      return <RefCommitteeLevel>{};
    }
  }

  /**
   * Check if no Validation Errors
   */
  public getValidationErrors(){
    let validationCount = 0;
    let exceptionsCount = 0;

   try {
    if(this.valiationerrors) {
      Object.keys(this.valiationerrors).forEach(tab => {
        exceptionsCount += this.valiationerrors[tab].exceptions.level2;
        validationCount += this.valiationerrors[tab].validations.level2;
      });
     }
   } catch {
     exceptionsCount = 0;
     validationCount = 0;
   }

    return {
      validationCount: validationCount,
      exceptionsCount: exceptionsCount
    };
  }
}


export class AddNotifyPartyResponse {
  Committee: Array<Loan_Committee>;
  Meetings: Array<Committee_Meetings>;
}
