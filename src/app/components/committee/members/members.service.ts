import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { ApiService } from '@lenda/services';

import { ResponseModel } from '@lenda/models/resmodels/reponse.model';
import {
  RecordVote,
  RescindVote
} from '@lenda/models/committee-status/committee-status.model';

@Injectable({
  providedIn: 'root'
})
export class MembersService {
  constructor(private apiservice: ApiService) {}

  voteLoan(
    loanFullID: string,
    userId: number,
    vote: number,
    voteType: number,
    voteString: string,
    systemMessage: string = null
  ): Observable<ResponseModel> {
    if (loanFullID) {
      let body = <RecordVote>{
        Loan_Full_ID: loanFullID,
        User_ID: userId,
        Vote: vote,
        VoteType: voteType,
        Comment: voteString,
        System_Comment: systemMessage
      };

      const route = '/api/Loan/VoteLoan';
      return this.apiservice.post(route, body).map(res => res);
    }
  }

  rescindVote(
    loanFullID: string,
    committeeMemberId: number,
    userId: number,
    loggedInUserId: number,
    comment = '',
    systemMessage: string = null
  ): Observable<ResponseModel> {
    if (!!loanFullID && userId == loggedInUserId) {
      const body = <RescindVote>{
        CM_ID: committeeMemberId,
        Comment: comment,
        Loan_Full_ID: loanFullID,
        LoggedIn_UserId: loggedInUserId,
        User_ID: userId,
        System_Comment: systemMessage
      };

      const route = '/api/Loan/RescindVote';
      return this.apiservice.post(route, body).map(res => res);
    } else {
      return Observable.of(<ResponseModel>{
        Data: null,
        Message: 'Rescinding vote unsuccessful.',
        ResCode: 0
      });
    }
  }
}
