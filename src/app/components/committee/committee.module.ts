import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';
import { AgGridModule } from 'ag-grid-angular';

import { CommitteeComponent } from './committee.component';
import { MembersComponent } from './members/members.component';
import { LoanValidationsComponent } from './loan-validations/loan-validations.component';
import { CommitteeExceptionsComponent } from './exceptions/exceptions.component';
import { ExceptionItemComponent } from './exceptions/exception-item/exception-item.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    UiComponentsModule,
    AgGridModule
  ],
  declarations: [
    CommitteeComponent,
    MembersComponent,
    LoanValidationsComponent,
    CommitteeExceptionsComponent,
    ExceptionItemComponent
  ],
  exports: [CommitteeComponent]
})
export class CommitteeModule {}
