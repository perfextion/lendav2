import {
  Component,
  OnInit,
  OnDestroy,
  ViewEncapsulation,
  AfterViewInit,
  ViewChild
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatExpansionPanel } from '@angular/material';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import * as moment from 'moment';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';
import { loan_model, LoanStatus } from '@lenda/models/loanmodel';
import { Page } from '@lenda/models/page.enum';
import {
  percentageFormatterValue,
  daysDiff
} from '@lenda/aggridformatters/valueformatters';
import { RefDataModel } from '@lenda/models/ref-data-model';

import { RefDataService } from '@lenda/services/ref-data.service';
import { errormodel } from '@lenda/models/commonmodels';
import { MasterService } from '@lenda/master/master.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { CommitteeService } from './committee.service';
import { DataService } from '@lenda/services/data.service';
import { PublishService } from '@lenda/services/publish.service';

@Component({
  selector: 'app-committee',
  templateUrl: './committee.component.html',
  styleUrls: ['./committee.component.scss'],
  providers: [CommitteeService],
  encapsulation: ViewEncapsulation.None
})
export class CommitteeComponent implements OnInit, OnDestroy, AfterViewInit {
  private subscription: ISubscription;

  public localloanobj: loan_model;
  public isFormUpdated: boolean = false;
  public currentPageName: Page = Page.committee;
  public loanComment: string;

  private refdata: RefDataModel;

  @ViewChild('exceptionPanel') MatExpansionPanel: MatExpansionPanel;

  formatValue = (val: any) => {
    return percentageFormatterValue(val || 0, 2);
  }

  constructor(
    private localstorageservice: LocalStorageService,
    private loanserviceworker: LoancalculationWorker,
    private publishService: PublishService,
    private committeeService: CommitteeService,
    private dataService: DataService,
    private validationService: ValidationService,
    private refdataservice: RefDataService,
    public masterSvc: MasterService,
    private activatedRoute: ActivatedRoute
  ) {
    this.refdata = this.localstorageservice.retrieve(
      environment.referencedatakey
    );

    this.localloanobj = this.localstorageservice.retrieve(environment.loankey);
    this.loanComment = this.localloanobj.LoanMaster.Loan_Comment;
  }

  public get showErrorMessage() {
    if(this.localloanobj) {
      let lm = this.localloanobj.LoanMaster;
      if(lm) {
        return lm.FC_Invalid_Service || lm.FC_Invalid_Origination || lm.FC_Invalid_ARM_Fee || lm.FC_Invalid_Dist_Fee;
      }
      return false;
    }
    return false;
  }

  public getsum() {
    return (this.localloanobj.LoanMaster.ARM_Rate_Fee || 0 ) + (this.localloanobj.LoanMaster.Dist_Rate_Fee || 0 );
  }

  public getoriginatingCalculation() {
    return this.localloanobj.LoanMaster.Origination_Fee_Amount || 0;
  }

  public getservicefeeCalculation() {
    return this.localloanobj.LoanMaster.Service_Fee_Amount || 0;
  }

  public getinterestfeeCalculation(isARM: boolean) {
    if (isARM) {
      return this.localloanobj.LoanMaster.ARM_Rate_Fee || 0;
    } else {
      return this.localloanobj.LoanMaster.Dist_Rate_Fee || 0;
    }
  }

  public getArmSum() {
    return this.localloanobj.LoanMaster.Total_ARM_Fees_And_Interest || 0;
  }

  public getallsum() {
    return (this.localloanobj.LoanMaster.Total_ARM_Fees_And_Interest || 0) + ( this.localloanobj.LoanMaster.Dist_Rate_Fee || 0);
  }

  public gettotalsum() {
    return (this.localloanobj.LoanMaster.Total_ARM_Fees_And_Interest || 0) + (this.localloanobj.LoanMaster.Dist_Rate_Fee || 0);
  }

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res) {
        this.localloanobj = res;
        this.loanComment = this.localloanobj.LoanMaster.Loan_Comment;
      }
      this.validate();
    });

    this.validationService.ValidateLoanComment(this.localloanobj.LoanMaster.Loan_Comment);

    this.activatedRoute.queryParams.subscribe(params => {
      if(params['tab'] == 'exceptions') {
        setTimeout(() => {
          this.MatExpansionPanel.open();
        }, 500);
      }
    });
  }

  ngAfterViewInit() {
    this.validate();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public formatMaturityDate(strDate) {
    let finalDate;
    if (strDate) {
      let date = new Date(strDate);
      if (isNaN(+date)) {
        finalDate = this.refdataservice.getDefaultMaturityDate(
          this.localloanobj.LoanMaster.Crop_Year
        );
      } else {
        finalDate = date;
      }
    } else {
      finalDate = this.refdataservice.getDefaultMaturityDate(
        this.localloanobj.LoanMaster.Crop_Year
      );
    }

    return moment(finalDate).format('MM/DD/YYYY');
  }

  public formateClosingDate(strDate) {
    let date;
    if (strDate) {
      date = new Date(strDate);
      if (isNaN(date.getTime()) || date.getFullYear() < 2000) {
        date = new Date();
      }
    } else {
      date = new Date();
    }

    return moment(date).format('MM/DD/YYYY');
  }


  public onDateChange($event) {
    this.localloanobj.LoanMaster.Maturity_Date = $event;
    this.updateLocalStorage();
  }

  public onCloseDateChange($event) {
    this.localloanobj.LoanMaster.Close_Date = $event;
    this.updateLocalStorage();
  }

  public lineBreak(event) {
    if ((event.ctrlKey || event.metaKey) && (event.keyCode == 13 || event.keyCode == 10)) {
      let cursorPosition = event.currentTarget.selectionStart;
      event.currentTarget.value =
        event.currentTarget.value.substr(0, cursorPosition) +
        '\n--' +
        event.currentTarget.value.substr(cursorPosition);

      this.loanComment = event.currentTarget.value;
    }
  }

  public updateValue(val: any, key: string) {
    if(key == 'Origination_Fee_Percent') {
      this.localloanobj.LoanMaster.Origination_Fee_Percent = val;
    }

    if(key == 'Service_Fee_Percent') {
      this.localloanobj.LoanMaster.Service_Fee_Percent = val;
    }

    if(key == 'Extension_Fee_Percent') {
      this.localloanobj.LoanMaster.Extension_Fee_Percent = val;
    }

    if(key == 'Interest_Percent') {
      this.localloanobj.LoanMaster.Interest_Percent = val;
    }

    this.updateLocalStorage();
  }

  public updateLocalStorage() {
    if (!this.isLoanEditable) {
      return;
    }

    this.isFormUpdated = true;
    let loanMaster = this.localloanobj.LoanMaster;
    loanMaster.Loan_Comment = this.loanComment;

    this.getEstimatedDays();

    this.loanserviceworker.performcalculationonloanobject(this.localloanobj);

    this.validationService.ValidateLoanComment(this.loanComment);
    this.validate();

    this.publishService.enableSync(Page.committee);
  }

  public getEstimatedDays() {
    let loanMaster = this.localloanobj.LoanMaster;

    this.localloanobj.LoanMaster.Maturity_Date = this.formatMaturityDate(
      this.localloanobj.LoanMaster.Maturity_Date
    );
    this.localloanobj.LoanMaster.Close_Date = this.formateClosingDate(
      this.localloanobj.LoanMaster.Close_Date
    );

    if (
      this.localloanobj.LoanMaster.Maturity_Date &&
      this.localloanobj.LoanMaster.Close_Date
    ) {
      loanMaster.Estimated_Days = daysDiff(
        this.localloanobj.LoanMaster.Maturity_Date,
        this.localloanobj.LoanMaster.Close_Date
      );
    }
  }

  public get estimatedDaysClass() {
    if(this.localloanobj.LoanMaster.Estimated_Days) {
      return 'number-' + String(this.localloanobj.LoanMaster.Estimated_Days).length;
    }
    return 'number-1';
  }

  /**
   * Sync to database - publish button event
   */
  public synctoDb() {
    setTimeout(() => {
      // waiting for 0.5 sec to get the blur events excecuted meanwhile
      this.publishService.syncCompleted();
      this.committeeService.syncToDb(
        this.localstorageservice.retrieve(environment.loankey)
      );
    }, 500);
  }

  public get isLoanEditable() {
    return (
      this.localloanobj &&
      this.localloanobj.LoanMaster.Loan_Status === LoanStatus.Working
    );
  }

  private validate() {
    try {
      let currenterrors = this.localstorageservice.retrieve(
        environment.errorbase
      ) as Array<errormodel>;

      _.remove(currenterrors, function(param) {
        return param.chevron == `Loan_TERMS`;
      });

      if(this.localloanobj.LoanMaster.Maturity_Date) {
        let today = new Date();
        let d = new Date(this.localloanobj.LoanMaster.Maturity_Date);
        const Maturity_Date_Year = +this.localloanobj.LoanMaster.Crop_Year + 1;

        if(+today < +this.localloanobj.LoanMaster.Maturity_Date) {
          currenterrors.push({
            chevron: 'Loan_TERMS',
            tab: Page.committee,
            level: 2,
            details: [],
            Validation_ID_Text: `Maturity must be after today`,
            hoverText: `Maturity Date must be after today's date`,
            Validation_ID: 48
          });
        }

        if(
          (d.getFullYear() > Maturity_Date_Year) ||
          (d.getFullYear() == Maturity_Date_Year && d.getMonth() > 6) ||
          (d.getFullYear() == Maturity_Date_Year && d.getMonth() == 6 && d.getDate() > 15 )
        ) {
          currenterrors.push({
            chevron: 'Loan_TERMS',
            tab: Page.committee,
            level: 2,
            details: [],
            Validation_ID_Text: `Maturity must be before 07/15/` + Maturity_Date_Year,
            hoverText: `Maturity Date must come before July 31 of the following Crop Year`,
            Validation_ID: 38
          });
        }
      } else {
        currenterrors.push({
          chevron: 'Loan_TERMS',
          tab: Page.committee,
          level: 2,
          details: [],
          Validation_ID_Text: `Maturity Date required`,
          hoverText: `Maturity Date required`,
          Validation_ID: 37
        });
      }

      this.localstorageservice.store(
        environment.errorbase,
        _.uniq(currenterrors)
      );
    } catch {}

  }
}
