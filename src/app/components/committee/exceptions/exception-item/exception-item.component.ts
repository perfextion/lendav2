import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewEncapsulation
} from '@angular/core';

import { SessionStorageService } from 'ngx-webstorage';

import { Loan_Exception } from '@lenda/models/loan/loan_exceptions';
import { PublishService } from '@lenda/services/publish.service';
import { Page } from '@lenda/models/page.enum';
import { LoggedInUserModel } from '@lenda/models/commonmodels';
import { loan_model, LoanStatus } from '@lenda/models/loanmodel';

import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';

@Component({
  selector: 'app-exception-item',
  templateUrl: './exception-item.component.html',
  styleUrls: ['./exception-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ExceptionItemComponent implements OnInit {
  @Input() exception: Loan_Exception;
  @Input() localLoanObject: loan_model;

  @Output() checkErrors = new EventEmitter();

  private loggedInUser: LoggedInUserModel;
  private loggedInUserId: number;
  private Role_Type_Code: string;
  private Region_ID: number;
  private Username: string;

  public showTextarea = true;

  constructor(
    private loanCalcultionService: LoancalculationWorker,
    private publishService: PublishService,
    private sessionStorage: SessionStorageService
  ) {
    this.loggedInUser = this.sessionStorage.retrieve('UID');
    this.loggedInUserId = this.loggedInUser.UserID;
    this.Role_Type_Code = this.loggedInUser.Job_Title_Code;
    this.Region_ID = this.loggedInUser.Region_ID;
    this.Username = this.loggedInUser.FirstName + ' ' + this.loggedInUser.LastName;
  }

  ngOnInit() {
    if (this.exception) {
      this.showTextarea = !!this.exception.Mitigation_Text;
    }
  }

  public submit(exception: Loan_Exception, roleId: number) {
    if (
      this.canComment(
        exception[`Support_Role_Type_Code_${roleId}`],
        exception[`Support_User_ID_${roleId}`]
      )
    ) {
      exception[`Support_User_ID_${roleId}`] = this.loggedInUserId;
      exception[`Support_User_Name_${roleId}`] = this.Username;
      exception[`Support_Date_Time_${roleId}`] = new Date().toISOString();

      const Role = exception[`Support_Role_Type_Code_${roleId}`];

      this.Remove_Support_PA(exception, Role);
      this.onBlur(exception);
    }
  }

  private Remove_Support_PA(exception: Loan_Exception, Role: any) {
    try {
      let User_Pending_Actions = this.localLoanObject.User_Pending_Actions.filter(
        a =>
        a.Trigger_ID == exception.Exception_ID &&
        a.PA_Code == 'SUP' &&
        a.Trigger_Role == Role &&
        a.ActionStatus != 3
      );

      User_Pending_Actions.forEach(a => {
        if (a.ActionStatus == 1) {
          this.localLoanObject.User_Pending_Actions.splice(this.localLoanObject.User_Pending_Actions.indexOf(a), 1);
        } else {
          a.ActionStatus = 3;
        }
      });
    } catch { }

    try {
      const User_Pending_Actions = this.localLoanObject.User_Pending_Actions.filter(a => a.ActionStatus == 1 && a.Support_Roles.includes(Role));

      User_Pending_Actions.forEach(a => {
        if(a.Support_Roles.length == 1) {
          this.localLoanObject.User_Pending_Actions.splice(this.localLoanObject.User_Pending_Actions.indexOf(a), 1);
        } else {
          a.Support_Roles.splice(a.Support_Roles.indexOf(Role), 1);
        }
      });
    } catch { }
  }

  public onBlur(exception: Loan_Exception) {
    this.publishService.enableSync(Page.committee);
    if(exception.ActionStatus != 1) {
      exception.ActionStatus = 2;
    }

    this.loanCalcultionService.performcalculationonloanobject(
      this.localLoanObject,
      false
    );

    this.checkErrors.emit();
  }

  public isLoanEditable() {
    return (
      this.localLoanObject &&
      this.localLoanObject.LoanMaster.Loan_Status === LoanStatus.Working
    );
  }

  public canComment(Support_Role_Type_Code: string, Role_ID: number) {
    if (Role_ID) {
      return false;
    }

    if (this.isLoanEditable()) {
      if(Support_Role_Type_Code == 'LO' || Support_Role_Type_Code == '!LO') {
        return this.localLoanObject.LoanMaster.Loan_Officer_ID == this.loggedInUserId;
      }

      if (Support_Role_Type_Code.includes('!')) {
        Support_Role_Type_Code = Support_Role_Type_Code.substr(1);

        return (
          this.Role_Type_Code == Support_Role_Type_Code &&
          this.Region_ID == this.localLoanObject.LoanMaster.Region_ID
        );
      }

      return this.Role_Type_Code == Support_Role_Type_Code;
    }

    return false;
  }

  public showHideTextarea() {
    this.showTextarea = !this.showTextarea;
  }
}
