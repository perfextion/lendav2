import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  ViewEncapsulation
} from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';
import { Page } from '@lenda/models/page.enum';
import { loan_model, LoanStatus } from '@lenda/models/loanmodel';
import { Loan_Exception } from '@lenda/models/loan/loan_exceptions';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { errormodel } from '@lenda/models/commonmodels';

import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-committee-exceptions',
  templateUrl: './exceptions.component.html',
  styleUrls: ['./exceptions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CommitteeExceptionsComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;

  public exceptions: Array<ExceptionGroupedList> = [];
  public localLoanObject: loan_model;

  @Input() displayHeader = true;
  @Input() askForJudgement = false;

  public loanStatus: string;
  public LoanStatus: typeof LoanStatus = LoanStatus;

  public recommendButtonClicked: boolean;
  public errorsList: any = [];

  constructor(
    private localStorageService: LocalStorageService,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.recommendButtonClicked = false;

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res) {
        this.localLoanObject = res;
        this.prepareData();
        this.checkErrors();
      }
    });

    this.localLoanObject = this.localStorageService.retrieve(
      environment.loankey
    );

    if (this.localLoanObject) {
      this.prepareData();
      this.checkErrors();
    }
  }

  private prepareData() {
    let exceptions = this.localLoanObject.Loan_Exceptions.filter(
      ex => ex.ActionStatus != 3 && ex.Mitigation_Ind == 1
    );

    this.loanStatus = this.localLoanObject.LoanMaster.Loan_Status;

    let groupExceptions = _.groupBy(exceptions, e => e.Tab_ID);
    let refData: RefDataModel = this.dataService.getRefData();

    this.exceptions = [];

    for (let tab in groupExceptions) {
      let refTab = refData.RefTabs.find(t => t.Tab_ID.toString() == tab);
      if (refTab) {
        this.exceptions.push({
          tab: refTab.Tab_Name,
          exceptions: _.sortBy(groupExceptions[tab], e => e.Sort_Order)
        });
      }
    }
  }

  public checkErrors() {
    try {
      let currenterrors = this.localStorageService.retrieve(
        environment.errorbase
      ) as Array<errormodel>;

      _.remove(currenterrors, function(param) {
        return param.chevron == `Committee_Exception`;
      });

      let exceptions = this.localLoanObject.Loan_Exceptions.filter(
        ex => ex.ActionStatus != 3 && ex.Mitigation_Ind == 1
      );

      exceptions.forEach(ex => {
        if (!ex.Mitigation_Text && ex.Exception_ID_Level == 2) {
          currenterrors.push({
            chevron: 'Committee_Exception',
            tab: Page.committee,
            level: 2,
            details: [],
            quetionId: ex.Exception_ID,
            Validation_ID_Text: `Mitigation Text required - ${ex.Exception_ID_Text}`,
            hoverText: `Mitigation Text required - ${ex.Exception_ID_Text}`,
            Validation_ID: 24
          });
        }

        if (ex.Support_Ind) {
          this.Validate_Support(ex, currenterrors);
        }
      });

      this.localStorageService.store(
        environment.errorbase,
        _.uniq(currenterrors)
      );
    } catch {}
  }

  public Validate_Support(
    ex: Loan_Exception,
    currenterrors: Array<errormodel>
  ) {
    const indicators = [];

    if (ex.Support_Role_Type_Code_1) {
      if (ex.Support_User_ID_1) {
        indicators.push(1);
      } else {
        indicators.push(0);
      }
    }

    if (ex.Support_Role_Type_Code_2) {
      if (ex.Support_User_ID_2) {
        indicators.push(1);
      } else {
        indicators.push(0);
      }
    }

    if (ex.Support_Role_Type_Code_3) {
      if (ex.Support_User_ID_3) {
        indicators.push(1);
      } else {
        indicators.push(0);
      }
    }

    if (ex.Support_Role_Type_Code_4) {
      if (ex.Support_User_ID_4) {
        indicators.push(1);
      } else {
        indicators.push(0);
      }
    }

    if (indicators.some(a => a == 0)) {
      currenterrors.push({
        chevron: 'Committee_Exception',
        tab: Page.committee,
        level: 2,
        details: [],
        quetionId: ex.Exception_ID,
        Validation_ID_Text: `Support required  - ${ex.Exception_ID_Text}`,
        hoverText: `Support required  - ${ex.Exception_ID_Text}`,
        Validation_ID: 34
      });
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public isLoanEditable() {
    return (
      !this.localLoanObject ||
      this.localLoanObject.LoanMaster.Loan_Status === LoanStatus.Working
    );
  }
}

class ExceptionGroupedList {
  tab: string = '';
  exceptions: Array<Loan_Exception> = [];
}
