import { Component, OnInit, ViewEncapsulation, OnDestroy, Input, ViewChild } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { errormodel } from '@lenda/models/commonmodels';
import { environment } from '@env/environment.prod';
import { ISubscription } from 'rxjs/Subscription';
import * as _ from 'lodash';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { ActivatedRoute } from '@angular/router';
import { MatExpansionPanel } from '@angular/material';

@Component({
  selector: 'app-loan-validations',
  templateUrl: './loan-validations.component.html',
  styleUrls: ['./loan-validations.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoanValidationsComponent implements OnInit, OnDestroy {
  validationGroups: Array<ValidationGroup> = [];
  validations: Array<errormodel>;

  private subscription: ISubscription;
  private refdata: RefDataModel;

  @Input() expanded: boolean = true;

  @ViewChild(MatExpansionPanel)  MatExpansionPanel:  MatExpansionPanel;

  constructor(
    private localstorage: LocalStorageService,
    private activatedRoute: ActivatedRoute
  ) {
    this.validations = this.localstorage.retrieve(environment.errorbase);
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);

    this.getData();

    this.subscription = this.localstorage
      .observe(environment.errorbase)
      .subscribe(errors => {
        this.validations = errors;
        this.getData();
      });

    this.activatedRoute.queryParams.subscribe(params => {
      let tab = params['tab'];
      if(tab == 'validations') {
        setTimeout(() => {
          this.MatExpansionPanel.open();
        }, 500);
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getData() {
    this.validations = _.orderBy(this.validations.filter(a => !a.ignore), v => this.getTabId(v.tab));
    let groups = _.groupBy(this.validations, v => v.tab);

    this.validationGroups = [];

    for (let group in groups) {
      this.validationGroups.push({
        tab: group,
        validations: _.sortBy(groups[group], v => v.level)
      });
    }
  }

  getTabId(tabname: string) {
    try {
      let tab = this.refdata.RefTabs.find(
        a => a.Tab_Name.toUpperCase() == tabname.toUpperCase()
      );
      return tab.Tab_ID;
    } catch {
      return 0;
    }
  }
}

export class ValidationGroup {
  tab: string;
  validations: Array<errormodel>;
}
