import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { LayoutService } from "@lenda/shared/layout/layout.service";
import { loan_model, LoanGroup } from "@lenda/models/loanmodel";
import { SchematicSettings } from "@lenda/preferences/models/user-settings/schematic.model";
import { LocalStorageService } from "ngx-webstorage";
import { environment } from "@env/environment.prod";
import { LoanMaster } from '@lenda/models/ref-data-model';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { Preferences } from '@lenda/preferences/models/index.model';
import { ToastrService } from 'ngx-toastr';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoanvalidatorService } from '@lenda/services/loanvalidator.service';
import { Router } from '@angular/router';
import { JsonConvert } from 'json2typescript';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { PublishService } from '@lenda/services/publish.service';
import { SummaryPanelService } from './summary-panel.service';
import { SchematicsSettings } from '@lenda/models/schematics/index.model';
import { CriticalItem } from '@lenda/models/schematics/base-entities/critical-item.model';
import { CurrencyPipe } from '@angular/common';
import { SchematicsItemSettings } from '@lenda/models/schematics/schematics-item-settings.model';
import { MasterService } from '@lenda/master/master.service';
import { Get_Borrower_Name } from '@lenda/services/common-utils';

@Component({
  selector: '[app-summary-panel]',
  templateUrl: './summary-panel.component.html',
  styleUrls: ['./summary-panel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SummaryPanelComponent implements OnInit {
  @Input() currentSelectedLoan: loan_model;
  private _loanGroups: LoanGroup[];
  public crossCollateralItems: LoanGroup[];
  public crossCollateralItemsDetails = [];

  @Input() schematicPreferences: SchematicSettings;
  public summaryDetails = [];
  public farmerTime: Date;
  public armTime: Date;

  public isExpanded: boolean = true;
  private cp = new CurrencyPipe('en-US');

  sample = 'Nicholas Dean Christensen';

  @Output() schematicsSettingChange = new EventEmitter<boolean>(true);

  @Input()
  schematicSettings: SchematicsSettings;

  isCrossCollaterized: boolean;

  constructor(
    private localstorageservice: LocalStorageService,
    private layoutService: LayoutService,
    private settingsService: SettingsService,
    private toaster: ToastrService,
    private loanservice: LoanApiService,
    private logging: LoggingService,
    private loanvalidator: LoanvalidatorService,
    private route: Router,
    private loancalculationservice: LoancalculationWorker,
    private alertify: AlertifyService,
    private publishService: PublishService,
    private summaryPanelService: SummaryPanelService,
    private masterService: MasterService
  ) { }

  get loanGroups(): LoanGroup[] {
    return this._loanGroups;
  }

  @Input() set loanGroups(value: LoanGroup[]) {
    this._loanGroups = value;
    this.crossCollateralItemsDetails = [];
    this.summaryPanelService.getCrossCollateralDetails(this.loanGroups);
  }

  ngOnInit() {
    // Sidebar expand/collapse to expand/collapse the schematics
    this.layoutService.isSidebarExpanded().subscribe((value) => {
      this.isExpanded = value;
    });

    this.summaryPanelService.crossCollateralItems$.subscribe((val: LoanGroup[]) => {
      this.crossCollateralItems = val;
      if (this.crossCollateralItems) {
        this.getItemsDetails();
      }
    });
  }

  /**
   * Gets cross collateral item details
   */
  getItemsDetails() {
    this.isCrossCollaterized = this.crossCollateralItems.some(lg => lg.IsCrossCollateralized);
    this.crossCollateralItemsDetails = [];

    this.crossCollateralItems = this.sortLoanGroups(this.crossCollateralItems);

    for (let group of this.crossCollateralItems) {
      let loanMaster =  group.LoanJSON.LoanMaster as LoanMaster;
      let ARM_Commitment = loanMaster.ARM_Commitment || 0;
      let outstandingBalance = loanMaster.Outstanding_Balance;
      let pastDueDays = 2;

      let details = [
        `${this.cp.transform(ARM_Commitment || 0, 'USD','symbol', '1.0-0')}`
      ];

      let loan_id = loanMaster.Loan_ID + '_' + loanMaster.Loan_Seq_num;

      this.crossCollateralItemsDetails.push({
        borrowerName: Get_Borrower_Name(group.LoanJSON.LoanMaster),
        isPathCompleted: group.IsAffiliated && group.IsCrossCollateralized,
        Loan_Full_ID: group.LoanJSON.Loan_Full_ID,
        loan_id: loan_id,
        details: details
      });

      let hoverinfo = [
        new CriticalItem(`ARM Commit    : ${this.cp.transform(ARM_Commitment || 0, 'USD','symbol', '1.0-0')}`),
        new CriticalItem(`Outstanding Balance    : ${this.cp.transform(outstandingBalance || 0, 'USD','symbol', '1.0-0')}`),
        new CriticalItem(`Past due   : ${Number(pastDueDays).toLocaleString('en-US')} days`)
      ];

      if(!this.schematicSettings[`collateral_` + loan_id]) {
        this.schematicSettings[`collateral_` + loan_id] = new SchematicsItemSettings();
      }

      this.schematicSettings[`collateral_` + loan_id].hoverInfos = hoverinfo;
    }
  }

  sortLoanGroups(d: Array<LoanGroup>) {
    const data = d.sort((a, b) => {
      if (a.IsAffiliated && a.IsCrossCollateralized && (!b.IsAffiliated || !b.IsCrossCollateralized)) {
        return -1;
      } else if (a.IsAffiliated && a.IsCrossCollateralized && b.IsAffiliated && b.IsCrossCollateralized) {
        return 0;
      }
      return 1;
    });

    return data;
  }

  isPathCompleted(index: number, items: Array<any>) {
    if(index < items.length) {
      return items[index + 1].isPathCompleted;
    }
    return false;
  }

    /**
   * Returns transform x for node item
   * @param index index of item
   */
  getTranslate(index) {
    return `translate(${index * 150}, 0)`;
  }

  /**
   * Gets translate x for name to center text from node
   * @param name borrower name
   */
  getTranslateText(name) {
    if (name.length <= 6) {
      return `translate(0, 50)`;
    }
    return `translate(-${(name.length - 6) * 4}, 50)`;
  }

  /**
   * Navigates to Loan
   */
  navigatetoloan(id: string) {
    if (!id) {
      return;
    }

    this.alertify.confirm('Confirm', 'Are you sure you want to leave the loan?').subscribe(res => {
      if (res) {
        // Get default landing page from my preferences
        let preferences: Preferences = this.settingsService.preferences;
        this.getLoanBasicDetails(preferences, id);
      }
    });
  }

  getLoanBasicDetails(preferences: Preferences, loanid) {
    this.loanservice.getLoanById(loanid).subscribe(res => {
      this.masterService.isMinimize = false;
      this.logging.checkandcreatelog(3, 'Overview', "APi LOAN GET with Response " + res.ResCode);
      if (res.ResCode == 1) {
        this.loanvalidator.validateloanobject(res.Data).then(errors => {
          if (errors && errors.length > 0) {
            this.toaster.error("Loan Data is invalid");
          } else {
            let jsonConvert: JsonConvert = new JsonConvert();
            this.loancalculationservice.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model));
            this.localstorageservice.store(environment.errorbase, []);
            this.loancalculationservice.saveValidationsToLocalStorage(res.Data);

            this.localstorageservice.store(environment.loanGroup, []); //reset loan group
            this.localstorageservice.store(environment.loanidkey, loanid);
            this.localstorageservice.store(environment.loankey_copy, res.Data);
            this.localstorageservice.store(environment.loanCrossCollateral, []);

            this.route.navigateByUrl(`/home/loanoverview/${loanid.replace("-", "/")}/${preferences.userSettings.landingPage}`);

            this.loanservice.getLoanGroups(loanid).subscribe(res => {
              if (res && res.ResCode == 1) {
                this.localstorageservice.store(environment.loanGroup, res.Data);
              }
            });

            this.loanservice.getDistinctLoanIDList(loanid).subscribe(res => {
              if (res && res.ResCode == 1) {
                this.localstorageservice.store(environment.loanCrossCollateral, res.Data);
              }
            });

            this.localstorageservice.store(environment.exceptionStorageKey, []);
            this.localstorageservice.store(environment.modifiedbase, []);

            this.publishService.syncCompleted();

            this.schematicPreferences.isEnhanceDetailActive = false;
            this.schematicsSettingChange.emit(false);
          }
        });
      } else {
        this.toaster.error("Could not fetch Loan Object from API")
      }
    });
  }
}
