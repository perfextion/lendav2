import { Injectable } from "@angular/core";
import { LoanGroup } from "@lenda/models/loanmodel";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class SummaryPanelService {
    public crossCollateralItems: LoanGroup[];
    public crossCollateralItems$: BehaviorSubject<LoanGroup[]> = new BehaviorSubject<LoanGroup[]>([]);

    constructor() { }

    /**
    * Gets cross collateral details from loan groups
    */
    getCrossCollateralDetails(loanGroups: LoanGroup[]) {
        if (loanGroups) {
            this.crossCollateralItems = loanGroups.filter((item) => item.IsCrossCollateralized || item.IsAffiliated);
        }
        this.crossCollateralItems$.next(this.crossCollateralItems);
    }
}
