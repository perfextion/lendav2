import { Component, OnInit, ElementRef } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';
import { loan_model } from '@lenda/models/loanmodel';
import { SchematicSettings } from '@lenda/preferences/models/user-settings/schematic.model';
import { SchematicsSettings } from '@lenda/models/schematics/index.model';
import {
  AutoUnsubscribe,
  SubscriptionList
} from '@lenda/services/auto-unsubscribe';

import { SvgTooltipService } from '@lenda/ui-components/svg-tooltip/svg-tooltip.service';
import { FlowchartService } from '@lenda/components/flowchart/flowchart.service';
import { LayoutService } from '@lenda/shared/layout/layout.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { SummaryPanelService } from './summary-panel/summary-panel.service';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { FlowchartHelper } from './flowchart-helper';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-flowchart',
  templateUrl: './flowchart.component.html',
  styleUrls: ['./flowchart.component.scss'],
  providers: [SvgTooltipService, FlowchartService, SummaryPanelService]
})
@AutoUnsubscribe({
  subscriptionKey: 'subscriptions'
})
export class FlowchartComponent implements OnInit {
  public schematicPreferences: SchematicSettings;
  public schematicSettings: SchematicsSettings = new SchematicsSettings();
  public isExpanded: boolean = true;
  public currentSelectedLoan: loan_model;
  public loanGroups = [];
  public hasSpouse: boolean;
  private refdata: RefDataModel;

  private subscriptions = new SubscriptionList();

  constructor(
    private localstorage: LocalStorageService,
    public elRef: ElementRef,
    private svgTooltipService: SvgTooltipService,
    private flowchartService: FlowchartService,
    private layoutService: LayoutService,
    private settingsService: SettingsService,
    private flowchartHelper: FlowchartHelper,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);

    // Get selected loan
    let res = this.localstorage.retrieve(environment.loankey);
    this.prepareData(res);

    // Sidebar expand/collapse to expand/collapse the schematics
    this.subscriptions.sidebarExpandedSub = this.layoutService
      .isSidebarExpanded()
      .subscribe(value => {
        this.isExpanded = value;
      });

    this.loanGroups = this.localstorage.retrieve(environment.loanGroup);
    // Need to observe as otherwise sometimes loangroup is not available
    this.subscriptions.loanGroupSub = this.localstorage
      .observe(environment.loanGroup)
      .subscribe(res => {
        this.loanGroups = res;
        if (this.loanGroups && this.loanGroups.length > 0) {
          setTimeout(() => {
            this.addTooltips();
          }, 500);
        }
      });

    this.subscriptions.loankeySub = this.localstorage
      .observe(environment.loankey)
      .subscribe((res: loan_model) => {
        if (res && res.LoanMaster) {
          this.prepareData(res);
        }
      });

    this.subscriptions.enhancedDetailChangeSub = this.settingsService.enhancedDetailChange.subscribe(
      res => {
        if (res) {
          setTimeout(() => {
            this.addTooltips();
          }, 500);
        }
      }
    );

    this.subscriptions.dataServiceSub = this.dataService.getLoanObject().subscribe(loan => {
      this.prepareData(loan);
    });
  }

  private prepareData(loan: loan_model) {
    this.currentSelectedLoan = loan;
    // Get schematic settings
    let res = this.flowchartHelper.getSchematicsSettings(loan, this.refdata, this.loanGroups || []);
    this.schematicSettings = res.schematicSettings;
    this.hasSpouse = res.hasSpouse;

    // Currently saving in local storage once initialized
    this.flowchartService.saveSchematicSettings(this.schematicSettings);
    // Get preferences for schematics
    this.getUserPreferences();
  }

  getUserPreferences() {
    this.schematicPreferences = this.settingsService.preferences.userSettings.schematicSettings;

    this.subscriptions.preferencesChangeSub = this.settingsService.preferencesChange.subscribe(
      preferences => {
        this.schematicPreferences = preferences.userSettings.schematicSettings;
        if (this.schematicPreferences.isEnhanceDetailActive) {
          setTimeout(() => {
            this.addTooltips();
          }, 500);
        }
      }
    );
  }

  /**
   * Add Tooltips for Icons, Validation Icons and Exception Icons
   */
  addTooltips() {
    // Icon Bubbles
    this.svgTooltipService.initTooltip(
      this.elRef.nativeElement.querySelectorAll('.icon'),
      'flowchart',
      9
    );

    // Main Icons
    this.svgTooltipService.initTooltipForIcons(
      this.elRef.nativeElement.querySelectorAll('.loan-icon'),
      'flowchart',
      9
    );

    // Error and Validation Icons
    this.svgTooltipService.initTooltipForErrorAndValidationIcons(
      this.elRef.nativeElement.querySelectorAll('[app-critical-item]'),
      'flowchart',
      9
    );
  }
}
