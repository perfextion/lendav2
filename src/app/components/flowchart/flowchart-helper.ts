import { Injectable } from '@angular/core';
import { CurrencyPipe, DatePipe } from '@angular/common';

import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { loan_model, LoanGroup } from '@lenda/models/loanmodel';
import { SchematicsSettings } from '@lenda/models/schematics/index.model';
import {
  CriticalItem,
  CriticalItemLevel
} from '@lenda/models/schematics/base-entities/critical-item.model';
import { SchematicsItemSettings } from '@lenda/models/schematics/schematics-item-settings.model';
import { SchematicStatus } from '@lenda/models/schematics/base-entities/schematic-status.enum';
import {
  CollateralValueKey,
  CollateralReportService
} from '../reports/collateral-report.service';
import { CollateralHelper } from '../collateral/collateral-helper.service';
import {
  lookupCropTypeWithRefData,
  lookupCropPracticeTypeWithRefData
} from '@lenda/Workers/utility/aggrid/cropboxes';
import { LoanMaster, RefDataModel } from '@lenda/models/ref-data-model';
import { ExceptionTabs, ValidationTabs } from './schematic-rules.values';
import {
  exceptionlevel,
  errormodel,
  validationlevel
} from '@lenda/models/commonmodels';
import {
  resetConnectorsStatus,
  setConnectorsStatus
} from './connectors/connection-helper';
import { LocalStorageService } from 'ngx-webstorage';

@Injectable({
  providedIn: 'root'
})
export class FlowchartHelper {
  private formatter = new Intl.NumberFormat('en-US', {
    maximumFractionDigits: 1,
    minimumFractionDigits: 1
  });

  private cp = new CurrencyPipe('en-US');

  private currentSelectedLoan: loan_model;
  private schematicSettings: SchematicsSettings;
  public hasSpouse: boolean;

  private refdata: RefDataModel;
  private loanGroups: Array<LoanGroup>;

  constructor(
    private collateralReportService: CollateralReportService,
    private localstorage: LocalStorageService
  ) {}

  getSchematicsSettings(
    loan: loan_model,
    refdata: RefDataModel,
    loanGroups: Array<LoanGroup>
  ) {
    this.currentSelectedLoan = loan;
    this.refdata = refdata;
    this.loanGroups = loanGroups;
    this.schematicSettings = new SchematicsSettings();

    this.getItemsDetails();
    this.setFarmerInfo();
    // Spouse
    this.setSpouseDetails();
    this.setCommitteeList();
    // Cross Collateral
    this.setCrossCollateralDetails();

    this.setExceptions();

    return {
      schematicSettings: this.schematicSettings,
      hasSpouse: this.hasSpouse
    }
  }

  setSummaryDetails() {
    let loanMaster = this.loanMaster;

    let ARM = this.cp.transform(
      this.loanMaster.ARM_Commitment || 0,
      'USD',
      'symbol',
      '1.0-0'
    );

    let RC = this.cp.transform(
      this.loanMaster.Risk_Cushion_Amount || 0,
      'USD',
      'symbol',
      '1.0-0'
    );

    let excessIns =
      loanMaster.Net_Market_Value_Insurance +
      loanMaster.FC_Total_Addt_Collateral_Value -
      (loanMaster.ARM_Commitment +
        loanMaster.Dist_Commitment +
        loanMaster.Interest_Est_Amount);

    let Margin = this.cp.transform(excessIns || 0, 'USD', 'symbol', '1.0-0');
    let CF = this.cp.transform(
      loanMaster.Cash_Flow_Amount || 0,
      'USD',
      'symbol',
      '1.0-0'
    );

    this.schematicSettings.summary.details.push('RC: ' + RC);

    this.schematicSettings.summary.hoverInfos = [
      new CriticalItem(`ARM Commit :  ${ARM}`),
      new CriticalItem(`Cash Flow :  ${CF}`),
      new CriticalItem(`Risk Cushion :  ${RC}`),
      new CriticalItem(`Margin :  ${Margin}`)
    ];
  }

  /**
   * Set Spouse Details
   */
  public setSpouseDetails() {
    if (!this.schematicSettings.spouse) {
      this.schematicSettings.spouse = new SchematicsItemSettings(
        SchematicStatus.success
      );
    }

    if (this.loanMaster.Borrower_Entity_Type_Code == 'INS') {
      this.hasSpouse = true;
      this.schematicSettings.spouse.hoverInfos = [
        new CriticalItem(
          `${this.loanMaster.Spouse_First_Name} ${this.loanMaster.Spouse_Last_name}`
        )
      ];
    } else {
      this.hasSpouse = false;
      this.schematicSettings.spouse.hoverInfos = [];
    }
  }

  /**
   * Gets items details to be displayed as extra details below `icons`
   */
  getItemsDetails() {
    this.schematicSettings.farmer.details = [];
    this.schematicSettings.borrower.details = [];
    this.schematicSettings.crop.details = [];
    this.schematicSettings.irr.details = [];
    this.schematicSettings.ni.details = [];
    this.schematicSettings.farm.details = [];
    this.schematicSettings.farm.infos = [];
    this.schematicSettings.insurance.details = [];
    this.schematicSettings.budget.details = [];
    this.schematicSettings.optimizer.details = [];
    this.schematicSettings.collateral.details = [];
    this.schematicSettings.summary.details = [];

    this.setFarmDetails();
    this.setCropAcersDetails();
    this.setBorrowerStatus();
    this.setBudgetDetails();

    this.setInsuranceDetails();
    this.setOptimizerDetails();

    this.setCollateralDetails();
    // this.setSummaryDetails();
    this.setDocumentDetails();
    this.setNotridgeDetails();
    this.setDecideDetails();
  }

  private setDecideDetails() {
    this.schematicSettings.decide.hoverInfos = [];

    try {
      if (this.currentSelectedLoan) {
        if (this.currentSelectedLoan.LoanComments) {
          let groups = _.groupBy(
            this.currentSelectedLoan.LoanComments,
            c => c.User_ID
          );

          for (let group in groups) {
            let isUnread = groups[group].some(comment => !comment.IsRead);

            let user =
              groups[group][0].LastName + ', ' + groups[group][0].FirstName;
            if (isUnread) {
              if (user) {
                let item = new CriticalItem(`${user} has unread comments.`);
                this.schematicSettings.decide.hoverInfos.push(item);
              }
            }
          }
        }

        if (this.schematicSettings.decide.hoverInfos.length == 0) {
          let item = new CriticalItem(`No unread comments.`);
          this.schematicSettings.decide.hoverInfos.push(item);
        }
      }
    } catch {}
  }

  private setNotridgeDetails() {
    let Total = this.cp.transform(
      this.currentSelectedLoan.LoanMaster.Outstanding_Balance || 0,
      'USD',
      'symbol',
      '1.0-0'
    );

    this.schematicSettings.nortridge.details = [
      `${Total}`,
      `Past Due : 89 Days`
    ];

    let dp = new DatePipe('en-US');

    this.schematicSettings.nortridge.hoverInfos = [
      new CriticalItem(`Outstanding Balance: ${Total}`),
      new CriticalItem(`As of: ${dp.transform(new Date(), 'MM/dd/yyyy')}`)
    ];
  }

  setDocumentDetails() {
    this.schematicSettings.underwriting.details = [];
    this.schematicSettings.underwriting.hoverInfos = [];
    this.schematicSettings.closing.details = [];
    this.schematicSettings.closing.hoverInfos = [];
    this.schematicSettings.prerequisiteConditions.details = [];
    this.schematicSettings.prerequisiteConditions.hoverInfos = [];

    this.currentSelectedLoan.Loan_Documents.filter(
      a => a.ActionStatus != 3
    ).forEach(a => {
      if (!a.Upload_Date_Time) {
        let item = new CriticalItem(`${a.Document_Name}`);
        if (a.Document_Type_Level == 1) {
          this.schematicSettings.underwriting.hoverInfos.push(item);
        } else if (a.Document_Type_Level == 2) {
          this.schematicSettings.prerequisiteConditions.hoverInfos.push(item);
        } else if (a.Document_Type_Level == 3) {
          this.schematicSettings.closing.hoverInfos.push(item);
        }
      }
    });
  }

  private setInsuranceDetails() {
    let INS_Value = this.cp.transform(
      this.loanMaster.FC_Net_Market_Value_Insurance || 0,
      'USD',
      'symbol',
      '1.0-0'
    );

    this.schematicSettings.insurance.details.push(INS_Value);

    let hoverInfos = [];

    let LoanPolicies = this.currentSelectedLoan.LoanPolicies.filter(
      a => a.Select_Ind && a.ActionStatus != 3
    );

    let plans = _.groupBy(LoanPolicies, p => p.Ins_Plan);

    for (let plan in plans) {
      let policies = plans[plan];

      let crop_units = this.currentSelectedLoan.LoanCropUnits.filter(
        cu =>
          policies.some(a => a.Loan_Policy_ID == cu.FC_Ins_Policy_ID) &&
          cu.ActionStatus != 3
      );

      let ins = _.sumBy(crop_units, c => c.Ins_Value || 0);

      let ins_value = this.cp.transform(ins || 0, 'USD', 'symbol', '1.0-0');

      hoverInfos.push(new CriticalItem(`${plan} : ${ins_value}`));
    }

    this.schematicSettings.insurance.hoverInfos = hoverInfos;
  }

  setOptimizerDetails() {
    let RC_P = (this.loanMaster.Risk_Cushion_Percent || 0).toFixed(1);

    this.schematicSettings.optimizer.details.push(RC_P + ' %');

    this.schematicSettings.optimizer.hoverInfos = [
      new CriticalItem(`Optimized RC:  ${RC_P} %`)
    ];
  }

  setCollateralDetails() {
    let headers = [CollateralValueKey.Market, CollateralValueKey.Insurance];
    let collateralReport = this.collateralReportService.getCollateralValueForSchematics(
      this.currentSelectedLoan,
      headers
    );

    let LTV = this.formatter.format(collateralReport.LTV.Mkt || 0);

    this.schematicSettings.collateral.details.push('LTV Mkt: ' + LTV + ' %');

    this.schematicSettings.collateral.hoverInfos = [];

    let crops_col =
      this.currentSelectedLoan.LoanMaster.Net_Market_Value_Crops || 0;
    if (crops_col > 0) {
      let item = new CriticalItem(
        `Crops: ${this.cp.transform(crops_col, 'USD', 'symbol', '1.0-0')}`
      );
      this.schematicSettings.collateral.hoverInfos.push(item);
    }

    const collaterals = _.groupBy(
      this.currentSelectedLoan.LoanCollateral,
      a => a.Collateral_Category_Code
    );

    for (let collateral in collaterals) {
      let items = collaterals[collateral];

      let total_mkt = _.sumBy(items, i => Number(i.Net_Market_Value));
      if (total_mkt > 0) {
        this.schematicSettings.collateral.hoverInfos.push(
          new CriticalItem(
            `${CollateralHelper.getCollateralAlisas(
              collateral
            )}: ${this.cp.transform(total_mkt, 'USD', 'symbol', '1.0-0')}`
          )
        );
      }
    }

    if (this.schematicSettings.collateral.hoverInfos.length == 0) {
      let item = new CriticalItem(`No Collateral info.`);
      this.schematicSettings.collateral.hoverInfos.push(item);
    }
  }

  /**
   * Sets `Budget` related details
   */
  private setBudgetDetails() {
    let totalBudget = this.loanMaster.Loan_Total_Budget || 0;
    this.schematicSettings.budget.details.push(
      `${this.cp.transform(totalBudget, 'USD', 'symbol', '1.0-0')}`
    );

    this.schematicSettings.budget.hoverInfos = [];

    const total_budgets = this.currentSelectedLoan.LoanBudget.filter(
      a =>
        a.ActionStatus != 3 && a.Crop_Practice_ID == 0 && a.Budget_Type == 'V'
    );

    total_budgets.forEach(bgt => {
      let Total = this.cp.transform(
        bgt.Total_Budget_Crop_ET || 0,
        'USD',
        'symbol',
        '1.0-0'
      );
      this.schematicSettings.budget.hoverInfos.push(
        new CriticalItem(`${bgt.Expense_Type_Name} | ${Total}`)
      );
    });
  }

  /**
   * Sets `Crop` related details
   */
  private setCropAcersDetails() {
    let totalIRRAcers = 0;
    let totalNIRAcers = 0;
    let totalAcers = 0;

    this.currentSelectedLoan.LoanCropPractices.forEach(lcp => {
      if (lcp.ActionStatus != 3) {
        if (lcp.FC_PracticeType === 'Irr') {
          totalIRRAcers += lcp.LCP_Acres;
        }

        if (lcp.FC_PracticeType === 'NI') {
          totalNIRAcers += lcp.LCP_Acres;
        }
      }
    });

    totalAcers = totalIRRAcers + totalNIRAcers;

    this.schematicSettings.crop.details.push(
      `${this.formatter.format(totalAcers)} Ac`
    );
    this.schematicSettings.irr.details.push(
      `${this.formatter.format(totalIRRAcers)} Ac`
    );
    this.schematicSettings.ni.details.push(
      `${this.formatter.format(totalNIRAcers)} Ac`
    );

    this.schematicSettings.irr.hoverInfos = [];
    this.schematicSettings.ni.hoverInfos = [];

    try {
      this.currentSelectedLoan.LoanCropPractices.filter(
        a => a.ActionStatus != 3
      ).forEach(a => {
        let crop = a.FC_CropName;
        let crop_type = lookupCropTypeWithRefData(
          a.Crop_Practice_ID,
          this.refdata
        );
        let crop_practice = lookupCropPracticeTypeWithRefData(
          a.Crop_Practice_ID,
          this.refdata
        );
        let acres = this.formatter.format(a.LCP_Acres);

        let hoverInfo = new CriticalItem(
          `${crop} | ${crop_type} | ${crop_practice} | ${acres} Ac`
        );

        if (a.FC_PracticeType == 'Irr') {
          this.schematicSettings.irr.hoverInfos.push(hoverInfo);
        }

        if (a.FC_PracticeType == 'NI') {
          this.schematicSettings.ni.hoverInfos.push(hoverInfo);
        }
      });
    } catch {}
  }

  /**
   * Sets `Farm` related details
   */
  private setFarmDetails() {
    let ownedLand = 0;
    let leasedLand = 0;

    this.currentSelectedLoan.Farms.forEach(farm => {
      if (farm.ActionStatus != 3) {
        if (farm.Owned == 0) {
          leasedLand += farm.FC_Total_Acres;
        } else {
          ownedLand += farm.FC_Total_Acres;
        }
      }
    });

    const totalLand = ownedLand + leasedLand;
    let ownedLandPerc = 0;

    if (totalLand > 0) {
      ownedLandPerc = (ownedLand / totalLand) * 100;
    }

    this.schematicSettings.farm.hoverInfos = [
      new CriticalItem(`Owned: ${this.formatter.format(ownedLand || 0)} Acres`),
      new CriticalItem(
        `Leased: ${this.formatter.format(leasedLand || 0)} Acres`
      )
    ];

    this.schematicSettings.farm.details.push(
      `Owned: ${ownedLandPerc.toFixed(1)} %`
    );
  }

  /**
   * ============
   * HELPERS
   * ============
   */

  /**
   * Sets borrower status based on selected loan
   */
  private setBorrowerStatus() {
    let armCommit = this.loanMaster.ARM_Commitment;
    let outstandingBalance = this.loanMaster.Outstanding_Balance || 0;
    let pastDueDays = this.loanMaster.Past_Due_Amount;

    this.schematicSettings.borrower.details = [
      `${this.cp.transform(armCommit || 0, 'USD', 'symbol', '1.0-0')}`
    ];

    this.schematicSettings.borrower.hoverInfos = [
      new CriticalItem(
        `ARM Commit    : ${this.cp.transform(
          armCommit || 0,
          'USD',
          'symbol',
          '1.0-0'
        )}`
      ),
      new CriticalItem(
        `Outstanding Balance    : ${this.cp.transform(
          outstandingBalance || 0,
          'USD',
          'symbol',
          '1.0-0'
        )}`
      ),
      new CriticalItem(
        `Past due   : ${Number(pastDueDays).toLocaleString('en-US')} days`
      )
    ];
  }

  /**
   * Sets `Farmer` related info
   */
  private setFarmerInfo() {
    let armCommit = this.loanMaster.ARM_Commitment;
    let pastDueBalance = this.loanMaster.Past_Due_Amount || 0;

    if (this.loanGroups) {
      for (let group of this.loanGroups) {
        let loan_master: LoanMaster = group.LoanJSON.LoanMaster;

        let armCommitment = loan_master.ARM_Commitment || 0;
        let pastDue = loan_master.Past_Due_Amount || 0;

        armCommit += armCommitment;
        pastDueBalance += pastDue;
      }

      this.schematicSettings.farmer.infos = [];
      this.loanGroups
        .filter(lg => lg.IsAffiliated)
        .forEach(al => {
          this.schematicSettings.farmer.infos.push(
            new CriticalItem(
              `${al.LoanJSON.LoanMaster.Borrower_First_Name} ${al.LoanJSON
                .LoanMaster.Borrower_Last_Name || ''}`
            )
          );
        });
    }

    this.schematicSettings.farmer.details = [
      `${this.cp.transform(armCommit || 0, 'USD', 'symbol', '1.0-0')}`
    ];

    this.schematicSettings.farmer.hoverInfos = [
      new CriticalItem(
        `${this.loanMaster.Farmer_Last_Name}, ${this.loanMaster.Farmer_First_Name} ${this.loanMaster.Farmer_MI}`
      ),
      new CriticalItem(
        `ARM Commit     : ${this.cp.transform(
          armCommit || 0,
          'USD',
          'symbol',
          '1.0-0'
        )}`
      ),
      new CriticalItem(
        `Outstanding Balance    : ${this.cp.transform(
          pastDueBalance || 0,
          'USD',
          'symbol',
          '1.0-0'
        )}`
      ),
      new CriticalItem(
        `Past Due   : ${Number(
          pastDueBalance.toFixed(0)
        ).toLocaleString()} days`
      )
    ];
  }

  /**
   * Set `Exception` for Respective `tabs`
   */
  private setExceptions() {
    Object.keys(ExceptionTabs).forEach(tab => {
      try {
        let exceptions = this.currentSelectedLoan.Loan_Exceptions.filter(
          e =>
            e.ActionStatus != 3 &&
            e.Tab_ID == ExceptionTabs[tab] &&
            !e.Complete_Ind
        );

        if (this.schematicSettings[tab]) {
          this.schematicSettings[tab].warnings = [];
          this.schematicSettings[tab].errors = [];

          exceptions.forEach(e => {
            if (e.Exception_ID_Level == exceptionlevel.level1) {
              this.schematicSettings[tab].warnings.push(
                new CriticalItem(e.Exception_ID_Text)
              );
            } else {
              this.schematicSettings[tab].errors.push(
                new CriticalItem(e.Exception_ID_Text)
              );
            }
          });
        }
      } catch {}
    });

    this.setValidations();
  }

  /**
   * Sets `Validations`
   */
  private setValidations() {
    let errors = this.localstorage.retrieve(environment.errorbase) as Array<
      errormodel
    >;
    errors = errors.filter(a => !a.ignore);

    if (errors) {
      Object.keys(ValidationTabs).forEach(tab => {
        try {
          if (!this.schematicSettings[tab].warnings) {
            this.schematicSettings[tab].warnings = [];
          } else {
            _.remove(
              this.schematicSettings[tab].warnings,
              w => w['level'] == CriticalItemLevel.Level1
            );
          }

          this.schematicSettings[tab].warnings.push(
            ...this.getValidations(errors, tab, validationlevel.level1)
          );

          if (!this.schematicSettings[tab].warnings) {
            this.schematicSettings[tab].warnings = [];
          } else {
            _.remove(
              this.schematicSettings[tab].errors,
              w => w['level'] == CriticalItemLevel.Level1
            );
          }

          this.schematicSettings[tab].errors.push(
            ...this.getValidations(errors, tab, validationlevel.level2)
          );
        } catch {}

      });

      this._setConnectorStatus(errors);
    }
  }

  /**
   * Gets `Array of Critical item` for validations
   * @param count number of validations
   */
  public getValidations(
    errors: Array<errormodel>,
    tab: string,
    level: validationlevel
  ) {
    let criticalItems: Array<CriticalItem> = [];

    let tabErrors: Array<errormodel> = [];

    if (tab == 'underwriting') {
      tabErrors = errors.filter(
        a =>
          a.tab.toLowerCase() == 'checkout' &&
          a.level == level &&
          a.quetionId == 1
      );
    } else if (tab == 'prerequisiteConditions') {
      tabErrors = errors.filter(
        a =>
          a.tab.toLowerCase() == 'checkout' &&
          a.level == level &&
          a.quetionId == 2
      );
    } else if (tab == 'closing') {
      tabErrors = errors.filter(
        a =>
          a.tab.toLowerCase() == 'checkout' &&
          a.level == level &&
          a.quetionId == 3
      );
    } else if (tab == 'farmer') {
      tabErrors = errors.filter(
        a =>
          a.chevron == 'farmer' &&
          a.tab.toLowerCase() == 'borrower' &&
          a.level == level
      );
    } else if (tab == 'borrower') {
      tabErrors = errors.filter(
        a =>
          a.chevron != 'farmer' &&
          a.tab.toLowerCase() == 'borrower' &&
          a.level == level
      );
    } else {
      tabErrors = errors.filter(
        a => a.tab.toLowerCase() == tab.toLowerCase() && a.level == level
      );
    }

    tabErrors.forEach(e => {
      let item = new CriticalItem(e.Validation_ID_Text);
      item.level = CriticalItemLevel.Level1;
      criticalItems.push(item);
    });

    return criticalItems;
  }

  private _setConnectorStatus(errors: Array<errormodel>) {
    try {
      let countObj = {};

      Object.keys(this.schematicSettings).forEach(key => {
        let tab = this.schematicSettings[key] as SchematicsItemSettings;
        if (tab.errors) {
          countObj[key] = tab.errors.filter(
            a => a.level == CriticalItemLevel.Level1
          ).length;
        } else {
          countObj[key] = 0;
        }
      });

      resetConnectorsStatus(this.schematicSettings);

      let farmerErrors = errors.filter(a => a.chevron == 'farmer');
      countObj['farmer'] = farmerErrors.length;

      let IrrAcres = _.sumBy(
        this.currentSelectedLoan.LoanCropPractices.filter(
          a => a.FC_Crop_Practice_Type == 'Irr'
        ),
        a => a.LCP_Acres
      );

      countObj['irr'] = IrrAcres == 0 ? 1 : 0;

      let NIAcres = _.sumBy(
        this.currentSelectedLoan.LoanCropPractices.filter(
          a => a.FC_Crop_Practice_Type == 'NI'
        ),
        a => a.LCP_Acres
      );

      countObj['ni'] = NIAcres == 0 ? 1 : 0;

      setConnectorsStatus(
        this.schematicSettings,
        countObj,
        this.currentSelectedLoan
      );
    } catch {}
  }

  /**
   * Sets `Committee list` on info item
   */
  private setCommitteeList() {
    this.schematicSettings.committee.hoverInfos = [];
    this.schematicSettings.committee.infos = [];

    if (
      this.currentSelectedLoan.LoanCommittees &&
      this.currentSelectedLoan.LoanCommittees.length > 0
    ) {
      for (let lc of this.currentSelectedLoan.LoanCommittees) {
        if (lc.ActionStatus != 3 && lc.CM_Role == 1) {
          let item = new CriticalItem(
            `${lc.FirstName} ${lc.LastName || ''}`,
            lc.Vote
          );
          this.schematicSettings.committee.hoverInfos.push(item);
        }
      }
    } else {
      let item = new CriticalItem(`No Committee Members.`);
      this.schematicSettings.committee.hoverInfos.push(item);
    }

    if (this.schematicSettings.committee.hoverInfos.length == 0) {
      let item = new CriticalItem(`No Committee Members.`);
      this.schematicSettings.committee.hoverInfos.push(item);
    }
  }

  setCrossCollateralDetails() {
    if (!this.schematicSettings.crossCollateral) {
      this.schematicSettings.crossCollateral = new SchematicsItemSettings(
        SchematicStatus.success
      );
    }
    // When groups are not available, take name of borrower form LoanMaster
    if (!this.loanGroups || this.loanGroups.length === 0) {
      this.schematicSettings.crossCollateral.details = [];

      let loanMaster: LoanMaster = this.currentSelectedLoan.LoanMaster;

      let totalCommitment = loanMaster.ARM_Commitment || 0;
      let outstandingBalance = loanMaster.Outstanding_Balance || 0;

      let pastDueBalance = loanMaster.Past_Due_Amount || 0;

      this.schematicSettings.crossCollateral.details = [
        `${this.cp.transform(totalCommitment || 0, 'USD', 'symbol', '1.0-0')}`
      ];

      if (outstandingBalance > 0) {
        this.schematicSettings.crossCollateral.details.push(
          `${this.cp.transform(
            outstandingBalance || 0,
            'USD',
            'symbol',
            '1.0-0'
          )}`
        );
      }

      if (pastDueBalance > 0) {
        this.schematicSettings.crossCollateral.details.push(
          `${this.cp.transform(pastDueBalance || 0, 'USD', 'symbol', '1.0-0')}`
        );
      }
    } else {
      this.schematicSettings.crossCollateral.details = [];
      let loan_master: LoanMaster = this.loanGroups[0].LoanJSON.LoanMaster;

      let totalCommitment = loan_master.ARM_Commitment || 0;
      let oustanding = loan_master.Outstanding_Balance || 0;
      let pastDue = loan_master.Past_Due_Amount || 0;

      this.schematicSettings.crossCollateral.details = [
        `${this.cp.transform(totalCommitment || 0, 'USD', 'symbol', '1.0-0')}`
      ];

      if (oustanding > 0) {
        this.schematicSettings.crossCollateral.details.push(
          `${this.cp.transform(oustanding || 0, 'USD', 'symbol', '1.0-0')}`
        );
      }

      if (pastDue > 0) {
        this.schematicSettings.crossCollateral.details.push(
          `${this.cp.transform(pastDue || 0, 'USD', 'symbol', '1.0-0')}`
        );
      }
    }
  }

  private get loanMaster() {
    return this.currentSelectedLoan.LoanMaster as LoanMaster;
  }
}
