import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '@lenda/shared/material.module';
import { SharedModule } from '@lenda/shared/shared.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';

import { FlowchartService } from './flowchart.service';
import { SvgTooltipService } from '@lenda/ui-components/svg-tooltip/svg-tooltip.service';
import { SummaryPanelService } from './summary-panel/summary-panel.service';

import { FlowchartComponent } from './flowchart.component';
import { ConnectorsComponent } from './connectors/connectors.component';
import { ConnectorComponent } from './connectors/connector/connector.component';
import { DocManagementPanelComponent } from './doc-management-panel/doc-management-panel.component';
import { CriticalItemComponent } from './entities/critical-item/critical-item.component';
import { ItemDetailsComponent } from './entities/item-details/item-details.component';
import { SchematicsLoanStatusIconComponent } from './entities/schematics-loan-status-icon/schematics-loan-status-icon.component';
import { IconsLibraryComponent } from './icons-library/icons-library.component';
import { LoanManagementPanelComponent } from './loan-management-panel/loan-management-panel.component';
import { ResourcesPanelComponent } from './resources-panel/resources-panel.component';
import { RiskChartCollateralComponent } from './risk-chart/risk-chart-collateral/risk-chart-collateral.component';
import { RiskChartWorkingLoanComponent } from './risk-chart/risk-chart-working-loan/risk-chart-working-loan.component';
import { SchematicAreasComponent } from './schematic-areas/schematic-areas.component';
import { AreaTitleComponent } from './schematic-areas/area-title/area-title.component';
import { CriticalInfoComponent } from './schematics-items/critical-info/critical-info.component';
import { InfoBubbleComponent } from './schematics-items/info-bubble/info-bubble.component';
import { SummaryPanelComponent } from './summary-panel/summary-panel.component';
import { WorkingLoanPanelComponent } from './working-loan-panel/working-loan-panel.component';
import { CropItemsComponent } from './working-loan-panel/crop-items/crop-items.component';

@NgModule({
  imports: [CommonModule, MaterialModule, SharedModule, UiComponentsModule],
  exports: [FlowchartComponent],
  declarations: [
    FlowchartComponent,
    ConnectorsComponent,
    ConnectorComponent,
    DocManagementPanelComponent,
    CriticalItemComponent,
    ItemDetailsComponent,
    SchematicsLoanStatusIconComponent,
    IconsLibraryComponent,
    LoanManagementPanelComponent,
    ResourcesPanelComponent,
    RiskChartCollateralComponent,
    RiskChartWorkingLoanComponent,
    SchematicAreasComponent,
    AreaTitleComponent,
    CriticalInfoComponent,
    InfoBubbleComponent,
    SummaryPanelComponent,
    WorkingLoanPanelComponent,
    CropItemsComponent
  ],
  providers: [FlowchartService, SvgTooltipService, SummaryPanelService]
})
export class FlowChartModule {}
