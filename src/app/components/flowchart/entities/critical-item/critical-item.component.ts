import { Component, OnInit, Input } from '@angular/core';
import { CriticalItemLevel } from '@lenda/models/schematics/base-entities/critical-item.model';

@Component({
  selector: '[app-critical-item]',
  templateUrl: './critical-item.component.html',
  styleUrls: ['./critical-item.component.scss']
})
export class CriticalItemComponent implements OnInit {
  @Input()
  type: string;

  @Input()
  x: string;

  @Input()
  y: string;

  @Input()
  number: number = 0;

  @Input()
  level: CriticalItemLevel = CriticalItemLevel.Level2;

  constructor() {}

  ngOnInit() {}

  getX() {
    if(this.number > 9 ) {
      return -1.7;
    }

    return 1.3;
  }
}
