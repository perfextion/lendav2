import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: '[app-item-details]',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ItemDetailsComponent implements OnInit {
  @Input() details: Array<string>;
  @Input() x: number;
  @Input() y: number;

  @Input() highlightIndexes: number[] = [];

  constructor() {}

  ngOnInit() {}


  hasError(index: number) {
    if(this.details.length > 1) {
      return this.highlightIndexes.indexOf(index) > -1;
    }
  }
}
