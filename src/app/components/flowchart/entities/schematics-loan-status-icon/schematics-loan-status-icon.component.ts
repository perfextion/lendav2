import { Component, OnChanges, Input } from '@angular/core';

@Component({
  selector: '[app-schematics-loan-status-icon]',
  templateUrl: './schematics-loan-status-icon.component.html'
})
export class SchematicsLoanStatusIconComponent implements OnChanges {
  @Input() Loan_Status: string;

  constructor() {}

  ngOnChanges() {
    if(this.Loan_Status && this.Loan_Status.includes('_')) {
      this.Loan_Status = this.Loan_Status.substring(0, this.Loan_Status.indexOf('_'));
    }
  }
}
