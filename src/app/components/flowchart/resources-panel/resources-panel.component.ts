import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { SchematicsSettings } from '@lenda/models/schematics/index.model';
import { SchematicSettings } from '@lenda/preferences/models/user-settings/schematic.model';
import { loan_model, LoanGroup } from '@lenda/models/loanmodel';
import { SchematicStatus } from '@lenda/models/schematics/base-entities/schematic-status.enum';
import { DateDiff } from '@lenda/services/common-utils';
import * as _ from 'lodash';

@Component({
  selector: '[app-resources-panel]',
  templateUrl: './resources-panel.component.html',
  styleUrls: ['./resources-panel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ResourcesPanelComponent implements OnInit {
  @Input() schematicSettings: SchematicsSettings;
  @Input() schematicPreferences: SchematicSettings;
  @Input() currentSelectedLoan: loan_model;

  private _loanGroups: Array<LoanGroup> = [];

  @Input() set loanGroups(value: Array<LoanGroup>) {
    this._loanGroups = value;
    this.setLoanList();
  }

  get loanGroups() {
    return this._loanGroups;
  }

  private applicationDate: string;
  datediff: any;

  loans = [];

  constructor() {}

  ngOnInit() {
    this.setApplicationDate();
    this.schematicSettings.reconciliation.details = [];
    this.schematicSettings.reconciliation.details.push('08/25/18');
    this.schematicSettings.reconciliation.details.push('Quality B');

    if (this.schematicSettings && this.schematicSettings.apply) {
      this.schematicSettings.apply.iconStatus = SchematicStatus.success;
    }

    this.datediff = DateDiff(new Date(), this.applicationDate);
    this.setLoanList();
  }

  private setLoanList() {
    this.loans = [];

    if (this.loanGroups) {
      let groups = this.loanGroups.filter(a => a.IsPreviousYearLoan);
      this.loans.push(this.currentSelectedLoan);
      groups = _.uniqBy(groups, g => g.LoanJSON.LoanMaster.Loan_ID);
      groups.forEach(g => {
        this.loans.push(g.LoanJSON);
      });
    } else {
      this.loans.push(this.currentSelectedLoan);
    }
  }

  abs(v) {
    return Math.abs(v);
  }

  setApplicationDate() {
    this.applicationDate = this.currentSelectedLoan.LoanMaster.Application_Date;
  }
}
