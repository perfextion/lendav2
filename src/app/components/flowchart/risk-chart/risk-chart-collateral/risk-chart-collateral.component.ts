import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from "@lenda/services/data.service";
import { loan_model, LoanGroup } from "@lenda/models/loanmodel";
import { LocalStorageService } from "ngx-webstorage";
import { environment } from "@env/environment.prod";
import { ISubscription } from 'rxjs/Subscription';
import * as d3 from 'd3';
import { chartSettings } from '@lenda/chart-settings';
import { ScaleHelper } from '@lenda/services/scale.helper';
import { RefDataService } from '@lenda/services/ref-data.service';
import { CollateralSchematicsService } from '@lenda/components/reports/cross-collateral/collateral-schamtics.service';

@Component({
    selector: '[risk-chart-collateral]',
    templateUrl: 'risk-chart-collateral.component.html',
    styleUrls: ['risk-chart-collateral.component.scss']
})
export class RiskChartCollateralComponent {

/* Initial Risk Chart  */
private subscription: ISubscription;
private scaleSubscription: ISubscription;

localLoanObject: loan_model;
public info = {
  riskCushionAmount: 0,
  riskCushionPercent: '',
  returnPercent: '',
  // TODO: Replace with real value for black and red diamond
  blackDiamond: 0,
  redDiamond: 0,
  armCommitment: 0,
  armInfo: 'Marty Tubbs; Cleveland, MS',
  startBlack: 0,
  startRed: 0
};
/* Initial Risk Chart Ends */

riskScale = [0, 2.5, 5.0, 7.5, 10.0];
returnScale = [5.0, 6.4, 7.8, 10.8, 13.7];
actualScale = [0, 25, 50, 75, 100];

private _loanGroups: any[];

@Input()
set loanGroups(lg) {
  this._loanGroups = lg;
  if(this.localLoanObject && this._loanGroups) {
    this.getRiskReturnValuesFromLocalStorage(this.localLoanObject.LoanMaster);
  }
}

constructor(
  private localstorageservice: LocalStorageService,
  private refDataService: RefDataService,
  private collateralSchamticsService: CollateralSchematicsService
) { }

ngOnInit() {
  this.localLoanObject = this.localstorageservice.retrieve(environment.loankey);
  if (this.localLoanObject && this.localLoanObject.LoanMaster) {
    this.getRiskReturnValuesFromLocalStorage(this.localLoanObject.LoanMaster);
  }

    this.setScale();
  }

  ngOnDestroy() {
    if(this.scaleSubscription) {
      this.scaleSubscription.unsubscribe();
    }

    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }

  private setScale() {
    this.returnScale = this.refDataService.getReturnScale(this.localLoanObject.LoanMaster.FC_Return_Scale || 0);
    this.riskScale = this.refDataService.getRiskScale();

    this.scaleSubscription = this.refDataService.returnScaleUpdated.subscribe(newscale => {
      this.returnScale = newscale;
      this.getRiskReturnValuesFromLocalStorage(this.localLoanObject.LoanMaster);
      this.setRiskAndReturnScale();
    });
  }

/* Set Risk Chart  */
getRiskReturnValuesFromLocalStorage(loanMaster) {
  this.info.riskCushionAmount = loanMaster.Risk_Cushion_Amount && loanMaster.Risk_Cushion_Amount !== 0 ? loanMaster.Risk_Cushion_Amount : 0;
  this.info.riskCushionPercent = loanMaster.Risk_Cushion_Percent || 0;

  this.info.returnPercent = loanMaster.Return_Percent || 0;
  this.info.armCommitment = loanMaster.ARM_Commitment ? loanMaster.ARM_Commitment : 0;

  if(this._loanGroups) {
    if(!this._loanGroups[0].returnPercent) {
      this._loanGroups[0].returnPercent = this.collateralSchamticsService.prepeareReturnPercent(this.localLoanObject, this._loanGroups);
    }

    this.info.returnPercent = this._loanGroups[0].returnPercent || this.info.returnPercent;
    this.info.blackDiamond = ScaleHelper.getPosition(this.returnScale, this.actualScale, parseFloat(this.info.returnPercent));

    if(!this._loanGroups[0].RC) {
      this._loanGroups[0].RC = this.collateralSchamticsService.prepareRC(this.localLoanObject, this._loanGroups);
    }
    this.info.riskCushionPercent = this._loanGroups[0].RC || this.info.riskCushionPercent;
    this.info.redDiamond = ScaleHelper.getPosition(this.riskScale, this.actualScale, parseFloat(this.info.riskCushionPercent));
  }

  this.setRiskAndReturnScale();
}

setChart(item, rangeStart, rangeEnd, color) {
  let linearScale = d3.scaleLinear()
    .domain([0, 1])
    .range([rangeStart, rangeEnd]);

  let myData = d3.range(0, 1);

  d3.select(item)
    .selectAll('rect')
    .data(myData)
    .enter()
    .append('rect')
    .attr('x', function (d) {
      return linearScale(d);
    })
    .attr('transform', function (d, i) {
      return 'translate(0, 10)';
    })
    .attr('width', 25)
    .attr('height', 10)
    .attr('fill', color);
}

setRiskAndReturnScale() {
  let symbolGenerator = d3.symbol()
    .size(30);
  let symbolTypes = ['symbolDiamond'];
  let xScale = d3.scaleLinear().range([0, 100]);

  d3.select('#blackDiamondCollateral')
    .html(null)
    .selectAll('path')
    .data(symbolTypes)
    .enter()
    .append('path')
    .attr('transform', (d, i) => {
      return 'translate(' + this.info.startBlack + ', 26)';
    })
    .transition()
    .duration(1500)
    .ease(d3.easeLinear)
    .attr('transform', (d, i) => {
      return 'translate(' + this.info.blackDiamond + ', 26)';
    })
    .attr('d', function (d) {
      symbolGenerator
        .type(d3[d]);
      return symbolGenerator();
    }).attr('fill', chartSettings.riskAndReturns.blackDiamond);

  this.info.startBlack = this.info.blackDiamond;

  d3.select('#redDiamondCollateral')
    .html(null)
    .selectAll('path')
    .data(symbolTypes)
    .enter()
    .append('path')
    .attr('transform', (d, i) => {
      return 'translate(' + this.info.startRed + ', 4)';
    })
    .transition()
    .duration(1750)
    .attr('transform', (d, i) => {
      return 'translate(' + this.info.redDiamond + ', 4)';
    })
    .attr('d', function (d) {
      symbolGenerator
        .type(d3[d]);

      return symbolGenerator();
    })
    .attr('fill', chartSettings.riskAndReturns.redDiamond);

    this.info.startRed = this.info.redDiamond;
  }

  ngAfterViewInit() {
    // Set bar
    this.setChart('#riskCollateral', 0, 25, chartSettings.riskAndReturns.riskBg);
    this.setChart('#cushionCollateral', 25, 50, chartSettings.riskAndReturns.cushionBg);
    this.setChart('#returnFirstCollateral', 50, 75, chartSettings.riskAndReturns.returnLightGreen);
    this.setChart('#returnSecondCollateral', 75, 100, chartSettings.riskAndReturns.returnDarkGreen);
    // Set diamond
    this.setRiskAndReturnScale();
  }
  /* Set Risk Chart Ends */
}
