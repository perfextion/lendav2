import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchematicAreasComponent } from './schematic-areas.component';

describe('SchematicAreasComponent', () => {
  let component: SchematicAreasComponent;
  let fixture: ComponentFixture<SchematicAreasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchematicAreasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchematicAreasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
