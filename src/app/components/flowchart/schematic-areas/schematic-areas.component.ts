import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { SchematicSettings } from '@lenda/preferences/models/user-settings/schematic.model';
import { loan_model } from '@lenda/models/loanmodel';

@Component({
  selector: '[app-schematic-areas]',
  templateUrl: './schematic-areas.component.html',
  styleUrls: ['./schematic-areas.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SchematicAreasComponent implements OnInit {
  @Input()
  schematicPreferences: SchematicSettings;

  @Input()
  currentSelectedLoan: loan_model;

  constructor() {}

  ngOnInit() {}

  get height() {
    if (this.schematicPreferences) {
      if (this.schematicPreferences.isEnhanceDetailActive) {
        return 110;
      }
      return 80;
    }
    return 80;
  }

  get width() {
    if (this.currentSelectedLoan) {
      if (this.currentSelectedLoan.LoanCrops) {
        return this.currentSelectedLoan.LoanCrops.length * 110;
      }
      return 0;
    }
    return 0;
  }
}
