import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-area-title]',
  templateUrl: './area-title.component.html',
  styleUrls: ['./area-title.component.scss']
})
export class AreaTitleComponent implements OnInit {
  @Input() text: string;
  @Input() icon: string;
  @Input() x;
  @Input() y;
  @Input() marginLeft: number = 0;


  constructor() { }

  ngOnInit() {
  }

  getMarginLeft() {
    return this.marginLeft + 10;
  }
}
