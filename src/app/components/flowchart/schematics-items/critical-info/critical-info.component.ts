import { Component, OnChanges, Input } from '@angular/core';
import { SchematicsItemSettings } from '@lenda/models/schematics/schematics-item-settings.model';
import {
  CriticalItemLevel,
  CriticalInfo
} from '@lenda/models/schematics/base-entities/critical-item.model';
import { SchematicStatus } from '@lenda/models/schematics/base-entities/schematic-status.enum';
import { Router } from '@angular/router';
import { PageInfoService } from '@lenda/services/page-info.service';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';

@Component({
  selector: '[app-critical-info]',
  templateUrl: './critical-info.component.html',
  styleUrls: ['./critical-info.component.scss']
})
export class CriticalInfoComponent implements OnChanges {
  @Input()
  settings: SchematicsItemSettings;

  @Input()
  x;

  @Input()
  y;

  @Input() source: string;

  criticalInfos: Array<CriticalInfo> = [];

  private url: string;

  constructor(
    private router: Router,
    private localstorage: LocalStorageService,
    private pageInfoService: PageInfoService
  ) {
    this.url = this.router.url;
  }

  ngOnChanges() {
    this.criticalInfos = [];
    this.groupErrorsAndWarnings();
  }

  redirectTo(level: CriticalItemLevel) {
    let item = 'committee';
    this.setCurrentPage(item);
    let url = this.url.substr(0, this.url.lastIndexOf('/'));

    if(level == CriticalItemLevel.Level2) {
      url = `${url}/${item}?tab=exceptions&chevron=${this.source}`;
    } else {
      url = `${url}/${item}?tab=validations&chevron=${this.source}`;
    }

    this.router.navigateByUrl(url);
  }

  setCurrentPage(page: any){
    this.localstorage.store(environment.currentpage, page);
    this.pageInfoService.setCurrentPage(page);
  }

  private groupErrorsAndWarnings() {
    let validationsLevel1Info = new CriticalInfo(
      CriticalItemLevel.Level1,
      SchematicStatus.warning,
      this.settings.warnings.filter(w => w.level == CriticalItemLevel.Level1),
      'warnings'
    );

    if (validationsLevel1Info.items.length > 0) {
      this.criticalInfos.push(validationsLevel1Info);
    }

    let validationsLevel2Info = new CriticalInfo(
      CriticalItemLevel.Level1,
      SchematicStatus.error,
      this.settings.errors.filter(e => e.level == CriticalItemLevel.Level1),
      'errors'
    );

    if (validationsLevel2Info.items.length > 0) {
      this.criticalInfos.push(validationsLevel2Info);
    }

    let exceptionsLevel1Info = new CriticalInfo(
      CriticalItemLevel.Level2,
      SchematicStatus.warning,
      this.settings.warnings.filter(w => w.level == CriticalItemLevel.Level2),
      'warnings'
    );

    if (exceptionsLevel1Info.items.length > 0) {
      this.criticalInfos.push(exceptionsLevel1Info);
    }

    let exceptionsLevel2Info = new CriticalInfo(
      CriticalItemLevel.Level2,
      SchematicStatus.error,
      this.settings.errors.filter(e => e.level == CriticalItemLevel.Level2),
      'errors'
    );

    if (exceptionsLevel2Info.items.length > 0) {
      this.criticalInfos.push(exceptionsLevel2Info);
    }
  }

  getX(level: CriticalItemLevel, index: number) {
    if (level == CriticalItemLevel.Level2) {
      if (index == 0) {
        return 0;
      }
      return index * 24 - 3;
    }

    if (index == 0) {
      return 3;
    }

    if (index > 0) {
      return index * 22 + 3;
    }
  }
}
