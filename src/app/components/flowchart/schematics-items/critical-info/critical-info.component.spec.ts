import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CriticalInfoComponent } from './critical-info.component';

describe('CriticalInfoComponent', () => {
  let component: CriticalInfoComponent;
  let fixture: ComponentFixture<CriticalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CriticalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriticalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
