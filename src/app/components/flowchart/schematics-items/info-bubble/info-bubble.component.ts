import { Component, OnInit, Input } from '@angular/core';
import { SchematicStatus } from "@lenda/models/schematics/base-entities/schematic-status.enum";
import { SchematicsItemSettings } from '@lenda/models/schematics/schematics-item-settings.model';

@Component({
  selector: '[app-info-bubble]',
  templateUrl: './info-bubble.component.html',
  styleUrls: ['./info-bubble.component.scss']
})
export class InfoBubbleComponent implements OnInit {
  @Input() item: SchematicsItemSettings;
  @Input() x;
  @Input() y;
  @Input() cx;
  @Input() cy: number = 0;
  @Input() source;
  @Input() bubbleType: SchematicStatus = SchematicStatus.success;

  radius = 9;
  xMinus = 3;

  constructor() { }

  ngOnInit() {
    if(this.item.infos.length > 9){
      this.xMinus = 6;
    }
    if(this.item.infos.length > 99){
      this.xMinus = 10;
      this.radius = 12;
    }
  }

}
