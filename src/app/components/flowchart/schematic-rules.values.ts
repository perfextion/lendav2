export const ExceptionTabs = {
  summary: 2,
  borrower: 3,
  crop: 4,
  farm: 5,
  insurance: 6,
  budget: 7,
  optimizer: 13,
  collateral: 14,
  committee: 15
};

export const ValidationTabs = {
  farmer: 'farmer',
  borrower: 'borrower',
  crop: 'crop',
  farm: 'farm',
  insurance: 'insurance',
  budget: 'budget',
  optimizer: 'optimizer',
  collateral: 'collateral',
  committee: 'committee',
  underwriting: 'underwriting',
  prerequisiteConditions: 'prerequisiteConditions',
  closing: 'closing'
};
