export class CropItem {
  cropCode: string;
  acres: string;
  item: CropItemWithIcon;
  crop_type: string;
  irr_acres: string;
  ni_acres: string;
  crop_name: string;
  crop_name_ellipsis: string;
}

export class CropItemWithIcon {
  text: string;
  icon: string;
}
