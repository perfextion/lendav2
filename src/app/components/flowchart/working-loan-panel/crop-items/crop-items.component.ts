import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

import * as _ from 'lodash';

import { SchematicsSettings } from '@lenda/models/schematics/index.model';
import { loan_model } from '@lenda/models/loanmodel';
import { SchematicSettings } from '@lenda/preferences/models/user-settings/schematic.model';
import { CropItemWithIcon, CropItem } from './crop-item.model';
import { IDictionary } from '@lenda/services/common-utils';

@Component({
  selector: '[app-crop-items]',
  templateUrl: './crop-items.component.html',
  styleUrls: ['./crop-items.component.scss']
})
export class CropItemsComponent implements OnChanges {
  @Input() schematicPreferences: SchematicSettings;
  @Input() schematicSettings: SchematicsSettings;
  @Input() currentSelectedLoan: loan_model;

  public cropItems: Array<CropItem>;

  @Input() cropList: IDictionary<string>;

  constructor() {
    this.cropItems = [];
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.currentSelectedLoan) {
      this.cropItems = [];
      this.getCropItems();
    }
  }

  /**
   * Gets crop items
   */
  private getCropItems() {
    let formatter = new Intl.NumberFormat('en-US', {
      maximumFractionDigits: 1,
      minimumFractionDigits: 1
    });

    let crops = this.currentSelectedLoan.LoanCrops.filter(
      a => a.ActionStatus != 3
    );

    crops = _.sortBy(crops, ['Crop_Code', 'Crop_ID']);

    for (let crop of crops) {
      let item = this.getItemByCropCode(crop.Crop_Code);
      let crop_name = this.getCropName(item.text, crop.Crop_Type_Code) || '';

      this.cropItems.push({
        cropCode: crop.Crop_Code,
        acres: `Total ${formatter.format(crop.Acres || 0)} Ac`,
        item: item,
        crop_type: crop.Crop_Type_Code,
        irr_acres: `Irr ${formatter.format(crop.Irr_Acres || 0)} Ac`,
        ni_acres: `NI ${formatter.format(crop.NI_Acres || 0)} Ac`,
        crop_name: this.getCropName(item.text, crop.Crop_Type_Code),
        crop_name_ellipsis: crop_name.length > 12 ? crop_name.substr(0, 10) + '...' : crop_name
      });
    }
  }

  private getCropName(name: string, type_code: string) {
    if (type_code == '-' || !type_code) {
      return name;
    }

    return `${name} (${type_code})`;
  }

  /**
   * Gets shifting index based on item index
   * @param index index of item
   */
  public getTranslateForItem(index: number) {
    return `translate(${index * 115}, 0)`;
  }

  /**
   * Finds out centering of text based on text length
   * @param text
   */
  public translateForText(text: string) {
    if (text.length <= 4) {
      return 0;
    }
    return `-${text.length * 1.35}`;
  }

  /**
   * Maps crop code with icon to be displayed
   * @param cropCode crop code
   */
  private getItemByCropCode(cropCode: string): CropItemWithIcon {
    switch (cropCode) {
      case 'CRN':
      case 'SWC':
        return {
          text: 'Corn',
          icon: '#corn'
        };

      case 'SOY':
        return {
          text: 'Soybean',
          icon: '#soybeans-in-pod'
        };

      case 'WHT':
        return {
          text: 'Wheat',
          icon: '#wheat-grain-close-up'
        };

      case 'COT':
        return {
          text: 'Cotton',
          icon: '#cotton'
        };

      case 'SRG':
        return {
          text: 'Sorghum',
          icon: '#gluten'
        };

      case 'RIC':
        return {
          text: 'Rice',
          icon: '#rice'
        };

      case 'SWP':
        return {
          text: 'Sweet Potato',
          icon: '#potato'
        };

      case 'TBC':
        return {
          text: 'Tobacco',
          icon: '#tobacco'
        };

      case 'PNT':
        return {
          text: 'Peanuts',
          icon: '#peanut'
        };

      default:
        return {
          text: this.cropList[cropCode] || cropCode,
          icon: '#sprout'
        };
    }
  }

  public get dotsY() {
    if (this.schematicPreferences) {
      if (this.schematicPreferences.isEnhanceDetailActive) {
        return 70;
      }
      return 30;
    }
    return 30;
  }
}
