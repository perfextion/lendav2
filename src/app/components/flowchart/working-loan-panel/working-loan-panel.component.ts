import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { SchematicsSettings } from '@lenda/models/schematics/index.model';
import { loan_model, LoanGroup } from '@lenda/models/loanmodel';
import { SchematicSettings } from '@lenda/preferences/models/user-settings/schematic.model';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { IDictionary } from '@lenda/services/common-utils';
import { LayoutService } from '@lenda/shared/layout/layout.service';
import { PageInfoService } from '@lenda/services/page-info.service';
import {
  AutoUnsubscribe,
  SubscriptionList
} from '@lenda/services/auto-unsubscribe';

@Component({
  selector: '[app-working-loan-panel]',
  templateUrl: './working-loan-panel.component.html',
  styleUrls: ['./working-loan-panel.component.scss']
})
@AutoUnsubscribe({
  subscriptionKey: 'subscriptionList'
})
export class WorkingLoanPanelComponent implements OnInit {
  @Input()
  schematicSettings: SchematicsSettings;

  @Input()
  currentSelectedLoan: loan_model;

  @Input()
  schematicPreferences: SchematicSettings;

  @Input()
  loanGroups: Array<LoanGroup>;

  private refdata: RefDataModel;
  public cropItems: IDictionary<string>;

  public url: string;

  @Input()
  public hasSpouse: boolean;

  private isRightSidebarExpanded = false;
  private subscriptionList = new SubscriptionList();

  constructor(
    private router: Router,
    private layoutService: LayoutService,
    private localstorage: LocalStorageService,
    private pageInfoService: PageInfoService
  ) {
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);

    let uniqueCrops = _.uniqBy(this.refdata.CropList, a => a.Crop_Code);

    this.cropItems = {};
    uniqueCrops.forEach(c => {
      this.cropItems[c.Crop_Code] = c.Crop_Name;
    });
  }

  public get Loan_Status() {
    return this.currentSelectedLoan.LoanMaster.Loan_Status;
  }

  ngOnInit() {
    this.url = this.router.url;

    this.subscriptionList.rightSidebarExpandedSub = this.layoutService
      .isRightSidebarExpanded()
      .subscribe(value => {
        this.isRightSidebarExpanded = value;
      });
  }

  public redirectTo(item: string) {
    this.setCurrentPage(item);
    let url = this.url.substr(0, this.url.lastIndexOf('/'));
    this.router.navigateByUrl(`${url}/${item}`);
  }

  private setCurrentPage(page: any) {
    this.localstorage.store(environment.currentpage, page);
    this.pageInfoService.setCurrentPage(page);
  }

  public openDecide() {
    this.isRightSidebarExpanded = !this.isRightSidebarExpanded;
    this.layoutService.toggleRightSidebar(this.isRightSidebarExpanded);
  }
}
