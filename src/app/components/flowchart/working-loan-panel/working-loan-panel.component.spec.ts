import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkingLoanPanelComponent } from './working-loan-panel.component';

describe('WorkingLoanPanelComponent', () => {
  let component: WorkingLoanPanelComponent;
  let fixture: ComponentFixture<WorkingLoanPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkingLoanPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkingLoanPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
