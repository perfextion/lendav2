import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { SchematicsSettings } from "@lenda/models/schematics/index.model";
import { SchematicSettings } from "@lenda/preferences/models/user-settings/schematic.model";
import { environment } from '@env/environment.prod';
import { loan_model } from '@lenda/models/loanmodel';
import { Loan_Disburse } from '@lenda/models/disbursemodels';
import { Helper } from '@lenda/services/math.helper';

@Component({
  selector: '[app-loan-management-panel]',
  templateUrl: './loan-management-panel.component.html',
  styleUrls: ['./loan-management-panel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoanManagementPanelComponent implements OnInit {
  @Input() schematicSettings: SchematicsSettings;
  @Input() schematicPreferences: SchematicSettings;
  @Input() currentSelectedLoan: loan_model;


  constructor() { }

  ngOnInit() {
  }

  gotoNortridge() {
    window.open(environment.notridgeURL, '_blank', 'toolbar=0');
  }


  getX(text: string) {
    if(text) {
      if(text.length > 6) {
        return -text.length;
      }

      if(text.length == 2) {
        return 12;
      }

      if(text.length == 3) {
        return 10;
      }

      if(text.length == 4) {
        return 8;
      }

      if(text.length == 5) {
        return 3;
      }

      if(text.length == 6) {
        return 0;
      }
    }
    return 2;
  }


  get usedPercent() {
    if(this.currentSelectedLoan && this.currentSelectedLoan.Disbursements && this.currentSelectedLoan.LoanBudget) {
      let used = 0;
      let total_budget = 0;

      this.currentSelectedLoan.LoanBudget.forEach(bgt => {
        used += this.getusedbyexpensetype(bgt.Expense_Type_ID, this.currentSelectedLoan.Disbursements);
        total_budget += (bgt.ARM_Budget_Crop || 0);
      });


      return Helper.percent(used, total_budget);
    }
  }

  private getusedbyexpensetype(id: number, Disbursements: Array<Loan_Disburse>) {
    //let everything come in not looking for Status
    let sum = 0;
    Disbursements.forEach(element => {
      let expesne = element.Details.find(p => p.Budget_Expense_ID == id);
      sum = sum + (expesne == undefined ? 0 : expesne.Disburse_Detail_Requested_Amount)
    });
    return sum;
  }
}
