import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanManagementPanelComponent } from './loan-management-panel.component';

describe('LoanManagementPanelComponent', () => {
  let component: LoanManagementPanelComponent;
  let fixture: ComponentFixture<LoanManagementPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanManagementPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanManagementPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
