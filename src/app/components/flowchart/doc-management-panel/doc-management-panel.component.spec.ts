import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocManagementPanelComponent } from './doc-management-panel.component';

describe('DocManagementPanelComponent', () => {
  let component: DocManagementPanelComponent;
  let fixture: ComponentFixture<DocManagementPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocManagementPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocManagementPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
