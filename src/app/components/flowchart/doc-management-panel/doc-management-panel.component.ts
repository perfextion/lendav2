import { Component, OnInit, Input } from '@angular/core';
import { SchematicsSettings } from '@lenda/models/schematics/index.model';
import { loan_model } from '@lenda/models/loanmodel';
import { SchematicSettings } from '@lenda/preferences/models/user-settings/schematic.model';

import { Router } from '@angular/router';
import { environment } from '@env/environment.prod';

@Component({
  selector: '[app-doc-management-panel]',
  templateUrl: './doc-management-panel.component.html',
  styleUrls: ['./doc-management-panel.component.scss']
})
export class DocManagementPanelComponent implements OnInit {
  @Input()
  schematicSettings: SchematicsSettings;

  @Input()
  schematicPreferences: SchematicSettings;

  public url: string;

  constructor(private router: Router) {}

  ngOnInit() {
    this.url = this.router.url;
  }

  redirectTo(item: string) {
    let url = this.url.substr(0, this.url.lastIndexOf('/'));
    this.router.navigateByUrl( `${url}/${item}`);
  }

  gotoKL(){
    window.open(environment.knowledgeLakeURL, '_blank',"toolbar=0");
  }

  gotoKluwer() {
    window.open(environment.woltersKluwer, '_blank', 'toolbar=0');
  }
}
