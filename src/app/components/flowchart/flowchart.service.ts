import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { SchematicsSettings } from "@lenda/models/schematics/index.model";
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from "@env/environment";

@Injectable()
export class FlowchartService {
  public schematicsSettings: SchematicsSettings = new SchematicsSettings();
  public schematicsSettingsChange: Subject<SchematicsSettings> = new Subject<SchematicsSettings>();

  constructor(
    private localStorageService: LocalStorageService
  ) {
    this.getSchematicSettings();
  }

  /**
   * Retrieves schematic settings from local storage
   * TODO: Update this to use the data service
   */
  public getSchematicSettings() {
    return this.localStorageService.retrieve(environment.schematicsSettings) || new SchematicsSettings();
  }

  /**
   * Saves settings in local storage
   */
  public saveSchematicSettings(schematicsSettings: SchematicsSettings) {
    // Update in local storage
    this.localStorageService.store(environment.schematicsSettings, schematicsSettings);
  }
}
