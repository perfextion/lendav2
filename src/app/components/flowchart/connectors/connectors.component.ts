import { Component, OnInit, Input } from '@angular/core';
import { SchematicsSettings } from "@lenda/models/schematics/index.model";
import { SchematicSettings } from "@lenda/preferences/models/user-settings/schematic.model";
import { SummaryPanelService } from '../summary-panel/summary-panel.service';
import { LoanGroup, loan_model } from '@lenda/models/loanmodel';

@Component({
  selector: '[app-connectors]',
  templateUrl: './connectors.component.html',
  styleUrls: ['./connectors.component.scss']
})
export class ConnectorsComponent implements OnInit {
  @Input() schematicSettings: SchematicsSettings;
  @Input() schematicPreferences: SchematicSettings;

  @Input() currentSelectedLoan: loan_model;

  public crossCollateralItems: LoanGroup[];
  public isAffiliatedLoan: boolean;

  constructor(public summaryPanelService: SummaryPanelService) { }

  ngOnInit() {
    this.summaryPanelService.crossCollateralItems$.subscribe((val: LoanGroup[]) => {
      this.crossCollateralItems = val;

      if(this.crossCollateralItems) {
        this.isAffiliatedLoan = this.crossCollateralItems.some(A => A.IsAffiliated);
      }
    });
  }

  /**
   * Checks if enhanced detail is active or not and sets y for an item
   * based on whether extra details are shown or not
   * @param xVal y value of item
   */
  getYBasedOnEnhancedDetail(yVal, length: number = 0) {
    if (this.schematicPreferences.isEnhanceDetailActive) {
      return yVal;
    }
    return yVal - (20 * length);
  }


  getY(yVal, length = 0) {
    if (!this.schematicPreferences.isEnhanceDetailActive) {
      return yVal;
    }
    return yVal + (20 * length);
  }
}
