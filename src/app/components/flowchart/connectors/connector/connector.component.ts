import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-connector]',
  templateUrl: './connector.component.html',
  styleUrls: ['./connector.component.scss']
})
export class ConnectorComponent implements OnInit {
  @Input() uid: string;
  @Input() startX;
  @Input() startY;
  @Input() endX;
  @Input() endY;
  @Input() isMarkerVisible: boolean;
  @Input() isStepCompleted: boolean;

  constructor() { }

  ngOnInit() {
  }

  getArrowMarker() {
    return `url(#${this.uid})`;
  }

}
