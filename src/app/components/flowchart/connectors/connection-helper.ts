import { SchematicsSettings } from '@lenda/models/schematics/index.model';
import { loan_model, LoanStatus } from '@lenda/models/loanmodel';
import * as _ from 'lodash';

const TABS = [
  'borrower',
  'crop',
  'farm',
  'budget',
  'insurance',
  'optimizer',
  'collateral',
  'underwriting',
  'committee',
  'prerequisiteConditions'
];

export function resetConnectorsStatus(settings: SchematicsSettings) {
  Object.keys(settings).forEach(tab => {
    if (settings[tab]) {
      settings[tab].isStepCompleted = false;
    }
  });
}

export function setConnectorsStatus(
  settings: SchematicsSettings,
  obj: any,
  loan: loan_model
) {
  setApplyStatus(settings, loan);
  setFarmerStatus(settings, obj);
  setBorrowerStatus(settings, obj);
  setCropStatus(settings, obj);
  setIRRStatus(settings, obj);
  setNIStatus(settings, obj);
  setFarmStatus(settings, obj);
  setBudgetStatus(settings, obj);
  setInsuranceStatus(settings, obj);
  setOptimizerStatus(settings, obj);
  setCollateralStatus(settings, obj);
  setUnderwritingstatus(settings, obj);
  // setSummarystatus(settings, obj);

  if (![LoanStatus.Working].includes(loan.LoanMaster.Loan_Status)) {
    setCommitteStepStatus(settings, obj);

    setPreDocStatus(settings, obj);
    setclosingsstatus(settings, obj);
  }

  if(![LoanStatus.Working, LoanStatus.Recommended, LoanStatus.Approved, LoanStatus.Declined].includes(loan.LoanMaster.Loan_Status)) {
    setCheckoutStatus(settings, obj);
  }
}

function setCommitteStepStatus(settings: SchematicsSettings, obj: any) {
  setCommitteestatus(settings, obj);

  let anyVoteRequired = settings.committee.hoverInfos.some(
    a => a.isActionRequired != 1
  );

  if (!anyVoteRequired && settings.committee.isStepCompleted) {
    settings.committee.isStepCompleted = true;
  } else {
    settings.committee.isStepCompleted = false;
  }
}

function setApplyStatus(settings: SchematicsSettings, loan: loan_model) {
  settings.apply.isStepCompleted = loan.LoanMaster.Loan_Seq_num == 1;
}

function setBorrowerStatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'borrower', []);
}

function setFarmerStatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'farmer', []);
}

function setCropStatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'crop', ['borrower']);
}

function setIRRStatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'irr', ['borrower']);
}

function setNIStatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'ni', ['borrower']);
}

function setFarmStatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'farm', TABS.slice(0, 2));
}

function setBudgetStatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'budget', TABS.slice(0, 2));
}

function setInsuranceStatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'insurance', TABS.slice(0, 2));
}

function setOptimizerStatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'optimizer', TABS.slice(0, 5));
}

function setCollateralStatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'collateral', TABS.slice(0, 6));
}

function setUnderwritingstatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'underwriting', TABS.slice(0, 7));
}

// function setSummarystatus(settings: SchematicsSettings, obj: any) {
//   setVisibility(settings, obj, 'summary', TABS.slice(0, 8));
// }

function setCommitteestatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'committee', TABS.slice(0, 8));
}

function setPreDocStatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'prerequisiteConditions', TABS.slice(0, 9));
}

function setclosingsstatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'closing', TABS);
}

function setCheckoutStatus(settings: SchematicsSettings, obj: any) {
  setVisibility(settings, obj, 'checkout', [...TABS, 'closing']);
}

function setVisibility(
  settings: SchematicsSettings,
  obj: any,
  tab: string,
  prevTabs: Array<string> = []
) {
  if (obj[tab] > 0) {
    settings[tab].isStepCompleted = false;
  } else {
    const isPrevTabsCompleted = prevTabs.every(a => {
      if (settings[a]) {
        return settings[a].isStepCompleted;
      }
      return false;
    });

    if (isPrevTabsCompleted) {
      settings[tab].isStepCompleted = true;
    } else {
      settings[tab].isStepCompleted = false;
    }
  }
}
