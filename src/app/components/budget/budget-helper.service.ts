import { Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash'

import { Loan_Crop_Practice, Loan_Budget, loan_model, AssociationTypeCode, Loan_Association } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';
import { Helper } from '@lenda/services/math.helper';

@Injectable()
export class BudgetHelperService {
  refData: RefDataModel;

  constructor(private localStorageService: LocalStorageService) {
    //refdata get api is taking time, so for a safer side putting observer on refData
    this.localStorageService.observe(environment.referencedatakey).subscribe(refdata=>{
      if(refdata){
        this.refData = refdata;
      }
    });

    this.refData = localStorageService.retrieve(environment.referencedatakey);
  }

  validateThirdPartyAmount(loan: loan_model) {
    try {
      let total_budgets = loan.LoanBudget.filter(bgt => {
        return (
          bgt.ActionStatus != 3 &&
          bgt.Crop_Practice_ID == 0 &&
          bgt.Budget_Type.trim() == 'V' &&
          bgt.Crop_Input_Ind == '1'
        )
      });

      const third_party_associations = loan.Association.filter(
        a =>
          a.ActionStatus != 3 &&
          a.Assoc_Type_Code == AssociationTypeCode.ThirdParty
      );

      const total_third_party_budget = _.sumBy(total_budgets, bgt => bgt.Third_Party_Budget_Crop || 0);

      if(total_third_party_budget > 0) {

        if(third_party_associations.length > 0) {
          const total_third_party_amount = _.sumBy(third_party_associations, a => a.Amount || 0);

          if(total_third_party_amount < total_third_party_budget) {
            third_party_associations.forEach(a => {
              a.FC_INVALID_THR = true;
            });
          } else {
            third_party_associations.forEach(a => {
              a.FC_INVALID_THR = false;
            });
          }
        } else {
          let tp = new Loan_Association();
          tp.ActionStatus = 1;
          tp.Assoc_ID = getRandomInt(Math.pow(10, 15), Math.pow(10, 18));
          tp.Loan_Full_ID = loan.Loan_Full_ID;
          tp.Loan_ID = loan.LoanMaster.Loan_ID;
          tp.Loan_Seq_Num = loan.LoanMaster.Loan_Seq_num;
          tp.Preferred_Contact_Ind = 1;
          tp.Assoc_Type_Code = AssociationTypeCode.ThirdParty;
          tp.FC_INVALID_THR = true;
          loan.Association.push(tp);
        }
      } else {
        third_party_associations.forEach(a => {
          a.FC_INVALID_THR = false;
        })
      };
    } catch (ex) {
      if (environment.isDebugModeActive) console.log(ex);
    }
  }

  prepareCropPractice(lstCropPractice: Array<Loan_Crop_Practice>) {
    let cropList: Array<any> = this.refData.CropList;

    lstCropPractice.map(cp => {
      try {
        let crop = cropList.find(cl => cl.Crop_And_Practice_ID === cp.Crop_Practice_ID);
        if(crop) {
          cp.Fc_Crop_Code = crop.Crop_Code;
          cp.FC_CropName = crop.Crop_Name;
          cp.FC_Crop_Type = crop.Crop_Type_Code;
          cp.FC_Crop_Practice_Type = crop.Crop_Prac_Code;
          cp.FC_PracticeType = crop.Irr_Prac_Code;
        }
        return cp;
      } catch (ex) {
        if (environment.isDebugModeActive) console.log(ex);
        return cp;
      }
    });

    return lstCropPractice;
  }

  getLoanBudgetForCropPractice(lstLoanBudget: Array<Loan_Budget>, cropPracticeId: number) {
    lstLoanBudget = _.sortBy(
      lstLoanBudget.filter(budget => budget.Crop_Practice_ID === cropPracticeId && budget.ActionStatus !=3),
      this.sort_budget()
    );

    return lstLoanBudget;
  }

  sort_budget(): _.Many<_.ListIteratee<Loan_Budget>> {
    return (bgt: Loan_Budget) => {
      if (!bgt.IsCustom) {
        return bgt.Sort_order;
      }
      if (bgt.IsCustom) {
        if (bgt.ActionStatus == 1) {
          return bgt.Loan_Budget_ID + bgt.Expense_Type_ID;
        }
        if (bgt.Expense_Type_ID) {
          return bgt.Sort_order;
        }
        return bgt.Loan_Budget_ID;
      }
    };
  }

  getInsurancePremiums(localloanobject: loan_model) {
    try {
      const LoanPolicies = localloanobject.LoanPolicies.filter(a => a.Select_Ind);

      localloanobject.LoanCropPractices.forEach(lcp => {
        let cropUnits = localloanobject.LoanCropUnits.filter(cu => cu.Crop_Practice_ID == lcp.Crop_Practice_ID);

        let totalPremiumCrop = 0;
        let totalPremium = 0;

        cropUnits.forEach(cu => {
          let policies = LoanPolicies.filter(p => p.Policy_Key == cu.Crop_Unit_Key);

          if(policies.length == 0) {
            policies = LoanPolicies.filter(p => p.Policy_Key.includes(cu.Crop_Unit_Key));
          }

          let totalPremiumAcre = _.sumBy(policies, p => p.Premium);
          totalPremium += totalPremiumAcre;
          totalPremiumCrop += totalPremiumAcre * (cu.CU_Acres || 0) * (cu.FC_Insurance_Share || 100) / 100;
        });

        let bgt = localloanobject.LoanBudget.find(b => b.Crop_Practice_ID == lcp.Crop_Practice_ID && b.Expense_Type_ID == 10);

        if(bgt) {
          bgt.ARM_Budget_Acre = 0;
          bgt.Distributor_Budget_Acre = 0;

          bgt.Third_Party_Budget_Crop = totalPremiumCrop;
          bgt.Third_Party_Budget_Acre = parseFloat((totalPremiumCrop / lcp.LCP_Acres).toFixed(2));

          if(lcp.LCP_Acres == 0) {
            bgt.Third_Party_Budget_Acre = parseFloat(Helper.divide(totalPremium, cropUnits.length).toFixed(2));
          }
        }
      });

      return localloanobject;
    } catch {
      return localloanobject;
    }
  }

  multiplyPropsWithAcres(budget: Loan_Budget, acres: number) {
    budget.ARM_Budget_Crop = budget.ARM_Budget_Acre * acres;
    budget.Distributor_Budget_Crop = budget.Distributor_Budget_Acre * acres;

    if(budget.Expense_Type_ID != 10) {
      budget.Third_Party_Budget_Crop = 0;

      budget.Third_Party_Budget_Crop = budget.Third_Party_Budget_Acre * acres;
      budget.Total_Budget_Crop_ET = budget.Total_Budget_Acre * acres;
    } else {
      budget.Third_Party_Budget_Crop = parseFloat((budget.Third_Party_Budget_Crop || 0 ).toFixed(2));
      budget.Total_Budget_Crop_ET = parseFloat((budget.Third_Party_Budget_Crop || 0).toFixed(2));
    }

    return budget;
  }

  populateTotalsInCropPractice(cropPractice: Loan_Crop_Practice, lstLoanBuget: Array<Loan_Budget>) {
    // to be reviewd: the correctness of filtering logic based on practice id
    if(cropPractice) {
      let budgets = lstLoanBuget.filter(x => x.Crop_Practice_ID === cropPractice.Crop_Practice_ID);
      let totals = this.getTotals(budgets)[0];
      cropPractice.LCP_ARM_Budget = totals.ARM_Budget_Crop;
      cropPractice.LCP_Third_Party_Budget = totals.Third_Party_Budget_Crop;
      cropPractice.LCP_Distributer_Budget = totals.Distributor_Budget_Crop;
      return cropPractice;
    }
  }

  getTotals(lstloanBudget: Array<Loan_Budget>) {
    return [{
      Expense_Type_Name: 'Total',
      Other_Description_Text: 'Total',
      ARM_Budget_Acre: _.sumBy(lstloanBudget, p => parseFloat((p.ARM_Budget_Acre || 0).toString())),
      Third_Party_Budget_Acre:_.sumBy(lstloanBudget, p => parseFloat((p.Third_Party_Budget_Acre || 0).toString())),
      Distributor_Budget_Acre: _.sumBy(lstloanBudget, p => parseFloat((p.Distributor_Budget_Acre || 0).toString())),
      Total_Budget_Acre: _.sumBy(lstloanBudget, p => parseFloat((p.Total_Budget_Acre || 0).toString())),
      ARM_Budget_Crop: _.sumBy(lstloanBudget, p => parseFloat((p.ARM_Budget_Crop || 0).toString())),
      Third_Party_Budget_Crop: _.sumBy(lstloanBudget, p => parseFloat((p.Third_Party_Budget_Crop || 0).toString())),
      Distributor_Budget_Crop: _.sumBy(lstloanBudget, p => parseFloat((p.Distributor_Budget_Crop || 0).toString())),
      Total_Budget_Crop_ET: _.sumBy(lstloanBudget, p => parseFloat((p.Total_Budget_Crop_ET || 0).toString())),
    }] as Array<Loan_Budget>;
  }

  getTotalTableData(lstLoanBudget: Array<Loan_Budget>, cropPractices: Array<Loan_Crop_Practice>, loanfullid: string) {

    let loanExpenseTypes: Array<Total_Budget> = [];

    _.uniqBy(
      lstLoanBudget.filter(
      a =>
        a.ActionStatus != 3 &&
        a.Budget_Type != 'V' &&
        !!a.Expense_Type_Name
      ),
      'Expense_Type_Name'
    ).map(budget => {
      loanExpenseTypes.push({
        id : budget.Expense_Type_ID,
        name : budget.Expense_Type_Name,
        sort_order: budget.Sort_order,
        crop_input_ind: budget.Crop_Input_Ind || '0'
      });
    });

    let totalTableData: Array<Loan_Budget> = [];

    loanExpenseTypes.map(expense => {
      let budgetForExpense = new Loan_Budget();
      budgetForExpense.Loan_Budget_ID = getRandomInt();
      budgetForExpense.Expense_Type_ID = expense.id;
      budgetForExpense.Expense_Type_Name = expense.name;
      budgetForExpense.ARM_Budget_Crop = 0
      budgetForExpense.Distributor_Budget_Crop = 0
      budgetForExpense.Third_Party_Budget_Crop = 0
      budgetForExpense.Total_Budget_Crop_ET = 0
      budgetForExpense.Crop_Practice_ID = 0;
      budgetForExpense.Budget_Type = 'V';
      budgetForExpense.Loan_Full_ID = loanfullid;
      budgetForExpense.Sort_order = expense.sort_order;
      budgetForExpense.Crop_Input_Ind = expense.crop_input_ind;

      let ref_expense_type = this.refData.BudgetExpenseType.find(a => a.Budget_Expense_Type_ID == expense.id);
      if(ref_expense_type) {
        budgetForExpense.Validation_Exception_Ind = ref_expense_type.Validation_Exception_Ind;
        budgetForExpense.Exception_ID = ref_expense_type.Exception_ID;
        budgetForExpense.Validation_ID = ref_expense_type.Validation_ID;;
      } else {
        budgetForExpense.Validation_Exception_Ind = 0;
        budgetForExpense.Exception_ID = 0;
        budgetForExpense.Validation_ID = 0;
      }

      cropPractices.forEach(cp => {
        let budget = lstLoanBudget.find(b => b.Crop_Practice_ID === cp.Crop_Practice_ID && (b.Expense_Type_ID === expense.id && b.Expense_Type_Name == expense.name) && !b.Budget_Type);
        if (budget) {
          this.SumBudgets(budgetForExpense, budget);
        }
      });

      // add fixed expenses to total budget
      let fixedbudgets = lstLoanBudget.filter(budget => budget.Budget_Type == 'F' && budget.Expense_Type_ID === expense.id && budget.ActionStatus != 3);

      fixedbudgets.forEach(fixedbudget => {
        this.SumBudgets(budgetForExpense, fixedbudget);
      });

      totalTableData.push(budgetForExpense);
    });

    return totalTableData;

  }

  private SumBudgets(budgetForExpense: Loan_Budget, budget: Loan_Budget) {
    budgetForExpense.ARM_Budget_Acre += budget.ARM_Budget_Acre ? parseFloat(budget.ARM_Budget_Acre.toString()) : 0;
    budgetForExpense.Distributor_Budget_Acre += budget.Distributor_Budget_Acre ? parseFloat(budget.Distributor_Budget_Acre.toString()) : 0;
    budgetForExpense.Third_Party_Budget_Acre += budget.Third_Party_Budget_Acre ? parseFloat(budget.Third_Party_Budget_Acre.toString()) : 0;
    budgetForExpense.Total_Budget_Acre = (budgetForExpense.ARM_Budget_Acre + budgetForExpense.Distributor_Budget_Acre + budgetForExpense.Third_Party_Budget_Acre); // budget.Total_Budget_Acre ? parseFloat(budget.Total_Budget_Acre.toString()): 0;
    budgetForExpense.ARM_Budget_Crop += budget.ARM_Budget_Crop ? parseFloat(budget.ARM_Budget_Crop.toString()) : 0;
    budgetForExpense.Distributor_Budget_Crop += budget.Distributor_Budget_Crop ? parseFloat(budget.Distributor_Budget_Crop.toString()) : 0;
    budgetForExpense.Third_Party_Budget_Crop += budget.Third_Party_Budget_Crop ? parseFloat(budget.Third_Party_Budget_Crop.toString()) : 0;
    budgetForExpense.Total_Budget_Crop_ET = (budgetForExpense.ARM_Budget_Crop + budgetForExpense.Distributor_Budget_Crop + budgetForExpense.Third_Party_Budget_Crop);
  }

  prepareTotalBudget(localloanobj: loan_model) {
    let budgettotals = this.getTotalTableData(localloanobj.LoanBudget, this.prepareCropPractice(localloanobj.LoanCropPractices), localloanobj.Loan_Full_ID);

    let totalbudgetrows = localloanobj.LoanBudget.filter(
      p =>
        p.Crop_Practice_ID == 0 &&
        p.Budget_Type.trim() == 'V'
    );

    totalbudgetrows.forEach(a => {
      a.Needed_Ind = 0;
    });

    budgettotals.forEach(element => {
      let item = totalbudgetrows.find(
        p =>  p.Expense_Type_Name == element.Expense_Type_Name
      );

      let index = localloanobj.LoanBudget.indexOf(item);

      if (item == null || item == undefined) {
        element.ActionStatus = 1;
        element.Needed_Ind = 1;
        localloanobj.LoanBudget.push(element);
      } else {
        element.Loan_Budget_ID = item.Loan_Budget_ID;
        element.Needed_Ind = 1;
        item.Needed_Ind = 1;
        item.Sort_order = element.Sort_order;
        element.Notes = item.Notes;
        element.ActionStatus = item.ActionStatus;
        localloanobj.LoanBudget[index] = element;
      }
    });

    totalbudgetrows.filter(a => a.Needed_Ind == 0).forEach(a => {
      if(a.ActionStatus == 1) {
        let index = localloanobj.LoanBudget.findIndex(b => b.Loan_Budget_ID == a.Loan_Budget_ID);
        if(index > -1) {
          localloanobj.LoanBudget.splice(index, 1);
        }
      } else {
        a.ActionStatus = 3;
      }
    });
  }
}

export class Total_Budget {
  id: number;
  name: string;
  sort_order: number;
  crop_input_ind: string;
}
