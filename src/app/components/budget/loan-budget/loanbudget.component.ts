import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import {
  loan_model,
  Loan_Budget,
  Loan_Crop_Practice,
  LoanStatus,
  Loan_Crop,
  Update_Budget_Default
} from '../../../models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';
import { LoancalculationWorker } from '../../../Workers/calculations/loancalculationworker';
import { environment } from '../../../../environments/environment.prod';
import {
  numberValueSetter,
  getNumericCellEditor,
  numberWithOneDecPrecValueSetter
} from '../../../Workers/utility/aggrid/numericboxes';
import { getTextCellEditor } from '../../../Workers/utility/aggrid/textboxes';
import { BudgetHelperService } from '../budget-helper.service';
import { PublishService } from '../../../services/publish.service';
import { Page } from '@lenda/models/page.enum';
import {
  cellclassmaker,
  CellType,
  headerclassmaker,
  isgrideditable,
  calculatecolumnwidths,
  autoSizeAll
} from '../../../aggriddefinations/aggridoptions';
import {
  currencyFormatter,
  BudgetFormatter
} from '../../../aggridformatters/valueformatters';
import { ISubscription } from 'rxjs/Subscription';
import { DataService } from '@lenda/services/data.service';
import { DeleteButtonRenderer } from '@lenda/aggridcolumns/deletebuttoncolumn';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';
import { BudgetpublisherService } from '@lenda/services/budget/budgetpublisher.service';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import {
  Get_Crop_Name_By_Crop_Practice,
  Get_Distributor_Name,
  Get_Default_Budget
} from '@lenda/services/common-utils';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import {
  RefDataModel,
  RefBudgetExpenseType
} from '@lenda/models/ref-data-model';
import { BudgetAutoCompleteCellEditor } from '@lenda/aggridcolumns/budget-auto-complete-cell/budget-auto-complete-cell.component';
import { BudgetExpenseTypeNameValidation } from '@lenda/Workers/utility/validation-functions';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { errormodel } from '@lenda/models/commonmodels';
import { ToastrService } from 'ngx-toastr';
import { EditDefaultSettings } from '@lenda/preferences/models/user-settings/edit-default-settings.model';
import * as _ from 'lodash';

/// <reference path="../../../Workers/utility/aggrid/numericboxes.ts" />
declare function setmodifiedall(array, keyword): any;
declare function setmodifiedsingle(obj, keyword): any;

@Component({
  selector: 'app-loanbudget',
  templateUrl: './loanbudget.component.html',
  styleUrls: ['./loanbudget.component.scss']
})
export class LoanbudgetComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  // Following properties are added as produ build was failing
  // ----
  frameworkcomponents;
  context;
  public isprebuttonenabled = true;
  // ---
  @Input() cropPractice: Loan_Crop_Practice;
  @Input() allCropPractices: Array<Loan_Crop_Practice>;
  @Input() expanded: boolean = true;

  columnDefs: Array<any>;
  rowData: Array<any>;
  private gridApi;
  private columnApi;
  public getRowStyle;
  public pinnedBottomRowData;
  public components;
  private localLoanObject: loan_model;
  defaultAcresVal = 0;

  style = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };

  public columnsDisplaySettings: ColumnsDisplaySettings = new ColumnsDisplaySettings();

  refdata: RefDataModel;
  Budget_Expense_Types: Array<RefBudgetExpenseType>;

  private currenterrors: Array<errormodel> = [];

  editDefaultSettings: EditDefaultSettings;
  private preferencesChangeSub: ISubscription;

  get canUpdateDefault() {
    if(this.editDefaultSettings) {
      return this.editDefaultSettings.isCropBudgetEnabled;
    }
    return false;
  }

  constructor(
    private localStorageService: LocalStorageService,
    private budgetService: BudgetHelperService,
    private loanserviceworker: LoancalculationWorker,
    private publishService: PublishService,
    private dataService: DataService,
    private alertify: AlertifyService,
    private budgetpublisher: BudgetpublisherService,
    private dtss: DatetTimeStampService,
    private settingsService: SettingsService,
    private validationService: ValidationService,
    private toastr: ToastrService
  ) {
    this.frameworkcomponents = {
      deletecolumn: DeleteButtonRenderer,
      autocompleteEditor: BudgetAutoCompleteCellEditor
    };

    this.context = {
      componentParent: this
    };

    this.components = {
      numericCellEditor: getNumericCellEditor(),
      textCellEditor: getTextCellEditor()
    };

    this.getUserPreferences();
  }

  getcurrenterrors() {
    this.currenterrors = (this.localStorageService.retrieve(
      environment.errorbase
    ) as Array<errormodel>).filter(
      p => p.chevron == 'Budget_' + this.cropPractice.Crop_Practice_ID
    );

    this.validationService.highlighErrorCells(
      this.currenterrors,
      'Crop_Budget_' + this.cropPractice.Crop_Practice_ID
    );
  }

  getBudgetName(cp: Loan_Crop_Practice) {
    return Get_Crop_Name_By_Crop_Practice(cp, this.columnsDisplaySettings);
  }

  getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;
    this.editDefaultSettings = this.settingsService.preferences.userSettings.editDefaultSettings;

    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe(preferences => {
      this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
      this.editDefaultSettings = preferences.userSettings.editDefaultSettings;
    });
  }

  getAcres(cropPractice: Loan_Crop_Practice) {
    if (cropPractice.LCP_Acres) {
      return this.cropPractice.LCP_Acres.toLocaleString('en-US', {
        maximumFractionDigits: 1,
        minimumFractionDigits: 1
      });
    }
    return this.defaultAcresVal.toFixed(1);
  }

  processAllIn() {
    //this.columnApi.setColumnsVisible(['Distributor_Budget_Crop','Distributor_Budget_Acre'], false)
  }
  progressAgPro() {
    //this.columnApi.setColumnVisible('Distributor_Budget_Crop', false);
    //this.publishService.enableSync(Page.budget);
  }

  checkifpreenabled() {
    if (this.cropPractice.FC_Crop_Practice_Type == 'NI') {
      let correspcrop = this.allCropPractices.find(
        p =>
          p.FC_CropName == this.cropPractice.FC_CropName &&
          p.FC_Crop_Type == this.cropPractice.FC_Crop_Type &&
          p.FC_Crop_Practice_Type == 'Irr'
      );
      if (correspcrop == null) this.isprebuttonenabled = false;
    } else {
      this.isprebuttonenabled = true;
    }
  }

  ngOnInit() {
    this.checkifpreenabled();
    const totalRowHeader = 'Total'; // is used to check if the current row should be editable or not

    this.budgetpublisher.AllinClicked.subscribe(res => {
      this.processAllIn();
    });

    this.budgetpublisher.AgProClicked.subscribe(res => {
      this.progressAgPro();
    });

    this.localLoanObject = this.localStorageService.retrieve(
      environment.loankey
    );

    this.refdata = this.localStorageService.retrieve(
      environment.referencedatakey
    );

    this.Budget_Expense_Types = _.sortBy(this.refdata.BudgetExpenseType.filter(a => a.Fixed_Only == 0), e => e.Sort_order);

    let distName = Get_Distributor_Name(this.localLoanObject);
    let distNameTooltip = Get_Distributor_Name(this.localLoanObject, false);

    this.columnDefs = [
      {
        headerName: 'Expense',
        field: 'Expense_Type_Name',
        minWidth: 120,
        width: 120,
        maxWidth: 120,
        tooltip: params => params.data['Expense_Type_Name'],
        editable: params => {
          let status = params.context.componentParent.Loan_Status;
          return isgrideditable(params.data.IsCustom, status);
        },
        cellEditor: 'autocompleteEditor',
        cellClass: params => {
          let status = params.context.componentParent.Loan_Status;
          return cellclassmaker(CellType.Text, params.data.IsCustom == 1, status);
        },
        validations: { BudgetExpenseTypeName: BudgetExpenseTypeNameValidation }
      },
      {
        headerName: 'Per Acres Budget',
        children: [
          {
            headerName: 'ARM',
            field: 'ARM_Budget_Acre',
            cellEditor: 'numericCellEditor',
            headerClass: headerclassmaker(CellType.Integer),
            cellClass: params => {
              let status = params.context.componentParent.Loan_Status;
              return cellclassmaker(CellType.Integer, params.data.Standard_Ind != 2, status);
            },
            valueSetter: params => numberWithOneDecPrecValueSetter(params, 2),
            valueFormatter: params => currencyFormatter(params, 2),
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            editable: params => {
              let status = params.context.componentParent.Loan_Status;
              let val = params.data.Expense_Type_Name !== totalRowHeader + ':';
              return isgrideditable(
                val &&
                  params.data.Standard_Ind != 2 &&
                  params.data['Expense_Type_Name'] != 'Total',
                  status
              );
            }
          },
          {
            headerName: distName,
            field: 'Distributor_Budget_Acre',
            headerTooltip: distNameTooltip,
            cellEditor: 'numericCellEditor',
            valueSetter: params => numberWithOneDecPrecValueSetter(params, 2),
            headerClass: headerclassmaker(CellType.Integer),
            cellClass: params => {
              let status = params.context.componentParent.Loan_Status;
              return cellclassmaker(CellType.Integer, params.data.Standard_Ind != 2, status);
            },
            valueFormatter: params => currencyFormatter(params, 2),
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            editable: params => {
              let status = params.context.componentParent.Loan_Status;
              let val = params.data.Expense_Type_Name !== totalRowHeader + ':';
              return isgrideditable(
                val &&
                  params.data.Standard_Ind != 2 &&
                  params.data['Expense_Type_Name'] != 'Total',
                  status
              );
            }
          },
          {
            headerName: '3rd Party',
            field: 'Third_Party_Budget_Acre',
            cellEditor: 'numericCellEditor',
            valueSetter: params => numberWithOneDecPrecValueSetter(params, 2),
            headerClass: headerclassmaker(CellType.Integer),
            cellClass: params => {
              let status = params.context.componentParent.Loan_Status;
              return cellclassmaker(CellType.Integer, params.data.Standard_Ind != 2, status);
            },
            valueFormatter: params => currencyFormatter(params, 2),
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            editable: params => {
              let status = params.context.componentParent.Loan_Status;
              let val = params.data.Expense_Type_Name !== totalRowHeader + ':';
              return isgrideditable(
                val &&
                  params.data.Standard_Ind != 2 &&
                  params.data['Expense_Type_Name'] != 'Total',
                  status
              );
            }
          },
          {
            headerName: totalRowHeader,
            field: 'Total_Budget_Acre',
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            editable: false,
            headerClass: headerclassmaker(CellType.Integer),
            cellClass: cellclassmaker(CellType.Integer, false),
            valueFormatter: params => currencyFormatter(params, 2)
          }
        ]
      },
      {
        headerName: 'Crop Budget',
        headerClass: 'headerinsurance',
        children: [
          {
            headerName: 'ARM',
            field: 'ARM_Budget_Crop',
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            headerClass:
              'headerinsurance ' + headerclassmaker(CellType.Integer),
            cellClass: cellclassmaker(CellType.Integer, false),
            editable: false,
            valueFormatter: params => BudgetFormatter(params.value)
          },
          {
            headerName: distName ? distName : 'Distributor',
            field: 'Distributor_Budget_Crop',
            headerTooltip: distNameTooltip ? distNameTooltip : 'Distributor',
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            editable: false,
            headerClass:
              'headerinsurance ' + headerclassmaker(CellType.Integer),
            cellClass: cellclassmaker(CellType.Integer, false),
            valueFormatter: params => BudgetFormatter(params.value)
          },
          {
            headerName: '3rd Party',
            field: 'Third_Party_Budget_Crop',
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            editable: false,
            headerClass:
              'headerinsurance ' + headerclassmaker(CellType.Integer),
            cellClass: cellclassmaker(CellType.Integer, false),
            valueFormatter: params => BudgetFormatter(params.value)
          },
          {
            headerName: 'Total',
            field: 'Total_Budget_Crop_ET',
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            editable: false,
            headerClass:
              'headerinsurance ' + headerclassmaker(CellType.Integer),
            cellClass: cellclassmaker(CellType.Integer, false),
            valueFormatter: params => BudgetFormatter(params.value)
          }
        ]
      },
      {
        headerName: '',
        field: 'value',
        minWidth: 60,
        width: 60,
        maxWidth: 60,
        cellRenderer: 'deletecolumn',
        suppressToolPanel: true
      }
    ];

    this.getRowStyle = function(params) {
      if (params.node.rowPinned) {
        return {
          'font-weight': 'bold',
          'background-color': '#F5F7F7',
          'pointer-events': 'none'
        };
      }
    };

    //TODO-SANKET can we have obsever one level up instead of for each cropPractice ?
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localLoanObject = res;

      if (environment.isDebugModeActive) console.time('loanbudget observer' + this.cropPractice.Crop_Practice_ID);

      if (
        this.localLoanObject.srccomponentedit === 'LoanBudgetComponent' + this.cropPractice.Crop_Practice_ID
      ) {
        let budgets = [
          ...this.budgetService.getLoanBudgetForCropPractice(
            this.localLoanObject.LoanBudget,
            this.cropPractice.Crop_Practice_ID
          )
        ];

        this.rowData[this.localLoanObject.lasteditrowindex] = budgets[this.localLoanObject.lasteditrowindex];
        this.pinnedBottomRowData = this.budgetService.getTotals(this.rowData);

        this.gridApi.refreshCells();
      } else if(
        this.localLoanObject.srccomponentedit == 'CopyPrevLoanBudgetComponent' &&
        this.localLoanObject.lasteditrowindex == this.cropPractice.Crop_Practice_ID
        ) {
          this.localLoanObject = res;
          this.bindData(this.localLoanObject);
          this.gridApi.refreshCells();
      } else if(
        this.localLoanObject.srccomponentedit == 'FixedExpenseBudget' ||
        this.localLoanObject.srccomponentedit == 'DELETE_FixedExpenseBudget'||
        (this.localLoanObject.srccomponentedit && this.localLoanObject.srccomponentedit.includes('LoanBudgetComponent'))
        ) {
        if (environment.isDebugModeActive) console.timeEnd('loanbudget observer'+ this.cropPractice.Crop_Practice_ID);
        return;
      } else {
        this.localLoanObject = res;
        this.bindData(this.localLoanObject);
        this.gridApi.refreshCells();
      }

      this.setValidationErrorForLoanBudget();

      setTimeout(() => {
        this.getcurrenterrors();

        setmodifiedall(
          this.localStorageService.retrieve(environment.modifiedbase),
          'Lbg_'
        );

        this.gridApi.sizeColumnsToFit();
        autoSizeAll(this.columnApi);
        this.adjustToFitAgGrid();
      }, 5);

      if (environment.isDebugModeActive) console.timeEnd('loanbudget observer'+ this.cropPractice.Crop_Practice_ID);
    });

    this.rowData = [];
    this.bindData(this.localLoanObject);
  }

  checkdistributorcolumnsvisiility() {
    if (this.rowData.find(p => p.Distributor_Budget_Acre != 0) == undefined) {
      //hide
      //this.columnApi.setColumnVisible("Distributor_Budget_Acre",false);
    }

    if (this.rowData.find(p => p.Distributor_Budget_Crop != 0) == undefined) {
      //hide
      //this.columnApi.setColumnVisible("Distributor_Budget_Crop",false);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  bindData(localLoanObject: loan_model) {
    let lstLoanBudget = localLoanObject.LoanBudget;

    this.rowData = [];
    this.rowData =
      localLoanObject.LoanCropUnits !== null
        ? [
            ...this.budgetService.getLoanBudgetForCropPractice(
              lstLoanBudget,
              this.cropPractice.Crop_Practice_ID
            )
          ]
        : [];

    this.gridApi && this.gridApi.refreshCells();
    //this.rowData =this.budgetService.getLoanBudgetForCropPractice(loanBudget, this.cropPractice.Crop_Practice_ID, this.cropPractice.LCP_Acres);
    this.pinnedBottomRowData = this.budgetService.getTotals(this.rowData);
    this.calculatebudgettotals();

    setTimeout(() => {
      this.getcurrenterrors();

      setmodifiedall(
        this.localStorageService.retrieve(environment.modifiedbase),
        'Lbg_'
      );
    }, 10);
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;

    //these two methods remove side option toolbar , sets column , removes header menu
    // setgriddefaults(this.gridApi, this.columnApi);
    // params.api.sizeColumnsToFit();
    ///////////////////////////////////////////////////////////////////////////////////
    this.checkdistributorcolumnsvisiility();
    this.adjustToFitAgGrid();
    this.setValidationErrorForLoanBudget();
  }

  isLoanEditable() {
    return (
      !this.localLoanObject ||
      this.localLoanObject.LoanMaster.Loan_Status === LoanStatus.Working
    );
  }

  onBtExport() {
    const params = {
      skipHeader: false,
      columnGroups: true,
      skipFooters: false,
      skipGroups: false,
      skipPinnedTop: false,
      skipPinnedBottom: false,
      allColumns: true,
      fileName: `Budget_${this.cropPractice.FC_CropName}_${this.cropPractice.FC_PracticeType}_Loan_${this.localLoanObject.Loan_Full_ID}_${this.dtss.getTimeStamp()}.xlxs`,
      sheetName: 'Sheet 1'
    };
    // Export Data to Excel file
    this.gridApi.exportDataAsExcel(params);
  }

  adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  displayedColumnsChanged(params) {
    this.adjustToFitAgGrid();
  }

  rowvaluechanged(value: any) {
    if (
      value &&
      (value.oldValue == value.value || (!value.oldValue && !value.value))
    )
      return;

    if (!value && !value.column && !value.column.colId) return;

    //---------Modified yellow values-------------
    //MODIFIED YELLOW VALUES
    let modifiedvalues = this.localStorageService.retrieve(
      environment.modifiedbase
    ) as Array<String>;

    if (!modifiedvalues.includes('Lbg_' + value.data.Loan_Budget_ID + '_' + value.colDef.field)
    ) {
      modifiedvalues.push('Lbg_' + value.data.Loan_Budget_ID + '_' + value.colDef.field);
      this.localStorageService.store(environment.modifiedbase, modifiedvalues);
      setmodifiedsingle('Lbg_' + value.data.Loan_Budget_ID + '_' + value.colDef.field,'Lbg_');
    }

    //MODIFIED YELLOW VALUES
    //--------------------------------------------
    let budget = this.localLoanObject.LoanBudget.find(
      bg =>
        bg.ActionStatus != 3 && bg.Loan_Budget_ID == value.data.Loan_Budget_ID
    );

    if (value.data.ActionStatus != 1) {
      budget.ActionStatus = 2;
    }

    this.checkIfDefaultBudgetChanged(budget, this.cropPractice, value.colDef.field);

    budget.Total_Budget_Acre =
      parseFloat(budget.ARM_Budget_Acre.toString()) +
      parseFloat(budget.Distributor_Budget_Acre.toString()) +
      parseFloat(budget.Third_Party_Budget_Acre.toString());

      this.calculatebudgettotals();

    //this shall have the last edit
    this.localLoanObject.srccomponentedit = 'LoanBudgetComponent' + this.cropPractice.Crop_Practice_ID;
    this.localLoanObject.lasteditrowindex = value.rowIndex;
    this.loanserviceworker.performcalculationonloanobject(this.localLoanObject);
    this.publishService.enableSync(Page.budget);

    this.setValidationErrorForLoanBudget();
    this.getcurrenterrors();
  }

  private checkIfDefaultBudgetChanged(bgt: Loan_Budget, cp: Loan_Crop_Practice, field: string) {
    if(this.canUpdateDefault && bgt.Standard_Ind == 1 && field == 'ARM_Budget_Acre' && !cp.Update_Budget_Ind) {
      let crop = this.refdata.CropList.find(a => a.Crop_And_Practice_ID == cp.Crop_Practice_ID);
      if(crop) {

        let Update_Budget_Default = <Update_Budget_Default>{
          Crop_Full_Key: crop.Crop_Full_Key,
          Z_Office_ID: this.localLoanObject.LoanMaster.Office_ID,
          Z_Region_ID: this.localLoanObject.LoanMaster.Region_ID,
          Crop_Practice_ID: cp.Crop_Practice_ID
        };

        if(!this.localLoanObject.Update_Budget_Defaults) {
          this.localLoanObject.Update_Budget_Defaults = [];
        }

        if(!this.localLoanObject.Update_Budget_Defaults.includes(Update_Budget_Default)) {
          cp.Update_Budget_Ind = 1;
          this.localLoanObject.Update_Budget_Ind = 1;
          this.localLoanObject.Update_Budget_Defaults.push(Update_Budget_Default);
        }
      }
    }
  }

  addrow() {
    if (!this.isLoanEditable()) {
      return;
    }

    let isAnyRowWithoutExpenseName = this.rowData.some(
      a => !a.Expense_Type_Name
    );

    if(isAnyRowWithoutExpenseName) {
      this.alertify.alert('Alert', 'Warning: You already have added a row without Expense name');
    } else {

      let budget = new Loan_Budget();
      budget.Crop_Practice_ID = this.cropPractice.Crop_Practice_ID;
      budget.ActionStatus = 1;
      budget.IsCustom = 1;
      budget.Loan_Full_ID = this.localLoanObject.Loan_Full_ID;
      budget.Z_Loan_ID = this.localLoanObject.LoanMaster.Loan_ID;
      budget.Z_Loan_Seq_num = this.localLoanObject.LoanMaster.Loan_Seq_num;
      budget.Loan_Budget_ID = getRandomInt(Math.pow(10, 5), Math.pow(10, 9));
      budget.Expense_Type_ID = 0;
      budget.Sort_order = budget.Loan_Budget_ID * 100 + 1;

      this.localLoanObject.LoanBudget.push(budget);

      this.rowData.push(budget);

      this.gridApi.setRowData(this.rowData);
      this.gridApi.startEditingCell({
        rowIndex: this.rowData.length - 1,
        colKey: 'Expense_Type_Name'
      });

      this.loanserviceworker.performcalculationonloanobject(this.localLoanObject);
    }
  }

  public gridOptions = {
    getRowNodeId: function(data) {
      return 'Lbg_' + data.Loan_Budget_ID;
    }
  };

  DeleteClicked(rowIndex: any) {
    this.alertify
      .confirm('Confirm', 'Do you Really Want to Delete this Record?')
      .subscribe(res => {
        if (res == true) {
          let obj: Loan_Budget = this.rowData[rowIndex];

          let budgetIndex = this.localLoanObject.LoanBudget.findIndex(
            budget => budget == obj
          );

          if (obj.ActionStatus == 1) {
            this.localLoanObject.LoanBudget.splice(budgetIndex, 1);
          } else {
            this.localLoanObject.LoanBudget[budgetIndex].ActionStatus = 3;
          }

          //remove respective total budget expense

          //check if the same expennse exist in other practices or not
          let otherBudgetWithSameExpense = this.localLoanObject.LoanBudget.filter(
            lb =>
              lb.Expense_Type_ID == obj.Expense_Type_ID &&
              lb.Crop_Practice_ID != 0 &&
              lb.ActionStatus != 3
          );

          if (otherBudgetWithSameExpense.length == 0) {
            //if no other crop practice budgets have same expense
            let respectiveTotalBudgetIndex = this.localLoanObject.LoanBudget.findIndex(
              lb =>
                lb.Expense_Type_ID == obj.Expense_Type_ID &&
                lb.Crop_Practice_ID == 0
            );

            if(respectiveTotalBudgetIndex > -1) {
              if (
                this.localLoanObject.LoanBudget[respectiveTotalBudgetIndex].ActionStatus == 1
              ) {
                this.localLoanObject.LoanBudget.splice(respectiveTotalBudgetIndex,1);
              } else {
                this.localLoanObject.LoanBudget[respectiveTotalBudgetIndex].ActionStatus = 3;
              }
            }
          }
          this.localLoanObject.srccomponentedit = undefined;
          this.localLoanObject.lasteditrowindex = undefined;
        }

        this.loanserviceworker.performcalculationonloanobject(
          this.localLoanObject
        );
        this.publishService.enableSync(Page.budget);

        this.setValidationErrorForLoanBudget();
        this.getcurrenterrors();
      });
  }

  private calculatebudgettotals() {
    for (let i = 0; i < this.localLoanObject.LoanBudget.length; i++) {
      let currentBudget = this.localLoanObject.LoanBudget[i];
      if (currentBudget.Crop_Practice_ID != 0) {
        let cropPractice = this.localLoanObject.LoanCropPractices.find(
          cp => cp.Crop_Practice_ID === currentBudget.Crop_Practice_ID
        );
        currentBudget = this.budgetService.multiplyPropsWithAcres(
          currentBudget,
          cropPractice.LCP_Acres
        );
      }
    }
    //budget = this.budgetService.multiplyPropsWithAcres(budget, this.cropPractice.LCP_Acres);
    //this.localLoanObject = this.budgetService.caculateTotalsBeforeStore(this.localLoanObject);
    let cropPractice = this.localLoanObject.LoanCropPractices.find(
      cp => cp.Crop_Practice_ID === this.cropPractice.Crop_Practice_ID
    );

    if(cropPractice) {
      cropPractice = this.budgetService.populateTotalsInCropPractice(
        cropPractice,
        this.localLoanObject.LoanBudget
      );
    }
  }

  // getgridheight() {
  //   this.style.height = (29 * (this.rowData.length + 3)).toString() + "px";
  // }

  copyIRRData() {
    this.alertify
      .confirm('Confirm', 'Do you really want to copy above Record?')
      .subscribe(res => {
        if (res) {
          let copyFromCropPractise = this.allCropPractices.find(
            cp =>
              cp.FC_CropName == this.cropPractice.FC_CropName &&
              cp.FC_Crop_Type == this.cropPractice.FC_Crop_Type &&
              cp.FC_PracticeType != this.cropPractice.FC_PracticeType
          );

          if (copyFromCropPractise) {
            let srcObj = this.localLoanObject.LoanBudget.filter(
                a =>
                  a.Crop_Practice_ID == copyFromCropPractise.Crop_Practice_ID &&
                  a.ActionStatus != 3
              ) || [];

            let targetObj = this.rowData;

            for (let ex of srcObj) {
              let expense = targetObj.find(
                a => a.Expense_Type_Name == ex.Expense_Type_Name
              );

              if (expense && expense.Expense_Type_ID != 10) {
                if (expense.ARM_Budget_Acre != ex.ARM_Budget_Acre) {
                  expense.ARM_Budget_Acre = ex.ARM_Budget_Acre;
                  this.modifycell(expense.Loan_Budget_ID, 'ARM_Budget_Acre');
                  this.checkIfDefaultBudgetChanged(expense, this.cropPractice, 'ARM_Budget_Acre');
                }

                if (
                  expense.Distributor_Budget_Acre != ex.Distributor_Budget_Acre
                ) {
                  expense.Distributor_Budget_Acre = ex.Distributor_Budget_Acre;
                  this.modifycell(
                    expense.Loan_Budget_ID,
                    'Distributor_Budget_Acre'
                  );
                }

                if (
                  expense.Third_Party_Budget_Acre != ex.Third_Party_Budget_Acre
                ) {
                  expense.Third_Party_Budget_Acre = ex.Third_Party_Budget_Acre;
                  this.modifycell(
                    expense.Loan_Budget_ID,
                    'Third_Party_Budget_Acre'
                  );
                }

                expense.Total_Budget_Acre =
                  parseFloat(expense.ARM_Budget_Acre) +
                  parseFloat(expense.Distributor_Budget_Acre) +
                  parseFloat(expense.Third_Party_Budget_Acre);

                if (expense.ActionStatus != 1) {
                  expense.ActionStatus = 2;
                }
              }
            }

            this.calculatebudgettotals();
            this.localLoanObject.srccomponentedit = 'CopyPrevLoanBudgetComponent';
            this.localLoanObject.lasteditrowindex = this.cropPractice.Crop_Practice_ID;
            this.loanserviceworker.performcalculationonloanobject(
              this.localLoanObject
            );
            this.publishService.enableSync(Page.budget);
            this.toastr.success('Data copied successfully.');
          }
        }
      });
  }

  get Loan_Status () {
    if(this.localLoanObject) {
      return this.localLoanObject.LoanMaster.Loan_Status;
    }
    return '';
  }

  private modifycell(Loan_Budget_ID, field) {
    let modifiedvalues = this.localStorageService.retrieve(
      environment.modifiedbase
    ) as Array<String>;

    if (!modifiedvalues.includes('Lbg_' + Loan_Budget_ID + '_' + field)) {
      modifiedvalues.push('Lbg_' + Loan_Budget_ID + '_' + field);
      this.localStorageService.store(environment.modifiedbase, modifiedvalues);
      setmodifiedsingle('Lbg_' + Loan_Budget_ID + '_' + field, 'Lbg_');
    }
  }

  setValidationErrorForLoanBudget() {
    if (environment.isDebugModeActive) console.time('Validation Crop Budget');
    if (this.cropPractice) {
      // this.hyperfieldValidation.processHyperfieldValidation(this.chevronId, this.rowData, this.associationChevron.Chevron_Code);

      this.validationService.validateTableFields<Loan_Budget>(
        this.rowData,
        this.columnDefs,
        Page.budget,
        'Lbg_',
        'Budget_' + this.cropPractice.Crop_Practice_ID,
        'Loan_Budget_ID'
      );
    }

    if (environment.isDebugModeActive)
      console.timeEnd('Validation Crop Budget');
  }
}
