import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedRevenueComponent } from './fixed-revenue.component';

describe('FixedRevenueComponent', () => {
  let component: FixedRevenueComponent;
  let fixture: ComponentFixture<FixedRevenueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedRevenueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedRevenueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
