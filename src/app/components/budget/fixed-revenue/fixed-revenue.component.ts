import { SelectEditor } from './../../../aggridfilters/selectbox';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import {
  loan_model,
  Loan_Crop_Practice,
  Loan_Budget,
  LoanStatus
} from '../../../models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';
import { LoancalculationWorker } from '../../../Workers/calculations/loancalculationworker';
import { LoggingService } from '../../../services/Logs/logging.service';
import { environment } from '../../../../environments/environment.prod';
import { InsuranceapiService } from '../../../services/insurance/insuranceapi.service';
import { AlertifyService } from '../../../alertify/alertify.service';
import { LoanApiService } from '../../../services/loan/loanapi.service';
import { JsonConvert } from 'json2typescript';
import { PublishService } from '../../../services/publish.service';
import { Page } from '@lenda/models/page.enum';
import { BudgetFormatter } from '../../../aggridformatters/valueformatters';
import {
  isgrideditable,
  cellclassmaker,
  CellType,
  headerclassmaker,
  removeHeaderMenu,
  calculatecolumnwidths
} from '../../../aggriddefinations/aggridoptions';
import { Chevron } from '../../../models/loan-response.model';
import { ToasterService } from '../../../services/toaster.service';
import { DeleteButtonRenderer } from '../../../aggridcolumns/deletebuttoncolumn';
import { BudgetHelperService } from '../budget-helper.service';
import * as _ from 'lodash';
import { NumericEditor } from '../../../aggridfilters/numericaggrid';
import { ISubscription } from 'rxjs/Subscription';
import { DataService } from '@lenda/services/data.service';
import { BudgetpublisherService } from '@lenda/services/budget/budgetpublisher.service';
import { getNumericCellEditor, numberValueSetter } from '@lenda/Workers/utility/aggrid/numericboxes';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';
import { RefDataModel, RefBudgetExpenseType } from '@lenda/models/ref-data-model';
import { Get_Distributor_Name } from '@lenda/services/common-utils';
import { BudgetAutoCompleteCellEditor } from '@lenda/aggridcolumns/budget-auto-complete-cell/budget-auto-complete-cell.component';
import { errormodel } from '@lenda/models/commonmodels';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { BudgetExpenseTypeNameValidation } from '@lenda/Workers/utility/validation-functions';
declare function setmodifiedall(array, keyword): any;
declare function setmodifiedsingle(obj, keyword): any;

@Component({
  selector: 'app-fixed-revenue',
  templateUrl: './fixed-revenue.component.html',
  styleUrls: ['./fixed-revenue.component.scss']
})
export class FixedRevenueComponent implements OnInit, OnDestroy {
  private subscription: ISubscription;
  public cropPractices: Array<Loan_Crop_Practice>;
  public Chevron: typeof Chevron = Chevron;
  // Following properties are added as produ build was failing
  // ----
  components;
  frameworkcomponents = {
    deletecolumn: DeleteButtonRenderer,
    numericCellEditor: NumericEditor,
    selectEditor: SelectEditor,
    autocompleteEditor: BudgetAutoCompleteCellEditor
  };
  context = { componentParent: this };
  cellvaluechanged;
  // ----
  posts: any[];
  columnDefs: Array<any>;
  rowData: Array<Loan_Budget>;
  localLoanObject: loan_model;
  gridApi;
  columnApi;
  pinnedBottomRowData;
  public getRowStyle;
  currentPageName: Page = Page.budget;
  expenseList = [];

  style = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };

  isFixedExpensesOpen = false;

  refdata: RefDataModel;
  Budget_Expense_Types: Array<RefBudgetExpenseType>;

  @Input() expanded: boolean = true;

  constructor(
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public insuranceservice: InsuranceapiService,
    public logging: LoggingService,
    public alertify: AlertifyService,
    public loanapi: LoanApiService,
    private budgetService: BudgetHelperService,
    private publishService: PublishService,
    private toasterService: ToasterService,
    private dataService: DataService,
    private budgetpublisher: BudgetpublisherService,
    private dtss: DatetTimeStampService,
    private validationService: ValidationService
  ) {
    this.components = {
      numericCellEditor: getNumericCellEditor()
    };

    this.localLoanObject = this.localstorageservice.retrieve(
      environment.loankey
    );

    this.refdata = this.localstorageservice.retrieve(
      environment.referencedatakey
    );

    this.Budget_Expense_Types = _.sortBy(this.refdata.BudgetExpenseType, e => e.Sort_order);

    let distName = Get_Distributor_Name(this.localLoanObject);
    let distNameTooltip = Get_Distributor_Name(this.localLoanObject, false);

    this.columnDefs = [
      {
        headerName: 'Expense',
        field: 'Expense_Type_Name',
        tooltip: params => params.data['Expense_Type_Name'],
        minWidth: 120,
        width: 120,
        maxWidth: 120,
        cellClass: params => cellclassmaker(CellType.Text, !!params.data['IsCustom']),
        editable: params => {
          return isgrideditable(!!params.data['IsCustom']);
        },
        cellEditor: 'autocompleteEditor',
        validations: {roq: BudgetExpenseTypeNameValidation }
      },
      // {
      //   headerName: '',
      //   minWidth: 480,
      //   width: 480,
      //   maxWidth: 480,
      //   field: 'Notes',
      //   editable: false,
      //   hide: false
      // },
      {
        headerName: 'Additional Credit Notes',
        minWidth: 480,
        width: 480,
        maxWidth: 480,
        cellClass: params => cellclassmaker(CellType.Text, !!params.data['IsCustom']),
        field: 'Notes',
        editable: params => !!params.data['IsCustom'],
        hide: false
      },
      {
        headerName: 'Total Fixed Expenses',
        headerClass: 'headerinsurance',
        children: [
          {
            headerName: 'ARM',
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            field: 'ARM_Budget_Crop',
            cellEditor: 'numericCellEditor',
            headerClass: `headerinsurance ${headerclassmaker(
              CellType.Integer
            )}`,
            valueSetter: numberValueSetter,
            cellClass: params =>
              cellclassmaker(CellType.Integer, !!params.data['IsCustom']),
            valueFormatter: params => BudgetFormatter(params.value),
            editable: params => {
              return isgrideditable(!!params.data['IsCustom']);
            }
          },
          {
            headerName: distName,
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            field: 'Distributor_Budget_Crop',
            headerTooltip: distNameTooltip,
            cellEditor: 'numericCellEditor',
            headerClass: `headerinsurance ${headerclassmaker(
              CellType.Integer
            )}`,
            valueSetter: numberValueSetter,
            cellClass: params =>
              cellclassmaker(CellType.Integer, !!params.data['IsCustom']),
            valueFormatter: params => BudgetFormatter(params.value),
            editable: params => {
              return isgrideditable(!!params.data['IsCustom']);
            }
          },
          {
            headerName: '3rd Party',
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            field: 'Third_Party_Budget_Crop',
            cellEditor: 'numericCellEditor',
            headerClass: `headerinsurance ${headerclassmaker(
              CellType.Integer
            )}`,
            valueSetter: numberValueSetter,
            cellClass: params =>
              cellclassmaker(CellType.Integer, !!params.data['IsCustom']),
            valueFormatter: params => BudgetFormatter(params.value),
            editable: params => {
              return isgrideditable(!!params.data['IsCustom']);
            }
          },
          {
            headerName: 'Total',
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            field: 'Total_Budget_Crop_ET',
            editable: false,
            headerClass: `headerinsurance ${headerclassmaker(
              CellType.Integer
            )}`,
            cellClass: cellclassmaker(CellType.Integer, false),
            valueFormatter: params => BudgetFormatter(params.value)
          }
        ]
      },
      {
        headerName: '',
        field: 'value',
        cellRenderer: 'deletecolumn',
        cellClass: 'fixed-expense',
        minWidth: 60,
        width: 60,
        suppressToolPanel: true,
        maxWidth: 60
      }
    ];

    this.getRowStyle = function(params) {
      if (params.node.rowPinned) {
        return {
          'font-weight': 'bold',
          'background-color': '#F5F7F7',
          'pointer-events': 'none'
        };
      }
    };

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localLoanObject = res;

      if(this.localLoanObject.srccomponentedit == 'FixedExpenseBudget') {
        let fixedExpense = _.sortBy(this.localLoanObject.LoanBudget.filter(
          p =>
            p.Crop_Practice_ID == 0 &&
            p.Budget_Type.trim() == 'F' &&
            p.ActionStatus != 3
        ), bgt => bgt.Sort_order)[res.lasteditrowindex];

        fixedExpense.Total_Budget_Crop_ET = _.sum([
          parseFloat(fixedExpense.ARM_Budget_Crop.toString()),
          parseFloat(fixedExpense.Distributor_Budget_Crop.toString()),
          parseFloat(fixedExpense.Third_Party_Budget_Crop.toString())
        ]);

        this.rowData[this.localLoanObject.lasteditrowindex] = fixedExpense;

        this.pinnedBottomRowData = this.budgetService.getTotals(this.rowData);
      } else {
        this.bindData(this.localLoanObject);
      }

      this.setValidationErrorForFixedExpenses();

      this.gridApi.refreshCells({
        columns: ['Total_Budget_Crop_ET']
      });
    });

    this.bindData(this.localLoanObject);
  }

  currenterrors: Array<errormodel> = [];
  getcurrenterrors() {
    this.currenterrors = (this.localstorageservice.retrieve(environment.errorbase) as Array<errormodel>).filter(
      p => p.chevron == 'Fixed_Expenses'
    );

    this.validationService.highlighErrorCells(this.currenterrors, 'Fixed_Expenses');
  }

  checkdistributorcolumnsvisiility() {
    if (this.rowData.find(p => p.Distributor_Budget_Acre != 0) == undefined) {
      //hide
      this.columnApi.setColumnVisible('Distributor_Budget_Acre', false);
    }
    if (this.rowData.find(p => p.Distributor_Budget_Crop != 0) == undefined) {
      //hide
      // this.columnApi.setColumnVisible("Distributor_Budget_Crop",false);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.getcurrenterrors();
      setmodifiedall(
        this.localstorageservice.retrieve(environment.modifiedbase),
        'Frv_'
      );
    }, 10);
    this.checkIfTotalBudgetIsZero();
  }

  checkIfTotalBudgetIsZero() {
    let total = 0;

    if(this.rowData) {
      this.rowData.forEach(el => {
        total += el.Total_Budget_Crop_ET;
      });
    }


    this.isFixedExpensesOpen = total != 0;
  }

  bindData(loanObject: loan_model) {
    if (loanObject.LoanCropPractices) {
      this.rowData = _.sortBy(this.localLoanObject.LoanBudget.filter(
          p =>
            p.Crop_Practice_ID == 0 &&
            p.Budget_Type.trim() == 'F' &&
            p.ActionStatus != 3
        ), bgt => bgt.Sort_order);

      this.rowData.forEach(element => {
        element.Total_Budget_Crop_ET = _.sum([
          parseFloat(element.ARM_Budget_Crop.toString()),
          parseFloat(element.Distributor_Budget_Crop.toString()),
          parseFloat(element.Third_Party_Budget_Crop.toString())
        ]);
      });
    }

    this.pinnedBottomRowData = this.budgetService.getTotals(this.rowData);

    setTimeout(() => {
      this.getcurrenterrors();
      this.adjustToFitAgGrid();
      setmodifiedall(
        this.localstorageservice.retrieve(environment.modifiedbase),
        'Frv_'
      );
    }, 10);
  }

  DeleteClicked(rowIndex: any, data) {
    this.alertify
      .confirm('Confirm', 'Do you Really Want to Delete this Record?')
      .subscribe(res => {
        if (res == true) {
          let obj = this.rowData[rowIndex];
          if (obj.ActionStatus == 1) {
            let localFarmRow = this.localLoanObject.LoanBudget.findIndex(
              f => f === data
            );
            this.localLoanObject.LoanBudget.splice(localFarmRow, 1);
          } else {
            obj.ActionStatus = 3;
          }

          this.localLoanObject.srccomponentedit = 'DELETE_FixedExpenseBudget';
          this.localLoanObject.lasteditrowindex = undefined;
          this.loanserviceworker.performcalculationonloanobject(
            this.localLoanObject
          );
          this.publishService.enableSync(Page.budget);

          this.setValidationErrorForFixedExpenses();
          this.getcurrenterrors();
        }
      });
  }

  syncToDb(localloanobject: loan_model) {
    localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);

    this.loanapi.syncloanobject(localloanobject).subscribe(res => {
      if (res.ResCode == 1) {
        this.localstorageservice.store(environment.modifiedbase, []);
        this.loanapi
          .getLoanById(localloanobject.Loan_Full_ID)
          .subscribe(res1 => {
            this.logging.checkandcreatelog(
              3,
              'Overview',
              'APi LOAN GET with Response ' + res1.ResCode
            );
            if (res1.ResCode == 1) {
              this.toasterService.success('Records Synced');
              let jsonConvert: JsonConvert = new JsonConvert();
              this.loanserviceworker.performcalculationonloanobject(
                jsonConvert.deserialize(res1.Data, loan_model)
              );
              this.loanserviceworker.saveValidationsToLocalStorage(res1.Data);
            } else {
              this.toasterService.error('Could not fetch Loan Object from API');
            }
          });
      } else {
        this.toasterService.error(res.Message || 'Error in Sync');
      }
    });
  }

  ngOnInit() {
    this.budgetpublisher.AllinClicked.subscribe(res => {
      //This Means that All in is Clicked;
      this.processAllIn();
    });
    this.budgetpublisher.AgProClicked.subscribe(res => {
      //This Means that All in is Clicked;
      this.progressAgPro();
    });
  }

  processAllIn() {
    //this.columnApi.setColumnVisible('Distributor_Budget_Crop', false);
    this.publishService.enableSync(Page.budget);
  }

  progressAgPro() {
    //this.columnApi.setColumnVisible('Distributor_Budget_Crop', false);
    this.publishService.enableSync(Page.budget);
  }

  addrow() {
    if(!this.isLoanEditable()) {
      return;
    }

    let isAnyRowWithoutExpenseName = this.rowData.some(
      a => !a.Expense_Type_Name
    );

    if(isAnyRowWithoutExpenseName) {
      this.alertify.alert('Alert', 'Warning: You already have added a Fixed Expense without name');
    } else {
      let newobj = new Loan_Budget();
      newobj.ActionStatus = 1;
      newobj.Budget_Type = 'F';
      newobj.Loan_Full_ID = this.localLoanObject.Loan_Full_ID;
      newobj.Z_Loan_ID = this.localLoanObject.LoanMaster.Loan_ID;
      newobj.Z_Loan_Seq_num = this.localLoanObject.LoanMaster.Loan_Seq_num;
      newobj.Crop_Practice_ID = 0;
      newobj.Crop_Input_Ind = '0';
      newobj.Loan_Budget_ID = getRandomInt(Math.pow(10, 4), Math.pow(10, 8));
      newobj.IsCustom = 1;

      this.gridApi.updateRowData({ add: [newobj] });

      this.gridApi.startEditingCell({
        rowIndex: this.rowData.length,
        colKey: 'Expense_Type_Name'
      });

      this.localLoanObject.LoanBudget.push(newobj);
      this.loanserviceworker.performcalculationonloanobject(this.localLoanObject, false);
    }
  }

  rowvaluechanged(params) {

    if(params && (params.oldValue == params.value || (!params.oldValue && !params.value))) return;

    if(!params && !params.column && !params.column.colId) return;

    //---------Modified yellow values-------------
    let nodeId = params.node.id;
    //MODIFIED YELLOW VALUES
    let modifiedvalues = this.localstorageservice.retrieve(
      environment.modifiedbase
    ) as Array<String>;

    if (!modifiedvalues.includes(nodeId + '_' + params.colDef.field)) {
      modifiedvalues.push(nodeId + '_' + params.colDef.field);
      this.localstorageservice.store(environment.modifiedbase, modifiedvalues);
      setmodifiedsingle(nodeId + '_' + params.colDef.field, 'Frv_');
    }
    //MODIFIED YELLOW VALUES
    //--------------------------------------------

    // if (!_.isNumber(params.value) && params.colDef.cellEditor != 'numericCellEditor') {
    //   this.localLoanObject.LoanBudget[index][params.column.colId] = params.value;
    // } else {
    //   this.localLoanObject.LoanBudget[index][params.column.colId] = parseFloat(params.value);
    // }

    let fixedExpense = this.rowData.find(a => a.Loan_Budget_ID == params.data.Loan_Budget_ID);

    if(fixedExpense) {
      fixedExpense.Total_Budget_Crop_ET = _.sum([
        parseFloat(fixedExpense.ARM_Budget_Crop.toString()),
        parseFloat(fixedExpense.Distributor_Budget_Crop.toString()),
        parseFloat(fixedExpense.Third_Party_Budget_Crop.toString())
      ]);
    }

    this.localLoanObject.srccomponentedit = 'FixedExpenseBudget';
    this.localLoanObject.lasteditrowindex = params.rowIndex;

    this.loanserviceworker.performcalculationonloanobject(this.localLoanObject);
    this.publishService.enableSync(Page.budget);

    this.setValidationErrorForFixedExpenses();
    this.getcurrenterrors();
  }

  public gridOptions = {
    getRowNodeId: function(data) {
      return 'Frv_' + data.Loan_Budget_ID;
    }
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    //these two methods remove side option toolbar , sets column , removes header menu
    removeHeaderMenu(this.gridApi);
    // params.api.sizeColumnsToFit();
    ///////////////////////////////////////////////////////////////////////////////////
    // this.getgridheight();
    this.checkdistributorcolumnsvisiility();
    this.adjustToFitAgGrid();
    this.setValidationErrorForFixedExpenses();
  }

  adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  displayedColumnsChanged(params) {
    this.adjustToFitAgGrid();
  }

  isLoanEditable() {
    return (
      !this.localLoanObject ||
      this.localLoanObject.LoanMaster.Loan_Status === LoanStatus.Working
    );
  }

  onBtExport() {
    const params = {
      skipHeader: false,
      columnGroups: true,
      skipFooters: false,
      skipGroups: false,
      skipPinnedTop: false,
      skipPinnedBottom: false,
      allColumns: true,
      fileName: `Fixed_Expenses_${
        this.localLoanObject.Loan_Full_ID
      }_${this.dtss.getTimeStamp()}.xlxs`,
      sheetName: 'Sheet 1'
    };
    // Export Data to Excel file
    this.gridApi.exportDataAsExcel(params);
  }

  // getgridheight() {
  //   this.style.height = (29 * (this.rowData.length + 3)).toString() + "px";
  // }

  /**
   * Sync to database - publish button event
   */
  synctoDb() {
    this.publishService.syncCompleted();
    this.syncToDb(this.localstorageservice.retrieve(environment.loankey));
  }

  setValidationErrorForFixedExpenses() {
    if(environment.isDebugModeActive)  console.time('Validation - Fixed Expenses');

    this.validationService.validateTableFields<Loan_Budget>(
      this.rowData,
      this.columnDefs,
      Page.budget,
      'Frv_',
      'Fixed_Expenses',
      'Loan_Budget_ID'
    );

    if(environment.isDebugModeActive) console.timeEnd('Validation - Fixed Expenses');
  }
}
