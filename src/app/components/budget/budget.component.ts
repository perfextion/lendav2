import { Component, OnInit, OnDestroy, AfterContentInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { JsonConvert } from 'json2typescript';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';
import { loan_model, Loan_Crop_Practice, Loan_Budget, LoanStatus } from '@lenda/models/loanmodel';
import { Page } from '@lenda/models/page.enum';
import { BudgetFormatter } from '@lenda/aggridformatters/valueformatters';
import { isgrideditable, cellclassmaker, CellType, headerclassmaker, calculatecolumnwidths } from '@lenda/aggriddefinations/aggridoptions';
import { Chevron } from '@lenda/models/loan-response.model';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { Get_Distributor_Name } from '@lenda/services/common-utils';

import { MasterService } from '@lenda/master/master.service';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { DataService } from '@lenda/services/data.service';
import { BudgetpublisherService } from '@lenda/services/budget/budgetpublisher.service';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { InsuranceapiService } from '@lenda/services/insurance/insuranceapi.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { BudgetHelperService } from './budget-helper.service';
import { PublishService } from '@lenda/services/publish.service';
import { ToasterService } from '@lenda/services/toaster.service';
import { GlobalService } from '@lenda/services/global.service';

/// <reference path="../../../Workers/utility/aggrid/numericboxes.ts" />
declare function setmodifiedall(array, keyword): any;
declare function setmodifiedsingle(obj, keyword): any;

@Component({
  selector: 'app-budget',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.scss'],
  providers: [BudgetHelperService]
})
export class BudgetComponent implements OnInit, AfterContentInit, OnDestroy {

  private subscription : ISubscription;
  // Following properties are added as produ build was failing
  // ----
  components;
  frameworkcomponents;
  context;
  private refData: RefDataModel;
  cellvaluechanged;
  // ----
  posts: any[];
  columnDefs: Array<any>;
  rowData: Array<Loan_Budget>;
  localLoanObject: loan_model;
  gridApi;
  columnApi;
  pinnedBottomRowData;
  public getRowStyle;
  currentPageName: Page = Page.budget;

  isTotalBudgetOpen: boolean = false;
  isDistributerOpen: boolean = false;
  isThirdPartyOpen: boolean = false;
  isHarvesterOpen: boolean = false;
  // constructor() {

  // this.posts =[];
  // this.posts.push({title:"DIS",postText:"Wow greate post"});
  // this.posts.push({title:"THR", postText:"WoW"});
  // this.posts.push({title:"HAR", postText:"WoW"});

  //  }

  style = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };

  public cropPractices: Array<Loan_Crop_Practice>;
  public Chevron: typeof Chevron = Chevron;

  constructor(
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    private budgetService: BudgetHelperService,
    public insuranceservice: InsuranceapiService,
    public logging: LoggingService,
    public alertify: AlertifyService,
    public loanapi: LoanApiService,
    private publishService: PublishService,
    private toasterService: ToasterService,
    private dataService: DataService,
    private budgetpublisher:BudgetpublisherService,
    private dtss: DatetTimeStampService,
    private validationService: ValidationService,
    public masterSvc: MasterService
  ) {

    this.refData=this.localstorageservice.retrieve(environment.referencedatakey);
    this.localLoanObject = this.localstorageservice.retrieve(environment.loankey);

    let distName = Get_Distributor_Name(this.localLoanObject);
    let distNameTooltip = Get_Distributor_Name(this.localLoanObject, false);

    this.columnDefs = [
      {
        headerName: 'Expense',
        field: 'Expense_Type_Name',
        tooltip: params => params.data['Expense_Type_Name'],
        editable: false,
        minWidth: 120,
        width: 120,
        maxWidth: 120
      },
      {
        headerName: 'Additional Credit Notes',
        field: 'Notes',
        minWidth: 480,
        width: 480,
        maxWidth: 480,
        cellClass: cellclassmaker(CellType.Text, true),
        editable: params => {
          return isgrideditable(params.data['Expense_Type_Name'] != 'Total');
        }
      },
      {
        headerName: 'Total Budget',
        headerClass: 'headerinsurance',
        children: [
          {
            headerName: 'ARM',
            field: 'ARM_Budget_Crop',
            editable: false,
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            headerClass: `headerinsurance ${headerclassmaker(CellType.Integer)}`,
            cellClass: cellclassmaker(CellType.Integer, false),
            valueFormatter: params => BudgetFormatter(params.value)
          },
          {
            headerName: distName,
            headerTooltip: distNameTooltip,
            field: 'Distributor_Budget_Crop',
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            headerClass: `headerinsurance ${headerclassmaker(CellType.Integer)}`,
            editable: false,
            cellClass: cellclassmaker(CellType.Integer, false),
            valueFormatter: params => BudgetFormatter(params.value)
          },
          {
            headerName: '3rd Party',
            field: 'Third_Party_Budget_Crop',
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            editable: false,
            headerClass: `headerinsurance ${headerclassmaker(CellType.Integer)}`,
            cellClass: cellclassmaker(CellType.Integer, false),
            valueFormatter: params => BudgetFormatter(params.value)
          },
          {
            headerName: 'Total',
            field: 'Total_Budget_Crop_ET',
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            editable: false,
            headerClass: `headerinsurance ${headerclassmaker(CellType.Integer)}`,
            cellClass: cellclassmaker(CellType.Integer, false),
            valueFormatter: params => BudgetFormatter(params.value)
          }
        ]
      }
    ];

    this.getRowStyle = function (params) {
      if (params.node.rowPinned) {
        return {
          "font-weight": "bold",
          "background-color": "#F5F7F7",
          "pointer-events": "none"
        };
      }
    };

    this.subscription = this.dataService.getLoanObject().subscribe(res=>{
      if(environment.isDebugModeActive) console.time('budget observer');

      this.localLoanObject = res;
      this.bindData(this.localLoanObject);
      if(environment.isDebugModeActive) console.timeEnd('budget observer');

      this.checkIfTotalBudgetIsZero();
      this.checkIfAssocChevronsIsOpen();


      setTimeout(() => {
        this.adjustToFitAgGrid();
      }, 300);
    });

    //TODO-SANKET remove below line, once the api data is up to date
    //this.localLoanObject.LoanBudget.map(budget=> this.budgetService.muplitypePropsWithAcres(budget,this.cropPractice.LCP_Acres))

    try {
      this.localLoanObject.LoanBudget.map(budget => {

      budget.Total_Budget_Acre = parseFloat(budget.ARM_Budget_Acre.toString()) + parseFloat(budget.Distributor_Budget_Acre.toString()) + parseFloat(budget.Third_Party_Budget_Acre.toString());
      return budget;
    });
  } catch {}
    //REMOVE END
    this.bindData(this.localLoanObject);
  }

  isValidCropPractice(cp: Loan_Crop_Practice) {
    return this.refData.CropList.some(c => c.Crop_And_Practice_ID == cp.Crop_Practice_ID);
  }


  checkdistributorcolumnsvisiility(){
    if(this.rowData.find(p=>p.Distributor_Budget_Acre!=0)==undefined)
    {
      //hide
      this.columnApi.setColumnVisible("Distributor_Budget_Acre",false);
      this.columnApi.setColumnWidth("Notes", 360, true)
    }
    if(this.rowData.find(p=>p.Distributor_Budget_Crop!=0)==undefined)
    {
      //hide
      // this.columnApi.setColumnVisible("Distributor_Budget_Crop",false);
    }
   }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  bindData(loanObject: loan_model) {
    if (loanObject.LoanCropPractices) {
      this.cropPractices = this.budgetService.prepareCropPractice(loanObject.LoanCropPractices.filter(a => a.ActionStatus != 3));
      this.cropPractices = _.sortBy(this.cropPractices, ['FC_CropName', 'FC_Crop_Type', 'FC_PracticeType', 'FC_Crop_Practice_Type']);

      let data = this.localLoanObject.LoanBudget.filter(p=>p.Crop_Practice_ID==0 && p.Budget_Type.trim()=='V' && p.ActionStatus !=3);
      data = data.filter(a => a.Total_Budget_Crop_ET > 0);

      this.rowData = _.sortBy(data, b => b.Sort_order || b.Loan_Budget_ID);

      this.pinnedBottomRowData = this.budgetService.getTotals(this.rowData);
    }

    setTimeout(() => {
      setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), "Bgt_");
    }, 10);
  }

  syncToDb(localloanobject: loan_model) {
    // Loan Condition Calculation
    localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);

    this.loanapi.syncloanobject(localloanobject).subscribe(res => {
      if (res.ResCode == 1) {
        this.localstorageservice.store(environment.modifiedbase, []);
        this.loanapi.getLoanById(localloanobject.Loan_Full_ID).subscribe(res1 => {
          this.logging.checkandcreatelog(3, 'Overview', "APi LOAN GET with Response " + res1.ResCode);
          if (res1.ResCode == 1) {
            this.toasterService.success("Records Synced");
            let jsonConvert: JsonConvert = new JsonConvert();
            this.loanserviceworker.performcalculationonloanobject(jsonConvert.deserialize(res1.Data, loan_model));
            this.loanserviceworker.saveValidationsToLocalStorage(res1.Data);
            new GlobalService(this.localstorageservice).updateLocalStorage('Bgt_');
            new GlobalService(this.localstorageservice).updateLocalStorage('Lbg_');
            new GlobalService(this.localstorageservice).updateLocalStorage('Frv_');
          }
          else {
            this.toasterService.error("Could not fetch Loan Object from API")
          }
        });
      }
      else {
        this.toasterService.error(res.Message || "Error in Sync");
      }
    });

  }

  ngOnInit() {
  }

  ngAfterContentInit() {
    this.checkIfTotalBudgetIsZero();
    this.checkIfAssocChevronsIsOpen();
  }

  checkIfTotalBudgetIsZero() {
    let total = 0;
    this.rowData.forEach(el => {
      total += el.Total_Budget_Crop_ET;
    });

    this.isTotalBudgetOpen = total != 0;
  }

  checkIfAssocChevronsIsOpen() {

    let openDistributor = _.find(this.rowData, (el) => {
      return el.Distributor_Budget_Crop > 0;
    });

    if (openDistributor) {
      this.isDistributerOpen = true;
    } else {
      this.isDistributerOpen = false;
    }

    this.isFixedExpenseOpen();

    let openHarvester = _.find(this.rowData, (el) => {
        return el.Expense_Type_Name == 'Harvesting' && el.Third_Party_Budget_Crop > 0;
    });
    this.isHarvesterOpen = openHarvester ? true : false;

    this.localLoanObject.Association.forEach(el => {
      if (el.Assoc_Type_Code == 'DIS') {
        this.isDistributerOpen = true;
      }

      if (el.Assoc_Type_Code == 'THR') {
        this.isThirdPartyOpen = true;
      }

      if (el.Assoc_Type_Code == 'HAR') {
        this.isHarvesterOpen = true;
      }
    });
  }

  isFixedExpenseOpen() {
    let fixedExp3rdPartyTotal = this.localLoanObject.LoanBudget.filter(p => p.Crop_Practice_ID == 0 && p.Budget_Type.trim() == 'F' && p.ActionStatus != 3);
    let openThirdParty = _.find(fixedExp3rdPartyTotal, (el) => {
      return el.Third_Party_Budget_Crop > 0;
    });

    if (openThirdParty) {
      this.isThirdPartyOpen = true;
    } else {
      this.isThirdPartyOpen = false;
    }
  }

  ngAfterViewInit(){

    setTimeout(() => {
      setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), "Bgt_");
    }, 10);
  }


  rowvaluechanged(params){

    //If no value change dont record it.
    if(params && params.oldValue == params.value) return;

    //---------Modified yellow values-------------
    let modifiedvalues = this.localstorageservice.retrieve(environment.modifiedbase) as Array<String>;

    if (!modifiedvalues.includes("Bgt_" + params.data.Loan_Budget_ID + "_" + params.colDef.field)) {
      modifiedvalues.push("Bgt_" + params.data.Loan_Budget_ID + "_" + params.colDef.field);
      this.localstorageservice.store(environment.modifiedbase, modifiedvalues); +
        setmodifiedsingle("Bgt_" + params.data.Loan_Budget_ID + "_" + params.colDef.field, "Bgt_");

    }
    //MODIFIED YELLOW VALUES
    //----------------------------------------
    let bgt = this.localLoanObject.LoanBudget.find(p=>p.Loan_Budget_ID==params.data.Loan_Budget_ID);
    if(bgt) {
      bgt.Notes = params.value;
      this.dataService.setLoanObjectwithoutsubscription(this.localLoanObject);
    }

    this.publishService.enableSync(Page.budget);
  }
  onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    //these two methods remove side option toolbar , sets column , removes header menu
    // setgriddefaults(this.gridApi, this.columnApi);
    params.api.sizeColumnsToFit();
    ///////////////////////////////////////////////////////////////////////////////////
   // this.getgridheight();
   this.checkdistributorcolumnsvisiility();
   this.adjustToFitAgGrid();
  }

  isLoanEditable(){
    return !this.localLoanObject || this.localLoanObject.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  onBtExport() {
    const params = {
      skipHeader: false,
      columnGroups: true,
      skipFooters: false,
      skipGroups: false,
      skipPinnedTop: false,
      skipPinnedBottom: false,
      allColumns: true,
      fileName: `Total_Budget_${this.localLoanObject.Loan_Full_ID}_${this.dtss.getTimeStamp()}.xlxs`,
      sheetName: 'Sheet 1',
    };
    // Export Data to Excel file
    this.gridApi.exportDataAsExcel(params);
  }

  adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  displayedColumnsChanged(params){
    this.adjustToFitAgGrid();
  }

  public gridOptions = {
    getRowNodeId: function (data) {

      return "Bgt_" + data.Loan_Budget_ID;
    }
  }
  // getgridheight() {
  //   this.style.height = (29 * (this.rowData.length + 3)).toString() + "px";
  // }

  /**
   * Sync to database - publish button event
   */
  synctoDb() {
    this.publishService.syncCompleted();
    let latestLoanObj : loan_model= this.localstorageservice.retrieve(environment.loankey);
    latestLoanObj.LoanBudget.forEach(budget =>{
      if(budget.ActionStatus ==1){
        budget.Loan_Budget_ID = 0;
      }
    })
    this.syncToDb(latestLoanObj);
  }

  //ALLINTRIGGER FROM TOTAL BUDGET

  AllInClicked(){
    this.budgetpublisher.publishAllInChanged(true);
    this.processAllIn();
  }

  AgproClicked(){
    this.budgetpublisher.publishAllInChanged(true);
    this.processAgPro();
  }

  processAllIn() {
    //let expensesAllowed=this.refData.BudgetExpenseType.filter(p=>p.Crop_Input_Ind=="1").map(p=>p.Budget_Expense_Type_ID);
    let budgets=this.localLoanObject.LoanBudget.filter(a => a.ActionStatus != 3);
    for(let i =0;i<budgets.length;i++){
      let bgt = budgets[i];

      bgt.ARM_Budget_Acre=bgt.ARM_Budget_Acre+bgt.Distributor_Budget_Acre;
      bgt.Distributor_Budget_Acre=0;

      bgt.ARM_Budget_Crop=bgt.ARM_Budget_Crop+bgt.Distributor_Budget_Crop;
      bgt.Distributor_Budget_Crop=0;

      if(bgt.ActionStatus != 1 ) {
        bgt.ActionStatus=2;
      }
    }

    this.localLoanObject.srccomponentedit = 'PROCESS_ALL_IN';
    this.loanserviceworker.performcalculationonloanobject(this.localLoanObject,true);
  }

  processAgPro() {
    let expensesAllowed=this.refData.BudgetExpenseType.filter(p=>p.Crop_Input_Ind=="1").map(p=>p.Budget_Expense_Type_ID);
    let budgets=this.localLoanObject.LoanBudget.filter(p=> p.ActionStatus != 3 && expensesAllowed.includes(p.Expense_Type_ID));
    for(let i =0;i<budgets.length;i++){
      budgets[i].Distributor_Budget_Acre=budgets[i].ARM_Budget_Acre+budgets[i].Distributor_Budget_Acre;
      budgets[i].ARM_Budget_Acre=0;

      budgets[i].Distributor_Budget_Crop=budgets[i].ARM_Budget_Crop+budgets[i].Distributor_Budget_Crop;
      budgets[i].ARM_Budget_Crop=0;

      if(budgets[i].ActionStatus!=3 ||budgets[i].ActionStatus!=1 ) {
        budgets[i].ActionStatus=2;
      }
    }

    this.localLoanObject.srccomponentedit = 'PROCESS_AG_PRO';
    this.loanserviceworker.performcalculationonloanobject(this.localLoanObject,true);
  }
}
