import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';

import { LocalStorageService } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';
import { ISubscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { Page } from '@lenda/models/page.enum';
import { loan_model, LoanStatus } from '@lenda/models/loanmodel';
import {
  Loan_Document,
  Loan_Document_Action
} from '@lenda/models/loan/loan_document';
import { Ref_Document_Type_Level } from '@lenda/models/ref-data-model';
import { errormodel } from '@lenda/models/commonmodels';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';
import { ChangeStatusModel, LoanStatusChange } from '@lenda/models/copy-loan/copyLoanModel';

import { ValidationErrorsCountService } from '@lenda/Workers/calculations/validationerrorscount.service';
import { MasterService } from '@lenda/master/master.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { DataService } from '@lenda/services/data.service';
import { PublishService } from '@lenda/services/publish.service';
import { CheckoutService } from './checkout.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';

import { AddLoanDocumentComponent } from './loan-document/add-loan-document/add-loan-document.component';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit, OnDestroy {
  public currentPageName: string = Page.checkout;
  public localLoanObj: loan_model;

  public underwritingDocuments: Array<Loan_Document>;
  public prerequisiteDocuments: Array<Loan_Document>;
  public closingDocuments: Array<Loan_Document>;
  public cropMonitoringDocuments: Array<Loan_Document>;

  private subscription: ISubscription;

  constructor(
    private localstorageservice: LocalStorageService,
    private checkoutService: CheckoutService,
    private toastr: ToastrService,
    private publishService: PublishService,
    private dataservice: DataService,
    private dialog: MatDialog,
    private alertify: AlertifyService,
    private validationCount: ValidationErrorsCountService,
    public masterSvc: MasterService,
    private loanService: LoanApiService,
    private toaster: ToastrService,
    private validationService: ValidationService
  ) {}

  ngOnInit() {
    this.localLoanObj = this.localstorageservice.retrieve(environment.loankey);
    this.setData(this.localLoanObj);
    this.checkErrors();

    //this.enableSync();

    this.subscription = this.dataservice.getLoanObject().subscribe(data => {
      this.localLoanObj = data;
      this.setData(this.localLoanObj);
      this.checkErrors();
      // this.enableSync();
    });
  }

  enableSync() {
    let isAnyDocAdded = this.localLoanObj.Loan_Documents.some(
      a => a.ActionStatus == 1
    );

    if (isAnyDocAdded) {
      this.publishService.enableSync(Page.checkout);
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private setData(localLoanObj: loan_model) {
    if (localLoanObj) {
      let docs = _.sortBy(
        localLoanObj.Loan_Documents.filter(a => a.ActionStatus != 3),
        ['Document_Type_Level', 'Sort_Order']
      );

      this.underwritingDocuments = docs.filter(
        a => a.Document_Type_Level == Ref_Document_Type_Level.Underwriting
      );

      this.prerequisiteDocuments = docs.filter(
        a => a.Document_Type_Level == Ref_Document_Type_Level.Prerequisite
      );

      this.closingDocuments = docs.filter(
        a => a.Document_Type_Level == Ref_Document_Type_Level.Closing
      );

      this.cropMonitoringDocuments = docs.filter(
        a => a.Document_Type_Level == Ref_Document_Type_Level.Crop_Monitoring
      );
    } else {
      this.underwritingDocuments = [];
      this.prerequisiteDocuments = [];
      this.closingDocuments = [];
      this.cropMonitoringDocuments = [];
    }
  }

  public addDocument($event: { title: string; Level: number }) {
    this.dialog
      .open(AddLoanDocumentComponent, {
        data: {
          title: $event.title
        }
      })
      .afterClosed()
      .subscribe(res => {
        if (res) {
          let doc = new Loan_Document();
          doc.Loan_Full_ID = this.localLoanObj.Loan_Full_ID;
          doc.Loan_ID = this.localLoanObj.LoanMaster.Loan_ID;
          doc.Loan_Document_ID = getRandomInt(10000, 100000);
          doc.Document_Type_Level = $event.Level;
          doc.Document_Name = res;
          doc.ActionStatus = 1;
          doc.Action_Ind = Loan_Document_Action.RequireUpload;
          doc.Doc_Needed_Ind = 1;
          doc.Is_Custom = 1;
          doc.Sort_Order = doc.Loan_Document_ID * 100 + 1;

          this.localLoanObj.Loan_Documents.push(doc);
          this.dataservice.setLoanObject(this.localLoanObj);
          this.publishService.enableSync(Page.checkout);
        }
      });
  }

  public deleteDocument($event: { Loan_Document: Loan_Document; Level: number }) {
    this.alertify.deleteRecord().subscribe(res => {
      if (res) {
        let index = this.localLoanObj.Loan_Documents.findIndex(
          a =>
            a.Loan_Document_ID == $event.Loan_Document.Loan_Document_ID &&
            a.Document_Type_Level == $event.Level
        );

        if (index > -1) {
          if ($event.Loan_Document.ActionStatus == 1) {
            this.localLoanObj.Loan_Documents.splice(index, 1);
          } else {
            let doc = this.localLoanObj.Loan_Documents[index];
            doc.ActionStatus = 3;
            doc.Doc_Needed_Ind = 0;
            doc.Action_Ind = Loan_Document_Action.Archived;
          }

          this.dataservice.setLoanObject(this.localLoanObj);
          this.publishService.enableSync(Page.checkout);
        }
      }
    });
  }

  public transmitToKL() {
    this.checkoutService.transmitToKL().subscribe(res => {
      if (res.ResCode == 1) {
        this.toastr.success('KL Transmit Successfully.');
      } else {
        this.toastr.error('KL Transmit Failed');
      }
    });
  }

  public checkout() {
    if (this.isCheckoutButtonEnabled) {
      this.alertify.confirm('Confirm', 'Are you sure you want to upload the Loan to Notridge?').subscribe(res => {
        if(res) {
          const body = <ChangeStatusModel>{
            FromLoanFullID: this.localLoanObj.Loan_Full_ID,
            ToStatus: LoanStatusChange.Uploaded
          };

          this.loanService.changeStatus(body).subscribe(
            res => {
              if(res.ResCode == 1) {
                this.toaster.success('Loan Uploaded to Notridge Successfully.');
                this.localLoanObj.LoanMaster.Loan_Status = LoanStatus.Uploaded;
                this.dataservice.setLoanObject(this.localLoanObj);
              } else {
                this.toaster.error('Loan Uploaded to Notridge Failed.');
              }
            },
            error => {
              this.toaster.error('Loan Uploaded to Notridge Failed.');
            }
          );
        }
      });
    }
  }

  public synctoDb() {
    this.checkoutService.syncToDb(
      this.localstorageservice.retrieve(environment.loankey)
    );
    this.publishService.syncCompleted();
  }

  private checkErrors() {
    this.validationService.Validate_Loan_Documents(this.localLoanObj);
  }

  public onDownload($event: { Loan_Document: Loan_Document }) {
    let url =
      $event.Loan_Document.Document_URL ||
      $event.Loan_Document.Document_URL_Executed ||
      $event.Loan_Document.Document_URL_Unexecuted;

    const width = window.innerWidth - 400;
    const height = window.innerHeight - 200;

    window.open(url,  '_blank' , `toolbar=no,scrollbars=yes,resizable= yes,top=100,left=100,width=${width},height=${height}`);
  }

  /**
   * Check if no Validation Errors && No changes
   */
  public get isCheckoutButtonEnabled() {
    let validationCount = 0;

    let valiationerrors = this.validationCount.getValidationErrors();

    Object.keys(valiationerrors).forEach(tab => {
      validationCount += valiationerrors[tab].validations.level2;
    });

    let modifiedValues = this.localstorageservice.retrieve(
      environment.modifiedbase
    ) as Array<string>;

    return (
      validationCount == 0 &&
      (modifiedValues && modifiedValues.length == 0) &&
      (this.localLoanObj && (this.localLoanObj.LoanMaster.Loan_Status == LoanStatus.Recommended || this.localLoanObj.LoanMaster.Loan_Status == LoanStatus.Approved))
    );
  }
}
