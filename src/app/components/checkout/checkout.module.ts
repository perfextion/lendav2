import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';

// Components
import { CheckoutComponent } from './checkout.component';
import { LoanDocumentComponent } from './loan-document/loan-document.component';
import { AddLoanDocumentComponent } from './loan-document/add-loan-document/add-loan-document.component';

// Services
import { CheckoutService } from './checkout.service';

@NgModule({
  imports: [CommonModule, SharedModule, MaterialModule, UiComponentsModule],
  declarations: [
    CheckoutComponent,
    LoanDocumentComponent,
    AddLoanDocumentComponent
  ],
  providers: [CheckoutService],
  entryComponents: [AddLoanDocumentComponent]
})
export class CheckoutModule {}
