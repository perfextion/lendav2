import { Injectable } from '@angular/core';
import { ApiService, GlobalService } from '@lenda/services';
import { Observable } from 'rxjs';
import { ResponseModel } from '@lenda/models/resmodels/reponse.model';
import { Loan_Condition, loan_model } from '@lenda/models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { ToasterService } from '@lenda/services/toaster.service';
import { JsonConvert } from 'json2typescript';
import { environment } from '@env/environment.prod';

@Injectable()
export class CheckoutService {
  constructor(
    private api: ApiService,
    public localstorageservice: LocalStorageService,
    public logging: LoggingService,
    public loanserviceworker: LoancalculationWorker,
    public loanapi: LoanApiService,
    public toasterService: ToasterService
  ) {}

  // TODO: Complete the logic
  transmitToKL(): Observable<ResponseModel> {
    return this.api.post('', {});
  }

  // TODO: Complete the logic
  checkout(): Observable<ResponseModel> {
    return this.api.post('', {});
  }

  syncToDb(localloanobject: loan_model) {
    // Loan Condition Calculation
    localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);

    this.loanapi.syncloanobject(localloanobject).subscribe(synsResponse => {
      if (synsResponse.ResCode == 1) {
        this.localstorageservice.store(environment.modifiedbase, []);
        this.loanapi
          .getLoanById(localloanobject.Loan_Full_ID)
          .subscribe(res => {
            this.logging.checkandcreatelog(
              3,
              'Overview',
              'APi LOAN GET with Response ' + res.ResCode
            );
            if (res.ResCode == 1) {
              this.toasterService.success('Records Synced');

              let jsonConvert: JsonConvert = new JsonConvert();
              this.loanserviceworker.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model));
              this.loanserviceworker.saveValidationsToLocalStorage(res.Data);
            } else {
              this.toasterService.error('Could not fetch Loan Object from API');
            }
          });
      } else {
        this.toasterService.error(synsResponse.Message || 'Error in Sync');
      }
    });
  }
}
