import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  Output,
  EventEmitter
} from '@angular/core';

import { Loan_Document, Loan_Document_Downloan_Type } from '@lenda/models/loan/loan_document';

@Component({
  selector: 'app-loan-document',
  templateUrl: './loan-document.component.html',
  styleUrls: ['./loan-document.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoanDocumentComponent implements OnInit {
  /**
   * Document Type Name
   */
  @Input() name: string;

  /**
   * Documents List
   */
  @Input() documents: Array<Loan_Document> = [];

  /**
   * Level
   */
  @Input()
  Level: number;

  @Input() expanded = true;

  @Output() addDocument = new EventEmitter<any>();
  @Output() deleteDocument = new EventEmitter<any>();
  @Output() download = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  onAddDocument() {
    this.addDocument.emit({
      title: this.name,
      Level: this.Level
    });
  }

  onDeleteDocument(doc: Loan_Document) {
    this.deleteDocument.emit({
      Loan_Document: doc,
      Level: this.Level
    });
  }

  onDownloand(type: Loan_Document_Downloan_Type, doc: Loan_Document): void {
    if(doc.Upload_Date_Time) {
      if(type == Loan_Document_Downloan_Type.Unexecuted) {
        if(doc.Document_URL_Unexecuted) {
          this.download.emit({
            Type: Loan_Document_Downloan_Type.Unexecuted,
            Loan_Document: doc
          });
        }
      } else if(type == Loan_Document_Downloan_Type.Executed) {
        if(doc.Document_URL_Executed) {
          this.download.emit({
            Type: Loan_Document_Downloan_Type.Executed,
            Loan_Document: doc
          });
        }
      } else if(doc.Document_URL) {
        this.download.emit({
          Type: Loan_Document_Downloan_Type.Executed,
          Loan_Document: doc
        });
      } else {
        this.download.emit({
          Type: null,
          Loan_Document: doc
        });
      }
    }
  }

  getDocIcon(doc: Loan_Document) {
    if(!doc.Request_User_ID && !doc.Upload_Date_Time) {
      return 'green_doc';
    }

    if(doc.Request_User_ID && !doc.Upload_Date_Time) {
      return 'yellow_doc';
    }

    if(doc.Upload_Date_Time) {
      if(!doc.Document_URL && !doc.Document_URL_Executed) {
        return 'red_doc';
      }

      return 'blue_doc';
    }

    return 'gray_doc';
  }

  getDocStatus(doc: Loan_Document) {
    if(!doc.Request_User_ID && !doc.Upload_Date_Time) {
      return 'Document requirement not yet transmitted';
    }

    if(!doc.Upload_Date_Time) {
      return 'Document not yet generated';
    }

    if(doc.Upload_Date_Time) {
      if(!doc.Document_URL && !doc.Document_URL_Executed) {
        return 'Document generated but not yet executed';
      }

      return 'Document added by ' + (doc.Upload_User_Name || doc.Upload_User_Email);
    }

    return 'Document Inactive';
  }
}
