import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-loan-document',
  templateUrl: './add-loan-document.component.html',
  styleUrls: ['./add-loan-document.component.scss']
})
export class AddLoanDocumentComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AddLoanDocumentComponent>
  ) {}

  ngOnInit() {}

  onCancel(): void {
    this.dialogRef.close(false);
  }
}
