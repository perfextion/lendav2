export class YieldModel {
  Crop_ID: number;
  Crop_Code: string;
  Crop_Name: string;
  cropType: string;
  Practice: string;
  IrNI: string;
  Crop_Practice: string;
  CropYield: number;
  Rate_Yield: number;
  APH: number;
  Bu: string;
  ActionStatus: number;
  Loan_Full_ID: string;
}
