import {
  Component,
  OnInit,
  Inject,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  OnDestroy,
  Input
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

import { ISubscription } from 'rxjs/Subscription';

import { LocalStorageService } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';
import { GridOptions } from 'ag-grid';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { LoanStatus } from '@lenda/models/loanmodel';
import { Loan_Key_Visibilty, Loansettings } from '@lenda/models/loansettings';
import { loan_model } from '@lenda/models/loanmodel';
import {
  yieldValueSetter,
  getIntegerCellEditor
} from '@lenda/Workers/utility/aggrid/numericboxes';
import {
  APHRoundValueSetter,
  lookupUOM
} from '@lenda/Workers/utility/aggrid/cropboxes';
import { DeleteButtonRenderer } from '@lenda/aggridcolumns/deletebuttoncolumn';
import { SelectEditor } from '@lenda/aggridfilters/selectbox';
import { Sync_Status } from '@lenda/models/syncstatusmodel';
import { Page } from '@lenda/models/page.enum';
import {
  isgrideditable,
  cellclassmaker,
  CellType,
  headerclassmaker,
  calculatecolumnwidths
} from '@lenda/aggriddefinations/aggridoptions';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';
import { numberFormatter, yieldFormatter } from '@lenda/aggridformatters/valueformatters';
import { CropService } from '../crop.service';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { CropAutoCompleteEditor } from '@lenda/aggridcolumns/crop-auto-complete-editor/crop-auto-complete-editor.component';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';

import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { CropapiService } from '@lenda/services/crop/cropapi.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { PublishService } from '@lenda/services/publish.service';
import { MasterService } from '@lenda/master/master.service';
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { ExportExcelService } from '@lenda/services/export-excel.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { DataService } from '@lenda/services/data.service';

declare function setmodifiedall(array, keyword): any;
declare function setmodifiedsingle(obj, keyword): any;

@Component({
  selector: 'app-yield',
  templateUrl: './yield.component.html',
  styleUrls: ['./yield.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class YieldComponent implements OnInit, OnDestroy {
  private _columnsDisplaySettings: ColumnsDisplaySettings = new ColumnsDisplaySettings();

  @Input() set columnsDisplaySettings(value: ColumnsDisplaySettings) {
    this._columnsDisplaySettings = value;
  }

  get columnsDisplaySettings(): ColumnsDisplaySettings {
    return this._columnsDisplaySettings;
  }

  public years = [];
  public localloanobject: loan_model = new loan_model();
  public cropYear: number;

  public rowData = [];
  public columnDefs = [];
  public components: any;
  public gridApi: any;
  public columnApi: any;
  public context: any;
  public frameworkcomponents: any;
  public style: any = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };
  public defaultColDef = {
    enableValue: true,
    enableRowGroup: true,
    enablePivot: true
  };
  public gridOptions: GridOptions;

  private deleteAction = false;
  private refdata: RefDataModel = <RefDataModel>{};
  private subscription: ISubscription;
  private preferencesChangeSub: ISubscription;

  constructor(
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public cropserviceapi: CropapiService,
    public logging: LoggingService,
    public loanapi: LoanApiService,
    public alertify: AlertifyService,
    public dialog: MatDialog,
    private publishService: PublishService,
    private dataService: DataService,
    private settingsService: SettingsService,
    private dtss: DatetTimeStampService,
    public excel: ExportExcelService,
    private yieldValidation: CropService,
    private toastar: ToastrService,
    public masterSvc: MasterService
  ) {

    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);
    this.setCropYear();

    this.components = { intCellEditor: getIntegerCellEditor() };
    this.frameworkcomponents = {
      selectEditor: SelectEditor,
      deletecolumn: DeleteButtonRenderer,
      autocompleteEditor: CropAutoCompleteEditor
    };

    this.gridOptions = <GridOptions>{
      getRowNodeId: function (data) {
        return 'Yld_' + data.crop_yield_id;
      },
      onGridColumnsChanged: (params) => params.api.doLayout()
    };

    this.context = { componentParent: this };
  }

  private setCropYear() {
    try {
      this.cropYear = this.localloanobject.LoanMaster.Crop_Year || 0;

      this.years = [];

      for (let i = 1; i < 8; i++) {
        this.years.push(this.cropYear - i);
      }
    } catch {

    }
  }

  ngOnInit() {
    this.getUserPreferences();
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localloanobject = res;
      this.assignTempID();
      if (res.srccomponentedit == 'YieldComponent') {
        //if the same table invoked the change .. change only the edited row
        this.rowData[res.lasteditrowindex] = this.localloanobject.CropYield.filter(p => p.ActionStatus != 3)[res.lasteditrowindex];
        this.gridApi.refreshCells();
      } else if(res.srccomponentedit == 'UpdateColumnSettings_Price') {
        this.rowData = res.CropYield.filter(cy => cy.ActionStatus != 3);
        this.hideUnhideLoanKeys();
        this.adjustToFitAgGrid();
        this.gridApi.refreshCells();
      } else if (res.srccomponentedit == 'EditLoanParameters') {
        this.setCropYear();
        this.declareColumns();
        this.rowData = res.CropYield.filter(cy => cy.ActionStatus != 3);
        this.gridApi.setColumnDefs(this.columnDefs)
      } else {
        this.rowData = res.CropYield.filter(cy => cy.ActionStatus != 3);
        this.rowData = _.orderBy(this.rowData, ['Crop_Code', 'cropType', 'Practice']);
        this.gridApi.refreshCells();
      }

      this.gridApi.refreshCells();
      this.setModifiedCells();
    });

    this.setModifiedCells();
  }

  private setModifiedCells() {
    setTimeout(() => {
      setmodifiedall(
        this.localstorageservice.retrieve(environment.modifiedbase),
        'Yld_'
      );
    }, 1000);
  }

  private getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;

    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe(preferences => {
      this.columnsDisplaySettings =  preferences.loanSettings.columnsDisplaySettings;
      this.hideUnhideLoanKeys();
    });
  }

  //Hide Columns Based ON Loan Settings
  private LoankeySettings: Loan_Key_Visibilty;

  hideUnhideLoanKeys() {

    let LoanSettings: Loansettings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);

    if (LoanSettings != undefined || LoanSettings != null) {
      if (
        LoanSettings.Loan_key_Settings != undefined ||
        LoanSettings.Loan_key_Settings != null
      ) {
        this.LoankeySettings = LoanSettings.Loan_key_Settings;
      } else {
        this.LoankeySettings = new Loan_Key_Visibilty();
      }
    } else {
      this.LoankeySettings = new Loan_Key_Visibilty();
    }

    this.columnsDisplaySettings.cropPrac = this.LoankeySettings.Crop_Practice == null ? this.columnsDisplaySettings.cropPrac : this.LoankeySettings.Crop_Practice;
    this.columnsDisplaySettings.cropType = this.LoankeySettings.Crop_Type == null ? this.columnsDisplaySettings.cropType : this.LoankeySettings.Crop_Type;
    this.columnsDisplaySettings.irrPrac = this.LoankeySettings.Irr_Practice == null ? this.columnsDisplaySettings.irrPrac : this.LoankeySettings.Irr_Practice;
    this.columnsDisplaySettings.rateYield = this.LoankeySettings.RateYield == null ? this.columnsDisplaySettings.rateYield : this.LoankeySettings.RateYield;
    this.columnsDisplaySettings.aph = this.LoankeySettings.APH == null ? this.columnsDisplaySettings.aph : this.LoankeySettings.APH;
    this.declareColumns();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  sortBySetter() {
    let sort = [
      {
        colId: "Crop_Code",
        sort: "asc"
      },
      {
        colId: "cropType",
        sort: "asc"
      },
      {
        colId: "Practice",
        sort: "asc"
      }
    ];

    this.gridApi.setSortModel(sort);
  }

  declareColumns() {
    this.columnDefs = [
      {
        headerName: 'Crop ID',
        field: 'Crop_ID',
        hide: true,
        minWidth: 90,
        width: 90,
        maxWidth: 90
      },
      {
        headerName: 'Crop',
        field: 'Crop_Code',
        tooltip: params => params.data['Crop_Name'],
        editable: params => {
          return params.data.Crop_Code ? false : true;
        },
        cellClass: params => {
          return cellclassmaker(CellType.Text, !params.data.Crop_Code);
        },
        minWidth: 90,
        width: 90,
        maxWidth: 90,
        cellEditor: 'autocompleteEditor',
        valueFormatter: params => {
          return params.data['Crop_Name']
        }
      },
      {
        headerName: 'Crop Type',
        field: 'cropType',
        tooltip: params => params.data['cropType'],
        editable: false,
        minWidth: 90,
        width: 90,
        maxWidth: 90,
        hide: !this.columnsDisplaySettings.cropType,
        valueFormatter: params => {
          return params.data['cropType'];
        }
      },
      {
        headerName: 'Irr Prac',
        field: 'Practice',
        tooltip: params => params.data['Practice'],
        editable: false,
        minWidth: 90,
        width: 90,
        maxWidth: 90,
        suppressSizeToFit: true,
        hide: !this.columnsDisplaySettings.irrPrac,
      },
      {
        headerName: 'Crop Prac',
        field: 'Crop_Practice',
        tooltip: params => params.data['Crop_Practice'],
        editable: false,
        minWidth: 90,
        width: 90,
        maxWidth: 90,
        suppressSizeToFit: true,
        hide: !this.columnsDisplaySettings.cropPrac,
        valueFormatter: params => {
          return params.data['Crop_Practice'];
        }
      }
    ];

    this.years.forEach(element => {
      this.columnDefs.push({
        headerName: element.toString(), suppressSorting: true, sortable: false,
        minWidth: 90,
        width: 90,
        maxWidth: 90,
        field: element.toString(),
        cellClass: cellclassmaker(CellType.Integer, true),
        headerClass: headerclassmaker(CellType.Integer),
        editable: isgrideditable(true),
        cellEditor: 'intCellEditor',
        valueSetter: yieldValueSetter,
        valueFormatter: yieldFormatter
      });
    });

    this.columnDefs.push({
      headerName: 'Crop Yield', suppressSorting: true,
      field: 'CropYield',
      minWidth: 90,
      width: 90,
      maxWidth: 90,
      headerClass: headerclassmaker(CellType.Integer),
      cellClass: 'rightaligned',
      editable: false,
      valueFormatter: params => {
        return numberFormatter(params);
      }
    });

    this.columnDefs.push({
      headerName: 'Rate Yield', suppressSorting: true,
      field: 'Rate_Yield',
      minWidth: 90,
      width: 90,
      maxWidth: 90,
      cellClass: 'rightaligned',
      editable: isgrideditable(false),
      cellEditor: 'intCellEditor',
      hide: !this.columnsDisplaySettings.rateYield,
      valueFormatter: function (params) {
        return params ? numberFormatter(params) : 0;
      }
    });

    this.columnDefs.push({
      headerName: 'APH', suppressSorting: true,
      field: 'APH',
      minWidth: 90,
      width: 90,
      maxWidth: 90,
      editable: false,
      headerClass: headerclassmaker(CellType.Integer),
      cellClass: 'rightaligned',
      valueFormatter: APHRoundValueSetter,
      hide: !this.columnsDisplaySettings.aph
    });

    this.columnDefs.push({
      headerName: 'Ins UoM', suppressSorting: true,
      field: 'InsUOM',
      minWidth: 90,
      width: 90,
      maxWidth: 90,
      editable: false
    });

    this.columnDefs.push({
      headerName: '', suppressSorting: true,
      field: 'value',
      cellRenderer: 'deletecolumn',
      width: 60,
      suppressToolPanel: true,
      maxWidth: 60
    });

    this.context = { componentParent: this };
  }

  Get_Crops_Practices_List() {
    let selection = [];

    let existingCrops = this.localloanobject.CropYield.filter(p => p.ActionStatus != 3);

    this.refdata.CropList.forEach(c => {
      if (!existingCrops.some(x => x.Crop_ID == c.Crop_And_Practice_ID)) {
        selection.push({
          Crop_Code: c.Crop_Code,
          Crop_Name: c.Crop_Name,
          Crop_Type_Code: c.Crop_Type_Code,
          Irr_Prac_Code: c.Irr_Prac_Code,
          Crop_Prac_Code: c.Crop_Prac_Code,
          Crop_And_Practice_ID: c.Crop_And_Practice_ID,
          key: c.Crop_And_Practice_ID,
          value: `${c.Crop_Name} | ${c.Crop_Type_Code} | ${c.Irr_Prac_Code} | ${c.Crop_Prac_Code}`,
          InsUOM: lookupUOM(c.Crop_Code, this.refdata)
        })
      }
    });

    selection = _.uniqBy(selection, 'key');
    selection = _.sortBy(selection, ['Crop_Name', 'Crop_Type_Code', 'Irr_Prac_Code', 'Crop_Prac_Code'])

    return selection;
  }

  getdataforgrid() {
    if (this.localloanobject) {
      this.assignTempID();

      this.rowData = _.orderBy(this.localloanobject.CropYield.filter(cy => {
        return cy.ActionStatus != 3;
      }), ['Crop_Code', 'cropType', 'Practice']);
    }

    this.setModifiedCells();
  }

  private assignTempID() {
    this.localloanobject.CropYield.forEach(cy => {
      if(!cy.crop_yield_id) {
        cy.crop_yield_id = cy.Crop_ID;
      }
    });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    this.adjustToFitAgGrid();
    this.getdataforgrid();
  }

  rowvaluechanged(value: any) {
    //---------Modified yellow values-------------
    if (value && value.oldValue == value.value && value.colDef.field != 'Crop_Code') return;

    if (!value && !value.column && !value.column.colId) return;

    // this.excel.removeExportToExcelTime(Page.crop);

    //MODIFIED YELLOW VALUES
    let modifiedvalues = this.localstorageservice.retrieve(environment.modifiedbase) as Array<String>;

    if (!modifiedvalues.includes("Yld_" + value.data.crop_yield_id + "_" + value.colDef.field)) {
      modifiedvalues.push("Yld_" + value.data.crop_yield_id + "_" + value.colDef.field);
      this.localstorageservice.store(environment.modifiedbase, modifiedvalues);
      setmodifiedsingle("Yld_" + value.data.crop_yield_id + "_" + value.colDef.field, "Yld_");
    }
    //MODIFIED YELLOW VALUES
    //--------------------------------------------

    let obj = value.data;

    if (obj.ActionStatus != 1) obj.ActionStatus = 2;

    this.localloanobject.srccomponentedit = 'YieldComponent';
    this.localloanobject.lasteditrowindex = value.rowIndex;
    this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
    this.publishService.enableSync(Page.crop);
  }

  addrow() {
    let isExistingBlankRow = this.rowData.find(a => !a.Crop_Code);

    if (isExistingBlankRow) {
      this.alertify.alert('Alert', 'You already have a blank row without Crop Code | Crop Type | Practice.');
    } else {
      let item = {
        crop_yield_id: getRandomInt(Math.pow(10, 8), Math.pow(10, 10)),
        Crop_ID: getRandomInt(Math.pow(10, 8), Math.pow(10, 10)),
        Crop: '',
        Loan_ID: this.localloanobject.LoanMaster.Loan_ID,
        Loan_Full_ID: this.localloanobject.Loan_Full_ID,
        IrNI: '',
        Practice: '',
        CropYield: '',
        APH: '',
        InsUOM: '',
        ActionStatus: 1,
        CropYear: this.localloanobject.LoanMaster.Crop_Year
      };

      this.rowData.push(item);
      this.gridApi.updateRowData({ add: [item] });
      this.localloanobject.CropYield.push(item);

      this.gridApi.startEditingCell({
        rowIndex: this.rowData.length,
        colKey: 'Crop_ID'
      });

      this.updateSyncStatus();
    }
  }

  /**
   * Export Yield Data
   */
  onBtExport() {
    const ts = this.dtss.getTimeStamp();

    const params = {
      skipHeader: false,
      columnGroups: true,
      skipFooters: false,
      skipGroups: false,
      skipPinnedTop: false,
      skipPinnedBottom: false,
      allColumns: true,
      exportMode: 'xlsx',
      suppressTextAsCDATA: true,
      fileName: `Yield_${this.localloanobject.Loan_Full_ID}_${ts}.xls`,
      sheetName: 'Sheet 1'
    };

    this.excel.updateExportToExcelTime(Page.crop, ts);
    this.publishService.enableSync(Page.crop);

    // Export Data to Excel file
    this.gridApi.exportDataAsExcel(params);
  }

  onImportExcel(data: any[]) {
    try {

      const rows = this.yieldValidation.getValidCrops(data, this.localloanobject);

      this.localloanobject.CropYield = rows;
      this.publishService.enableSync(Page.crop);
      this.localloanobject.SyncStatus.Status_Crop_Practice = 2;
      this.loanserviceworker.performcalculationonloanobject(this.localloanobject);

    } catch (ex) {
      this.toastar.error(ex.message);
    }
  }

  DeleteClicked(rowIndex: number) {
    this.alertify.confirm(
      "Confirm",
      "Do you really want to delete " +
      (this.rowData[rowIndex].Crop_Name || '') + " | " +
      (this.rowData[rowIndex].cropType || '') + " | " +
      (this.rowData[rowIndex].Practice || '') + " | " +
      (this.rowData[rowIndex].Crop_Practice || '') + " " + " on this record?"
    ).subscribe(
      res => {
        if (res == true) {
          let obj = this.rowData[rowIndex];

          let yIndex = this.localloanobject.CropYield.findIndex(f => f.Crop_ID === obj.Crop_ID && f.ActionStatus != 3);

          if (obj.ActionStatus == 1) {
            this.localloanobject.CropYield.splice(yIndex, 1);
          } else {
            this.deleteAction = true;
            this.localloanobject.CropYield[yIndex].ActionStatus = 3;
          }

          this.updateSyncStatus();
          this.localloanobject.srccomponentedit = undefined;
          this.localloanobject.lasteditrowindex = undefined;
          this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
          this.publishService.enableSync(Page.crop);
        }
      });
  }

  updateSyncStatus() {
    let syncFarmStatus: Sync_Status;

    if (this.localloanobject.CropYield.filter(p => p.ActionStatus == 1 || p.ActionStatus == 3).length > 0) {
      syncFarmStatus = Sync_Status.ADDORDELETE;
    } else if (
      this.localloanobject.CropYield.filter(p => p.ActionStatus == 2).length > 0
    ) {
      syncFarmStatus = Sync_Status.EDITED;
    } else {
      syncFarmStatus = Sync_Status.NOCHANGE;
    }

    this.localloanobject.SyncStatus.Status_Crop_Practice = syncFarmStatus;
  }

  adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  isLoanEditable() {
    return (
      !this.localloanobject ||
      this.localloanobject.LoanMaster.Loan_Status === LoanStatus.Working
    );
  }

  columnVisible($event) {

    if (this.columnApi != undefined && $event != undefined) {
      //first One to save to Loan Settings
      let LoanSettings: Loansettings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
      if (LoanSettings == undefined || LoanSettings == null) {
        LoanSettings = new Loansettings();
      }
      if (
        LoanSettings.Loan_key_Settings == undefined ||
        LoanSettings.Loan_key_Settings == null
      ) {
        LoanSettings.Loan_key_Settings = new Loan_Key_Visibilty();
      }
      //Since we dont have column thats changed so lets see manually

      let mainkey = '';
      let preferencesKey = '';

      switch ($event.column.colDef.headerName) {
        case 'Crop Type':
          mainkey = 'Crop_Type';
          preferencesKey = 'cropType';
          break;
        case 'Irr Prac':
          mainkey = 'Irr_Practice';
          preferencesKey = 'irrPrac';
          break;
        case 'Crop Prac':
          mainkey = 'Crop_Practice';
          preferencesKey = 'cropPrac';
          break;
        case 'Rate Yield':
          mainkey = 'RateYield';
          preferencesKey = 'rateYield';
          break;
        case 'APH':
          mainkey = 'APH';
          preferencesKey = 'aph';
          break;
      }
      let column = $event.column;
      LoanSettings.Loan_key_Settings[mainkey] = column.visible;
      this.settingsService.preferences.loanSettings.columnsDisplaySettings[preferencesKey] = column.visible;

      this.localloanobject.LoanMaster.Loan_Settings = JSON.stringify(LoanSettings);

      this.localloanobject.srccomponentedit = 'UpdateColumnSettings_Yield';
      this.dataService.setLoanObject(this.localloanobject);
      this.localloanobject.srccomponentedit = undefined;

      //Second Execution
      this.adjustToFitAgGrid();
    }
    this.adjustToFitAgGrid();
  }

  syncenabled() {
    if (
      this.rowData.filter(p => p.ActionStatus != undefined).length > 0 ||
      this.deleteAction
    )
      return '';
    else return 'disabled';
  }

  getCropValues() {
    let cropValues = _.uniqBy(this.mapCropValues(), 'key');
    return { values: cropValues };
  }

  private mapCropValues(): Array<Object> {
    if (!this.refdata && !this.refdata.CropList) return [];

    let cropValues = [];

    this.refdata.CropList.map(cl => {
      cropValues.push({ key: cl.Crop_Code, value: cl.Crop_Name });
    });

    return cropValues;
  }
}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'yield.dialog.component.html'
})
export class YieldDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<YieldDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
