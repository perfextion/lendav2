import { Component, OnInit, OnDestroy } from '@angular/core';

import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';
import { Sync_Status } from '@lenda/models/syncstatusmodel';
import { Page } from '@lenda/models/page.enum';
import { loan_model } from '@lenda/models/loanmodel';
import { ColumnsDisplaySettings } from "@lenda/preferences/models/loan-settings/columns-display.model";

import { CropService } from './crop.service';
import { PublishService } from '@lenda/services/publish.service';
import { DataService } from '@lenda/services/data.service';
import { SettingsService } from "@lenda/preferences/settings/settings.service";
import { MasterService } from '@lenda/master/master.service';
import { CanComponentDeactivate } from '@lenda/services/route/deactivate.guard';

@Component({
  selector: 'app-crop',
  templateUrl: './crop.component.html',
  styleUrls: ['./crop.component.scss'],
  providers: [CropService]
})
export class CropComponent implements OnInit, CanComponentDeactivate, OnDestroy {
  public columnsDisplaySettings: ColumnsDisplaySettings = new ColumnsDisplaySettings;
  public currentPageName: string = Page.crop;

  private subscription: ISubscription;
  private localloanobject: loan_model = new loan_model();
  private preferencesChangeSub: ISubscription;

  constructor(
    private cropService: CropService,
    private localStorageService: LocalStorageService,
    private publishService: PublishService,
    private dataService: DataService,
    private settingsService: SettingsService,
    public masterSvc: MasterService
  ) {
    this.localloanobject = this.localStorageService.retrieve(environment.loankey);

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localloanobject = res;
    });
  }

  ngOnInit() {
    this.getUserPreferences();
  }

  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;
    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe((preferences) => {
      this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
    });
  }

  /**
  * Sync to database - publish button event
  */
  synctoDb() {
    this.publishService.syncCompleted();
    this.cropService.syncToDb(this.localStorageService.retrieve(environment.loankey));
  }

  // Check if user has unsave changes
  confirm() {
    return this.localloanobject.SyncStatus.Status_Crop_Practice == Sync_Status.ADDORDELETE;
  }
}
