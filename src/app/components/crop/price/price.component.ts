import { Component, OnInit, OnDestroy, Input, ViewEncapsulation } from '@angular/core';

import { ISubscription } from 'rxjs/Subscription';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';

import { environment } from '@env/environment.prod';

import { LoanStatus } from '@lenda/models/loanmodel';
import { Loan_Key_Visibilty, Loansettings } from '@lenda/models/loansettings';
import { loan_model, Loan_Crop } from '@lenda/models/loanmodel';
import { getNumericCellEditor, numberValueSetter } from '@lenda/Workers/utility/aggrid/numericboxes';
import { SelectEditor } from '@lenda/aggridfilters/selectbox';
import { DeleteButtonRenderer } from '@lenda/aggridcolumns/deletebuttoncolumn';
import { CropTypevaluesetter } from '@lenda/Workers/utility/aggrid/cropboxes';
import { headerclassmaker, CellType, cellclassmaker, isgrideditable, calculatecolumnwidths, autoSizeAll } from '@lenda/aggriddefinations/aggridoptions';
import { Page } from '@lenda/models/page.enum';
import { EditDefaultSettings } from '@lenda/preferences/models/user-settings/edit-default-settings.model';
import { CroptypeidtonameFormatter, CropidtonameFormatter, percentageFormatter, croppriceFormatter, calculatedNumberFormatter, currencyFormatter } from '@lenda/aggridformatters/valueformatters';
import { ColumnsDisplaySettings } from "@lenda/preferences/models/loan-settings/columns-display.model";
import { LoanSettings, TableState, Chevrons, TableAddress } from '@lenda/preferences/models/loan-settings/index.model';

import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { MasterService } from '@lenda/master/master.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { PublishService } from '@lenda/services/publish.service';
import { DataService } from '@lenda/services/data.service';

declare function setmodifiedall(array, keyword): any;
declare function setmodifiedsingle(obj, keyword): any;

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PriceComponent implements OnInit, OnDestroy {
  private _columnsDisplaySettings: ColumnsDisplaySettings = new ColumnsDisplaySettings();
  private preferencesChangeSub: ISubscription;

  @Input() set columnsDisplaySettings(value: ColumnsDisplaySettings) {
    this._columnsDisplaySettings = value;
    this.declareColumns();
  }

  get columnsDisplaySettings(): ColumnsDisplaySettings {
    return this._columnsDisplaySettings;
  }

  private editDefaultSettings : EditDefaultSettings = new EditDefaultSettings();

  public refdata: any = {};
  public columnDefs = [];
  private localloanobject: loan_model = new loan_model();
  private subscription: ISubscription;

  // Aggrid
  public rowData: Loan_Crop[] = [];
  public components;
  public context;
  public frameworkcomponents;
  public editType;
  private gridApi;
  private columnApi;

  public style = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };

  public defaultColDef = {
    enableValue: true,
    enableRowGroup: true,
    enablePivot: true
  };

  //region Ag grid Configuration
  constructor(
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public logging: LoggingService,
    public alertify: AlertifyService,
    public loanapi: LoanApiService,
    private publishService: PublishService,
    private dataService : DataService,
    private dtss: DatetTimeStampService,
    private settingsService: SettingsService,
    public masterSvc: MasterService
  ) {

    this.frameworkcomponents = { selectEditor: SelectEditor, deletecolumn: DeleteButtonRenderer };
    this.components = { numericCellEditor: getNumericCellEditor(true) };
    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);

    this.context = { componentParent: this };
  }

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      // this.logging.checkandcreatelog(1, 'CropPrice', "LocalStorage updated");
      this.localloanobject = res;
      if (res.srccomponentedit == "PriceComponent") {
        //if the same table invoked the change .. change only the edited row
        this.rowData[res.lasteditrowindex] = this.localloanobject.LoanCrops.filter(p => p.ActionStatus != 3)[res.lasteditrowindex];
      } else if(res.srccomponentedit == 'UpdateColumnSettings_Yield') {
        this.rowData = [];
        this.rowData = this.localloanobject.LoanCrops !== null ? this.localloanobject.LoanCrops.filter(p => p.ActionStatus != 3) : [];
        this.hideUnhideLoanKeys();
        this.declareColumns();
      } else {
        this.rowData = [];
        this.rowData = this.localloanobject.LoanCrops !== null ? this.localloanobject.LoanCrops.filter(p => p.ActionStatus != 3) : [];
      }
      //this.getgridheight();
      this.gridApi.refreshCells();
      setTimeout(() => {
        setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), "Prc_");
      }, 0);
    })
    this.getdataforgrid();

    this.getUserPreferences();
  }

  private getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;
    this.editDefaultSettings = this.settingsService.preferences.userSettings.editDefaultSettings;

    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe((preferences) => {
      this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
      this.editDefaultSettings = preferences.userSettings.editDefaultSettings;
      this.hideUnhideLoanKeys();
      this.declareColumns();
    });
  }

  //Hide Columns Based ON Loan Settings
  private LoankeySettings:Loan_Key_Visibilty;
  private hideUnhideLoanKeys() {
    let LoanSettings: Loansettings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
    if (LoanSettings != undefined || LoanSettings != null) {
      if (LoanSettings.Loan_key_Settings != undefined || LoanSettings.Loan_key_Settings != null) {
        this.LoankeySettings=LoanSettings.Loan_key_Settings
      }
      else
      {
        this.LoankeySettings=new Loan_Key_Visibilty();
      }
    }
    else
    {
      this.LoankeySettings=new Loan_Key_Visibilty();
    }
    this.columnsDisplaySettings.cropType=this.LoankeySettings.Crop_Type==null?this.columnsDisplaySettings.cropType:this.LoankeySettings.Crop_Type;
  }

  public Loankeys = ["Crop_Type_Code"];
  displayColumnsChanged($event){
    if (this.columnApi != undefined && $event!=undefined) {

      //first One to save to Loan Settings
      let LoanSettings: Loansettings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
      if (LoanSettings == undefined || LoanSettings == null) {
        LoanSettings = new Loansettings();
      }
      if (LoanSettings.Loan_key_Settings == undefined || LoanSettings.Loan_key_Settings == null) {
        LoanSettings.Loan_key_Settings = new Loan_Key_Visibilty();
      }
      //Since we dont have column thats changed so lets see manually
      this.Loankeys.forEach(key => {
        let mainkey = "";
        let preferencesKey = '';

        switch (key) {
          case "Crop_Type_Code":
            mainkey = "Crop_Type"
            preferencesKey = 'cropType';
            break;
        }

        let column = this.columnApi.getColumn(key);
        LoanSettings.Loan_key_Settings[mainkey] = column.visible;
        this.settingsService.preferences.loanSettings.columnsDisplaySettings[preferencesKey] = column.visible;
      });

      this.localloanobject.LoanMaster.Loan_Settings = JSON.stringify(LoanSettings);
      this.localloanobject.srccomponentedit = 'UpdateColumnSettings_Price';
      this.dataService.setLoanObjectwithoutsubscription(this.localloanobject);
      this.localloanobject.srccomponentedit = undefined;
      //Second Execution
      this.adjustToFitAgGrid();
    }
    this.adjustToFitAgGrid();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  ngAfterViewInit() {
    //calling setmodifiedall here as it was not working on page refresh
    //because in get grid data methoed it was getting called onInit() i.e. before pageload
    setTimeout(() => {
      setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), "Prc_");
    }, 10);
  }

  sortBySetter() {
    let sort = [
      {
        colId: "Crop_Code",
        sort: "asc"
      },
      {
        colId: "Crop_Type_Code",
        sort: "asc"
      }
    ];
    this.gridApi.setSortModel(sort);
  }

  private declareColumns() {
    this.columnDefs = [
      {
        headerName: 'Crop',
        field: 'Crop_Code',
        tooltip: params => CropidtonameFormatter(params, this.refdata),
        valueFormatter: params => CropidtonameFormatter(params, this.refdata),
        minWidth: 90,
        width: 90,
        maxWidth: 90
      },
      {
        headerName: 'Crop Type',
        field: 'Crop_Type_Code',
        tooltip: params => params.data['Crop_Type_Code'],
        valueFormatter: CroptypeidtonameFormatter,
        valueSetter: CropTypevaluesetter,
        minWidth: 90,
        width: 90,
        maxWidth: 90,
        hide: !this.columnsDisplaySettings.cropType
      },
      {
        headerName: 'Crop Price',
        field: 'Crop_Price',
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: cellclassmaker(CellType.Integer, false),
        valueFormatter: croppriceFormatter,
        suppressSorting: true,
        sortable:false,
        minWidth: 90,
        width: 90,
        maxWidth: 90
      },
      {
        headerName: 'Basis Adj',
        field: 'Basic_Adj',
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: cellclassmaker(CellType.Integer, true),
        editable: isgrideditable(true),
        cellEditor: "numericCellEditor",
        valueSetter: numberValueSetter,
        suppressSorting: true,
        sortable:false,
        valueFormatter: params => {
          return currencyFormatter(params, 4);
        },
        minWidth: 90,
        width: 90,
        maxWidth: 90
      },
      {
        headerName: 'Mkt Adj',
        field: 'Marketing_Adj',
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: cellclassmaker(CellType.Integer, false),
        valueFormatter: croppriceFormatter,
        suppressSorting: true,
        sortable:false,
        minWidth: 90,
        width: 90,
        maxWidth: 90
      },
      {
        headerName: 'Rebate Adj',
        field: 'Rebate_Adj',
        headerClass: headerclassmaker(CellType.Integer),
        suppressSorting: true,
        sortable:false,
        cellClass: cellclassmaker(CellType.Integer, true),
        editable: isgrideditable(true),
        cellEditor: "numericCellEditor",
        valueSetter: numberValueSetter,
        valueFormatter: params => {
          return currencyFormatter(params, 4);
        },
        minWidth: 90, width: 90, maxWidth: 90
      },
      {
        headerName: 'Adj Price',
        field: 'Adj_Price',
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: cellclassmaker(CellType.Integer, false),
        suppressSorting: true,
        sortable:false,
        valueFormatter: params => {
          return currencyFormatter(params, 4);
        },
        minWidth: 90,
        width: 90,
        maxWidth: 90
      },
      {
        headerName: 'Contract Qty',
        field: 'Contract_Qty',
        suppressSorting: true,
        sortable:false,
        editable: false,
        headerClass:
        headerclassmaker(CellType.Integer),
        cellClass: cellclassmaker(CellType.Integer, false),
        valueFormatter: function (params){
          return params && params.value >= 0 ? calculatedNumberFormatter(params, 0) : '';
        },
        minWidth: 90,
        width: 90,
        maxWidth: 90
      },
      {
        headerName: 'Contract Price',
        field: 'Contract_Price',
        suppressSorting: true,
        sortable:false,
        editable: false,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: cellclassmaker(CellType.Integer, false),
        valueFormatter: params => {
          return currencyFormatter(params, 4);
        },
        minWidth: 90,
        width: 90,
        maxWidth: 90
      },
      {
        headerName: 'Booked %',
        field: 'Percent_booked',
        sortable:false,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: cellclassmaker(CellType.Integer, false),
        valueFormatter: percentageFormatter,
        minWidth: 90,
        width: 90,
        maxWidth: 90
      },
      {
        headerName: 'Ins UoM',
        field: 'InsUOM',
        editable: false,
        suppressSorting: true,
        sortable:false,
        minWidth: 90,
        width: 90,
        maxWidth: 90
      },
    ];
  }

  private getdataforgrid() {
    let obj: any = this.localstorageservice.retrieve(environment.loankey);
    // this.logging.checkandcreatelog(1, 'CropPrice', "LocalStorage retrieved");
    if (obj != null && obj != undefined) {
      this.localloanobject = obj;
      this.rowData = [];
      this.rowData = this.localloanobject.LoanCrops !== null ? this.localloanobject.LoanCrops.filter(p => p.ActionStatus != 3) : [];
      this.rowData = _.orderBy(this.rowData, ['Crop_Code', 'Crop_Type_Code'], ['asc', 'asc']);
    }
    // this.gridApi && this.gridApi.sizeColumnsToFit();
    setTimeout(() => {
      setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), "Prc_");
    }, 10);
  }

  rowvaluechanged(value: any) {

    //If no value change dont record it.
    if(value && value.oldValue == value.value) return;

    //---------Modified yellow values-------------

    let modifiedvalues = this.localstorageservice.retrieve(environment.modifiedbase) as Array<String>;

    if (!modifiedvalues.includes("Prc_" + value.data.Crop_ID + "_" + value.colDef.field)) {
      modifiedvalues.push("Prc_" + value.data.Crop_ID + "_" + value.colDef.field);
      this.localstorageservice.store(environment.modifiedbase, modifiedvalues);
        setmodifiedsingle("Prc_" + value.data.Crop_ID + "_" + value.colDef.field, "Prc_");
    }
    //--------------------------------------------

    // check edit default basis adj

    let col = value.colDef.field;
    if(col == 'Basic_Adj') {
      this.checkIfBasisAdjUpdated(value.data);
    }

    //Change class here for editing

    let obj: Loan_Crop = value.data;
    if (obj.ActionStatus != 1) {
      obj.ActionStatus = 2;
    }

    let marketingContracts = this.localloanobject.LoanMarketingContracts.find(mc => mc.Crop_Code === obj.Crop_Code);
    if (marketingContracts) {
      obj.Contract_Price = marketingContracts.Price || 0; //
      obj.Contract_Qty = marketingContracts.Quantity || 0;

      //the same caclulation is in marketing calculation service, which should be shisted to common place
      obj.Marketing_Adj = (obj.Contract_Price - (obj.Basic_Adj + obj.Crop_Price)) * (obj.Percent_booked / 100);
      obj.Adj_Price = (obj.Crop_Price || 0) + (obj.Basic_Adj || 0) + (obj.Marketing_Adj || 0) + (obj.Rebate_Adj || 0);

    } else {
      obj.Adj_Price = (obj.Crop_Price || 0) + (obj.Basic_Adj || 0) + (obj.Marketing_Adj || 0) + (obj.Rebate_Adj || 0);
    }

    //this shall have the last edit
    this.localloanobject.srccomponentedit = "PriceComponent";
    this.localloanobject.lasteditrowindex = value.rowIndex;
    this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
    this.publishService.enableSync(Page.crop);
  }

  get canUpdateBasisDefault() {
    if(this.editDefaultSettings) {
      return this.editDefaultSettings.isCropBasisEnabled;
    }
    return false;
  }

  checkIfBasisAdjUpdated(crop: Loan_Crop) {
    if(this.canUpdateBasisDefault && !crop.Update_Basis_Ind) {
      this.alertify.confirm(
        'Confirm',
        'Warning: Default data is being changed. Do you want to update the default data?'
      ).subscribe(res => {
        if(res) {
          crop.Update_Basis_Ind = 1;
          crop.Z_Office_ID = this.localloanobject.LoanMaster.Office_ID;
          crop.Z_Region_ID = this.localloanobject.LoanMaster.Region_ID;
        } else {
          crop.Update_Basis_Ind = 0;
        }
      });

      // crop.Update_Basis_Ind = 1;
      // crop.Z_Office_ID = this.localloanobject.LoanMaster.Office_ID;
      // crop.Z_Region_ID = this.localloanobject.LoanMaster.Region_ID;
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    autoSizeAll(this.columnApi);

    this.gridApi.addEventListener('displayedColumnsChanged', event => {
      if (environment.isDebugModeActive) console.time('started');

      let state: any = this.columnApi.getColumnState();

      let obj = JSON.parse(
        this.localloanobject.LoanMaster.Loan_Settings
      ) as LoanSettings;

      if (obj.Table_States == undefined || obj.Table_States == null) {
        obj.Table_States = new Array<TableState>();
      }

      let data = obj.Table_States.find(
        p =>
          p.address.page == Page.crop &&
          p.address.chevron == Chevrons.crop_prices
      );

      if (data != undefined) {
        data.data = state;
      } else {
        let farm_farmstate = new TableState();
        farm_farmstate.address = new TableAddress();
        farm_farmstate.address.page = Page.crop;
        farm_farmstate.address.chevron = Chevrons.crop_prices;
        farm_farmstate.data = state;
        obj.Table_States.push(farm_farmstate);
      }

      this.localloanobject.LoanMaster.Loan_Settings = JSON.stringify(obj);
      this.dataService.setLoanObjectwithoutsubscription(this.localloanobject);

      if (environment.isDebugModeActive) console.timeEnd('started');
    });

    let loansettingd = JSON.parse(
      this.localloanobject.LoanMaster.Loan_Settings
    ) as LoanSettings;

    if (
      loansettingd.Table_States != undefined &&
      loansettingd.Table_States != null
    ) {
      let data = loansettingd.Table_States.find(
        p =>
          p.address.page == Page.crop &&
          p.address.chevron == Chevrons.crop_prices
      );
      if (data != undefined) this.columnApi.setColumnState(data.data);
    }

    this.adjustToFitAgGrid();
  }

  private adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  displayedColumnsChanged(params){
    this.adjustToFitAgGrid();
  }

  isLoanEditable(){
    return !this.localloanobject || this.localloanobject.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  onColumnhiderequested(event, header: string) {
    let checked = event.srcElement.checked;
    this.columnApi.setColumnVisible(header, checked);
    //this.gridApi.sizeColumnsToFit();
    this.adjustToFitAgGrid();
  }

  onGridSizeChanged(params) {
    params.api.resetRowHeights();
  }

  public gridOptions = {
    getRowNodeId: function (data) {
      return "Prc_" + data.Crop_ID;
    }
  }
}

