import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  OnDestroy
} from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { SelectEditor } from '@lenda/aggridfilters/selectbox';
import { DeleteButtonRenderer } from '@lenda/aggridcolumns/deletebuttoncolumn';
import {
  getNumericCellEditor,
  numberWithOneDecPrecValueSetter
} from '@lenda/Workers/utility/aggrid/numericboxes';
import { environment } from '@env/environment.prod';
import { RefDataModel, Ref_Other_Income } from '@lenda/models/ref-data-model';
import {
  loan_model,
  LoanStatus,
  Loan_Other_Income,
  Loan_Collateral,
  Collateral_Category_Code
} from '@lenda/models/loanmodel';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';
import { DataService } from '@lenda/services/data.service';
import { ISubscription } from 'rxjs/Subscription';
import {
  calculatecolumnwidths,
  headerclassmaker,
  CellType,
  cellclassmaker,
  isgrideditable,
  IParam,
  SelectEditorParams
} from '@lenda/aggriddefinations/aggridoptions';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';
import { PublishService } from '@lenda/services/publish.service';
import { Page } from '@lenda/models/page.enum';
import { currencyFormatter } from '@lenda/aggridformatters/valueformatters';
import { OtherIncomeAutocompleteCellEditor } from '@lenda/aggridcolumns/other-income-autocomplete/other-income-autocomplete.component';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { CheckboxCellRenderer } from '@lenda/aggridcolumns/checkbox-cell-editor/checkbox-cell-rendere';
import { Helper } from '@lenda/services/math.helper';
import * as _ from 'lodash';
import { CropValidation } from '@lenda/Workers/utility/validation-functions';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { errormodel } from '@lenda/models/commonmodels';

declare function setmodifiedall(array, keyword): any;
declare function setmodifiedsingle(obj, keyword): any;

type OTParams = IParam<OtherIncomeComponent, Loan_Other_Income>;

@Component({
  selector: 'app-other-income',
  templateUrl: './other-income.component.html',
  styleUrls: ['./other-income.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OtherIncomeComponent implements OnInit, OnDestroy {
  currentPageName = Page.crop;
  refdata: RefDataModel;
  localloanobject: loan_model;
  subscription: ISubscription;
  Ref_Other_Incomes: Ref_Other_Income[];

  rowData = [];
  columnDefs = [];

  private _columnsDisplaySettings: ColumnsDisplaySettings = new ColumnsDisplaySettings();
  @Input() set columnsDisplaySettings(value: ColumnsDisplaySettings) {
    this._columnsDisplaySettings = value;
  }

  get columnsDisplaySettings(): ColumnsDisplaySettings {
    return this._columnsDisplaySettings;
  }

  @Input() expanded = true;

  //#region Ag Grid

  style: any = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };

  defaultColDef = {
    enableValue: true,
    enableRowGroup: true,
    enablePivot: true
  };

  gridApi: any;
  columnApi: any;
  gridOptions = {
    getRowNodeId: data => `OTHR_${data.Loan_Other_Income_ID}`
  }
  context: any;
  frameworkcomponents: any;
  components: any;

  //#endregion Ag Grid

  cropList: SelectEditorParams;

  errorsub: ISubscription;

  constructor(
    private localstorageservice: LocalStorageService,
    private loanserviceworker: LoancalculationWorker,
    private dataService: DataService,
    private publishService: PublishService,
    private alertify: AlertifyService,
    private validationService: ValidationService
  ) {
    this.context = { componentParent: this };

    this.frameworkcomponents = {
      selectEditor: SelectEditor,
      deletecolumn: DeleteButtonRenderer,
      autocompleteEditor: OtherIncomeAutocompleteCellEditor,
      checkboxCellRenderer: CheckboxCellRenderer
    };

    this.components = { numericCellEditor: getNumericCellEditor(false) };

    this.refdata = this.localstorageservice.retrieve(
      environment.referencedatakey
    );

    this.localloanobject = this.localstorageservice.retrieve(
      environment.loankey
    );

    this.Ref_Other_Incomes = this.refdata.Ref_Other_Incomes || [];
  }

  ngOnInit() {
    this.declareColDefs();

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localloanobject = res;

      if (res.srccomponentedit == 'OtherIncome') {
        let OtherIncomes = res.LoanOtherIncomes.filter(
          a => a.ActionStatus != 3
        );

        this.rowData[res.lasteditrowindex] = OtherIncomes[res.lasteditrowindex];
        this.gridApi.refreshCells();
      } else {
        this.rowData = res.LoanOtherIncomes.filter(a => a.ActionStatus != 3);
        this.gridApi.refreshCells();
      }

      this.setValidationErrorForOtherIncome();
      this.getErrorsAndHighlightModifiedCells();
    });

    this.setValidationErrorForOtherIncome();

    this.getErrorsAndHighlightModifiedCells();

    this.errorsub = this.localstorageservice.observe(environment.errorbase).subscribe(() => {
      setTimeout(() => {
        this.getcurrenterrors();
      }, 5);
    });
  }

  private getErrorsAndHighlightModifiedCells() {
    setTimeout(() => {
      this.getcurrenterrors();
      setmodifiedall(
        this.localstorageservice.retrieve(environment.modifiedbase),
        'OTHR_'
      );
    }, 0);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.errorsub) {
      this.errorsub.unsubscribe();
    }
  }

  private declareColDefs() {
    this.columnDefs = [
      {
        headerName: 'Category',
        field: 'Other_Income_Name',
        minWidth: 200,
        width: 200,
        maxWidth: 200,
        editable: (params: OTParams) => {
          let isnotlinked = params.data.Loan_Collateral_ID  == 0 || params.data.Collateral_Ind == 0;
          let status = params.context.componentParent.Loan_Status;
          return isgrideditable(isnotlinked, status);
        },
        cellClassRules: {
          'editable-color': (params: OTParams) => {
            let status = params.context.componentParent.Loan_Status;
            let isnotlinked = params.data.Loan_Collateral_ID  == 0 && params.data.Collateral_Ind == 0;
            return status == 'W' && isnotlinked;
          },
          'default-cell': (params: OTParams) => {
            let status = params.context.componentParent.Loan_Status;
            let islinked = params.data.Loan_Collateral_ID  > 0 && params.data.Collateral_Ind == 1;
            return status != 'W' || islinked;
          }
        },
        cellEditor: 'autocompleteEditor'
      },
      {
        headerName: 'Crop',
        field: 'Crop_Code',
        minWidth: 200,
        width: 200,
        maxWidth: 200,
        editable: (params: OTParams) => {
          let isstoredcrop = params.data.Other_Income_Name == 'Stored Crop';
          let status = params.context.componentParent.Loan_Status;
          return isgrideditable(isstoredcrop, status);
        },
        cellClassRules: {
          'editable-color': (params: OTParams) => {
            let status = params.context.componentParent.Loan_Status;
            let isstoredcrop = params.data.Other_Income_Name == 'Stored Crop';
            return status == 'W' && isstoredcrop;
          },
          'default-cell': (params: OTParams) => {
            let status = params.context.componentParent.Loan_Status;
            let isnotstoredcrop = params.data.Other_Income_Name != 'Stored Crop';
            return status != 'W' || isnotstoredcrop;
          }
        },
        cellEditor: 'selectEditor',
        cellEditorParams: this.getCropList(),
        valueFormatter: (params: OTParams) => {
          let crop_code = params.data.Crop_Code;
          if(params.data.Other_Income_Name == 'Stored Crop') {
            let crop = this.refdata.CropList.find(a => a.Crop_Code == crop_code);
            if(crop) {
              return crop.Crop_Name;
            }
            return '';
          }
          return '';
        },
        validations: {
          cropName: CropValidation
        }
      },
      {
        headerName: 'Description',
        field: 'Other_Description_Text',
        minWidth: 300,
        width: 300,
        maxWidth: 300,
        editable: params => {
          let status = params.context.componentParent.Loan_Status;
          return isgrideditable(true, status);
        },
        cellClassRules: {
          'editable-color': (params: OTParams) => {
            let status = params.context.componentParent.Loan_Status;
            return status == 'W';
          },
          'default-cell': (params: OTParams) => {
            let status = params.context.componentParent.Loan_Status;
            return status != 'W';
          }
        }
      },
      {
        headerName: 'Amount',
        field: 'Amount',
        headerClass: headerclassmaker(CellType.Integer),
        minWidth: 120,
        width: 120,
        maxWidth: 120,
        cellEditor: "numericCellEditor",
        editable: params => {
          let status = params.context.componentParent.Loan_Status;
          return isgrideditable(true, status);
        },
        cellClassRules: {
          'editable-color rightaligned': (params: OTParams) => {
            let status = params.context.componentParent.Loan_Status;
            return status == 'W';
          },
          'default-cell rightaligned': (params: OTParams) => {
            let status = params.context.componentParent.Loan_Status;
            return status != 'W';
          }
        },
        valueSetter: params => numberWithOneDecPrecValueSetter(params, 0),
        valueFormatter: params => {
          return currencyFormatter(params, 0);
        }
      },
      {
        headerName: 'Collateral',
        field: 'Collateral_Ind',
        minWidth: 90, width: 90, maxWidth: 90,
        cellClass: function (params) {
          let status = params.context.componentParent.Loan_Status;
          return cellclassmaker(CellType.Text, true, '', status);
        },
        cellRenderer: 'checkboxCellRenderer',
        cellRendererParams: (params: OTParams) => {
          return {
            type: 'Collateral_Ind',
            edit: params.context.componentParent.Loan_Status == 'W'
          }
        }
      },
      {
        headerName: '',
        field: 'value',
        cellRenderer: 'deletecolumn',
        width: 60,
        maxWidth: 60,
        suppressToolPanel: true
      }
    ];
  }

  /**
   * Get Crop List for Dropdown
   */
  private getCropList() {
    if(!this.cropList) {
      let crops = _.uniqBy(this.refdata.CropList, a => a.Crop_Code);
      this.cropList =  {
        values: crops.map(a => {
          return {
            key: a.Crop_Code,
            value: a.Crop_Name
          }
        })
      }
    }
    return this.cropList;
  }

  getdataforgrid() {
    if (!!this.localloanobject) {
      if (this.localloanobject.LoanOtherIncomes) {
        this.rowData = _.sortBy(
          this.localloanobject.LoanOtherIncomes.filter(cy => {
            return cy.ActionStatus != 3;
          }),
          a => a.Sort_Order
        );
      } else {
        this.rowData = [];
      }
    }

    this.setValidationErrorForOtherIncome();

    setTimeout(() => {
      this.getcurrenterrors();
      setmodifiedall(
        this.localstorageservice.retrieve(environment.modifiedbase),
        'OTHR_'
      );
      this.rowData = _.sortBy(this.rowData, a => a.Sort_Order);
    }, 10);
  }

  displayColumnsChanged($event) {}

  /**
   * Callback function for Checkbox ( Collateral_Ind )
   */
  MethodFromCheckbox(params: any, type: string, $event: any) {
    if(type == 'Collateral_Ind') {
      if ($event.srcElement.checked) {
        params.data['Collateral_Ind'] = 1;
      } else {
        params.data['Collateral_Ind'] = 0;
      }

      params.api.refreshCells({
        columns: ['Collateral_Ind']
      });

      let obj = params.data as Loan_Other_Income;

      if (obj.ActionStatus != 1) obj.ActionStatus = 2;

      this.localloanobject.srccomponentedit = 'OtherIncome';
      this.localloanobject.lasteditrowindex = params.rowIndex;

      this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
      if(this.isLoanEditable()) {
        this.publishService.enableSync(Page.crop);
      }
    }
  }


  rowvaluechanged(value) {
    //MODIFIED YELLOW VALUES
    let modifiedvalues = this.localstorageservice.retrieve(environment.modifiedbase) as Array<String>;

    if (!modifiedvalues.includes("OTHR_" + value.data.Loan_Other_Income_ID + "_" + value.colDef.field)) {
      modifiedvalues.push("OTHR_" + value.data.Loan_Other_Income_ID + "_" + value.colDef.field);
      this.localstorageservice.store(environment.modifiedbase, modifiedvalues);
      setmodifiedsingle("OTHR_" + value.data.Loan_Other_Income_ID + "_" + value.colDef.field, "OTHR_");
    }

    let obj: Loan_Other_Income = value.data;

    if (obj.ActionStatus != 1) obj.ActionStatus = 2;

    this.UpdateRelatedCollaterals(obj, value);

    this.localloanobject.srccomponentedit = 'OtherIncome';
    this.localloanobject.lasteditrowindex = value.rowIndex;

    this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
    this.publishService.enableSync(Page.crop);
  }

  private UpdateRelatedCollaterals(obj: Loan_Other_Income, value: any) {
    if(obj.Collateral_Ind == 1) {
      let Collaterals = this.localloanobject.LoanCollateral.filter(
        a =>
          a.ActionStatus != 3 &&
          (a.Collateral_ID == obj.Loan_Collateral_ID || a.Loan_Other_Income_ID == obj.Loan_Other_Income_ID)
      );

      Collaterals.forEach(Collateral => {
        this.UpdatedCollateral(Collateral, obj, value.column.colId);
      });
    }
  }

  private UpdatedCollateral(Collateral: Loan_Collateral, obj: Loan_Other_Income, column: string) {
    Collateral.Net_Market_Value = obj.Amount;
    Collateral.Market_Value = obj.Amount + (parseFloat(Collateral.Prior_Lien_Amount as any) || 0);

    if(Collateral.Collateral_Category_Code == Collateral_Category_Code.StoredCrop) {
      if(!Collateral.Qty) {
        Collateral.Qty = 1;
      }

      Collateral.Price = Helper.divide(Collateral.Market_Value, Collateral.Qty);
      Collateral.Crop_Detail = obj.Crop_Code;
    }

    if(column == 'Other_Description_Text') {
      Collateral.Collateral_Description = obj.Other_Description_Text;
    }

    if(column == 'Other_Income_Name') {
      Collateral.Collateral_Category_Code = this.OtherIncomeNameToCollateralCategory(obj.Other_Income_Name);
    }

    if (Collateral.ActionStatus != 1) {
      Collateral.ActionStatus = 2;
    }
  }

  private OtherIncomeNameToCollateralCategory(income_name) {
    if(income_name == 'FSA') {
      return Collateral_Category_Code.FSA;
    }

    if(income_name == 'Livestock') {
      return Collateral_Category_Code.Livestock;
    }

    if(income_name == 'Equiment') {
      return Collateral_Category_Code.Equipemt;
    }

    if(income_name == 'Stored Crop') {
      return Collateral_Category_Code.StoredCrop;
    }

    if(income_name == 'Real Estate') {
      return Collateral_Category_Code.RealEState;
    }

    return Collateral_Category_Code.Other;
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;

    this.adjustToFitAgGrid();
    this.setValidationErrorForOtherIncome();
    this.getdataforgrid();
  }

  currenterrors: Array<errormodel>;

  getcurrenterrors() {
    this.currenterrors = (this.localstorageservice.retrieve(environment.errorbase) as Array<errormodel>).filter(
      p => p.chevron == 'OTHR'
    );

    this.validationService.highlighErrorCells(this.currenterrors, 'other_income');
  }

  adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  addrow() {
    if (!this.rowData) {
      this.rowData = [];
    }

    let hasBlankRow = this.rowData.some(a => !a.Other_Income_Name);

    if(hasBlankRow) {
      this.alertify.alert('Alert', 'You already have a blank row without Category.');
    } else {
      let newItem = new Loan_Other_Income();
      newItem.Loan_Full_ID = this.localloanobject.Loan_Full_ID;
      newItem.Loan_Other_Income_ID = getRandomInt(
        Math.pow(10, 5),
        Math.pow(10, 8)
      );
      newItem.Amount = 0;
      newItem.Crop_Code = '';
      newItem.ActionStatus = 1;
      newItem.Collateral_Ind = 0;
      newItem.Loan_Collateral_ID = 0;

      this.rowData.push(newItem);
      this.gridApi.updateRowData({ add: [newItem] });

      if (!this.localloanobject.LoanOtherIncomes) {
        this.localloanobject.LoanOtherIncomes = [];
      }

      this.localloanobject.LoanOtherIncomes.push(newItem);

      this.gridApi.startEditingCell({
        rowIndex: this.rowData.length,
        colKey: 'Other_Income_ID'
      });

      this.publishService.enableSync(Page.crop);

      this.setValidationErrorForOtherIncome();
      this.getcurrenterrors();
    }
  }

  DeleteClicked(rowIndex: number) {
    this.alertify
      .confirm('Confirm', 'Do you really want to delete this record?')
      .subscribe(res => {
        if (res == true) {
          let obj = this.rowData[rowIndex] as Loan_Other_Income;

          let yIndex = this.localloanobject.LoanOtherIncomes.findIndex(
            f =>
              f.Loan_Other_Income_ID === obj.Loan_Other_Income_ID &&
              f.ActionStatus != 3
          );

          if (obj.ActionStatus) {
            this.localloanobject.LoanOtherIncomes.splice(yIndex, 1);
          } else {
            this.localloanobject.LoanOtherIncomes[yIndex].ActionStatus = 3;
          }

          if(obj.Collateral_Ind == 1) {
            let Collaterals = this.localloanobject.LoanCollateral.filter(a => (a.Collateral_ID == obj.Loan_Collateral_ID || a.Loan_Other_Income_ID == obj.Loan_Other_Income_ID) && a.ActionStatus != 3);
            Collaterals.forEach(Collateral => {
              if(Collateral.ActionStatus == 1) {
                this.localloanobject.LoanCollateral.splice(this.localloanobject.LoanCollateral.indexOf(Collateral), 1);
              } else {
                Collateral.ActionStatus = 3;
              }
            });
          }

          this.setValidationErrorForOtherIncome();
          this.getcurrenterrors();

          this.localloanobject.srccomponentedit = undefined;
          this.localloanobject.lasteditrowindex = undefined;

          this.loanserviceworker.performcalculationonloanobject(
            this.localloanobject
          );

          this.publishService.enableSync(Page.crop);
        }
      });
  }

  setValidationErrorForOtherIncome() {
    if(environment.isDebugModeActive)  console.time('Validation Other Income');

    this.validationService.validateTableFields<Loan_Other_Income>(
      this.rowData,
      this.columnDefs,
      this.currentPageName,
      'OTHR_',
      'OTHR',
      'Loan_Other_Income_ID'
    );

    if(environment.isDebugModeActive) console.timeEnd('Validation Other Income');
  }

  get Loan_Status() {
    if (this.localloanobject) {
      return this.localloanobject.LoanMaster.Loan_Status;
    }

    return '';
  }

  isLoanEditable() {
    return !this.localloanobject || this.Loan_Status === LoanStatus.Working;
  }

  gotoBottom($event: any) {
    window.scrollTo(0, document.body.scrollHeight);
  }
}
