import { Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { finalize } from 'rxjs/operators';
import { JsonConvert } from 'json2typescript';

import { environment } from '@env/environment.prod';

import { YieldModel } from './yield/yield.model';
import { loan_model } from '@lenda/models/loanmodel';
import { RefDataModel } from '@lenda/models/ref-data-model';

import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { ToasterService } from '@lenda/services/toaster.service';
import { GlobalService } from '@lenda/services/global.service';
import { PublishService } from '@lenda/services/publish.service';

@Injectable()
export class CropService {
  refData: RefDataModel;

  constructor(
    public localstorageservice: LocalStorageService,
    public logging: LoggingService,
    public loanserviceworker: LoancalculationWorker,
    public loanapi: LoanApiService,
    public toasterService: ToasterService
  ) {
    this.refData = this.localstorageservice.retrieve(
      environment.referencedatakey
    );

    this.localstorageservice
      .observe(environment.referencedatakey)
      .subscribe(res => {
        this.refData = res;
      });
  }

  syncToDb(localloanobject: loan_model) {
    // Loan Condition Calculation
    localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);
    PublishService.isSyncInProgress = true;
    this.loanapi.syncloanobject(localloanobject).subscribe(res => {
      if (res.ResCode == 1) {
        this.localstorageservice.store(environment.modifiedbase, []);
        this.loanapi
          .getLoanById(localloanobject.Loan_Full_ID)
          .pipe(
            finalize(() =>{
              PublishService.isSyncInProgress = false;
            })
          )
          .subscribe(res1 => {
            this.logging.checkandcreatelog(
              3,
              'Overview',
              'APi LOAN GET with Response ' + res1.ResCode
            );
            if (res1.ResCode == 1) {
              this.toasterService.success('Records Synced');

              let jsonConvert: JsonConvert = new JsonConvert();
              this.loanserviceworker.performcalculationonloanobject(
                jsonConvert.deserialize(res1.Data, loan_model)
              );
              this.loanserviceworker.saveValidationsToLocalStorage(res1.Data);

              this.updateLocalStorage();
            } else {
              this.toasterService.error('Could not fetch Loan Object from API');
            }
          });
      } else {
        this.toasterService.error(res.Message || 'Error in Sync');
      }
    });
  }

  updateLocalStorage() {
    new GlobalService(this.localstorageservice).updateLocalStorage('Yld_');
    new GlobalService(this.localstorageservice).updateLocalStorage('Prc_');
    new GlobalService(this.localstorageservice).updateLocalStorage('MRKT_');
  }

  getValidCrops(crops: any[], loan: loan_model) {
    let rows = [];

    crops.forEach((row, index) => {
      rows.push(this._validate_crop(row, index, loan, rows));
    });

    const deleted_crops = this._deleted_crops(loan, rows);

    const total_crops = [...rows, ...deleted_crops];

    return total_crops;
  }

  private _validate_crop(
    crop: YieldModel,
    index: number,
    loan: loan_model,
    allCrops: any[]
  ) {
    try {
      // validate crop id
      if (crop.Crop_ID) {
        if (loan.CropYield.find(a => a.Crop_ID == crop.Crop_ID)) {
          let key = `${crop.Crop_Code}_${crop.Practice}`;

          if (allCrops.find(c => `${c.Crop_Code}_${c.Practice}` == key)) {
            throw new Error('Duplicate Crop Row');
          }

          crop.ActionStatus = 2;
        } else {
          crop.ActionStatus = 1;
          crop.Crop_ID = 0;
        }
      } else {
        let crop_key = `${crop.Crop_Code}_${crop.Practice}`;

        if (
          loan.CropYield.find(c => `${c.Crop_Code}_${c.Practice}` == crop_key)
        ) {
          crop.ActionStatus = 2;
        } else {
          if (allCrops.find(c => `${c.Crop_Code}_${c.Practice}` == crop_key)) {
            throw new Error('Duplicate Crop Row');
          } else {
            crop.ActionStatus = 1;
            crop.Crop_ID = 0;
          }
        }
      }

      crop.Loan_Full_ID = loan.Loan_Full_ID;
      crop.IrNI = crop.Practice;

      // validate crop
      let isValidCrop = this.refData.CropList.some(
        c => c.Crop_Code == crop.Crop_Code
      );
      if (!isValidCrop) {
        throw new Error('Invalid Crop Code');
      }

      crop.Crop_Name = this.mapCropName(crop.Crop_Code);

      // validate irrigation practice
      let isValidIrrPractice = ['IRR', 'NI'].some(a => a == crop.Practice.toUpperCase());
      if (!isValidIrrPractice) {
        throw new Error('Invalid Irrigation Practice code');
      }

      const crop_year = loan.LoanMaster.Crop_Year;

      // assigning crop year
      crop['CropYear'] = crop_year;

      for (let i = crop_year - 1; i >= crop_year - 7; i--) {
        if (crop[i] == null || crop[i] == undefined) {
          throw new Error('Crop Year ' + i + ' is  missing');
        }
      }

      // preparing crop yield
      this.prepare_crop_yield(crop, crop_year);

      return crop;
    } catch (ex) {
      throw new Error(`Row ${index} has error - ${ex.message}`);
    }
  }

  private _deleted_crops(loan: loan_model, total_crops: YieldModel[]) {
    let deleted_crops = [];

    try {
      loan.CropYield.forEach((y: YieldModel) => {
        let crop = total_crops.find(a => a.Crop_ID == y.Crop_ID);

        if (!crop) {
          y.ActionStatus = 3;

          deleted_crops.push(y);

          loan.LoanCropUnits.filter(
            x =>
              x.Crop_Code == y.Crop_Code &&
              x.Crop_Practice_Type_Code == y.Practice
          ).forEach(x => {
            x.ActionStatus = 3;
          });

          loan.LoanPolicies.filter(
            x =>
              x.Crop_Code == y.Crop_Code &&
              x.Crop_Practice_Type_Code == y.Practice
          ).forEach(x => {
            x.ActionStatus = 3;
          });

          loan.AphUnits.filter(
            x =>
              x.Crop_Code == y.Crop_Code &&
              x.Crop_Practice_Type_Code == y.Practice
          ).forEach(x => {
            x.ActionStatus = 3;
          });

          let Crop_And_Practice_ID = this._get_crop_practice_id(y);

          loan.LoanCropPractices.filter(
            a => a.Crop_Practice_ID == Crop_And_Practice_ID
          ).forEach(x => {
            x.ActionStatus = 3;
          });

          loan.LoanBudget.filter(
            x => x.Crop_Practice_ID == Crop_And_Practice_ID
          ).forEach(x => {
            x.ActionStatus = 3;
          });
        }
      });

      return deleted_crops;
    } catch {
      return [];
    }
  }

  private _get_crop_practice_id(crop: YieldModel) {
    try {
      let cp = this.refData.CropList.find(
        a =>
          a.Crop_Code == crop.Crop_Code && a.Irr_Prac_Code == crop.Practice
      );
      return cp.Crop_And_Practice_ID;
    } catch {
      return '';
    }
  }

  private prepare_crop_yield(crop: any, crop_year: number) {
    let cropyielditems = [];
    let years = [];

    crop.CropYield = crop.APH;

    for (let year = 1; year < 8; year++) {
      years.push(crop_year - year);
    }

    years.forEach(year => {
      if (crop[year] != null && crop[year] != 0) {
        crop[year] = Number(crop[year]);
        cropyielditems.push(crop[year]);
      }
    });

    if (cropyielditems.length <= 2) {
      crop.CropYield = Math.round(crop.APH);
    } else {
      let sum = cropyielditems.reduce((p, n) => {
        return p + n;
      });
      let max = Math.max.apply(null, cropyielditems);
      let min = Math.min.apply(null, cropyielditems);
      let coutie = cropyielditems.length - 2;
      crop.CropYield = Math.round((sum - max - min) / coutie);
    }
  }

  private mapCropName(crop_code: string) {
    if (!this.refData) {
      return '';
    }

    let crop = this.refData.CropList.find(a => a.Crop_Code == crop_code);
    if (crop) {
      return crop.Crop_Name;
    }
    return '';
  }
}
