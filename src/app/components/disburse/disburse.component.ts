import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { loan_model } from '@lenda/models/loanmodel';
import { ISubscription } from 'rxjs/Subscription';
import { Sync_Status } from '@lenda/models/syncstatusmodel';
import { PublishService } from '@lenda/services/publish.service';
import { Page } from '@lenda/models/page.enum';
import { RefChevron } from '@lenda/models/ref-data-model';
import { calculatecolumnwidths, cellclassmakerdisburse, CellType, setgriddefaults, toggletoolpanel, isgrideditabledisburse } from '@lenda/aggriddefinations/aggridoptions';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { DataService } from '@lenda/services/data.service';
import { getNumericCellEditor } from '@lenda/Workers/utility/aggrid/numericboxes';
import { getAlphaNumericCellEditor } from '@lenda/Workers/utility/aggrid/alphanumericboxes';
import { getDateCellEditor } from '@lenda/Workers/utility/aggrid/dateboxes';
import { environment } from '@env/environment';
import { percentageFormatter, currencyFormatter } from '@lenda/aggridformatters/valueformatters';
import { createviewfordisburse } from './disburse.rows.static';
import { BudgetHelperService } from '../budget/budget-helper.service';
import { SettingButtonRenderer } from '@lenda/aggridcolumns/settingsbuttonrender';
import { MatDialog } from '@angular/material';
import { Loan_Disburse, Loan_Disburse_Detail, Loan_Disburse_Approval_Hist } from '@lenda/models/disbursemodels';
import { DisburseapiService } from '@lenda/services/disburse/disburseapi.service';
import * as _ from 'lodash';
import { DisbursepopupComponent } from '@lenda/aggridcolumns/disbursepopup/disbursepopup.component';
import { DisbursecommentComponent } from '@lenda/aggridcolumns/disbursecomment/disbursecomment.component';
import { DisburseConditions } from './disburse.condtions';
import { errormodel } from '@lenda/models/commonmodels';
import { Helper } from '@lenda/services/math.helper';
import { MasterService } from '@lenda/master/master.service';
import { Loan_Exception } from '@lenda/models/loan/loan_exceptions';

@Component({
  selector: 'app-disburse',
  templateUrl: './disburse.component.html',
  styleUrls: ['./disburse.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DisburseComponent implements OnInit {

  public refdata: any = {};
  public userdata: any = {};
  public columnDefs = [];
  private subscription: ISubscription;
  private loanobject: loan_model;
  // Aggrid
  public rowData = [];
  public isAdding: boolean = false;
  public currenteditedfield: string = null;
  public currenteditrowindex: number = -1;
  public components;
  public context;
  public frameworkcomponents;
  public editType;
  private gridApi;
  private columnApi;
  private disbursements: Array<Loan_Disburse>;
  public syncFarmStatus: Sync_Status;
  public uploadtext: string;
  public adddisabled: boolean = false;
  private syncSub: ISubscription;
  currenterrors: Array<errormodel>;
  public harvestamount = 0;
  //region Ag grid Configuration

  //TO DO:
  //Declared ONLY to build file
  tableId: any;

  currentPageName: Page = Page.disburse;
  farmChevron: RefChevron;

  style = {
    marginTop: '-10px',
    width: '100% !important',
    boxSizing: 'border-box',
    height: '500px'
  };

  defaultColDef = {
    enableValue: true,
    sortable: true,
    resizable: true,
  };
  public getRowHeight;
// Component Variables END HERE
  onGridReady(params) {

    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    //these two methods remove side option toolbar , sets column , removes header menu
    setgriddefaults(this.gridApi, this.columnApi);
    toggletoolpanel(false, this.gridApi);
    //save settings here
    this.getdataforgrid();
    this.style.height = this.rowData.length * 25 + 12 + "px";
    // params.api.sizeColumnsToFit();
    this.declareColumns();
    this.getuploadedloaninfo();
    setTimeout(() => {
      this.getInitialExceptions();
    }, 2000);

    //this.adjustToFitAgGrid();


  }


  public getuploadedloaninfo() {

    let date = this.loanobject.LoanMaster.Nortridge_UploadDate;
    if (date == null) {
      this.adddisabled = true;
      this.uploadtext = "Loan not yet Approved and Uploaded";
    } else {
      this.adddisabled = false;
    }

  }
  private getInitialExceptions() {
    let _currenterrors = this.localstorageservice.retrieve(environment.errorbase);

    _.remove(_currenterrors, function (param: any) {
      return param.chevron == `Disburse`;
    });

    this.disbursements.forEach(element => {
      this.generateexceptions(element, _currenterrors);
      this.disburseInspector.checkRentStatus(this.loanobject);
    });
  }

  adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  displayedColumnsChanged() {
    this.adjustToFitAgGrid();
  }

  public gridOptions = {
    getRowNodeId: data => {
      let _rowid = `DIS_${data.ExpenseID}`;
      return _rowid;
    }
  };



  //End here
  // Aggrid ends
  constructor(
    public localstorageservice: LocalStorageService,
    public sessionstorageservice: SessionStorageService,
    public loanserviceworker: LoancalculationWorker,
    public logging: LoggingService,
    public alertify: AlertifyService,
    public loanapi: LoanApiService,
    public budgetService: BudgetHelperService,
    private publishService: PublishService,
    public disburseInspector: DisburseConditions,
    public validationservice: ValidationService,
    private dataService: DataService,
    private apiService: DisburseapiService,
    public dialog: MatDialog,
    public masterSvc: MasterService
  ) {

    this.frameworkcomponents = {
      settingsrender: SettingButtonRenderer
    };
    this.getRowHeight = function (params) {
      return params.data.rowHeight;
    };
    this.components = {
      numericCellEditor: getNumericCellEditor(),
      alphaNumericCellEditor: getAlphaNumericCellEditor(),
      dateCellEditor: getDateCellEditor()
    };
    this.loanobject = this.localstorageservice.retrieve(environment.loankey);
    this.syncSub = this.publishService.syncCompletedevent.subscribe(p => {
      this.loanobject = p;
      this.getdataforgrid();
      this.getuploadedloaninfo();
      this.style.height = this.rowData.length * 25 + 12 + "px";
      // params.api.sizeColumnsToFit();
      this.declareColumns();

      setTimeout(() => {
        this.getInitialExceptions();
      }, 1000);
    });
    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
    this.userdata = this.sessionstorageservice.retrieve("UID");
    this.context = { componentParent: this };
  }

  private declareColumns() {

    this.columnDefs = [
      {
        headerName: 'Values',
        minWidth: 120, width: 120, maxWidth: 120,
        field: 'Values',
        tooltip: params => params.data['Values'],
        cellClass: function (params) {
          if (params.data.isheader) {
            // return 'headerclass'
            return 'budgetcell';
          } else if (params.data.isActionBar) {
            return "budgetcell";
          } else if (params.data.isTotal == true) {
            return cellclassmakerdisburse(CellType.Text, false, 'totalclass');
          } else {
            return cellclassmakerdisburse(CellType.Text, false);
          }
        },
        editable: false
      },
      {
        headerName: 'Commit',
        minWidth: 120, width: 120, maxWidth: 120,
        field: 'Commit',
        cellClass: function (params) {
          if (params.data.isheader) {
            // return 'headerclass'
            return 'budgetcell';
          } else if (params.data.isActionBar) {
            return "budgetcell";
          } else if (params.data.isTotal == true) {
            return cellclassmakerdisburse(CellType.Integer, false, 'totalclass');
          } else {
            return cellclassmakerdisburse(CellType.Integer, false);
          }
        },
        editable: false,
        valueFormatter: function (params) {
          if (params.value == "NA") {
            return '';
          }

          if (params.data.isheader) {

            return params.value;
          } else {
            return currencyFormatter(params);
          }
        },
      },
      {
        headerName: 'Used',
        minWidth: 120, width: 120, maxWidth: 120,
        field: 'Used',
        cellClass: function (params) {
          if (params.data.isheader) {
            // return 'headerclass'
            return 'budgetcell';
          } else if (params.data.isActionBar) {
            return "budgetcell";
          } else if (params.data.isTotal == true) {
            return cellclassmakerdisburse(CellType.Integer, false, 'totalclass');
          } else {
            return cellclassmakerdisburse(CellType.Integer, false);
          }
        },
        editable: false,
        valueFormatter: function (params) {
          if (params.value == "NA") {
            return '';
          }

          if (params.data.isheader) {

            return params.value;
          } else {
            return currencyFormatter(params);
          }
        },
      },
      {
        headerName: 'Percentage',
        minWidth: 120, width: 120, maxWidth: 120,
        field: 'Percentage',
        cellClass: function (params) {
          if (params.data.isheader) {
            // return 'headerclass'
            return 'budgetcell';
          } else if (params.data.isActionBar) {
            return "budgetcell";
          } else if (params.data.isTotal == true) {
            return cellclassmakerdisburse(CellType.Integer, false, 'totalclass');
          } else {
            return cellclassmakerdisburse(CellType.Integer, false);
          }
        },
        editable: false,
        valueFormatter: function (params) {
          if (params.value == "NA") {
            return '';
          }

          if (params.data.isheader) {

            return params.value;
          } else {
            return percentageFormatter(params);
          }
        },
      },
      {
        headerName: 'Available',
        minWidth: 120, width: 120, maxWidth: 120,
        field: 'Available',
        cellClass: function (params) {
          if (params.data.isheader) {
            // return 'headerclass'
            return 'budgetcell';
          } else if (params.data.isActionBar) {
            return "budgetcell";
          } else if (params.data.isTotal == true) {
            return cellclassmakerdisburse(CellType.Integer, false, 'totalclass');
          } else {
            return cellclassmakerdisburse(CellType.Integer, false);
          }
        },
        editable: false,
        valueFormatter: function (params) {

          if (params.value == "NA") {
            return '';
          }
          if (params.data.isheader) {

            return params.value;
          } else {
            return currencyFormatter(params);
          }
        },
      }
    ];

    //here add the columns according to disbursements
    //

    this.disbursements.forEach(element => {
      //add cancelled and done here
      this.columnDefs.push(
        {
          headerName: 'disburse_' + element.Loan_Disburse_ID,
          minWidth: 140, width: 150, maxWidth: 150,
          field: 'disburse_' + element.Loan_Disburse_ID,
          cellClass: function (params) {
            if (params.data.isTotal == true) {
              if (params.context.componentParent.loanobject.Loan_Exceptions.find(p => p.Exception_ID == 154 && p.ActionStatus != 3) != null) {
                let classes = cellclassmakerdisburse(CellType.Integer, false, 'validation2disburse');
                return classes;
              } else {
                return cellclassmakerdisburse(CellType.Integer, false, 'totalclass');
              }
            }
            if (params.data.isbudget == true) {
              //first check for readonly
              if (params.data[params.colDef.field + "_Status"] > 0) {
                return cellclassmakerdisburse(CellType.Integer, false, 'grayedcell');
              }
              if (params.data.isbudget && (params.data.Values == "Cash Rent" || params.data.Values == "Harvest")) {
                return cellclassmakerdisburse(CellType.Integer, false, 'grayedcell');
              } else if (params.data.isTotal == true) {
                return cellclassmakerdisburse(CellType.Integer, false, 'totalclass');
              } else if (params.data.isbudget != true && !params.data.isheader) {
                return cellclassmakerdisburse(CellType.Integer, false);
              } else if ((params.data.isbudget == true && !params.data.isheader && params.colDef.field.replace('disburse_', '').includes('e')) || (params.data.Status == 0)) {
                return cellclassmakerdisburse(CellType.Integer, true);
              } else if (params.data.isbudget == true && !params.data.isheader) {
                return cellclassmakerdisburse(CellType.Integer, false);
              } else {
                return "";
              }
            } else {
              if (params.data.isheader) {
                // return 'headerclass'
                return 'approvedisburse';
              } else if (params.data.isActionBar) {
                return "approvedisburse";
              }
            }
          },
          cellRendererSelector: function (params) {
            if (params.data.editordisburse == "Special") {
              return {
                component: 'settingsrender'
              };
            } else {

              return null;
            }
          },
          cellRendererParams: {
            user: this.userdata
          },
          tooltip: function (params) {
            if (params.data.isbudget && (params.data.Values == "Cash Rent" || params.data.Values == "Harvest")) {
              return "Please Fill Values from Pencil Icon from Top";
            }
          },
          editable: function (params) {
            if (params.data[params.colDef.field + "_Status"] > 0) {
              return isgrideditabledisburse(false);
            }

            if (params.data.isbudget == true) {

              if (params.data.isbudget && (params.data.Values == "Cash Rent" || params.data.Values == "Harvest")) {
                return isgrideditabledisburse(false);
              }
              if (params.data.isbudget && params.column.colId.replace('disburse_', '').includes('e')) {

                return isgrideditabledisburse(true);
              } else {
                if (params.data.Status == 0) {
                  return isgrideditabledisburse(true);
                }
                return isgrideditabledisburse(false);
              }
            }
          },
          valueFormatter: function (params) {
            if (params.data.isbudget == true && !params.data.isheader) {
              return currencyFormatter(params);
            }
            if (params.data.isTotal) {
              return currencyFormatter(params);
            }
          }

        });
    });
  }

  getCurrentErrors() {
    this.currenterrors = (this.localstorageservice.retrieve(
      environment.errorbase
    ) as Array<errormodel>).filter(p => p.chevron == 'Disburse');

    this.validationservice.highlighErrorCells(this.currenterrors, 'disburse_table');
  }

  setCurrentErrors() {
    setTimeout(() => {
      this.getCurrentErrors();
    }, 500);
  }

  isAddEnabled() {
    try {
      if (this.disbursements.find(p => (p.Status == 0 || p.Status == undefined)) != undefined) {
        return true;
      } else {
        return false;
      }
    } catch {
      return true;
    }
  }

  adddisbursement() {
    //Check if we have existing Reimburses
    if (this.disbursements.find(p => (p.Status == 0 || p.Status == undefined)) != undefined) {
      this.alertify.alert("Action Prevented", "Only one active request may exist at a time; you may either wait for the existing Request to be processed or cancel the existing Request and enter a new one");
      return;
    }

    let _neededID = 1;
    let _iteminedit = this.disbursements.filter(p => p.Loan_Disburse_ID.toString().includes('e')).sort(p => p.Loan_Disburse_ID);
    if (_iteminedit.length != 0) {
      _neededID = Number(_iteminedit[_iteminedit.length - 1].Loan_Disburse_ID.toString().replace('disburse_e', '')) + 1;
    }
    let _newrecord = new Loan_Disburse();
    _newrecord.ActionStatus = 1;
    // tslint:disable-next-line: radix
    _newrecord.Loan_ID = parseInt(this.loanobject.Loan_Full_ID.split('-')[0]);
    _newrecord.Loan_Disburse_ID = 'e' + _neededID;
    let that = this;
    this.apiService.SaveUpdateDisbursement(_newrecord).subscribe(() => {
      that.GetDisbursementfromAPI();
    });
  }

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      //that.getdataforgrid();
      this.loanobject = res;
    });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.syncSub) {
      this.syncSub.unsubscribe();
    }
  }

  //Popup method
  invokesettingsMethod(id) {

    let _disburse = id.toString().replace('disburse_', '');
    let _founddisbursement = this.disbursements.find(p => p.Loan_Disburse_ID.toString() == _disburse.toString());
    const dialogRef = this.dialog.open(DisbursepopupComponent, {
      width: '800px',
      data: { disbursement: _founddisbursement, rowdata: this.rowData }

    });
    dialogRef.afterClosed().take(1).subscribe(_result => {
      if (_result != null && _result.data != false) {
        if (_result.ActionStatus != 1) {
          _result.ActionStatus = 2;
        }

        this.disbursements[this.disbursements.indexOf(_founddisbursement)] = _result.data;
        // tslint:disable-next-line:no-shadowed-variable
        this.disbursements[this.disbursements.indexOf(_founddisbursement)].Total_Disburse_Amount = _.sum(this.disbursements[this.disbursements.indexOf(_founddisbursement)].Details.map(p => p.Disburse_Detail_Requested_Amount));
        this.loanobject.Disbursements = this.disbursements;
        this.getdataforgrid();
        this.setbudgetUsed();
        this.setFarmbudgets();
        this.dataService.setLoanObjectwithoutsubscription(this.loanobject);
        this.generateexceptions(_founddisbursement);
        this.disburseInspector.checkRentStatus(this.loanobject);
        this.publishService.enableSync(Page.disburse);
      } else {
        if (_result != null && _result.data == false) {
          _founddisbursement.FC_PopupStatus = _result.code;
        } else {
          _founddisbursement.FC_PopupStatus = 3;
        }
      }
    });
  }

    // Dummy method for testing DOnt delete
  invokesettingsMethodtest(id) {
    let _disburse = id.toString().replace('disburse_', '');
    let _founddisbursement = this.disbursements.find(p => p.Loan_Disburse_ID.toString() == _disburse.toString());
    const dialogRef = this.dialog.open(DisbursepopupComponent, {
      width: '0px',
      height: '0px',
      data: { disbursement: _founddisbursement, rowdata: this.rowData }

    });
    setTimeout(() => {
      dialogRef.componentInstance.onNoClick();
    }, 200);

    dialogRef.afterClosed().take(1).subscribe(_result => {

      if (_result != null && _result.data != false) {
        if (_result.ActionStatus != 1) {
          _result.ActionStatus = 2;
        }

        this.disbursements[this.disbursements.indexOf(_founddisbursement)] = _result.data;
        // tslint:disable-next-line:no-shadowed-variable
        this.disbursements[this.disbursements.indexOf(_founddisbursement)].Total_Disburse_Amount = _.sum(this.disbursements[this.disbursements.indexOf(_founddisbursement)].Details.map(p => p.Disburse_Detail_Requested_Amount));
        this.loanobject.Disbursements = this.disbursements;
        this.getdataforgrid();
        this.setbudgetUsed();
        this.setFarmbudgets();
        this.dataService.setLoanObjectwithoutsubscription(this.loanobject);
        this.generateexceptions(_founddisbursement);
        this.disburseInspector.checkRentStatus(this.loanobject);
        this.publishService.enableSync(Page.disburse);
      } else {
        if (_result != null && _result.data == false) {
          _founddisbursement.FC_PopupStatus = _result.code;
        } else {
          _founddisbursement.FC_PopupStatus = 3;
        }
      }
    });
  }
  private generateexceptions(disbursement: Loan_Disburse, currenterrors = null) {
    if (currenterrors == null) {
      currenterrors = this.localstorageservice.retrieve(
        environment.errorbase
      );

      _.remove(currenterrors, function (param: any) {
        return param.chevron == `Disburse`;
      });
    }

    this.loanobject = this.disburseInspector.checkTotalArmbudgetStatus(this.rowData, this.loanobject, this.disbursements[this.disbursements.indexOf(disbursement)]);
    this.disbursements[this.disbursements.indexOf(disbursement)].Details.forEach((element) => {
      let name = this.loanobject.LoanBudget.find(p => p.Expense_Type_ID == element.Budget_Expense_ID).Expense_Type_Name;
      this.loanobject = this.disburseInspector.checkTotalbudgetStatus({
        column: {
          colId: this.columnDefs[this.columnDefs.length - 1].field
        },
        data: {
          Values: name,
          ExpenseID: element.Budget_Expense_ID,
          Loan_Disburse_ID: disbursement.Loan_Disburse_ID,
          Available: (this.rowData.find(p => p.ExpenseID == element.Budget_Expense_ID).Available)
        },
        value: element.Disburse_Detail_Requested_Amount
      }, currenterrors, this.loanobject, this.disbursements[this.disbursements.indexOf(disbursement)]);
    });
    this.localstorageservice.store(environment.errorbase, _.uniq(currenterrors)
    );
    this.setCurrentErrors();
  }

  setFarmbudgets() {
    this.loanobject.Disbursements.forEach(_iteration => {
      try {
        _iteration.RentDetails.forEach(_rentdetail => {
          let oldcashrent = Number(this.loanobject.Farms.find(p => p.Farm_ID == Number(_rentdetail.Farm_ID)).Cash_Rent_Used);
          this.loanobject.Farms.find(p => p.Farm_ID == Number(_rentdetail.Farm_ID)).Cash_Rent_Used = oldcashrent + Number(_rentdetail.Cash_Rent_Amount);
        });
      } catch {

      }
    });
  }

  invokedeleteMethod(id: any) {
    this.alertify.deleteRecord().subscribe(_response => {
      if (_response == true) {
        if (id.toString().
          includes('e')) {
          let _index = this.disbursements.indexOf(this.disbursements.find(p => p.Loan_Disburse_ID == id));
          this.disbursements.splice(_index, 1);
          this.loanobject.Disbursements = this.disbursements;
          this.loanserviceworker.performcalculationonloanobject(this.loanobject, false);
          this.columnDefs = [];
          this.getdataforgrid();
          this.declareColumns();
          this.getInitialExceptions();
        } else {
          //make Api hit
          let that = this;
          this.apiService.DeleteDisubrsement(id).subscribe(() => {
            that.GetDisbursementfromAPI();
            this.disburseInspector.checkRentStatus(this.loanobject);
          });
        }
      }
    });
  }

  invokesubmitMethod(id: any) {

    let _disburse = id.replace('disburse_', '');
    let disbursement = this.disbursements.find(p => p.Loan_Disburse_ID.toString() == _disburse.toString());
    if (disbursement) {
      this.submitdisbursement(disbursement);
    } else {
      this.alertify.alert("Error", "Please Input some values to Start");
    }
  }

  invokedetailsMethod() {
  }

  invokeapproveMethod(disurseitem, status) {
    let that = this;
    this.publishService.syncCompletedevent.subscribe((_responseloanmodel: loan_model) => {
      if (this.checkifloanhaserrors(_responseloanmodel.Loan_Exceptions)) {
        this.alertify.alert('Warning', "Disburse cannot be processed with Validation error");
      } else {
        //it adds it
        let _statusitem = new Loan_Disburse_Approval_Hist();
        _statusitem.Approval_Date_Time = new Date().toLocaleString();
        _statusitem.Approval_Status = status;
        _statusitem.Loan_Disburse_ID = disurseitem;
        // tslint:disable-next-line: radix
        _statusitem.Loan_ID = parseInt(this.loanobject.Loan_Full_ID.split('-')[0], 10);
        _statusitem.User_ID = this.userdata.UserID;
        _statusitem.User_Role = this.getinvokerRole(status);
        _statusitem.Action_Code = 1;
        that.apiService.AddnewDisubrsementActionSingle(_statusitem).subscribe(() => {
          that.GetDisbursementfromAPI();
        });

      }
      this.publishService.syncCompletedevent.unsubscribe();
    });

    this.publishService.syncToDb();

    //statusitem.Comments = result;

    //approve current one

  }

  invokerequestMethod(disburseitem: Loan_Disburse) {

    let _statusitem = new Loan_Disburse_Approval_Hist();
    _statusitem.Approval_Date_Time = new Date().toLocaleString();
    _statusitem.Approval_Status = 300;
    _statusitem.Loan_Disburse_ID = disburseitem.Loan_Disburse_ID;
    // tslint:disable-next-line: radix
    _statusitem.Loan_ID = parseInt(this.loanobject.Loan_Full_ID.split('-')[0]);
    _statusitem.User_ID = this.userdata.UserID;
    _statusitem.User_Role = this.getinvokerRole(100);
    _statusitem.Action_Code = 1;
    disburseitem.History = [];
    disburseitem.History.push(_statusitem);
    //statusitem.Comments = result;
    let that = this;
    this.publishService.syncCompletedevent.subscribe(() => {
      that.apiService.AddnewDisubrsementAction(disburseitem).subscribe(() => {
        that.GetDisbursementfromAPI();

      });
      //this.publishService.syncCompletedevent.unsubscribe();
    });

    this.publishService.syncToDb();




  }


  checkifloanhaserrors(exceptions: Array<Loan_Exception>) {
    return exceptions.filter(p => p.Complete_Ind != 1).length > 0;
    //return false;
  }
  getinvokerRole(status: number) {
    switch (status) {
      case 100:
        return 'Borrower';
      case 200:
        return 'Loan Officer';
      case 300:
        return 'Risk Admin';
      case 400:
        return 'Processor';
      default:
        break;
    }
  }

  invokedeclineMethod(disurseitem) {
    const dialogRef = this.dialog.open(DisbursecommentComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(_result => {
      if (_result != undefined) {
        let _statusitem = new Loan_Disburse_Approval_Hist();
        _statusitem.Approval_Date_Time = new Date().toLocaleString();
        _statusitem.Approval_Status = 1000;
        _statusitem.Loan_Disburse_ID = disurseitem;
        // tslint:disable-next-line: radix
        _statusitem.Loan_ID = parseInt(this.loanobject.Loan_Full_ID.split('-')[0]);
        _statusitem.User_ID = this.userdata.UserID;
        _statusitem.Action_Code = 0;
        _statusitem.Comments = _result;
        let that = this;
        this.apiService.AddnewDisubrsementActionSingle(_statusitem).subscribe(() => {
          that.GetDisbursementfromAPI();
        });
      }
    });
  }

  getdataforgrid() {
    this.rowData = [];
    this.disbursements = this.loanobject.Disbursements;
    if (environment.isDebugModeActive) { console.time('Disburse Grid - Initial Load'); }
    // this.logging.checkandcreatelog(1, 'LoanFarms', "LocalStorage retrieved");
    if (this.loanobject != null && this.loanobject != undefined) {
      //STATIC ROWS FIRST
      this.rowData = createviewfordisburse(this.disbursements);

      this.rowData = this.rowData.concat(this.budgetdata(this.loanobject));
      this.createtotalRow();
      //now get the total budget
    }

    if (environment.isDebugModeActive) { console.timeEnd('Farm Grid - Initial Load'); }
  }

  createtotalRow() {

    let _totalrow = this.rowData.find(p => p.isTotal == true);
    if (_totalrow != undefined) {
      _.remove(this.rowData, p => p.isTotal == true);
    }
    let _rowitem: any = {};
    let items = this.rowData.filter(p => p.isbudget == true);
    _rowitem.Values = "Total";
    _rowitem.Commit = _.sumBy(items, 'Commit');
    _rowitem.Used = _.sumBy(items, 'Used');
    _rowitem.isTotal = true;
    _rowitem.rowHeight = 25;
    _rowitem.Percentage = Helper.percent(_rowitem.Used, _rowitem.Commit);
    _rowitem.Available = _.sumBy(items, 'Available');
    _rowitem.editordisburse = _.sum(items.map(p => Number(p.editordisburse == undefined ? 0 : p.editordisburse)));
    this.disbursements.forEach(element => {
      _rowitem["disburse_" + element.Loan_Disburse_ID] = _.sum(items.map(p => Number(p["disburse_" + element.Loan_Disburse_ID] == undefined ? 0 : p["disburse_" + element.Loan_Disburse_ID])));
    });
    this.rowData.push(_rowitem);
    this.gridApi.setRowData(this.rowData);
  }

  budgetdata(loanObject: loan_model) {
    let _temprows = [];
    if (loanObject.LoanCropPractices) {
      let _founditems = _.sortBy((loanObject.LoanBudget.filter(p => p.Crop_Practice_ID == 0 && p.Budget_Type.trim() == 'V' && p.ActionStatus != 3)), b => b.Sort_order || b.Loan_Budget_ID);
      for (let i = 0; i < _founditems.length; i++) {
        let _rowitem: any = {};
        _rowitem.Values = _founditems[i].Expense_Type_Name;
        _rowitem.Commit = _founditems[i].ARM_Budget_Crop;
        _rowitem.Used = this.getusedbyexpensetype(_founditems[i].Expense_Type_ID);
        _rowitem.ExpenseID = _founditems[i].Expense_Type_ID;
        _rowitem.isbudget = true;
        _rowitem.rowHeight = 25;
        _rowitem.Percentage = (_rowitem.Used / (_rowitem.Commit == 0 ? 1 : _rowitem.Commit) * 100);
        _rowitem.Available = _rowitem.Commit - _rowitem.Used;
        _rowitem.editordisburse = 0;
        this.disbursements.forEach(element => {
          let _relatedexpense = element.Details.find(p => p.Budget_Expense_ID == _rowitem.ExpenseID);
          _rowitem["disburse_" + element.Loan_Disburse_ID] = _relatedexpense == undefined ? 0 : _relatedexpense.Disburse_Detail_Requested_Amount;
          _rowitem["disburse_" + element.Loan_Disburse_ID + "_Status"] = (element.History == null || element.History.length == 0) ? 0 : element.History[element.History.length - 1].Approval_Status;
          _rowitem.Status = element.Status;
        });
        _temprows.push(_rowitem);
      }
      return _temprows;
    }
  }


  getusedbyexpensetype(id: number) {
    //let everything come in not looking for Status
    let _sum = 0;
    this.disbursements.filter(p => p.Status == 1).forEach(element => {
      let expesne = element.Details.find(p => p.Budget_Expense_ID == id);
      _sum = _sum + (expesne == undefined ? 0 : expesne.Disburse_Detail_Requested_Amount);
    });
    return _sum;
  }

  rowvaluechanged(event: any) {

    let _disburseitem = event.column.colId.replace('disburse_', '');
    let _foundrecord = this.disbursements.find(p => p.Loan_Disburse_ID.toString() == _disburseitem.toString());
    //If Older Record
    if (_foundrecord != undefined) {
      let _relatedbudget = _foundrecord.Details.find(p => p.Budget_Expense_ID == event.data.ExpenseID);
      if (_relatedbudget) {
        _relatedbudget.Disburse_Detail_Requested_Amount = Number(event.value);
        if (_relatedbudget.ActionStatus != 1) {
          _relatedbudget.ActionStatus = 2;
        }
      } else {
        let _tempnewobj = new Loan_Disburse_Detail();
        _tempnewobj.Budget_Expense_ID = event.data.ExpenseID;
        _tempnewobj.Disburse_Detail_Requested_Amount = Number(event.value);
        if (_foundrecord.Details == null) {
          _foundrecord.Details = new Array<Loan_Disburse_Detail>();
        }
        _tempnewobj.Status = 0;
        _tempnewobj.ActionStatus = 1;
        _foundrecord.Details.push(_tempnewobj);
      }
      _foundrecord.Total_Disburse_Amount = _.sum(_foundrecord.Details.map(p => p.Disburse_Detail_Requested_Amount));
      if (_foundrecord.ActionStatus != 1) {
        _foundrecord.ActionStatus = 2;
      }
      let index = this.disbursements.indexOf(_foundrecord);
      this.disbursements[index] = _foundrecord;
    } else {
      let newrecord = new Loan_Disburse();
      newrecord.ActionStatus = 1;
      // newrecord.Request_Status_Ind=0;
      // newrecord.Request_User_ID=1;
      newrecord.Loan_Disburse_ID = _disburseitem;
      let _tempnewobj = new Loan_Disburse_Detail();
      _tempnewobj.Budget_Expense_ID = event.data.ExpenseID;
      _tempnewobj.Disburse_Detail_Requested_Amount = Number(event.value);
      newrecord.Details = new Array<Loan_Disburse_Detail>();
      _tempnewobj.Status = 0;
      newrecord.Details.push(_tempnewobj);
      this.loanobject.Disbursements.push(newrecord);
      newrecord.Total_Disburse_Amount = _.sum(newrecord.Details.map(p => p.Disburse_Detail_Requested_Amount));
      _foundrecord = newrecord;
    }
    //Raising at End so that all things are Confirmed and then we do it
    //this.setbudgetUsed();
    this.disburseInspector.checkRentStatus(this.loanobject);
    this.loanserviceworker.performcalculationonloanobject(this.loanobject, false);
    this.dataService.setLoanObjectwithoutsubscription(this.loanobject);
    this.getdataforgrid();
    this.createtotalRow();
    this.generateexceptions(_foundrecord);
    this.publishService.enableSync(Page.disburse);
    this.setCurrentErrors();
  }


  //here we Set budgets for the used ARM
  setbudgetUsed() {
    this.rowData.forEach(_record => {

      if (_record.isbudget) {
        try {
          this.loanobject.LoanBudget.find(p => p.Expense_Type_ID == _record.ExpenseID).ARM_Used = _record.Used;
        } catch {
          //Just in case somehthing Happens
        }
      }
    });
  }

  /**
   * Sync to database - publish button event
   */
  synctoDb() {
    this.gridApi.stopEditing();
    this.publishService.syncCompleted();
  }

  //submit to disbursement
  submitdisbursement(_item: Loan_Disburse) {

    if (_item.Total_Disburse_Amount > 0) {

      // tslint:disable-next-line: radix
      _item.Loan_ID = parseInt(this.loanobject.Loan_Full_ID.split('-')[0]);
      let requestitem = new Loan_Disburse_Approval_Hist();
      requestitem.Approval_Status = 300;
      _item.Status = 0;
      // tslint:disable-next-line:radix
      requestitem.Loan_ID = parseInt(this.loanobject.Loan_Full_ID.split('-')[0]);
      requestitem.User_ID = this.userdata.UserID;
      requestitem.User_Role = this.getinvokerRole(100);
      requestitem.Approval_Date_Time = new Date().toLocaleString();
      requestitem.Action_Code = 1;
      _item.History = new Array<Loan_Disburse_Approval_Hist>();
      _item.History.push(requestitem);
      _item.ActionStatus = 2;
      let that = this;
      //Save to API
      this.apiService.SaveUpdateDisbursement(_item).subscribe(() => {
        that.GetDisbursementfromAPI();

      });

    }
  }

  GetDisbursementfromAPI() {
    // tslint:disable-next-line:radix
    this.apiService.GetDisbursement(parseInt(this.loanobject.Loan_Full_ID.split('-')[0])).subscribe(res => {
      this.disbursements = res.Data;
      this.loanobject.Disbursements = res.Data;
      this.loanserviceworker.performcalculationonloanobject(this.loanobject, false);
      this.columnDefs = [];
      this.getdataforgrid();
      this.declareColumns();
      this.getInitialExceptions();
    });
  }
}
