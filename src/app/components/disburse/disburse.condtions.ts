import * as _ from 'lodash';
import { ExceptionWorkerService } from '@lenda/Workers/calculations/exceptionworker.service';
import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment';
import { errormodel } from '@lenda/models/commonmodels';
import { Page } from '@lenda/models/page.enum';
import { loan_model } from '@lenda/models/loanmodel';
import { formatcurrency } from '@lenda/aggridformatters/valueformatters';
import { DataService } from '@lenda/services/data.service';
import { ValidationErrorsCountService } from '@lenda/Workers/calculations/validationerrorscount.service';
@Injectable()
export class DisburseConditions {

  constructor(
    public exceptionService: ExceptionWorkerService,
    public localStorageService: LocalStorageService,
    public dataservice: DataService,
    private validationCount: ValidationErrorsCountService
  ) { }

  //This method Checks if the Total budegt Has Been Exceeded
  checkTotalArmbudgetStatus(rowData, loanmodel: loan_model, disburse) {

    let items = rowData.filter(p => p.isbudget == true);
    let Available = Number(_.sumBy(items, 'Available'));
    let requested = Number(_.sumBy(items, p => Number(p['disburse_' + disburse.Loan_Disburse_ID])));
    if (Available < requested) {
      disburse.Amount = formatcurrency(parseFloat(Math.abs(requested - (Available + disburse.Total_Disburse_Amount)).toFixed(2)));
      //Raise Validation here also Exception 154
      this.exceptionService.Process_Exception_By_ID(loanmodel, 154, disburse, 1);
    } else {
      this.exceptionService.Process_Exception_By_ID(loanmodel, 154, disburse, null);
    }
    return loanmodel;
    //this.highlighErrorCellsfortotal(loanmodel);
  }

  //This method Checks if the current individual budegt Has Been Exceeded
  checkTotalbudgetStatus(event, currenterrors = [], loanmodel: loan_model, disburse) {

    let exceededamount = formatcurrency(parseFloat((event.value - event.data.Available).toString()).toFixed(2));
    if (event.data.Values != "Cash Rent" && event.data.Values != "Harvest" && event.data.Available - Number(event.value) < 0) {
      this.exceptionService.Process_Exception_By_ID(loanmodel, 160, { Amount: exceededamount, Expense_Name: event.data.Values, Expense_ID: event.data.ExpenseID, Disburse_ID: disburse.Loan_Disburse_ID  }, 1);

      let text = 'Disburse ' + event.data.Values + ' Request of ' + formatcurrency(event.value);
      const cssclasses = ['cell-validation', 'exception-1'];
      //Raise validation here

      let validation = this.getErrorModel(event, cssclasses, text, 24);
      if (!currenterrors.find(p => p.quetionId == (event.column.colId + '-' + event.data.ExpenseID) as any)) {
        currenterrors.push(validation);
      }
    } else {
      this.exceptionService.Process_Exception_By_ID(loanmodel, 160, { Amount: exceededamount , Expense_Name: event.data.Values, Expense_ID: event.data.ExpenseID, Disburse_ID: disburse.Loan_Disburse_ID }, null);
    }

    //CASH RENT SECTION
    if (event.data.Values == "Cash Rent") {
      if (event.value != 0 && event.value != null) {
        this.exceptionService.Process_Exception_By_ID(loanmodel, 156, { Amount: formatcurrency(event.value) }, 1);

        let text = 'Disburse Rent Request of ' + formatcurrency(event.value);
        const cssclasses = ['cell-validation', 'exception-1'];

        let validation = this.getErrorModel(event, cssclasses, text, 25);
        if (!currenterrors.find(p => p.quetionId == (event.column.colId + '-' + event.data.ExpenseID) as any)) {
          currenterrors.push(validation);
        }
      } else {
        this.exceptionService.Process_Exception_By_ID(loanmodel, 156, { Amount: formatcurrency(event.value) }, null);
      }
    }

    if (event.data.Values == "Harvest") {
      if (event.value != 0 && event.value != null) {
        this.exceptionService.Process_Exception_By_ID(loanmodel, 155, { Amount: formatcurrency(event.value) }, 1);


        let text = 'Disburse Harvest Request of ' + formatcurrency(event.value);
        const cssclasses = ['cell-validation', 'exception-1'];

        let validation = this.getErrorModel(event, cssclasses, text, 26);
        if (!currenterrors.find(p => p.quetionId == (event.column.colId + '-' + event.data.ExpenseID) as any)) {
          currenterrors.push(validation);
        }
      }  else {
        this.exceptionService.Process_Exception_By_ID(loanmodel, 155, { Amount: formatcurrency(event.value)}, null);
      }
    }
    return loanmodel;
  }

  private getErrorModel(event: any, classes, text, Validation_ID) {
    return {
      chevron: 'Disburse',
      tab: Page.disburse,
      colId: `[col-id="${event.column.colId}"]`,
      rowId: `[row-id="DIS_${event.data.ExpenseID}"]`,
      level: 1,
      details: classes,
      quetionId: event.column.colId + '-' + event.data.ExpenseID,
      Validation_ID_Text: text,
      Validation_ID: Validation_ID
    };
  }

  clearpreviousdisburseerror() {

    let currenterrors = this.localStorageService.retrieve(
      environment.errorbase
    ) as Array<errormodel>;
    this.localStorageService.store(
      environment.errorbase,
      _.uniq(currenterrors.filter(p => p.chevron != "Disburse")));

  }

  checkRentStatus(loanmodel: loan_model) {

    loanmodel.Disbursements.forEach(element => {
      if (element.RentDetails != undefined) {
        element.RentDetails.forEach(rentdet => {
          let farm = loanmodel.Farms.find(p => p.Farm_ID == Number(rentdet.Farm_ID));
          let entity = {
            Loan_Disburse_ID: element.Loan_Disburse_ID,
            Farm_ID: farm.Farm_ID,
            FC_State_Code: farm.FC_State_Code,
            FC_County_Name: farm.FC_County_Name,
            FSN: farm.FSN,
            Landowner: farm.Landowner,
            Loan_Disburse_Detail_ID: rentdet.Loan_Disburse_Rent_Detail_ID
          };

          if (rentdet.Reimbusement == true) {
            this.exceptionService.Process_Exception_By_ID(loanmodel, 158, entity, 1);
          } else {
            this.exceptionService.Process_Exception_By_ID(loanmodel, 158, entity, null);
          }

        });
      }
    });

    this.validationCount.updateValidationErrorCounts(loanmodel);
  }
}


