import { Loan_Disburse } from "@lenda/models/disbursemodels";
import { dateformat, date } from "@lenda/aggridformatters/valueformatters";



const values: string[] = [
    'Submitted By',
    // 'Disbursement Status',
    // 'Date Time',
    '',
    '',
    'Approving Loan Officer',
    'Disbursement Status',
    'Date Time',
    '',
    'Approving Risk Admin',
    'Disbursement Status',
    'Date Time',
    '',
    'Processor',
    'Disbursement Status',
    'Date Time',
    ''

];

const propertiesforvalues: string[] = [
    'Request_User_Name',
    // 'Request_Status_Ind',
    // 'Request_Date_Time',
    'Special_Inst_Ind',
    '',
    'LO_Approval_User_Name',
    'LO_Approval_Status_Ind',
    'LO_Approval_Date_Time',
    '',
    'Risk_Approval_User_Name',
    'Risk_Approval_Status_Ind',
    'Risk_Approval_Date_Time',
    '',
    'Processor_User_Name',
    'Processor_Status_Ind',
    'Processor_Date_time',
    ''

];

export function createviewfordisburse(obj: Array<Loan_Disburse>) {

  let rows = new Array<any>();

  //first row OLD CODE
  for (let i = 1; i < 2; i++) {
      let row: any = {};
      row.Values = 'Expense';
      row.Commit = "Commit";
      row.Used = "Used";
      row.isheader = true;
      row.Percentage = 'Used %';
      row.Available = "Available";
      row.isActionBar = true;
      obj.forEach(element => {
          let val = propertiesforvalues[i];
          if (i == 1) {
          row["disburse_" + element.Loan_Disburse_ID] = obj.find(p => p.Loan_Disburse_ID == element.Loan_Disburse_ID);
          } else {
          row["disburse_" + element.Loan_Disburse_ID] = getformattedvalue(val, element[val]);
          }
      });
      if (i == 1) {
      row.editordisburse = "Special";
      row.rowHeight = 35;
      }
      //after these start the disursements that we get
      //
      rows.push(row);
      }

  return rows;

}
// tslint:disable-next-line:no-shadowed-variable
function getformattedvalue(type: string, values: any) {

    switch (type) {
        case 'Request_Date_Time':
            return dateformat(values);
        case 'LO_Approval_Date_Time':
            return dateformat(values);
        case 'Risk_Approval_Date_Time':
            return dateformat(values);
        case 'Processor_Date_time':
            return dateformat(values);
            //status ones
        case 'Request_Status_Ind':
            return getstatuscodesfromid(values);
        case 'LO_Approval_Status_Ind':
            return getstatuscodesfromid(values);
        case 'Risk_Approval_Status_Ind':
            return getstatuscodesfromid(values);
        case 'Processor_Status_Ind':
            return getstatuscodesfromid(values);
        default:
            return values;
    }

}

export function getstatuscodesfromid(id: number) {
    switch (id) {
        case 1:
            return "Approved";
        case 0:
            return "Cancelled";
        default:

    }
}
