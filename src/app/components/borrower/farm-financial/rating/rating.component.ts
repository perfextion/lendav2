import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import { Options } from 'ng5-slider';

import { loan_model, LoanStatus } from "@lenda/models/loanmodel";
import { environment } from "@env/environment";
import { Page } from '@lenda/models/page.enum';
import { validationlevel, errormodel } from "@lenda/models/commonmodels";
import { Loan_Master_validator } from '@lenda/models/validatormodels/loanmodel.validator';
import { RefDataModel, RefChevron, LoanMaster } from '@lenda/models/ref-data-model';
import { BorrowerSettings } from '@lenda/preferences/models/user-settings/borrower-settings.model';

import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { DataService } from '@lenda/services/data.service';
import { SettingsService } from "@lenda/preferences/settings/settings.service";
import { PublishService } from '@lenda/services/publish.service';
import { LoanMasterCalculationWorkerService } from "@lenda/Workers/calculations/loan-master-calculation-worker.service";
import { LoancalculationWorker } from "@lenda/Workers/calculations/loancalculationworker";

import { ValueType } from "@lenda/components/borrower/shared/cell-value/cell-value.component";

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RatingComponent implements OnInit, OnDestroy {
  public settings: BorrowerSettings;
  public emptyString: string =  '';

  private subscription: ISubscription;
  public data: any = {};
  public agProReqCredit: number = 0;
  public localloanobj: loan_model;
  public ratingCount: number = 0;
  private reqCreditValid: boolean = false;
  private borrowerChevron: RefChevron = <RefChevron>{};
  private refdata: any = {};
  public toggle = false;
  private preferencesChangeSub: ISubscription;

  constructor(
    private localstorageservice: LocalStorageService,
    private loanMasterCaculationWorker: LoanMasterCalculationWorkerService,
    private loanCalculationWorker: LoancalculationWorker,
    private dataService: DataService,
    private publishService: PublishService,
    private validationService: ValidationService,
    private settingsService: SettingsService
  ) {
    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
  }

  ngOnInit() {

    this.getUserPreferences();

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res != null) {
        this.localloanobj = res;
        this.binddata(this.localloanobj);
      }
    });

    this.localloanobj = this.localstorageservice.retrieve(environment.loankey);

    if (this.localloanobj != null && this.localloanobj != undefined) {

      this.loanMasterCaculationWorker.validateRevenueAndThresholdValues(this.localloanobj);

      this.binddata(this.localloanobj);
      this.agProReqCredit = this.localloanobj.LoanMaster.Ag_Pro_Requested_Credit;
    }

    if(this.refdata){
      this.borrowerChevron = (this.refdata as RefDataModel).RefChevrons.find(c => c.Chevron_ID == 1);
    } else {
      this.borrowerChevron = <RefChevron>{};
    }

    this.settingsService.labelChange.subscribe(res => {
      this.binddata(this.localloanobj);
    });
  }

  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  isLoanEditable(){
    return this.localloanobj.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  private binddata(loanObject: loan_model) {
    if (this.localloanobj && this.localloanobj.LoanMaster) {

      let loanMaster = loanObject.LoanMaster as LoanMaster;

      this.ratingCount = "*".repeat(loanMaster.Borrower_Rating || 0).length;
      this.ratingCount = this.ratingCount > 1 ? this.ratingCount : 1;

      let maxCropStaticValues = this.loanMasterCaculationWorker.getMaxCropStaticValue(loanObject);

      this.data = {
        partOne: [
          {
            text: 'Borrower Rating',
            value: '',
            staticValues: this.loanMasterCaculationWorker.borrowerRatingParams.borrowerRatingstaticValues.borrowerRating,
            hightlightRow: true,
            emptyDefault: true
          },
          {
            text: 'FICO Score',
            value: this.getFicoScoreValue(loanMaster),
            staticValues: this.loanMasterCaculationWorker.borrowerRatingParams.borrowerRatingstaticValues.FICOScore,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
            options: this.getOptions('FICO')
          },
          {
            text: 'CPA Financials',
            value: loanMaster.CPA_Prepared_Financials && loanMaster.CPA_Prepared_Financials === 1 ? 90 : 50,
            staticValues: this.loanMasterCaculationWorker.borrowerRatingParams.borrowerRatingstaticValues.CPAFiancial,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
            options: this.getOptions('CPA')
          },
          {
            text: '3yrs Tax Returns',
            value: loanMaster.Borrower_3yr_Tax_Returns && loanMaster.Borrower_3yr_Tax_Returns === 1 ? 90 : 50,
            staticValues: this.loanMasterCaculationWorker.borrowerRatingParams.borrowerRatingstaticValues.threeYrsReturns,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
            options: this.getOptions('Tax')
          },
          {
            text: 'Bankruptcy',
            value: loanMaster.Previous_Bankruptcy_Status && loanMaster.Previous_Bankruptcy_Status === 1 ? 30 : 90,
            staticValues: this.loanMasterCaculationWorker.borrowerRatingParams.borrowerRatingstaticValues.bankruptcy,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
           options: this.getOptions('Bankruptcy')
          },
          {
            text: 'Judgement',
            value: loanMaster.Judgement_Ind && loanMaster.Judgement_Ind === 1 ? 30 : 90,
            staticValues: this.loanMasterCaculationWorker.borrowerRatingParams.borrowerRatingstaticValues.judgement,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
            options: this.getOptions('Judgement')
          },
          {
            text: 'Years Farming',
            value: this.getYearsFarmingValue(loanMaster),
            staticValues: this.loanMasterCaculationWorker.borrowerRatingParams.borrowerRatingstaticValues.yearsFarming,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
           options: this.getOptions('Farming')
          },
          {
            text: 'Farm Finacial Rating',
            value: loanMaster.Borrower_Farm_Financial_Rating ? this.getRatingPosition(loanMaster.Borrower_Farm_Financial_Rating) : 0,
            staticValues: this.loanMasterCaculationWorker.borrowerRatingParams.borrowerRatingstaticValues.farmFinnacialRatingDisplay,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
           options: this.getOptions('Rating')
          }
        ],
        partTwo: [
          {
            text: 'Income Constant %',
            value: 0,
            staticValues: this.loanMasterCaculationWorker.borrowerRatingParams.incomeConstant,
            valueType: ValueType.PERCENTAGE,
            isConstantPercentItem: true,
          },
          {
            text: 'Revenue Constraint',
            value: this.loanMasterCaculationWorker.getRevanueThresholdValue(loanObject),
            staticValues: this.loanMasterCaculationWorker.getRevanueThresholdStaticValues(loanObject),
            valueType: ValueType.AMOUNT,
            emptyDefault: true,
            isinvalid: this.loanMasterCaculationWorker.invalidRevenueThreshold
          },
          {
            text: 'Insurance Constant %',
            value: 0,
            staticValues: this.loanMasterCaculationWorker.borrowerRatingParams.insuranceConstant,
            valueType: ValueType.PERCENTAGE,
            isConstantPercentItem: true
          },
          {
            text: 'Insurance Constraint',
            value: this.loanMasterCaculationWorker.getInsuranceThresholdValue(loanObject),
            staticValues: this.loanMasterCaculationWorker.getInsuranceThresholdStaticValue(loanObject),
            valueType: ValueType.AMOUNT,
            emptyDefault: true,
            isinvalid: this.loanMasterCaculationWorker.invalidInsuranceThreshold
          },
          {
            text: 'Max Crop Loan',
            value: this.loanMasterCaculationWorker.getMaxCropLoanValue(loanObject),
            staticValues: maxCropStaticValues,
            valueType: ValueType.AMOUNT,
            hightlightRow: true,
            emptyDefault: true,
            totalRow: true,
            isinvalid: this.loanMasterCaculationWorker.invalidMaxCrop
          },
        ],
        partThree: [
          {
            text: 'Max Amount Constraint',
            value: 0,
            staticValues: this.loanMasterCaculationWorker.borrowerRatingParams.maxAmountConstant,
            valueType: ValueType.AMOUNT,
            emptyDefault: true
          },
          {
            text: 'Disc Net Worth Constant %',
            value: 0,
            staticValues: this.loanMasterCaculationWorker.borrowerRatingParams.discNetWorthConstant,
            valueType: ValueType.PERCENTAGE,
            isConstantPercentItem: true
          },
          {
            text: 'Disc Net Worth Constraint',
            value: this.loanMasterCaculationWorker.getDiscNetWorthValue(loanObject),
            staticValues: this.loanMasterCaculationWorker.getDiscWorthStaticValue(loanObject),
            valueType: ValueType.AMOUNT,
            emptyDefault: true
          },
          {
            text: 'Ag-Pro Max Addition',
            value: '', // when changing it to som real value, Make sure to consider in LoanMasterCalculationWorkerService.performDashboardCaclulation
            staticValues: this.loanMasterCaculationWorker.getAgProMaxAdditionStaticValue(loanObject),
            valueType: ValueType.AMOUNT,
            hightlightRow: true,
            emptyDefault: true,
            totalRow: true
          },
          {
            text: 'Ag-Pro Requested Credit',
            showInput: true,
            staticValues: ['', '', '', '', ''],
            emptyDefault: true,
            hightlightRow: true,
            value: '',
          }
        ]
      };
    }
  }

  private getYearsFarmingValue(loanMaster: LoanMaster) {
    const year = new Date().getFullYear();

    if(loanMaster.Year_Begin_Farming) {
      const farming = year - loanMaster.Year_Begin_Farming;
      if(farming >= 7) {
        return 90;
      } else if (farming >= 5) {
        return 70;
      } else if(farming >= 3) {
        return 50;
      } else if(farming >= 2) {
        return 30;
      } else {
        return 10;
      }
    } else {
      return 10;
    }
  }

  private getFicoScoreValue(loanMaster: LoanMaster) {
    const fico = this.loanMasterCaculationWorker.borrowerRatingParams.borrowerRatingstaticValues.FICOScore;
    return !loanMaster.Credit_Score || loanMaster.Credit_Score < fico[3]
      ? 10
      : loanMaster.Credit_Score >= fico[3] && loanMaster.Credit_Score < fico[2]
      ? 30
      : loanMaster.Credit_Score >= fico[2] && loanMaster.Credit_Score < fico[1]
      ? 50
      : loanMaster.Credit_Score >= fico[1] && loanMaster.Credit_Score < fico[0]
      ? 70
      : 90;
  }

  getRatingPosition(Rating: number) {
    return 105 + 0.2 * (Rating - 200);
  }

  getOptions(type?: string) {
    let loanMaster =  this.localloanobj.LoanMaster as LoanMaster;

    let options: Options = {
      floor: 0,
      ceil: 100,
      rightToLeft: true,
      readOnly: true,
      hideLimitLabels: true,
      autoHideLimitLabels: true,
      hidePointerLabels: this.settings ? !this.settings.isLabelsEnabled : true
    };

    if(type == 'FICO') {
      options.translate = () => {
        return String(this.localloanobj.LoanMaster.Credit_Score || 0);
      };
    }

    if(type == 'CPA') {
      options.translate = () => {
        return loanMaster.CPA_Prepared_Financials && loanMaster.CPA_Prepared_Financials === 1 ? 'Yes': 'No';
      };
    }

    if(type == 'Tax') {
      options.translate = () => {
        return loanMaster.Borrower_3yr_Tax_Returns && loanMaster.Borrower_3yr_Tax_Returns === 1 ? 'Yes': 'No';
      };
    }

    if(type == 'Bankruptcy') {
      options.translate = () => {
        return loanMaster.Previous_Bankruptcy_Status == 1 ? 'Yes': 'No';
      };
    }

    if(type == 'Judgement') {
      options.translate = () => {
        return loanMaster.Judgement_Ind && loanMaster.Judgement_Ind === 1 ? 'Yes': 'No';
      };
    }

    if(type == 'Farming') {
      options.translate = () => {
        return loanMaster.Year_Begin_Farming ? (new Date().getFullYear() - loanMaster.Year_Begin_Farming).toString() : '0';
      };
    }

    if(type == 'Rating') {
      options.translate = () => {
        return loanMaster.Borrower_Farm_Financial_Rating ?  loanMaster.Borrower_Farm_Financial_Rating.toLocaleString('en-US', {
          minimumFractionDigits: 2
        }) + '%' : '0%';
      };
    }

    return options;
  }

  getFormattedData(loanObject: loan_model) {
    let loanMaster = loanObject.LoanMaster;
    let borrowerRatingValues = new BorrowerRatingParams();
    borrowerRatingValues.borrowerRating = "*".repeat(loanMaster.Borrower_Rating || 0);
    borrowerRatingValues.FICOScore = loanMaster.Credit_Score;

    borrowerRatingValues.CPAFiancial = loanMaster.CPA_Prepared_Financials ? 'Yes' : 'No';
    //pulling the data from loan master instead of borrower
    // borrowerRatingValues.CPAFiancial = loanMaster.CPA_Prepared_Financials ? 100 : 50
    // borrowerRatingValues.CPAFiancial = loanMaster.CPA_Prepared_Financials === 1 ? 'Yes' : 'No'; //pulling the data from loan master instead of borrower
    borrowerRatingValues.threeYrsReturns = loanMaster.Borrower_3yr_Tax_Returns ? 'Yes' : 'No';
    borrowerRatingValues.bankruptcy = loanMaster.Previous_Bankruptcy_Status ? 'Yes' : 'No';
    // borrowerRatingValues.bankruptcy = loanMaster.Previously_Bankrupt === 1 ? 'Yes' : 'No';
    borrowerRatingValues.judgement = loanMaster.Judgement_Ind ? 'Yes' : 'No';
    // borrowerRatingValues.judgement = loanMaster.Judgement === 1 ? 'Yes' : 'No';
    borrowerRatingValues.yearsFarming = loanMaster.Year_Begin_Farming ? (new Date()).getFullYear() - loanMaster.Year_Begin_Farming : 0;
    borrowerRatingValues.farmFinnacialRating = loanMaster.Borrower_Farm_Financial_Rating || 0;
    return borrowerRatingValues;
  }

  isReqCreditValid(agProReqCredit, val){
    this.reqCreditValid = agProReqCredit >= 0 && agProReqCredit <= val && (this.localloanobj.LoanMaster.Dist_Total_Budget < 1 || val === 0);
    return this.reqCreditValid;
  }

  updateLocalStorage(agProReqCredit?, val?) {
      this.setRequestedCreditvalidationError();
      this.localloanobj.LoanMaster.Ag_Pro_Requested_Credit = this.agProReqCredit || 0;
      this.loanCalculationWorker.performcalculationonloanobject(this.localloanobj);
      this.publishService.enableSync(Page.borrower);
  }

  isUnachievedRating(rating) {
    if (this.localloanobj && this.localloanobj.LoanMaster) {
      if (rating > this.localloanobj.LoanMaster.Borrower_Rating) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  setRequestedCreditvalidationError(){

    this.validationService.validateTableFields<Loan_Master_validator>(
      [],
      [],
      Page.borrower,
      '',
      this.borrowerChevron.Chevron_Code,
      'Ag_Pro_Requested_Credit'
    );

    if(!this.reqCreditValid) return;

    let currenterrors = this.localstorageservice.retrieve(
      environment.errorbase
    ) as Array<errormodel>;

    let errorM = <errormodel>{};


    return {
      result: this.reqCreditValid,
      validation: validationlevel.level2
    }
  }

  getUserPreferences() {
    this.settings = this.settingsService.preferences.userSettings.borrowerSettings;

    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe((preferences) => {
      this.settings = preferences.userSettings.borrowerSettings;
      this.toggle = !this.toggle;
    });
  }
}


class BorrowerRatingParams {
  borrowerRating: string;
  FICOScore: number
  CPAFiancial: string;
  threeYrsReturns: string
  bankruptcy: string;
  judgement: string;
  yearsFarming: number;
  farmFinnacialRating: number;

}
