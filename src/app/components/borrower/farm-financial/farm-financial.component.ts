import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';
import { Options } from 'ng5-slider';

import { loan_model } from '@lenda/models/loanmodel';
import { environment } from '@env/environment.prod';
import { Helper } from '@lenda/services/math.helper';
import { BorrowerSettings } from '@lenda/preferences/models/user-settings/borrower-settings.model';

import { ValueType } from '../shared/cell-value/cell-value.component';
import { calculatedNumberFormatter, currencyFormatter } from '@lenda/aggridformatters/valueformatters';

import { LoanMasterCalculationWorkerService, DefaultFarmFinacialStaticValues } from '@lenda/Workers/calculations/loan-master-calculation-worker.service';
import { SettingsService } from "@lenda/preferences/settings/settings.service";
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-farm-financial',
  templateUrl: './farm-financial.component.html',
  styleUrls: ['./farm-financial.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FarmFinancialComponent implements OnInit, OnDestroy {
  public borrowerSettings: BorrowerSettings;
  private subscription: ISubscription;
  public data: any;
  public localloanobj: loan_model;
  private ffStaticValue: DefaultFarmFinacialStaticValues;
  public highlightCellIndex:  number;

  public options: Options = {
    floor: 0,
    ceil: 90,
    rightToLeft: true,
    readOnly: true,
    hideLimitLabels: true,
    autoHideLimitLabels: true,
  };

  private preferencesChangeSub: ISubscription;

  constructor(
    private localstorageservice: LocalStorageService,
    private loanMasterCaculationWorker: LoanMasterCalculationWorkerService,
    private loanCalculationWorker: LoancalculationWorker,
    private dataService: DataService, private settingsService: SettingsService
  ) { }

  ngOnInit() {

    this.getUserPreferences();

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res != null) {

        this.localloanobj = res;
        this.binddata(this.localloanobj);
      }
    });

    this.settingsService.labelChange.subscribe(res => {
      this.binddata(this.localloanobj);
    });

    this.localloanobj = this.localstorageservice.retrieve(environment.loankey);
    if (this.localloanobj != null && this.localloanobj != undefined)
      this.binddata(this.localloanobj);
  }

  getUserPreferences() {
    this.borrowerSettings = this.settingsService.preferences.userSettings.borrowerSettings;

    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe((preferences) => {
      this.borrowerSettings = preferences.userSettings.borrowerSettings;
    });
  }

  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  private addMergeStaticValue(staticArray, value){
    if(staticArray.length < 4){
      staticArray.push('');
      staticArray.push(value);
    }
    return staticArray;
  }

  private binddata(loanObject: loan_model) {

    this.ffStaticValue = this.loanMasterCaculationWorker.getFarmFinancialStaticValues(loanObject.LoanMaster.FC_Owned);
    if (this.localloanobj && this.localloanobj.LoanMaster) {

      let entities = ['currentRatio', 'workingCapital', 'debtByAssets', 'debtByEquity', 'equityByAssets', 'ROA', 'operatingProfit', 'operatingByExpRev', 'interestByCashFlow'];

      let loanMaster = this.localloanobj.LoanMaster;
      let farmFinancialRatingValues: FarmFinacialValueParams = { ... this.getFormattedData(loanObject) };

      this.data = {
        header: [
            {
            text: 'Farm Financial',
            value: '',
            staticValues: ['Strong', 'Stable', 'Weak', '', 'Weight'],
            hightlightRow: true,
            emptyDefault: true
          }
        ],
        liquidityAnalysis: [
          {
            text: 'Current Ratio',
            value: Helper.rating(farmFinancialRatingValues.currentRatio),
            staticValues: this.addMergeStaticValue(this.ffStaticValue.currentRatio, this.getWeight(farmFinancialRatingValues.currentRatio_weight)),
            emptyDefault: true,
            disableText: true,
            hideScale: true,
            option: this.getOption(this.ffStaticValue.currentRatio[0], this.ffStaticValue.currentRatio[1], false)
          },
          {
            text: 'Working Capital',
            value: Helper.rating(farmFinancialRatingValues.workingCapital),
            staticValues: this.addMergeStaticValue(this.ffStaticValue.workingCapital, this.getWeight(farmFinancialRatingValues.workingCapital_weight)),
            emptyDefault: true,
            disableText: true,
            hideScale: true,
            option: this.getOption(this.ffStaticValue.workingCapital[0], this.ffStaticValue.workingCapital[1], false)
          }
        ],
        solvencyAnalysis: [
          {
            text: 'Debt/Assets',
            value: Helper.rating(farmFinancialRatingValues.debtByAssets),
            staticValues: this.addMergeStaticValue(this.ffStaticValue.debtByAssets, this.getWeight(farmFinancialRatingValues.debtByAssets_weight)),
            staticValueType: ValueType.PERCENTAGE,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
            option: this.getOption(this.ffStaticValue.debtByAssets[0], this.ffStaticValue.debtByAssets[1], true)
          },
          {
            text: 'Equity/Assets',
            value: Helper.rating(farmFinancialRatingValues.equityByAssets),
            staticValues: this.addMergeStaticValue(this.ffStaticValue.equityByAssets, this.getWeight(farmFinancialRatingValues.equityByAssets_weight)),
            staticValueType: ValueType.PERCENTAGE,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
            option: this.getOption(this.ffStaticValue.equityByAssets[0], this.ffStaticValue.equityByAssets[1], false)
          },
          {
            text: 'Debt/Equity',
            value: Helper.rating(farmFinancialRatingValues.debtByEquity),
            staticValues: this.addMergeStaticValue(this.ffStaticValue.debtByEquity, this.getWeight(farmFinancialRatingValues.debtByEquity_weight)),
            staticValueType: ValueType.PERCENTAGE,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
            option: this.getOption(this.ffStaticValue.debtByEquity[0], this.ffStaticValue.debtByEquity[1], true)
          }
        ],
        profitabilityAnalysis: [
          {
            text: 'ROA',
            value: Helper.rating(farmFinancialRatingValues.ROA),
            staticValues: this.addMergeStaticValue(this.ffStaticValue.ROA, this.getWeight(farmFinancialRatingValues.ROA_weight)),
            staticValueType: ValueType.PERCENTAGE,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
            option: this.getOption(this.ffStaticValue.ROA[0], this.ffStaticValue.ROA[1], false)
          },
          {
            text: 'Operating Profit',
            value: Helper.rating(farmFinancialRatingValues.operatingProfit),
            staticValues: this.addMergeStaticValue(this.ffStaticValue.operatingProfit, this.getWeight(farmFinancialRatingValues.operatingProfit_weight)),
            staticValueType: ValueType.PERCENTAGE,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
            option: this.getOption(this.ffStaticValue.operatingProfit[0], this.ffStaticValue.operatingProfit[1], false)
          },
          {
            text: 'Operating Exp/Rev',
            value: Helper.rating(farmFinancialRatingValues.operatingByExpRev),
            staticValues: this.addMergeStaticValue(this.ffStaticValue.operatingByExpRev, this.getWeight(farmFinancialRatingValues.operatingByExpRev_weight)),
            staticValueType: ValueType.PERCENTAGE,
            emptyDefault: true,
            disableText: true,
            hideScale: true,
            option: this.getOption(this.ffStaticValue.operatingByExpRev[0], this.ffStaticValue.operatingByExpRev[1], true)
          },
          {
            text: 'Interest/Cashflow',
            value: Helper.rating(farmFinancialRatingValues.interestByCashFlow),
            staticValues: this.addMergeStaticValue(this.ffStaticValue.interestByCashFlow, this.getWeight(farmFinancialRatingValues.interestByCashFlow_weight)),
            staticValueType: ValueType.PERCENTAGE,
            disableText: true,
            emptyDefault: true,
            hideScale: true,
            option: this.getOption(this.ffStaticValue.interestByCashFlow[0], this.ffStaticValue.interestByCashFlow[1], true)
          },
          {
            text: 'Farm Financial Rating',
            value: Helper.rating(loanMaster.Borrower_Farm_Financial_Rating),
            weightSum: calculatedNumberFormatter(farmFinancialRatingValues.weightSum, 2),
            staticValues: this.addMergeStaticValue(['100.0%', '0.0%', '-100.0%'], calculatedNumberFormatter(farmFinancialRatingValues.totalFarmFinacialRating_weight, 2)),
            rating: farmFinancialRatingValues.totalFarmFinacialRating,
            staticValueType: ValueType.PERCENTAGE,
            hightlightRow: true,
            disableText: true,
            emptyDefault: true,
            hideScale: true,
            option: this.getOption('100.0%', '0.0', false, true)
          }
        ]
      };

      this.highlightCellIndex = farmFinancialRatingValues.totalFarmFinacialRating && farmFinancialRatingValues.totalFarmFinacialRating >= 100 ? 0 : farmFinancialRatingValues.totalFarmFinacialRating >= 0 ? 1 : 2;
      if (this.localloanobj.LoanMaster.Borrower_Farm_Financial_Rating != farmFinancialRatingValues.totalFarmFinacialRating) {
        this.localloanobj.LoanMaster.Borrower_Farm_Financial_Rating = parseFloat(farmFinancialRatingValues.totalFarmFinacialRating.toFixed(1));
        // this.loanCalculationWorker.performcalculationonloanobject(this.localloanobj, false);
      }
    }
  }

  getWeight(weight: number) {
    let v = calculatedNumberFormatter(weight, 2);

    if(parseFloat(v) == 0) {
      return '-';
    }
    return v;
  }

  getOption(strong, stable, isLessThan?: boolean, isFarm = false){
    strong = parseFloat(strong);
    stable = parseFloat(stable);

    let options: Options = {
      floor: 0,
      ceil: 90,
      rightToLeft: true,
      readOnly: true,
      hideLimitLabels: true,
      autoHideLimitLabels: true,
      translate: (value: number): string => {
        if(isFarm) {
          return (value / 10000).toLocaleString('en-US',{
            style: 'percent',
            minimumFractionDigits: 2
          });
        }
        return parseFloat((value / 100).toFixed(2)).toLocaleString('en-US',{
          minimumFractionDigits: 2
        });
      },
      hidePointerLabels: !this.borrowerSettings.isLabelsEnabled
    };

    let ratio = Math.abs(strong - stable);
    options.rightToLeft = isLessThan ? false : true;

    if(options.rightToLeft) {
      options.floor = Helper.rating(stable - ratio);
      options.ceil = Helper.rating(strong + ratio);
    } else {
      options.floor = Helper.rating(strong - ratio);
      options.ceil = Helper.rating(stable + ratio);
    }

    return options;
  }

  private getFormattedData(loanObject: loan_model) {
    return this.loanMasterCaculationWorker.getFarmFinancialRating(loanObject);
  }

  private getRevanueThresholdValue(loanObject){
    return this.loanMasterCaculationWorker.getRevanueThresholdValue(loanObject) || 0;
  }

  getPossibleData(entities: Array<string>, farmFinancialRatingValues: FarmFinacialValueParams, ffStaticValue) {
    let ffPossibleValues = new FarmFinacialValueParams();
    let totalPossibles = 0;
    entities.forEach(entity => {
      ffPossibleValues[entity] = this.loanMasterCaculationWorker.getPossible(farmFinancialRatingValues[entity], ffStaticValue[entity]);
      totalPossibles += ffPossibleValues[entity];
    });

    return {
      ffPossibleValues: ffPossibleValues,
      totalPossibles: totalPossibles
    }
  }

  getStateData(entities: Array<string>, farmFinancialRatingValues: FarmFinacialValueParams, ffStaticValue) {
    let ffStateValues = new FarmFinacialValueParams();

    entities.forEach(entity => {
      ffStateValues[entity] = this.loanMasterCaculationWorker.getState(farmFinancialRatingValues[entity], ffStaticValue[entity]);
    });

    return ffStateValues;
  }

  // getRatingData(entities: Array<string>, farmFinancialRatingValues: FarmFinacialValueParams, ffStaticValue) {
  //   let ffRatingValues = new FarmFinacialValueParams();
  //   let totalRatings = 0;
  //   entities.forEach(entity => {
  //     ffRatingValues[entity] = this.loanMasterCaculationWorker.getRating(farmFinancialRatingValues[entity], ffStaticValue[entity]);
  //     totalRatings += ffRatingValues[entity];
  //   });

  //   let possibleObj = this.getPossibleData(entities, farmFinancialRatingValues, ffStaticValue);
  //   let totalPossible = possibleObj.totalPossibles;

  //   return {
  //     //100+(Actual value-Strong) *(100-0)/ (Strong-Stable)
  //     ffRatingValues: ffRatingValues,
  //     // weight sum - weight
  //     totalRatings: parseFloat((totalRatings / totalPossible).toFixed(1))
  //   }
  // }

  private currencyFormatter(value){
    return currencyFormatter(value, 0);
  }

  private getRating(input: number, strong: number, stable: number){
    if(!input) input = 0;
    let n = input.toFixed(2);
    return (100+(parseFloat(n)-strong) *(100-0)/ (strong-stable))/100;
  }

  private getWeightSum(weight: number, rating: number){

    return weight * rating;
  }
}

export class FarmFinacialValueParams {
  currentRatio: number;
  workingCapital: number;
  debtByAssets: number;
  debtByEquity: number;
  equityByAssets: number;
  ROA: number;
  operatingProfit: number;
  operatingByExpRev: number;
  interestByCashFlow: number;
  totalFarmFinacialRating: number;

  currentRatio_rating: number;
  workingCapital_rating: number;
  debtByAssets_rating: number;
  debtByEquity_rating: number;
  equityByAssets_rating: number;
  ROA_rating: number;
  operatingProfit_rating: number;
  operatingByExpRev_rating: number;
  interestByCashFlow_rating: number;
  totalFarmFinacialRating_rating: number;

  currentRatio_weight: number = 3;
  workingCapital_weight: number = 3;
  debtByAssets_weight: number = 3;
  debtByEquity_weight: number = 3;
  equityByAssets_weight: number = 3;
  ROA_weight: number = 1;
  operatingProfit_weight: number = 1;
  operatingByExpRev_weight: number;
  interestByCashFlow_weight: number;
  totalFarmFinacialRating_weight: number = 17;

  currentRatio_weight_sum: number;
  workingCapital_weight_sum: number;
  debtByAssets_weight_sum: number;
  debtByEquity_weight_sum: number;
  equityByAssets_weight_sum: number;
  ROA_weight_sum: number;
  operatingProfit_weight_sum: number;
  operatingByExpRev_weight_sum: number;
  interestByCashFlow_weight_sum: number;
  totalFarmFinacialRating_weight_sum: number;

  weightSum: number;
}
