import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-cell-value',
  templateUrl: './cell-value.component.html',
  styleUrls: ['./cell-value.component.scss']
})
export class CellValueComponent {
  @Input('bolderFont') bolderFont: boolean;
  @Input('borderRight') borderRight: boolean;
  @Input('valueType') valueType: ValueType;
  @Input('value') value: any;
  @Input('isStar') isStar: boolean;
  @Input('disableText') disableText: boolean;
  @Input('emptyDefault') emptyDefault: boolean;
  @Input('backgroundHighlight') backgroundHighlight: boolean;
  @Input('cellHighlight') cellHighlight: boolean;
  @Input('backgroundGreen') backgroundGreen: boolean;
  @Input('backgroundLightGreen') backgroundLightGreen: boolean;
  @Input('backgroundYellow') backgroundYellow: boolean;
  @Input('colorRed') colorRed: boolean;
  @Input('nobackground') nobackground: boolean;
  @Input('backgroundRed') backgroundRed: boolean;
  @Input('ratingCount') ratingCount: number;
  @Input() lightColor: boolean;

  ValueType = ValueType;
}

export enum ValueType {
  AMOUNT,
  PERCENTAGE,
  EMPTY_DEFAULT
}
