import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { Masks } from '@lenda/shared/shared.constants';
import { loan_model, loan_farmer } from '@lenda/models/loanmodel';
import { Page } from '@lenda/models/page.enum';
import { Chevron, Exception_ID } from '@lenda/models/loan-response.model';
import { RefDataModel, RefChevron } from '@lenda/models/ref-data-model';
import { BorrowerSettings } from '@lenda/preferences/models/user-settings/borrower-settings.model';

import { MasterService } from '@lenda/master/master.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { PublishService } from '@lenda/services/publish.service';
import { BorrowerService } from './borrower.service';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { DataService } from '@lenda/services/data.service';
import { SettingsService } from "@lenda/preferences/settings/settings.service";
import { Get_Formatted_Date } from '@lenda/services/common-utils';

@Component({
  selector: 'app-borrower',
  templateUrl: './borrower.component.html',
  styleUrls: ['./borrower.component.scss'],
  providers: [BorrowerService]
})
export class BorrowerComponent implements OnInit, AfterViewInit, OnDestroy {
  public borrowerSettings: BorrowerSettings;
  public currentPageName: Page = Page.borrower;
  public Chevron: typeof Chevron = Chevron;
  public affiliatedLoans: any[] = [];

  public num = Masks.number;

  public creditScoreExc = Exception_ID.Credit_Score;

  public creditScoreForm: FormGroup;
  private creditScoreChevRon;
  private refdata: RefDataModel;
  public localLoanObj: loan_model;

  public total_acres: number;
  public total_arm_commit: number;
  public total_dist_commit: number;
  public total_commit: number;
  public total_risk_cushion: number;
  public total_outstanding: number;

  public userBorrower: boolean = false;
  public farmerInfo: loan_farmer;

  private getLoanObjectSub: ISubscription;
  preferencesChangeSub: ISubscription;

  max_credit_score_date: string;

  constructor(
    private borrowerService: BorrowerService,
    private localStorageService: LocalStorageService,
    private publishService: PublishService,
    private loanCalculationService: LoancalculationWorker,
    private settingsService: SettingsService,
    private fb: FormBuilder,
    private validationService: ValidationService,
    private dataservice: DataService,
    public masterSvc: MasterService
  ) {
    this.refdata = this.localStorageService.retrieve(environment.referencedatakey);
    this.localLoanObj = this.localStorageService.retrieve(environment.loankey);

    if(this.refdata){
      this.creditScoreChevRon = (this.refdata as RefDataModel).RefChevrons.find(c => c.Chevron_ID == Chevron.Credit_Score);
    } else {
      this.creditScoreChevRon = <RefChevron>{};
    }

    this.max_credit_score_date = Get_Formatted_Date(new Date());
  }

  ngOnInit() {
    this.createCreditScoreForm();
    let loan: loan_model = this.localStorageService.retrieve(environment.loankey);
    this.initBorrowerInfo(loan);

    // Get settings for borrwer
    this.getUserPreferences();
    this.getAffiliatedLoans();

    this.getLoanObjectSub = this.dataservice.getLoanObject().subscribe(res => {
      this.localLoanObj = res;
      this.initBorrowerInfo(res);
    });
  }

  ngOnDestroy() {
    if(this.getLoanObjectSub) {
      this.getLoanObjectSub.unsubscribe();
    }

    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  useBorrowerChange($event) {
    if ($event && this.isLoanEditable()) {
      let obj: any = {};
      obj.Farmer_First_Name = this.localLoanObj.LoanMaster.Borrower_First_Name;
      obj.Farmer_MI = this.localLoanObj.LoanMaster.Borrower_MI;
      obj.Farmer_Last_Name = this.localLoanObj.LoanMaster.Borrower_Last_Name;
      obj.Farmer_SSN_Hash = this.localLoanObj.LoanMaster.Borrower_SSN_Hash;
      obj.Farmer_Address = this.localLoanObj.LoanMaster.Borrower_Address;
      obj.Farmer_City = this.localLoanObj.LoanMaster.Borrower_City;
      obj.Farmer_State_ID = this.localLoanObj.LoanMaster.Borrower_State_Abbrev;
      obj.Farmer_Zip = this.localLoanObj.LoanMaster.Borrower_Zip;
      obj.Farmer_DL_Num = this.localLoanObj.LoanMaster.Borrower_DL_Num;
      obj.Farmer_DL_State = this.localLoanObj.LoanMaster.Borrower_DL_State_Abbrev;
      obj.Farmer_Phone = this.localLoanObj.LoanMaster.Borrower_Phone;
      obj.Farmer_Email = this.localLoanObj.LoanMaster.Borrower_Email;
      obj.Farmer_DOB = this.localLoanObj.LoanMaster.Borrower_DOB;
      obj.Farmer_Pref_Contact_Ind = this.localLoanObj.LoanMaster.Borrower_Preferred_Contact_Ind;

      this.farmerInfo = obj;
    }
  }


  private initBorrowerInfo(loan: loan_model) {
    if (loan != null || loan != undefined) {
      let creditdate = loan.LoanMaster.Credit_Score_Date;
      creditdate = this.formatDate(creditdate);

      if (creditdate == '1/1/1900') {
        creditdate = null;
      }

      if (creditdate) {
        this.creditScoreForm.controls.Credit_Score_Date.setValue(this.formatDate(creditdate));
      }

      let value = loan.LoanMaster.Credit_Score && loan.LoanMaster.Credit_Score > 0 ? loan.LoanMaster.Credit_Score : null;
      this.creditScoreForm.controls.Credit_Score.setValue(value);

      if(this.isLoanEditable()) {
        this.creditScoreForm.enable();
      } else {
        this.creditScoreForm.disable();
      }
    }
  }

  ngAfterViewInit(): void {
    this.creditScoreForm.markAsTouched();
    this.markFormGroupTouched(this.creditScoreForm);
  }

  private createCreditScoreForm(){
    this.creditScoreForm =  this.fb.group({
      Credit_Score: ['', Validators.required],
      Credit_Score_Date:  [null, Validators.required]
    });
  }

  formatDate(strDate) {
    if (strDate) {
      let date = new Date(strDate);
      const dateToString = d => `${('00' + (d.getMonth() + 1)).slice(-2)}/${('00' + d.getDate()).slice(-2)}/${d.getFullYear()}`
      return dateToString(date);
    } else {
      return '';
    }
  }

  private getUserPreferences() {
    this.borrowerSettings = this.settingsService.preferences.userSettings.borrowerSettings;

    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe((preferences) => {
      this.borrowerSettings = preferences.userSettings.borrowerSettings;
    });
  }

  public updatecreditvalues() {
    if(this.isLoanEditable()) {
      this.localLoanObj.LoanMaster.Credit_Score = this.creditScoreForm.controls.Credit_Score.value;
      this.localLoanObj.LoanMaster.Credit_Score_Date = this.creditScoreForm.controls.Credit_Score_Date.value;

      this.localLoanObj.srccomponentedit = 'CreditScore';
      this.loanCalculationService.performcalculationonloanobject(this.localLoanObj, true);
      this.publishService.enableSync(this.currentPageName);

      this.getAllValidationErrors(this.creditScoreForm);
    }
  }


  /**
  * Sync to database - publish button event
  */
  public synctoDb() {
    setTimeout(() => {
      // waiting for 0.5 sec to get the blur events excecuted meanwhile
      this.publishService.syncCompleted();
      this.borrowerService.syncToDb(this.localStorageService.retrieve(environment.loankey));
    }, 500);

  }

  public isLoanEditable(){
    return this.borrowerService.isLoanEditable();
  }

  private markFormGroupTouched(formGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
    });

    this.getAllValidationErrors(this.creditScoreForm);
  };

  getAllValidationErrors(formGroup){

    //SaveForm Validation Errors
    this.validationService.validateFormField(formGroup, Page.borrower, 'app-borrower', 'creditScoreForm', this.creditScoreChevRon.Chevron_Code);
  }

  private getAffiliatedLoans(){
    //Checking on the Loan_Full_ID for now since incorrect data is stored in Loan_ID
    let loanID = this.localStorageService.retrieve(environment.loankey).Loan_Full_ID;
    loanID = loanID.split('-')[0];
    loanID = Number(loanID);

    this.affiliatedLoans= [];
    let loans = this.localStorageService.retrieve(environment.loanGroup);
    let filteredLoans = loans ? loans.filter(loan => {
      return loan.IsAffiliated;
    }) : [];

    filteredLoans.forEach(loan => {
      this.affiliatedLoans.push(loan.LoanJSON);
    });

    this.getTotals();
  }

  private getTotals() {

    this.total_acres = 0;
    this.total_arm_commit = 0;
    this.total_dist_commit = 0;
    this.total_commit = 0;
    this.total_risk_cushion = 0;
    this.total_outstanding = 0;

    this.affiliatedLoans.forEach(loan=>{

      let master = loan.LoanMaster;

      this.total_acres += master.Total_Acres || 0;
      this.total_arm_commit += master.ARM_Commitment || 0;
      this.total_dist_commit += master.Dist_Commitment || 0;
      this.total_commit += master.Total_Commitment || 0;
      this.total_risk_cushion += master.Risk_Cushion_Percent || 0;
      this.total_outstanding += (master.Total_Commitment || 0 ) - ((master.Total_Commitment || 0) * 0.80) || 0;
    })
  }

  public crossCollAffLoans() {
    if(this.isLoanEditable) {
      this.loanCalculationService.crossCollAffLoans();
    }
  }

  isCrossCollateralized(Loan_ID: string){
    let LoanID = Number(Loan_ID.split('-')[0]);

    let loanObject: loan_model = this.localStorageService.retrieve(environment.loankey);
    let collateralizeItems = loanObject.Cross_Collateralized_Loans;
    let existingIDs = collateralizeItems.filter(loan => loan.ActionStatus != 3).map(loan => loan.Loan_ID)

    return existingIDs.includes(LoanID);
  }

  public get hasIncomeHistory() {
    try {
      return this.localLoanObj.BorrowerIncomeHistory.some(i => i.Borrower_Revenue - i.Borrower_Expense > 0);
    } catch {
      return false;
    }
  }

  public get hasCreditScore() {
    try {
      return this.localLoanObj.LoanMaster.Credit_Score > 0;
    } catch {
      return false;
    }
  }

  public get hasFarmerInfo() {
    try {
      let lm = this.localLoanObj.LoanMaster;
      return (
        lm.Farmer_First_Name || lm.Farmer_Last_Name || lm.Farmer_SSN_Hash
      );
    } catch {
      return false;
    }
  }

  public get hasBorrowerInfo() {
    try {
      let lm = this.localLoanObj.LoanMaster;
      return (
        lm.Borrower_First_Name || lm.Borrower_Last_Name || lm.Borrower_SSN_Hash
      );
    } catch {
      return false;
    }
  }

  public validateLength(event) {
    return event.target.value && String(event.target.value).length < 3;
  }
}
