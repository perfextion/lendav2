import { Component, OnInit, Output, Input, EventEmitter, OnChanges, AfterViewInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';
import { AsYouType } from 'libphonenumber-js'

import { environment } from '@env/environment.prod';

import { BorrowerEntityType, borrower_model, LoanStatus } from '@lenda/models/loanmodel';
import { Preferred_Contact_Ind_Options } from '@lenda/Workers/utility/aggrid/preferredcontactboxes';
import { Masks, Patterns } from '@lenda/shared/shared.constants';
import { Page } from '@lenda/models/page.enum';
import { Chevron } from '@lenda/models/loan-response.model';
import { RefDataModel, RefChevron } from '@lenda/models/ref-data-model';
import { Master_Borrower } from '@lenda/models/master-borrower.model';

import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { BorrowerService } from '../../borrower.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';

import { BorrowerSignerComponent } from '../../borrower-signer/borrower-signer.component';
import { Get_Formatted_Date } from '@lenda/services/common-utils';

@Component({
  selector: 'app-borrower-info-form',
  templateUrl: './borrower-info-form.component.html',
  styleUrls: ['./borrower-info-form.component.scss'],
  providers : [BorrowerService]
})
export class BorrowerInfoFormComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {

  borrowerInfoForm: FormGroup;
  stateList: Array<any>;

  entityType = [];
  idTypes = [{key : IDType.SSN, value : 'SSN'}, {key : IDType.Tax_ID, value : 'Tax ID'}];
  ssnFormats = [{key : IDType.SSN, value : Masks.ssn}, {key : IDType.Tax_ID, value : Masks.ein}];
  ssnPatterns = [{key : IDType.SSN, value : Patterns.ssn}, {key : IDType.Tax_ID, value : Patterns.ein}];

  individualEntities = [
    BorrowerEntityType.Individual,
    BorrowerEntityType.IndividualWithSpouse
  ];

  orgnaizationEntities = [
    BorrowerEntityType.Partner,
    BorrowerEntityType.Joint,
    BorrowerEntityType.Corporation,
    BorrowerEntityType.LLC,
    BorrowerEntityType.Limited_Liability_Partnership,
    BorrowerEntityType.Limited_Partnership,
    BorrowerEntityType.Trust,
  ];

  EntityGroup : typeof EntityGroup = EntityGroup;
  BorrowerEntityType : typeof BorrowerEntityType = BorrowerEntityType;

  associationTypeCodes = [BorrowerEntityType.Partner, BorrowerEntityType.Joint, BorrowerEntityType.Corporation, BorrowerEntityType.LLC];

  Preferred_Contact_Ind_Options = Preferred_Contact_Ind_Options;

  @Output('onFormValueChange')
  onFormValueChange: EventEmitter<any> = new EventEmitter<any>();

  @Input() borrowerInfo : borrower_model;

  @Input() hasSignerDetails: boolean;

  private borrowerChevRon: RefChevron;
  private refdata: RefDataModel;

  private masterBorrowerList: Array<Master_Borrower> = [];
  public filteredBorrwerList: Observable<Array<Master_Borrower>>;
  public sshFilteredList: Array<Master_Borrower> = [];

  showExitingBorrowerMessage: boolean;
  borrowerFirstName: string;

  show_ssn_dropdown: boolean;
  ssnSubscription: ISubscription;

  max_dob: string;

  constructor(
    private fb: FormBuilder,
    private localStorageService : LocalStorageService,
    private borrowerService : BorrowerService,
    private validationService: ValidationService,
    private loanApiService: LoanApiService,
    private toastr: ToastrService,
    private dialog: MatDialog
  ) {

    this.refdata = this.localStorageService.retrieve(environment.referencedatakey);

    if(this.refdata){
      this.borrowerChevRon = (this.refdata as RefDataModel).RefChevrons.find(c => c.Chevron_ID == Chevron.Borrower);
    } else {
      this.borrowerChevRon = <RefChevron>{};
    }

    this.max_dob = Get_Formatted_Date(new Date());
  }

  ngOnInit() {
    this.stateList = this.localStorageService.retrieve(environment.referencedatakey).StateList;
    this.entityType = this.borrowerService.entityType;
    this.borrowerFirstName = this.borrowerInfo.Borrower_First_Name;

    this.isLoanEditable();
    this.initMasterBorrowerList();

    this.borrowerInfoForm.controls['Borrower_Phone'].valueChanges.subscribe(
      (selectedValue) => {
        if(this.borrowerInfoForm.get('Borrower_Phone').value){
          let str = this.borrowerInfoForm.get('Borrower_Phone').value.toString();
          if(new AsYouType('US').input(str) !== str){
            this.borrowerInfoForm.controls['Borrower_Phone'].setValue(new AsYouType('US').input(str));
          }
          if(str.length > 14){
            this.borrowerInfoForm.controls['Borrower_Phone'].setValue(str.substring(0, 14))
          }
        }

      }
  );

    this.borrowerInfoForm.controls['Spouse_Phone'].valueChanges.subscribe(
      (selectedValue) => {
        if(this.borrowerInfoForm.get('Spouse_Phone').value){
          let str = this.borrowerInfoForm.get('Spouse_Phone').value.toString();
          if(new AsYouType('US').input(str) !== str){
            this.borrowerInfoForm.controls['Spouse_Phone'].setValue(new AsYouType('US').input(str));
          }
          if(str.length > 14){
            this.borrowerInfoForm.controls['Spouse_Phone'].setValue(str.substring(0, 14))
          }
        }

      }
    );

    this.borrowerInfoForm.controls['Borrower_State_Abbrev'].valueChanges.subscribe(
      selectedValue => {
        let state = this.refdata.StateList.find(a => a.State_Abbrev == selectedValue);

        if(!this.borrowerInfoForm.get('Borrower_DL_State_Abbrev').value){
          this.borrowerInfoForm.get('Borrower_DL_State_Abbrev').setValue(selectedValue);
        if(state) {
          this.borrowerInfo['Borrower_DL_State'] = state.State_Name;
        } else {
          this.borrowerInfo['Borrower_DL_State'] = '';
        }
        }

        if(state) {
          this.borrowerInfo['Borrower_State'] = state.State_Name;
        } else {
          this.borrowerInfo['Borrower_State'] = '';
        }
      }
    );

    this.borrowerInfoForm.controls['Borrower_DL_State_Abbrev'].valueChanges.subscribe(
      selectedValue => {
        let dlstate = this.refdata.StateList.find(a => a.State_Abbrev == selectedValue);
        if(dlstate) {
          this.borrowerInfo['Borrower_DL_State'] = dlstate.State_Name;
        } else {
          this.borrowerInfo['Borrower_DL_State'] = '';
        }
      }
    );

    this.borrowerInfoForm.controls['Spouse_State_Abbrev'].valueChanges.subscribe(
      selectedValue => {
        let state = this.refdata.StateList.find(a => a.State_Abbrev == selectedValue);
        if(state) {
          this.borrowerInfo.Spouse_State = state.State_Name;
        } else {
          this.borrowerInfo.Spouse_State = '';
        }
      }
    );

    this.borrowerInfoForm.controls['Borrower_SSN_Hash'].valueChanges.subscribe(
      (value: any) => {
        if(this.show_ssn_dropdown) {
          if(value && String(value).length == 9) {
            this.ssnSubscription = this.loanApiService.getBorrowerBySSN_Hash(value, this.borrowerInfo['Crop_Year']).subscribe(res => {
              if(res.ResCode == 1) {
                if(res.Data.Master_Borrower_ID > 0) {
                  this.sshFilteredList = [res.Data];
                }
                this.showExitingBorrowerMessage = false;
              } else {
                this.showExitingBorrowerMessage = true;
              }
            });
          } else {
            this.sshFilteredList = [];
          }
        }
      }
    );
  }

  ngOnDestroy() {
    if(this.ssnSubscription) {
      this.ssnSubscription.unsubscribe();
    }
  }

  private initMasterBorrowerList() {
    this.masterBorrowerList = this.localStorageService.retrieve(environment.masterborrowerlist) || [];

    this.filteredBorrwerList = this.borrowerInfoForm.controls['Borrower_First_Name'].valueChanges
      .pipe(
        startWith<string | Master_Borrower>(''),
        map(value => typeof value === 'string' ? value : value ? value.Borrower_First_Name : ''),
        map(name => {
          let data: Master_Borrower[];

          if(name){
            data = this._filter(name);

            if(name.length > 0) {
              this.show_ssn_dropdown = false;
            } else {
              this.show_ssn_dropdown = true;
            }
          } else {
            data = this.masterBorrowerList.slice();
            this.show_ssn_dropdown = true;
          }

          this.set_ssn_drop_down();
          return data;
        })
      );
  }

  onSSNSelectionChange(obj: Master_Borrower) {
    this.setValues(obj);
    this.show_ssn_dropdown = false;
    this.onValueChange();
  }

  private _filter(name: string): Master_Borrower[] {
    const filterValue = name.toLowerCase();
    return this.masterBorrowerList.filter(option => (option.Borrower_First_Name.toLowerCase() + ' ' + option.Borrower_Last_Name.toLowerCase()).indexOf(filterValue) > -1);
  }

  onSelectionChange($event, obj: Master_Borrower) {
    if($event.source.selected) {
      this.loanApiService.getBorrower(obj.Master_Borrower_ID, this.borrowerInfo['Crop_Year']).subscribe(res => {
        if (res.ResCode == 1) {
          this.setValues(res.Data);
          this.borrowerFirstName = res.Data.Borrower_First_Name;
          this.showExitingBorrowerMessage = false;
          this.show_ssn_dropdown = false;
        } else {
          this.showExitingBorrowerMessage = true;
          this.borrowerInfoForm.controls['Borrower_First_Name'].setValue(this.borrowerFirstName);
        }
      });
    }
  }

  ngAfterViewInit(): void {

    this.setSpouseValidation();

    let sourcepath = location.pathname;

    if(!(sourcepath.includes("createloan"))){
      this.borrowerInfoForm.markAsTouched();
      this.markFormGroupTouched(this.borrowerInfoForm);
    }
  }

  ngOnChanges() {
    if (this.borrowerInfo) {
      this.borrowerInfo.Borrower_Entity_Type_Code = this.borrowerInfo.Borrower_Entity_Type_Code || BorrowerEntityType.Individual;
      this.borrowerInfo.Borrower_ID_Type = this.borrowerInfo.Borrower_ID_Type || IDType.SSN;

      this.createForm(this.borrowerInfo);
      this.setSpouseValidation();

     this.set_ssn_drop_down();

      if(this.borrowerInfoForm) {
        if(this.borrowerInfo['Loan_Status'] == LoanStatus.Working) {
          this.borrowerInfoForm.enable();
        } else {
          this.borrowerInfoForm.disable();
        }
      }
    }
  }

  private set_ssn_drop_down() {
    if(this.individualEntities.findIndex(a => a == this.borrowerInfo.Borrower_Entity_Type_Code) > -1) {
      this.show_ssn_dropdown = !this.borrowerInfo.Borrower_First_Name && !this.borrowerInfo.Borrower_Last_Name;
    } else {
      this.show_ssn_dropdown = !this.borrowerInfo.Borrower_First_Name;
    }
  }

  private markFormGroupTouched(formGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
    });
    this.getAllValidationErrors(this.borrowerInfoForm);
  };


  getPlaceholder(){
    let selectedIdType = this.idTypes.find(i=>i.key === this.borrowerInfoForm.controls['Borrower_ID_Type'].value)
    if(selectedIdType){
      return selectedIdType.value || selectedIdType.key;
    }else{
      throw "Invalid Entity Group";

    }
  }

  getSSNFormat(){
    let selectedIdType = this.ssnFormats.find(i=>i.key === this.borrowerInfoForm.controls['Borrower_ID_Type'].value);
    if(selectedIdType) {
      return selectedIdType.value;
    }
    return  this.ssnFormats.find(a => a.key == IDType.SSN);
  }

  getSSNPattern(){
    let selectedIdType = this.ssnPatterns.find(i=>i.key === this.borrowerInfoForm.controls['Borrower_ID_Type'].value);
    if(selectedIdType) {
      return selectedIdType.value;
    }
    return  this.ssnPatterns.find(a => a.key == IDType.SSN);
  }


  createForm(formData) {

    if(this.borrowerInfoForm){
      formData.Borrower_DOB=this.formatDate(formData.Borrower_DOB);
      this.borrowerInfoForm.patchValue(formData);
    }else{
        this.borrowerInfoForm = this.fb.group({
        Borrower_First_Name: [formData.Borrower_First_Name || '', Validators.required],
        Borrower_MI: [formData.Borrower_MI || ''],
        Borrower_Last_Name: [formData.Borrower_Last_Name || ''],
        Borrower_ID_Type : [{value :formData.Borrower_ID_Type || '',disabled : true}, Validators.required],
        Borrower_SSN_Hash: [formData.Borrower_SSN_Hash || '', Validators.required],
        Borrower_Entity_Type_Code: [formData.Borrower_Entity_Type_Code || '', Validators.required],
        Borrower_DL_State_Abbrev : [formData.Borrower_DL_State_Abbrev || ''],
        Borrower_Dl_Num : [formData.Borrower_DL_Num || ''],
        Borrower_Address: [formData.Borrower_Address || '', Validators.required],
        Borrower_City: [formData.Borrower_City || '', Validators.required],
        Borrower_State_Abbrev: [formData.Borrower_State_Abbrev || '', Validators.required],
        Borrower_Zip: [formData.Borrower_Zip || '', Validators.required],
        Borrower_Phone: [new AsYouType('US').input(formData.Borrower_Phone) || '', Validators.required],
        Borrower_Email: [formData.Borrower_Email || '', [Validators.required, Validators.email, Validators.pattern(Patterns.email)]],
        Borrower_DOB: [formData.Borrower_DOB ? this.formatDate(formData.Borrower_DOB) : '', Validators.required],
        Borrower_Preferred_Contact_Ind: [formData.Borrower_Preferred_Contact_Ind || 1, Validators.required],
        Spouse_First_Name: [formData.Spouse_First_Name || ''],
        Spouse_MI: [formData.Spouse_MI || ''],
        Spouse_Last_name: [formData.Spouse_Last_name || ''],
        Spouse_SSN_Hash: [formData.Spouse_SSN_Hash || ''],
        Spouse_Address: [formData.Spouse_Address || ''],
        Spouse_City: [formData.Spouse_City || ''],
        Spouse_State_Abbrev: [formData.Spouse_State_Abbrev || ''],
        Spouse_Zip: [formData.Spouse_Zip || ''],
        Spouse_Phone: [new AsYouType('US').input(formData.Spouse_Phone) || ''],
        Spouse_Email: [formData.Spouse_Email || ''],
        Spouse_Preferred_Contact_Ind: [formData.Spouse_Preferred_Contact_Ind || 0]
      });

      this.borrowerInfoForm.controls['Borrower_SSN_Hash'].updateValueAndValidity();
    }

    if(!this.isLoanEditable()){
      this.borrowerInfoForm.disable();
    }
  }

  setValues(borrowerData: Master_Borrower){
    this.borrowerInfoForm.controls['Borrower_MI'].setValue(borrowerData.Borrower_MI);
    this.borrowerInfoForm.controls['Borrower_First_Name'].setValue(borrowerData.Borrower_First_Name);
    this.borrowerInfoForm.controls['Borrower_Last_Name'].setValue(borrowerData.Borrower_Last_Name);
    this.borrowerInfoForm.controls['Borrower_SSN_Hash'].setValue(borrowerData.Borrower_SSN_Hash);
    this.borrowerInfoForm.controls['Borrower_DL_State_Abbrev'].setValue(borrowerData.Borrower_DL_state);
    this.borrowerInfoForm.controls['Borrower_Dl_Num'].setValue(borrowerData.Borrower_Dl_Num);
    this.borrowerInfoForm.controls['Borrower_Address'].setValue(borrowerData.Borrower_Address);
    this.borrowerInfoForm.controls['Borrower_City'].setValue(borrowerData.Borrower_City);
    this.borrowerInfoForm.controls['Borrower_State_Abbrev'].setValue(borrowerData.Borrower_State_ID);
    this.borrowerInfoForm.controls['Borrower_Zip'].setValue(borrowerData.Borrower_Zip);
    this.borrowerInfoForm.controls['Borrower_Phone'].setValue(borrowerData.Borrower_Phone);
    this.borrowerInfoForm.controls['Borrower_Email'].setValue(borrowerData.Borrower_email);
    this.borrowerInfoForm.controls['Borrower_DOB'].setValue(this.formatDate(borrowerData.Borrower_DOB));
    this.borrowerInfoForm.controls['Borrower_Preferred_Contact_Ind'].setValue(borrowerData.Borrower_Preferred_Contact_ind);
    this.borrowerInfoForm.controls['Borrower_ID_Type'].setValue(borrowerData.Borrower_ID_Type);

    let indexEntityType = this.entityType.findIndex(a => a.key == borrowerData.Borrower_Entity_Type_Code);
    if(indexEntityType > -1) {
      this.borrowerInfoForm.controls['Borrower_Entity_Type_Code'].setValue(borrowerData.Borrower_Entity_Type_Code);
    } else {
      this.borrowerInfoForm.controls['Borrower_Entity_Type_Code'].setValue(BorrowerEntityType.Individual);
    }
  }

  getEntityGroup(entityTypeCode : BorrowerEntityType){
    if(this.orgnaizationEntities.includes(entityTypeCode)){
      return EntityGroup.Organization;
    }else{
      return EntityGroup.Individual;

    }
  }

  onValueChange() {
    if(this.borrowerInfoForm.getRawValue() && this.isLoanEditable()){
      this.borrowerInfo = Object.assign(this.borrowerInfo, this.borrowerInfoForm.getRawValue());

      this.onFormValueChange.emit({
        value : this.borrowerInfo,
        isValid : this.isBorrowerFarmValid()
      });

      this.getAllValidationErrors(this.borrowerInfoForm);
    }
  }

  isBorrowerFarmValid() {
    let Borrower_Entity_Type_Code = this.borrowerInfoForm.controls['Borrower_Entity_Type_Code'].value;

    let Borrower_First_Name = this.borrowerInfoForm.controls['Borrower_First_Name'].valid;
    let Borrower_Last_Name = this.borrowerInfoForm.controls['Borrower_Last_Name'].valid;

    if(Borrower_Entity_Type_Code == 'IND' || Borrower_Entity_Type_Code == 'INS') {
      return Borrower_First_Name && Borrower_Last_Name;
    }
    return Borrower_First_Name;
  }

  formatDate(strDate) {
    if (strDate) {
      let date = new Date(strDate);
      const dateToString = d => `${('00' + (d.getMonth() + 1)).slice(-2)}/${('00' + d.getDate()).slice(-2)}/${d.getFullYear()}`
      return dateToString(date);
    } else {
      return '';
    }
  }

  onEntityTypeChange(data) {

    if(this.individualEntities.includes(data.value)){
      this.borrowerInfoForm.controls['Borrower_ID_Type'].setValue(IDType.SSN);
      this.onValueChange();
    }else if(this.orgnaizationEntities.includes(data.value)){
      this.borrowerInfoForm.controls['Borrower_ID_Type'].setValue(IDType.Tax_ID);
      this.borrowerInfoForm.controls['Borrower_Last_Name'].setValue('');
      this.borrowerInfoForm.controls['Borrower_MI'].setValue('');
      this.onValueChange();
    }else{
      throw "Invalid Borrower_Entity_Type_Code";
    }

     // set Address, City, State, zip
     try {
      let id_type = this.borrowerInfoForm.controls['Borrower_Entity_Type_Code'].value;
      if(id_type == BorrowerEntityType.IndividualWithSpouse) {
        if(!this.borrowerInfoForm.controls.Spouse_Address.value) {
          this.borrowerInfoForm.controls.Spouse_Address.setValue(this.borrowerInfoForm.controls.Borrower_Address.value);
        }

        if(!this.borrowerInfoForm.controls.Spouse_City.value) {
          this.borrowerInfoForm.controls.Spouse_City.setValue(this.borrowerInfoForm.controls.Borrower_City.value);
        }

        if(!this.borrowerInfoForm.controls.Spouse_State_Abbrev.value) {
          this.borrowerInfoForm.controls.Spouse_State_Abbrev.setValue(this.borrowerInfoForm.controls.Borrower_State_Abbrev.value);
        }

        if(!this.borrowerInfoForm.controls.Spouse_Zip.value) {
          this.borrowerInfoForm.controls.Spouse_Zip.setValue(this.borrowerInfoForm.controls.Borrower_Zip.value);
        }
      }

    } catch {}

    this.setSpouseValidation();

  }

  private setSpouseValidation(){

    let id_type = this.borrowerInfoForm.controls['Borrower_Entity_Type_Code'].value;
    this.entityType.map((item) => item.key == id_type);
    let required = id_type == BorrowerEntityType.IndividualWithSpouse ? [Validators.required] : null;

    //Set Validators
    this.borrowerInfoForm.controls.Spouse_First_Name.setValidators(required);
    this.borrowerInfoForm.controls.Spouse_Last_name.setValidators(required);
    this.borrowerInfoForm.controls.Spouse_Phone.setValidators(required);
    this.borrowerInfoForm.controls.Spouse_Preferred_Contact_Ind.setValidators(required);

    //Update Form Group
    this.borrowerInfoForm.controls.Spouse_First_Name.updateValueAndValidity();
    this.borrowerInfoForm.controls.Spouse_Last_name.updateValueAndValidity();
    this.borrowerInfoForm.controls.Spouse_Phone.updateValueAndValidity();
    this.borrowerInfoForm.controls.Spouse_Preferred_Contact_Ind.updateValueAndValidity();

    required = id_type == BorrowerEntityType.IndividualWithSpouse || id_type == BorrowerEntityType.Individual ? [Validators.required] : null;
    this.borrowerInfoForm.controls.Borrower_Last_Name.setValidators(required);
    this.borrowerInfoForm.controls.Borrower_Last_Name.updateValueAndValidity();

    //Rerun Validation
    this.getAllValidationErrors(this.borrowerInfoForm);
  }

  isLoanEditable(){
    return this.borrowerService.isLoanEditable();
  }

  private getAllValidationErrors(formGroup){
    this.validationService.validateFormField(formGroup, Page.borrower, 'app-borrower', 'borrowerInfoForm', 'borrower');
  }

  get showSignerGear() {
    if(this.borrowerInfoForm && this.borrowerInfo) {
      let entityTypeCode = this.borrowerInfoForm.controls['Borrower_Entity_Type_Code'].value;

      return entityTypeCode != 'IND' && entityTypeCode != 'INS' && !this.borrowerInfo['Is_Coborrower_Popup'];
    }
    return false;
  }

  openSignerDetails() {
    let dialogRef = this.dialog.open(BorrowerSignerComponent, {
      height: 'auto',
      panelClass: 'borrowe-signer',
      disableClose: true,
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(res => {
      if(res) {
        this.onValueChange();
      }
    });
  }
}

export enum IDType{
  SSN = 1,
  Tax_ID = 2
}

export enum EntityGroup{
  Individual = 'Individual',
  Organization= 'Organization'
}
