import { Component, OnInit, Input, Inject, OnDestroy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';

import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import {
  loan_model,
  borrower_model,
  BorrowerEntityType,
  LoanStatus
} from '@lenda/models/loanmodel';
import { Page } from '@lenda/models/page.enum';
import { EntityGroup } from './borrower-info-form/borrower-info-form.component';
import { calculatecolumnwidths } from '@lenda/aggriddefinations/aggridoptions';
import { Sync_Status } from '@lenda/models/syncstatusmodel';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { Preferred_Contact_Ind_Options } from '@lenda/Workers/utility/aggrid/preferredcontactboxes';

import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { PublishService } from '@lenda/services/publish.service';
import { BorrowerService } from '../borrower.service';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-borrower-info',
  templateUrl: './borrower-info.component.html',
  styleUrls: ['./borrower-info.component.scss'],
  providers: [BorrowerService]
})
export class BorrowerInfoComponent implements OnInit, OnDestroy {
  @Input() currentPageName: Page;

  @Input('allowIndividualSave')
  allowIndividualSave: boolean;

  @Input('mode')
  mode: string;

  localloanobj: loan_model;
  selectedAssociaionTypeCode: string = '';
  loan_id: number;
  borrowerInfo: any;
  private subscription: ISubscription;

  coborrowers: Array<borrower_model> = [];
  isEditable: boolean = true;

  style: any = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };

  public columnApi: any;
  private gridApi;

  refdata: RefDataModel;

  constructor(
    private fb: FormBuilder,
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public logging: LoggingService,
    public loanapi: LoanApiService,
    private publishService: PublishService,
    public dialog: MatDialog,
    private borrowerService: BorrowerService,
    private alertify: AlertifyService,
    private dataService: DataService
  ) {
    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
  }

  ngOnInit() {
    if (this.mode === 'create' || !this.borrowerInfo) {
      this.borrowerInfo = {};
    }

    this.localloanobj = this.localstorageservice.retrieve(environment.loankey);
    if (this.localloanobj) {
      this.prepateFormAndVariables();
    }

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res) {
        this.localloanobj = res;
        this.prepateFormAndVariables();
      }
    });
  }

  ngOnDestroy() {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    this.adjustToFitAgGrid();
  }

  private prepateFormAndVariables() {
    if (this.localloanobj && this.localloanobj.LoanMaster) {
      this.borrowerInfo = this.localloanobj.LoanMaster;
      this.coborrowers = this.localloanobj.CoBorrower.filter(
        cb => cb.ActionStatus != 3
      );
      this.loan_id = this.localloanobj.LoanMaster.Loan_ID;
    }
  }

  onAddCoBorrower() {
    if (!this.isLoanEditable()) {
      return;
    }

    let borrower = this.getNewBorrowerInstance();
    borrower['Loan_Status'] = this.localloanobj.LoanMaster.Loan_Status;
    borrower['Crop_Year'] = this.localloanobj.LoanMaster.Crop_Year;
    borrower['Is_Coborrower_Popup'] = true;

    const dialogRef = this.dialog.open(CoBorrowerDialogComponent, {
      panelClass: 'borrower-dialog',
      data: { coBorrowerInfo: borrower }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.doneEditingCoborrowerInfo(result);
      }
    });
  }

  onEditCoBorrower(coborrower, index) {
    if (!this.isLoanEditable()) {
      return;
    }

    coborrower['Loan_Status'] = this.localloanobj.LoanMaster.Loan_Status;
    coborrower['Crop_Year'] = this.localloanobj.LoanMaster.Crop_Year;
    coborrower['Is_Coborrower_Popup'] = true;

    const dialogRef = this.dialog.open(CoBorrowerDialogComponent, {
      panelClass: 'borrower-dialog',
      data: { coBorrowerInfo: coborrower }
    });

    dialogRef.afterClosed().subscribe((result: borrower_model) => {
      if (result) {
        if (result.CoBorrower_ID) {
          coborrower.ActionStatus = 2;
        } else {
          coborrower.ActionStatus = 1;
        }
        this.doneEditingCoborrowerInfo(result, index);
      }
    });
  }

  onDeleteCoborrower(coborrower: borrower_model, index) {
    if (!this.isLoanEditable()) {
      return;
    }

    this.alertify
      .confirm('Confirm', 'Please confirm to delete.')
      .subscribe(res => {
        if (res) {
          if (coborrower.CoBorrower_ID) {
            coborrower.ActionStatus = 3;
          } else {
            this.localloanobj.CoBorrower.splice(index, 1);
          }

          this.updateCoBorrowerCount();
          this.localloanobj.srccomponentedit = 'BorrowerComponent';
          this.loanserviceworker.performcalculationonloanobject(
            this.localloanobj,
            false
          );
          this.publishService.enableSync(this.currentPageName);
        }
      });
  }

  updateCoBorrowerCount() {
    if(this.localloanobj.CoBorrower) {
      this.localloanobj.LoanMaster.Co_Borrower_Count = this.localloanobj.CoBorrower.filter(a => a.ActionStatus != 3).length;
    }
  }

  onFormValueChange(data) {
    if (this.localloanobj.LoanMaster) {

      this.setborrowervalues(data.value);
      this.localloanobj.SyncStatus.Status_Borrower = Sync_Status.EDITED;

      // we have fields in Loan Master as well, as many places we are refering Borrower info from LoanMaster
      this.localloanobj.LoanMaster.Borrower_First_Name = data.value.Borrower_First_Name;
      this.localloanobj.LoanMaster.Borrower_MI = data.value.Borrower_MI;
      this.localloanobj.LoanMaster.Borrower_Last_Name = data.value.Borrower_Last_Name;
      this.localloanobj.LoanMaster = Object.assign(this.localloanobj.LoanMaster,data.value);

      this.localloanobj.srccomponentedit = 'BorrowerComponent';
      this.loanserviceworker.performcalculationonloanobject(
        this.localloanobj,
        false
      );

      this.publishService.enableSync(this.currentPageName);
    }
  }

  getNewBorrowerInstance() {
    let borrower = new borrower_model();
    borrower.ActionStatus = 1;
    borrower.Borrower_Entity_Type_Code = BorrowerEntityType.Individual;
    borrower.Loan_Full_ID = this.localloanobj.Loan_Full_ID;
    return borrower;
  }

  doneEditingCoborrowerInfo(
    coborrower: borrower_model,
    index: number = undefined
  ) {
    if (coborrower.CoBorrower_ID) {
      coborrower.ActionStatus = 2;
    } else if (coborrower.ActionStatus != 3) {
      coborrower.ActionStatus = 1;
    }

    if (index == 0 || index) {
      this.localloanobj.CoBorrower.splice(index, 1, coborrower);
    } else {
      if (!this.localloanobj.CoBorrower) {
        this.localloanobj.CoBorrower = [];
      }
      this.localloanobj.CoBorrower.push(coborrower);
    }

    this.updateCoBorrowerCount();

    this.loanserviceworker.performcalculationonloanobject(
      this.localloanobj,
      false
    );

    this.publishService.enableSync(this.currentPageName);
  }

  getBorrowerTypeName(code) {
    return this.borrowerService.getTypeNameOfCB(code);
  }

  isIndividual(borrower: borrower_model) {
    return (
      borrower.Borrower_Entity_Type_Code == BorrowerEntityType.Individual ||
      borrower.Borrower_Entity_Type_Code == BorrowerEntityType.IndividualWithSpouse
    );
  }

  getBorrowerName(borrower: borrower_model) {
    if (borrower.Borrower_Entity_Type_Code == EntityGroup.Organization) {
      return borrower.Borrower_First_Name;
    }
    let name = borrower.Borrower_Last_Name;

    if (name) {
      name += ', ';
    }

    name += + borrower.Borrower_First_Name;

    if (borrower.Borrower_MI) {
      name += ' ' + borrower.Borrower_MI;
    }

    return name;
  }

  getPreferredContactValue(preferenceKey) {
    let selectedPref = Preferred_Contact_Ind_Options.find(
      p => p.key == preferenceKey
    );
    return selectedPref ? selectedPref.value : 'NA';
  }

  adjustToFitAgGrid() {
    this.style.width = calculatecolumnwidths(this.columnApi) + 40 + 'px';
  }

  isLoanEditable() {
    if (this.localloanobj) {
      return this.localloanobj.LoanMaster.Loan_Status == LoanStatus.Working;
    }
    return false;
  }

  setborrowervalues(b: any) {
    this.localloanobj.LoanMaster.Borrower_ID_Type = b.Borrower_ID_Type;
    this.localloanobj.LoanMaster.Borrower_SSN_Hash = b.Borrower_SSN_Hash;
    this.localloanobj.LoanMaster.Borrower_Entity_Type_Code = b.Borrower_Entity_Type_Code;
    this.localloanobj.LoanMaster.Borrower_Last_Name = b.Borrower_Last_Name;
    this.localloanobj.LoanMaster.Borrower_First_Name = b.Borrower_First_Name;
    this.localloanobj.LoanMaster.Borrower_MI = b.Borrower_MI;
    this.localloanobj.LoanMaster.Borrower_Address = b.Borrower_Address;
    this.localloanobj.LoanMaster.Borrower_City = b.Borrower_City;
    this.localloanobj.LoanMaster.Borrower_State_Abbrev = b.Borrower_State_Abbrev;
    this.localloanobj.LoanMaster.Borrower_State = b.Borrower_State;
    this.localloanobj.LoanMaster.Borrower_Zip = b.Borrower_Zip;
    this.localloanobj.LoanMaster.Borrower_Phone = b.Borrower_Phone;
    this.localloanobj.LoanMaster.Borrower_Email = b.Borrower_Email;
    this.localloanobj.LoanMaster.Borrower_DL_State = b.Borrower_DL_State;
    this.localloanobj.LoanMaster.Borrower_DL_State_Abbrev = b.Borrower_DL_State_Abbrev;
    this.localloanobj.LoanMaster.Borrower_DL_Num = b.Borrower_Dl_Num;
    this.localloanobj.LoanMaster.Borrower_DOB = b.Borrower_DOB;
    this.localloanobj.LoanMaster.Borrower_Preferred_Contact_Ind = b.Borrower_Preferred_Contact_Ind;
    this.localloanobj.LoanMaster.Borrower_County_ID = b.Borrower_County_ID;
    this.localloanobj.LoanMaster.Borrower_Lat = b.Borrower_Lat;
    this.localloanobj.LoanMaster.Borrower_Long = b.Borrower_Long;

    if (
      b.Borrower_Entity_Type_Code == BorrowerEntityType.IndividualWithSpouse
    ) {
      this.localloanobj.LoanMaster.Spouse_SSN_Hash = b.Spouse_SSN_Hash;
      this.localloanobj.LoanMaster.Spouse_Last_name = b.Spouse_Last_name;
      this.localloanobj.LoanMaster.Spouse_First_Name = b.Spouse_First_Name;
      this.localloanobj.LoanMaster.Spouse_MI = b.Spouse_MI;
      this.localloanobj.LoanMaster.Spouse_Address = b.Spouse_Address;
      this.localloanobj.LoanMaster.Spouse_City = b.Spouse_City;
      this.localloanobj.LoanMaster.Spouse_State_Abbrev = b.Spouse_State_Abbrev;
      this.localloanobj.LoanMaster.Spouse_State = b.Spouse_State;
      this.localloanobj.LoanMaster.Spouse_Zip = b.Spouse_Zip;
      this.localloanobj.LoanMaster.Spouse_Phone = b.Spouse_Phone;
      this.localloanobj.LoanMaster.Spouse_Email = b.Spouse_Email;
      this.localloanobj.LoanMaster.Spouse_Preferred_Contact_Ind = b.Spouse_Preferred_Contact_Ind;
    }
  }
}

@Component({
  templateUrl: 'coborrower-info.dialog.component.html'
})
export class CoBorrowerDialogComponent {
  coBorrowerInfo: borrower_model;
  isFormValid: boolean;
  isEditable: boolean;

  constructor(
    public dialogRef: MatDialogRef<CoBorrowerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.coBorrowerInfo) {
      this.coBorrowerInfo = data.coBorrowerInfo;
      this.isEditable = this.coBorrowerInfo['Loan_Status'] == 'W';
    }
  }

  onFormValueChange(data) {
    if (data) {
      this.coBorrowerInfo = data.value;
      this.isFormValid = data.isValid;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick() {
    this.dialogRef.close(this.coBorrowerInfo);
  }
}
