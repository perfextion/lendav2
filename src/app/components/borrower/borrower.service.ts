import { Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import { JsonConvert } from 'json2typescript';

import { environment } from '@env/environment.prod';

import { loan_model, BorrowerEntityType, LoanStatus } from '@lenda/models/loanmodel';
import { RefDataModel, LIST_GROUPS } from '@lenda/models/ref-data-model';

import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { ToasterService } from '@lenda/services/toaster.service';

@Injectable()
export class BorrowerService {

  localloanobj: loan_model;
  ref_data: RefDataModel;

  constructor(
    public localstorageservice: LocalStorageService,
    public logging: LoggingService,
    public loanserviceworker: LoancalculationWorker,
    public loanapi: LoanApiService,
    public toasterService: ToasterService
  ) {
    this.localloanobj = this.localstorageservice.retrieve(environment.loankey);
    this.ref_data = this.localstorageservice.retrieve(environment.referencedatakey);
  }

  /**
   * ====
   * Borrower Entity Types
   * ====
   */
  get entityType () {
    if(!this.ref_data) {
      this.ref_data = this.localstorageservice.retrieve(environment.referencedatakey);
    }

    if(this.ref_data && this.ref_data.Ref_List_Items) {
      return this.ref_data.Ref_List_Items.filter(a => a.List_Group_Code == LIST_GROUPS.BORROWER_ENTITY_TYPE).map(entity_type => {
        return {
          key: entity_type.List_Item_Value,
          value: entity_type.List_Item_Name
        }
      });
    } else {
      return [
        { key: BorrowerEntityType.Individual, value: 'Individual' },
        { key: BorrowerEntityType.IndividualWithSpouse, value: 'Individual w/ Spouse' },
        { key: BorrowerEntityType.Partner, value: 'Partner' },
        { key: BorrowerEntityType.Joint, value: 'Joint' },
        { key: BorrowerEntityType.Corporation, value: 'Corporation' },
        { key: BorrowerEntityType.LLC, value: 'LLC' },
        { key: BorrowerEntityType.Limited_Liability_Partnership, value: 'Limited Liability Partnership' },
        { key: BorrowerEntityType.Limited_Partnership, value: 'Limited Partnership' },
        { key: BorrowerEntityType.Trust, value: 'Trust' },
      ];
    }
  }

  public isLoanEditable(){
    return this.localloanobj && this.localloanobj.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  public syncToDb(localloanobject: loan_model) {
    // Loan Condition Calculation
    localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);

    this.loanapi.syncloanobject(localloanobject).subscribe(res => {
      if (res.ResCode == 1) {
        this.localstorageservice.store(environment.modifiedbase, []);
        this.loanapi.getLoanById(localloanobject.Loan_Full_ID).subscribe(res1 => {
          this.logging.checkandcreatelog(3, 'Overview', "APi LOAN GET with Response " + res1.ResCode);
          if (res1.ResCode == 1) {
            this.toasterService.success("Records Synced");
            let jsonConvert: JsonConvert = new JsonConvert();
            this.loanserviceworker.performcalculationonloanobject(jsonConvert.deserialize(res1.Data, loan_model));
            this.loanserviceworker.saveValidationsToLocalStorage(res1.Data);
          }
          else {
            this.toasterService.error("Could not fetch Loan Object from API")
          }
        });
      }
      else {
        this.toasterService.error(res.Message || "Error in Sync");
      }
    });
  }

  public getTypeNameOfCB(cbTypeID: any){
    let entity = this.entityType.find(et => et.key == cbTypeID);
    return entity ? entity.value : '';
  }
}
