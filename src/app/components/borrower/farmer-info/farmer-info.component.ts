import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, OnChanges } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { LocalStorageService } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';
import { AsYouType } from 'libphonenumber-js';
import { ISubscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { environment } from '@env/environment.prod';

import { loan_model, loan_farmer, FarmersList, FarmerDataModel, LoanStatus } from '@lenda/models/loanmodel';
import { Page } from '@lenda/models/page.enum';
import { Preferred_Contact_Ind_Options } from '@lenda/Workers/utility/aggrid/preferredcontactboxes';
import { Masks, Patterns } from '@lenda/shared/shared.constants';
import { RefDataModel, RefChevron, LoanMaster } from '@lenda/models/ref-data-model';
import { Chevron } from '@lenda/models/loan-response.model';
import { Sync_Status } from '@lenda/models/syncstatusmodel';

import { DataService } from '@lenda/services/data.service';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { PublishService } from '@lenda/services/publish.service';
import { Get_Formatted_Date } from '@lenda/services/common-utils';

@Component({
  selector: 'app-farmer-info',
  templateUrl: './farmer-info.component.html',
  styleUrls: ['./farmer-info.component.scss']
})
export class FarmerInfoComponent implements OnInit, OnDestroy, OnChanges {

  emailPattern = "^([a-z0-9._%+-]{4,60})+@([a-z0-9.-]{3,60})+\\.[a-z]{2,4}$";

  private subsription : ISubscription;

  farmerInfoForm: FormGroup;
  localloanobj: loan_model;
  testControl: FormControl = new FormControl();
  stateList: Array<any>;
  loan_id: number;
  farmersList: Array<FarmersList> = new Array<FarmersList>();
  farmer: FarmerDataModel;
  filteredOptions: Observable<FarmersList[]>;
  isSubmitted: boolean; // to enable or disable the sync button as there is not support to un-dirty the form after submit

  @Input('allowIndividualSave')
  allowIndividualSave: boolean;

  @Input('mode')
  mode: string;

  @Input()
  farmerInfo: loan_farmer;

  @Output('onFormValueChange')
  onFormValueChange: EventEmitter<any> = new EventEmitter<any>();

  ssnMask = Masks.ssn;
  dobMask = Masks.date;

  private borrowerChevRon;
  private refdata: RefDataModel;

  Preferred_Contact_Ind_Options = Preferred_Contact_Ind_Options;

  private dataServiceSubscription: ISubscription;

  max_dob: string;
  max_year: number;

  constructor(
    private fb: FormBuilder,
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public logging: LoggingService,
    private loanApiService: LoanApiService,
    private toaster: ToastrService,
    private publishService : PublishService,
    private validationService: ValidationService,
    private dataservice: DataService
  ) {
    if(this.refdata){
      this.borrowerChevRon = (this.refdata as RefDataModel).RefChevrons.find(c => c.Chevron_ID == Chevron.Borrower);
    } else {
      this.borrowerChevRon = <RefChevron>{};
    }

    this.max_dob = Get_Formatted_Date(new Date());
    this.max_year = new Date().getUTCFullYear();
  }

  ngOnInit() {

    this.stateList = this.localstorageservice.retrieve(environment.referencedatakey).StateList;

    if (this.mode === 'create') {
      this.createForm({});
    } else {
      this.localloanobj = this.localstorageservice.retrieve(environment.loankey);

      if (this.localloanobj && this.localloanobj.LoanMaster) {
        this.createForm(this.localloanobj.LoanMaster);
        this.loan_id = this.localloanobj.LoanMaster.Loan_ID;
      }
    }

    this.farmersList = this.localstorageservice.retrieve(environment.masterfarmerlist) || [];

    this.filteredOptions = this.farmerInfoForm.controls['Farmer_First_Name'].valueChanges
      .pipe(
        startWith<string | FarmersList>(''),
        map(value => typeof value === 'string' ? value : value ? value.Farmer_First_Name : ''),
        map(name => name ? this._filter(name) : this.farmersList.filter(a => a.Farmer_SSN_Hash).slice())
      );

    this.subsription= this.farmerInfoForm.controls['Farmer_Phone'].valueChanges.subscribe(
      (selectedValue) => {
        if(this.farmerInfoForm.get('Farmer_Phone').value){
          let str = this.farmerInfoForm.get('Farmer_Phone').value.toString();
          if(new AsYouType('US').input(str) !== str){
            this.farmerInfoForm.controls['Farmer_Phone'].setValue(new AsYouType('US').input(str));
          }
          if(str.length > 14){
            this.farmerInfoForm.controls['Farmer_Phone'].setValue(str.substring(0, 14))
          }
        }
      }
    );

    this.farmerInfoForm.controls['Farmer_State_Abbrev'].valueChanges.subscribe(
      selectedValue => {
        let state = this.stateList.find(a => a.State_Abbrev == selectedValue);

        if(!this.farmerInfoForm.get('Farmer_DL_State_Abbrev').value) {
          this.farmerInfoForm.get('Farmer_DL_State_Abbrev').setValue(selectedValue);

          if(state) {
            this.localloanobj.LoanMaster.Farmer_DL_State = state.State_Name;
          } else {
            this.localloanobj.LoanMaster.Farmer_DL_State = '';
          }
        }

        if(state) {
          this.localloanobj.LoanMaster.Farmer_State = state.State_Name;
        } else {
          this.localloanobj.LoanMaster.Farmer_State = '';
        }
      }
    );

    this.farmerInfoForm.controls['Farmer_DL_State_Abbrev'].valueChanges.subscribe(
      selectedValue => {

        let state = this.stateList.find(a => a.State_Abbrev == selectedValue);
        if(state) {
          this.localloanobj.LoanMaster.Farmer_DL_State = state.State_Name;
        } else {
          this.localloanobj.LoanMaster.Farmer_DL_State = '';
        }
      }
    );

    this.dataServiceSubscription = this.dataservice.getLoanObject().subscribe(res => {
      this.localloanobj = res;
      this.updateForm(this.localloanobj.LoanMaster);
      if(this.farmerInfoForm) {
        if(this.isLoanEditable()) {
          this.farmerInfoForm.enable();
        } else {
          this.farmerInfoForm.disable();
        }

        setTimeout(() => {
          this.getAllValidationErrors(this.farmerInfoForm);
        }, 100);
      }
    });
  }

  ngOnChanges() {
    if(this.farmerInfo && this.farmerInfoForm) {
      this.setValues(this.farmerInfo as any);
    }
  }

  ngAfterViewInit(): void {

    let sourcepath = location.pathname;

    if(!(sourcepath.includes("createloan"))){
      this.farmerInfoForm.markAsTouched();
      this.markFormGroupTouched(this.farmerInfoForm);
    }
  }

  private markFormGroupTouched(formGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
    });
    this.getAllValidationErrors(this.farmerInfoForm);
  };

  onValueChange($event: any) {
    if(this.isLoanEditable()) {
      this.localloanobj.SyncStatus.Status_Farmer = Sync_Status.EDITED;
      this.localloanobj.LoanMaster = Object.assign(this.localloanobj.LoanMaster, this.farmerInfoForm.value);
      this.dataservice.setLoanObject(this.localloanobj);
      this.publishService.enableSync(Page.borrower);
    }
  }

  private getAllValidationErrors(formGroup){
    this.validationService.validateFormField(formGroup, Page.borrower, 'app-farmer', 'farmerInfoForm', 'farmer');
  }

  ngOnDestroy(){
    this.subsription.unsubscribe();
    this.dataServiceSubscription.unsubscribe();
  }

  private _filter(name: string): FarmersList[] {
    const filterValue = name.toLowerCase();
    return this.farmersList.filter(a => a.Farmer_SSN_Hash).filter(option => (option.Farmer_First_Name.toLowerCase() + ' ' + option.Farmer_Last_Name.toLowerCase()).indexOf(filterValue) > -1);
  }

  onSelectionChange(obj){
    this.loanApiService.getCoBorrower(obj.Farmer_ID).subscribe(res => {
      if (res.ResCode == 1) {
        this.farmer = res.Data;
        this.setValues(this.farmer);
      }
    });
  }

  setValues(borrowerData: FarmerDataModel){
    if(this.farmerInfoForm) {
      this.farmerInfoForm.controls['Farmer_MI'].setValue(borrowerData.Farmer_MI);
      this.farmerInfoForm.controls['Farmer_First_Name'].setValue(borrowerData.Farmer_First_Name);
      this.farmerInfoForm.controls['Farmer_Last_Name'].setValue(borrowerData.Farmer_Last_Name);
      this.farmerInfoForm.controls['Farmer_SSN_Hash'].setValue(borrowerData.Farmer_SSN_Hash);
      this.farmerInfoForm.controls['Farmer_DL_State_Abbrev'].setValue(borrowerData.Farmer_DL_State);
      this.farmerInfoForm.controls['Farmer_DL_Num'].setValue(borrowerData.Farmer_DL_Num);
      this.farmerInfoForm.controls['Farmer_Address'].setValue(borrowerData.Farmer_Address);
      this.farmerInfoForm.controls['Farmer_City'].setValue(borrowerData.Farmer_City);
      this.farmerInfoForm.controls['Farmer_State_Abbrev'].setValue(borrowerData.Farmer_State_ID);
      this.farmerInfoForm.controls['Farmer_Zip'].setValue(borrowerData.Farmer_Zip);
      this.farmerInfoForm.controls['Farmer_Phone'].setValue(new AsYouType('US').input(borrowerData.Farmer_Phone));
      this.farmerInfoForm.controls['Farmer_Email'].setValue(borrowerData.Farmer_Email);
      this.farmerInfoForm.controls['Farmer_DOB'].setValue(this.formatDate(borrowerData.Farmer_DOB));
      this.farmerInfoForm.controls['Farmer_Preferred_Contact_Ind'].setValue(borrowerData.Farmer_Pref_Contact_Ind);
      this.farmerInfoForm.controls['Year_Begin_Farming'].setValue(borrowerData.Year_Begin_farming);
      this.farmerInfoForm.controls['Year_Begin_Client'].setValue(borrowerData.Year_Begin_Client);

      this.onValueChange(borrowerData);
    }
  }

  createForm(formData) {

    this.farmerInfoForm = this.fb.group({
      Farmer_First_Name: [formData.Farmer_First_Name || '', Validators.required],
      Farmer_MI: [formData.Farmer_MI || ''],
      Farmer_Last_Name: [formData.Farmer_Last_Name || '', Validators.required],
      Farmer_SSN_Hash: [formData.Farmer_SSN_Hash || '', Validators.required],
      Farmer_DL_State_Abbrev: [formData.Farmer_DL_State_Abbrev || ''],
      Farmer_DL_Num: [formData.Farmer_DL_Num || ''],
      Farmer_Address: [formData.Farmer_Address || '', Validators.required],
      Farmer_City: [formData.Farmer_City || '', Validators.required],
      Farmer_State_Abbrev: [formData.Farmer_State_Abbrev || '', Validators.required],
      Farmer_Zip: [formData.Farmer_Zip || '', Validators.required],
      Farmer_Phone: [new AsYouType('US').input(formData.Farmer_Phone) || '', Validators.required],
      Farmer_Email: [formData.Farmer_Email || '', [Validators.required, Validators.email, Validators.pattern(Patterns.email)]],
      Farmer_DOB: [formData.Farmer_DOB ? this.formatDate(formData.Farmer_DOB) : '', [Validators.required]],
      Year_Begin_Farming: [formData.Year_Begin_Farming || '', Validators.required],
      Year_Begin_Client: [formData.Year_Begin_Client || '', Validators.required],
      Farmer_Preferred_Contact_Ind: [formData.Farmer_Preferred_Contact_Ind ? (formData.Farmer_Preferred_Contact_Ind as number).toString() : '', Validators.required],
    });

    if(!this.isLoanEditable()){
      this.farmerInfoForm.disable();
    }
  }

  updateForm(borrowerData: LoanMaster) {
    if(this.farmerInfoForm) {
      this.farmerInfoForm.controls['Farmer_MI'].setValue(borrowerData.Farmer_MI);
      this.farmerInfoForm.controls['Farmer_First_Name'].setValue(borrowerData.Farmer_First_Name);
      this.farmerInfoForm.controls['Farmer_Last_Name'].setValue(borrowerData.Farmer_Last_Name);
      this.farmerInfoForm.controls['Farmer_SSN_Hash'].setValue(borrowerData.Farmer_SSN_Hash);
      this.farmerInfoForm.controls['Farmer_DL_State_Abbrev'].setValue(borrowerData.Farmer_DL_State_Abbrev);
      this.farmerInfoForm.controls['Farmer_DL_Num'].setValue(borrowerData.Farmer_DL_Num);
      this.farmerInfoForm.controls['Farmer_Address'].setValue(borrowerData.Farmer_Address);
      this.farmerInfoForm.controls['Farmer_City'].setValue(borrowerData.Farmer_City);
      this.farmerInfoForm.controls['Farmer_State_Abbrev'].setValue(borrowerData.Farmer_State_Abbrev);
      this.farmerInfoForm.controls['Farmer_Zip'].setValue(borrowerData.Farmer_Zip);
      this.farmerInfoForm.controls['Farmer_Phone'].setValue(borrowerData.Farmer_Phone);
      this.farmerInfoForm.controls['Farmer_Email'].setValue(borrowerData.Farmer_Email);
      this.farmerInfoForm.controls['Farmer_DOB'].setValue(this.formatDate(borrowerData.Farmer_DOB));
      this.farmerInfoForm.controls['Farmer_Preferred_Contact_Ind'].setValue(borrowerData.Farmer_Preferred_Contact_Ind);
      this.farmerInfoForm.controls['Year_Begin_Farming'].setValue(borrowerData.Year_Begin_Farming);
      this.farmerInfoForm.controls['Year_Begin_Client'].setValue(borrowerData.Year_Begin_Client);
    }
  }

  isFarmerFarmValid() {
    let Farmer_First_Name = this.farmerInfoForm.controls['Farmer_First_Name'].valid;
    let Farmer_Last_Name = this.farmerInfoForm.controls['Farmer_Last_Name'].valid;
    let Farmer_SSN_Hash = this.farmerInfoForm.controls['Farmer_SSN_Hash'].valid;

    return Farmer_First_Name && Farmer_Last_Name && Farmer_SSN_Hash;
  }

  isLoanEditable(){
    return !this.localloanobj || this.localloanobj.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  savedByparentSuccessssCallback = () => {
    this.createForm({});
  }

  formatDate(strDate) {
    if (strDate) {
      let date = new Date(strDate);
      const dateToString = d => `${('00' + (d.getMonth() + 1)).slice(-2)}/${('00' + d.getDate()).slice(-2)}/${d.getFullYear()}`
      return dateToString(date);
    } else {
      return '';
    }
  }

  public validateLength(event) {
    return event.target.value && String(event.target.value).length < 4;
  }
}
