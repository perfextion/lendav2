import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy
} from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { Page } from '@lenda/models/page.enum';

import { loan_model, Borrower_Income_History } from '@lenda/models/loanmodel';

import {
  calculatecolumnwidths,
  IAgGridContext,
  IAgGridComponents
} from '@lenda/aggriddefinations/aggridoptions';
import {
  LoanSettings,
  TableState,
  Chevrons,
  TableAddress
} from '@lenda/preferences/models/loan-settings/index.model';
import {
  getNumericCellEditor,
  numberValueSetter
} from '@lenda/Workers/utility/aggrid/numericboxes';
import {
  setgriddefaults,
  isgrideditable,
  cellclassmaker,
  CellType,
  headerclassmaker
} from '@lenda/aggriddefinations/aggridoptions';
import { currencyFormatter } from '@lenda/aggridformatters/valueformatters';

import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { CropapiService } from '@lenda/services/crop/cropapi.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { PublishService } from '@lenda/services/publish.service';
import { DataService } from '@lenda/services/data.service';

@Component({
  selector: 'app-borrower-income-history',
  templateUrl: './borrower-income-history.component.html',
  styleUrls: ['./borrower-income-history.component.scss']
})
export class BorrowerIncomeHistoryComponent implements OnInit, OnDestroy {
  public refdata: any = {};
  public columnDefs = [];
  private localloanobject: loan_model = new loan_model();
  public rowData = [];
  public context: IAgGridContext<BorrowerIncomeHistoryComponent>;
  public components: IAgGridComponents;
  public gridApi;
  public columnApi;
  public cropYear: number;
  private subscription: ISubscription;

  public frameworkcomponents: IAgGridComponents;
  public style = {
    marginTop: '0px',
    width: '280px',
    boxSizing: 'border-box'
  };

  @ViewChild('myGrid') public gridEl: ElementRef;

  public defaultColDef = {
    enableValue: true,
    enableRowGroup: true,
    enablePivot: true
  };

  constructor(
    public localst: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public cropunitservice: CropapiService,
    public logging: LoggingService,
    public alertify: AlertifyService,
    public loanapi: LoanApiService,
    private publishService: PublishService,
    private dataService: DataService
  ) {
    this.components = { numericCellEditor: getNumericCellEditor() };
    this.refdata = this.localst.retrieve(environment.referencedatakey);

    this.columnDefs = [
      {
        headerName: 'Year',
        headerClass: headerclassmaker(CellType.Integer),
        minWidth: 120,
        width: 120,
        maxWidth: 120,
        cellClass: cellclassmaker(CellType.Text, false),
        field: 'Borrower_Year',
        editable: false
      },
      {
        headerName: 'Revenue',
        field: 'Borrower_Revenue',
        minWidth: 120,
        width: 120,
        maxWidth: 120,
        headerClass: headerclassmaker(CellType.Integer),
        editable: isgrideditable(true),
        cellClass: cellclassmaker(CellType.Integer, true),
        valueFormatter: currencyFormatter,
        valueSetter: numberValueSetter,
        cellEditor: 'numericCellEditor'
      },
      {
        headerName: 'Expenses',
        field: 'Borrower_Expense',
        minWidth: 120,
        width: 120,
        maxWidth: 120,
        headerClass: headerclassmaker(CellType.Integer),
        editable: isgrideditable(true),
        cellClass: cellclassmaker(CellType.Integer, true),
        cellEditor: 'numericCellEditor',
        valueSetter: numberValueSetter,
        valueFormatter: currencyFormatter
      },
      {
        headerName: 'Income',
        field: 'FC_Borrower_Income',
        minWidth: 120,
        width: 120,
        maxWidth: 120,
        headerClass: headerclassmaker(CellType.Integer),
        editable: false,
        cellClass: cellclassmaker(CellType.Integer, false),
        valueFormatter: currencyFormatter,
        cellStyle: { textAlign: 'right' }
      }
    ];

    this.context = { componentParent: this };
  }

  ngOnInit() {
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localloanobject = res;

      if (res.srccomponentedit == 'BorrowerIncomeHistoryComponent') {
        //if the same table invoked the change .. change only the edited row
        this.localloanobject = res;
        this.rowData[res.lasteditrowindex] = this.setData(this.localloanobject.BorrowerIncomeHistory)[res.lasteditrowindex];
      } else {
        this.localloanobject = res;
        this.rowData = [];
        this.rowData = this.setData(this.localloanobject.BorrowerIncomeHistory);
        this.localloanobject.BorrowerIncomeHistory = this.rowData;
      }

      this.gridApi && this.gridApi.refreshCells();
    });

    this.getdataforgrid();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private getdataforgrid() {
    let obj: any = this.localst.retrieve(environment.loankey);
    // this.logging.checkandcreatelog(1, 'Borrower - IncomeHistory', "LocalStorage updated");
    if (obj != null && obj != undefined) {
      this.localloanobject = obj;
      this.rowData = [];
      this.rowData = this.setData(this.localloanobject.BorrowerIncomeHistory);
      this.localloanobject.BorrowerIncomeHistory = this.rowData;
    }

    //this.adjustgrid();
  }

  private setData(params: Array<Borrower_Income_History>) {
    let incomeData = [];
    let incomeHistory = params;
    let years = [];
    this.cropYear = this.localloanobject.LoanMaster.Crop_Year;

    for (let i = 1; i < 6; i++) {
      years.push(this.cropYear - i);
    }

    years.forEach(ye => {
      incomeData.push({
        BIH_ID: 0,
        Borrower_ID: 0,
        Loan_Full_ID: this.localloanobject.Loan_Full_ID,
        Borrower_Year: ye,
        Borrower_Expense: '',
        Borrower_Revenue: '',
        FC_Borrower_Income: '',
        Status: 0,
        ActionStatus: 0
      });
    });

    incomeData.forEach(ih => {
      incomeHistory.forEach(id => {
        if (ih.Borrower_Year === id.Borrower_Year) {
          ih.BIH_ID = id.BIH_ID;
          ih.Borrower_Expense = id.Borrower_Expense;
          ih.Borrower_Revenue = id.Borrower_Revenue;
          ih.FC_Borrower_Income = id.FC_Borrower_Income;
          ih.ActionStatus = id.ActionStatus;
        }
      });
    });

    return incomeData;
  }

  public syncenabled() {
    if (
      this.localloanobject.BorrowerIncomeHistory.filter(
        p => p.ActionStatus == 2
      ).length == 0
    ) {
      return 'disabled';
    } else {
      return '';
    }
  }

  public rowvaluechanged(value: any) {
    let obj = value.data;

    let rowindex = this.localloanobject.BorrowerIncomeHistory.findIndex(
      lc => lc.Borrower_Year == obj.Borrower_Year
    );

    obj.ActionStatus = 2;
    this.localloanobject.BorrowerIncomeHistory[rowindex] = obj;
    this.loanserviceworker.performcalculationonloanobject(this.localloanobject);

    //this shall have the last edit
    this.localloanobject.srccomponentedit = 'BorrowerIncomeHistoryComponent';
    this.localloanobject.lasteditrowindex = value.rowIndex;
    this.publishService.enableSync(Page.borrower);
  }

  public onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    setgriddefaults(this.gridApi, this.columnApi);

    this.gridApi.addEventListener('displayedColumnsChanged', (event) => {
      if (environment.isDebugModeActive) console.time('started');
      let state: any = this.columnApi.getColumnState();

      let obj = JSON.parse(
        this.localloanobject.LoanMaster.Loan_Settings
      ) as LoanSettings;

      if (obj.Table_States == undefined || obj.Table_States == null) {
        obj.Table_States = new Array<TableState>();
      }

      let data = obj.Table_States.find(
        p =>
          p.address.page == Page.borrower &&
          p.address.chevron == Chevrons.borrower_incomehistory
      );

      if (data != undefined) {
        data.data = state;
      } else {
        let farm_farmstate = new TableState();
        farm_farmstate.address = new TableAddress();
        farm_farmstate.address.page = Page.borrower;
        farm_farmstate.address.chevron = Chevrons.borrower_incomehistory;
        farm_farmstate.data = state;
        obj.Table_States.push(farm_farmstate);
      }

      this.localloanobject.LoanMaster.Loan_Settings = JSON.stringify(obj);
      this.dataService.setLoanObjectwithoutsubscription(this.localloanobject);

      if (environment.isDebugModeActive) console.timeEnd('started');
    });

    let loansettingd = JSON.parse(
      this.localloanobject.LoanMaster.Loan_Settings
    ) as LoanSettings;

    if (
      loansettingd.Table_States != undefined &&
      loansettingd.Table_States != null
    ) {
      let data = loansettingd.Table_States.find(
        p =>
          p.address.page == Page.borrower &&
          p.address.chevron == Chevrons.borrower_incomehistory
      );
      if (data != undefined) {
        this.columnApi.setColumnState(data.data);
      }
    }

    this.adjustToFitAgGrid();
  }

  public adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  public displayedColumnsChanged(params) {
    this.adjustToFitAgGrid();
  }
}
