import { Component, OnInit, Input, OnDestroy, AfterContentInit, AfterViewInit } from '@angular/core';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';
import { LoanQResponse, RefQuestions } from '@lenda/models/loan-response.model';
import { loan_model, LoanStatus } from '@lenda/models/loanmodel';
import { Page } from '@lenda/models/page.enum';
import { RefDataModel } from '@lenda/models/ref-data-model';

import { ExceptionWorkerService } from '@lenda/Workers/calculations/exceptionworker.service';
import { MasterService } from '@lenda/master/master.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { DataService } from '@lenda/services/data.service';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { PublishService } from '@lenda/services/publish.service';
import { PubSubService } from '@lenda/services/pubsub.service';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit, AfterContentInit, OnDestroy, AfterViewInit {
  @Input() currentPageName: Page;

  @Input('chevronId')
  chevronId: number;

  @Input('chevronHeader')
  chevronHeader: string;

  @Input('displayAsChildChevron')
  displayAsChildChevron: boolean = false;

  @Input('expanded')
  expanded: boolean = true;

  @Input() policy: string;

  @Input() showAllQuestion: boolean = true;

  @Input() showToggleButton = true;

  public isResponseUpdated: boolean = false;
  public isPublishing: boolean = false;
  public isQuestionOpen: boolean = false;

  private subscription: ISubscription;

  public refdata: RefDataModel;
  public localloanobject: loan_model;
  public questions: Array<RefQuestions> = [];
  private pubSubscription: ISubscription;

  constructor(
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public publishService: PublishService,
    private dataService: DataService,
    private validation: ValidationService,
    private exceptionWorker: ExceptionWorkerService,
    public masterSvc: MasterService,
    private pubSubservice: PubSubService
  ) { }

  ngOnInit() {

    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
    this.subscription = this.dataService.getLoanObject().subscribe(res => {

      if (environment.isDebugModeActive) console.time('question observer');

      this.localloanobject = res;
      this.prepareQuestionsList();
      if (environment.isDebugModeActive) console.timeEnd('question observer');

      this.checkIfQuestion();

      this.validate();
    })

    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);
    this.prepareQuestionsList();

    this.questions.forEach(que => {
      que.isQuestionVisible = true;
      if (que.FC_Response_Detail != undefined) {
        if (que.Choice2 == que.FC_Response_Detail) {
          que.isQuestionVisible = false;
        }
      }
    });

    this.validate();

    this.pubSubscription = this.pubSubservice.subscribeToEvent().subscribe(event => {
      if(event.name == 'event.question.slider.updated' && this.currentPageName == event.data.page) {
        this.showAllQuestion = event.data.value;
      }
    });
  }

  private validate() {
    if (environment.isDebugModeActive) console.time('Question Validation');
    this.validation.validate_Questions(this.questions, this.currentPageName, this.policy);
    if (environment.isDebugModeActive) console.timeEnd('Question Validation');
  }

  public isLoanEditable() {
    return this.localloanobject && this.localloanobject.LoanMaster.Loan_Status === LoanStatus.Working;
  }


  ngAfterContentInit() {
    this.checkIfQuestion();
  }

  ngAfterViewInit() {
    this.questions.forEach(que => {
      let response = this.localloanobject.LoanQResponse.find(res => res.Question_ID == que.Question_ID);
      if (response) {
        que.FC_Response_Detail = response.Response_Detail;
        this.exceptionWorker.LogQuestionExceptionIfApplicable(response, this.localloanobject);
      }
    });
  }

  public questionVisibilityState(que: RefQuestions) {
    let showQuestion = true;
    if (que.FC_Response_Detail != undefined) {
      if (que.Choice2 == que.FC_Response_Detail) {
        showQuestion = false;
      }
    } else {
      showQuestion = true;
    }
    return showQuestion;
  }

  public onDisplayDetailsChange(event) {
    this.showAllQuestion = event.checked;
  }

  private checkIfQuestion() {
    let isAllAnswerChoice2 = true;
    this.questions.forEach(que => {
      if (que.FC_Response_Detail != undefined) {
        if (que.Choice2 != que.FC_Response_Detail) {
          isAllAnswerChoice2 = false;
        }
      }
    });

    this.isQuestionOpen = !isAllAnswerChoice2;

    this.questions.forEach(el => {
      if (el.FC_Response_Detail == undefined || el.Choice2 != el.FC_Response_Detail) {
        this.isQuestionOpen = true;
      }
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.pubSubscription) {
      this.pubSubscription.unsubscribe();
    }
  }

  private prepareQuestionsList() {
    if (this.localloanobject && this.localloanobject.LoanMaster) {
      this.questions = _.sortBy(this.refdata.RefQuestions.filter(que => que.Chevron_ID == this.chevronId), q => q.Sort_Order);

      this.questions.forEach(que => {
        let response = this.localloanobject.LoanQResponse.find(res => res.Question_ID == que.Question_ID);
        if (response) {
          que.FC_Response_Detail = response.Response_Detail;
        } else {
          que.FC_Response_Detail = undefined;
        }

        this.updateSubsidiaryQuestionVisibility(que, this.questions, false);
      });
      this.updatePublishStatus();
    }
  }

  private updatePublishStatus() {
    if (this.localloanobject.LoanQResponse.find(res => res.Chevron_ID == this.chevronId && (res.ActionStatus == 1 || res.ActionStatus == 2))) {
      this.isResponseUpdated = true;
      this.publishService.enableSync(this.currentPageName);
    } else {
      this.isResponseUpdated = false;
    }
  }

  public getVisibility(Parent_Question_ID: number) {
    let matchedParent = this.questions.find(res => res.Question_ID == Parent_Question_ID);
    if (matchedParent) {
      return matchedParent.Choice1 == matchedParent.FC_Response_Detail;
    } else {
      return false;
    }
  }

  public async change(question: RefQuestions) {
    if(!this.isLoanEditable()) {
      return;
    }
    if (environment.isDebugModeActive) console.time('QUESTION EXCEPTION');

    if (question) {
      let matchedResponse = this.localloanobject.LoanQResponse.find(res => res.Question_ID == question.Question_ID);

      if (matchedResponse) {
        matchedResponse.Response_Detail = question.FC_Response_Detail;

        if (matchedResponse.Loan_Q_response_ID) {
          matchedResponse.ActionStatus = 2;
        } else {
          matchedResponse.ActionStatus = 1;
        }
        this.updateLoanMaster(question, matchedResponse);
        this.exceptionWorker.LogQuestionExceptionIfApplicable(matchedResponse, this.localloanobject);
      } else {
        let response = new LoanQResponse();
        response.Question_ID = question.Question_ID;
        response.Chevron_ID = question.Chevron_ID;
        response.Question_Category_Code = question.Questions_Cat_Code;
        response.Loan_ID = this.localloanobject.LoanMaster.Loan_ID;
        response.Loan_Full_ID = this.localloanobject.LoanMaster.Loan_Full_ID;
        response.Loan_Seq_Num = this.localloanobject.LoanMaster.Loan_Seq_num;
        response.Response_Detail = question.FC_Response_Detail;
        response.ActionStatus = 1;
        this.localloanobject.LoanQResponse.push(response);
        this.updateLoanMaster(question, response);
        this.exceptionWorker.LogQuestionExceptionIfApplicable(response, this.localloanobject);
      }

      this.updateSubsidiaryQuestionVisibility(question, this.questions, true);
    }

    if (environment.isDebugModeActive) console.timeEnd('QUESTION EXCEPTION');

    this.isResponseUpdated = true;

    if (environment.isDebugModeActive) console.time('QUESTION STORING IN LOCALSTORAGE');
    this.localloanobject.srccomponentedit = 'QuestionComponent';
    this.loanserviceworker.performcalculationonloanobject(this.localloanobject, true);
    if (environment.isDebugModeActive) console.timeEnd('QUESTION STORING IN LOCALSTORAGE');

    this.publishService.enableSync(this.currentPageName);
  }

  private updateSubsidiaryQuestionVisibility(q: RefQuestions, allQuestions: Array<RefQuestions>, updateActionStatusInd: boolean) {
    let subsidiaryQuestions = allQuestions.filter(a => a.Subsidiary_Question_ID_Ind == 1 && a.Parent_Question_ID == q.Question_ID);
    subsidiaryQuestions.forEach(qu => {
      qu.Subsidiary_Q_Visible = q.FC_Response_Detail == q.Choice1;

      if(!qu.Subsidiary_Q_Visible) {
        qu.FC_Response_Detail = '';
        let response = this.localloanobject.LoanQResponse.find(a => a.Question_ID == qu.Question_ID);
        if(response) {
          response.Response_Detail = '';
          if(response.ActionStatus != 1 && updateActionStatusInd) {
            response.ActionStatus = 2;
          }

          this.exceptionWorker.LogQuestionExceptionIfApplicable(response, this.localloanobject);
        }
      }
    });
  }

  private updateLoanMaster(q: RefQuestions, response: LoanQResponse) {
    //Distance from ARM
    if (q.Question_ID == 2) {
      if (response.Response_Detail) {
        if (parseFloat(response.Response_Detail) > 47) {
          this.localloanobject.LoanMaster.Local_Ind = 0;
        } else {
          this.localloanobject.LoanMaster.Local_Ind = 1;
        }
      } else {
        this.localloanobject.LoanMaster.Local_Ind = 0;
      }
    }

    //Controlled Disbursement
    if (q.Question_ID == 73) {
      if (response.Response_Detail) {
        if (response.Response_Detail == q.Choice1) {
          this.localloanobject.LoanMaster.Controlled_Disbursement_Ind = 1;
        } else {
          this.localloanobject.LoanMaster.Controlled_Disbursement_Ind = 0;
        }
      } else {
        this.localloanobject.LoanMaster.Controlled_Disbursement_Ind = 0;
      }
    }

    //Income Detail
    if(q.Question_ID == 9) {
      if(response.Response_Detail) {
        if (response.Response_Detail == q.Choice1) {
          this.localloanobject.LoanMaster.Income_Detail_Ind = 1;
        } else {
          this.localloanobject.LoanMaster.Income_Detail_Ind = 0;
        }
      } else {
        this.localloanobject.LoanMaster.Income_Detail_Ind = 0;
      }
    }
  }

  public filterNumber(event: KeyboardEvent) {
    const charCode = event.which || event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  public get isAnyQuestionAnswered() {
    try {
      return this.questions.some(a => !!a.FC_Response_Detail);
    } catch {
      return false;
    }
  }
}
