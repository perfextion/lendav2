import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder
} from '@angular/forms';

import { JsonConvert } from 'json2typescript';
import { LocalStorageService } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model, LoanStatus } from '@lenda/models/loanmodel';
import { RefDataModel, RefChevron } from '@lenda/models/ref-data-model';
import { Chevron } from '@lenda/models/loan-response.model';
import {
  Chevrons,
  LoanSettings,
  TableState,
  TableAddress
} from '@lenda/preferences/models/loan-settings/index.model';
import { Page } from '@lenda/models/page.enum';

import {
  getNumericCellEditor,
  numberValueSetter
} from '@lenda/Workers/utility/aggrid/numericboxes';
import {
  currencyFormatter,
  percentageFormatter
} from '@lenda/aggridformatters/valueformatters';
import {
  setgriddefaults,
  calculatecolumnwidths,
  cellclassmaker,
  CellType,
  headerclassmaker,
  isgrideditable
} from '@lenda/aggriddefinations/aggridoptions';

import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { BorrowerapiService } from '@lenda/services/borrower/borrowerapi.service';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { PublishService } from '@lenda/services/publish.service';
import { DataService } from '@lenda/services/data.service';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { MasterService } from '@lenda/master/master.service';
import { ILogger } from '@lenda/Workers/utility/logger';

@Component({
  selector: 'app-balancesheet',
  templateUrl: './balancesheet.component.html',
  styleUrls: ['./balancesheet.component.scss']
})
export class BalancesheetComponent implements OnInit, OnDestroy, AfterViewInit {
  public financialsAsOfForm: FormGroup;
  private localloanobject: loan_model = new loan_model();
  private subscription: ISubscription;

  // Aggrid
  public rowData = [];
  public pinnedBottomRowData = [];
  public getRowClass;
  private gridApi;
  private columnApi;
  public components;

  public date = new FormControl();
  public CPA_Prepared_Financials = false;
  public three_Years_Tax_Return: boolean;
  public defaultDiscounts: any = [];

  public currentDate = new Date();
  public startDate = new Date();

  private balanceSheetChevron;
  private refdata: RefDataModel;

  //region Ag grid Configuration

  style = {
    marginTop: '0px',
    width: '280px',
    boxSizing: 'border-box'
  };

  columnDefs = [
    {
      headerName: 'Asset Class',
      field: 'Financials',
      minWidth: 120,
      width: 120,
      maxWidth: 120
    },
    {
      headerName: 'Asset Value',
      field: 'Assets',
      valueFormatter: currencyFormatter,
      cellEditor: 'numericCellEditor',
      headerClass: headerclassmaker(CellType.Integer),
      cellClass: cellclassmaker(CellType.Integer, true),
      valueSetter: numberValueSetter,
      minWidth: 120,
      width: 120,
      maxWidth: 120,
      editable: params => {
        let result = params.data.Financials !== 'Total';
        return isgrideditable(result);
      }
    },
    {
      headerName: 'Disc %',
      field: 'Discount',
      cellEditor: 'numericCellEditor',
      headerClass: headerclassmaker(CellType.Integer),
      cellClass: cellclassmaker(CellType.Integer, true),
      valueSetter: numberValueSetter,
      minWidth: 120,
      width: 120,
      maxWidth: 120,
      valueFormatter: function(params) {
        if (params.data.Financials !== 'Total') {
          return percentageFormatter(params);
        } else {
          return '';
        }
      },
      editable: params => {
        let result = params.data.Financials !== 'Total';
        return isgrideditable(result);
      }
    },
    {
      headerName: 'Adj Assets Value',
      field: 'AdjValue',
      valueFormatter: currencyFormatter,
      editable: false,
      headerClass: headerclassmaker(CellType.Integer),
      cellClass: cellclassmaker(CellType.Integer, false),
      minWidth: 120,
      width: 120,
      maxWidth: 120
    },
    {
      headerName: 'Debt',
      field: 'Debt',
      valueFormatter: currencyFormatter,
      cellEditor: 'numericCellEditor',
      headerClass: headerclassmaker(CellType.Integer),
      cellClass: cellclassmaker(CellType.Integer, true),
      valueSetter: numberValueSetter,
      minWidth: 120,
      width: 120,
      maxWidth: 120,
      editable: params => {
        let result = params.data.Financials !== 'Total';
        return isgrideditable(result);
      }
    },
    {
      headerName: 'Disc Adj NW',
      field: 'DiscountedNW',
      editable: false,
      headerClass: headerclassmaker(CellType.Integer),
      cellClass: cellclassmaker(CellType.Integer, false),
      minWidth: 120,
      width: 120,
      maxWidth: 120,
      valueFormatter: currencyFormatter
    }
  ];

  private syncloanobjectSub: ISubscription;

  constructor(
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public borrowerservice: BorrowerapiService,
    private toaster: ToastrService,
    public logging: LoggingService,
    private loanapi: LoanApiService,
    private publishService: PublishService,
    private dataService: DataService,
    private fb: FormBuilder,
    private validationService: ValidationService,
    public masterSvc: MasterService
  ) {
    this.startDate.setMonth(this.currentDate.getMonth() - 6);

    this.getRowClass = params => {
      if (params.node.rowPinned) {
        return 'ag-aggregate-row';
      }
    };

    this.components = { numericCellEditor: getNumericCellEditor() };
    this.defaultDiscounts = this.localstorageservice.retrieve(environment.referencedatakey).Discounts;

    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
    if (this.refdata) {
      this.balanceSheetChevron = this.refdata.RefChevrons.find(
        c => c.Chevron_ID == Chevron.Balance_Sheet
      );
    } else {
      this.balanceSheetChevron = <RefChevron>{};
    }
  }

  updaterownode(index, data) {
    let rowNode = this.gridApi.getRowNode(index);
    Object.keys(data).forEach(element => {
      rowNode.setDataValue(element, data[element]);
    });
  }

  private createForm() {
    this.financialsAsOfForm = this.fb.group({
      Financials_Date: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);
    this.startDate.setFullYear(this.localloanobject.LoanMaster.Crop_Year);
    this.createForm();
    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      this.localloanobject = res;
      this.pinnedBottomRowData = [];
      this.CPA_Prepared_Financials = this.localloanobject.LoanMaster.CPA_Prepared_Financials ? true : false;
      this.three_Years_Tax_Return = this.localloanobject.LoanMaster.Borrower_3yr_Tax_Returns ? true : false;
      this.financialsAsOfForm.controls.Financials_Date.setValue(this.getFinancilaDate());
      this.enableDisableForm();

      let rows = this.prepareviewmodel();

      switch (this.localloanobject.srccomponentedit) {
        case 'Balancesheet-Current':
          this.updaterownode(0, rows[0]);
          break;
        case 'Balancesheet-Intermediate':
          this.updaterownode(1, rows[1]);
          break;
        case 'Balancesheet-Fixed':
          this.updaterownode(2, rows[2]);
          break;

        default:
          this.localloanobject.LoanMaster.ActionStatus = 0;
          this.rowData = rows;

          break;
      }
      //this.localloanobject.srccomponentedit = undefined;
      if (this.gridApi != undefined) {
        let params = { force: true };
        this.gridApi.refreshCells(params);
      }

      setTimeout(() => {
        this.validateBalanceSheet();
      }, 100);
    });
    this.getdataforgrid();
  }

  private enableDisableForm() {
    if (this.isLoanEditable()) {
      this.financialsAsOfForm.enable();
    } else {
      this.financialsAsOfForm.disable();
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.validateBalanceSheet();
    }, 100);
  }

  private validateBalanceSheet() {
    Object.values(this.financialsAsOfForm.controls).forEach(control => {
      control.markAsTouched();
    });

    this.validationService.validateFormField(
      this.financialsAsOfForm,
      Page.borrower,
      'app-borrower',
      'financialsAsOfForm',
      this.balanceSheetChevron.Chevron_Code
    );
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.syncloanobjectSub) {
      this.syncloanobjectSub.unsubscribe();
    }
  }

  private getFinancilaDate() {
    let date = this.localloanobject.LoanMaster.Financials_Date || '';
    if (date) {
      date = this.formatDate(date);
      if (date == '1/1/1900') {
        return '';
      } else {
        return date;
      }
    } else {
      return '';
    }
  }

  getdataforgrid() {
    if (this.localloanobject) {
      this.CPA_Prepared_Financials = this.localloanobject.LoanMaster.CPA_Prepared_Financials ? true : false;
      this.three_Years_Tax_Return = this.localloanobject.LoanMaster.Borrower_3yr_Tax_Returns ? true : false;
      this.financialsAsOfForm.controls.Financials_Date.setValue(this.getFinancilaDate());

      this.enableDisableForm();
    }
    this.rowData = this.prepareviewmodel();
  }

  // Ag grid Editing Event
  celleditingstopped(event: any) {
    let financetypeOriginal = event.data.Financials;
    let financetype = '';

    if (financetypeOriginal == 'Intermediate') {
      financetype = 'Inter';
    } else {
      financetype = financetypeOriginal;
    }

    let property = '';
    if (event.colDef.field == 'Discount') {
      property = financetype + '_Assets_Disc_Percent';
    }

    if (event.colDef.field == 'Assets') {
      property = financetype + '_' + event.colDef.field;
    }

    if (event.colDef.field == 'Debt') {
      property = financetype + '_Liabilities';
    }

    this.localloanobject.LoanMaster[property] = parseFloat(event.value);
    this.localloanobject.LoanMaster.ActionStatus = 2;
    this.localloanobject.srccomponentedit = "Balancesheet-" + financetypeOriginal;
      this.logging.checkandcreatelog(3, 'BalanceSheet', "Field Edited -" + property);
    this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
    this.publishService.enableSync(Page.borrower);
  }

  //Aggrid Data Preperation
  prepareviewmodel() {
    //prepare three rows here

    let rows = [];
    if (this.localloanobject.LoanMaster) {
      let loanMaster = this.localloanobject.LoanMaster;
      //1st Current Financial Row
      let currentobj = {
        Financials: 'Current',
        Assets: loanMaster.Current_Assets,
        Discount: loanMaster.Current_Assets_Disc_Percent,
        AdjValue: loanMaster.FC_Current_Adjvalue,
        Debt: loanMaster.Current_Liabilities,
        DiscountedNW: loanMaster.Current_Disc_Net_Worth
      };

      if (currentobj.Discount == 0) {
        currentobj.Discount = _.filter(this.defaultDiscounts, [
          'Discount_Key',
          'CURRENT_DISC'
        ])[0].Discount_Value;
      }

      rows.push(currentobj);

      //1st Intermediate Financial Row
      let Intermediateobj = {
        Financials: 'Intermediate',
        Assets: loanMaster.Inter_Assets,
        Discount: loanMaster.Inter_Assets_Disc_Percent,
        AdjValue: loanMaster.FC_Inter_Adjvalue,
        Debt: loanMaster.Inter_Liabilities,
        DiscountedNW: loanMaster.Inter_Disc_Net_Worth
      };
      if (Intermediateobj.Discount == 0) {
        Intermediateobj.Discount = _.filter(this.defaultDiscounts, [
          'Discount_Key',
          'INTER_DISC'
        ])[0].Discount_Value;
      }
      rows.push(Intermediateobj);

      //1st LongTerm Financial Row
      let LongTermobj = {
        Financials: 'Fixed',
        Assets: loanMaster.Fixed_Assets,
        Discount: loanMaster.Fixed_Assets_Disc_Percent,
        AdjValue: loanMaster.FC_Fixed_Adjvalue,
        Debt: loanMaster.Fixed_Liabilities,
        DiscountedNW: loanMaster.Fixed_Disc_Net_Worth
      };
      if (LongTermobj.Discount == 0) {
        LongTermobj.Discount = _.filter(this.defaultDiscounts, [
          'Discount_Key',
          'FIXED_DISC'
        ])[0].Discount_Value;
      }
      rows.push(LongTermobj);

      //Last Aggregate Row
      let Aggregateobj = {
        Financials: 'Total',
        Assets: loanMaster.Total_Assets,
        Discount: '',
        AdjValue: loanMaster.FC_Total_AdjValue,
        Debt: loanMaster.Total_Liabilities,
        DiscountedNW: loanMaster.Total_Disc_Net_Worth
      };
      this.pinnedBottomRowData.push(Aggregateobj);
    }
    return rows;
  }

  updateLocalStorage() {
    if (this.isLoanEditable()) {
      this.localloanobject.LoanMaster.Financials_Date = this.formatDate(
        this.financialsAsOfForm.controls.Financials_Date.value
      );

      this.localloanobject.LoanMaster.CPA_Prepared_Financials = this.CPA_Prepared_Financials ? 1 : 0;
      this.localloanobject.LoanMaster.Borrower_3yr_Tax_Returns = this.three_Years_Tax_Return ? 1 : 0;
      this.loanserviceworker.performcalculationonloanobject(
        this.localloanobject
      );
      this.publishService.enableSync(Page.borrower);
    }
  }

  formatDate(strDate) {
    if (strDate) {
      let date = new Date(strDate);
      if (isNaN(+date)) {
        return '';
      }
      const dateToString = d => `${('00' + (d.getMonth() + 1)).slice(-2)}/${('00' + d.getDate()).slice(-2)}/${d.getFullYear()}`
      return dateToString(date);
    } else {
      return '';
    }
  }

  synctoDb() {
    this.gridApi.showLoadingOverlay();
    this.localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(this.localloanobject);
    this.syncloanobjectSub = this.loanapi.syncloanobject(this.localloanobject).subscribe(res => {
      if (res.ResCode == 1) {
        this.localstorageservice.store(environment.modifiedbase, []);
        this.loanapi
          .getLoanById(this.localloanobject.Loan_Full_ID)
          .subscribe(res1 => {
            if (res1.ResCode == 1) {
              this.toaster.success('Records Synced');
              let jsonConvert: JsonConvert = new JsonConvert();
              this.loanserviceworker.performcalculationonloanobject(jsonConvert.deserialize(res1.Data, loan_model));
              this.loanserviceworker.saveValidationsToLocalStorage(res1.Data);
            } else {
              this.toaster.error('Could not fetch Loan Object from API');
            }
            this.gridApi.hideOverlay();
          });
      } else {
        this.gridApi.hideOverlay();
        this.toaster.error(res.Message || 'Error in Sync');
      }
    });
  }

  syncenabled() {
    if (!(this.localloanobject.LoanMaster.ActionStatus == 2)) return 'disabled';
    else {
      return '';
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    setgriddefaults(this.gridApi, this.columnApi);

    this.gridApi.addEventListener('displayedColumnsChanged', () => {
      ILogger.time('started');

      let state: any = this.columnApi.getColumnState();
      let obj = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings) as LoanSettings;

      if (obj.Table_States == undefined || obj.Table_States == null) {
        obj.Table_States = [];
      }

      let data = obj.Table_States.find(
        p =>
          p.address.page == Page.borrower &&
          p.address.chevron == Chevrons.borrower_balancesheet
      );

      if (data != undefined) {
        data.data = state;
      } else {
        let farm_farmstate = new TableState();
        farm_farmstate.address = new TableAddress();
        farm_farmstate.address.page = Page.borrower;
        farm_farmstate.address.chevron = Chevrons.borrower_balancesheet;
        farm_farmstate.data = state;
        obj.Table_States.push(farm_farmstate);
      }

      this.localloanobject.LoanMaster.Loan_Settings = JSON.stringify(obj);
      this.dataService.setLoanObjectwithoutsubscription(this.localloanobject);

      ILogger.timeEnd('started');
    });

    let loansettingd = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings) as LoanSettings;

    if (
      loansettingd.Table_States != undefined &&
      loansettingd.Table_States != null
    ) {
      let data = loansettingd.Table_States.find(
        p =>
          p.address.page == Page.borrower &&
          p.address.chevron == Chevrons.borrower_balancesheet
      );

      if (data != undefined) this.columnApi.setColumnState(data.data);
    }

    this.adjustToFitAgGrid();
  }

  adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  displayedColumnsChanged(params) {
    this.adjustToFitAgGrid();
  }

  isLoanEditable() {
    return (
      this.localloanobject &&
      this.localloanobject.LoanMaster.Loan_Status === LoanStatus.Working
    );
  }

  hasData() {
    try {
      return (
        this.localloanobject.LoanMaster.Total_Net_Worth > 0 ||
        this.localloanobject.LoanMaster.Total_Disc_Net_Worth > 0
      );
    } catch {
      return false;
    }
  }
}
