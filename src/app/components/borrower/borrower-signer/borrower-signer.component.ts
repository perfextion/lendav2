import { Component, OnInit, Input, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { ISubscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { Page } from '@lenda/models/page.enum';

import { LoanStatus, loan_model, Borrower_Signer } from '@lenda/models/loanmodel';

import { setgriddefaults, calculatecolumnwidths, cellclassmaker, CellType, isgrideditable, IParam, headerclassmaker, IAgGridContext, IAgGridComponents } from '@lenda/aggriddefinations/aggridoptions';
import { EmailEditor } from '@lenda/Workers/utility/aggrid/emailboxes';
import { SelectEditor } from '@lenda/aggridfilters/selectbox';
import { DeleteButtonRenderer } from '@lenda/aggridcolumns/deletebuttoncolumn';
import { getNumericCellEditor, getPhoneCellEditor, formatPhoneNumberAsYouType } from '@lenda/Workers/utility/aggrid/numericboxes';
import { getAlphaNumericCellEditor } from '@lenda/Workers/utility/aggrid/alphanumericboxes';
import { Preferred_Contact_Ind_Options, PreferredContactFormatter } from '@lenda/Workers/utility/aggrid/preferredcontactboxes';
import { CheckboxCellRenderer } from '@lenda/aggridcolumns/checkbox-cell-editor/checkbox-cell-rendere';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';

import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoggingService } from '@lenda/services/Logs/logging.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { DataService } from '@lenda/services/data.service';

export type BorrowerSignerParams = IParam<BorrowerSignerComponent, Borrower_Signer>;

declare function setmodifiedall(array, keyword): any;
/// <reference path="../../../Workers/utility/aggrid/numericboxes.ts" />

@Component({
  selector: 'app-borrower-signer',
  templateUrl: './borrower-signer.component.html',
  styleUrls: ['./borrower-signer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BorrowerSignerComponent implements OnInit {
  public isAdding: boolean = false;
  public isgriddirty: boolean;
  public indexsedit = [];
  public columnDefs = [];

  private localloanobject: loan_model = new loan_model();

  private subscription: ISubscription;

  @Input()
  public currentPageName: Page;

  //#region Ag grid Configuration

  public rowData = new Array<Borrower_Signer>();
  public components: IAgGridComponents;
  public context: IAgGridContext<BorrowerSignerComponent>;
  public frameworkcomponents: IAgGridComponents;
  public editType: any;
  public pinnedBottomRowData = [];
  private gridApi;
  public columnApi: any;
  private states = [];

  public Ref_Association_Defaults: any = [];
  public Ref_Associations: any = [];

  public style: any = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };

  gridOptions = {
    getRowNodeId: data => `SIGNER_${data.Signer_ID}`
  };

  get tableId() {
    return 'Table_SIGNER';
  }

  get rowId(){
    return 'SIGNER_';
  }

  //#endregion Ag grid Configuration

  constructor(
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public logging: LoggingService,
    public alertify: AlertifyService,
    public loanapi: LoanApiService,
    private dataService: DataService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<BorrowerSignerComponent>
  ) {
    this.frameworkcomponents = {
      emaileditor: EmailEditor,
      selectEditor: SelectEditor,
      deletecolumn: DeleteButtonRenderer,
      checkboxCellRenderer: CheckboxCellRenderer
    };

    this.components = {
      numericCellEditor: getNumericCellEditor(),
      alphaNumericCellEditor: getAlphaNumericCellEditor(),
      phoneCellEditor: getPhoneCellEditor()
    };

    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);
  }

  ngOnInit() {
    this.prepareColDefs();

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
       if(environment.isDebugModeActive) console.time('Signer Observer');

      if (res) {
        this.localloanobject = res;
        if (this.localloanobject.Borrower_Signers) {
          if ( this.localloanobject.srccomponentedit == 'SignerComponent' ) {
            this.rowData[this.localloanobject.lasteditrowindex] = this.localloanobject.Borrower_Signers.filter( p =>
                p.ActionStatus != 3
            )[this.localloanobject.lasteditrowindex];
          } else {
            this.rowData = this.localloanobject.Borrower_Signers.filter(p =>
                p.ActionStatus != 3
            );
          }
        }

        setTimeout(() => {
          setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), this.rowId);
        }, 5);
      }

       if(environment.isDebugModeActive) console.timeEnd('Signer Observer');
    });
  }

  ngOnDestroy() {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }

  public onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    //these two methods remove side option toolbar , sets column , removes header menu
    setgriddefaults(this.gridApi, this.columnApi);
    this.adjustToFitAgGrid();
  }

  public adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  public displayedColumnsChanged(params){
    this.adjustToFitAgGrid();
  }

  prepareColDefs() {
    this.columnDefs = [
      {
        headerName: 'Name',
        field: 'Signer_Name',
        minWidth: 180,
        width: 180,
        maxWidth: 180,
        tooltip: params => params.data['Signer_Name'],
        cellClass: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          let iseditable = params.data.ActionStatus != -1;
          return cellclassmaker(CellType.Text, iseditable, '', status);
        },
        editable: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          let iseditable = params.data.ActionStatus != -1;
          return isgrideditable(iseditable, status);
        },
        cellEditor: 'agTextCellEditor'
      },
      {
        headerName: 'Location',
        field: 'Location',
        minWidth: 120,
        width: 120,
        maxWidth: 120,
        tooltip: params => params.data['Location'],
        cellClass: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          let iseditable = params.data.ActionStatus != -1;
          return cellclassmaker(CellType.Text, iseditable, '', status);
        },
        editable: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          let iseditable = params.data.ActionStatus != -1;
          return isgrideditable(iseditable, status);
        },
        cellEditor: 'agTextCellEditor'
      },
      {
        headerName: 'Phone',
        field: 'Phone',
        minWidth: 120,
        width: 120,
        maxWidth: 120,
        tooltip: params => params.data['Phone'],
        cellClass: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          let iseditable = params.data.ActionStatus != -1;
          return cellclassmaker(CellType.Text, iseditable, '', status);
        },
        editable: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          let iseditable = params.data.ActionStatus != -1;
          return isgrideditable(iseditable, status);
        },
        headerClass: headerclassmaker(CellType.Text),
        cellEditor: 'phoneCellEditor',
        valueFormatter: formatPhoneNumberAsYouType
      },
      {
        headerName: 'Email',
        field: 'Email',
        minWidth: 120,
        width: 120,
        maxWidth: 120,
        tooltip: params => params.data['Email'],
        cellClass: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          let iseditable = params.data.ActionStatus != -1;
          return cellclassmaker(CellType.Text, iseditable, '', status);
        },
        editable: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          let iseditable = params.data.ActionStatus != -1;
          return isgrideditable(iseditable, status);
        },
        cellEditor: 'agTextCellEditor'
      },
      {
        headerName: 'Pref Contact',
        width: 120,
        minWidth: 120,
        maxWidth: 120,
        field: 'Preferred_Contact_Ind',
        cellClass: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          let iseditable = params.data.ActionStatus != -1;
          return cellclassmaker(CellType.Text, iseditable, '', status);
        },
        editable: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          let iseditable = params.data.ActionStatus != -1;
          return isgrideditable(iseditable, status);
        },
        cellEditor: 'selectEditor',
        cellEditorParams: { values: Preferred_Contact_Ind_Options },
        valueFormatter: PreferredContactFormatter
      },
      {
        headerName: 'Title',
        field: 'Title',
        minWidth: 120,
        width: 120,
        maxWidth: 120,
        tooltip: params => params.data['Title'],
        cellClass: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          let iseditable = params.data.ActionStatus != -1;
          return cellclassmaker(CellType.Text, iseditable, '', status);
        },
        editable: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          let iseditable = params.data.ActionStatus != -1;
          return isgrideditable(iseditable, status);
        },
        cellEditor: 'agTextCellEditor'
      },
      {
        headerName: 'SBI Ind',
        width: 80,
        minWidth: 80,
        maxWidth: 80,
        field: 'SBI_Ind',
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Integer, true, '', status);
        },
        cellRenderer: 'checkboxCellRenderer',
        cellRendererParams: (params: BorrowerSignerParams) =>{
          return {
            type: 'SBI_Ind',
            edit: params.context.componentParent.localloanobject.LoanMaster.Loan_Status == 'W' && params.data.ActionStatus != -1
          }
        }
      },
      {
        headerName: 'Signer Ind',
        width: 80,
        minWidth: 80,
        maxWidth: 80,
        field: 'Signer_Ind',
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Integer, true, '', status);
        },
        cellRenderer: 'checkboxCellRenderer',
        cellRendererParams: (params: BorrowerSignerParams) =>{
          return {
            type: 'Signer_Ind',
            edit: params.context.componentParent.localloanobject.LoanMaster.Loan_Status == 'W' && params.data.ActionStatus != -1
          }
        }
      },
      {
        headerName: 'Guarantor Ind',
        width: 80,
        minWidth: 80,
        maxWidth: 80,
        field: 'Guarantor_Ind',
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: (params: BorrowerSignerParams) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Integer, true, '', status);
        },
        cellRenderer: 'checkboxCellRenderer',
        cellRendererParams: (params: BorrowerSignerParams) =>{
          return {
            type: 'Guarantor_Ind',
            edit: params.context.componentParent.localloanobject.LoanMaster.Loan_Status == 'W' && params.data.ActionStatus != -1
          }
        }
      },
      {
        headerName: '',
        field: 'value',
        minWidth: 60,
        width: 60,
        maxWidth: 60,
        suppressToolPanel: true,
        cellRendererSelector: function(params) {
          let iseditable = params.context.componentParent.localloanobject.LoanMaster.Loan_Status == 'W';
          if(params.data.ActionStatus == -1 || !iseditable) {
            return null;
          }
          return {
            component: 'deletecolumn'
          };
        }
      }
    ];

    this.context = {
      componentParent: this,
      states: this.states,
      names: this.Ref_Association_Defaults
    };

    this.getdataforgrid();
    this.editType = '';
  }

  public MethodFromCheckbox(params, type, $event) {
    this.Set_Ind(type, $event.srcElement.checked, params)
  }

  private Set_Ind(field: string, isChecked: boolean, params) {
    let signer = params.data;

    if(isChecked) {
      signer[field] = 1;
    } else {
      signer[field] = 0;
    }

    if(signer.ActionStatus != 1) {
      signer.ActionStatus = 2;
    }

    this.localloanobject.srccomponentedit = 'SignerComponent';
    this.localloanobject.lasteditrowindex = params.rowIndex;

    this.loanserviceworker.performcalculationonloanobject(this.localloanobject, false);
  }


  private getdataforgrid() {
    if (this.localloanobject != null && this.localloanobject != undefined) {
      if (this.localloanobject.Borrower_Signers) {
        this.rowData = this.localloanobject.Borrower_Signers.filter(
          p =>
            p.ActionStatus != 3
        );
      } else {
        this.rowData = null;
      }
    }
  }

  public rowvaluechanged(params: any) {
    let column = params.colDef.field;

    //#region MODIFIED YELLOW VALUES

    let modifiedvalues = this.localstorageservice.retrieve(environment.modifiedbase) as Array<String>;

    if (!modifiedvalues.includes(this.rowId + params.data.Signer_ID + "_" + params.colDef.field)) {
      modifiedvalues.push(this.rowId + params.data.Signer_ID + "_" + params.colDef.field);
      this.localstorageservice.store(environment.modifiedbase, modifiedvalues);
    }

    let obj = params.data as Borrower_Signer;
    if (obj.ActionStatus != 1) {
      obj.ActionStatus = 2;
    }

    this.localloanobject.srccomponentedit = 'SignerComponent';
    this.localloanobject.lasteditrowindex = params.rowIndex;

    this.loanserviceworker.performcalculationonloanobject(this.localloanobject, false);
  }

  //Grid Events
  public addrow() {
    if(!this.isLoanEditable()) {
      return;
    }
    if(this.localloanobject.Borrower_Signers) {
      let newRowWithoutName = this.localloanobject.Borrower_Signers.filter(a => {
        return a.ActionStatus == 1 && !a.Signer_Name
      });

      if(newRowWithoutName.length > 0) {
        this.alertify.alert('Alert',  'You already have a blank added row.');
        return;
      }
    }

    let newItem = new Borrower_Signer();
    this.isAdding = true;
    newItem.ActionStatus = 1;
    newItem.Signer_ID = getRandomInt(Math.pow(10, 6), Math.pow(10, 9));
    newItem.Loan_Full_ID = this.localloanobject.Loan_Full_ID;
    newItem.Preferred_Contact_Ind = 0;
    newItem.Guarantor_Ind = 0;
    newItem.SBI_Ind = 0;
    newItem.Signer_Ind = 0;
    newItem.Borrower_ID = this.localloanobject.LoanMaster.Borrower_ID;

    this.gridApi.updateRowData({ add: [newItem] });

    this.gridApi.startEditingCell({
      rowIndex: this.rowData.length,
      colKey: 'Signer_name'
    });

    if(!this.localloanobject.Borrower_Signers) {
      this.localloanobject.Borrower_Signers = [];
    }


    this.localloanobject.Borrower_Signers.push(newItem);
    this.rowData.push(newItem);
    this.adjustToFitAgGrid();
  }

  public DeleteClicked(rowIndex: any, data: Borrower_Signer) {
    this.alertify.deleteRecord().subscribe(res => {
      if (res == true) {
        let localObjIndex = this.localloanobject.Borrower_Signers.findIndex(
          assoc => assoc.Signer_ID == data.Signer_ID
        );

        if (localObjIndex > -1) {
          if (data.ActionStatus == 1) {
            this.localloanobject.Borrower_Signers.splice(localObjIndex, 1);
          } else {
            data.ActionStatus = 3;
          }

          this.localloanobject.srccomponentedit = undefined;
          this.localloanobject.lasteditrowindex = undefined;
        }

        this.loanserviceworker.performcalculationonloanobject(this.localloanobject, false);
        this.isAdding = false;
      }
    });
  }

  public isLoanEditable(){
    return this.localloanobject && this.localloanobject.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  public onOkClick() {
    if(this.isLoanEditable()) {
      this.dialogRef.close(true);
    }
  }

  public onCancelClick() {
    this.dialogRef.close(false);
  }
}
