import { Injectable } from '@angular/core';

import { finalize } from 'rxjs/operators';
import { LocalStorageService } from 'ngx-webstorage';
import { JsonConvert } from 'json2typescript';

import { LoggingService } from '@lenda/services/Logs/logging.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { ToasterService } from '@lenda/services/toaster.service';
import { loan_model } from '@lenda/models/loanmodel';
import { GlobalService } from '@lenda/services/global.service';
import { PublishService } from '@lenda/services/publish.service';

import { Loan_Farm } from '@lenda/models/farmmodel.';
import { environment } from '@env/environment.prod';
import { RatedValues, RentUOMValues, PermToInsuranceValues, OwnedValues } from './farm.model';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';

@Injectable()
export class FarmService {
  private refData: RefDataModel;

  constructor(
    public localstorageservice: LocalStorageService,
    public logging: LoggingService,
    public loanserviceworker: LoancalculationWorker,
    public loanapi: LoanApiService,
    public toasterService: ToasterService
    ) {
    this.refData = this.localstorageservice.retrieve(environment.referencedatakey);

    this.localstorageservice.observe(environment.referencedatakey).subscribe(res => {
      this.refData = res;
    });
  }

  syncToDb(localloanobject: loan_model) {
    // Loan Condition Calculation
    localloanobject = this.loanserviceworker.setValidationErrorsBeforeSave(localloanobject);
    PublishService.isSyncInProgress = true;

    this.loanapi.syncloanobject(localloanobject).subscribe(synsResponse => {
      if (synsResponse.ResCode == 1) {
        this.localstorageservice.store(environment.modifiedbase, []);
        this.loanapi.getLoanById(localloanobject.Loan_Full_ID)
        .pipe(
          finalize(() =>{
            PublishService.isSyncInProgress = false;
          })
        )
          .subscribe(res => {
            this.logging.checkandcreatelog(3, 'Overview', 'APi LOAN GET with Response ' + res.ResCode );
            if (res.ResCode == 1) {
               this.toasterService.success('Records Synced');

              let jsonConvert: JsonConvert = new JsonConvert();
              this.loanserviceworker.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model));
              this.loanserviceworker.saveValidationsToLocalStorage(res.Data);
              new GlobalService(this.localstorageservice).updateLocalStorage('FRM_');
              new GlobalService(this.localstorageservice).updateLocalStorage('LLD_');
            } else {
              this.toasterService.error('Could not fetch Loan Object from API');
            }
          });
      } else {
        this.toasterService.error(synsResponse.Message || 'Error in Sync');
      }
    });
  }

  getValidFarms(data: Loan_Farm[] = [], loan: loan_model) {
    let rows: Loan_Farm[] = [];

    data.forEach((row, index) => {
      rows.push(this.validateFarm(row, index, loan, rows));
    });

    const deletedFarms = this.deleted_farms(loan, rows);
    const totalFarms = [...rows, ...deletedFarms];

    return totalFarms;
  }

  private validateFarm(farm: Loan_Farm, index: number, loan: loan_model, allFarms: Loan_Farm[]) {
    try {
        // validate farm id
        if(farm.Farm_ID) {
          if(!loan.Farms.some(a => a.Farm_ID == farm.Farm_ID)){
            throw new Error("Invalid Farm Id");
          } else {
            if(allFarms.some(a => this.farmKey(a) == this.farmKey(farm))){
              throw new Error("Duplicate Farm");
            }
            farm.ActionStatus = 2;
            farm.Loan_Full_ID = loan.Loan_Full_ID;
          }
        } else {
          if(allFarms.some(a => this.farmKey(a) == this.farmKey(farm))){
            throw new Error("Duplicate Farm");
          } else {
            farm.Loan_Full_ID = loan.Loan_Full_ID;
            farm.Farm_ID = getRandomInt(Math.pow(10, 5), Math.pow(10, 10));
            farm.ActionStatus = 1;
          }
        }

        // validate state
        let state = this.refData.StateList.find(a => a.State_Abbrev == farm.FC_State_Code)
        if(!state) {
          throw new Error("Invalid State Id");
        } else {
          farm.Farm_State_ID = state.State_ID;
        }

        // validate county
        let county = this.refData.CountyList.find(a => a.County_Name == farm.FC_County_Name && a.State_ID == farm.Farm_State_ID)
        if(!county){
          throw new Error("Invalid County Id");
        } else {
          farm.Farm_County_ID = county.County_ID;
        }

        // validate Rated
        if(farm.Rated) {
          if(!RatedValues.some(a => a.key == farm.Rated)) {
            throw new Error("Invalid Rated Value");
          }
        }

        // convert to numbers
        if(isNaN(parseFloat(farm.Irr_Acres as any))) {
          throw new Error("Value must be number for Irr acres.");
        } else {
          farm.Irr_Acres = parseFloat(String(farm.Irr_Acres));
        }

        if(isNaN(parseFloat(farm.NI_Acres as any))) {
          throw new Error("Value must be number for NI acres.");
        } else {
          farm.NI_Acres = parseFloat(String(farm.NI_Acres));
        }

        if(!OwnedValues.some(a => a.key == farm.Owned)) {
          throw new Error("Invalid Owned Value");
        }

        // owned
        farm.Owned = parseFloat(String(farm.Owned));

        // production percent
        farm.Percent_Prod = parseFloat(String(farm.Percent_Prod));
        if( farm.Percent_Prod > 100 ||  farm.Percent_Prod < 0){
          throw new Error("Invalid Production Percent Value");
        }

        farm.Display_Rent = parseFloat(String(farm.Display_Rent));

        if(farm.Owned == 1) {
          farm.Percent_Prod = 100;
          farm.Display_Rent = 0;
          farm.Rent_UOM  = 1;
          farm.Cash_Rent_Due_Date = '';
          farm.Cash_Rent_Waived = 0;
          farm['Share_Rent'] = 0;
          farm.Permission_To_Insure = 1;
          farm.Landowner = '';
        } else {
          farm['Share_Rent'] = 100 - farm.Percent_Prod;
        }

        // Validation LLD
        if(farm.Owned == 0) {
          let LLD = loan.Association.filter(a => a.ActionStatus != 3)
          .find(a => a.Assoc_Name == farm.Landowner);

          if(!LLD) {
            throw new Error('Landowner does not exists.');
          }
        }

        // Rent $ UOM
        farm.Rent_UOM = parseFloat(String(farm.Rent_UOM));

        if(farm.Owned == 0) {
          if(farm.Display_Rent > 0 && !RentUOMValues.some(a => a.key == farm.Rent_UOM)){
            throw new Error("Invalid Rent UOM");
          }
        } else {
          farm.Rent_UOM = 1;
        }

        // Rent Due Date
        if(farm.Owned == 0) {
          if(farm.Display_Rent > 0 && !!farm.Cash_Rent_Due_Date && isNaN(+new Date(farm.Cash_Rent_Due_Date))){
            throw new Error("Invalid Rent Due Date");
          }
        } else {
          farm.Cash_Rent_Due_Date = '';
        }

        // waived amount
        if(farm.Owned == 0){
          farm.Cash_Rent_Waived_Amount = parseFloat(String(farm.Cash_Rent_Waived_Amount || 0));
        } else {
          farm.Cash_Rent_Waived_Amount = 0;
        }

        // Rent %
        farm['Share_Rent'] = parseFloat(String(farm['Share_Rent'] || 0));
        if(farm.Owned == 0 && farm['Share_Rent'] > 100 ||  farm.Percent_Prod < 0){
          throw new Error("Invalid Rent Percent Value");
        } else {
          farm['Share_Rent'] = 0;
        }

        // PTI
        if(!PermToInsuranceValues.some(a => a.key == farm.Permission_To_Insure)){
          throw new Error("Invalid Permission to insurance Value");
        }

        if(farm.Owned == 1) {
          if(farm.Permission_To_Insure != 1) {
            throw new Error("Invalid Permission to insurance Value");
          }
        }
      return farm;
    } catch (ex) {
      throw new Error(`Row ${index + 1} has error - ${ex.message}`);
    }

  }

  private farmKey(row: Loan_Farm) {
    return `${row.Farm_State_ID}_${row.Farm_County_ID}_${row.FSN}`;
  }

  private deleted_farms(loan: loan_model, rows: Loan_Farm[]){
    let deleteFarms: Loan_Farm[] = [];

    try {
      loan.Farms.forEach(a => {
        let farm = rows.find(x => x.Farm_ID == a.Farm_ID);

        if(!farm){
          a.ActionStatus = 3;

          deleteFarms.push(a);

          loan.LoanCropUnits.filter(x => x.Farm_ID == a.Farm_ID).forEach(x => {
            x.ActionStatus = 3;
          });

          loan.LoanPolicies
          .filter(x => x.County_ID == a.Farm_County_ID && x.State_ID == a.Farm_State_ID)
          .forEach(x => {
            x.ActionStatus = 3;
          });

          loan.AphUnits.filter(x => x.County_ID == a.Farm_County_ID && x.State_ID == a.Farm_State_ID)
          .forEach(x => {
            x.ActionStatus = 3;
          });
        }
      });

      return deleteFarms;
    } catch {
      return [];
    }

  }

  getColumnsToCopy(colDef: Array<any>, columnName: string){
    let index = colDef.findIndex(a => a.field == columnName);
    return colDef.slice(0, index);
  }

  copyrow(from_row: Loan_Farm, to_farm: Loan_Farm, cols_to_copy: Array<any>) {
    cols_to_copy.filter(a => !a.hide).filter(a => !a.supressCopy).forEach(col => {
      let field = col.field;

      if(field == 'FC_State_Code') {
        to_farm[field] = from_row[field];
        to_farm.Farm_State_ID = parseInt(from_row.Farm_State_ID as any);
        return;
      }

      if(field == 'FC_County_Name') {
        to_farm[field] = from_row[field];
        to_farm.Farm_County_ID = parseInt(from_row.Farm_County_ID as any);
        return;
      }

      if(field == 'Owned' || field == 'Rent_UOM') {
        to_farm[field] = from_row[field];
        return;
      }

      if(to_farm[field] == null || String(to_farm[field]) === '') {
        to_farm[field] = from_row[field];
      }
    });

    return to_farm;
  }
}
