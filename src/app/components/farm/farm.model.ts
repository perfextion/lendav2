export const OwnedValues = [{ key: 1, value: 'Yes' }, { key: 0, value: 'No' }];

export const RatedValues = [
  { key: '', value: '' },
  { key: 'AAA', value: 'AAA' },
  { key: 'BBB', value: 'BBB' }
];

export const RentUOMValues = [
  { key: 1, value: '$ / acre' },
  { key: 2, value: '$ Total' }
];

export const PermToInsuranceValues = [
  { key: 1, value: 'Yes' },
  { key: 0, value: 'No' }
];
