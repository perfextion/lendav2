import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';

import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import { ISubscription } from 'rxjs/Subscription';

import { Page } from '@lenda/models/page.enum';
import { environment } from '@env/environment.prod';
import {
  FarmOverrideModel,
  loan_model,
  LoanStatus
} from '@lenda/models/loanmodel';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { ColumnsDisplaySettings } from '@lenda/preferences/models/loan-settings/columns-display.model';

import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { PublishService } from '@lenda/services/publish.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { Loan_Farm } from '@lenda/models/farmmodel.';

@Component({
  selector: 'app-manage-override-override-dialog',
  templateUrl: './manage-farm-override-dialog.component.html',
  styleUrls: ['./manage-farm-override-dialog.component.scss'],
  providers: [PublishService]
})
export class ManageFarmOverrideDialogComponent implements OnInit, OnDestroy {
  public farmOverrideForm: FormGroup;
  public irrCrops: any;
  public nirCrops: any;

  public hasCrops: boolean = false;
  public loanObj: loan_model;
  public refData: RefDataModel;
  public fcOverride: any;

  public customMaskConfig = {
    align: 'right',
    allowNegative: false,
    allowZero: true,
    decimal: '.',
    precision: 1,
    prefix: '',
    suffix: '%',
    thousands: ',',
    nullable: false
  };

  // The variable that will be passed to the Override API
  public farmOverrideData: any[];
  public farmOverrideDataCollection: any[] = [];

  private preferencesubscription: ISubscription;
  public columnsDisplaySettings: ColumnsDisplaySettings;

  public isLoanEditable: boolean;

  constructor(
    private fb: FormBuilder,
    private localst: LocalStorageService,
    private loanserviceworker: LoancalculationWorker,
    private publishService: PublishService,
    public dialogRef: MatDialogRef<ManageFarmOverrideDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private settingsService: SettingsService
  ) {
    this.loanObj = this.localst.retrieve(environment.loankey);
    this.refData = this.localst.retrieve(environment.referencedatakey);
    this.isLoanEditable = this.loanObj.LoanMaster.Loan_Status == LoanStatus.Working;
  }

  ngOnInit() {
    this.farmOverrideForm = this.fb.group({
      irrAcres: this.fb.array([]),
      nirAcres: this.fb.array([])
    });

    let farm = this.loanObj.Farms.find(a => a.Farm_ID == this.data.Farm_ID);

    this.irrCrops = <FormArray>this.farmOverrideForm.get('irrAcres');
    this.nirCrops = <FormArray>this.farmOverrideForm.get('nirAcres');

    this.getUserPreferences();

    if (
      !_.isEmpty(this.loanObj) &&
      !_.isEmpty(this.loanObj.LoanCropUnits) &&
      !_.isEmpty(this.refData.CropList)
    ) {
      this.setInitialData(farm);
    }

    if (_.isEmpty(this.farmOverrideDataCollection)) {
      this.farmOverrideDataCollection = [];
    }

    if (this.loanObj.LoanMaster.Loan_Status != LoanStatus.Working) {
      this.farmOverrideForm.disable();
    }
  }

  private getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;

    this.preferencesubscription = this.settingsService.preferencesChange.subscribe(
      preferences => {
        this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
      }
    );
  }

  private setInitialData(farm: Loan_Farm) {
    this.farmOverrideDataCollection = this.loanObj.FarmCropOverride;

    this.farmOverrideDataCollection.forEach(elOverride => {
      elOverride.Status = 2;
    });

    this.loanObj.LoanCropUnits.forEach(cy => {
      this.refData.CropList.forEach(cl => {
        if (cy.Crop_Practice_ID == cl.Crop_And_Practice_ID) {
          let rentPercentage =  this.data.Owned == 1 ? 0 : 100 - this.data.Percent_Prod;

          let exstingFCOverride = _.find(
            this.farmOverrideDataCollection,
            elSelectedOverride => {
              return (
                elSelectedOverride.Crop_Code == cl.Crop_Full_Key &&
                elSelectedOverride.Farm_ID == this.data.Farm_ID
              );
            }
          );

          if (cy.Farm_ID == this.data.Farm_ID) {
            let optionData = this.fb.group({
              Farm_ID: this.data.Farm_ID,
              Loan_Full_ID: this.loanObj.Loan_Full_ID,
              crop: cl.Crop_Name,
              crop_type: cl.Crop_Type_Code,
              irr_prac: cl.Irr_Prac_Code,
              crop_prac: cl.Crop_Prac_Code,
              Crop_Code: cl.Crop_Full_Key,
              Share_Rent_Override: _.isEmpty(exstingFCOverride) ? true : exstingFCOverride.Share_Rent_Override,
              Share_Rent_Percent: _.isEmpty(exstingFCOverride) ? rentPercentage : exstingFCOverride.Share_Rent_Percent,
              Status: _.isEmpty(exstingFCOverride) ? 1 : 2
            } as FarmOverrideModel);

            if (farm.Owned == 1) {
              optionData.controls['Share_Rent_Percent'].disable();
            }

            switch (cl.Irr_Prac_Code.toUpperCase()) {
              case 'IRR':
                this.irrCrops.push(optionData);
                break;

              case 'NI':
                this.nirCrops.push(optionData);
                break;

              default:
                break;
            }
          }
        }
      });
    });
  }

  private setFarmCropOverrideCollection() {
    this.formatToCropDetailData();

    // CHECK IF LOAN HAS NO FARM OVERRIDE YET AND CREATES A RECORD
    if (_.isEmpty(this.farmOverrideDataCollection)) {
      this.farmOverrideData.forEach(el => {
        this.farmOverrideDataCollection.push(el);
      });
    } else {
      // CHECK IF CROP IS ALREADT EXISTING
      this.farmOverrideDataCollection.forEach(elOverride => {
        this.farmOverrideData.forEach(elSOverride => {
          if (
            elOverride.Crop_Code == elSOverride.Crop_Code &&
            elOverride.Farm_ID == elSOverride.Farm_ID
          ) {
            elOverride.Farm_ID = elSOverride.Farm_ID;
            elOverride.Loan_Full_ID = elSOverride.Loan_Full_ID;
            elOverride.Crop_Code = elSOverride.Crop_Code;
            elOverride.Share_Rent_Override = elSOverride.Share_Rent_Override;
            elOverride.Share_Rent_Percent = elSOverride.Share_Rent_Percent;
            elOverride.Status = 2;
          }
        });
      });

      // CHECK IF CROP IS NEWLY ADDED
      this.farmOverrideData.forEach(el => {
        if (el.Farm_ID == this.data.Farm_ID && el.Status == 1) {
          this.farmOverrideDataCollection.push(el);
        }
      });
    }

    this.loanObj['FarmCropOverride'] = this.farmOverrideDataCollection;
    this.localst.store(environment.loankey, this.loanObj);

    if (
      this.farmOverrideForm.dirty &&
      this.loanObj.LoanMaster.Loan_Status == LoanStatus.Working
    ) {
      let farm = this.loanObj.Farms.find(a => a.Farm_ID == this.data.Farm_ID);

      let allSame = this.farmOverrideDataCollection
        .filter(a => a.Farm_ID == this.data.Farm_ID)
        .every(f => f.Share_Rent_Percent == farm.Share_Rent);

      let isAnyUnselected = this.farmOverrideDataCollection
        .filter(a => a.Farm_ID == this.data.Farm_ID)
        .some(a => a.Share_Rent_Override == 0);

      if (!allSame || isAnyUnselected) {
        this.data.Crop_share_Detail_Indicator = 1;
      } else {
        this.data.Crop_share_Detail_Indicator = 0;
      }

      this.data.ActionStatus = 2;
      this.publishService.enableSync(Page.farm);
      this.loanserviceworker.performcalculationonloanobject(this.loanObj);
    }
  }

  // Data that is expected by the API
  private formatToCropDetailData() {
    this.farmOverrideData = [];
    this.farmOverrideForm.value.irrAcres.forEach(irrEl => {
      this.farmOverrideData.push({
        Farm_ID: irrEl.Farm_ID,
        Loan_Full_ID: irrEl.Loan_Full_ID,
        Crop_Code: irrEl.Crop_Code,
        Share_Rent_Override: irrEl.Share_Rent_Override,
        Share_Rent_Percent: irrEl.Share_Rent_Percent,
        Status: irrEl.Status
      });
    });

    this.farmOverrideForm.value.nirAcres.forEach(nirEl => {
      this.farmOverrideData.push({
        Farm_ID: nirEl.Farm_ID,
        Loan_Full_ID: nirEl.Loan_Full_ID,
        Crop_Code: nirEl.Crop_Code,
        Share_Rent_Override: nirEl.Share_Rent_Override,
        Share_Rent_Percent: nirEl.Share_Rent_Percent,
        Status: nirEl.Status
        // Action_Status: nirEl.Action_Status
      });
    });
  }

  public checkRentValue(afc, index, fc, event) {
    if (fc.value > 100.0) {
      afc[index].get('Share_Rent_Percent').setValue(100);
    }
  }

  public checkCropInclusion(afc, index, fc) {
    if (fc.value) {
      afc[index].get('Share_Rent_Percent').setValue(0);
    }
  }

  public closeModal(): void {
    this.farmOverrideForm.reset();
    this.dialogRef.close();
  }

  public onOkClick() {
    this.setFarmCropOverrideCollection();
    this.dialogRef.close();
  }

  ngOnDestroy() {
    if (this.preferencesubscription) {
      this.preferencesubscription.unsubscribe();
    }
  }
}
