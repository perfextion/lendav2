import { Component, OnInit, ViewEncapsulation, OnDestroy, AfterContentInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { ISubscription, Subscription } from 'rxjs/Subscription';
import { LocalStorageService } from 'ngx-webstorage';
import * as _ from 'lodash';
import * as XLSX from 'xlsx';

import { environment } from '@env/environment.prod';

import { numberValueSetter, getNumericCellEditor, numberWithOneDecPrecValueSetter } from '@lenda/Workers/utility/aggrid/numericboxes';
import { extractStateValues, lookupStateValue } from '@lenda/Workers/utility/aggrid/stateandcountyboxes';
import { SelectEditor } from '@lenda/aggridfilters/selectbox';
import { DeleteButtonRenderer } from '@lenda/aggridcolumns/deletebuttoncolumn';

import { getAlphaNumericCellEditor } from '@lenda/Workers/utility/aggrid/alphanumericboxes';
import { getDateCellEditor, getDateValue, formatDateValue } from '@lenda/Workers/utility/aggrid/dateboxes';
import { isgrideditable, cellclassmaker, CellType, headerclassmaker, calculatecolumnwidths } from '@lenda/aggriddefinations/aggridoptions';
import {
  percentageFormatter,
  currencyFormatter,
  acresFormatter,
  UnitperacreFormatter
} from '@lenda/aggridformatters/valueformatters';
import { RadioboxCellEditor } from '@lenda/aggridcolumns/radiobox-cell-editor/radiobox-cell-editor';
import { CheckboxCellRenderer } from '@lenda/aggridcolumns/checkbox-cell-editor/checkbox-cell-rendere';

import { RatedValues, RentUOMValues } from './farm.model';
import { Loan_Farm } from '@lenda/models/farmmodel.';
import { loan_model, LoanStatus } from '@lenda/models/loanmodel';
import { Sync_Status } from '@lenda/models/syncstatusmodel';
import {
  errormodel,
  SourceComponent
} from '@lenda/models/commonmodels';
import { LandownerValidation, RentDueDateValidation, RentAndProdPercValidation, RentUOMValidation, RentValidation, FarmTotalAcres, StateValidation, CountyValidation, WaivedRentValidation, DuplicateFarmKeyValidation, RentExceedsValidation, RentPercentValidation } from '@lenda/Workers/utility/validation-functions';
import { getRandomInt } from '@lenda/Workers/utility/randomgenerator';
import { LoanSettings, Chevrons, TableState, TableAddress } from '@lenda/preferences/models/loan-settings/index.model';
import { RefDataModel, RefChevron } from '@lenda/models/ref-data-model';
import { ColumnsDisplaySettings } from "@lenda/preferences/models/loan-settings/columns-display.model";
import { Loansettings, Loan_Key_Visibilty } from '@lenda/models/loansettings';

import { SettingsService } from "@lenda/preferences/settings/settings.service";
import { DatetTimeStampService } from '@lenda/services/datet-time-stamp.service';
import { ExportExcelService } from '@lenda/services/export-excel.service';
import { ToasterService } from '@lenda/services/toaster.service';
import { PubSubService } from '@lenda/services/pubsub.service';
import { MasterService } from '@lenda/master/master.service';
import { DataService } from '@lenda/services/data.service';
import { ValidationService } from '@lenda/Workers/calculations/validation.service';
import { FarmService } from './farm.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';
import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { PublishService } from '@lenda/services/publish.service';
import { Page } from '@lenda/models/page.enum';
import { CanComponentDeactivate } from '@lenda/services/route/deactivate.guard';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { LoggingService } from '@lenda/services/Logs/logging.service';

import { ManageFarmOverrideDialogComponent } from './farm-override/manage-farm-override-dialog.component';

declare function setmodifiedall(array, keyword): any;
declare function setmodifiedsingle(obj, keyword): any;
/// <reference path="../../Workers/utility/aggrid/numericboxes.ts" />

@Component({
  selector: 'app-farm',
  templateUrl: './farm.component.html',
  styleUrls: ['./farm.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [FarmService]
})
export class FarmComponent implements OnInit, AfterContentInit, OnDestroy, CanComponentDeactivate {
  private refdata: RefDataModel = new RefDataModel();
  public columnDefs = [];
  private localloanobject: loan_model = new loan_model();
  public columnsDisplaySettings: ColumnsDisplaySettings = new ColumnsDisplaySettings();

  // Aggrid
  public rowData = [];
  public pinnedBottomRowData = [];
  public isAdding: boolean = false;
  public enableCopy: boolean = false;
  public currenteditedfield: string = null;
  public currenteditrowindex: number = -1;
  public components: any;
  public context: any;
  public frameworkcomponents: any;
  public editType: any;
  private gridApi: any;
  private columnApi: any;
  public syncFarmStatus: Sync_Status;

  //region Ag grid Configuration

  public style = {
    marginTop: '10px',
    width: '100%',
    boxSizing: 'border-box'
  };

  public defaultColDef = {
    enableValue: true,
    sortable:false
  };

  public sortingOrder = ["asc", "desc"];

  public getRowStyle: (params: any) => {
    'font-weight': string;
    'background-color': string;
    'pointer-events': string;
  };

  public farmChevron: RefChevron;

  public get rowId() {
    return this.farmChevron.Chevron_Code + '_';
  }

  public get tableId() {
    return 'Table_' + this.farmChevron.Chevron_Code;
  }

  public gridOptions = {
    getRowNodeId: data => `${this.rowId}${data.Farm_ID}`
  };

  // Aggrid ends

  public isFarmOpen: boolean = false;
  public currentPageName: Page = Page.farm;
  private currenterrors: Array<errormodel>;

  private preferencesubscription: Subscription;
  private subscription: ISubscription;
  private errorSubscription: ISubscription;

  //Hide Columns Based ON Loan Settings
  private LoankeySettings: Loan_Key_Visibilty;

  private rentdetailcolumns = [
    'Owned',
    'Display_Rent',
    'Rent_UOM',
    'Cash_Rent_Due_Date',
    'Waived_Rent',
    'Share_Rent',
    'Permission_To_Insure'
  ];

  constructor(
    public localstorageservice: LocalStorageService,
    public loanserviceworker: LoancalculationWorker,
    public farmservice: FarmService,
    public logging: LoggingService,
    public alertify: AlertifyService,
    public loanapi: LoanApiService,
    private pubsubService: PubSubService,
    private publishService: PublishService,
    public validationservice: ValidationService,
    private dataService: DataService,
    private settingsService: SettingsService,
    private dtss: DatetTimeStampService,
    private dialog: MatDialog,
    public excel: ExportExcelService,
    public toasterService: ToasterService,
    public masterSvc: MasterService
  ) {

    this.frameworkcomponents = {
      selectEditor: SelectEditor,
      deletecolumn: DeleteButtonRenderer,
      radioboxCellEditor: RadioboxCellEditor,
      checkboxCellRenderer: CheckboxCellRenderer
    };

    this.components = {
      numericCellEditor: getNumericCellEditor(),
      alphaNumericCellEditor: getAlphaNumericCellEditor(),
      dateCellEditor: getDateCellEditor()
    };

    this.refdata = this.localstorageservice.retrieve(environment.referencedatakey);
    this.localloanobject = this.localstorageservice.retrieve(environment.loankey);


    if (this.refdata) {
      this.farmChevron = (this.refdata as RefDataModel).RefChevrons.find(c => c.Chevron_ID == 3);
    } else {
      this.farmChevron = <RefChevron>{};
    }

    //Coldef here
    if (this.refdata != null) this.declareColumns();

    this.context = { componentParent: this };

    this.getRowStyle = function (params) {
      if (params.node.rowPinned) {
        return {
          'font-weight': 'bold',
          'background-color': '#F5F7F7',
          'pointer-events': 'none'
        };
      }
    };
    this.InitializeServices();
  }

  ngOnInit() {

  }

  private InitializeServices() {
    this.localstorageservice.store(environment.currentpage, Page.farm);

    this.getUserPreferences();

    this.pubsubService.subscribeToEvent().subscribe((event) => {
      if (event.name === 'event.farm.rentdetail.updated') {
        this.showhiderentdetails(event.data.value);
      }
    });

    this.subscription = this.dataService.getLoanObject().subscribe(res => {
      if (res != null) {
        this.checkIfFarmIsOwned();
        if (environment.isDebugModeActive)
          console.time('Farm Grid Observer');
        // this.logging.checkandcreatelog(1, 'LoanFarms', "LocalStorage updated");
        this.localloanobject = res;
        if (res.Farms && res.srccomponentedit == 'FarmComponent') {
          if(res.lasteditrowindex) {
            let farm = this.localloanobject.Farms.filter(a => a.ActionStatus != 3).find(a => a.Farm_ID == res.lasteditrowindex);
            let Rfarm = this.rowData.find(a => a.Farm_ID == res.lasteditrowindex);

            if(farm && Rfarm) {
              Rfarm = farm;
            }
          }
          this.localloanobject.srccomponentedit = undefined;
          this.localloanobject.lasteditrowindex = undefined;
          //this.rowData = _.orderBy(this.rowData, ['FC_State_Code','FC_County_Name','Percent_Prod','Landowner','FSN','Section','Rated']);
          this.pinnedBottomRowData = this.getTotalRow(this.rowData);
        }
        else if (res.Farms) {
          this.rowData = this.localloanobject.Farms.filter(p => p.ActionStatus != 3);
          this.rowData = _.orderBy(this.rowData, ['FC_State_Code','FC_County_Name','Percent_Prod','Landowner','FSN','Section','Rated']);
          this.pinnedBottomRowData = this.getTotalRow(this.rowData);
        }
        else {
          this.rowData = [];
        }
        this.gridApi.refreshCells({
          columns: ['FC_Total_Acres']
        });
        this.updateSyncStatus();
        this.setValidationErrorForFarm();
        setTimeout(() => {
          setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), this.rowId);
        }, 5);
        if (environment.isDebugModeActive)
          console.timeEnd('Farm Grid Observer');
      }
    });

    this.errorSubscription = this.localstorageservice.observe(environment.errorbase).subscribe(() => {
      setTimeout(() => {
        this.getCurrentErrors();
      }, 5);
    });

    if (this.localloanobject != null && this.localloanobject != undefined) {
      this.getdataforgrid();
    }
  }

  private getUserPreferences() {
    this.columnsDisplaySettings = this.settingsService.preferences.loanSettings.columnsDisplaySettings;
    this.preferencesubscription= this.settingsService.preferencesChange.subscribe((preferences) => {
      this.columnsDisplaySettings = preferences.loanSettings.columnsDisplaySettings;
      this.hideUnhideLoanKeys();
      this.declareColumns();
    });
  }

  private getCurrentErrors() {
    if (environment.isDebugModeActive) console.time('Highlight Validation - Farm');

    this.currenterrors = (this.localstorageservice.retrieve(
      environment.errorbase
    ) as Array<errormodel>).filter(p => p.chevron == this.farmChevron.Chevron_Code);

    this.validationservice.highlighErrorCells(this.currenterrors, this.tableId);

    if (environment.isDebugModeActive) console.timeEnd('Highlight Validation - Farm');
  }

  public onGridReady(params) {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    //these two methods remove side option toolbar , sets column , removes header menu
    // setgriddefaults(this.gridApi, this.columnApi);
    //save settings here
    let that = this;
    this.gridApi.addEventListener('displayedColumnsChanged', function () {
      if (environment.isDebugModeActive) console.time("started")

      let state: any = that.columnApi.getColumnState();

      if (state.filter(p => p.hide == false).length > 0) {
        let obj = JSON.parse(that.localloanobject.LoanMaster.Loan_Settings) as LoanSettings;
        if (obj.Table_States != undefined && obj.Table_States != null) {
          let data = obj.Table_States.find(p => p.address.page == Page.farm && p.address.chevron == Chevrons.farm_farm);
          if (data != undefined)
            data.data = state;
        } else {
          //add here
          let farm_farmstate = new TableState();
          farm_farmstate.address = new TableAddress();
          farm_farmstate.address.chevron = Chevrons.farm_farm;
          farm_farmstate.address.page = Page.farm;
          farm_farmstate.data = state;

          obj.Table_States = [];
          obj.Table_States.push(farm_farmstate);
        }
        that.localloanobject.LoanMaster.Loan_Settings = JSON.stringify(obj);
        //window.localStorage.setItem("ng2-webstorage|currentselectedloan", JSON.stringify(that.localloanobject))
        that.dataService.setLoanObjectwithoutsubscription(that.localloanobject);
      }
      if (environment.isDebugModeActive) console.timeEnd("started")
    });

    this.getdataforgrid();

    setTimeout(() => {
      let loansettingd = (JSON.parse(this.localloanobject.LoanMaster.Loan_Settings) as LoanSettings);
      if (loansettingd.Table_States != undefined && loansettingd.Table_States != null) {
        let data = loansettingd.Table_States.find(p => p.address.page == Page.farm && p.address.chevron == Chevrons.farm_farm);
        if (data != undefined)
          this.columnApi.setColumnState(data.data);
      }
      this.getCurrentErrors();
    }, 1000);

    // params.api.sizeColumnsToFit();
    this.adjustToFitAgGrid();
  }

  private hideUnhideLoanKeys() {
    let LoanSettings: Loansettings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
    if (LoanSettings != undefined || LoanSettings != null) {
      if (LoanSettings.Loan_key_Settings != undefined || LoanSettings.Loan_key_Settings != null) {
        this.LoankeySettings = LoanSettings.Loan_key_Settings
      }
      else {
        this.LoankeySettings = new Loan_Key_Visibilty();
      }
    }
    else {
      this.LoankeySettings = new Loan_Key_Visibilty();
    }
    this.columnsDisplaySettings.rated = this.LoankeySettings.Rated == null ? this.columnsDisplaySettings.rated : this.LoankeySettings.Rated;
    this.columnsDisplaySettings.section = this.LoankeySettings.Section == null ? this.columnsDisplaySettings.section : this.LoankeySettings.Section;

  }

  //An Array of properties that Represent Keys in the same table
  public Loankeys = ["Section", "Rated"]

  public displayColumnsChanged($event) {

    if (this.columnApi != undefined && $event != undefined) {

      //first One to save to Loan Settings
      let LoanSettings: Loansettings = JSON.parse(this.localloanobject.LoanMaster.Loan_Settings);
      if (LoanSettings == undefined || LoanSettings == null) {
        LoanSettings = new Loansettings();
      }
      if (LoanSettings.Loan_key_Settings == undefined || LoanSettings.Loan_key_Settings == null) {
        LoanSettings.Loan_key_Settings = new Loan_Key_Visibilty();
      }
      //Since we dont have column thats changed so lets see manually
      this.Loankeys.forEach(key => {

        let column = this.columnApi.getColumn(key);
        LoanSettings.Loan_key_Settings[key] = column.visible;
      });
      this.localloanobject.LoanMaster.Loan_Settings = JSON.stringify(LoanSettings);

      this.dataService.setLoanObjectwithoutsubscription(this.localloanobject);
      this.getCurrentErrors();
      //Second Execution
      this.adjustToFitAgGrid();
    }
  }

  public adjustToFitAgGrid() {
    if (this.columnApi != undefined) {
      this.style.width = calculatecolumnwidths(this.columnApi);
    }
  }

  private declareColumns() {
    this.columnDefs = [
      {
        headerName: '',
        suppressSorting: true,
        supressCopy: true,
        field: 'value',
        supressExcel: true,
        cellRenderer: (params) => {
          if (params.data.istotal) {
            return '';
          }

          let ind = params.data.Crop_share_Detail_Indicator;
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          if (ind) {
            return '<i class="material-icons gear-icon">settings</i>';
          } else {
            return '<i class="material-icons no-crop-share gear-icon">settings</i>';
          }
        },
        cellClass: (params) => {
          if (params.data.istotal) {
            return 'aggregatecol';
          }
        },
        suppressToolPanel: true,
        onCellClicked: (params) => {

          if (params.data.istotal) {
            return;
          }

          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;

          if(params.data.ActionStatus == 1 && params.data.Owned == 0 && isgrideditable(true, status)) {
            params.context.componentParent.publishService.syncToDb();
          }

          this.overrideFarmModal(params);

          return;
        },
        minWidth: 40,
        width: 40,
        maxWidth: 40,
      },
      {
        headerName: 'Farm Id',
        minWidth: 90,
        width: 100,
        maxWidth: 100,
        field: 'Farm_ID',
        hide: true,
        suppressToolPanel: true,
        supressCopy: true
      },
      {
        headerName: 'State',
        minWidth: 90, width: 100, maxWidth: 100,headerClass:'options',sortable: false,
        field: 'FC_State_Code',
        cellClass: (params) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Text, !params.data.Farm_State_ID || params.data.ActionStatus == 1, '', status);
        },
        editable: (params) => {
          return !params.data.Farm_State_ID || params.data.ActionStatus == 1;
        },
        cellEditor: 'selectEditor',
        calculationinvoke: true,
        cellEditorParams: {
          values: extractStateValues(this.refdata.StateList)
        },
        required: true,
        valueSetter: function(params){
          params.data.Farm_State_ID=parseInt(params.newValue);
          params.data.FC_State_Code= lookupStateValue(
            params.colDef.cellEditorParams.values,
            parseInt(params.newValue, 10)
          );
        },
        validations: { State: StateValidation, DuplicateKey: DuplicateFarmKeyValidation },
        eventDelegations: { calculations: false, validations: true }
      },
      {
        headerName: 'County',
        minWidth: 90, width: 100, maxWidth: 100,headerClass:'options',sortable: true,
        field: 'FC_County_Name',
        cellClass: (params) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Text, !params.data.Farm_County_ID || params.data.ActionStatus == 1, '', status);
        },
        editable: (params) => {
          if (params.data.istotal) {
            return false;
          }
          return !params.data.Farm_County_ID || params.data.ActionStatus == 1;
        },
        cellEditor: "selectEditor",
        cellEditorParams: (params) => {
          return { values: this.getCountiesByState(params.data.Farm_State_ID) };
        },
        valueSetter: function(params){
          params.data.Farm_County_ID=parseInt(params.newValue);
          let county = params.context.componentParent.refdata.CountyList.find(p=>p.County_ID==parseInt(params.newValue, 10));
          if(county) {
            params.data.FC_County_Name = county.County_Name;
          } else {
            params.data.FC_County_Name = '';
          }
        },
        validations: { County: CountyValidation },
        eventDelegations: { calculations: false, validations: true }
      },
      {
        headerName: 'Prod %',
        minWidth: 90, width: 100, maxWidth: 100,
        headerClass: headerclassmaker(CellType.Integer,"options"),sortable: true,
        field: 'Percent_Prod',
        validations: { RentValidation: RentAndProdPercValidation },
        cellClass: 'rightaligned',
        cellEditor: 'numericCellEditor',
        valueFormatter: params => {
          return percentageFormatter(params);
        },
        valueSetter: function (params) {
          params.data['Share_Rent'] = 100 - parseFloat(params.newValue);
          numberValueSetter(params);
          if (params.newValue) {
            if (!params.data.Owned) {
              params.data['Share_Rent'] = 100 - parseFloat(params.newValue);
            }
          }
          return true;
        },
        eventDelegations: { calculations: true, validations: true },
        cellClassRules: {
          'editable-color': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] != 1 && status == 'W';
          },
          'default-cell': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] == 1 || status != 'W';
          }
        },
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          let val = !!!params.data.Owned;
          return isgrideditable(val, status);
        }
      },
      {
        headerName: 'Landlord',
        minWidth: 90, width: 100, maxWidth: 100,headerClass: "options",sortable: true,
        field: 'Landowner',
        tooltip: params => params.data['Landowner'],
        validations: { 'Landowner': LandownerValidation },
        cellClassRules: {
          'editable-color': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] != 1 && status == 'W';
          },
          'default-cell': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] == 1 || status != 'W';
          }
        },
        eventDelegations: { calculations: false, validations: false },
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          let val = !!!params.data.Owned;
          return isgrideditable(val, status);
        },
        cellEditor: 'selectEditor',
        cellEditorParams: () => {
          return this.getLandlords();
        }
      },
      {
        headerName: 'FSN',
        field: 'FSN',
        tooltip: params => params.data['FSN'],
        minWidth: 90, width: 100, maxWidth: 100,
        headerClass: headerclassmaker(CellType.Text,"options"),sortable: true,
        cellClass: (params) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Text, true, '', status);
        },
        editable: (params) => {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          let val = true;
          return isgrideditable(val, status);
        },
        cellEditor: 'agTextCellEditor',
        eventDelegations: { calculations: true, validations: true },
        hide: !this.columnsDisplaySettings.fsn
      },
      {
        headerName: 'Section',
        field: 'Section',
        minWidth: 90, width: 100, maxWidth: 100,headerClass: "options",sortable: true,
        cellClass: (params) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Text, true, '', status);
        },
        editable: (params) => {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          let val = true;
          return isgrideditable(val, status);
        },
        cellEditor: 'agTextCellEditor',
        eventDelegations: { calculations: true, validations: true },
        hide: !this.columnsDisplaySettings.section
      },
      {
        headerName: 'Rated',
        field: 'Rated',
        minWidth: 90, width: 100, maxWidth: 100,
        cellClass: (params) => {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Text, true, '', status);
        },
        editable: (params) => {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          let val = true;
          return isgrideditable(val, status);
        },
        cellEditor: 'selectEditor',
        cellEditorParams: {
          values: RatedValues
        },
        eventDelegations: { calculations: true, validations: true },
        hide: !this.columnsDisplaySettings.rated
      },
      {
        headerName: 'Owned',suppressSorting: true,
        field: 'Owned',
        minWidth: 90, width: 100, maxWidth: 100,
        validations: { required: true },
        cellClass: function (params) {
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Text, true, '', status) + ' center';
        },
        cellRenderer: 'checkboxCellRenderer',
        cellRendererParams: params => {
          return {
            type: 'Owned',
            edit: params.context.componentParent.localloanobject.LoanMaster.Loan_Status == 'W'
          }
        },
        eventDelegations: { calculations: true, validations: true }
      },
      {
        headerName: 'Rent $',suppressSorting: true,
        minWidth: 90, width: 100, maxWidth: 100,
        headerClass: headerclassmaker(CellType.Integer),
        field: 'Display_Rent',
        cellClass: 'rightaligned',
        validations: { rent: RentValidation, rentExceeds: RentExceedsValidation },
        cellClassRules: {
          'editable-color': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] != 1 && status == 'W';
          },
          'default-cell': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] == 1 || status != 'W';
          }
        },
        eventDelegations: { calculations: true, validations: true },
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          let val = !!!params.data.Owned;
          return isgrideditable(val, status);
        },
        cellEditor: 'numericCellEditor',
        valueSetter: params => numberWithOneDecPrecValueSetter(params, 2),
        valueFormatter: params => {
          if (params.data.istotal) {
            return '';
          }
          if (params.data['Owned'] == 0)
            return currencyFormatter(params, 2);
          return '';
        }
      },
      {
        headerName: 'Rent $ UoM',suppressSorting: true,
        minWidth: 90, width: 100, maxWidth: 100,
        field: 'Rent_UOM',
        cellClassRules: {
          'editable-color': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] != 1 && status == 'W';
          },
          'default-cell': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] == 1 || status != 'W';
          }
        },
        eventDelegations: { calculations: true, validations: false },
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          let val = !!!params.data.Owned;
          return isgrideditable(val, status);
        },
        cellEditor: 'selectEditor',
        cellEditorParams: {
          values: RentUOMValues
        },
        valueFormatter: params => {
          if (params.data.istotal) {
            return '';
          }
          return UnitperacreFormatter(params);
        },
        validations: { UOM: RentUOMValidation }
      },
      {
        headerName: 'Rent Due Dt',suppressSorting: true,
        minWidth: 90, width: 100, maxWidth: 100,
        headerClass: headerclassmaker(CellType.Integer),
        field: 'Cash_Rent_Due_Date',
        cellClass: 'rightaligned',
        validations: RentDueDateValidation,
        cellRenderer: (params) => {
          return _.isEqual(this.formateDate(params.value), '1/1/1900') ||
            _.isEmpty(params.value) ? null : this.formateDate(params.value);
        },
        cellClassRules: {
          'editable-color': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] != 1 && status == 'W';
          },
          'default-cell': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] == 1 || status != 'W';
          }
        },
        eventDelegations: { calculations: false, validations: false },
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          let val = !!!params.data.Owned;
          return isgrideditable(val, status);
        },
        cellEditor: 'dateCellEditor',
        cellEditorParams: getDateValue,
        valueFormatter: params => {
          if (params.data.istotal) {
            return '';
          }
          if (params.data['Owned'] != 1) {
            return formatDateValue(params);
          }
          return '';
        },
        valueSetter: function (params) {
          if (params.newValue) {
            params.data['Cash_Rent_Due_Date'] = _.isEqual(params.newValue, '1/1/1900') ||
              _.isEmpty(params.newValue) ? null : params.newValue;
          } else {
            params.data['Cash_Rent_Due_Date'] = null;
          }
          return true;
        }
      },
      {
        headerName: 'Waived $',suppressSorting: true,
        minWidth: 90, width: 100, maxWidth: 100,
        headerClass: headerclassmaker(CellType.Integer),
        field: 'Waived_Rent',
        hide: true,
        cellClass: 'rightaligned',
        validations: { waived: WaivedRentValidation },
        cellClassRules: {
          'editable-color': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] != 1 && status == 'W';
          },
          'default-cell': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] == 1 || status != 'W';
          }
        },
        eventDelegations: { calculations: true, validations: true },
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          let val = !!!params.data.Owned;
          return isgrideditable(val, status);
        },
        cellEditor: 'numericCellEditor',
        valueSetter: params => numberWithOneDecPrecValueSetter(params, 2),
        valueFormatter: params => {
          if (params.data.istotal || params.data.Owned) {
            return '';
          }

          if (params.data['Owned'] != 1)
            return currencyFormatter(params, 2);
          return '';
        }
      },
      {
        headerName: 'Rent %',suppressSorting: true,
        minWidth: 90, width: 100, maxWidth: 100,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: 'rightaligned',
        field: 'Share_Rent',
        validations: { rentValidation: RentPercentValidation,  rentAndProdPercValidation: RentAndProdPercValidation },
        valueFormatter: params => {
          return percentageFormatter(params);
        },
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          let val = !!!params.data.Owned;
          return isgrideditable(val, status);
        },
        cellEditor: 'numericCellEditor',
        cellClassRules: {
          'editable-color': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] != 1 && status == 'W';
          },
          'default-cell': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] == 1 || status != 'W';
          }
        },
        valueSetter: function (params) {
          numberValueSetter(params);
          if (params.newValue) {
            if (!params.data.Owned) {
              params.data['Percent_Prod'] = 100 - parseFloat(params.newValue);
            }
          }
          return true;
        },
        eventDelegations: { calculations: true, validations: true }
      },
      {
        headerName: 'Rent Paid',
        suppressSorting: true,
        minWidth: 90,
        width: 100,
        maxWidth: 100,
        hide: true,
        field: 'Cash_Rent_Paid',
        cellRenderer: 'checkboxCellRenderer',
        cellRendererParams: params => {
          return {
            type: 'Rent_Paid',
            edit: params.data['Owned'] != 1 && params.context.componentParent.localloanobject.LoanMaster.Loan_Status == 'W'
          }
        },
        cellClass: 'center',
        cellClassRules: {
          'editable-color': (params) => {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] != 1 && status == 'W';
          },
          'default-cell': (params) => {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] == 1 || status != 'W';
          }
        },
        eventDelegations: {
          calculations: true,
          validations: false
        }
      },
      {
        headerName: 'PTI',suppressSorting: true,
        minWidth: 90, width: 100, maxWidth: 100,
        field: 'Permission_To_Insure',
        hide: true,
        cellRenderer: 'checkboxCellRenderer',
        cellRendererParams: params => {
          return {
            type: 'PTI',
            edit: params.data['Owned'] != 1 && params.context.componentParent.localloanobject.LoanMaster.Loan_Status == 'W'
          }
        },
        cellClass: 'center',
        cellClassRules: {
          'editable-color': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] != 1 && status == 'W';
          },
          'default-cell': function (params) {
            let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
            return params.data['Owned'] == 1 || status != 'W';
          }
        },
        eventDelegations: { calculations: true, validations: false }
      },
      {
        headerName: 'Irr Acres',suppressSorting: true,
        minWidth: 90, width: 100, maxWidth: 100,
        headerClass: headerclassmaker(CellType.Integer),
        field: 'Irr_Acres',
        cellClass: function (params) {
          if (params.data.istotal) {
            return 'rightaligned aggregatecol';
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Integer, true, '', status);
        },
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return isgrideditable(true, status);
        },
        cellEditor: 'numericCellEditor',
        valueSetter: numberWithOneDecPrecValueSetter,
        valueFormatter: acresFormatter,
        validations: { acres: FarmTotalAcres },
        eventDelegations: { calculations: true, validations: true }
      },
      {
        headerName: 'NI Acres',suppressSorting: true,
        minWidth: 90, width: 100, maxWidth: 100,
        headerClass: headerclassmaker(CellType.Integer),
        field: 'NI_Acres',
        cellClass: function (params) {
          if (params.data.istotal) {
            return 'rightaligned aggregatecol';
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Integer, true, '', status);
        },
        editable: function (params) {
          if (params.data.istotal) {
            return false;
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return isgrideditable(true, status);
        },
        cellEditor: 'numericCellEditor',
        valueSetter: numberWithOneDecPrecValueSetter,
        valueFormatter: acresFormatter,
        validations: { acres: FarmTotalAcres },
        eventDelegations: { calculations: true, validations: true }
      },
      {
        headerName: 'Total Acres',suppressSorting: true,
        minWidth: 90, width: 100, maxWidth: 100,
        headerClass: headerclassmaker(CellType.Integer),
        cellClass: function (params) {
          if (params.data.istotal) {
            return 'rightaligned aggregatecol';
          }
          let status = params.context.componentParent.localloanobject.LoanMaster.Loan_Status;
          return cellclassmaker(CellType.Integer, false, '', status);
        },
        field: 'FC_Total_Acres',
        cellEditor: 'numericCellEditor',
        valueSetter: numberWithOneDecPrecValueSetter,
        valueFormatter: acresFormatter,
        validations: { required: true }
      },
      {
        headerName: '',suppressSorting: true,
        field: 'value', supressExcel: true,
        cellRendererSelector: function (params) {
          if (params.data.istotal) {
            return null;
          }
          return {
            component: 'deletecolumn'
          }
        },
        suppressToolPanel: true,
        cellClass: (params) => {
          if (params.data.istotal) {
            return 'aggregatecol';
          }
        },
        minWidth: 60, width: 60, maxWidth: 60,
      }
    ];
  }

  public MethodFromCheckbox(params: any, type: string, $event: any) {
    if(type == 'Owned') {
      if ($event.srcElement.checked) {
        params.data['Owned'] = 1;
      } else {
        params.data['Owned'] = 0;
      }

      if (params.data['Owned'] == 1) {
        params.data['Display_Rent'] = 0;
        params.data['Waived_Rent'] = 0;
        params.data['Rent_UOM'] = 0;
        params.data['Cash_Rent_Due_Date'] = '';

        params.data['Share_Rent'] = 0;
        params.data['Percent_Prod'] = 100;

        params.data['Permission_To_Insure'] = 1;
        params.data['Landowner'] = '';
      } else {
        params.data['Display_Rent'] = 0;
        params.data['Waived_Rent'] = 0;
        params.data['Rent_UOM'] = 1;
      }

      params.api.refreshCells({
        columns: ['Owned', 'Display_Rent', 'Waived_Rent', 'Rent_UOM', 'Cash_Rent_Due_Date', 'Share_Rent', 'Percent_Prod', 'Permission_To_Insure', 'Landowner' ]
      });
    }

    if(type == 'PTI') {
      if ($event.srcElement.checked) {
        params.data['Permission_To_Insure'] = 1;
      } else {
        params.data['Permission_To_Insure'] = 0;
      }

      params.api.refreshCells({
        columns: ['Permission_To_Insure']
      });
    }

    if(type == 'Rent_Paid') {
      if ($event.srcElement.checked) {
        params.data['Cash_Rent_Paid'] = 1;
      } else {
        params.data['Cash_Rent_Paid'] = 0;
      }

      params.api.refreshCells({
        columns: ['Cash_Rent_Paid']
      });
    }

    if (params.data.ActionStatus != 1) {
      params.data.ActionStatus = 2;
    }

    this.localloanobject.srccomponentedit = SourceComponent.FarmComponent;
    this.localloanobject.lasteditrowindex = params.rowIndex;

    this.updateSyncStatus();
    const eventDelegations = params.column.colDef.eventDelegations;

    // Step 1 Calculation service
    if (eventDelegations) {
      this.loanserviceworker.performcalculationonloanobject(this.localloanobject, eventDelegations.calculations);
    }

    // Step 2 validate row here
    if (eventDelegations && eventDelegations.validations) {
      this.setValidationErrorForFarm();
    }

    this.publishService.enableSync(Page.farm);
  }

  private showhiderentdetails(visible: boolean) {
    if (visible) {
      this.columnApi.setColumnsVisible(this.rentdetailcolumns, true)
    }
    else {
      this.columnApi.setColumnsVisible(this.rentdetailcolumns, false)
    }
  }

  ngAfterContentInit() {
    this.checkIfFarmIsOwned();
    this.updateSyncStatus();
  }

  private checkIfFarmIsOwned() {
    let isOwned = false;
    let hasLandlord = false;

    this.localloanobject.Association.forEach(el => {
      if (el.Assoc_Type_Code == 'LLD') {
        hasLandlord = true;
      }
    });

    this.localloanobject.Farms.forEach(el => {
      if (el.Owned == 0) {
        isOwned = true;
      }
    });

    this.isFarmOpen = isOwned;

    if (hasLandlord) {
      this.isFarmOpen = hasLandlord;
    }
  }

  public overrideFarmModal(params) {
    let dialogRef = this.dialog.open(ManageFarmOverrideDialogComponent, {
      disableClose: true,
      autoFocus: false,
      panelClass: ['farm-modal-dialog'],
      data: params.data
    });

    dialogRef.afterClosed().subscribe(() => { });
  }

  private getdataforgrid() {
    if (environment.isDebugModeActive) console.time('Farm Grid - Initial Load');

    let obj: loan_model = this.localstorageservice.retrieve(environment.loankey);
    // this.logging.checkandcreatelog(1, 'LoanFarms', "LocalStorage retrieved");
    if (obj != null && obj != undefined) {
      this.localloanobject = obj;
      if (obj.Farms) {
        this.rowData = [];
        this.rowData = _.orderBy(obj.Farms.filter(p => p.ActionStatus != 3),['FC_State_Code','FC_County_Name','Percent_Prod','Landowner','FSN','Section','Rated']);
        this.pinnedBottomRowData = this.getTotalRow(this.localloanobject.Farms);
      } else {
        this.rowData = [];
      }
    }

    this.setValidationErrorForFarm();

    setTimeout(() => {
      setmodifiedall(this.localstorageservice.retrieve(environment.modifiedbase), this.rowId);
    }, 5);

    if (environment.isDebugModeActive) console.timeEnd('Farm Grid - Initial Load');
  }

  private getTotalRow(farms: any[]) {
    let row: any = {};
    row.Farm_ID = undefined;
    row.Farm_State_ID = 'All';
    row.Farm_County_ID = 'All';
    row.FSN = 'All';


    row.Irr_Acres = _.sumBy(farms, function (o) {
      return o.Irr_Acres;
    });

    row.NI_Acres = _.sumBy(farms, function (o) {
      return o.NI_Acres;
    });

    row.FC_Total_Acres = _.sumBy(farms, function (o) {
      return o.FC_Total_Acres;
    });

    if (row.FC_Total_Acres > 0) {
      let total_prod_perc = _.sumBy(farms, (f: Loan_Farm) => f.Percent_Prod * f.FC_Total_Acres);
      row.Percent_Prod = total_prod_perc / row.FC_Total_Acres;
    } else {
      row.Percent_Prod = 0;
    }

    if (row.FC_Total_Acres > 0) {
      let total_rent_perc = _.sumBy(farms, (f: Loan_Farm) => f['Share_Rent'] * f.FC_Total_Acres);
      row['Share_Rent'] = total_rent_perc / row.FC_Total_Acres;
    } else {
      row.Percent_Prod = 0;
    }

    row.istotal = true;

    return [row];
  }

  public formateDate(strDate) {
    let date;
    if (strDate) {
      date = new Date(strDate);
      if (date.getFullYear() < 2000) {
        return '';
      } else {
        return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
      }
    } else {
      return '';
    }
  }


  public rowvaluechanged(value: any) {
    this.excel.removeExportToExcelTime(Page.farm);

    //---------Modified yellow values-------------
    if (value && (value.oldValue == value.value || (!value.oldValue && !value.value))) return;

    if (!value && !value.column && !value.column.colId) return;

    //MODIFIED YELLOW VALUES
    let modifiedvalues = this.localstorageservice.retrieve(environment.modifiedbase) as Array<String>;

    if (!modifiedvalues.includes(this.rowId + value.data.Farm_ID + '_' + value.colDef.field)
    ) {
      modifiedvalues.push(this.rowId + value.data.Farm_ID + '_' + value.colDef.field);
      this.localstorageservice.store(environment.modifiedbase, modifiedvalues);
      setmodifiedsingle(this.rowId + value.data.Farm_ID + '_' + value.colDef.field, this.rowId);
    }
    //MODIFIED YELLOW VALUES
    //--------------------------------------------

    //for states and County

    // if (value.colDef.headerName == 'State') {
    //   const county = this.refdata.CountyList.find(p => p.State_ID == value.data.Farm_State_ID);
    //   value.data.Farm_County_ID = county ? county.County_ID : 0;
    // }

    this.currenteditedfield = null;
    this.currenteditrowindex = -1;

    if (!this.localloanobject.Farms) {
      this.localloanobject.Farms = [];
    }

    if (value.data.ActionStatus != 1) value.data.ActionStatus = 2;

    this.localloanobject.srccomponentedit = SourceComponent.FarmComponent;
    this.localloanobject.lasteditrowindex = value.data.Farm_ID;
    this.updateSyncStatus();

    const eventDelegations = value.column.colDef.eventDelegations;
    // Step 1 Calculation service
    if (eventDelegations) {
      this.loanserviceworker.performcalculationonloanobject(this.localloanobject, eventDelegations.calculations);
    }
    // Step 2 validate row here
    if (eventDelegations && eventDelegations.validations) {
      this.setValidationErrorForFarm();
    }
    this.publishService.enableSync(Page.farm);
  }

  /**
   * Sync to database - publish button event
   */
  synctoDb() {
    this.gridApi.stopEditing();
    this.publishService.syncCompleted();
    this.farmservice.syncToDb(this.localstorageservice.retrieve(environment.loankey));
  }

  //Grid Events
  public addrow() {
    if (!this.rowData) {
      this.rowData = [];
    }

    let isAlreadyAddedRow = this.rowData.find(a => {
      return !a.Farm_State_ID || !a.Farm_County_ID
    });

    if(isAlreadyAddedRow ) {
      this.alertify.alert('Alert',' You already have added a new row with no state/county.');
      return;
    }

    this.isAdding = true;
    this.enableCopy = true;

    let newItem: Loan_Farm = new Loan_Farm();
    newItem.ActionStatus = 1;
    newItem.Loan_Full_ID = this.localloanobject.Loan_Full_ID;
    // newItem.Farm_State_ID = this.refdata.StateList[0].State_ID;
    newItem.Farm_ID = getRandomInt(Math.pow(10, 5), Math.pow(10, 7));
    newItem.Percent_Prod = 100;
    newItem.FC_Total_Acres = 0;
    newItem.Waived_Rent = 0;
    newItem.Display_Rent = 0;
    newItem.Cash_Rent_Waived_Amount = 0;
    newItem.Farm_State_ID = null;

    this.rowData.push(newItem);
    // this.gridApi.setRowData(this.rowData); // Why setRowData ?

    this.gridApi.updateRowData({ add: [newItem] });

    this.localloanobject.Farms.push(newItem);

    this.gridApi.startEditingCell({
      rowIndex: this.rowData.length,
      colKey: 'Farm_State_ID'
    });

    this.publishService.enableSync(Page.farm);
    this.setValidationErrorForFarm();
  }

  public onCellClicked(event: any) {
    if (event.type == 'cellClicked' && event.colDef.field != 'value' && this.enableCopy) {
      this.enableCopy = false;

      let current_row: Loan_Farm = this.rowData[event.rowIndex];
      let previous_row = this.rowData[event.rowIndex - 1];

      if (current_row && current_row.ActionStatus == 1 && !!previous_row) {
        let column: string = event.colDef.field;
        let columns_to_copy = this.farmservice.getColumnsToCopy(this.columnDefs, column);
        current_row = this.farmservice.copyrow(previous_row, current_row, columns_to_copy);

        this.rowData[event.rowIndex] = current_row;

        this.gridApi.updateRowData({ update: [current_row] });
        this.publishService.enableSync(Page.farm);
        this.setValidationErrorForFarm();
      }
    }
  }

  public DeleteClicked(rowIndex: any, data) {
    this.alertify.deleteRecord().subscribe(res => {
      if (res == true) {
        let obj = this.rowData[rowIndex] as Loan_Farm;

        let farmIndex = this.localloanobject.Farms.findIndex(f => f === data);
        if (obj.ActionStatus == 1) {
          this.localloanobject.Farms.splice(farmIndex, 1); // it will then assigned to rowData so it will be affected to it
        } else {
          this.localloanobject.Farms[farmIndex].ActionStatus = 3;
        }

        this.localloanobject.srccomponentedit = undefined;
        this.localloanobject.lasteditrowindex = undefined;
        this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
        this.publishService.enableSync(Page.farm);

        this.setValidationErrorForFarm();
        this.isAdding = false;
      }
    });
  }

  getgridheight() {
    // this.style.height = (29 * (this.rowData.length + 2)).toString() + "px";
  }

  isLoanEditable() {
    return !this.localloanobject || this.localloanobject.LoanMaster.Loan_Status === LoanStatus.Working;
  }

  public onBtExport() {
    const ts = this.dtss.getTimeStamp();

    let columnKeys = this.columnDefs.filter(a => !a.supressExcel);
    columnKeys = columnKeys.map(a => a.field);

    const params = {
      skipHeader: false,
      columnGroups: true,
      skipFooters: false,
      skipGroups: false,
      skipPinnedTop: false,
      skipPinnedBottom: false,
      allColumns: true,
      exportMode: 'xlsx',
      suppressTextAsCDATA: true,
      fileName: `Farm_${this.localloanobject.Loan_Full_ID}_${ts}.xls`,
      columnKeys: columnKeys,
      shouldRowBeSkipped: params => {
        return params.node.data.istotal;
      },
      sheetName: 'Sheet 1',
      processCellCallback: params => {
        if(params.column.colId == 'Cash_Rent_Due_Date') {
          let v = formatDateValue(params);
          if(v == '1/1/1900') {
            return '';
          }
          return v;
        }
        return params.value;
      }
    };

    this.excel.updateExportToExcelTime(Page.farm, ts);

    // Export Data to Excel file
    let farmdata = this.gridApi.getDataAsExcel(params);

    let parser = new DOMParser();
    let farmXML = parser.parseFromString(farmdata, 'text/xml');

    let oSerializer = new XMLSerializer();
    let sXML = oSerializer.serializeToString(farmXML);

    let workbook = XLSX.read(sXML, {type: 'string'});
    XLSX.writeFile(workbook, `Farm_${this.localloanobject.Loan_Full_ID}_${ts}.xlsx`);

    this.publishService.enableSync(Page.farm);
  }

  public onImportExcel(data: any[]) {
    try {
      const rows = this.farmservice.getValidFarms(data, this.localloanobject);

      this.localloanobject.Farms = rows;
      this.publishService.enableSync(Page.farm);
      this.updateSyncStatus();
      this.loanserviceworker.performcalculationonloanobject(this.localloanobject);
    } catch (ex) {
      this.toasterService.error(ex.message);
    }
  }

  private updateSyncStatus() {
    if (this.localloanobject.Farms.filter(p => p.ActionStatus == 1 || p.ActionStatus == 3).length > 0) {
      this.syncFarmStatus = Sync_Status.ADDORDELETE;
    } else if (this.localloanobject.Farms.filter(p => p.ActionStatus == 2).length > 0) {
      this.syncFarmStatus = Sync_Status.EDITED;
    } else {
      this.syncFarmStatus = Sync_Status.NOCHANGE;
    }

    this.localloanobject.SyncStatus.Status_Farm = this.syncFarmStatus;
  }

  private setValidationErrorForFarm() {
    if (environment.isDebugModeActive) console.time('Farm Validation Time');

    this.validationservice.validateTableFields<Loan_Farm>(
      this.rowData,
      this.columnDefs,
      this.currentPageName,
      this.rowId,
      this.farmChevron.Chevron_Code,
      'Farm_ID'
    );

    if (environment.isDebugModeActive) console.timeEnd('Farm Validation Time');
  }

  statechanged() {

  }

  public confirm() {
    return this.syncFarmStatus == Sync_Status.ADDORDELETE;
  }

  private getCountiesByState(stateID) {
    let selection = [];
    let result = this.refdata.CountyList.filter(p => p.State_ID == stateID);

    result.forEach(cl => {
      selection.push({ key: cl.County_ID, value: cl.County_Name })
    })

    selection = _.uniqBy(selection, 'key');

    return selection;
  }

  private getLandlords() {
    let values = [];

    this.localloanobject.Association.filter(p => p.Assoc_Type_Code == 'LLD' && p.ActionStatus != 3).forEach(element => {
      values.push({ value: element.Assoc_Name, key: element.Assoc_Name });
    });

    return { values: values };

  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.preferencesubscription) {
      this.preferencesubscription.unsubscribe();
    }

    if (this.errorSubscription) {
      this.errorSubscription.unsubscribe();
    }
  }
}
