function setmodifiedall(arrayy, keyword) {

  arrayy.filter(p => !p.includes("Verf_")).forEach(obj => {
    try {
      var filter = Array.prototype.filter
      var selectedelements = document.querySelectorAll('[row-id="' + keyword + obj.split("_")[1] + '"]')
      var filtered = filter.call(selectedelements, function (node) {
        return node.childNodes.length > 0;
      });
      var splitwith = keyword + obj.split("_")[1] + '_';
      var cell = filtered[0].querySelector('[col-id="' + obj.split(splitwith)[1] + '"]');
      cell.classList.add("touched");
    }
    catch (ex) {

    }
  });
}

function setmodifiedallIns(arrayy, keyword) {
  arrayy.filter(p => !p.includes("Verf_")).forEach(obj => {
    try {
      var filter = Array.prototype.filter
      var selectedelements = document.querySelectorAll('[row-id="' + keyword + obj.split("_")[1] + '"]')
      var filtered = filter.call(selectedelements, function (node) {
        return node.childNodes.length > 0;
      });
      var splitwith = keyword + obj.split("_")[1] + '_';
      filtered.forEach(element => {
        var cell = element.querySelector('[col-id="' + obj.split(splitwith)[1] + '"]');
        if (cell != null)
          cell.classList.add("touched");
      });

    }
    catch (ex) {

    }
  });
}

function setverificationerrorall(arrayy, keyword) {
  arrayy.forEach(obj => {
    try {
      var filter = Array.prototype.filter
      var selectedelements = document.querySelectorAll('[row-id="' + keyword + obj.split("_")[1] + '"]')
      var filtered = filter.call(selectedelements, function (node) {
        return node.childNodes.length > 0;
      });
      var splitwith = keyword + obj.split("_")[1] + '_';
      var cell = filtered[0].querySelector('[col-id="' + obj.split(splitwith)[1] + '"]');
      cell.classList.add("verification-error");
    }
    catch (ex) {

    }
  });
}

function setverificationerroralloptimizer(arrayy, keyword) {
  //
  arrayy.forEach(obj => {
    try {

      var filter = Array.prototype.filter
      var selectedelements = document.querySelectorAll('[row-id="' + obj.split("_")[0] + "_" + obj.split("_")[1] + '"]')
      var filtered = filter.call(selectedelements, function (node) {
        return node.childNodes.length > 0;
      });
      var splitwith = obj.split("_")[0] + '_' + obj.split("_")[1] + "_";
      var cell = filtered[0].querySelector('[col-id="' + obj.split(splitwith)[1] + '"]');
      cell.classList.add("verification-error");
    }
    catch (ex) {

    }
  });
}

function setverificationerrorallInsurance(arrayy, keyword) {

  arrayy.forEach(obj => {
    try {
      var filter = Array.prototype.filter
      var selectedelements = document.querySelectorAll('[row-id="' + keyword + obj.split("_")[1] + '"]')
      var filtered = selectedelements;
      Array.from(filtered).forEach(element => {
        try {
          var splitwith = keyword + obj.split("_")[1] + '_';
          var cell = element.querySelector('[col-id="' + obj.split(splitwith)[1] + '"]');

          cell.classList.add("verification-error");

        }
        catch (ex) {

        }
      });

    }
    catch (ex) {

    }
  });
}

function setmodifiedsingle(obj, keyword) {
  if (!obj.includes("Verf_")) {
    try {
      var filter = Array.prototype.filter
      var selectedelements = document.querySelectorAll('[row-id="' + keyword + obj.split("_")[1] + '"]')
      var filtered = filter.call(selectedelements, function (node) {
        return node.childNodes.length > 0;
      });
      var splitwith = keyword + obj.split("_")[1] + '_';
      var cell = filtered[0].querySelector('[col-id="' + obj.split(splitwith)[1] + '"]');
      cell.classList.remove('edited-color');
      cell.classList.add("touched");
    }
    catch (ex) {

    }
  }
}
