import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { LocalStorageService } from 'ngx-webstorage';
import { ISubscription } from 'rxjs/Subscription';

import { environment } from '@env/environment.prod';

import { loan_model, LoanStatus } from '@lenda/models/loanmodel';

import { PageInfoService } from '@lenda/services/page-info.service';
import { PublishService } from '@lenda/services/publish.service';
import { AlertifyService } from '@lenda/alertify/alertify.service';

@Component({
  selector: 'app-loan-overview',
  templateUrl: './loan-overview.component.html',
  styleUrls: ['./loan-overview.component.scss']
})
export class LoanOverviewComponent implements OnInit, OnDestroy {
  loanid: string;
  localloanobject: loan_model;

  isSyncRequired: boolean;

  private publishSyncSub: ISubscription;
  private windowBeforeUnloadSub: ISubscription;

  constructor(
    private route: ActivatedRoute,
    private localst: LocalStorageService,
    private pageInfoService: PageInfoService,
    private publishService: PublishService,
    public alertify: AlertifyService
  ) {
    this.route.params.subscribe(params => {
      // Defaults to 0 if no query param provided.
      this.loanid = params['loan'].toString() + '-' + params['seq'];
      let currentloanid = this.localst.retrieve(environment.loanidkey);

      if (this.loanid != currentloanid) {
        this.localst.store(environment.loanidkey, this.loanid);
      }

      this.localloanobject = this.localst.retrieve(environment.loankey);
    });

    this.publishSyncSub = this.publishService
      .listenToSyncRequired()
      .subscribe(res => {
        this.isSyncRequired = res && res.length > 0;
      });
  }

  get hasUnsavedChanges() {
    return this.isSyncRequired;
  }

  ngOnInit() {
    this.route.firstChild.data.subscribe(data => {
      if (data.page) {
        this.pageInfoService.setCurrentPage(data.page);
      }
    });
  }

  ngOnDestroy() {
    if (this.publishSyncSub) {
      this.publishSyncSub.unsubscribe();
    }

    if (this.windowBeforeUnloadSub) {
      this.windowBeforeUnloadSub.unsubscribe();
    }
  }

  get isActiveUnprotected() {
    try {
      if (this.localloanobject) {
        return (
          this.localloanobject.LoanMaster.Is_Latest &&
          (this.loanStatus == LoanStatus.Working ||
            this.loanStatus == LoanStatus.Uploaded)
        );
      }
      return false;
    } catch {
      return false;
    }
  }

  get isActiveProtected() {
    return (
      this.loanStatus == LoanStatus.Recommended ||
      this.loanStatus == LoanStatus.Approved ||
      this.loanStatus == LoanStatus.Uploaded
    );
  }

  get isInActiveProtected() {
    return (
      this.loanStatus == LoanStatus.Void ||
      this.loanStatus == LoanStatus.Withdrawn ||
      this.loanStatus == LoanStatus.Declined ||
      this.loanStatus == LoanStatus.Historical ||
      this.loanStatus == LoanStatus.PaidOff ||
      this.loanStatus == LoanStatus.WrittenOff
    );
  }

  private get loanStatus() {
    try {
      if (this.localloanobject) {
        return this.localloanobject.LoanMaster.Loan_Status;
      }
      return '';
    } catch {
      return '';
    }
  }

  /**
   * To scroll to top as the page changes
   * @param event OnActivate event object
   */
  onActivate(event) {
    window.scroll(0, 0);
  }
}
