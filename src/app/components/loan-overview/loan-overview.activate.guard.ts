import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateChild,
  RouterStateSnapshot,
  CanActivate
} from '@angular/router';

import { Observable } from 'rxjs';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

@Injectable()
export class LoanOverviewActivateGuard implements CanActivateChild, CanActivate {
  constructor(private localstorage: LocalStorageService) {}

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> {
    let refdata = this.localstorage.retrieve(environment.referencedatakey);
    let loanobject = this.localstorage.retrieve(environment.loankey);

    if (!!refdata && !!loanobject) {
      return true;
    }

    return false;
  }
}
