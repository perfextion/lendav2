import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanDeactivate,
  RouterStateSnapshot,
  Router,
  NavigationEnd
} from '@angular/router';

import { Observable } from 'rxjs';
import { LocalStorageService } from 'ngx-webstorage';
import { JsonConvert } from 'json2typescript';

import { environment } from '@env/environment.prod';

import { Preferences } from '@lenda/preferences/models/index.model';
import { loan_model, Loan_Cross_Collateral } from '@lenda/models/loanmodel';

import { LoanApiService } from '@lenda/services/loan/loanapi.service';
import { SettingsService } from '@lenda/preferences/settings/settings.service';
import { MasterService } from '@lenda/master/master.service';
import { LoanvalidatorService } from '@lenda/services/loanvalidator.service';
import { ToasterService } from '@lenda/services/toaster.service';
import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { ApiService } from '@lenda/services';

import { LoanOverviewComponent } from './loan-overview.component';
import { ILogger } from '@lenda/Workers/utility/logger';
import { map } from 'rxjs/operators';
import { PublishService } from '@lenda/services/publish.service';

@Injectable()
export class LoanOverviewDeactivateGuard implements CanDeactivate<LoanOverviewComponent> {
  public isnavigating = false;

  constructor(
    public loanservice: LoanApiService,
    public localstorageservice: LocalStorageService,
    public settingsService: SettingsService,
    public router: Router,
    public masterService: MasterService,
    public loanvalidator: LoanvalidatorService,
    public toaster: ToasterService,
    public loancalculationservice: LoancalculationWorker,
    public apiService: ApiService,
    private publishService: PublishService
  ) {
    this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) this.isnavigating = false;
    });
  }

  canDeactivate(
    component: LoanOverviewComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (component.hasUnsavedChanges) {
      return component.alertify.confirm(
        'Confirm',
        environment.unsavedChangesWarningMessage
      ).pipe(
        map(res => {
          if(res) {
            this.publishService.syncCompleted();
            return this.validateAndNavigate(currentState, nextState);
          } else {
            return false;
          }
        })
      );
    }

    return this.validateAndNavigate(currentState, nextState);
  }

  private validateAndNavigate(
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ) {
    if (this.isnavigating == false) {
      //check here if value changed
      let oldurl = currentState.url;
      let newurl = nextState.url;
      if (oldurl.includes('loanoverview') && newurl.includes('loanoverview')) {
        let newloanid = newurl.split('/')[3] + '-' + newurl.split('/')[4];
        let currentloanid = oldurl.split('/')[3] + '-' + oldurl.split('/')[4];

        if (newloanid != currentloanid) {
          let loanid = this.localstorageservice.retrieve(environment.loanidkey);

          if (newloanid != loanid) {
            this.isnavigating = true;
            this.navigatetoloan(newloanid);
            return true;
          }
          return true;
        }
        return true;
      }
      return true;
    }
    return true;
  }

  public loanid: string;
  navigatetoloan(id: string) {
    //this.layoutService.toggleRightSidebar(false);
    this.loanid = id;
    this.localstorageservice.store(environment.loanidkey, id);
    //1 Get default landing page from my preferences
    let preferences: Preferences = this.settingsService.preferences;
    //get loan here now
    this.localstorageservice.store(environment.loanGroup, []); //reset loan group if loan to be opened is not exist in local storage
    this.getLoanBasicDetails(preferences);
  }

  getLoanBasicDetails(preferences: Preferences) {
    if (this.loanid != null) {
      let loaded = false;
      try {
        this.localstorageservice.clear(environment.loankey);
        this.localstorageservice.clear(environment.loanGroup);
      } catch (e) {
        ILogger.log(`trying to clear a non existing local storage key : ${environment.loankey}`);
      }

      this.loanservice.getLoanById(this.loanid).subscribe(res => {
        this.masterService.isMinimize = false;
        if (res.ResCode == 1) {
          this.loanvalidator.validateloanobject(res.Data).then(errors => {
            if (errors && errors.length > 0) {
              this.toaster.error('Loan Data is invalid ... Rolling Back to list');
              this.router.navigateByUrl('/home/loans');
            } else {
              //this.layoutService.toggleRightSidebar(false);
              let jsonConvert: JsonConvert = new JsonConvert();

              this.localstorageservice.store(environment.errorbase, []);
              this.loancalculationservice.saveValidationsToLocalStorage(res.Data);
              //we are making a copy of it also
              this.localstorageservice.store(environment.loankey_copy, res.Data);

              let currentPage = this.localstorageservice.retrieve(environment.currentpage);

              if (currentPage == 'loanlist' || currentPage == 'debug') {
                currentPage = preferences.userSettings.landingPage;
              }

              //loan affiliated loan, currently static
              const route = '/api/Loans/GetLoanGroups?LoanFullID=' + res.Data.Loan_Full_ID;

              this.apiService.get(route).subscribe(
                resp => {
                  if (resp) {
                    this.localstorageservice.store(
                      environment.loanGroup,
                      resp.Data
                    );

                    this.loancalculationservice.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model), true, true);

                    this.router.navigateByUrl('/home/loanoverview/' + this.loanid.replace('-', '/') + '/' + currentPage);
                  } else {
                    this.router.navigateByUrl('/home/loanoverview/' + this.loanid.replace('-', '/') + '/' + currentPage);
                  }
                },
                _ => {
                  this.router.navigateByUrl('/home/loanoverview/' + this.loanid.replace('-', '/') + '/' + currentPage);
                }
              );

              this.localstorageservice.store(environment.loanCrossCollateral, []);

              this.loanservice
                .getDistinctLoanIDList(this.loanid)
                .subscribe(resD => {
                  if (resD && resD.ResCode == 1) {
                    this.localstorageservice.store(environment.loanCrossCollateral, resD.Data as Array<Loan_Cross_Collateral>);
                  }
                });
            }
            this.localstorageservice.store(environment.exceptionStorageKey, []);
            this.localstorageservice.store(environment.modifiedbase, []);
            this.localstorageservice.store(environment.modifiedoptimizerdata, null);
          });
        } else {
          this.toaster.error('Could not fetch Loan Object from API');
          this.router.navigateByUrl('/home/loans');
        }
        loaded = true;
      });
    } else {
      this.toaster.error('Something went wrong');
      this.router.navigateByUrl('/home/loans');
    }
  }
}
