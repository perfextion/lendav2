import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Observable } from 'rxjs/Observable';

import { GetAllUsers } from '@lenda/components/committee/members/member.model';
import { loan_model } from '@lenda/models/loanmodel';

import { AlertComponent } from './components/alert.component';
import { ConfirmComponent } from './components/confirm.component';
import { CommitteStatusComponent } from './components/committee-status/committee-status.component';
import { AddNotifyPartyComponent } from './components/add-notify-party/add-notify-party.component';
import { SelectedCommitteeStatus } from '@lenda/models/committee-status/committee-status.model';
import { CopyLoanComponent } from './components/copy-loan/copy-loan.component';
import { EditLoanParamsComponent } from './components/edit-loan-parameters/edit-loan-params.component';
import { RecommendLoanComponent } from './components/recommend-loan/recommend-loan.component';
import { PendingActionGhostingComponent } from './components/pending-action-ghosting/pending-action-ghosting.component';

@Injectable({
  providedIn: 'root'
})
export class AlertifyService {
  constructor(public dialog: MatDialog) {}

  alert(title: string, description: string): Observable<any> {
    let dialogRef = this.dialog.open(AlertComponent, {
      height: 'auto',
      width: '570px',
      role: 'alertdialog',
      position: { top: '100px' },
      data: { title: title, description: description }
    });
    return dialogRef.afterClosed();
  }

  confirm(title: string, description: string, customBtn = 'confirm'): Observable<any> {
    let dialogRef = this.dialog.open(ConfirmComponent, {
      height: 'auto',
      width: '570px',
      role: 'alertdialog',
      position: { top: '100px' },
      disableClose: true,
      data: { title: title, description: description, btn: customBtn },
      autoFocus: false
    });
    return dialogRef.componentInstance.onDataRecieved;
  }

  copyLoan(loanId: string, fromCrossCollateral?: boolean): Observable<any> {
    let dialogRef = this.dialog.open(CopyLoanComponent, {
      height: 'auto',
      width: '800px',
      role: 'alertdialog',
      position: { top: '100px' },
      data: { loanId: loanId, fromCrossCollateral: fromCrossCollateral }
    });
    return dialogRef.afterClosed();
  }

  committeeStatus(title: string, committeeStatus: any): Observable<any> {
    let dialogRef = this.dialog.open(CommitteStatusComponent, {
      height: 'auto',
      width: '570px',
      role: 'alertdialog',
      position: { top: '100px' },
      data: { title: title, committeeStatus: committeeStatus }
    });
    return dialogRef.componentInstance.onDataRecieved;
  }

  openEmojiDialog(title: string): Observable<SelectedCommitteeStatus> {
    let dialogRef = this.dialog.open(CommitteStatusComponent, {
      height: 'auto',
      width: '570px',
      role: 'alertdialog',
      panelClass: 'emoji-vote-dialog',
      position: { top: '50px' },
      data: { title: title }
    });
    return dialogRef.afterClosed();
  }

  addNotifyParty(title: string, users: Array<any>): Observable<any> {
    let dialogRef = this.dialog.open(AddNotifyPartyComponent, {
      height: 'auto',
      width: '570px',
      role: 'alertdialog',
      position: { top: '100px' },
      data: { title: title, users: users }
    });
    return dialogRef.componentInstance.onDataRecieved;
  }

  editLoanParams(title: string, selectedLoan: loan_model): Observable<any> {
    let dialogRef = this.dialog.open(EditLoanParamsComponent, {
      height: 'auto',
      width: '570px',
      role: 'alertdialog',
      position: { top: '100px' },
      data: {
        title: title,
        selectedLoanMaster: selectedLoan
      }
    });
    return dialogRef.afterClosed();
  }

  openRecommendLoanDialog(
    validationCount: number,
    exceptionCount: number
  ): Observable<any> {
    let dialogRef = this.dialog.open(RecommendLoanComponent, {
      height: 'auto',
      width: '570px',
      role: 'alertdialog',
      position: { top: '100px' },
      panelClass: 'recommend-loan',
      autoFocus: false
    });

    dialogRef.componentInstance.validationCount = validationCount;
    dialogRef.componentInstance.exceptionCount = exceptionCount;

    return dialogRef.afterClosed();
  }

  /**
   * Use this method to display Confirm Dialog for deleting a record
   */
  deleteRecord(): Observable<boolean> {
    return this.confirm('Confirm', 'Please confirm to delete.');
  }

  openPendingActionGhosting(): Observable<{ res: boolean; user: GetAllUsers }> {
    let dialogRef = this.dialog.open(PendingActionGhostingComponent, {
      disableClose: true,
      autoFocus: false,
      panelClass: 'pending-action-ghosting-dialog',
      role: 'dialog',
      position: { top: '100px' }
    });
    return dialogRef.afterClosed();
  }
}
