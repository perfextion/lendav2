import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgSelectModule } from '@ng-select/ng-select';

import { SharedModule } from '@lenda/shared/shared.module';
import { MaterialModule } from '@lenda/shared/material.module';
import { UiComponentsModule } from '@lenda/ui-components/ui-components.module';

import {
  AddNotifyPartyComponent,
  CommitteStatusComponent,
  AlertComponent,
  ConfirmComponent
} from './components';
import { CopyLoanComponent } from './components/copy-loan/copy-loan.component';
import { EditLoanParamsComponent } from './components/edit-loan-parameters/edit-loan-params.component';
import { PendingActionGhostingComponent } from './components/pending-action-ghosting/pending-action-ghosting.component';
import { RecommendLoanComponent } from './components/recommend-loan/recommend-loan.component';
import { SyncValidationWarningComponent } from './components/sync-validation-warning/sync-validation-warning.component';

@NgModule({
  imports: [
    CommonModule,
    NgSelectModule,
    SharedModule,
    MaterialModule,
    UiComponentsModule
  ],
  declarations: [
    AddNotifyPartyComponent,
    CommitteStatusComponent,
    AlertComponent,
    ConfirmComponent,
    CopyLoanComponent,
    EditLoanParamsComponent,
    PendingActionGhostingComponent,
    RecommendLoanComponent,
    SyncValidationWarningComponent
  ],
  entryComponents: [
    AddNotifyPartyComponent,
    CommitteStatusComponent,
    AlertComponent,
    ConfirmComponent,
    CopyLoanComponent,
    EditLoanParamsComponent,
    PendingActionGhostingComponent,
    RecommendLoanComponent,
    SyncValidationWarningComponent
  ]
})
export class AlertifyModule {}
