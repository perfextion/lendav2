import {
  CopyLoanCollateralTypes,
  CopyLoanCommitteeTypes,
  CopyLoanOptimizerTypes,
  CopyLoanBudgetTypes,
  CopyLoanInsuranceTypes,
  CopyLoanFarmTypes,
  CopyLoanCropTypes,
  CopyLoanBorrowerTypes
} from '@lenda/models/copy-loan/copyLoanModel';

export type CopyLoanType =
  | CopyLoanBorrowerTypes
  | CopyLoanCropTypes
  | CopyLoanFarmTypes
  | CopyLoanInsuranceTypes
  | CopyLoanBudgetTypes
  | CopyLoanOptimizerTypes
  | CopyLoanCommitteeTypes
  | CopyLoanCollateralTypes;

export class CopyLoanOption<T> {
  name: string;
  value: T;
  checked: boolean;
  dataExists?: boolean;
  type: string;
};

export type BorrowerOptionsType = Array<CopyLoanOption<CopyLoanBorrowerTypes>>;
export type CropOptionsType = Array<CopyLoanOption<CopyLoanCropTypes>>;
export type FarmOptionsType = Array<CopyLoanOption<CopyLoanFarmTypes>>;
export type InsuranceOptionsType = Array<CopyLoanOption<CopyLoanInsuranceTypes>>;
export type BudgetOptionsType = Array<CopyLoanOption<CopyLoanBudgetTypes>>;
export type OptimizerOptionsType = Array<CopyLoanOption<CopyLoanOptimizerTypes>>;
export type CollateralOptionsType = Array<CopyLoanOption<CopyLoanCollateralTypes>>;
export type CommitteeOptionsType = Array<CopyLoanOption<CopyLoanCommitteeTypes>>

export type TotalType = Array<CopyLoanOption<CopyLoanType>>;

// Options
export const BorrowerOptions: BorrowerOptionsType = [
  {
    name: 'Referred From',
    value: CopyLoanBorrowerTypes.ReferredFrom,
    checked: false,
    dataExists: false,
    type: 'borrower'
  },
  {
    name: 'Credit Reference',
    value: CopyLoanBorrowerTypes.CreditReference,
    checked: false,
    dataExists: false,
    type: 'borrower'
  },
  {
    name: 'Questions',
    value: CopyLoanBorrowerTypes.Questions,
    checked: false,
    dataExists: false,
    type: 'borrower'
  },
  {
    name: 'Borrower',
    value: CopyLoanBorrowerTypes.Borrower,
    checked: false,
    dataExists: false,
    type: 'borrower'
  },
  {
    name: 'Co Borrower(s)',
    value: CopyLoanBorrowerTypes.CoBorrower,
    checked: false,
    dataExists: false,
    type: 'borrower'
  },
  {
    name: 'Farmer',
    value: CopyLoanBorrowerTypes.Farmer,
    checked: false,
    dataExists: false,
    type: 'borrower'
  },
  {
    name: 'Credit Score',
    value: CopyLoanBorrowerTypes.CreditScore,
    checked: false,
    dataExists: false,
    type: 'borrower'
  },
  {
    name: 'Balance Sheet',
    value: CopyLoanBorrowerTypes.BalanceSheet,
    checked: false,
    dataExists: false,
    type: 'borrower'
  },
  {
    name: 'Income History',
    value: CopyLoanBorrowerTypes.IncomeHistory,
    checked: false,
    dataExists: false,
    type: 'borrower'
  }
];

export const CropOptions: CropOptionsType = [
  {
    name: 'Buyer',
    value: CopyLoanCropTypes.Buyer,
    checked: false,
    type: 'crop'
  },
  {
    name: 'Rebator',
    value: CopyLoanCropTypes.Rebator,
    checked: false,
    type: 'crop'
  },
  {
    name: 'Questions',
    value: CopyLoanCropTypes.Questions,
    checked: false,
    type: 'crop'
  },
  {
    name: 'Yield',
    value: CopyLoanCropTypes.Yield,
    checked: false,
    type: 'crop'
  },
  {
    name: 'Other Income',
    value: CopyLoanCropTypes.OtherIncome,
    checked: false,
    type: 'crop'
  },
  {
    name: 'Contracts',
    value: CopyLoanCropTypes.Contracts,
    checked: false,
    type: 'collateral'
  }
];

export const FarmOptions: FarmOptionsType = [
  {
    name: 'Landlord',
    value: CopyLoanFarmTypes.Landlord,
    checked: false,
    type: 'farm'
  },
  {
    name: 'Farm',
    value: CopyLoanFarmTypes.Farm,
    checked: false,
    type: 'farm'
  }
];

export const InsuranceOptions: InsuranceOptionsType = [
  {
    name: 'Agency',
    value: CopyLoanInsuranceTypes.Agency,
    checked: false,
    type: 'insurance'
  },
  {
    name: 'AIP',
    value: CopyLoanInsuranceTypes.AIP,
    checked: false,
    type: 'insurance'
  },
  {
    name: 'Questions',
    value: CopyLoanInsuranceTypes.Questions,
    checked: false,
    type: 'insurance'
  },
  {
    name: 'Policy',
    value: CopyLoanInsuranceTypes.Policy,
    checked: false,
    type: 'insurance'
  },
  {
    name: 'APH',
    value: CopyLoanInsuranceTypes.APH,
    checked: false,
    type: 'insurance'
  }
];

export const BudgetOptions: BudgetOptionsType = [
  {
    name: 'Distributer',
    value: CopyLoanBudgetTypes.Distributer,
    checked: false,
    type: 'budget'
  },
  {
    name: 'Third Party',
    value: CopyLoanBudgetTypes.ThirdParty,
    checked: false,
    type: 'budget'
  },
  {
    name: 'Harvester',
    value: CopyLoanBudgetTypes.Harvester,
    checked: false,
    type: 'budget'
  },
  {
    name: 'Equipment Provider',
    value: CopyLoanBudgetTypes.EquipmentProvider,
    checked: false,
    type: 'budget'
  },
  {
    name: 'Questions',
    value: CopyLoanBudgetTypes.Questions,
    checked: false,
    type: 'budget'
  },
  {
    name: 'Crop Budgets',
    value: CopyLoanBudgetTypes.CropBudgets,
    checked: false,
    type: 'budget'
  },
  {
    name: 'Credit Notes',
    value: CopyLoanBudgetTypes.CreditNotes,
    checked: false,
    type: 'budget'
  }
];

export const OptimizerOptions: OptimizerOptionsType = [
  {
    name: 'Irr Acres',
    value: CopyLoanOptimizerTypes.IrrAcres,
    checked: false,
    type: 'optimizer'
  },
  {
    name: 'NI Acres',
    value: CopyLoanOptimizerTypes.NIAcres,
    checked: false,
    type: 'optimizer'
  }
];

export const CollateralOptions: CollateralOptionsType = [
  {
    name: 'Lienholder',
    value: CopyLoanCollateralTypes.Lienholder,
    checked: false,
    type: 'collateral'
  },
  {
    name: 'Guarantor',
    value: CopyLoanCollateralTypes.Guarantor,
    checked: false,
    type: 'collateral'
  },
  {
    name: 'Questions',
    value: CopyLoanCollateralTypes.Questions,
    checked: false,
    type: 'collateral'
  },
  {
    name: 'Additional Collateral',
    value: CopyLoanCollateralTypes.AdditionalCollateral,
    checked: false,
    type: 'collateral'
  }
];

export const CommitteeOptions: CommitteeOptionsType = [
  {
    name: 'Terms',
    value: CopyLoanCommitteeTypes.Terms,
    checked: false,
    type: 'committee'
  },
  {
    name: 'Loan Comment',
    value: CopyLoanCommitteeTypes.LoanComment,
    checked: false,
    type: 'committee'
  }
];
