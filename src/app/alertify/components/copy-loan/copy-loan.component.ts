import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CopyLoanService } from './copy-loan.service';
import {
  copy_loan_model,
  CopyLoanBorrowerTypes,
  CopyLoanCropTypes,
  CopyLoanFarmTypes,
  CopyLoanInsuranceTypes,
  CopyLoanBudgetTypes,
  CopyLoanOptimizerTypes,
  CopyLoanCollateralTypes,
  CopyLoanCommitteeTypes,
  CopyLoanTabs
} from '../../../models/copy-loan/copyLoanModel';
import * as OPTIONS from './copy-loan-options';
import { copy_loan_data } from '../../../models/copy-loan/copyLoanData';
import { copy_loan_input } from '../../../models/copy-loan/copyLoanInput';
import {
  loan_model,
  AssociationTypeCode,
  Cross_Collateralized_Loans
} from '../../../models/loanmodel';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '../../../../environments/environment.prod';
import { Chevron, RefQuestions, LoanQResponse } from '../../../models/loan-response.model';
import { DatePipe } from '@angular/common';
import { Get_Distributor_Name } from '@lenda/services/common-utils';
import { LoanMaster } from '@lenda/models/ref-data-model';

@Component({
  selector: 'copy-loan',
  templateUrl: 'copy-loan.component.html',
  styleUrls: ['copy-loan.component.scss'],
  providers: [DatePipe]
})
export class CopyLoanComponent {
  model: copy_loan_model;
  modelData: copy_loan_data;
  modelInput: copy_loan_input;
  // copyLoanFormGroup: FormGroup;
  selectAll = false;

  borrowerOptions: OPTIONS.BorrowerOptionsType;
  selectAllBorrower = false;

  cropOptions: OPTIONS.CropOptionsType;
  selectAllCrop = false;

  farmOptions: OPTIONS.FarmOptionsType;
  selectAllFarm = false;

  insuranceOptions: OPTIONS.InsuranceOptionsType;
  selectAllInsurance = false;

  budgetOptions: OPTIONS.BudgetOptionsType;
  selectAllBudget = false;

  optimizerOptions: OPTIONS.OptimizerOptionsType;
  selectAllOptimizer = false;

  collateralOptions: OPTIONS.CollateralOptionsType;
  selectAllCollateral = false;

  committeeOptions: OPTIONS.CommitteeOptionsType;
  selectAllCommittee = false;

  public localloanobject: loan_model = new loan_model();
  public Chevron: typeof Chevron = Chevron;
  public warningMessage: string = '';//'Warning: Existing data will be overwritten.';
  public loanFullId: string;
  headerLabel: string = 'Copy Previous Loan';

  collateralizeItems: Array<Cross_Collateralized_Loans> = [];

  constructor(
    public dialogRef: MatDialogRef<CopyLoanComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private localStorageService: LocalStorageService,
    private copyLoanService: CopyLoanService,
    private datePipe: DatePipe
  ) {
    this.model = new copy_loan_model();
    this.modelData = new copy_loan_data();
    this.modelInput = new copy_loan_input();
  }

  get showContent() {
    return (
      (this.data.fromCrossCollateral && this.collateralizeItems.length > 0) || (!this.data.fromCrossCollateral && this.localloanobject.LoanMaster.Prev_Year_Loan_Full_ID)
    );
  }

  get LoanTypeAndDistCode() {
    return this.loanType(this.localloanobject.LoanMaster);
  }

  loanType(loan) {
    return `${loan.Loan_Type_Name == 'Ag-Input' ? (loan.Loan_Type_Name + ' (' + Get_Distributor_Name(loan, false) +')') : loan.Loan_Type_Name}`;
  }

  ngOnInit() {
    this.localloanobject = this.localStorageService.retrieve(environment.loankey);

    if (this.data && this.data.fromCrossCollateral) {
      this.headerLabel = 'Copy Cross Collateralized Loan';
      this.collateralizeItems = this.localloanobject.Cross_Collateralized_Loans;
      this.prepareLoanList();
    }

    this.model.ToLoanFullID = this.data.loanId || this.localloanobject.LoanMaster.Loan_Full_ID;
    this.model.FromLoanFullID = this.data && !this.data.fromCrossCollateral ? this.localloanobject.LoanMaster.Prev_Year_Loan_Full_ID : '';

    //Set Options Starts
    this.setCopyLoanOptions();
    //Set Options Ends

    this.resetAllCheckboxes();
  }

  private setCopyLoanOptions() {
    this.borrowerOptions = OPTIONS.BorrowerOptions;
    this.cropOptions = OPTIONS.CropOptions;
    this.farmOptions = OPTIONS.FarmOptions;
    this.insuranceOptions = OPTIONS.InsuranceOptions;
    this.budgetOptions = OPTIONS.BudgetOptions;
    this.optimizerOptions = OPTIONS.OptimizerOptions;
    this.collateralOptions = OPTIONS.CollateralOptions;
    this.committeeOptions = OPTIONS.CommitteeOptions;
  }

  private resetAllCheckboxes() {
    this.reset(this.borrowerOptions);
    this.reset(this.cropOptions);
    this.reset(this.farmOptions);
    this.reset(this.insuranceOptions);
    this.reset(this.budgetOptions);
    this.reset(this.optimizerOptions);
    this.reset(this.collateralOptions);
    this.reset(this.committeeOptions);
  }

  private reset(options: Array<any>) {
    if(options) {
      options.forEach(opt => {
        opt.checked = false;
      });
    }
  }

  updateQuestionDataExists(option, obj: Array<LoanQResponse>) {
    if (obj.length > 0 && obj.some(a => !!a.Response_Detail)) {
      option.dataExists = true;
    } else {
      option.dataExists = false;
    }
  }

  updateOptionsDataExists(option, obj): void {
    if (obj.length > 0) {
      option.dataExists = true;
    } else {
      option.dataExists = false;
    }
  }

  checkIfValueExists(option: any): void {
    if (this.localloanobject && option.checked == true) {
      if (!this.model.IsNew) {
        //Borrower
        if (option.value == CopyLoanBorrowerTypes.ReferredFrom) {
          let obj = this.Get_Associations(AssociationTypeCode.ReferredFrom);
          this.updateOptionsDataExists(option, obj);
        } else if (option.value == CopyLoanBorrowerTypes.CreditReference) {
          let obj = this.Get_Associations(AssociationTypeCode.CreditRererrence);
          this.updateOptionsDataExists(option, obj);
        } else if (
          option.value == CopyLoanBorrowerTypes.Questions &&
          option.type == 'borrower'
        ) {
          let obj = this.Get_Question(Chevron.Borrower);
          this.updateQuestionDataExists(option, obj);
        } else if (option.value == CopyLoanBorrowerTypes.Borrower) {
          if (
            this.localloanobject.LoanMaster &&
            Object.keys(this.localloanobject.LoanMaster).length > 0
          ) {
            option.dataExists = true;
          } else {
            option.dataExists = false;
          }
        } else if (option.value == CopyLoanBorrowerTypes.CoBorrower) {
          if (
            this.localloanobject.CoBorrower &&
            this.localloanobject.CoBorrower.length > 0
          ) {
            option.dataExists = true;
          } else {
            option.dataExists = false;
          }
        } else if (option.value == CopyLoanBorrowerTypes.Farmer) {
          if (this.Check_Farmer(this.localloanobject.LoanMaster)) {
            option.dataExists = true;
          } else {
            option.dataExists = false;
          }
        } else if (option.value == CopyLoanBorrowerTypes.CreditScore) {
          if (
            (this.localloanobject.LoanMaster.Credit_Score &&
              this.localloanobject.LoanMaster.Credit_Score > 0) ||
            this.localloanobject.LoanMaster.Credit_Score_Date
          ) {
            option.dataExists = true;
          } else {
            option.dataExists = false;
          }
        } else if (option.value == CopyLoanBorrowerTypes.BalanceSheet) {
          if (this.CheckBalanceSheet()) {
            option.dataExists = true;
          } else {
            option.dataExists = false;
          }
        } else if (option.value == CopyLoanBorrowerTypes.IncomeHistory) {
          let obj = this.localloanobject.BorrowerIncomeHistory ? this.localloanobject.BorrowerIncomeHistory.filter(a => a.ActionStatus != 3 && (
            a.Borrower_Expense > 0 || a.Borrower_Revenue > 0
          )) : [];
          this.updateOptionsDataExists(option, obj);
        }
        //Crops
        else if (option.value == CopyLoanCropTypes.Rebator) {
          let obj = this.Get_Associations(AssociationTypeCode.Rebator);
          this.updateOptionsDataExists(option, obj);
        } else if (option.value == CopyLoanCollateralTypes.Buyer) {
          let obj = this.Get_Associations(AssociationTypeCode.Buyer);
          this.updateOptionsDataExists(option, obj);
        } else if (
          option.value == CopyLoanCropTypes.Questions &&
          option.type == 'crop'
        ) {
          let obj = this.Get_Question(Chevron.Crop);
          this.updateQuestionDataExists(option, obj);
        } else if (option.value == CopyLoanCropTypes.Yield) {
          if (this.localloanobject.CropYield.length > 0) {
            option.dataExists = true;
          } else {
            option.dataExists = false;
          }
        } else if (option.value == CopyLoanCropTypes.Price) {
          let obj = this.localloanobject.LoanCrops ? this.localloanobject.LoanCrops.filter(p => p.ActionStatus != 3) : [];
          this.updateOptionsDataExists(option, obj);
        } else if (option.value == CopyLoanCropTypes.OtherIncome) {
          let obj = this.localloanobject.LoanOtherIncomes ? this.localloanobject.LoanOtherIncomes.filter(p => p.ActionStatus != 3) : [];
          this.updateOptionsDataExists(option, obj);
        }
        //Farm
        else if (option.value == CopyLoanFarmTypes.Landlord) {
          let obj = this.Get_Associations(AssociationTypeCode.Landlord);
          this.updateOptionsDataExists(option, obj);
        } else if (
          option.value == CopyLoanFarmTypes.Questions &&
          option.type == 'farm'
        ) {
          let obj = this.Get_Question(Chevron.Farm);
          this.updateQuestionDataExists(option, obj);
        } else if (option.value == CopyLoanFarmTypes.Farm) {
          let obj = this.localloanobject.Farms ? this.localloanobject.Farms.filter(
            p => p.ActionStatus != 3
          ): [];
          this.updateOptionsDataExists(option, obj);
        }
        //Insurance
        else if (option.value == CopyLoanInsuranceTypes.Agency) {
          let obj =this.Get_Associations(AssociationTypeCode.Agency);
          this.updateOptionsDataExists(option, obj);
        } else if (option.value == CopyLoanInsuranceTypes.AIP) {
          let obj = this.Get_Associations(AssociationTypeCode.AIP);
          this.updateOptionsDataExists(option, obj);
        } else if (
          option.value == CopyLoanInsuranceTypes.Questions &&
          option.type == 'insurance'
        ) {
          let obj = this.localloanobject.LoanQResponse.filter(
            p =>
              p.ActionStatus != 3 &&
              (p.Chevron_ID == Chevron.Insurance ||
                p.Chevron_ID == Chevron.Insurance_ABC ||
                p.Chevron_ID == Chevron.Insurance_CROPHAIL ||
                p.Chevron_ID == Chevron.Insurance_HMAX ||
                p.Chevron_ID == Chevron.Insurance_ICE ||
                p.Chevron_ID == Chevron.Insurance_MPCI ||
                p.Chevron_ID == Chevron.Insurance_RAMP ||
                p.Chevron_ID == Chevron.Insurance_WFRP)
          );
          this.updateOptionsDataExists(option, obj);
        } else if (option.value == CopyLoanInsuranceTypes.Policy) {
          if (this.localloanobject.LoanPolicies.length > 0) {
            option.dataExists = true;
          } else {
            option.dataExists = false;
          }
        } else if (option.value == CopyLoanInsuranceTypes.APH) {
          let obj = this.localloanobject.LoanCropUnits.filter(
            p => p.ActionStatus != 3 && p.Ins_APH > 0
          );
          this.updateOptionsDataExists(option, obj);
        }
        //Budget
        else if (option.value == CopyLoanBudgetTypes.Distributer) {
          let obj =this.Get_Associations(AssociationTypeCode.Distributor);
          this.updateOptionsDataExists(option, obj);
        } else if (option.value == CopyLoanBudgetTypes.ThirdParty) {
          let obj = this.Get_Associations(AssociationTypeCode.ThirdParty);
          this.updateOptionsDataExists(option, obj);
        } else if (option.value == CopyLoanBudgetTypes.Harvester) {
          let obj = this.Get_Associations(AssociationTypeCode.Harvester);
          this.updateOptionsDataExists(option, obj);
        } else if (option.value == CopyLoanBudgetTypes.EquipmentProvider) {
          let obj = this.Get_Associations(AssociationTypeCode.EquipmentProvider);
          this.updateOptionsDataExists(option, obj);
        } else if (
          option.value == CopyLoanBudgetTypes.Questions &&
          option.type == 'budget'
        ) {
          let obj = this.Get_Question(Chevron.Budget);
          this.updateQuestionDataExists(option, obj);
        } else if (option.value == CopyLoanBudgetTypes.CropBudgets) {
          let obj = this.localloanobject.LoanBudget.filter(
            p =>
              p.Crop_Practice_ID == 0 &&
              p.Budget_Type == 'V' &&
              p.ActionStatus != 3 &&
              p.Total_Budget_Crop_ET > 0
          );
          this.updateOptionsDataExists(option, obj);
        } else if (option.value == CopyLoanBudgetTypes.CreditNotes) {
          let obj = this.localloanobject.LoanBudget.filter(
            p =>
              p.Notes &&
              p.Notes.trim().length > 0 &&
              p.Budget_Type == 'V' &&
              p.ActionStatus != 3
          );
          this.updateOptionsDataExists(option, obj);
        }
        //Optimizer
        else if (option.value == CopyLoanOptimizerTypes.IrrAcres) {
          let obj = this.localloanobject.LoanCropUnits.filter(
            p => p.Crop_Practice_Type_Code == 'Irr' && p.ActionStatus != 3 && p.CU_Acres > 0
          );
          this.updateOptionsDataExists(option, obj);
        } else if (option.value == CopyLoanOptimizerTypes.NIAcres) {
          let obj = this.localloanobject.LoanCropUnits.filter(
            p => p.Crop_Practice_Type_Code == 'NI' && p.ActionStatus != 3 && p.CU_Acres > 0
          );
          this.updateOptionsDataExists(option, obj);
        }
        //Collateral
        else if (option.value == CopyLoanCollateralTypes.Lienholder) {
          let obj = this.Get_Associations(AssociationTypeCode.LienHolder);
          this.updateOptionsDataExists(option, obj);
        } else if (option.value == CopyLoanCollateralTypes.Guarantor) {
          let obj = this.Get_Associations(AssociationTypeCode.Guarantor);
          this.updateOptionsDataExists(option, obj);
        } else if (
          option.value == CopyLoanCollateralTypes.Questions &&
          option.type == 'collateral'
        ) {
          let obj = this.Get_Question(Chevron.Collateral);
          this.updateQuestionDataExists(option, obj);
        } else if (
          option.value == CopyLoanCollateralTypes.AdditionalCollateral
        ) {
          let obj = this.localloanobject.LoanCollateral.filter(
            a =>
              a.Net_Market_Value > 0 &&
              a.ActionStatus != 3
          );
          this.updateOptionsDataExists(option, obj);
        } else if (option.value == CopyLoanCropTypes.Contracts) {
          let obj = this.localloanobject.LoanMarketingContracts.filter(
            a =>
              a.Market_Value > 0 &&
              a.ActionStatus != 3
          );
          this.updateOptionsDataExists(option, obj);
        }
        //Committee
        else if (option.value == CopyLoanCommitteeTypes.Terms) {
          if (this.CheckTerms()) {
            option.dataExists = true;
          } else {
            option.dataExists = false;
          }
        } else if (option.value == CopyLoanCommitteeTypes.LoanComment) {
          if (
            this.localloanobject.LoanMaster.Loan_Comment &&
            this.localloanobject.LoanMaster.Loan_Comment.length > 0
          ) {
            option.dataExists = true;
          } else {
            option.dataExists = false;
          }
        }
      }
    }
  }

  private CheckTerms() {
    return this.localloanobject.LoanMaster.Maturity_Date ||
      this.localloanobject.LoanMaster.Close_Date ||
      this.localloanobject.LoanMaster.Origination_Fee_Percent ||
      this.localloanobject.LoanMaster.Extension_Fee_Percent ||
      this.localloanobject.LoanMaster.Service_Fee_Percent ||
      this.localloanobject.LoanMaster.Interest_Percent ||
      this.localloanobject.LoanMaster.Interest_Est_Amount;
  }

  private CheckBalanceSheet() {
    return this.localloanobject.LoanMaster.CPA_Prepared_Financials ||
      this.localloanobject.LoanMaster.Borrower_3yr_Tax_Returns ||
      this.localloanobject.LoanMaster.Financials_Date ||
      this.localloanobject.LoanMaster.Current_Assets ||
      this.localloanobject.LoanMaster.Current_Assets_Disc_Percent ||
      this.localloanobject.LoanMaster.FC_Current_Adjvalue ||
      this.localloanobject.LoanMaster.Current_Liabilities ||
      this.localloanobject.LoanMaster.Current_Disc_Net_Worth ||
      this.localloanobject.LoanMaster.Inter_Assets ||
      this.localloanobject.LoanMaster.Inter_Assets_Disc_Percent ||
      this.localloanobject.LoanMaster.FC_Inter_Adjvalue ||
      this.localloanobject.LoanMaster.Inter_Liabilities ||
      this.localloanobject.LoanMaster.Inter_Disc_Net_Worth ||
      this.localloanobject.LoanMaster.Fixed_Assets ||
      this.localloanobject.LoanMaster.Fixed_Assets_Disc_Percent ||
      this.localloanobject.LoanMaster.FC_Fixed_Adjvalue ||
      this.localloanobject.LoanMaster.Fixed_Liabilities ||
      this.localloanobject.LoanMaster.Fixed_Disc_Net_Worth;
  }

  //Toggel All Checkbox
  toggelAll() {
    if (this.selectAll === true) {
      this.model.IsFull = true;

      //Borrower Start
      this.selectAllBorrower = true;
      this.borrowerOptions.map(option => {
        option.checked = true;
        this.checkIfValueExists(option);
      });
      //Borrower End

      //Crops Start
      this.selectAllCrop = true;
      this.cropOptions.map(option => {
        option.checked = true;
        this.checkIfValueExists(option);
      });
      //Crops End

      //Farms Start
      this.selectAllFarm = true;
      this.farmOptions.map(option => {
        option.checked = true;
        this.checkIfValueExists(option);
      });
      //Farms End

      //Insurances Start
      this.selectAllInsurance = true;
      this.insuranceOptions.map(option => {
        option.checked = true;
        this.checkIfValueExists(option);
      });
      //Insurances End

      //Budgets Start
      this.selectAllBudget = true;
      this.budgetOptions.map(option => {
        option.checked = true;
        this.checkIfValueExists(option);
      });
      //Budgets End

      //Optimizers Start
      this.selectAllOptimizer = true;
      this.optimizerOptions.map(option => {
        option.checked = true;
        this.checkIfValueExists(option);
      });
      //Optimizers End

      //Collaterals Start
      this.selectAllCollateral = true;
      this.collateralOptions.map(option => {
        option.checked = true;
        this.checkIfValueExists(option);
      });
      //Collaterals End

      //Committees Start
      this.selectAllCommittee = true;
      this.committeeOptions.map(option => {
        option.checked = true;
        this.checkIfValueExists(option);
      });
      //Cptimizers End
    } else {
      this.model.IsFull = false;

      //Borrower Start
      this.selectAllBorrower = false;
      this.borrowerOptions.map(option => {
        option.checked = false;
        option.dataExists = false;
      });
      //Borrower Start

      //Crops Start
      this.selectAllCrop = false;
      this.cropOptions.map(option => {
        option.checked = false;
        option.dataExists = false;
      });
      //Crops Start

      //Farms Start
      this.selectAllFarm = false;
      this.farmOptions.map(option => {
        option.checked = false;
        option.dataExists = false;
      });
      //Farms End

      //Insurances Start
      this.selectAllInsurance = false;
      this.insuranceOptions.map(option => {
        option.checked = false;
        option.dataExists = false;
      });
      //Insurances End

      //Budgets Start
      this.selectAllBudget = false;
      this.budgetOptions.map(option => {
        option.checked = false;
        option.dataExists = false;
      });
      //Budgets End

      //Optimizers Start
      this.selectAllOptimizer = false;
      this.optimizerOptions.map(option => {
        option.checked = false;
        option.dataExists = false;
      });
      //Optimizers End

      //Collaterals Start
      this.selectAllCollateral = false;
      this.collateralOptions.map(option => {
        option.checked = false;
        option.dataExists = false;
      });
      //Collaterals End

      //Committees Start
      this.selectAllCommittee = false;
      this.committeeOptions.map(option => {
        option.checked = false;
        option.dataExists = false;
      });
      //Committees End
    }
  }

  toggelAllBorrowers() {
    if (this.selectAllBorrower === true) {
      this.borrowerOptions.map(option => {
        option.checked = true;
        this.checkAllLoanInputSelected();
        this.checkIfValueExists(option);
      });
    } else {
      this.borrowerOptions.map(option => {
        option.checked = false;
        this.selectAll = false;
        option.dataExists = false;
      });
    }
  }

  toggelAllCrops() {
    if (this.selectAllCrop === true) {
      this.cropOptions.map(option => {
        option.checked = true;
        this.checkAllLoanInputSelected();
        this.checkIfValueExists(option);
      });
    } else {
      this.cropOptions.map(option => {
        option.checked = false;
        this.selectAll = false;
        option.dataExists = false;
      });
    }
  }

  toggelAllFarms() {
    if (this.selectAllFarm === true) {
      this.farmOptions.map(option => {
        option.checked = true;
        this.checkAllLoanInputSelected();
        this.checkIfValueExists(option);
      });
    } else {
      this.farmOptions.map(option => {
        option.checked = false;
        this.selectAll = false;
        option.dataExists = false;
      });
    }
  }

  toggelAllInsurances() {
    if (this.selectAllInsurance === true) {
      this.insuranceOptions.map(option => {
        option.checked = true;
        this.checkAllLoanInputSelected();
        this.checkIfValueExists(option);
      });
    } else {
      this.insuranceOptions.map(option => {
        option.checked = false;
        this.selectAll = false;
        option.dataExists = false;
      });
    }
  }

  toggelAllBudgets() {
    if (this.selectAllBudget === true) {
      this.budgetOptions.map(option => {
        option.checked = true;
        this.checkAllLoanInputSelected();
        this.checkIfValueExists(option);
      });
    } else {
      this.budgetOptions.map(option => {
        option.checked = false;
        this.selectAll = false;
        option.dataExists = false;
      });
    }
  }

  toggelAllOptimizers() {
    if (this.selectAllOptimizer === true) {
      this.optimizerOptions.map(option => {
        option.checked = true;
        this.checkAllLoanInputSelected();
        this.checkIfValueExists(option);
      });
    } else {
      this.optimizerOptions.map(option => {
        option.checked = false;
        this.selectAll = false;
        option.dataExists = false;
      });
    }
  }

  toggelAllCollaterals() {
    if (this.selectAllCollateral === true) {
      this.collateralOptions.map(option => {
        option.checked = true;
        this.checkAllLoanInputSelected();
        this.checkIfValueExists(option);
      });
    } else {
      this.collateralOptions.map(option => {
        option.checked = false;
        this.selectAll = false;
        option.dataExists = false;
      });
    }
  }

  toggelAllCommittees() {
    if (this.selectAllCommittee === true) {
      this.committeeOptions.map(option => {
        option.checked = true;
        this.checkAllLoanInputSelected();
        this.checkIfValueExists(option);
      });
    } else {
      this.committeeOptions.map(option => {
        option.checked = false;
        this.selectAll = false;
        option.dataExists = false;
      });
    }
  }

  toggelCheckbox(obj, type, total): void {
    if (total.filter(item => item.checked == false).length > 0) {
      if (type == 'borrowers') {
        this.selectAllBorrower = false;
      } else if (type == 'crops') {
        this.selectAllCrop = false;
      } else if (type == 'farms') {
        this.selectAllFarm = false;
      } else if (type == 'insurances') {
        this.selectAllInsurance = false;
      } else if (type == 'budgets') {
        this.selectAllBudget = false;
      } else if (type == 'optimizers') {
        this.selectAllOptimizer = false;
      } else if (type == 'collaterals') {
        this.selectAllCollateral = false;
      } else if (type == 'committees') {
        this.selectAllCommittee = false;
      }
      this.selectAll = false;
    } else {
      if (type == 'borrowers') {
        this.selectAllBorrower = true;
      } else if (type == 'crops') {
        this.selectAllCrop = true;
      } else if (type == 'farms') {
        this.selectAllFarm = true;
      } else if (type == 'insurances') {
        this.selectAllInsurance = true;
      } else if (type == 'budgets') {
        this.selectAllBudget = true;
      } else if (type == 'optimizers') {
        this.selectAllOptimizer = true;
      } else if (type == 'collaterals') {
        this.selectAllCollateral = true;
      } else if (type == 'committees') {
        this.selectAllCommittee = true;
      }
    }
    if (obj.checked == false) {
      obj.dataExists = false;
    }
    this.checkIfValueExists(obj);
    this.checkAllLoanInputSelected();
  }

  private checkSelectAll(type: CopyLoanTabs, total: OPTIONS.TotalType) {
    if (total.every(item => item.checked)) {
      if (type == 'borrowers') {
        this.selectAllBorrower = true;
      } else if (type == 'crops') {
        this.selectAllCrop = true;
      } else if (type == 'farms') {
        this.selectAllFarm = true;
      } else if (type == 'insurances') {
        this.selectAllInsurance = true;
      } else if (type == 'budgets') {
        this.selectAllBudget = true;
      } else if (type == 'optimizers') {
        this.selectAllOptimizer = true;
      } else if (type == 'collaterals') {
        this.selectAllCollateral = true;
      } else if (type == 'committees') {
        this.selectAllCommittee = true;
      }
    }
  }

  checkAllLoanInputSelected() {
    if (
      this.borrowerOptions.filter(item => item.checked == false).length > 0 ||
      this.cropOptions.filter(item => item.checked == false).length > 0 ||
      this.farmOptions.filter(item => item.checked == false).length > 0 ||
      this.insuranceOptions.filter(item => item.checked == false).length > 0 ||
      this.budgetOptions.filter(item => item.checked == false).length > 0 ||
      this.optimizerOptions.filter(item => item.checked == false).length > 0 ||
      this.collateralOptions.filter(item => item.checked == false).length > 0 ||
      this.committeeOptions.filter(item => item.checked == false).length > 0
    ) {
      this.selectAll = false;
    } else {
      this.selectAll = true;
    }
  }

  onPrevLoanClick(): void {
    this.model.IsNew = !this.model.IsNew;
    if (this.model.IsNew) {
      this.borrowerOptions.forEach(option => {
        this.checkIfValueExists(option);
      });

      this.cropOptions.forEach(option => {
        this.checkIfValueExists(option);
      });

      this.farmOptions.forEach(option => {
        this.checkIfValueExists(option);
      });

      this.insuranceOptions.forEach(option => {
        this.checkIfValueExists(option);
      });

      this.budgetOptions.forEach(option => {
        this.checkIfValueExists(option);
      });

      this.optimizerOptions.forEach(option => {
        this.checkIfValueExists(option);
      });

      this.collateralOptions.forEach(option => {
        this.checkIfValueExists(option);
      });

      this.committeeOptions.forEach(option => {
        this.checkIfValueExists(option);
      });
    } else {
      this.borrowerOptions.forEach(option => {
        option.dataExists = false;
      });

      this.cropOptions.forEach(option => {
        option.dataExists = false;
      });

      this.farmOptions.forEach(option => {
        option.dataExists = false;
      });

      this.insuranceOptions.forEach(option => {
        option.dataExists = false;
      });

      this.budgetOptions.forEach(option => {
        option.dataExists = false;
      });

      this.optimizerOptions.forEach(option => {
        option.dataExists = false;
      });

      this.collateralOptions.forEach(option => {
        option.dataExists = false;
      });

      this.committeeOptions.forEach(option => {
        option.dataExists = false;
      });
    }
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
    this.model.Data = this.getLoanModel();

    if(this.selectAll) {
      this.model.IsFull = true;
    }

    this.copyLoanService.Perform_Loan_Copy(this.model);
  }

  getLoanModel() {
    this.modelData = new copy_loan_data();
    this.modelInput = new copy_loan_input();

    this.borrowerOptions.forEach(option => {
      if (option.checked) {
        this.modelInput.borrowers[option.value] = true;
      }
    });

    this.cropOptions.forEach(option => {
      if (option.checked) {
        this.modelInput.crops[option.value] = true;
      }
    });

    this.farmOptions.forEach(option => {
      if (option.checked) {
        this.modelInput.farms[option.value] = true;
      }
    });

    this.insuranceOptions.forEach(option => {
      if (option.checked) {
        this.modelInput.insurances[option.value] = true;
      }
    });

    this.budgetOptions.forEach(option => {
      if (option.checked) {
        this.modelInput.budgets[option.value] = true;
      }
    });

    this.optimizerOptions.forEach(option => {
      if (option.checked) {
        this.modelInput.optimizers[option.value] = true;
      }
    });

    this.collateralOptions.forEach(option => {
      if (option.checked) {
        this.modelInput.collaterals[option.value] = true;
      }
    });

    this.committeeOptions.forEach(option => {
      if (option.checked) {
        this.modelInput.committees[option.value] = true;
      }
    });

    this.modelData.loanInputs = this.modelInput;
    return this.modelData;
  }

  private prepareLoanList() {
    this.collateralizeItems.forEach(loan => {
      loan.View = loan.Loan_Full_ID +'; ' + loan.Borrower +'; ' + loan.Crop_Year  + ' ' + (this.loanType(loan) || '')
    });
  }

  private Get_Associations(Assoc_Type_Code: AssociationTypeCode) {
    if(this.localloanobject.Association) {
      return this.localloanobject.Association.filter(
        p =>
          p.ActionStatus != 3 &&
          p.Assoc_Type_Code == Assoc_Type_Code
      );
    } else {
      return [];
    }
  }

  private Check_Farmer(LM: LoanMaster) {
    if(LM) {
      return Object.keys(LM)
        .filter(
          a =>
            a != 'Farmer_ID' &&
            (a.includes('Farmer_') || a.includes('Year_Begin'))
        ).some(a => LM[a]);
    } else {
      return false;
    }
  }

  private Get_Question(Chevron_ID: number) {
    if(this.localloanobject.LoanQResponse) {
      return this.localloanobject.LoanQResponse.filter(
        a =>
          a.ActionStatus != 3 &&
          a.Chevron_ID == Chevron_ID
      );
    } else {
      return [];
    }
  }

  private transformDate(date) {
    return this.datePipe.transform(date, 'MM/dd/yyyy');
  }
}
