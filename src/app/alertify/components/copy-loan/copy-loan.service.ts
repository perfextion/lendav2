import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { LoggingService } from '../../../services/Logs/logging.service';
import { LoancalculationWorker } from '../../../Workers/calculations/loancalculationworker';
import { LoanApiService } from '../../../services/loan/loanapi.service';
import { ToasterService } from '../../../services/toaster.service';
import { copy_loan_model } from '../../../models/copy-loan/copyLoanModel';
import { JsonConvert } from 'json2typescript';
import { loan_model } from '@lenda/models/loanmodel';

@Injectable({
  providedIn: 'root'
})
export class CopyLoanService {

  constructor(
    public localstorageservice: LocalStorageService,
    public logging: LoggingService,
    public loanserviceworker: LoancalculationWorker,
    public loanapi: LoanApiService,
    public toasterService: ToasterService
  ) { }

  Perform_Loan_Copy(copyloanobject: copy_loan_model) {
    this.loanapi.copyloan(copyloanobject).subscribe(res => {
      if (res.ResCode == 1) {
        this.loanapi.getLoanById(copyloanobject.ToLoanFullID).subscribe(res => {
          this.logging.checkandcreatelog(3, 'Overview', "APi LOAN GET with Response " + res.ResCode);
          if (res.ResCode == 1) {
            this.toasterService.success("Records Synced");
            let jsonConvert: JsonConvert = new JsonConvert();
            this.loanserviceworker.performcalculationonloanobject(jsonConvert.deserialize(res.Data, loan_model), true, true);
            this.loanserviceworker.saveValidationsToLocalStorage(res.Data);
          }
          else {
            this.toasterService.error("Could not fetch Loan Object from API")
          }
        });
      } else {
        this.toasterService.error("Error in Copy Loan");
      }
    }, error => {
      this.toasterService.error("Error in Copy Loan");
    });
  }

  private Validate_Copy_Loan_Object(copyObj: copy_loan_model) {
    let hasAnyValue = false;

    if(copyObj) {
      const Inputs = copyObj.Data.loanInputs;

      Object.keys(Inputs).forEach(a => {
        if(!hasAnyValue && Inputs[a]) {
          hasAnyValue = Object.keys(Inputs[a]).some(a => !!a);
        }
      });
    }

    return hasAnyValue;
  }
}

