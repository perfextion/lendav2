import { Component, EventEmitter, Inject, NgZone } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import * as _ from 'lodash';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '@env/environment.prod';

import { loan_model } from '@lenda/models/loanmodel';
import { Sync_Status } from '@lenda/models/syncstatusmodel';

import { LoancalculationWorker } from '@lenda/Workers/calculations/loancalculationworker';
import { PublishService } from '@lenda/services/publish.service';
import { CropService } from '@lenda/components/crop/crop.service';
import { FarmService } from '@lenda/components/farm/farm.service';

@Component({
  selector: 'confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./alertify.style.scss'],
  providers:[PublishService, LoancalculationWorker, FarmService, CropService],
})
export class ConfirmComponent {
  onDataRecieved = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<ConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private publishService : PublishService,
    private localStorageService : LocalStorageService,
    private ngZone: NgZone,
    private farmService: FarmService,
    private cropService: CropService
  ) { }

  onOkClick(): void {
    this.dialogRef.afterClosed().subscribe(res => {
    });
    this.dialogRef.beforeClose().subscribe(res => {
    });
    this.ngZone.run(() => {
      this.dialogRef.close();
    });
    this.onDataRecieved.emit(true);
  }

  onCancelClick(): void {
    this.ngZone.run(() => {
      this.dialogRef.close();
    });
    this.onDataRecieved.emit(false);
  }

  onPublishClick() {
    try {
      let loan: loan_model = this.localStorageService.retrieve(environment.loankey);

      let hasUnsaveChangesType = '';

      if(loan.SyncStatus.Status_Farm == Sync_Status.ADDORDELETE) {
        hasUnsaveChangesType = 'farm';
      }

      if(loan.SyncStatus.Status_Crop_Practice == Sync_Status.ADDORDELETE) {
        hasUnsaveChangesType = 'crop';
      }


      switch(hasUnsaveChangesType) {
        case 'farm':
          this.farmService.syncToDb(this.localStorageService.retrieve(environment.loankey));
          break;

        case 'crop':
          this.cropService.syncToDb(this.localStorageService.retrieve(environment.loankey));
          break;

        default: break;
      }
      this.publishService.syncCompleted();
      this.dialogRef.close();
    } catch {

    }
  }
}
