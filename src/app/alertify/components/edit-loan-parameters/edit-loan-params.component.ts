import { FormControl } from '@angular/forms';
import { environment } from '@env/environment.prod';
import { Validators } from '@angular/forms';
import { LoancalculationWorker } from './../../../Workers/calculations/loancalculationworker';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LocalStorageService } from 'ngx-webstorage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PublishService } from '@lenda/services/publish.service';
import * as _ from 'lodash';
import { RefDataModel, RefOfficeList, RefLoanOfficer } from '@lenda/models/ref-data-model';
import { loan_model } from '@lenda/models/loanmodel';
import { GetCurrentCropYear } from '@lenda/services/common-utils';

@Component({
  selector: 'edit-loan-params',
  templateUrl: 'edit-loan-params.component.html',
  styleUrls: ['edit-loan-params.component.scss'],
  providers: [PublishService]
})
export class EditLoanParamsComponent implements OnInit {
  editParamsForms: FormGroup;
  refData: RefDataModel;
  officeList: Array<RefOfficeList> = [];
  loanOfficerList: Array<RefLoanOfficer> = [];

  crop_year_start: number;
  crop_year_end: number;

  constructor(
    private fb: FormBuilder,
    private localStorageService: LocalStorageService,
    private loanserviceworker: LoancalculationWorker,
    private publishService: PublishService,
    public dialogRef: MatDialogRef<EditLoanParamsComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      title: string;
      selectedLoanMaster: loan_model;
    }
  ) {
    this.refData = this.localStorageService.retrieve(environment.referencedatakey);
    this.crop_year_end = GetCurrentCropYear();
    this.crop_year_start = this.crop_year_end - 7;
  }

  ngOnInit() {
    this.refData = this.localStorageService.retrieve(environment.referencedatakey);

    this.editParamsForms = this.fb.group({
      Crop_Year: ['', [Validators.required, this.forbiddenCropYear.bind(this)]],
      Office_ID: [null, Validators.required],
      Region_ID: [null, Validators.required],
      Loan_Officer_ID: [null, Validators.required]
    });

    try {
      if (!_.isEmpty(this.refData.OfficeList)) {
        this.officeList = this.refData.OfficeList;
        this.officeList = _.sortBy(this.officeList, a => a.Office_Name)
      }
    } catch (error) {
      this.officeList = [];
    }

    try {
      if (!_.isEmpty(this.refData.LoanOfficers)) {
        this.loanOfficerList = this.refData.LoanOfficers;
        this.loanOfficerList = _.sortBy(this.loanOfficerList, a => a.LastName || a.FirstName);
      }
    } catch (error) {
      this.loanOfficerList = [];
    }

    try {
      if (!_.isEmpty(this.data.selectedLoanMaster)) {
        this.editParamsForms.patchValue({
          Crop_Year: this.data.selectedLoanMaster.LoanMaster.Crop_Year,
          Office_ID: this.data.selectedLoanMaster.LoanMaster.Office_ID,
          Region_ID: this.data.selectedLoanMaster.LoanMaster.Region_ID,
          Loan_Officer_ID: this.data.selectedLoanMaster.LoanMaster.Loan_Officer_ID
        });
        this.editParamsForms.updateValueAndValidity();
      }
    } catch (error) {
      this.officeList = [];
    }
  }

  assignRegionByOfficeID() {
    let region = _.find(this.officeList, dataVal => {
      return dataVal.Office_ID === this.editParamsForms.get('Office_ID').value;
    });

    try {
      if (!_.isEmpty(region)) {
        this.editParamsForms.get('Region_ID').setValue(region.Region_ID);
      }
    } catch (error) {
      this.editParamsForms.get('Region_ID').setValue(0);
    }
  }

  forbiddenCropYear(fc: FormControl): { [s: string]: boolean } {
    return fc.value > this.crop_year_start && fc.value <= this.crop_year_end ? null : { required: true };
  }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }

  onOkClick(): void {
    let crop_year = this.editParamsForms.get('Crop_Year').value;

    if (crop_year != this.data.selectedLoanMaster.LoanMaster.Crop_Year) {
      this.data.selectedLoanMaster.LoanMaster['Update_Crop_Year_Ind'] = 1;
    } else {
      this.data.selectedLoanMaster.LoanMaster['Update_Crop_Year_Ind'] = 0;
    }

    this.data.selectedLoanMaster.LoanMaster.Crop_Year = crop_year;

    let office_id = this.editParamsForms.get('Office_ID').value;
    this.data.selectedLoanMaster.LoanMaster.Office_ID = office_id;

    let office = this.refData.OfficeList.find(a => a.Office_ID == office_id);
    if (office) {
      this.data.selectedLoanMaster.LoanMaster.Office_Name = office.Office_Name;
    }

    let region_id = this.editParamsForms.get('Region_ID').value;
    this.data.selectedLoanMaster.LoanMaster.Region_ID = region_id;

    let region = this.refData.RegionList.find(a => a.Region_ID == region_id);
    if (region) {
      this.data.selectedLoanMaster.LoanMaster.Region_Name = region.Region_Name;
    }

    let loan_officer_id = this.editParamsForms.get('Loan_Officer_ID').value;
    this.data.selectedLoanMaster.LoanMaster.Loan_Officer_ID = loan_officer_id;

    let loan_officer = this.refData.LoanOfficers.find(a => a.UserID == loan_officer_id);
    if (loan_officer) {
      let name = loan_officer.LastName || '';
      if (name) {
        name += ', ';
      }
      name += loan_officer.FirstName;

      this.data.selectedLoanMaster.LoanMaster.Loan_Officer_Name = name;
    }

    this.localStorageService.store(environment.loankey, this.data.selectedLoanMaster);

    if (!this.isFarmInvalid) {
      this.dialogRef.close(true);
    }
  }

  public get isFarmInvalid() {
    if(this.editParamsForms) {
      return !this.editParamsForms.dirty || this.editParamsForms.invalid;
    } else {
      return true;
    }
  }
}
