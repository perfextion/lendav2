import { Component, OnInit, EventEmitter, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { CommitteeStatusMock } from './../../../models/committee-status/mock-committee-status';
import { InnerCommitteeStatus, SelectedCommitteeStatus } from './../../../models/committee-status/committee-status.model';

@Component({
  selector: 'committe-status',
  templateUrl: './committee-status.component.html',
  styleUrls: ['./committee-status.style.scss']
})
export class CommitteStatusComponent {
  onDataRecieved = new EventEmitter();
  // mockCommitteeStatus: InnerCommitteeStatus[];
  selectedCommitteeStatus: InnerCommitteeStatus;

  allStatus: Array<any>;

  constructor(
    public dialogRef: MatDialogRef<CommitteStatusComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    // this.mockCommitteeStatus = CommitteeStatusMock.filter(s => s.id == this.data.committeeStatus)[0].innerStatus;

    this.getVoteStatus();
  }

  getVoteStatus() {
    this.allStatus = [];

    for(let s of CommitteeStatusMock.filter(a => a.id == 1 || a.id == 2 || a.id == 3)) {
      let status = s.innerStatus.map(a => {
        let x = a as any;
        x.parentId = s.id;
        x.class = s.title.toLowerCase();

        return x;
      });

      this.allStatus.push(...status);
    }
  }

  onOkClick(): void {
    this.dialogRef.close();
    const result = new SelectedCommitteeStatus();
    result.committeeStatusId = this.data.committeeStatus;
    result.innerCommitteeStatusId = this.selectedCommitteeStatus.id;
    this.onDataRecieved.emit(result);
  }

  emojiVote(statusId, innerStatusId) {
    const result = <SelectedCommitteeStatus>{
      committeeStatusId: statusId,
      innerCommitteeStatusId: innerStatusId
    };

    this.dialogRef.close(result);
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }
}
