import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { LocalStorageService } from 'ngx-webstorage';
import { ValidationGroup } from '@lenda/components/committee/loan-validations/loan-validations.component';
import { errormodel } from '@lenda/models/commonmodels';
import { RefDataModel } from '@lenda/models/ref-data-model';
import { environment } from '@env/environment.prod';
import * as _ from 'lodash';

@Component({
  selector: 'app-sync-validation-warning',
  templateUrl: './sync-validation-warning.component.html',
  styleUrls: ['./sync-validation-warning.component.scss']
})
export class SyncValidationWarningComponent implements OnInit {
  validationGroups: Array<ValidationGroup> = [];
  validations: Array<errormodel>;

  private refdata: RefDataModel;

  constructor(
    private dialogRef: MatDialogRef<SyncValidationWarningComponent>,
    private localstorage: LocalStorageService
  ) {
    this.validations = this.localstorage.retrieve(environment.errorbase);
    this.refdata = this.localstorage.retrieve(environment.referencedatakey);

    this.getData();
  }

  ngOnInit() {}

  getData() {
    this.validations = _.orderBy(this.validations, v => this.getTabId(v.tab));
    let groups = _.groupBy(this.validations, v => v.tab);

    this.validationGroups = [];

    for (let group in groups) {
      this.validationGroups.push({
        tab: group,
        validations: _.sortBy(groups[group], v => v.level)
      });
    }
  }

  getTabId(tabname: string) {
    try {
      let tab = this.refdata.RefTabs.find(
        a => a.Tab_Name.toUpperCase() == tabname.toUpperCase()
      );
      return tab.Tab_ID;
    } catch {
      return 0;
    }
  }

  onOkClick() {
    this.dialogRef.close(true);
  }
}
