import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PendingActionGhostingService {
  constructor() {}

  ghostUser$ = new EventEmitter<number>();
}
