import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { GetAllUsers } from '@lenda/components/committee/members/member.model';
import { Observable } from 'rxjs';
import { UserApiService } from '@lenda/services/user/user.service';
import { startWith, map } from 'rxjs/operators';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-pending-action-ghosting',
  templateUrl: './pending-action-ghosting.component.html'
})
export class PendingActionGhostingComponent implements OnInit, OnDestroy {
  userCtrl: FormControl;
  masterUserList: Array<GetAllUsers> = [];
  filteredUsers: Observable<Array<GetAllUsers>>;
  getUsersSub: ISubscription;

  constructor(
    private dialogRef: MatDialogRef<PendingActionGhostingComponent>,
    private userService: UserApiService
  ) {
    this.getUsersSub = this.userService.getUsers().subscribe(res => {
      this.masterUserList = res.Data;
    });
  }

  ngOnInit() {
    this.userCtrl = new FormControl('', Validators.required);

    this.filteredUsers = this.userCtrl.valueChanges.pipe(
      startWith<string | GetAllUsers>(''),
      map(value => (typeof value === 'string' ? value : '')),
      map(name => {
        let data: GetAllUsers[];

        if (name) {
          data = this._filter(name);
        } else {
          data = this.masterUserList.slice();
        }

        return data;
      })
    );
  }

  ngOnDestroy() {
    if(this.getUsersSub) {
      this.getUsersSub.unsubscribe();
    }
  }

  _filter(filterVal: string) {
    const term = filterVal.toLowerCase();
    return this.masterUserList.filter(
      a =>
        (a.FirstName && a.FirstName.toLowerCase().includes(term)) ||
        (a.LastName && a.LastName.toLowerCase().includes(term)) ||
        (a.Username && a.Username.toLowerCase().includes(term))
    );
  }

  displayFn(user: GetAllUsers) {
    if (user) {
      let name = '';
      if (user.LastName) {
        name = user.LastName + ', ';
      }
      name += user.FirstName;
      return name;
    }
    return '';
  }

  onOkClick() {
    this.dialogRef.close({
      res: true,
      user: this.userCtrl.value
    });
  }

  onCancelClick() {
    this.dialogRef.close({
      res: false,
      user: null
    });
  }
}
