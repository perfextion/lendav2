import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-recommend-loan',
  templateUrl: './recommend-loan.component.html',
  styleUrls: ['./../alertify.style.scss']
})
export class RecommendLoanComponent implements OnInit {
  validationCount: number;
  exceptionCount: number;

  bypassValidation = false;

  constructor(
    public dialogRef: MatDialogRef<RecommendLoanComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  recommend() {}

  onCancelClick() {
    this.dialogRef.close(false);
  }

  onOkClick() {
    this.dialogRef.close(true);
  }
}
