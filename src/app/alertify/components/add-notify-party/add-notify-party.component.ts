import { Component,  EventEmitter, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'add-notify-party',
  styles: [`
  .mat-dialog-container {
    .mat-dialog-title {
      background: #164c7b;
      color: #fff;
      padding: 10px;
      font-size: 14px;
      font-weight: normal;
    }
  }
  .modal-footer{
    text-align: right;
  }

  control-messages{
    display:none;
  }
  
  button.mat-raised-button[disabled] {
    background-color: rgba(0,0,0,.12)!important;
  }`],
  templateUrl: 'add-notify-party.component.html'
})
export class AddNotifyPartyComponent {
  public addMemberForm: FormGroup;

  onDataRecieved = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<AddNotifyPartyComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.addMemberForm = this.fb.group({
        User_ID: ['', [Validators.required]]
      });
    }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
    const selectedUser = this.data.users.filter(u => u.UserID == this.addMemberForm.value.User_ID);
    if (selectedUser) {
      const result = {UserId: this.addMemberForm.value.User_ID, JobTitleId: selectedUser[0].Job_Title_ID };
      this.onDataRecieved.emit(result);
    }
  }

}
