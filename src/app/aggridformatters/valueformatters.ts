import { CurrencyPipe, DatePipe, DecimalPipe } from '@angular/common';
import {
  extractCropValues,
  lookupCropValue,
  lookupCropTypeValue
} from '../Workers/utility/aggrid/cropboxes';
import {
  lookupStateValue,
  lookupCountyValue,
  lookupCountyValueFromPassedRefData
} from '../Workers/utility/aggrid/stateandcountyboxes';
import { RentUOMValues } from '@lenda/components/farm/farm.model';

//This file contains the centralized methods for all value formatters

//BUDGET FORMATTER
//value in Integer value
export function BudgetFormatter(value) {
  let cp: CurrencyPipe = new CurrencyPipe('en-US');
  value = parseFloat(value) || 0;
  return cp.transform(value, 'USD', 'symbol', '1.0-0', '');
}

//BUDGET FORMATTER

//THIS IS CURRENCY FORMATTER
//********************************* */
export function currencyFormatter(params, exception: number = null) {
  params.value = parseFloat(params.value) || 0;

  return formatcurrency(params.value, exception);
}
export function formatcurrency(number, exception: number = null) {
  if (exception == null) {
    return dollarformat(parseFloat(number), '', 0);
  } else {
    return dollarformat(parseFloat(number), '', exception);
  }
}

export function dollarformat(n, currency, decimal) {
  let cp: CurrencyPipe = new CurrencyPipe('en-US');
  let decimalLimit = decimal ? '1.' + decimal + '-' + decimal : '1.0-0';
  return cp.transform(n, 'USD', 'symbol', decimalLimit, '');
}
//********************************* */
// CURRENCY FORMATTER

//NUMBER FORMATTER .. It Only Adds Coma to the Number and Formats it .
export function numberFormatter(params) {
  params.value = parseFloat(params.value) || 0;
  return formatNumber(params.value);
}

function formatNumber(number) {
  // this puts commas into the number eg 1000 goes to 1,000,
  // i pulled this from stack overflow, i have no idea how it works
  return Math.floor(number)
    .toString()
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

/**
 * Will only format value add comma and decimal if needed, no dollar sign.
 * @param value the value to format.
 * @param decimal the max decimal of the value.
 */
export function calculatedNumberFormatter(value, decimal) {
  if (value.hasOwnProperty('value')) {
    value = value.value;
  }

  let cp: CurrencyPipe = new CurrencyPipe('en-US');
  if (!value) value = 0;

  value = parseFloat(value);

  let decimalLimit = decimal ? '1.' + decimal + '-' + decimal : '1.0-0';

  return cp
    .transform(value, 'USD', 'symbol', decimalLimit, '')
    .replace(/[$]/g, '');
}

//NUMBER FORMATTER

//THIS IS ACRES FORMATTER
export function acresFormatter(params) {

  params.value = parseFloat(params.value) || 0;

  return formatacres(params.value);
}

export function formatacres(number) {
  let fp: DecimalPipe = new DecimalPipe('en-US');
  //return parseFloat(number).toFixed(1);
  return fp.transform(number, '1.1-1'); //{minIntegerDigits}.{minFractionDigits}-{maxFractionDigits}}
}

export function creditvalueformatter(params) {
  params.value = parseFloat(params.value) || 0;
  let fp: DecimalPipe = new DecimalPipe('en-US');
  return fp.transform(params.value, '1.2-2');
}
//********************************* */

//THIS IS CROP PRICE FORMATTER
//********************************* */
export function croppriceFormatter(params) {
  params.value = parseFloat(params.value) || 0;

  return '$' + formatcroprice(params.value);
}

export function croppriceFormatterValue(params) {
  if (isNaN(params)) {
    params = 0;
  }
  return '$' + formatcroprice(params);
}

export function formatcroprice(number) {
  return parseFloat(number).toFixed(4);
}

export function yieldFormatter(params) {
  if (params.value) {
    params.value = parseFloat(params.value) || 0;
    let fp: DecimalPipe = new DecimalPipe('en-US');
    return fp.transform(params.value, '1.0-0');
  } else {
    return params.value;
  }
}

// CROP PRICE FORMATTER

//THIS IS PERCENTAGE FORMATTER
//********************************* */
export function percentageFormatter(params, exception?: number) {
  let value = parseFloat(params.value) || 0;
  return formatpercentage(value, exception);
}

export function percentageFormatterValue(params, exception?: number) {
  if (isNaN(params)) {
    params = 0;
  }
  return formatpercentage(params, exception);
}

function formatpercentage(number, exception?: number) {
  if (exception == null) {
    return parseFloat(number).toFixed(1) + '  %';
  } else {
    return parseFloat(number).toFixed(exception) + '  %';
  }
}

//********************************* */
// PERCENTAGE FORMATTER

//THIS IS ACRES FORMATTER
//********************************* */
export function UnitperacreFormatter(params) {
  params.value = parseFloat(params.value) || 0;

  let selected = RentUOMValues.find(v => v.key == params.value);
  return selected ? selected.value : '';
}

//********************************* */
// ACRES FORMATTER

//THIS IS CROP ID to NAME FORMATTER
//********************************* */
export function CropidtonameFormatter(params, refdata = null) {
  if(!refdata) {
    refdata = JSON.parse(
      '[' + window.localStorage.getItem('ng2-webstorage|refdata') + ']'
    )[0];
  }

  let values: Array<any> = extractCropValues(refdata.CropList);
  return lookupCropValue(values, params.value);
}

export function CropidtonameFormatterRef(params, refdata) {
  let values: Array<any> = extractCropValues(refdata.CropList);
  return lookupCropValue(values, params.value);
}

export function CropidtonameFormatterValue(params) {
  let refdata = JSON.parse(
    '[' + window.localStorage.getItem('ng2-webstorage|refdata') + ']'
  )[0];
  let values: Array<any> = extractCropValues(refdata.CropList);
  return lookupCropValue(values, params);
}
//********************************* */
// CROP ID to NAME FORMATTER

//THIS IS CROP TYPE ID to NAME FORMATTER
//********************************* */
export function CroptypeidtonameFormatter(params) {
  return lookupCropTypeValue(params.value);
}

export function CroptypeidtonameFormatterValue(params) {
  return lookupCropTypeValue(params);
}

//********************************* */
// CROP TYPE ID to NAME FORMATTER

//THIS IS STATE ID to NAME FORMATTER
//********************************* */
export function StateidtonameFormatter(params) {
  return lookupStateValue(
    params.colDef.cellEditorParams.values,
    parseInt(params.value, 10)
  );
}
//********************************* */
// STATE ID to NAME FORMATTER

//THIS IS COUNTY ID to NAME FORMATTER
//********************************* */
export function CountyidtonameFormatter(params, refdata?: any) {
  if (refdata == null)
    return params.value ? lookupCountyValue(params.value) : '';
  else
    return params.value
      ? lookupCountyValueFromPassedRefData(params.value, refdata)
      : '';
}
//********************************* */
// COUNTY ID to NAME FORMATTER

//BOOLEAN INSURED FORMATER FOR YES NO
export function insuredFormatter(params) {
  if (params.cellEditorparams) {
    return params.cellEditorparams.values[1].value;
  } else {
    if (params.value && params.value == 1) {
      return 'Yes';
    } else if (params.value == 0) {
      return 'No';
    } else {
      return '';
    }
  }
}

//BOOLEAN INSURED FORMATER FOR YES NO

//EMPTY FORMATTER
//for Dynamic cases
export function EmptyFormatter(params) {
  if (params.value == undefined) {
    return 0;
  }
  if (parseFloat(params.value) % 1 == 0) return parseInt(params.value);
  else return parseFloat(params.value);
}
//EMPTY FORMATTER

//This is Practice type Translator
export function PracticeTranslator(value: string) {
  switch (value.toLowerCase()) {
    case 'irr':
      return 'Irr';
    case 'nir':
    case 'ni':
      return 'NI';
    default:
      return '';
  }
}

//THIS IS DATE FORMATTER
//********************************* */
export function dateFormatter(params, exception: number = null) {
  return dateformat(params.value);
}

export function dateformat(date) {
  let cp: DatePipe = new DatePipe('en-US');
  return cp.transform(date, 'MM/dd/yyyy hh:mm:ss');
}

export function date(date) {
  let cp: DatePipe = new DatePipe('en-US');
  return cp.transform(date, 'MM/dd/yyyy');
}

export function daysDiff(date1, date2) {
  if (!date1) {
    date1 = new Date().toISOString();
  }

  if (!date2) {
    date2 = new Date().toISOString();
  }

  let diffTime = Math.abs(
    new Date(date1).getTime() - new Date(date2).getTime()
  );
  return Math.ceil(diffTime / (1000 * 3600 * 24));
}
