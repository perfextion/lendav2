import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewEncapsulation
} from '@angular/core';
import { isKeyPressedNumeric } from '@lenda/services/common-utils';

@Component({
  selector: 'app-btn-counter',
  templateUrl: './btn-counter.component.html',
  styleUrls: ['./btn-counter.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BtnCounterComponent implements OnInit {
  @Input() model: number = 0;
  @Output() modelChange: EventEmitter<number> = new EventEmitter<number>();

  @Input() max: number = 30;
  @Input() min: number = 0;
  @Input() step: number = 1;

  @Input() disabled: boolean = false;

  @Input() INPUT_TYPE: string;
  @Input() numberOfDecimals: number = 0;

  @Input() formatter: (val: number) => string;

  input: string;

  constructor() {}

  ngOnInit() {
    if(this.INPUT_TYPE == 'PERCENT') {
      this.input = this.model.toFixed(2);
    }
  }

  get val() {
    if (this.formatter) {
      return this.formatter(this.model);
    }
    return this.model;
  }

  modelUpdate($event) {
    this.model = parseFloat($event);
    this.modelChange.emit(this.model);

    if(this.INPUT_TYPE == 'PERCENT') {
      this.input = this.model.toFixed(2);
    }
  }

  valueChange($event) {
    if(this.INPUT_TYPE == 'PERCENT') {
      if (!isKeyPressedNumeric($event, this.model)) {
        if ($event.preventDefault) {
          $event.preventDefault();
        }
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  increase() {
    if (this.disabled) {
      return;
    }

    if (this.model + this.step <= this.max) {
      this.model += this.step;
      this.model = parseFloat(this.model.toFixed(this.numberOfDecimals));
      this.modelChange.emit(this.model);
    }

    if(this.INPUT_TYPE == 'PERCENT') {
      this.input = this.model.toFixed(2);
    }
  }

  decrease() {
    if (this.disabled) {
      return;
    }

    if (this.model - this.step >= this.min) {
      this.model -= this.step;
      this.model = parseFloat(this.model.toFixed(this.numberOfDecimals));
      this.modelChange.emit(this.model);
    }

    if(this.INPUT_TYPE == 'PERCENT') {
      this.input = this.model.toFixed(2);
    }
  }
}
