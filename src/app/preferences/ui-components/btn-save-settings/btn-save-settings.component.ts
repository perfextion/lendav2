import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-btn-save-settings',
  templateUrl: './btn-save-settings.component.html',
  styleUrls: ['./btn-save-settings.component.scss']
})
export class BtnSaveSettingsComponent implements OnInit {
  @Output() save = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.save.emit();
  }

}
