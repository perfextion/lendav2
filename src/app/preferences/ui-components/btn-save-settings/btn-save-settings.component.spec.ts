import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnSaveSettingsComponent } from './btn-save-settings.component';

describe('BtnSaveSettingsComponent', () => {
  let component: BtnSaveSettingsComponent;
  let fixture: ComponentFixture<BtnSaveSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnSaveSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnSaveSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
