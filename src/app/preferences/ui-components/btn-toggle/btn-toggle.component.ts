import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-btn-toggle',
  templateUrl: './btn-toggle.component.html',
  styleUrls: ['./btn-toggle.component.scss']
})
export class BtnToggleComponent implements OnInit {
  @Input() yesText: string;
  @Input() noText: string;
  @Input() model;
  @Input() disabled: boolean = false;
  @Output() modelChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {}

  public onChange($event) {
    this.model = $event.checked;
    this.modelChange.emit(this.model);
  }
}
