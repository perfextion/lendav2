import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ViewMode } from "../../models/base-entities/view-modes.enum";

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {
  @Input() options: string[];
  @Input()  placeholder: string;
  @Input() selected: string;
  @Output() selectedChanged: EventEmitter<string> = new EventEmitter<string>();
  @Input() isTitleCaseEnabled: boolean = true;

  @Input() disabled: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onValueChanged(value) {
    this.selectedChanged.emit(value);
  }
}
