import {
  Component,
  Input,
  EventEmitter,
  Output,
  OnChanges
} from '@angular/core';
import { Subscription } from 'rxjs';
import { DragulaService } from 'ng2-dragula';
import { DataType } from '@lenda/preferences/models/base-entities/column/index.model';
import * as _ from 'lodash';

@Component({
  selector: 'app-draggable-panel',
  templateUrl: './draggable-panel.component.html',
  styleUrls: ['./draggable-panel.component.scss']
})
export class DraggablePanelComponent implements OnChanges {
  @Input() containerName;
  @Input() headerText: string;
  @Input() model;
  @Output() modelChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() dragModelChange: EventEmitter<any> = new EventEmitter<any>();

  @Input() isTarget: boolean = false;
  @Output() advancedSettings: EventEmitter<any> = new EventEmitter<any>();
  filterCriteria: string = '';

  private subs = new Subscription();

  public selectedItem;

  constructor(private dragulaService: DragulaService) {
    this.addDropEvent();
  }

  ngOnChanges() {
    if (!this.isTarget) {
      this.model = _.sortBy(this.model, a => a.value);
    }
  }

  addDropEvent() {
    this.dragulaService.dropModel(this.containerName)
    .subscribe(({ item }) => {
      setTimeout(() => this.onDropped(item), 0);
    });
  }

  onDropped(item) {
    this.modelChange.emit(this.model);
  }

  /**
   * Emit that advanced setting is clicked
   * @param item item selected
   */
  setAdvancedSettings(item) {
    this.selectedItem = item;
    this.advancedSettings.emit(item);
  }

  hasFilter(item) {
    try {
      if (item) {
        if (
          item.settings.datatype == DataType.int ||
          item.settings.datatype == DataType.decimal ||
          item.settings.datatype == DataType.date
        ) {
          return item.settings.filter.begin || item.settings.filter.end;
        }

        if (item.settings.datatype == DataType.string) {
          return item.settings.filter.select;
        }
      }
      return false;
    } catch {
      return false;
    }
  }

  /**
   * Return true/false if item is selected or not
   * @param item column
   */
  isItemSelected(item) {
    return item === this.selectedItem;
  }

  isNameFiltered(value: string) {
    if(value) {
      if (this.filterCriteria === '') {
        return true;
      }
      return value.toLowerCase().includes(this.filterCriteria.toLowerCase());
    }
  }
}
