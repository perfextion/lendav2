import { Injectable } from "@angular/core";
import { Preferences } from "@lenda/preferences/models/index.model";
import deepKeys from 'deep-keys';

@Injectable()
export class KeysValidatorService {

  /**
   * Compares two objects by using deep keys and stores any differences in an array
   * @param obj1 First object
   * @param obj2 Second object
   */
  checkKeyDifferences(obj1, obj2) {
    let obj1Keys: Array<string> = deepKeys(obj1);
    let obj2Keys: Array<string> = deepKeys(obj2);
    return this.getObjectDifference(obj1Keys, obj2Keys);
  }

  /**
   * Iterates through the object keys and add the differences in an array
   * @param obj1Keys First objects deep keys
   * @param obj2Keys Second objects deep keys
   */
  private getObjectDifference(obj1Keys: Array<string>, obj2Keys: Array<string>) {
    let diff = [];

    obj1Keys.forEach((key, index) => {
      let obj2KeyIndex = obj2Keys.indexOf(key);
      // Check if other array contains same key
      if (obj2KeyIndex !== -1) {
        // If it exists then remove from both objects
        obj2Keys.splice(obj2KeyIndex, 1);
      } else {
        // It is removed from new preferences model
        // No action required for this
        // If some action is required then uncomment below line
        // diff.push(`-${key}`);
      }
    });

    // If there are any other items remaining in obj2Keys
    // It means that these are newly added to the preferences object
    obj2Keys.forEach((key) => {
      diff.push(`+${key}`);
    });

    return diff;
  }
}
