import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../shared/shared.module';
import { PreferencesRoutingModule } from './preferences-routing.module';
import { SettingsComponent } from './settings/settings.component';
import { PreferencesComponent } from './preferences.component';
import { LoanSettingsComponent } from './loan-settings/loan-settings.component';
import { LoanListComponent } from './loan-list/loan-list.component';
import { PortfolioAnalysisComponent } from './loan-list/portfolio-analysis/portfolio-analysis.component';
import { ColumnsDisplaySettingsComponent } from './loan-settings/columns-display-settings/columns-display-settings.component';
import { InsurancePlanSettingsComponent } from './loan-settings/insurance-plan-settings/insurance-plan-settings.component';
import { ReportsDisplaySettingsComponent } from './loan-settings/reports-display-settings/reports-display-settings.component';
import { BtnToggleComponent } from './ui-components/btn-toggle/btn-toggle.component';
import { DropdownComponent } from './ui-components/dropdown/dropdown.component';
import { LocalStorageService } from 'ngx-webstorage';
import { ToastrModule } from 'ngx-toastr';
import { ListSettingsComponent } from './loan-list/list-settings/list-settings.component';
import { DragulaModule } from 'ng2-dragula';
import { DraggablePanelComponent } from './ui-components/draggable-panel/draggable-panel.component';
import { BtnSaveSettingsComponent } from './ui-components/btn-save-settings/btn-save-settings.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { LoanDetailComponent } from './loan-list/list-settings/loan-detail/loan-detail.component';
import { VacationComponent } from '@lenda/preferences/user-settings/vacation/vacation.component';
import { PreferredContactComponent } from '@lenda/preferences/user-settings/preferred-contact/preferred-contact.component';
import { AdvancedSettingsComponent } from '@lenda/preferences/loan-list/list-settings/advanced-settings/advanced-settings.component';
import { BorrowerSettingsComponent } from '@lenda/preferences/user-settings/borrower-settings/borrower-settings.component';
import { KeysValidatorService } from '@lenda/preferences/utils/keys-validator.service';
import { DashboardSettingsComponent } from './user-settings/dashboard-settings/dashboard-settings.component';
import { UserNotificationsComponent } from './user-settings/user-notifications/user-notifications.component';
import { NotificationsSettingsComponent } from './notifications-settings/notifications-settings.component';
import { NotificationsComponent } from './notifications-settings/notifications/notifications.component';
import { MaterialModule } from '@lenda/shared/material.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    PreferencesRoutingModule,
    ToastrModule.forRoot(),
    DragulaModule.forRoot()
  ],
  declarations: [
    SettingsComponent,
    PreferencesComponent,
    LoanSettingsComponent,
    LoanListComponent,
    PortfolioAnalysisComponent,
    VacationComponent,
    PreferredContactComponent,
    ColumnsDisplaySettingsComponent,
    InsurancePlanSettingsComponent,
    ReportsDisplaySettingsComponent,
    BtnToggleComponent,
    DropdownComponent,
    ListSettingsComponent,
    AdvancedSettingsComponent,
    DraggablePanelComponent,
    BorrowerSettingsComponent,
    BtnSaveSettingsComponent,
    UserSettingsComponent,
    LoanDetailComponent,
    DashboardSettingsComponent,
    UserNotificationsComponent,
    NotificationsSettingsComponent,
    NotificationsComponent
  ],
  providers: [LocalStorageService, KeysValidatorService]
})
export class PreferencesModule {}
