import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { LayoutService } from '../shared/layout/layout.service';
import { Preferences } from "./models/index.model";
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from 'ngx-webstorage';
import { environment } from '@env/environment.prod';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss']
})
export class PreferencesComponent implements OnInit {
  public preferences: Preferences;
  public isSidebarExpanded: boolean = true;

  constructor(
    private layoutService: LayoutService,
    private toaster: ToastrService,
    private vcr: ViewContainerRef,
    private localst: LocalStorageService
  ) { }

  ngOnInit() {
     this.layoutService.isSidebarExpanded().subscribe((value) => {
      this.isSidebarExpanded = value;
    });
  }

  showSideBar(){
    let localloanobject = this.localst.retrieve(environment.loankey);
    return localloanobject ? true : false;
  }
}
