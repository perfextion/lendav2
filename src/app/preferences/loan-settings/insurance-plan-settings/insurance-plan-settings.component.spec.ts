import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsurancePlanSettingsComponent } from './insurance-plan-settings.component';

describe('InsurancePlanSettingsComponent', () => {
  let component: InsurancePlanSettingsComponent;
  let fixture: ComponentFixture<InsurancePlanSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsurancePlanSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsurancePlanSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
