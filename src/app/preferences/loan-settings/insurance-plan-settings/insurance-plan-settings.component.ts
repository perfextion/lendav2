import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { InsurancePlanSettings } from "@lenda/preferences/models/loan-settings/insurance-plan.model";

@Component({
  selector: 'app-insurance-plan-settings',
  templateUrl: './insurance-plan-settings.component.html',
  styleUrls: ['./insurance-plan-settings.component.scss']
})
export class InsurancePlanSettingsComponent implements OnInit {
  @Input() settings: InsurancePlanSettings = new InsurancePlanSettings();
  @Output() settingsChange: EventEmitter<InsurancePlanSettings> = new EventEmitter<InsurancePlanSettings>();

  constructor() { }

  ngOnInit() {
  }

}
