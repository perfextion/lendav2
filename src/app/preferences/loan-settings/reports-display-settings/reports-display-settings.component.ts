import { Component, OnInit, Output, Input, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { ReportsDisplaySettings } from "@lenda/preferences/models/loan-settings/reports-display.model";

@Component({
  selector: 'app-reports-display-settings',
  templateUrl: './reports-display-settings.component.html',
  styleUrls: ['./reports-display-settings.component.scss']
})
export class ReportsDisplaySettingsComponent implements OnInit {
  @Input() settings: ReportsDisplaySettings = new ReportsDisplaySettings();

  private _settings: ReportsDisplaySettings = new ReportsDisplaySettings();

  constructor() { }

  ngOnInit() {
  }

  onChange($event) {
    // this.settingsChange.emit(this.settings);
  }
}
