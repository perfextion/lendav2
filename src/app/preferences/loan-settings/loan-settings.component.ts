import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { LoanSettings } from "@lenda/preferences/models/loan-settings/index.model";

@Component({
  selector: 'app-loan-settings',
  templateUrl: './loan-settings.component.html',
  styleUrls: ['./loan-settings.component.scss']
})
export class LoanSettingsComponent implements OnInit {
  public selectedIndex: number = 0;
  @Input() settings: LoanSettings = new LoanSettings();
  @Output() settingsChange: EventEmitter<LoanSettings> = new EventEmitter<LoanSettings>();

  constructor() { }

  ngOnInit() {
    this.settingsChange.subscribe(changes => {
      console.log("LoanSettingsComponent");
      console.log(changes);
    })
   }
}
