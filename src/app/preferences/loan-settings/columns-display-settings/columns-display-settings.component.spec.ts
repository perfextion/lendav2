import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnsDisplaySettingsComponent } from './columns-display-settings.component';

describe('ColumnsDisplaySettingsComponent', () => {
  let component: ColumnsDisplaySettingsComponent;
  let fixture: ComponentFixture<ColumnsDisplaySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnsDisplaySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnsDisplaySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
