import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { ColumnsDisplaySettings } from "@lenda/preferences/models/loan-settings/columns-display.model";

@Component({
  selector: 'app-columns-display-settings',
  templateUrl: './columns-display-settings.component.html',
  styleUrls: ['./columns-display-settings.component.scss']
})
export class ColumnsDisplaySettingsComponent implements OnChanges {
  @Input() settings: ColumnsDisplaySettings = new ColumnsDisplaySettings();
  @Output() settingsChange: EventEmitter<ColumnsDisplaySettings> = new EventEmitter<ColumnsDisplaySettings>();

  constructor() { }

  ngOnChanges() {
    // console.log(this.settings);
  }

}
