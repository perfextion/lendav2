import { LoanReconciliationSettings } from "./loan-reconciliation.model";
import { LoanReconciliationDetailIDSettings } from "./loan-reconciliation-detail-id.model";
import { LoanReconciliationSettlementDetailSettings } from "./loan-reconciliation-settlement-detail.model";
import { LoanReconciliationMeasurementDetailSettings } from "./loan-reconciliation-measurement-detail.model";
import { LoanReconciliationVarianceDetailSettings } from "./loan-reconciliation-variance-detail.model";

export class LoanReconciliationDetailSettings {
  loanReconciliationSettings: LoanReconciliationSettings;
  loanReconciliationDetailIDSettings: LoanReconciliationDetailIDSettings;
  loanReconciliationSettlementDetailSettings: LoanReconciliationSettlementDetailSettings;
  loanReconciliationMeasurementDetailSettings: LoanReconciliationMeasurementDetailSettings;
  lLoanReconciliationVarianceDetailSettingsoanReconciliationVarianceDetailSettings: LoanReconciliationVarianceDetailSettings;
}
