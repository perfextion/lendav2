import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanReconciliationSettlementDetailSettings {
  Loan_Reconciliation_Settlement_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Loan_Reconciliation_Detail_ID: ColumnSettings;
  Settlement_Date: ColumnSettings;
  Association_Type: ColumnSettings;
  Association_ID: ColumnSettings;
  Settlement_Num: ColumnSettings;
  Settlement_Qty: ColumnSettings;
  Settlement_Value: ColumnSettings;
  Notes: ColumnSettings;
  User_ID: ColumnSettings;
  Date_Time: ColumnSettings;
  Status: ColumnSettings;
}
