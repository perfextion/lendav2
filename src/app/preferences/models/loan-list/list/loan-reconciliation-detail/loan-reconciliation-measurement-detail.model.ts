import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanReconciliationMeasurementDetailSettings {
  Loan_Reconciliation_Measurement_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Loan_Reconciliation_Detail_ID: ColumnSettings;
  Measurement_Date: ColumnSettings;
  Measurement_Type_Code: ColumnSettings;
  Location_Text: ColumnSettings;
  Measurement_Qty: ColumnSettings;
  Association_Type: ColumnSettings;
  Association_ID: ColumnSettings;
  Contract_Number: ColumnSettings;
  Contract_Qty: ColumnSettings;
  Contract_Price: ColumnSettings;
  Disc_Measurement_Value: ColumnSettings;
  Other_Description_Text: ColumnSettings;
  Notes: ColumnSettings;
  User_ID: ColumnSettings;
  Date_Time: ColumnSettings;
  Status: ColumnSettings;
}
