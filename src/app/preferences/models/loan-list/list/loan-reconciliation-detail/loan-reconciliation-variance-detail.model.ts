import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanReconciliationVarianceDetailSettings {
  Loan_Reconciliation_Variance_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Loan_Reconciliation_Detail_ID: ColumnSettings;
  Variance_Type_Code: ColumnSettings;
  Variance_Qty: ColumnSettings;
  Variance_Value: ColumnSettings;
  Other_Description_Text: ColumnSettings;
  Notes: ColumnSettings;
  User_ID: ColumnSettings;
  Date_Time: ColumnSettings;
  Status: ColumnSettings;
}
