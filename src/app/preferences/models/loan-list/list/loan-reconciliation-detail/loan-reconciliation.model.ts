import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanReconciliationSettings {
  Loan_Reconciliation_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Credit_Quality_Code: ColumnSettings;
  ARM_Additional_Needs: ColumnSettings;
  Ins_Premium_Outstanding: ColumnSettings;
  Dist_Balance: ColumnSettings;
  Dist_Additional_Needs: ColumnSettings;
  Maturity_Date: ColumnSettings;
  Estimated_Days: ColumnSettings;
  Origination_Fee_Percent: ColumnSettings;
  Service_Fee_Percent: ColumnSettings;
  Extension_Fee_Percent: ColumnSettings;
  Rate_Percent: ColumnSettings;
  Origination_Fee_$: ColumnSettings;
  Service_Fee_$: ColumnSettings;
  Extension_Fee_$: ColumnSettings;
  Risk_Cushion_$: ColumnSettings;
  Risk_Cushion_Percent: ColumnSettings;
  LTV_Percent: ColumnSettings;
  Return_Percent: ColumnSettings;
  User_ID: ColumnSettings;
  Date_Time: ColumnSettings;
  Status: ColumnSettings;
}
