import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanDetailPlanDetailSettings {
  Loan_Detail_Plan_Detail_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Loan_Detail_ID: ColumnSettings;
  Loan_Policy_ID: ColumnSettings;
  Ins_Plan_Code: ColumnSettings;
  Ins_Type_Code: ColumnSettings;
  Ins_Qty: ColumnSettings;
  Ins_Value: ColumnSettings;
  Disc_Ins_Value: ColumnSettings;
  Status: ColumnSettings;
}
