import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanFarmSettings {
  Farm_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  State_ID: ColumnSettings;
  County_ID: ColumnSettings;
  Prod_Share_Percent: ColumnSettings;
  Landowner: ColumnSettings;
  FSN: ColumnSettings;
  Section: ColumnSettings;
  Rated: ColumnSettings;
  Owner_Ind: ColumnSettings;
  Share_Rent_Percent: ColumnSettings;
  Cash_Rent_Total: ColumnSettings;
  Cash_Rent_Per_Acre: ColumnSettings;
  Cash_Rent_Due_Date: ColumnSettings;
  Cash_Rent_Paid: ColumnSettings;
  Permission_To_Insure: ColumnSettings;
  Cash_Rent_Waived: ColumnSettings;
  Cash_Rent_Waived_Amount: ColumnSettings;
  Irr_Acres: ColumnSettings;
  Nl_Acres: ColumnSettings;
  Crop_Share_Detail_Ind: ColumnSettings;
  Status: ColumnSettings;
}
