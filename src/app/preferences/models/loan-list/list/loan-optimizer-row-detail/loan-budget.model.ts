import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanBudgetSettings {
  Loan_Budget_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Loan_Crop_Practice_ID: ColumnSettings;
  Expense_Type_Code: ColumnSettings;
  ARM_Budget: ColumnSettings;
  Distributer_Budget: ColumnSettings;
  Third_Party_Budget: ColumnSettings;
  Notes: ColumnSettings;
  Other_Description_Text: ColumnSettings;
  Status: ColumnSettings;
}
