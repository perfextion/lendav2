import { ColumnSettings } from "../../../base-entities/column/index.model";

export class ApiInKlSettings {
  Loan_Collateral_Action_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Collateral_Action_Type_Code: ColumnSettings;
  User_ID: ColumnSettings;
  Date_Time: ColumnSettings;
  Status: ColumnSettings;
}
