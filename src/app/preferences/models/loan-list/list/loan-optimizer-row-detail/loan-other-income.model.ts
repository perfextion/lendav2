import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanOtherIncomeSettings {
  Other_Income_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Other_Income_Type_Code: ColumnSettings;
  Other_Description_Text: ColumnSettings;
  Amount: ColumnSettings;
  Status: ColumnSettings;
}
