import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanCropPriceSettings {
  Loan_Crop_Type_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Crop_Code: ColumnSettings;
  Crop_Type_Code: ColumnSettings;
  Crop_Price: ColumnSettings;
  Basis_Adj: ColumnSettings;
  Marketing_Adj: ColumnSettings;
  Rebate_Adj: ColumnSettings;
  Adj_Price: ColumnSettings;
  Booking_Ind: ColumnSettings;
  Status: ColumnSettings;
}
