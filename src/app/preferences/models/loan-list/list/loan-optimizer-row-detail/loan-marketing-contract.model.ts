import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanMarketingContractSettings {
  Contract_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Association_Type: ColumnSettings;
  Association_ID: ColumnSettings;
  Contract_Num: ColumnSettings;
  Contract_Type: ColumnSettings;
  Description_Text: ColumnSettings;
  Crop_Type_Code: ColumnSettings;
  Qty: ColumnSettings;
  Price: ColumnSettings;
  Status: ColumnSettings;
}
