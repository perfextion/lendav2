import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanPolicyOptionSettings {
  Loan_Policy_Option_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Loan_Policy_ID: ColumnSettings;
  Policy_Option_Type_Code: ColumnSettings;
  Other_Description_Text: ColumnSettings;
  Status: ColumnSettings;
}
