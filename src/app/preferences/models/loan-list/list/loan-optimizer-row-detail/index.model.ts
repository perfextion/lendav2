import { LoanDetailSettings } from "./loan-detail.model";
import { LoanDetailPlanDetailSettings } from "./loan-detail-plan-detail.model";
import { LoanCropPriceSettings } from "./loan-crop-price.model";
import { LoanMarketingContractSettings } from "./loan-marketing-contract.model";
import { LoanOtherIncomeSettings } from "./loan-other-income.model";
import { LoanFarmSettings } from "./loan-farm.model";
import { FarmCropShareDetailSettings } from "./farm-crop-share-detail.model";
import { LoanPolicySettings } from "./loan-policy.model";
import { LoanPolicyOptionSettings } from "./loan-policy-option.model";
import { LoanBudgetSettings } from "./loan-budget.model";
import { ApiInKlSettings } from "./api-in-kl.model";

export class LoanOptimizerRowDetailSettings {
  loanDetailSettings: LoanDetailSettings;
  loanDetailPlanDetailSettings: LoanDetailPlanDetailSettings;
  loanCropPriceSettings: LoanCropPriceSettings;
  loanMarketingContractSettings: LoanMarketingContractSettings;
  loanOtherIncomeSettings: LoanOtherIncomeSettings;
  loanFarmSettings: LoanFarmSettings;
  farmCropShareDetailSettings: FarmCropShareDetailSettings;
  loanPolicySettings: LoanPolicySettings;
  loanPolicyOptionSettings: LoanPolicyOptionSettings;
  loanBudgetSettings: LoanBudgetSettings;
  apiInKlSettings: ApiInKlSettings;
}
