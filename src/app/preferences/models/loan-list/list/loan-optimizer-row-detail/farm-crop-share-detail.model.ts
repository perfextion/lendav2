import { ColumnSettings } from "../../../base-entities/column/index.model";

export class FarmCropShareDetailSettings {
  Farm_Crop_Share_Detail_ID: ColumnSettings;
  Farm_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Crop_Code: ColumnSettings;
  Share_Rent_Override: ColumnSettings;
  Share_Rent_Percent: ColumnSettings;
  Status: ColumnSettings;
}
