import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanQuestionSettings {
  Question_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Response_Ind: ColumnSettings;
  Response_Detail_Text: ColumnSettings;
  Response_Detail_Value: ColumnSettings;
  Response_Detail_Field_ID: ColumnSettings;
  Status: ColumnSettings;
}
