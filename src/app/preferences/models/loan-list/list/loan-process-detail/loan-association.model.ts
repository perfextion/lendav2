import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanAssociationSettings {
  Association_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Association_Type_Code: ColumnSettings;
  Association_Name: ColumnSettings;
  Contact: ColumnSettings;
  Location: ColumnSettings;
  Phone: ColumnSettings;
  Email: ColumnSettings;
  Amount: ColumnSettings;
  Preferred_Contact_Ind: ColumnSettings;
  Status: ColumnSettings;
}
