import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanCommitteeSettings {
  Loan_Committee_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  User_ID: ColumnSettings;
  Date_Time: ColumnSettings;
  Voting_Member_Ind: ColumnSettings;
  All_Comments_Read_Ind: ColumnSettings;
  Vote: ColumnSettings;
  Status: ColumnSettings;
}
