import { ApiInNortridgeSettings } from "./api-in-nortridge.model";
import { LoanAssociationSettings } from "./loan-association.model";
import { LoanQuestionSettings } from "./loan-question.model";
import { LoanExceptionSettings } from "./loan-exception.model";
import { LoanConditionSettings } from "./loan-condition.model";
import { LoanCommitteeSettings } from "./loan-committee.model";
import { LoanCommentSettings } from "./loan-comment.model";
import { LoanAuditTrailSettings } from "./loan-audit-trail.model";

export class LoanProcessDetailSettings {
  apiInNortridgeSettings: ApiInNortridgeSettings;
  loanAssociationSettings: LoanAssociationSettings;
  loanQuestionSettings: LoanQuestionSettings;
  loanExceptionSettings: LoanExceptionSettings;
  loanConditionSettings: LoanConditionSettings;
  loanCommitteeSettings: LoanCommitteeSettings;
  loanCommentSettings: LoanCommentSettings;
  loanAuditTrailSettings: LoanAuditTrailSettings;
}
