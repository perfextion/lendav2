import { ColumnSettings } from "../../../base-entities/column/index.model";

export class ApiInNortridgeSettings {
  API_In_Nortridge_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Date_Time: ColumnSettings;
  Trial_Balance_Amount: ColumnSettings;
  Trial_Balance_Date: ColumnSettings;
  Days_Past_Due: ColumnSettings;
  Status: ColumnSettings;
}
