import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanCommentSettings {
  Loan_Comment_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Parent_Comment_ID: ColumnSettings;
  User_ID: ColumnSettings;
  Date_Time: ColumnSettings;
  Comment_Type_Code: ColumnSettings;
  Comment_Type_Level_Code: ColumnSettings;
  Tab_ID: ColumnSettings;
  Chevon_ID: ColumnSettings;
  Field_ID: ColumnSettings;
  Comment_Text: ColumnSettings;
  Comment_Emoji_ID: ColumnSettings;
  Comment_Read_Ind: ColumnSettings;
  Status: ColumnSettings;
}
