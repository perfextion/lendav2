import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanAuditTrailSettings {
  Loan_Audit_Trail_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  User_ID: ColumnSettings;
  Date_Time: ColumnSettings;
  Loan_Status: ColumnSettings;
  View_Update_Ind: ColumnSettings;
  Audit_Field_ID: ColumnSettings;
  Audit_Trail_Text: ColumnSettings;
}
