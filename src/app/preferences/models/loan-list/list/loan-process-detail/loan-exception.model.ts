import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanExceptionSettings {
  Loan_Exception_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Exception_ID: ColumnSettings;
  Exception_ID_Level: ColumnSettings;
  Tab_ID: ColumnSettings;
  Chevon_ID: ColumnSettings;
  Field_ID: ColumnSettings;
  Exception_Mitigation_Text: ColumnSettings;
  Status: ColumnSettings;
}
