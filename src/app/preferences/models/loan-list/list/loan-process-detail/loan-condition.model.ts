import { ColumnSettings } from "../../../base-entities/column/index.model";

export class LoanConditionSettings {
  Loan_Condition_ID: ColumnSettings;
  Loan_ID: ColumnSettings;
  Loan_Seq_Num: ColumnSettings;
  Condition_Type_ID: ColumnSettings;
  Document_ID: ColumnSettings;
  Association_ID: ColumnSettings;
  Suggestion_Only_Ind: ColumnSettings;
  User_ID: ColumnSettings;
  Date_Time: ColumnSettings;
  Other_Description_Text: ColumnSettings;
  Status: ColumnSettings;
}
