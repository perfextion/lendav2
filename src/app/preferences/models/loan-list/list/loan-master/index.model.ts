import { ColumnSettings, DataType, DisplayType } from "../../../base-entities/column/index.model";

export class LoanMasterSettings {
  Loan_Full_ID: ColumnSettings = new ColumnSettings(DataType.range, DisplayType.string, true, true, 0);
  Loan_Status: ColumnSettings = new ColumnSettings(DataType.string, DisplayType.string, true, true, 1);
  Loan_Tolerance_Level_Ind: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.string, true, false, 2);
  Pending_Action_Sort_Order: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.string, true, true, 3, true);
  Loan_Pending_Action_Type_Code: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.string, true, true, 4);
  Is_Latest: ColumnSettings = new ColumnSettings(DataType.string, DisplayType.string, true, false, 5);
  Crop_Year: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.string, true, false, 6);
  Borrower_Name: ColumnSettings = new ColumnSettings(DataType.string, DisplayType.string, true, false, 7);
  Arm_Commitment: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency, true, false, 8);
  Total_Commitment: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency, true, false, 9);

  Legacy_Loan_ID: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.Legacy_Loan_ID);
  Region_Name: ColumnSettings = new ColumnSettings();
  Office_Name: ColumnSettings = new ColumnSettings();
  Loan_Officer_Name: ColumnSettings = new ColumnSettings();
  Loan_Type_Name: ColumnSettings = new ColumnSettings();
  Farmer_Name: ColumnSettings = new ColumnSettings();

  Borrower_ID_Type: ColumnSettings = new ColumnSettings();
  Borrower_SSN_Hash: ColumnSettings = new ColumnSettings();
  Borrower_Entity_Type_Code: ColumnSettings = new ColumnSettings();
  Borrower_Address: ColumnSettings = new ColumnSettings();
  Borrower_City: ColumnSettings = new ColumnSettings();
  Borrower_State: ColumnSettings = new ColumnSettings();
  Borrower_Zip: ColumnSettings = new ColumnSettings();
  Borrower_Phone: ColumnSettings = new ColumnSettings();
  Borrower_Email: ColumnSettings = new ColumnSettings();
  Borrower_DL_State: ColumnSettings = new ColumnSettings();
  Borrower_DL_Num: ColumnSettings = new ColumnSettings();
  Borrower_DOB: ColumnSettings = new ColumnSettings(DataType.date, DisplayType.date);
  Borrower_Preferred_Contact_Ind: ColumnSettings = new ColumnSettings();
  Borrower_County_Name: ColumnSettings = new ColumnSettings();
  Borrower_Lat: ColumnSettings = new ColumnSettings();
  Borrower_Long: ColumnSettings = new ColumnSettings();
  Co_Borrower_Count: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);

  Spouse_SSN_Hash: ColumnSettings = new ColumnSettings();
  Spouse_Name: ColumnSettings = new ColumnSettings();
  Spouse_Address: ColumnSettings = new ColumnSettings();
  Spouse_City: ColumnSettings = new ColumnSettings();
  Spouse_State: ColumnSettings = new ColumnSettings();
  Spouse_Zip: ColumnSettings = new ColumnSettings();
  Spouse_Phone: ColumnSettings = new ColumnSettings();
  Spouse_Email: ColumnSettings = new ColumnSettings();
  Spouse_Preferred_Contact_Ind: ColumnSettings = new ColumnSettings();

  Farmer_ID_Type: ColumnSettings = new ColumnSettings();
  Farmer_SSN_Hash: ColumnSettings = new ColumnSettings();
  Farmer_Entity_Type: ColumnSettings = new ColumnSettings();
  Farmer_Address: ColumnSettings = new ColumnSettings();
  Farmer_City: ColumnSettings = new ColumnSettings();
  Farmer_State: ColumnSettings = new ColumnSettings();
  Farmer_Zip: ColumnSettings = new ColumnSettings();
  Farmer_Phone: ColumnSettings = new ColumnSettings();
  Farmer_Email: ColumnSettings = new ColumnSettings();
  Farmer_DL_State: ColumnSettings = new ColumnSettings();
  Farmer_DL_Num: ColumnSettings = new ColumnSettings();
  Farmer_DOB: ColumnSettings = new ColumnSettings(DataType.date, DisplayType.date);
  Farmer_Preferred_Contact_Ind: ColumnSettings = new ColumnSettings();

  Application_Date: ColumnSettings = new ColumnSettings(DataType.date, DisplayType.date);
  Original_Application_date: ColumnSettings = new ColumnSettings(DataType.date, DisplayType.date);
  Referral_Type_Code: ColumnSettings = new ColumnSettings();
  Year_Begin_Farming: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);
  Year_Begin_Client: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);

  Distributor_Name: ColumnSettings = new ColumnSettings();
  Primary_Agency_Name: ColumnSettings = new ColumnSettings();
  Primary_AIP_Name: ColumnSettings = new ColumnSettings();

  Current_Bankruptcy_Status: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);
  Original_Bankruptcy_Status: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);
  Previously_Bankruptcy_Status: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);

  Judgement_Ind: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);
  Local_Ind: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);
  Controlled_Disbursement_Ind: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);
  Watch_List_Ind: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);
  Rate_Yield_Ref_Yield_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);

  Credit_Score_Date: ColumnSettings = new ColumnSettings(DataType.date, DisplayType.date);
  Credit_Score: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);

  Financials_Date: ColumnSettings = new ColumnSettings(DataType.date, DisplayType.date);
  CPA_Prepared_Financials: ColumnSettings = new ColumnSettings();

  Current_Assets: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Inter_Assets: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Fixed_Assets: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Total_Assets: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Current_Liabilities: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Inter_Liabilities: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Fixed_Liabilities: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Total_Liabilities: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Current_Net_Worth: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Inter_Net_Worth: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Fixed_Net_Worth: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Total_Net_Worth: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Current_Assets_Disc_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);
  Inter_Assets_Disc_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);
  Fixed_Assets_Disc_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);
  Current_Disc_Net_Worth: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Inter_Disc_Net_Worth: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Fixed_Disc_Net_Worth: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Total_Disc_Net_Worth: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Income_Detail_Ind: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);

  Borrower_3yr_Tax_Returns: ColumnSettings = new ColumnSettings();
  Borrower_Farm_Financial_Rating: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.acres);
  Borrower_Rating: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.rating);

  AG_Pro_requested_Credit: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Requested_Credit: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Maturity_Date: ColumnSettings = new ColumnSettings(DataType.date, DisplayType.date);
  Original_Maturity_Date: ColumnSettings = new ColumnSettings(DataType.date, DisplayType.date);
  Estimated_Days: ColumnSettings = new ColumnSettings(DataType.int, DisplayType.number);

  Origination_Fee_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.interest);
  Service_Fee_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.interest);
  Extension_Fee_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.interest);
  Interest_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);

  Origination_Fee_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Service_Fee_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Extension_Fee_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Interest_Est_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Delta_Origination_Fee_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Service_Fee_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Extension_Fee_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Interest_Est_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Total_Acres: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.acres);
  Total_Crop_Acres: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.acres);

  Dist_Commitment: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Third_Party_Credit: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Crop_Acres: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_ARM_Commitment: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Dist_Commitment: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Third_Party_Credit: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Total_Commitment: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Net_Other_Income: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Total_Revenue: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Total_Revenue: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Cash_Flow_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Cash_Flow_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);
  Break_Even_percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);
  Risk_Cushion_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Risk_Cushion_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);

  ARM_Margin_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  ARM_Margin_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);
  Total_margin_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Total_Margin_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);
  LTV_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);
  Total_ARM_Fees_And_Interest: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Return_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);

  Net_Market_Value_Crops: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Net_Market_Value_FSA: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Net_Market_Value_Livestock: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Net_Market_Value_Stored_Crops: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Net_Market_Value_Equipment: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Net_Market_Value_Real_Estate: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Net_Market_Value_Other: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Net_Market_Value_Total: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Disc_value_Crops: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_value_FSA: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_value_Livestock: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_value_Stored_Crops: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_value_Equipment: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_value_Real_Estate: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_value_Other: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_value_Total: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Ins_Value_Crops: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Ins_Value_FSA: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Ins_Value_Livestock: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Ins_Value_Stored_Crops: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Ins_Value_Equipment: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Ins_Value_Real_Estate: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Ins_Value_Other: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Ins_Value_Total: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Disc_Ins_Value_Crops: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_Ins_Value_FSA: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_Ins_Value_Livestock: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_Ins_Value_Stored_Crops: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_Ins_Value_Equipment: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_Ins_Value_Real_Estate: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_Ins_Value_Other: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_Ins_Value_Total: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Net_Market_Value_Insurance: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Disc_value_Insurance: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  CEI_Value: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Decision_Date: ColumnSettings = new ColumnSettings(DataType.date, DisplayType.date);
  Close_Date: ColumnSettings = new ColumnSettings(DataType.date, DisplayType.date);

  Current_Addendum_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);
  Prev_Addendum_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.percent);

  AIP_Acres_Verified: ColumnSettings = new ColumnSettings();
  FSA_Acres_Verified: ColumnSettings = new ColumnSettings();

  Acres_Mapped: ColumnSettings = new ColumnSettings();
  Active_Ind: ColumnSettings = new ColumnSettings(DataType.string, DisplayType.Indicator);

  Delta_Cash_Flow_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Cash_Flow_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Risk_Cushion_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Risk_Cushion_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Return_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Delta_Current_Addendum_Percent: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Third_Party_Total_Budget: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Dist_Total_Budget: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  ARM_Total_Budget: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Total_Return_Fee: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Loan_Total_Budget: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  ARM_Extension_Fee: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Dist_Rate_Fee: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  ARM_Rate_Fee: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);

  Outstanding_Balance: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Past_Due_Amount: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Last_Nortridge_Sync_Date: ColumnSettings = new ColumnSettings(DataType.date, DisplayType.date);

  Primary_Col: ColumnSettings = new ColumnSettings();
  Rev_Ins_Col: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  Non_Rev_Ins_Col: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.currency);
  CAF1: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.number);
  CAF2: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.number);
  CAF3: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.number);
  CAF4: ColumnSettings = new ColumnSettings(DataType.decimal, DisplayType.number);

  Prev_Yr_Loan_ID: ColumnSettings = new ColumnSettings();
}

export enum LoanMasterColumn {
  Loan_Full_ID = 'Key: Loan ID',
  Legacy_Loan_ID = 'Key: Legacy Loan ID',
  Loan_Type_Name =  'Key: Loan Type',

  Crop_Year = 'Basic: Crop Year',
  Region_Name = 'Basic: Region',
  Office_Name = 'Basic: Office',
  Loan_Officer_Name = 'Basic: Loan Officer',
  Borrower_Name = 'Basic: Borrower',
  Farmer_Name = 'Basic: Farmer',

  Borrower_ID_Type = 'Borrower: Borrower ID Type',
  Borrower_SSN_Hash = 'Borrower: Borrower SSN Hash',
  Borrower_Entity_Type_Code = 'Borrower: Borrower Entity Type',
  Borrower_Address = 'Borrower: Borrower Address',
  Borrower_City = 'Borrower: Borrower City',
  Borrower_State = 'Borrower: Borrower State',
  Borrower_Zip = 'Borrower: Borrower Zip',
  Borrower_Phone = 'Borrower: Borrower Phone',
  Borrower_Email = 'Borrower: Borrower Email',
  Borrower_DL_State = 'Borrower: Borrower DL State',
  Borrower_DL_Num = 'Borrower: Borrower DL Num',
  Borrower_DOB = 'Borrower: Borrower DOB',
  Borrower_Preferred_Contact_Ind = 'Borrower: Borrower Preferred Contact Ind',
  Borrower_County_Name = 'Borrower: Borrower County',
  Borrower_Lat = 'Borrower: Borrower Lat',
  Borrower_Long = 'Borrower: Borrower Long',
  Co_Borrower_Count = 'Borrower: Co Borrower Count',

  Spouse_SSN_Hash = 'Spouse: Spouse SSN Hash',
  Spouse_Name = 'Spouse: Spouse',
  Spouse_Address = 'Spouse: Spouse Address',
  Spouse_City = 'Spouse: Spouse City',
  Spouse_State = 'Spouse: Spouse State',
  Spouse_Zip = 'Spouse: Spouse Zip',
  Spouse_Phone = 'Spouse: Spouse Phone',
  Spouse_Email = 'Spouse: Spouse Email',
  Spouse_Preferred_Contact_Ind = 'Spouse: Spouse Preferred Contact Ind',

  Farmer_ID_Type = 'Farmer: Farmer ID Type',
  Farmer_SSN_Hash = 'Farmer: Farmer SSN Hash',
  Farmer_Entity_Type = 'Farmer: Farmer Entity Type',
  Farmer_Address = 'Farmer: Farmer Address',
  Farmer_City = 'Farmer: Farmer City',
  Farmer_State = 'Farmer: Farmer State',
  Farmer_Zip = 'Farmer: Farmer Zip',
  Farmer_Phone = 'Farmer: Farmer Phone',
  Farmer_Email = 'Farmer: Farmer Email',
  Farmer_DL_State  = 'Farmer: Farmer DL State',
  Farmer_DL_Num = 'Farmer: Farmer DL Num',
  Farmer_DOB = 'Farmer: Farmer DOB',
  Farmer_Preferred_Contact_Ind = 'Farmer: Farmer Preferred Contact Ind',

  Year_Begin_Farming = 'Application: Year Begin FARMing',
  Year_Begin_Client = 'Application: Year Begin ARMing',
  Referral_Type_Code = 'Application: Referral Type Code',
  Primary_AIP_Name = 'Application: Primary AIP',
  Distributor_Name = 'Application: Distributor',
  Primary_Agency_Name = 'Application: Primary Agency',
  Current_Bankruptcy_Status = 'Application: Current Bankruptcy Status',
  Original_Bankruptcy_Status = 'Application: Original Current Bankruptcy Status' ,
  Previously_Bankruptcy_Status = 'Application: Previous Bankruptcy Status',
  Borrower_3yr_Tax_Returns = 'Application: 3yr Tax Returns Ind',
  Judgement_Ind = 'Application: Judgement Ind',
  Local_Ind = 'Application: Local Ind',
  Total_Acres = 'Application: Total Farm Acres',
  Total_Crop_Acres = 'Application: Total Crop Acres',
  Delta_Crop_Acres = 'Application: Delta Crop Acres',
  Application_Date = 'Application: Application Date',

  Credit_Score = 'Financial: Credit Score',
  Credit_Score_Date = 'Financial: Credit Score Date',
  CPA_Prepared_Financials = 'Financial: CPA Prepared Financials Ind',
  Financials_Date = 'Financial: Financials Date',

  Current_Assets = 'Financial: Current Assets',
  Inter_Assets = 'Financial: Inter Assets',
  Fixed_Assets = 'Financial: Fixed Assets',
  Total_Assets = 'Financial: Total Assets',
  Current_Assets_Disc_Percent = 'Financial: Current Assets Disc. %',
  Inter_Assets_Disc_Percent = 'Financial: Inter Assets Disc. %',
  Fixed_Assets_Disc_Percent = 'Financial: Fixed Assets Disc. %',
  Current_Liabilities = 'Financial: Current Liabilities',
  Inter_Liabilities = 'Financial: Inter Liabilities',
  Fixed_Liabilities = 'Financial: Fixed Liabilities',
  Total_Liabilities = 'Financial: Total Liabilities',
  Current_Net_Worth = 'Financial: Current Net',
  Inter_Net_Worth = 'Financial: Inter Net',
  Fixed_Net_Worth = 'Financial: Fixed Net',
  Total_Net_Worth = 'Financial: Total Net Worth',
  Current_Disc_Net_Worth = 'Financial: Current Disc Net',
  Inter_Disc_Net_Worth = 'Financial: Inter Disc Net',
  Fixed_Disc_Net_Worth = 'Financial: Fixed Disc Net',
  Total_Disc_Net_Worth = 'Financial: Total Disc Net Worth',
  AG_Pro_requested_Credit = 'Financial: AG Pro Requested Credit',
  Borrower_Farm_Financial_Rating = 'Financial: Farm Financial Rating',
  Borrower_Rating = 'Financial: Borrower Rating',
  Requested_Credit = 'Financial: Requested Credit',

  Maturity_Date = 'Terms: Maturity Date',
  Estimated_Days = 'Terms: Estimated Days',

  Origination_Fee_Percent = 'Terms: Origination Fee %' ,
  Service_Fee_Percent = 'Terms: Service Fee %',
  Extension_Fee_Percent = 'Terms: Extension Fee %',
  Interest_Percent = 'Terms: Interest Rate %',
  Origination_Fee_Amount = 'Terms: Origination Fee',
  Service_Fee_Amount = 'Terms: Service Fee',
  Extension_Fee_Amount = 'Terms: Extension Fee',
  Interest_Est_Amount = 'Terms: Estimated Interest',

  Return_Percent = 'Performance: Return %',
  Total_Revenue = 'Performance: Total Revenue',

  Arm_Commitment = 'Commitment: ARM Commitment',
  Dist_Commitment = 'Commitment: Dist Commitment',
  Third_Party_Credit = 'Commitment: Third Party Credit',
  Total_Commitment = 'Commitment: Total Commitment',

  Delta_ARM_Commitment = 'Commitment: Delta ARM Commitment',
  Delta_Dist_Commitment = 'Commitment: Delta Dist Commitment',
  Delta_Third_Party_Credit = 'Commitment: Delta Third Party Credit',
  Delta_Total_Commitment = 'Commitment: Delta Total Commitment',

  Delta_Origination_Fee_Amount = 'Terms: Delta Origination Fee',
  Delta_Service_Fee_Amount = 'Terms: Delta Service Fee',
  Delta_Extension_Fee_Amount = 'Terms: Delta Extension Fee',
  Delta_Interest_Est_Amount = 'Terms: Delta Estimated Interest',

  Delta_Cash_Flow_Amount = 'Performance: Performance: Delta Cash Flow',
  Delta_Cash_Flow_Percent = 'Performance: Delta Cash Flow %',
  Delta_Risk_Cushion_Amount = 'Performance: Delta Risk Cushion',
  Delta_Risk_Cushion_Percent = 'Performance: Delta Risk Cushion %',
  Delta_Return_Percent = 'Performance: Delta Return %',
  Delta_Current_Addendum_Percent = 'Performance: Delta Loan Addendum %',

  Net_Market_Value_Crops = 'Collateral: Mkt Value Crops',
  Net_Market_Value_Insurance = 'Collateral: Market Value Insurance',
  Net_Market_Value_FSA = 'Collateral: Mkt Value FSA',
  Net_Market_Value_Livestock = 'Collateral: Mkt Value Livestock',
  Net_Market_Value_Stored_Crops = 'Collateral: Mkt Value Stored Crops',
  Net_Market_Value_Equipment = 'Collateral: Mkt Value Equipment',
  Net_Market_Value_Real_Estate = 'Collateral: Mkt Value Real Estate' ,
  Net_Market_Value_Other = 'Collateral: Mkt Value Other' ,
  Net_Market_Value_Total = 'Collateral: Mkt Value Total' ,
  Disc_value_Crops = 'Collateral: Disc Mkt Value Crops' ,
  Disc_value_Insurance = 'Collateral: Disc Ins Value' ,
  Disc_value_FSA = 'Collateral: Disc Mkt Value FSA' ,
  Disc_value_Livestock = 'Collateral: Disc Mkt Value Livestock' ,
  Disc_value_Stored_Crops = 'Collateral: Disc Mkt Value Stored Crops' ,
  Disc_value_Equipment = 'Collateral: Disc Mkt Value Equipment' ,
  Disc_value_Real_Estate = 'Collateral: Disc Mkt Value Real Estate' ,
  Disc_value_Other = 'Collateral: Disc Mkt Value Other' ,
  Disc_value_Total = 'Collateral: Disc Mkt Value Total' ,
  Ins_Value_Crops = 'Collateral: Ins Value Crops',
  Ins_Value_Insurance = 'Collateral: Ins Value Insurance',
  Ins_Value_FSA = 'Collateral: Ins Value FSA',
  Ins_Value_Livestock = 'Collateral: Ins Value Livestock',
  Ins_Value_Stored_Crops = 'Collateral: Ins Value Stored Crops',
  Ins_Value_Equipment = 'Collateral: Ins Value Equipment',
  Ins_Value_Real_Estate = 'Collateral: Ins Value Real Estate' ,
  Ins_Value_Other = 'Collateral: Ins Value Other' ,
  Ins_Value_Total = 'Collateral: Ins Value Total',
  Disc_Ins_Value_Crops = 'Collateral: Disc Ins Value Crops' ,
  Disc_Ins_Value_Insurance = 'Collateral: Disc Ins Value Insurance' ,
  Disc_Ins_Value_FSA = 'Collateral: Disc Ins Value FSA' ,
  Disc_Ins_Value_Livestock = 'Collateral: Disc Ins Value Livestock' ,
  Disc_Ins_Value_Stored_Crops = 'Collateral: Disc Ins Value Stored Crops' ,
  Disc_Ins_Value_Equipment = 'Collateral: Disc Ins Value Equipment' ,
  Disc_Ins_Value_Real_Estate = 'Collateral: Disc Ins Value Real Estate' ,
  Disc_Ins_Value_Other = 'Collateral: Disc Ins Value Other' ,
  Disc_Ins_Value_Total = 'Collateral: Disc Ins Value Total',
  CEI_Value = 'Collateral: Disc CEI Value' ,

  Cash_Flow_Amount = 'Performance: Cash Flow',
  Cash_Flow_Percent = 'Performance: Cash Flow %',
  Break_Even_percent = 'Performance: Break Even %',
  Risk_Cushion_Amount = 'Performance: Risk Cushion',
  Risk_Cushion_Percent = 'Performance: Risk Cushion %' ,
  ARM_Margin_Amount = 'Performance: ARM Margin',
  ARM_Margin_Percent = 'Performance: ARM Margin %',
  Total_margin_Amount = 'Performance: Total Margin',
  Total_Margin_Percent = 'Performance: Total Margin %',
  LTV_Percent = 'Performance: LTV %',
  Delta_Total_Revenue = 'Performance: Delta Total Revenue',
  Net_Other_Income = 'Performance: Other Income',

  Decision_Date = 'Status: Decision Date',
  Close_Date = 'Status: Close Date',
  Loan_Status = 'Status: Status',
  Prev_Yr_Loan_ID = 'Status: Prev Yr Loan ID',
  Loan_Pending_Action_Type_Code = 'Status: Pending Action',
  Pending_Action_Sort_Order = 'Status: Pending Action Sort Order',
  AIP_Acres_Verified = 'Status: Verf Acres AIP Ind' ,
  FSA_Acres_Verified = 'Status: Verf Acres FSA Ind' ,
  Acres_Mapped = 'Status: Acres Map Ind' ,
  Prev_Addendum_Percent = 'Status: Prev Loan Addendum %' ,
  Current_Addendum_Percent = 'Status: Loan Addendum %' ,
  Active_Ind = 'Status: Active Ind' ,

  Watch_List_Ind = 'Application: Watch List Ind',
  Income_Detail_Ind = 'Application: Income Detail Ind',
  Rate_Yield_Ref_Yield_Percent = 'Application: Rate Yield Ref Yield %' ,

  Original_Application_date = 'Application: Original Application Date' ,
  Original_Maturity_Date = 'Terms: Original Maturity Date' ,
  Controlled_Disbursement_Ind = 'Application: Controlled Disbursement Ind' ,

  Third_Party_Total_Budget = 'Budget: Third Party Total Budget' ,
  Dist_Total_Budget = 'Budget: Dist Total Budget' ,
  ARM_Total_Budget = 'Budget: ARM Total Budget' ,
  Loan_Total_Budget = 'BudgetL Loan Total Budget' ,

  Total_ARM_Fees_And_Interest = 'Performance: ARM Fees And Est Interest' ,
  Total_Return_Fee = 'Performance: Total Return Fee' ,
  ARM_Extension_Fee = 'Performance: ARM Extension Fee' ,
  Dist_Rate_Fee = 'Performance: Dist Estimated Interest' ,
  ARM_Rate_Fee = 'Performance: ARM Estimated Interest' ,

  Outstanding_Balance = 'Account: Outstanding Balance' ,
  Past_Due_Amount = 'Account: Past Due Amount' ,
  Last_Nortridge_Sync_Date = 'Account: Last Nortridge Sync Date' ,

  CAF1 = 'Compliance: CAF1' ,
  CAF2 = 'Compliance: CAF2' ,
  CAF3 = 'Compliance: CAF3' ,
  CAF4 = 'Compliance: CAF4' ,
  Primary_Col = 'Compliance: Primary Collateral' ,
  Non_Rev_Ins_Col = 'Compliance: Non Revenue Ins Col.',
  Rev_Ins_Col = 'Compliance: Revenue Ins Col.',
}
