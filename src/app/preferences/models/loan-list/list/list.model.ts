import { LoanMasterSettings } from "./loan-master/index.model";
import { LoanOptimizerRowDetailSettings } from "./loan-optimizer-row-detail/index.model";
import { LoanProcessDetailSettings } from "./loan-process-detail/index.model";
import { LoanReconciliationDetailSettings } from "./loan-reconciliation-detail/index.model";

export class ListSettings {
  isIncludeInactiveEnabled: boolean = false;
  isLatestLoanVersionEnabled: boolean = false;
  isIncludeActiveOnlyEnabled: boolean = false;
  isIncludeMyReportsEnabled: boolean = true;
  isPendingActionSortOrderEnabled: boolean = true;
  loanMasterSettings: LoanMasterSettings = new LoanMasterSettings();
  // loanOptimizerRowDetailSettings: LoanOptimizerRowDetailSettings;
  // loanProcessDetailSettings: LoanProcessDetailSettings;
  // loanReconciliationDetailSettings: LoanReconciliationDetailSettings;
}
