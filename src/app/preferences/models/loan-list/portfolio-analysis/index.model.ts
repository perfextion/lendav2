import { ViewArea } from "../../base-entities/view-area.enum";
import { BasedUponCriteria } from "../../base-entities/based-upon-criteria.enum";

export class PortfolioAnalysisSettings {
  isDetailRecordsEnabled: boolean = true;
  isSumRowEnabled: boolean = true;
  isMaxRowEnabled: boolean = true;
  isAverageRowEnabled: boolean = true;
  isMinRowEnabled: boolean = true;
  weightedAveragePercentBasedUpon: BasedUponCriteria = BasedUponCriteria.Arm_Commitment;
}
