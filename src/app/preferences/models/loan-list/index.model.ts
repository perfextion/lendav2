import { ListSettings } from "./list/list.model";
import { PortfolioAnalysisSettings } from "./portfolio-analysis/index.model";

export class LoanListSettings {
  listSettings: ListSettings = new ListSettings();
  portfolioAnalysisSettings: PortfolioAnalysisSettings = new PortfolioAnalysisSettings();
}
