import { ArmUserNotificationsSettings } from "./arm-user-notifications-settings.model";
import { BorrowerNotificationsSettings } from "./borrower-notifications-settings.model";
import { DistributerNotificationsSettings } from "./distributer-notifications-settings.model";

export class NotificationsSettings {
    public armUserLoNotificationSettings: ArmUserNotificationsSettings = new ArmUserNotificationsSettings();
    public borrowerNotificationSettings: BorrowerNotificationsSettings = new BorrowerNotificationsSettings();
    public distributerNotificationSettings: DistributerNotificationsSettings = new DistributerNotificationsSettings();
}