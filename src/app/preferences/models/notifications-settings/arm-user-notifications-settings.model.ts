import { UserNotificationSettings, Level } from "./user-notification.model";
import { PrefContactSetting } from "./pref-contact.model";

export class ArmUserNotificationsSettings {
    public loanInventoryFrequencyUntilClosed: number = 2;
    public loanBalanceFrequencyWhileActive: number = 14;

    public userNotificationSettings: UserNotificationSettings[] = [
        new UserNotificationSettings('Committee Comments', true, true, Level.one, 1, 0),
        new UserNotificationSettings('Loan Inventory Notifications', true, false, Level.one, 2, 0),
        new UserNotificationSettings('Loan Status Change Notifications', false, false, Level.one, 3, 0),
        new UserNotificationSettings('Working', false, false, Level.two, 31, 3),
        new UserNotificationSettings('Voided', true, false, Level.two, 32, 3),
        new UserNotificationSettings('Recommended', true, false, Level.two, 33, 3),
        new UserNotificationSettings('Recommend Notification', true, true, Level.three, 301, 33),
        new UserNotificationSettings('Loan Summary Report.pdf', true, false, Level.three, 302, 33),
        new UserNotificationSettings('Borrower Loan Summary.pdf', true, false, Level.three, 303, 33),
        new UserNotificationSettings('Loan Summary Text Details', true, false, Level.three, 304, 33),
        new UserNotificationSettings('Withdrawn', true, false, Level.two, 34, 3),
        new UserNotificationSettings('Approved', true, false, Level.two, 35, 3),
        new UserNotificationSettings('Declined', true, false, Level.two, 36, 3),
        new UserNotificationSettings('Uploaded', true, false, Level.two, 37, 3),
        new UserNotificationSettings('Paid', true, false, Level.two, 38, 3),
        new UserNotificationSettings('Written Off', true, false, Level.two, 39, 3),
        new UserNotificationSettings('Loan Balance Notifications', true, false, Level.one, 4, 0),
  ];

  public prefContactSettings: PrefContactSetting = new PrefContactSetting();
}
