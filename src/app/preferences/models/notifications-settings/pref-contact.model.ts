export class PrefContact {
  disabled: boolean;
  value: boolean;

  constructor(value: boolean, disabled: boolean) {
    this.disabled = disabled;
    this.value = value;
  }
}


export class PrefContactSetting {
  systemNotification = new PrefContact(false, false);
  text = new PrefContact(false, true);
  email = new PrefContact(true, false);
}
