export class UserNotificationSettings {
  title: string;
  value: boolean;
  isRequired: boolean;
  level: Level = Level.one;
  parent_id: number = 0;
  id: number = 0;

  constructor(title: string, value: boolean, isRequired: boolean, level: Level, id = 0, parent_id = 0) {
    this.title = title;
    this.value = value;
    this.isRequired = isRequired;
    this.level = level;
    this.id = id;
    this.parent_id = parent_id;

    if(isRequired) {
      this.value = true;
    }
  }
}

/**
 * Level is used to signify if it is a child element
 * if its child then padding is added to display it as child
 */
export enum Level {
  one = 'one',
  two = 'two',
  three = 'three'
}
