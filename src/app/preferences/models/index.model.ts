import { LoanSettings } from './loan-settings/index.model';
import { LoanListSettings } from './loan-list/index.model';
import { UserSettings } from './user-settings/index.model';
import { NotificationsSettings } from './notifications-settings/index.model';
import { FavouriteLoan } from '@lenda/pipes/favourite-loan.pipe';

export class Preferences {
  public loanListSettings: LoanListSettings = new LoanListSettings();
  public loanSettings: LoanSettings = new LoanSettings();
  public userSettings: UserSettings = new UserSettings();
  public favouriteLoanList: Array<FavouriteLoan> = [];
}
