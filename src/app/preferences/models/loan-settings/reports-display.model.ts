export class ReportsDisplaySettings {
    //SUMMARY
    isArmDetailEnabled: boolean = true;
    isCropDetailEnabled: boolean = true;
    isRentDetailEnabled: boolean = true;
    isConditionDetailEnabled: boolean = true;
    isDiscValueEnabled: boolean = true;
    isShowCompletedEnabled: boolean = true;
}