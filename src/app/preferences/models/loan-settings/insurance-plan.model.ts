// TODO: Check with Suresh about possible values
export class InsurancePlanSettings {
  isMpciEnabled: boolean = true;
  isStaxEnabled: boolean = false;
  isScoEnabled: boolean = false;
  isHmaxEnabled: boolean = false;
  isRampEnabled: boolean = false;
  isIceEnabled: boolean = false;
  isAbcEnabled: boolean = false;
  isPciEnabled: boolean = false;
  isCropHailEnabled: boolean = false;
  isWfrpEnabled: boolean = false;

  isHrEnabled: boolean = false;
}
