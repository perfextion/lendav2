import { ColumnsDisplaySettings } from "./columns-display.model";
import { InsurancePlanSettings } from "./insurance-plan.model";
import { ReportsDisplaySettings } from "./reports-display.model";
import { Page } from '@lenda/models/page.enum';
import { Chevron } from "@lenda/models/loan-response.model";

export class LoanSettings {
  columnsDisplaySettings: ColumnsDisplaySettings = new ColumnsDisplaySettings();
  insurancePlanSettings: InsurancePlanSettings = new InsurancePlanSettings();
  reportsDisplaySettings: ReportsDisplaySettings = new ReportsDisplaySettings();
  Table_States: Array<TableState>;
}

export class TableState {
  public address: TableAddress;
  public data: any;
}
export class TableAddress {
  public page: Page;
  public chevron: Chevrons;
}

export enum Chevrons {
  borrower_reffrom,
  borrower_creditreference,
  borrower_incomehistory,
  borrower_balancesheet,
  crop_rebator,
  crop_yield,
  crop_prices,
  farm_landlord,
  farm_farm,
  insurance_agent,
  insurance_agency,
  insurance_aip,
  insurance_policies,
  insurance_aph,
  budget_distributor,
  budget_thirdparty,
  budget_harvestor,
  budget_total,
  budget_fixed,
  optimizer_nir,
  optimizer_irr,
  collateral_lienholder,
  collateral_buyer,
  collateral_guaranter,
  collateral_collateral,
  collateral_marketingcontracts,
  commitee_commitee,

}


