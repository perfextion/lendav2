export class ColumnsDisplaySettings {
  section: boolean = true;
  rated: boolean = true;
  cropType: boolean = true;
  irrPrac: boolean = true;
  cropPrac: boolean = true;
  rateYield:boolean=false;
  aph:boolean=false;
  // TODO  - Later
  fsn: boolean = true;
  owned: boolean = true;
  prodPercentage: boolean = true;
  landlord: boolean = true;
  rent$: boolean = true;
  rentUom: boolean = true;
  rentDue: boolean = true;
  waived: boolean = true;
  rentPercentage: boolean = true;
  permToIns: boolean = true;
  optimizerArh: boolean = true;
  cashFlow: boolean = true;
  riskCushion: boolean = true;
  margin: boolean = true;
}
