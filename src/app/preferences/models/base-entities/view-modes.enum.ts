export enum ViewMode {
  standard = 'standard',
  condensed = 'condensed',
  supercondensed = 'super-condensed',
};
