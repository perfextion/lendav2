export enum SortByOption {
  ascending = 'Ascending',
  descending = 'Descending'
}
