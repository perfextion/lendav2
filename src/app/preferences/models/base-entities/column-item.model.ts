import { ColumnSettings } from "@lenda/preferences/models/base-entities/column/index.model";

export class ColumnItem {
  key: any;
  value: any;
  settings: any;

  constructor(key: any, value: any, settings: any) {
    this.key = key;
    this.value = value;
    this.settings = settings;
  }
}
