export enum BasedUponCriteria {
  Arm_Commitment = 'ARM Commit',
  Total_Commitment = 'Total Commit',
  Total_Acres = 'Acres'
}
