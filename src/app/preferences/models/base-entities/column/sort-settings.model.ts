import { SortByOption } from "@lenda/preferences/models/base-entities/sort-by-options.enum";

export class SortSettings {
  sortOrder: string = SortByOption.ascending;
}
