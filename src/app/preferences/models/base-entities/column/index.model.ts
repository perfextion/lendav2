import { FilterSettings } from './filter-settings.model';
import { SortSettings } from './sort-settings.model';

export class ColumnSettings {
  datatype: DataType = DataType.string;
  displayType: DisplayType = DisplayType.string;
  colOrder: Number = 0;
  filter: FilterSettings = new FilterSettings();
  sort: SortSettings = new SortSettings();
  visible: boolean;
  isIncludedByDefault: boolean;
  isFrozen: boolean = false;
  isDisplayEnabled: boolean = true;

  constructor(
    datatype: DataType = DataType.string,
    displayType: DisplayType = DisplayType.string,
    visible: boolean = false,
    isFrozen: boolean = false,
    colOrder: Number = 0,
    isIncludedByDefault: boolean = false
  ) {
    this.visible = visible;
    this.isFrozen = isFrozen;
    this.colOrder = colOrder;
    this.datatype = datatype;
    this.displayType = displayType;
    this.isIncludedByDefault = isIncludedByDefault;
  }
}

/**
 * Available data types for column item
 */
export enum DataType {
  string = 'string',
  int = 'int',
  decimal = 'decimal',
  date = 'date',
  range = 'range'
}

/**
 * Applicable for various items that are of different types but behave differently
 * e.g. CropYear is integer but needs to be displayed as string
 */
export enum DisplayType {
  string = 'string',
  number = 'number',
  percent = 'percent',
  date = 'date',
  currency = 'currency',
  interest = 'interest',
  acres = 'acres',
  $peracre = '$peracre',
  quantity = 'quantity',
  price = 'price',
  rating = 'rating',
  numberWithoutTotal = 'numberWithoutTotal',
  Legacy_Loan_ID = "Legacy_Loan_ID",
  Indicator = 'Indicator'
}
