export class BorrowerSettings {
  public isScaleEnabled: boolean = true;
  public isLabelsEnabled: boolean = true;
  public isWeightEnabled: boolean = true;
  public isConstantPercentEnabled: boolean = true;
}
