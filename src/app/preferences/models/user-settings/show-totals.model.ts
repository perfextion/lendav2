export class ShowTotalsSettings {
    isValidationDetailsTabIconsEnabled: boolean = false;
    isExceptionDetailsTabIconsEnabled: boolean = false;
    isChevronOpenState: boolean = false;
    isValidationWarningDuringSyncEnabled: boolean = false;
}
