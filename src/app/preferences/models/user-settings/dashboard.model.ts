import { ViewMode } from "@lenda/preferences/models/base-entities/view-modes.enum";

/**
 * Dashboard settings
 */
 export class DashboardSettings {
  // Below are custom settings for chart view
  dashboard: DashboardItemSettings = new DashboardItemSettings(ViewMode.standard);
}

 /**
 * Dashboard item for viewmode and whether it is fixed on top or not
 */
 export class DashboardItemSettings {
  viewMode: string = ViewMode.standard;
  isFixedOnTop: boolean = false;
  isStandard: boolean = true;
  isCondensed: boolean = false;
  isSuperCondensed: boolean = false;

  constructor(viewMode: ViewMode = ViewMode.standard, isFixedOnTop: boolean = false) {
    this.viewMode = viewMode;
    this.isFixedOnTop = isFixedOnTop;
  }
}
