import { VacationSettings } from "@lenda/preferences/models/user-settings/vacation.model";
import { PreferredContactSettings } from "@lenda/preferences/models/user-settings/preferred-contact.model";
import { Page } from '@lenda/models/page.enum';
import { DashboardSettings } from "./dashboard.model";
import { SchematicSettings } from "./schematic.model";
import { ShowTotalsSettings } from "./show-totals.model";
import { BorrowerSettings } from "./borrower-settings.model";
import { EditDefaultSettings } from "./edit-default-settings.model";
import { NotificationsSettings } from "../notifications-settings/index.model";

export class UserSettings {
  public landingPage: Page = Page.schematic;
  public borrowerSettings: BorrowerSettings = new BorrowerSettings();
  public dashboardSettings: DashboardSettings = new DashboardSettings();
  public showTotalsSettings: ShowTotalsSettings = new ShowTotalsSettings();
  public editDefaultSettings: EditDefaultSettings = new EditDefaultSettings();
  public schematicSettings: SchematicSettings = new SchematicSettings();
  public preferredContactSettings: PreferredContactSettings = new PreferredContactSettings();
  public vacationSettings: VacationSettings = new VacationSettings();
  public notificationsSettings: NotificationsSettings = new NotificationsSettings();
}
