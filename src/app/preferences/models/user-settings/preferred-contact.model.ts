export class PreferredContactSettings {
  systemNotification: boolean = true;
  text: boolean = false;
  email: boolean = true;
}
