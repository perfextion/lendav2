export class EditDefaultSettings {
    public isNameAssociationEnabled: boolean = false;
    public isCropBasisEnabled: boolean = false;
    public isCropBudgetEnabled: boolean = false;
}