import { Component, OnInit } from '@angular/core';
import { ViewPosition } from "../../models/base-entities/view-position.enum";
import { BasedUponCriteria } from "../../models/base-entities/based-upon-criteria.enum";
import { Input, Output, EventEmitter } from "@angular/core";
import { PortfolioAnalysisSettings } from "@lenda/preferences/models/loan-list/portfolio-analysis/index.model";

@Component({
  selector: 'app-portfolio-analysis',
  templateUrl: './portfolio-analysis.component.html',
  styleUrls: ['./portfolio-analysis.component.scss']
})
export class PortfolioAnalysisComponent implements OnInit {
  @Input() settings: PortfolioAnalysisSettings = new PortfolioAnalysisSettings();
  @Output() settingsChange: EventEmitter<PortfolioAnalysisSettings> = new EventEmitter<PortfolioAnalysisSettings>();

  public positions: ViewPosition[] = [
    ViewPosition.top,
    ViewPosition.bottom
  ];

  public basedUponOptions: BasedUponCriteria[] = [];

  constructor() { }

  ngOnInit() {
    this.basedUponOptions = this.getOptions();
  }

  /**
   * Iterates through page enum and set pages options
   */
  getOptions(): BasedUponCriteria[] {
    return <BasedUponCriteria[]>Object.values(BasedUponCriteria).map(item => item);
  }

  /**
   * Called when weighted average % is updated
   * @param value Updated event for value update
   */
  onWeightedAverageUpdated(value) {
    this.settings.weightedAveragePercentBasedUpon = value;
  }
}
