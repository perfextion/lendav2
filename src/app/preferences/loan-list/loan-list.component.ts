import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { LoanListSettings } from "@lenda/preferences/models/loan-list/index.model";

@Component({
  selector: 'app-loan-list',
  templateUrl: './loan-list.component.html',
  styleUrls: ['./loan-list.component.scss']
})
export class LoanListComponent implements OnInit {
  @Input() settings: LoanListSettings = new LoanListSettings();
  @Output() settingsChange: EventEmitter<LoanListSettings> = new EventEmitter<LoanListSettings>();

  constructor() { }

  ngOnInit() {
  }
}
