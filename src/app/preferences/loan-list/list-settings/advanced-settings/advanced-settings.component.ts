import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SortByOption } from "@lenda/preferences/models/base-entities/sort-by-options.enum";
import { ColumnSettings, DataType } from "@lenda/preferences/models/base-entities/column/index.model";
import { SortSettings } from "@lenda/preferences/models/base-entities/column/sort-settings.model";
import { LoanMasterSettings } from '@lenda/preferences/models/loan-list/list/loan-master/index.model';
import { LocalStorageService } from 'ngx-webstorage';
import { RefOfficeList, RefDataModel, RefLoanOfficer } from '@lenda/models/ref-data-model';
import { environment } from '@env/environment.prod';
import * as _ from 'lodash';
import { LoanTypeList, LoanStatusList, ListItem } from './advanced-settings.data';

@Component({
  selector: 'app-advanced-settings',
  templateUrl: './advanced-settings.component.html',
  styleUrls: ['./advanced-settings.component.scss']
})
export class AdvancedSettingsComponent implements OnInit {
  @Input() selectedItem: ColumnSettings;
  @Output() selectedItemChange: EventEmitter<any> = new EventEmitter<any>();

  @Input() label: string;
  @Input() selectedKey: string;

  DataType: typeof DataType = DataType;

  public sortByOptions: SortByOption[] = [SortByOption.ascending, SortByOption.descending];

  private refdata: RefDataModel;
  officeList: Array<RefOfficeList> = [];
  loanTypes: Array<ListItem>;
  loanStatusList: Array<ListItem>;
  loanOfficerList = [];

  private _LoanMasterSettings: LoanMasterSettings;

  constructor(private localStorage: LocalStorageService) {
    this._LoanMasterSettings = new LoanMasterSettings();
    this.loanTypes = LoanTypeList;
    this.loanStatusList = LoanStatusList;
  }

  ngOnInit() {
    this.refdata = this.localStorage.retrieve(environment.referencedatakey);

    this.officeList = _.sortBy(this.refdata.OfficeList, a => a.Office_Name);
    let defaultOffice = new RefOfficeList();
    this.officeList.unshift(defaultOffice);

    this.loanOfficerList = _.sortBy(this.refdata.LoanOfficers, a => a.LastName || a.FirstName);

    this.loanOfficerList = this.loanOfficerList.map(a => {
      return {
        Name: this.getLOName(a)
      }
    });

    this.loanOfficerList.unshift({
      Name: ''
    });
  }

  ngOnChanges() {
    if (!this.selectedItem.sort) {
      this.selectedItem.sort = new SortSettings();
    }

    let originalSettings: ColumnSettings = this._LoanMasterSettings[this.selectedKey];
    if(originalSettings) {
      this.selectedItem.datatype = originalSettings.datatype;
    }
  }

  getLOName(loan_officer: RefLoanOfficer) {
    let name = loan_officer.LastName || '';
    if (name) {
      name += ', ';
    }
    name += loan_officer.FirstName;

    return name;
  }
}
