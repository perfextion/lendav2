export class ListItem {
  code: string;
  name: string;
}

export const LoanStatusList = [
  { code: '', name: '' },
  { code: 'W', name: 'Working' },
  { code: 'R', name: 'Recommended' },
  { code: 'A', name: 'Approved' },
  { code: 'U', name: 'Uploaded' },
  { code: 'H', name: 'Historical' },
  { code: 'P', name: 'Paid Off' },
  { code: 'X', name: 'Written Off' },
  { code: 'V', name: 'Voided' },
  { code: 'Y', name: 'Withdrawn' },
  { code: 'Z', name: 'Declined' }
];

export const LoanTypeList = [
  { code: '', name: '' },
  { code: 'Ag-Pro', name: 'Ag-Pro' },
  { code: 'All-In', name: 'All-In' },
  { code: 'Ag-Input', name: 'Ag-Input' },
  { code: 'N/A', name: 'N/A' }
];
