import { Injectable } from '@angular/core';
import { LoanMasterSettings, LoanMasterColumn } from "@lenda/preferences/models/loan-list/list/loan-master/index.model";
import { ColumnSettings } from "@lenda/preferences/models/base-entities/column/index.model";
import { ListSettings } from "@lenda/preferences/models/loan-list/list/list.model";

@Injectable()
export class LoanDetailService {
  constructor() {
  }

  /**
   * Gets loan list column settings and returns a object to be sent to getLoanList
   * @param loanMasterSettings loan settings from my preferences
   */
  public getLoanListColumnsSettings(listSettings: ListSettings): LoanListRequestModel {
    let visibleCols = [];
    let items: Array<LoanListColumnItem> = [];

    Object.keys(listSettings.loanMasterSettings).forEach((col: LoanMasterColumn, index) => {
      let item: ColumnSettings = listSettings.loanMasterSettings[col];
      // Set cols
      if (item.visible || item.isIncludedByDefault) {
        visibleCols[item.colOrder.toString()] = {
          key: col,
          item: item
        };
      }
    });

    for (let col of visibleCols) {
      items.push({
        col: col.key,
        colOrder: col.item.colOrder,
        isDoNotDisplayEnabled: !col.item.isDisplayEnabled,
        sort: this.getSortValue(col.item.sort.sortOrder.charAt(0).toLowerCase()),
        filter: {
          select: col.item.filter.select,
          begin: col.item.filter.begin,
          end: col.item.filter.end
        }
      });
    }

    return {
      loanListView: this.getLoanListView(listSettings.isIncludeMyReportsEnabled),
      items: items,
      includeInactiveLoans: listSettings.isIncludeInactiveEnabled,
      includeLatestVersion: !listSettings.isLatestLoanVersionEnabled
    }
  };

  /**
   * Converts Loan Master object to query to be sent to the server to query
   * columns as per settings done in my preferences by the user
   * @param loanDetailSettings loan detail settings object
   */
  public getLoanMasterQuery(loanMasterSettings: LoanMasterSettings) {
    let cols = '';
    let colOrder = '';
    let sort = '';
    let visibleCols = [];
    Object.keys(loanMasterSettings).forEach((col: LoanMasterColumn, index) => {
      let item: ColumnSettings = loanMasterSettings[col];
      // Set cols
      if (item.visible || item.isIncludedByDefault) {
        visibleCols[item.colOrder.toString()] = {
          key: col,
          item: item
        };
      }
    });

    for (let col of visibleCols) {
      if (cols === '') {
        cols += col.key;
        sort += col.item.sort.sortOrder.charAt(0).toLowerCase();
        colOrder += col.item.colOrder;
      } else {
        cols += `,${col.key}`;
        sort += `,${col.item.sort.sortOrder.charAt(0).toLowerCase()}`;
        colOrder += `,${col.item.colOrder}`;
      }
    }

    return `cols=${cols}&colOrder=${colOrder}&sort=${sort}`;
    // Expected output:
    // /GetAllLoans?cols=Loan_ID,Loan_Seq_Num,Crop_Year
    // &sort=a,d,a
  }

  /**
   * Returns 0 if show all else 1
   * @param loanListView view option
   */
  private getLoanListView(isIncludeMyReportsEnabled: boolean) {
    return isIncludeMyReportsEnabled ?  1 : 0;
  }

  /**
   * Returns 0 if ascending
   * @param sort sort value
   */
  private getSortValue(sort) {
    if (sort === 'a') {
      return 0;
    }
    return 1;
  }
}

export class LoanListRequestModel {
  loanListView: number;
  items: Array<LoanListColumnItem>;
  includeInactiveLoans: boolean;
  includeLatestVersion: boolean;
}

export class LoanListColumnItem {
  col: string;
  sort: number;
  colOrder: number;
  filter: LoanListColumnFilterSetting;
  isDoNotDisplayEnabled: boolean;
}

export class LoanListColumnFilterSetting {
  select?: string;
  begin?: string;
  end?: string;
}
