import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { LoanMasterColumn, LoanMasterSettings } from "@lenda/preferences/models/loan-list/list/loan-master/index.model";
import { SettingsService } from "@lenda/preferences/settings/settings.service";
import { Preferences } from "@lenda/preferences/models/index.model";
import { ColumnSettings } from "@lenda/preferences/models/base-entities/column/index.model";
import { KeyValuePair } from "@lenda/preferences/models/base-entities/key-value-pair.model";
import { ColumnItem } from "@lenda/preferences/models/base-entities/column-item.model";
import { ListSettings } from "@lenda/preferences/models/loan-list/list/list.model";
import { LoanListSettings } from "@lenda/preferences/models/loan-list/index.model";
import { PortfolioAnalysisSettings } from "@lenda/preferences/models/loan-list/portfolio-analysis/index.model";
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-loan-detail',
  templateUrl: './loan-detail.component.html',
  styleUrls: ['./loan-detail.component.scss']
})
export class LoanDetailComponent implements OnInit, OnDestroy {
  @Input() settings: LoanMasterSettings = new LoanMasterSettings();
  @Output() settingsChange: EventEmitter<LoanMasterSettings> = new EventEmitter<LoanMasterSettings>();

  @Input() listSettings: ListSettings = new ListSettings();
  @Output() listSettingsChange: EventEmitter<ListSettings> = new EventEmitter<ListSettings>();

  @Input() portfolioAnalysisSettings: PortfolioAnalysisSettings = new PortfolioAnalysisSettings();
  @Output() portfolioAnalysisSettingsChange: EventEmitter<PortfolioAnalysisSettings> = new EventEmitter<PortfolioAnalysisSettings>();

  public isAdvancedSettingsActive: boolean = false;
  public selectedItem: ColumnSettings;
  public selectedColKey: LoanMasterColumn;

  public loanMasterColumns: ColumnItem[] = [];
  public loanMasterColumnsTarget: ColumnItem[] = [];
  public loanMasterColumnsEnum: LoanMasterColumn;

  private preferencesChangeSub: ISubscription;

  constructor(
    private settingsService: SettingsService
  ) { }

  ngOnInit() {
    this.initColumns();
    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe((value) => {
      // We need to run this once the callback has finished, hence setTimeout 0
      // to signify that run this code once it is finished
      setTimeout(() => this.initColumns(), 0);
    });
  }

  ngOnDestroy() {
    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  /**
   * Updates loan list option to user selection
   * @param ev checked event
   */
  onLoanListUpdated(ev) {
    // this.loanListSettings = ev.checked;
    // this.loanListSettingsChange.emit(ev.checked);
  }

  /**
   * Advanced settings panel for selected item
   * @param item item selected
   */
  onAdvancedSettings(item: LoanMasterColumn) {
    this.isAdvancedSettingsActive = true;
    // Find selected item in LoanMasterSetting based on name
    this.selectedColKey = item;
    this.selectedItem = this.settings[item];
  }

  /**
   * Updated pending action sort order based on checkbox value
   * @param ev checked event
   */
  onPendingActionSortOrderUpdated(ev) {
    this.listSettings.isPendingActionSortOrderEnabled = ev.checked;
  }

  /**
   * Update is my direct reports included
   * @param ev checked event
   */
  onIncludeMyDirectReporsUpdated(ev) {
    this.listSettings.isIncludeMyReportsEnabled = ev.checked;
  }

  /**
   * Update settings for portfolio view
   * @param ev checked event
   */
  onPortfolioViewUpdated(ev) {
    this.portfolioAnalysisSettings.isDetailRecordsEnabled = ev.checked;
  }

  /**
   * Is Latest Loan Version Enabled
   * @param ev checked event
   */
  onIsLatestLoanVersionEnabled(ev) {
    this.listSettings.isLatestLoanVersionEnabled = ev.checked;
  }

  onIncludeInactiveToggle(ev) {
    this.listSettings.isIncludeInactiveEnabled = ev.checked;
  }

  /**
   * Fired when an element is dragged and dropped on a target panel
   */
  onModelChanged($event) {
    let startIndex = -1;
    // Move all frozen items up again and then allocate colOrder
    Object.keys(this.settings).forEach((col: LoanMasterColumn, index) => {
      let item = this.settings[col];
      if (item.isFrozen) {
        item.visible = true;
        item.colOrder = ++startIndex;
      }
    });

    // Set visibility of each column
    Object.keys(this.settings).forEach((col: LoanMasterColumn, index) => {
      let item = this.settings[col];
      if (!item.isFrozen) {
        if (this.itemIndexInTarget(col) !== -1) {
          item.visible = true;
          let index = this.itemIndexInTarget(col);;
          if (index <= startIndex) {
            item.colOrder = ++startIndex;
          } else {
            item.colOrder = index;
          }
        } else {
          // Reset to initial as it is removed from list
          item.visible = false;
          item.colOrder = 0;
          item.filter = {};
        }
      }
    });
  }

  /**
   * Checks if item exists and returns index
   * @param col col item
   */
  itemIndexInTarget(col): number {
    let index = 0;
    let isExist = false;
    this.loanMasterColumnsTarget.forEach((target) => {
      if (target.key === col) {
        isExist = true;
      }
      if (!isExist) {
        index++;
      }
    });
    return isExist ? index : -1;
  }

  /**
   * Initializes the columns for draggable areas
   */
  private initColumns() {
    this.loanMasterColumns = [];
    this.loanMasterColumnsTarget = [];
    Object.keys(this.settings).map((column: LoanMasterColumn) => {
      let item = this.settings[column];
      if (item.visible) {
        this.loanMasterColumnsTarget[item.colOrder] = {
          key: column,
          value: LoanMasterColumn[column],
          settings: this.settings[column]
        };
      } else {
        this.loanMasterColumns.push({
          key: column,
          value: LoanMasterColumn[column],
          settings: this.settings[column]
        });
      }
    });
  }

  getLabel(key: string) {
    // let label: string = LoanMasterColumn[key];

    // try {
    //   label = label.substring(label.indexOf(':') + 1);
    //   return label.trim();
    // } catch {
    //   return label;
    // }
    return LoanMasterColumn[key];
  }
}
