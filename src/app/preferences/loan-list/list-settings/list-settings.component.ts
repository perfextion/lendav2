import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ListSettings } from "@lenda/preferences/models/loan-list/list/list.model";
import { PortfolioAnalysisSettings } from "@lenda/preferences/models/loan-list/portfolio-analysis/index.model";

@Component({
  selector: 'app-list-settings',
  templateUrl: './list-settings.component.html',
  styleUrls: ['./list-settings.component.scss']
})
export class ListSettingsComponent implements OnInit {
  @Input() settings: ListSettings = new ListSettings();
  @Output() settingsChange: EventEmitter<ListSettings> = new EventEmitter<ListSettings>();

  @Input() portfolioAnalysisSettings: PortfolioAnalysisSettings = new PortfolioAnalysisSettings();
  @Output() portfolioAnalysisSettingsChange: EventEmitter<PortfolioAnalysisSettings> = new EventEmitter<PortfolioAnalysisSettings>();
  constructor() { }

  ngOnInit() {
  }
}
