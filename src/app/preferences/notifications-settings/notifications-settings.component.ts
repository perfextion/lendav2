import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NotificationsSettings } from '../models/notifications-settings/index.model';

@Component({
  selector: 'app-notifications-settings',
  templateUrl: './notifications-settings.component.html',
  styleUrls: ['./notifications-settings.component.scss']
})
export class NotificationsSettingsComponent implements OnInit {
  @Input() settings: NotificationsSettings = new NotificationsSettings();
  @Output() settingsChange: EventEmitter<NotificationsSettings> = new EventEmitter<NotificationsSettings>();

  @Input() userTypeCode: number;

  constructor() { }

  ngOnInit() {
  }

}
