import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserNotificationSettings } from '@lenda/preferences/models/notifications-settings/user-notification.model';
import { ArmUserNotificationsSettings } from '@lenda/preferences/models/notifications-settings/arm-user-notifications-settings.model';
import { BorrowerNotificationsSettings } from '@lenda/preferences/models/notifications-settings/borrower-notifications-settings.model';
import { DistributerNotificationsSettings } from '@lenda/preferences/models/notifications-settings/distributer-notifications-settings.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PrefContactSetting } from '@lenda/preferences/models/notifications-settings/pref-contact.model';

export type Notification = ArmUserNotificationsSettings | BorrowerNotificationsSettings | DistributerNotificationsSettings;

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  private _settings: UserNotificationSettings[];
  public displayedColumns: string[] = ['title'];
  public dataSource: UserNotificationSettings[];
  public loanInventoryFrequencyUntilClosed: number;
  public loanBalanceFrequencyWhileActive: number;
  public prefContactSettings: PrefContactSetting;

  @Input() isLocked: boolean = false;
  @Output() settingsChange: EventEmitter<Notification> = new EventEmitter<Notification>();


  @Input()
  set settings(val: Notification) {
    this._settings = val.userNotificationSettings;
    this.prefContactSettings = val.prefContactSettings;
    this.dataSource = val.userNotificationSettings;
    this.loanBalanceFrequencyWhileActive = val.loanBalanceFrequencyWhileActive;
    this.loanInventoryFrequencyUntilClosed = val.loanInventoryFrequencyUntilClosed;
  }

  constructor() { }

  format = (val: number) => {
    return `${val.toLocaleString('en-US')}`;
  }

  ngOnInit() { }

  onModelUpdated(notification: UserNotificationSettings, ev) {
    let allSettings = this.dataSource.filter(a => a.parent_id == notification.id && !a.isRequired);
    let parent = this.dataSource.find(a => a.id == notification.parent_id);

    if(ev.checked) {
      allSettings.forEach(a => {
        this.markAllNodes(notification, a, true);
      });
    } else {
      allSettings.forEach(a => {
        this.markAllNodes(notification, a, false);
      });
    }

    if(parent) {
      parent.value = !this.checkPartial(parent);
    }

    this.updateSettings();
  }

  private updateSettings() {
    this.settingsChange.emit({
      userNotificationSettings: this.dataSource,
      loanInventoryFrequencyUntilClosed: this.loanInventoryFrequencyUntilClosed,
      loanBalanceFrequencyWhileActive: this.loanBalanceFrequencyWhileActive,
      prefContactSettings: this.prefContactSettings
    });
  }

  private markAllNodes(parentNotification: UserNotificationSettings, childNotification: UserNotificationSettings, value) {
    childNotification.value = value;

    let childs = this.dataSource.filter(a => a.parent_id == childNotification.id && !a.isRequired);
    if(childs.length > 0) {
      childs.forEach(c => {
        this.markAllNodes(parentNotification, c, value);
      });
    }
  }

  checkPartial(notification: UserNotificationSettings) {
    return !this.checkChild(notification);
  }

  private checkChild(notification: UserNotificationSettings) {
    let allChilds = this.dataSource.filter(a => a.parent_id == notification.id);
    if(allChilds.length > 0) {
      let res =  allChilds.every(a => a.value);
      allChilds.forEach(a => {
        res = res && this.checkChild(a);
      });
      return res;
    } else {
      return true;
    }
  }


  frequnceyDaysUpdate_Active(newValue: number) {
    this.loanBalanceFrequencyWhileActive = newValue;
    this.updateSettings();
  }

  frequnceyDaysUpdate_Closed(newValue: number) {
    this.loanInventoryFrequencyUntilClosed = newValue;
    this.updateSettings();
  }
}
