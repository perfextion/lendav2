import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserNotificationSettings } from "@lenda/preferences/models/notifications-settings/user-notification.model";

@Component({
  selector: 'app-user-notifications',
  templateUrl: './user-notifications.component.html',
  styleUrls: ['./user-notifications.component.scss']
})

export class UserNotificationsComponent implements OnInit {
  @Input() settings: UserNotificationSettings[];
  @Output() settingsChange: EventEmitter<UserNotificationSettings[]> = new EventEmitter<UserNotificationSettings[]>();

  displayedColumns: string[] = ['title', 'isArmUserCommitteeEnabled'];
  dataSource;

  constructor() { }

  ngOnInit() {
    this.dataSource = this.settings;
  }
}
