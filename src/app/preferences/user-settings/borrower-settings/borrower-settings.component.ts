import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BorrowerSettings } from "@lenda/preferences/models/user-settings/borrower-settings.model";

@Component({
  selector: 'app-borrower-settings',
  templateUrl: './borrower-settings.component.html',
  styleUrls: ['./borrower-settings.component.scss']
})
export class BorrowerSettingsComponent implements OnInit {
  @Input() borrowerSettings: BorrowerSettings;
  @Output() borrowerSettingsChange: EventEmitter<BorrowerSettings> = new EventEmitter<BorrowerSettings>();

  constructor() { }

  ngOnInit() {
  }

}
