import { Component, OnInit } from '@angular/core';
import { Input, Output, EventEmitter } from "@angular/core";
import { UserSettings } from "@lenda/preferences/models/user-settings/index.model";

/**
 * Enum being used by my preferences model to fill available page options
 */
export enum PageOption {
  schematic = 'schematic',
  reports = 'reports',
  borrower = 'borrower',
  crop = 'crop',
  farm = 'farm',
  insurance = 'insurance',
  budget = 'budget',
  optimizer = 'optimizer',
  collateral = 'collateral',
  committee = 'committee',
  checkout = 'checkout',
  disburse = 'disburse',
  reconcile = 'reconcile'
}

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {
  @Input() settings: UserSettings = new UserSettings();
  @Input() userTypeCode: number;

  @Output() settingsChange: EventEmitter<UserSettings> = new EventEmitter<UserSettings>();

  public pages: PageOption[] = [];

  constructor() { }

  ngOnInit() {
    this.pages = this.getPages();
  }

  /**
   * Iterates through page enum and set pages options
   */
  getPages(): PageOption[] {
    return <PageOption[]>Object.values(PageOption).map(item => item);
  }

  /**
   * Settings updated callback
   */
  onLandingPageUpdated(page) {
    this.settings.landingPage = page;
  }
}
