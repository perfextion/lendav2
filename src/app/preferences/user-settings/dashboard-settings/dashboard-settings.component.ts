import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ViewMode } from "@lenda/preferences/models/base-entities/view-modes.enum";
import { UserSettings } from '@lenda/preferences/models/user-settings/index.model';

@Component({
  selector: 'app-dashboard-settings',
  templateUrl: './dashboard-settings.component.html',
  styleUrls: ['./dashboard-settings.component.scss']
})
export class DashboardSettingsComponent implements OnInit {
  @Input() settings: UserSettings = new UserSettings();
  @Output() settingsChange: EventEmitter<UserSettings> = new EventEmitter<UserSettings>();
  public viewModes: ViewMode[] = [
    ViewMode.standard,
    ViewMode.condensed,
    ViewMode.supercondensed
    // ViewMode.none
  ];

  constructor() { }

  ngOnInit() {
  }


  /**
   * Updates the target page setting for viewmode
   * @param item Target setting
   * @param value Updated value of viewmode
   */
  onChartViewUpdated(item, value) {
    this.settings.dashboardSettings[item].viewMode = value;
  }
}
