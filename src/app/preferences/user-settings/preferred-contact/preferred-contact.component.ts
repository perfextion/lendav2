import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PrefContactSetting } from '@lenda/preferences/models/notifications-settings/pref-contact.model';

@Component({
  selector: 'app-preferred-contact',
  templateUrl: './preferred-contact.component.html',
  styleUrls: ['./preferred-contact.component.scss']
})
export class PreferredContactComponent implements OnInit {
  @Input() settings: PrefContactSetting;
  @Output() settingsChange: EventEmitter<PrefContactSetting> = new EventEmitter<PrefContactSetting>();

  constructor() {}

  ngOnInit() {}
}
