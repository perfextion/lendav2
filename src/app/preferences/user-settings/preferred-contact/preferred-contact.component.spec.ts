import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreferredContactComponent } from './preferred-contact.component';

describe('PreferredContactComponent', () => {
  let component: PreferredContactComponent;
  let fixture: ComponentFixture<PreferredContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreferredContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreferredContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
