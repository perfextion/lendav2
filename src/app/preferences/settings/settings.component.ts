import { Component, OnInit, KeyValueDiffer, OnDestroy } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { SessionStorageService } from 'ngx-webstorage';

import { Preferences } from '../models/index.model';
import { LoggedInUserModel } from '@lenda/models/commonmodels';

import { SettingsService } from './settings.service';
import { AdminUser } from '@lenda/models/loanmodel';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
  preferences: Preferences;
  public activeTab: number;
  user: LoggedInUserModel;

  public selectedAdminUser: string;
  public adminUsers: Array<AdminUser> = [];

  private preferencesChangeSub: ISubscription;

  constructor(
    private settingsService: SettingsService,
    private toaster: ToastrService,
    private sessionStorage: SessionStorageService
  ) {
    this.user = this.sessionStorage.retrieve('UID');
  }

  ngOnInit() {
    this.preferences = this.settingsService.preferences;
    this.preferencesChangeSub = this.settingsService.preferencesChange.subscribe(val => {
      this.preferences = val;
    });

    this.settingsService.getAdminUserList().subscribe(data => {
      if(data.ResCode == 1) {
        this.adminUsers = data.Data;
      }
    });
  }

  ngOnDestroy() {
    if(this.preferencesChangeSub) {
      this.preferencesChangeSub.unsubscribe();
    }
  }

  /**
   * Resets the settings to factory
   */
  resetToFactorySettings() {
    this.settingsService.resetToFactorySettings(this.selectedAdminUser, this.toaster);
  }

  /**
   * Saves current preferences in local storage
   */
  onSave() {
    this.settingsService.savePreferences(this.preferences, this.toaster);
  }
}
