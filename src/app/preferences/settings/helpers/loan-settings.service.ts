import { Injectable } from "@angular/core";
import { LoanSettings } from "@lenda/preferences/models/loan-settings/index.model";
import { loan_model } from "@lenda/models/loanmodel";
import { environment } from "@env/environment";
import { LocalStorageService } from "ngx-webstorage";

@Injectable()
export class LoanSettingsService {
    private loanObject: loan_model;

    constructor(
        private localStorageService: LocalStorageService
    ) { }

    /**
     * Returns loan settings
     * Checks whether the preferences exist in loan or whether user preferences are to be loaded
     */
    getLoanSettings(): LoanSettings | undefined {
        if (!this.loanObject) {
            this.loanObject = this.localStorageService.retrieve(environment.loankey);
        }

        // Loan Level Settings: If settings exist in LoanMaster, then return these
        if (this.loanObject && this.loanObject.LoanMaster && this.loanObject.LoanMaster.Loan_Settings) {
            return JSON.parse(this.loanObject.LoanMaster.Loan_Settings);
        }
    }
}