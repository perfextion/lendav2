import { Injectable, EventEmitter } from '@angular/core';

import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { environment } from '@env/environment';

import { Preferences } from './../models/index.model';
import { LoanListSettings } from "@lenda/preferences/models/loan-list/index.model";
import { LoanSettings } from "@lenda/preferences/models/loan-settings/index.model";
import { UserSettings } from "@lenda/preferences/models/user-settings/index.model";
import { Loansettings } from '@lenda/models/loansettings';
import { loan_model } from '@lenda/models/loanmodel';

import { AppsettingsApiService } from '@lenda/services/appsettingsapi.service';
import { KeysValidatorService } from "@lenda/preferences/utils/keys-validator.service";
import { ToastrService } from 'ngx-toastr';


@Injectable()
export class SettingsService {
  public isLocalStoragePreferenceValidated: boolean = false;
  public preferences: Preferences = new Preferences();
  public preferencesChange: BehaviorSubject<Preferences>;

  userLoanSettings: Loansettings;
  private loanObject: loan_model;

  enhancedDetailChange = new EventEmitter<boolean>(true);
  labelChange = new EventEmitter(true);

  constructor(
    private localStorageService: LocalStorageService,
    private sessionStorage: SessionStorageService,
    private appSettingsApiService: AppsettingsApiService,
    private keysValidatorService: KeysValidatorService
  ) {

    this.userLoanSettings = new Loansettings();
    this.loanObject = this.localStorageService.retrieve(environment.loankey);
    this.getPreferences();
    this.preferencesChange = new BehaviorSubject<Preferences>(this.preferences);
    this.getLoanSettings();
  }

  /**
   * Saves preferences in local storage
   * @param myPreferences Preferences object
   */
  public savePreferences(myPreferences: Preferences, toaster?): void {
    // Update in local storage
    this.localStorageService.store(environment.myPreferences, myPreferences);

    if(toaster){
      // Save to server
      this.appSettingsApiService
      .syncMyPreferences(JSON.stringify(myPreferences))
      .subscribe(res => {
        if (res.ResCode == 1) {
          // this.preferencesChange.next(myPreferences);
          toaster.success('Preferences Saved to Server');
        } else {
          toaster.error('Error in Server Sync');
        }
      });
    }

    this.preferencesChange.next(myPreferences);
    this.preferences = myPreferences;
  }

  public savePreferencesAsync(myPreferences: Preferences): void {
    // Update in local storage
    this.localStorageService.store(environment.myPreferences, myPreferences);

    this.appSettingsApiService
      .syncMyPreferences(JSON.stringify(myPreferences))
      .subscribe(res => {  });
  }

  /**
   * Reset to factory settings
   * resets the preferences back to the default
   */
  public resetToFactorySettings(selectedUser: string, toaster: ToastrService) {
    let loggedinUser = this.sessionStorage.retrieve('UID');
    let userid = loggedinUser.UserID;
    let officerId = loggedinUser.Office_ID;

    this.appSettingsApiService.copyPreferencesDefaults(userid, officerId, selectedUser).subscribe(response => {
      if(response.ResCode == 1) {
        this.preferences = JSON.parse(response.Data);
        this.localStorageService.store(environment.myPreferences, this.preferences);
        this.preferencesChange.next(this.preferences);
        toaster.success('Copy Defaults Successfully.', 'Success');
      } else {
        toaster.error('Copy Defaults Failed', 'Error');
      }
    },
    () => {
      toaster.error('Copy Defaults Failed', 'Error');
    });
  }

  public getPreferencesObs(): Observable<Preferences> {
    return this.preferencesChange.asObservable();
  }

  /**
   * ==============
   * HELPER METHODS
   * ==============
   */

  /**
  * Gets Preferences from local storage
  * or returns a new instance of Preferences with default settings
  */
  private getPreferences() {
    this.appSettingsApiService.getMyPreferences().subscribe((resp: any) => {

      this.isLocalStoragePreferenceValidated = true;

      if (resp.Data && Object.getOwnPropertyNames(resp.Data).length > 0) {
        let preferences = JSON.parse(resp.Data);

        // Additional mapper function to validate all keys of preferences
        // If a new model is available then it converts the keys appropriately
        // and returns the object as per new mapping
        this.preferences = preferences;

        this.setDefaultIfEmpty();
        this.getLoanSettings();
        this.preferencesChange.next(this.preferences);
      }
    });
  }

  /**
   * If preference for that object does not exist
   * then set to default factory setting
   */
  private setDefaultIfEmpty() {
    if (!this.preferences.loanListSettings || Object.getOwnPropertyNames(this.preferences.loanListSettings).length === 0) {
      this.preferences.loanListSettings = new LoanListSettings();
    }
    if (!this.preferences.loanSettings || Object.getOwnPropertyNames(this.preferences.loanSettings).length === 0) {
      this.preferences.loanSettings = new LoanSettings();
    }
    if (!this.preferences.userSettings || Object.getOwnPropertyNames(this.preferences.userSettings).length === 0) {
      this.preferences.userSettings = new UserSettings();
    }
  }

  /**
   * 1. Convert to deep keys - preferences and factoryPreferences
   * e.g. [generalSettings, generalSettings.landingPage]
   * 2. Compare both arrays and find out the differences
   * 3. Write a handle to resolve the differences.
   * a) Addition of key - instantiate it/ask the user to set preference for that by redirecting
   * b) Deletion of key - no action required
   * c) Name edit of key - delete previous property name and store value with new one
   */
  private convertPreferencesToUpdatedTypeIfDifferent(preferences) {
    let differences = this.keysValidatorService.checkKeyDifferences(preferences, new Preferences());
    if (differences.length !== 0) {
      // For each property in differences that is added (with + sign)
      // get default value from factory settings and add to current user preferences object
      differences.forEach((key) => {
        let keyName = key.replace('+', '');
        // Gets default value for preference and appends to the user object
        this.assignDeepValue(preferences, keyName);
      });
    }
    return preferences;
  }

  /**
   * Assigns preference value to deep key
   * @param preferences preference object
   * @param keyName deep value of key
   */
  private assignDeepValue(preferences, keyName) {
    let keys = keyName.split('.');
    let preferenceProp = preferences;
    let factorSettingsProp = this.preferences;

    // Go deep in the keys and assign the value to user preferences from factory settings
    keys.forEach((key, index) => {
      if (preferences[key]) {
        preferenceProp = preferenceProp[key];
        factorSettingsProp = factorSettingsProp[key];
      } else {
        preferenceProp[key] = factorSettingsProp[key];
      }
    });
  }

  private getLoanSettings(){
    if(!this.loanObject){
      this.loanObject = this.localStorageService.retrieve(environment.loankey);
    }

    if(this.loanObject && this.loanObject.LoanMaster && this.loanObject.LoanMaster.Loan_Settings){
      let LoanSet = JSON.parse(this.loanObject.LoanMaster.Loan_Settings);
      this.userLoanSettings.dashboardSettings = LoanSet && LoanSet.dashboardSettings ? LoanSet.dashboardSettings : this.preferences.userSettings.dashboardSettings.dashboard;
      this.userLoanSettings.reportsSettings = LoanSet && LoanSet.reportsSettings ? LoanSet.reportsSettings : this.preferences.loanSettings.reportsDisplaySettings;
    }
  }

  public setPreferences(preferences: Preferences) {
    this.preferences = preferences;
    this.preferencesChange.next(this.preferences);
  }

  public getPreferencesByUserId(userid: number) {
    return this.appSettingsApiService.getMyPreferencesbyuserid(userid);
  }

  public getAdminUserList() {
    return this.appSettingsApiService.getAdminList();
  }
}
