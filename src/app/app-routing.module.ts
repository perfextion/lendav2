import { NortridgeFunctionsComponent } from './components/debug/nortridge-functions/nortridge-functions.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { DeactivateGuard } from '@lenda/services/route/deactivate.guard';
import { UrlRResolver } from '@lenda/routeresolvers/url.resolver';
import { LoanOverviewDeactivateGuard } from '@lenda/components/loan-overview/loan-overview.deactivate.guard';

import { Page } from '@lenda/models/page.enum';

import { LoginComponent } from '@lenda/login/login.component';
import { PageNotFoundComponent } from '@lenda/page-not-found/page-not-found.component';
import { MasterComponent } from '@lenda/master/master.component';
import { DashboardComponent } from '@lenda/dashboard/dashboard.component';
import { LoanListComponent } from '@lenda/components/loan-list/loan-list.component';
import { LoanOverviewComponent } from '@lenda/components/loan-overview/loan-overview.component';
import { BorrowerComponent } from '@lenda/components/borrower/borrower.component';
import { CropComponent } from '@lenda/components/crop/crop.component';
import { FarmComponent } from '@lenda/components/farm/farm.component';
import { BudgetComponent } from '@lenda/components/budget/budget.component';
import { InsuranceComponent } from '@lenda/components/insurance/insurance.component';
import { LoanviewerComponent } from '@lenda/components/loanviewer/loanviewer.component';
import { CreateLoanComponent } from '@lenda/components/create-loan/create-loan.component';
import { CollateralComponent } from '@lenda/components/collateral/collateral.component';
import { FlowchartComponent } from '@lenda/components/flowchart/flowchart.component';
import { OptimizerComponent } from '@lenda/components/optimizer/optimizer.component';
import { CommitteeComponent } from '@lenda/components/committee/committee.component';
import { StyleguideComponent } from '@lenda/styleguide/styleguide.component';
import { ReportsComponent } from '@lenda/components/reports/reports.component';
import { LoanHistoryComponent } from '@lenda/components/loan-history/loan-history.component';
import { ViewBalancesComponent } from '@lenda/components/viewbalances/viewbalances.component';
import { FormatComponent } from '@lenda/components/format/format.component';
import { PreferencesModule } from '@lenda/preferences/preferences.module';
import { ClosingComponent } from '@lenda/components/closing/closing.component';
import { DisburseComponent } from '@lenda/components/disburse/disburse.component';
import { CheckoutComponent } from '@lenda/components/checkout/checkout.component';

// Debug Components
import { DebugComponent } from '@lenda/components/debug/debug.component';
import { NortridgeUploadComponent } from '@lenda/components/debug/nortridge-upload/nortridge-upload.component';
import { SubsidiaryDatabaseDebugComponent } from '@lenda/components/debug/subsidiary-database-debug/subsidiary-database-debug.component';
import { LoanRxDebugComponent } from '@lenda/components/debug/loan-rx-debug/loan-rx-debug.component';
import { LoanSyncDebugComponent } from '@lenda/components/debug/loan-sync-debug/loan-sync-debug.component';
import { LogDebugComponent } from '@lenda/components/debug/log-debug/log-debug.component';
import { LocalStorageDebugComponent } from './components/debug/local-storage-debug/local-storage-debug.component';
import { OtherQueueComponent } from './components/debug/other-queue/other-queue.component';
import { WorkInProgressComponent } from './components/debug/work-in-progress/work-in-progress.component';
import { CustomentryComponent } from './components/debug/customentry/customentry.component';
import { LoanOverviewActivateGuard } from './components/loan-overview/loan-overview.activate.guard';

// This is done as there is a bug in Angular 5/6 while lazy loading the module
// DO NOT DELETE -- Used to ensure PreferencesModule is loaded in the same bundle.
// Referencing the function directly in `loadChildren` breaks AoT compiler.
export function preferencesModule() {
  return PreferencesModule;
}

const appRoutes: Routes = [
  {
    path: 'preferences',
    loadChildren: './preferences/preferences.module#PreferencesModule'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: MasterComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
        resolve: { message: UrlRResolver }
      },
      {
        path: 'loans',
        component: LoanListComponent,
        data: { page: Page.loanlist }
      },
      {
        path: 'Loanobjectpreview',
        component: LoanviewerComponent,
        data: { page: Page.loanobjectpreview }
      },
      {
        path: 'createloan',
        component: CreateLoanComponent,
        data: { page: Page.createloan }
      },
      {
        path: 'loanoverview/:loan/:seq',
        component: LoanOverviewComponent,
        canActivate: [LoanOverviewActivateGuard],
        canDeactivate: [LoanOverviewDeactivateGuard],
        children: [
          {
            path: 'summary',
            component: ReportsComponent,
            data: { page: Page.summary }
          },
          {
            path: 'reports',
            redirectTo: 'summary',
            pathMatch: 'full'
          },
          {
            path: 'borrower',
            component: BorrowerComponent,
            data: { page: Page.borrower }
          },
          {
            path: 'crop',
            component: CropComponent,
            data: { page: Page.crop },
            canDeactivate: [DeactivateGuard]
          },
          {
            path: 'farm',
            component: FarmComponent,
            data: { page: Page.farm },
            canDeactivate: [DeactivateGuard]
          },
          {
            path: 'insurance',
            component: InsuranceComponent,
            data: { page: Page.insurance }
          },
          {
            path: 'budget',
            component: BudgetComponent,
            data: { page: Page.budget }
          },
          {
            path: 'collateral',
            component: CollateralComponent,
            data: { page: Page.collateral }
          },
          {
            path: 'committee',
            component: CommitteeComponent,
            data: { page: Page.committee }
          },
          {
            path: 'closing',
            component: ClosingComponent,
            data: { page: Page.closing }
          },
          {
            path: 'work-in-progress',
            component: WorkInProgressComponent,
            data: { page: Page.workInProgress }
          },
          {
            path: 'other-queue',
            component: OtherQueueComponent,
            data: { page: Page.otherqueue }
          },
          {
            path: 'Loan Details',
            component: WorkInProgressComponent,
            data: { page: Page.loandetails }
          },
          {
            path: 'custom-entry',
            component: CustomentryComponent,
            data: { page: Page.customEntry }
          },
          {
            path: 'debug',
            component: DebugComponent,
            data: { page: Page.debug }
          },
          {
            path: 'optimizer',
            component: OptimizerComponent,
            data: { page: Page.optimizer }
          },
          {
            path: 'schematic',
            component: FlowchartComponent,
            data: { page: Page.schematic }
          },
          {
            path: 'disburse',
            component: DisburseComponent,
            data: { page: Page.disburse }
          },
          {
            path: 'loanHistory',
            component: LoanHistoryComponent,
            data: { page: Page.loanHistory }
          },
          {
            path: 'viewBalances',
            component: ViewBalancesComponent,
            data: { page: Page.viewBalances }
          },
          {
            path: 'checkout',
            component: CheckoutComponent,
            data: { page: Page.checkout }
          },
          {
            path: 'local-storage-debug',
            component: LocalStorageDebugComponent,
            data: { page: Page.debug }
          },
          {
            path: 'Nortridge',
            component: NortridgeUploadComponent
          },
          {
            path: 'Nortridgefunctions',
            component: NortridgeFunctionsComponent
          },
          {
            path: 'subsidiary-database-debug',
            component: SubsidiaryDatabaseDebugComponent,
            data: { page: Page.debug }
          },
          {
            path: 'loan-rx-debug',
            component: LoanRxDebugComponent,
            data: { page: Page.debug }
          },
          {
            path: 'loan-sync-debug',
            component: LoanSyncDebugComponent,
            data: { page: Page.debug }
          },
          {
            path: 'log-debug',
            component: LogDebugComponent,
            data: { page: Page.debug }
          }
        ]
      },
      {
        path: 'format',
        component: FormatComponent
      }
    ]
  },
  {
    path: 'admin',
    loadChildren: 'app/admin/admin.module#AdminModule'
  },
  {
    path: 'styleguide',
    component: StyleguideComponent
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      preloadingStrategy: PreloadAllModules,
      useHash: true
    })
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
