import * as _ from 'lodash';

export function setgriddefaults(gridapi: any, columnapi: any) {
  //toggletoolpanel(false, gridapi)
  removeHeaderMenu(gridapi);
  autoSizeAll(columnapi); //call in last always
}

export function autoSizeAll(gridColumnApi: any) {
  gridColumnApi.columnController.autoSizeAllColumns('contextMenu');
}

export function toggletoolpanel(toggle: boolean, gridapi: any) {
  let elem = gridapi.gridCore.eGridDiv;
  settoolpanelhidden(elem);
}

function settoolpanelhidden(Ele) {
  Ele.getElementsByClassName('ag-tool-panel')[0].style.display = 'none';
}

export function removeHeaderMenu(gridapi: any) {
  let coldefs = gridapi.gridCore.gridOptions.columnDefs;
  coldefs.forEach(function(column) {
    column.suppressMenu = true;
    column.suppressSorting = false;
    if (column.children != undefined) {
      column.children.forEach(element => {
        element.suppressMenu = true;
        element.suppressSorting = false;
      });
    }
  });
  gridapi.setColumnDefs(coldefs);
  gridapi.gridCore.gridOptions.enableSorting = true;
}

export function calculatecolumnwidthsins(columnApi: any) {
  let width =
    _.sum(
      columnApi.columnController.allDisplayedColumns.map(p => p.actualWidth)
    ) + 20;
  return Math.min(width, 1420) + 'px';
}

export function calculatecolumnwidths(columnApi: any) {
  return columnApi.columnController.bodyWidth + 40 + 'px';
}

export function isgrideditable(defaultvalue: boolean, Loan_Status = null) {
  let loanstatus;

  if (Loan_Status) {
    // console.time('isgrideditable - Argument');
    loanstatus = Loan_Status;
    // console.timeEnd('isgrideditable - Argument');
  } else {
    // console.time('isgrideditable - Localstorage')
    loanstatus = JSON.parse('[' + window.localStorage.getItem("ng2-webstorage|currentselectedloan") + ']')[0].LoanMaster.Loan_Status;
    // console.timeEnd('isgrideditable - Localstorage');
  }

  if (loanstatus == 'W') {
    return defaultvalue;
  }
  return false;
}

export function isgrideditabledisburse(defaultvalue: boolean) {
  return defaultvalue;
}

export function isgrideditableinsurancepoilcytable(
  params,
  defaultvalue: boolean,
  Loan_Status = null
) {
  let loanstatus: string;

  if (!Loan_Status) {
    // console.time('isgrideditableinsurancepoilcytable - Localstorage');
    loanstatus = JSON.parse('[' + window.localStorage.getItem("ng2-webstorage|currentselectedloan") + ']')[0].LoanMaster.Loan_Status;
    // console.timeEnd('isgrideditableinsurancepoilcytable - Localstorage');
  } else {
    // console.time('isgrideditableinsurancepoilcytable - Argument');
    loanstatus = Loan_Status;
    // console.time('isgrideditableinsurancepoilcytable - Argument');
  }

  if (loanstatus == 'W') {
    if (params.data.ispinned == true && !params.column.colId.includes('wfrp')) {
      return false;
    }

    if (params.data.ispinned != true && params.column.colId.includes('wfrp')) {
      return false;
    }
    return defaultvalue;
  }

  return false;
}

export function ischeckboxeditable() {
  let loanstatus = JSON.parse('[' + window.localStorage.getItem("ng2-webstorage|currentselectedloan") + ']')[0].LoanMaster.Loan_Status;
  if (loanstatus == 'W') {
    return true;
  } else { return false; }
}

export function ischeckboxeditablewfrp() {
  let loan = JSON.parse('[' + window.localStorage.getItem("ng2-webstorage|currentselectedloan") + ']');
  let loanstatus = loan[0].LoanMaster.Loan_Status;
  // if(loan[0].BorrowerIncomeHistory.filter(p=>p.FC_Borrower_Income!=0).length==0)
  // {
  //   return false;
  // }
  if (loanstatus == 'W') {
    return true;
  } else { return false; }
}
export function cellclassmaker(
  celltype: CellType,
  editable: boolean,
  overloadclass = '',
  Loan_Status = null
) {
  //
  let classobj: string = '';
  if (celltype == CellType.Integer) {
    classobj = 'rightaligned';
  }
  classobj = classobj + ' ' + overloadclass;
  if (editable) {
    if (isgrideditable(editable, Loan_Status)) {
      classobj = classobj + ' editable-color ' + overloadclass;
    }
  }
  return classobj;
}

export function cellclassmakerdisburse(
  celltype: CellType,
  editable: boolean,
  overloadclass = '',
  Loan_Status = null
) {
  //
  let classobj: string = '';
  if (celltype == CellType.Integer) {
    classobj = 'rightaligned';
  }
  classobj = classobj + ' ' + overloadclass;
  if (editable) {
    classobj = classobj + ' editable-color ' + overloadclass;
  }
  return classobj;
}

export function cellclassmakerverify(
  celltype: CellType,
  editable: boolean,
  Loan_Status = null
) {
  let classobj: string = '';
  if (celltype == CellType.Integer) {
    classobj = 'rightaligned';
  }
  if (editable) {
    if (isgrideditable(editable, Loan_Status)) {
      classobj = classobj + ' editable-color-verify';
    }
  }
  return classobj;
}

export function headerclassmaker(celltype: CellType, extra: string = '') {
  let classobj: string = '';
  if (celltype == CellType.Integer) {
    classobj = 'rightaligned';
  }
  return classobj + ' ' + extra;
}

export enum CellType {
  Integer,
  Text
}

export interface IParam<Component, GridRowType> {
  context: {
    componentParent: Component;
  };
  data: GridRowType;
}

export interface SelectEditorParams {
  values: Array<{ key: any; value: any }>;
}

export interface IAgGridContext<T> {
  componentParent: T;
  [key: string]: any ;
}

export interface IAgGridComponents {
  [key: string]: any ;
}

export interface IValueGetterParams {
    oldValue: any, // the value before the change
    newValue: any, // the value after the change
    data: any, // the data you provided for this row
    colDef: any;
}
