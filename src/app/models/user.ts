export class User {
   public firstName:string;
   public email:string;
}

export class LoanOfficerStats {
  Latest_Crop_Year: number;
  Total_Commitment: number;
  Average_Close_Days: number;

  constructor() {
    this.Latest_Crop_Year = new Date().getFullYear();
    this.Total_Commitment = 0;
    this.Average_Close_Days = 10;
  }
}
