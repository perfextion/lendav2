export class EmailModel {
  loanNumber: string;
  emailTemplateType: EmailTemplateType;
}

export enum EmailTemplateType {
  LoanApprovedEmail = 0,
  UserRegistrationEmail = 1
}

export class EmailTemplate {
  id: number;
  text: string;
}

export const EmailTemplates: Array<EmailTemplate> = [
  {
    text: 'Loan Approved Email',
    id: EmailTemplateType.LoanApprovedEmail
  },
  {
    text: 'User Registration Email',
    id: EmailTemplateType.UserRegistrationEmail
  }
];
