import { CommentTypeEnum } from '../comment-type/comment-type.model';

export class AddPA {
  LoanFullId: string;

  /**
   * User id who is initiating the action
   */
  FromUserId: number;

  /**
   * Ref Pending Action Id
   */
  Ref_PA_Id: number;

  /**
   * Custom Comment Text
   */
  CustomText: string = '';

  /**
   * System Generated Message
   */
  System_Comment: string;

  /**
   * PA_Group Code
   */
  PA_Group: string;

  CommentType: CommentTypeEnum;
}

/**
 * Add Pending Action Model
 */
export class AddPendingActionModel extends AddPA {
  /**
   * List of User ids to which action is being initiated
   */
  ToUserIds: Array<number> = new Array();
}

export class AddPAByRoles extends AddPA {
  /**
   * List of Roles
   */
  ToRoles: Array<string>;
}

export class Remove_PA {
  LoanFullId: string;
  PA_Code: string;
  User_Id: number;
}

export const PA_Icon_Color = {
  RMND: 'flaticon-primary',
  VOTE: 'flaticon-primary',
  CONF: 'flaticon-error',
  CCMT: 'flaticon-error',
  STL2: 'flaticon-error',
  STL1: 'flaticon-warning',
  WST3: 'flaticon-error-wst',
  WST2: 'flaticon-warning-wst',
  DOC2: 'flaticon-error',
  DOC1: 'flaticon-primary',
  VER2: 'flaticon-error',
  VER1: 'flaticon-primary',
  RCN2: 'flaticon-error',
  RCN1: 'flaticon-primary',
  DIS: 'flaticon-primary',
  SUP: 'flaticon-sup-primary '
};

export const PA_Icon_Color_With_Reminder = {
  RMND: 'flaticon-primary',
  VOTE: 'active flaticon-primary',
  CONF: 'active flaticon-error',
  CCMT: 'active flaticon-error',
  STL2: 'active flaticon-error',
  STL1: 'active flaticon-warning',
  WST3: 'active flaticon-error-wst',
  WST2: 'active flaticon-warning-wst',
  DOC2: 'active flaticon-error',
  DOC1: 'active flaticon-primary',
  VER2: 'active flaticon-error',
  VER1: 'active flaticon-primary',
  RCN2: 'active flaticon-error',
  RCN1: 'active flaticon-primary',
  DIS: 'active flaticon-primary',
  SUP: 'active flaticon-sup-primary '
};

export const PA_Icon_Color_Loan = {
  RMND: 'flaticon-black',
  VOTE: 'flaticon-black',
  CONF: 'flaticon-black',
  CCMT: 'flaticon-black',
  STL2: 'flaticon-black',
  STL1: 'flaticon-black',
  WST3: 'flaticon-black',
  WST2: 'flaticon-black',
  DOC2: 'flaticon-black',
  DOC1: 'flaticon-black',
  VER2: 'flaticon-black',
  VER1: 'flaticon-black',
  RCN2: 'flaticon-black',
  RCN1: 'flaticon-black',
  DIS: 'flaticon-black',
  SUP: 'flaticon-sup-black'
};

export const PA_Icon_Color_Loan_With_Reminder = {
  RMND: 'flaticon-black active',
  VOTE: 'flaticon-black active',
  CONF: 'flaticon-black active',
  CCMT: 'flaticon-black active',
  STL2: 'flaticon-black active',
  STL1: 'flaticon-black active',
  WST3: 'flaticon-black active',
  WST2: 'flaticon-black active',
  DOC2: 'flaticon-black active',
  DOC1: 'flaticon-black active',
  VER2: 'flaticon-black active',
  VER1: 'flaticon-black active',
  RCN2: 'flaticon-black active',
  RCN1: 'flaticon-black active',
  DIS: 'flaticon-black active',
  SUP: 'flaticon-sup-black active'
};

export class AddSupportQueueModel {
  LoanFullID: string;
  Roles: Array<string>;
}

export enum PendingAction {
  Support = 'SUP',
  Disburse = 'DIS',
  Vote = 'VOTE',
  Stale = 'STL',
  Conference = 'CONF',
  WorkingStatus = 'WST',
  Documentation = 'DOC',
  Verify = 'VER',
  Reconcile = 'RCN'
}

export class EmailAndNotificationModel {
  LoanFullID: string;
  EmailTemplateType: EmailTemplateType;
  EmailData?: any;
}

export enum EmailTemplateType {
  LoanApproval = 1,
  UserRegistration,
  LoanRecommendation,
  LoanStatusChange,
  PendingActionReminder,
  CommitteeComment,
  LoanInventory,
  LoanBalance
}
