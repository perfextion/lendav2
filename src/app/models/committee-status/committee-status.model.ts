export class CommitteeStatus {
  id: number;
  title: string;
  icon: string;
  innerStatus: InnerCommitteeStatus[];
  parentIcon?: string;
}

export class InnerCommitteeStatus {
  id: number;
  title: string;
  icon: string;
}

export class SelectedCommitteeStatus {
  committeeStatusId: number;
  innerCommitteeStatusId: number;
}

export class RecordVote {
  Loan_Full_ID: string;
  User_ID: number;
  Vote: number;
  VoteType: number;
  Comment: string;
  LoanList: string[];
  System_Comment: string;
}

export class RescindVote {
  Loan_Full_ID: string;
  User_ID: number;
  CM_ID: number;
  Comment: string;
  LoggedIn_UserId: number;
  System_Comment: string;
}
