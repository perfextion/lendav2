import { CommitteeStatus } from './committee-status.model';

export const CommitteeStatusMock: CommitteeStatus[] = [
  {
    id: 1,
    title: 'Approved',
    icon: 'em-thumbsup',
    parentIcon: 'em',
    innerStatus: [
      {
        icon: 'em-thumbsup',
        id: 0,
        title: 'Approved'
      },
      {
        id: 1,
        title: 'One of the Best',
        icon: 'em-trophy'
      },
      {
        id: 2,
        title: 'Good Looking Opportunity',
        icon: 'em-gem'
      },
      {
        id: 3,
        title: 'Congrats on Risk Return',
        icon: 'em-dart'
      },
      {
        id: 4,
        title: 'No Visible Alarms',
        icon: 'em-no_bell'
      },
      {
        id: 5,
        title: 'Improve Return',
        icon: 'em-chart_with_upwards_trend'
      }
    ]
  },
  {
    id: 3,
    icon: 'em-grey_question',
    title: 'Undecided',
    parentIcon: 'em',
    innerStatus: [
      {
        id: 1,
        title: 'Provide Key to Performance',
        icon: 'em-key'
      },
      {
        id: 2,
        title: 'Less Noise; to the Point Comments',
        icon: 'em-headphones'
      },
      {
        id: 3,
        title: 'Add Controlled Disbursement',
        icon: 'em-closed_lock_with_key'
      },
      {
        id: 4,
        title: 'Include all Closing Conditions',
        icon: 'em-construction'
      },
      {
        id: 5,
        title: 'Increase Risk Cushion',
        icon: 'em-bulb'
      },
      {
        id: 6,
        title: 'More Details',
        icon: 'em-flashlight'
      },
      {
        id: 7,
        title: 'More Analysis',
        icon: 'em-mag'
      },
      {
        id: 8,
        title: 'Structure is Lacking',
        icon: 'em-shit'
      },
      {
        id: 9,
        title: 'More Borrower Reference',
        icon: 'em-ear'
      }
    ]
  },
  {
    id: 2,
    title: 'Decline',
    icon: 'em-thumbsdown',
    parentIcon: 'em',
    innerStatus: [
      {
        icon: 'em-thumbsdown',
        id: 0,
        title: 'Declined'
      },
      {
        id: 1,
        title: 'Borrower Seems Squirrelly',
        icon: 'em-chipmunk'
      },
      {
        id: 2,
        title: 'Borrower Seems of Low Character',
        icon: 'em-snake'
      },
      {
        id: 3,
        title: 'Borrower May Have Other Problems',
        icon: 'em-pill'
      },
      {
        id: 4,
        title: 'Loan Does Not Smell Well',
        icon: 'em-nose'
      },
      {
        id: 5,
        title: 'Measured Risk Unacceptable',
        icon: 'em-thermometer'
      },
      {
        id: 6,
        title: 'Significant Risk',
        icon: 'em-bomb'
      },
      {
        id: 7,
        title: 'Dangerously Risky',
        icon: 'em-skull_and_crossbones'
      },
      {
        id: 8,
        title: 'Flush It And Move On',
        icon: 'em-toilet'
      }
    ]
  },
  {
    id: 4,
    title: 'Recommend',
    icon: '',
    parentIcon: 'flaticon',
    innerStatus: [
      {
        icon: 'flaticon-forward-arrow',
        id: 0,
        title: 'Acknowledge loan comment and exceptions.'
      }
    ]
  },
  {
    id: 5,
    title: 'Rescinded',
    icon: 'replay',
    parentIcon: 'material',
    innerStatus: [
      {
        id: 0,
        title: 'Rescinded',
        icon: 'replay'
      }
    ]
  },
  {
    id: 6,
    title: 'Watchlist',
    icon: 'replay',
    parentIcon: 'material',
    innerStatus: [
      {
        id: 0,
        title: 'removed from Watchlist',
        icon: 'remove_red_eye'
      },
      {
        id: 1,
        title: 'Added to Watchlist',
        icon: 'remove_red_eye'
      },
    ]
  }
];
