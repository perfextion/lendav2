export class EditLoanParamsModel {
  LoanFullID: string;
  Crop_Year: number;
  Office_ID: number;
  Office_Name: string;
  Region_ID: number;
  Region_Name: string;
  Loan_Officer_ID: number;
  Loan_Officer_Name: string;
  Update_Crop_Year_Ind: number;
}
