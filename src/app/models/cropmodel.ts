import {
  JsonProperty,
  JsonObject,
} from 'json2typescript';
import {
  IntConverter,
  StringConverter
} from '../Workers/utility/jsonconvertors';

@JsonObject
export class Loan_Crop_Unit {
  @JsonProperty('Loan_CU_ID', IntConverter, false)
  Loan_CU_ID: number = undefined;

  @JsonProperty('Loan_ID', IntConverter, false)
  Loan_ID: number = 0;

  @JsonProperty('Farm_ID', IntConverter, false)
  Farm_ID: number = 0;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string = '';

  @JsonProperty('CU_Acres', IntConverter, false)
  CU_Acres: number = 0;

  @JsonProperty('CU_APH', IntConverter, false)
  CU_APH: number = 0;

  @JsonProperty('Crop_Type_Code', StringConverter, false)
  Crop_Type_Code: string = '';

  @JsonProperty('Crop_Code', StringConverter, false)
  Crop_Code: string = '';

  @JsonProperty('Crop_Practice_Type_Code', StringConverter, false)
  Crop_Practice_Type_Code: string = '';

  @JsonProperty('Crop_Practice_ID', IntConverter, false)
  Crop_Practice_ID: number = 0;

  @JsonProperty('Z_Price', IntConverter, false)
  Z_Price: number = 0;

  @JsonProperty('Z_Basis_Adj', IntConverter, false)
  Z_Basis_Adj: number = 0;

  @JsonProperty('Z_Marketing_Adj', IntConverter, false)
  Z_Marketing_Adj: number = 0;

  @JsonProperty('Z_Rebate_Adj', IntConverter, false)
  Z_Rebate_Adj: number = 0;

  @JsonProperty('Z_Adj_Price', IntConverter, false)
  Z_Adj_Price: number = 0;

  @JsonProperty('Booking_Ind', IntConverter, false)
  Booking_Ind: number = 0;

  @JsonProperty('Status', IntConverter, false)
  Status: number = 0;

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number = 0;

  @JsonProperty('Added_Land', Boolean, false)
  Added_Land: boolean = false;

  @JsonProperty('Rate_Yield', IntConverter, false)
  Rate_Yield: number;
  // New Properties
  @JsonProperty('Ins_Value', IntConverter, false)
  Ins_Value: number = 0;

  @JsonProperty('Disc_Ins_value', IntConverter)
  Disc_Ins_value: number = 0;

  @JsonProperty('Mkt_Value', IntConverter)
  Mkt_Value: number = 0;

  @JsonProperty('Disc_Mkt_Value', IntConverter)
  Disc_Mkt_Value: number = 0;

  @JsonProperty('CEI_Value', IntConverter)
  CEI_Value: number = 0;

  @JsonProperty('Disc_CEI_Value', IntConverter)
  Disc_CEI_Value: number = 0;

  Crop_Unit_Key: string;

  Crop_Full_Key: string;
  // FC Values
  FC_Landlord: string;
  FC_LandAdded: string = 'Yes';
  FC_ProdPerc: number;
  FC_CountyID: number;
  FC_StateID: number;
  FC_FSN: string;
  FC_Section: string;
  FC_Rating: string;
  FC_Ins_Unit: string;
  FC_Ins_Policy_ID: number;
  FC_CropYield: number;
  FC_Primary_limit: number;
  FC_Stax: number;
  FC_SCO: number;
  FC_Revenue: number = 0;
  FC_Insurance_Share: number = 0;
  FC_ModifiedAPH: number = 0;

  //MPCI
  FC_MPCIvalue: number = 0;
  FC_Disc_MPCI_value: number = 0;
  FC_MpciPremium: number = 0;
  FC_Premium_MPCI: number = 0;

  //HMAX
  FC_Hmaxvalue: number = 0;
  FC_Disc_Hmaxvalue: number = 0;
  FC_HmaxPremium: number = 0;
  FC_Premium_Hmax: number = 0;

  //STAX
  FC_Staxvalue: number = 0;
  FC_Disc_Staxvalue: number = 0;
  FC_StaxPremium: number = 0;
  FC_Premium_Stax: number = 0;
  d;

  //SCO
  FC_Scovalue: number = 0;
  FC_Disc_Scovalue: number = 0;
  FC_Premium_SCO: number = 0;
  FC_ScoPremium: number = 0;

  //Ramp
  FC_Rampvalue: number = 0;
  FC_RampPremium: number = 0;
  FC_Disc_Rampvalue: number = 0;
  FC_Premium_Ramp: number = 0;
  //ICE
  FC_Icevalue: number = 0;
  FC_IcePremium: number = 0;
  FC_Disc_Icevalue: number = 0;
  FC_Premium_ICE: number = 0;
  //
  //ABC
  FC_Abcvalue: number = 0;
  FC_AbcPremium: number = 0;
  FC_Disc_Abcvalue: number = 0;
  FC_Premium_ABC: number = 0;
  //
  //PCI
  FC_Pcivalue: number = 0;
  FC_PciPremium: number = 0;
  FC_Disc_Pcivalue: number = 0;
  FC_Premium_PCI: number = 0;
  //
  //CropHail
  FC_Crophailvalue: number = 0;
  FC_CrophailPremium: number = 0;
  FC_Disc_Crophailvalue: number = 0;
  FC_Premium_CropHail: number = 0;
  //
  Ins_APH: number = 0;
  FC_Level1Perc: number = undefined;

  //Extra unscategorized properties
  Z_Crop_Adj_Price: number = undefined;

  FC_Cashflow: number = undefined;
  FC_Cashflow_Acre: number = undefined;

  FC_RiskCushion: number = undefined;
  FC_RiskCushion_Acre: number = undefined;

  FC_Margin: number = undefined;
  FC_Margin_Acre: number = undefined;

  //Verification related Models
  Verf_Ins_APH: number = 0;
  Verf_Ins_APH_Status: boolean = false;
  Verf_Ins_APH_touched: boolean = false;

  //Verification related Models On Optimizer wrt Acres
  Verf_FSA_Acres: number = 0;
  Verf_FSA_Acres_Status: boolean = false;
  Verf_FSA_Acres_touched: boolean = false;

  //Verification related Models On Optimizer wrt Acres
  Verf_AIP_Acres: number = 0;
  Verf_AIP_Acres_Status: boolean = false;
  Verf_AIP_Acres_touched: boolean = false;

  //Additional Properties As per Brad
  Arm_Budget_Acre: number = 0;
  Dist_Budget_Acre: number = 0;
  Third_Budget_Acre: number = 0;
  Arm_finance_Cost: number = 0;
  Dist_Finance_cost: number = 0;
  Premium: number = 0;
  Arm_Margin: number = 0;
  Total_Margin: number = 0;
  Arm_Budget_Sum: number = 0;
  Dist_Budget_Sum: number = 0;
  Third_Budget_Sum: number = 0;
  Total_Budget_Sum: number = 0;
  Finance_Cost_Sum: number = 0;
  Premium_Sum: number = 0;
  Arm_Margin_Sum: number = 0;
  Total_Margin_Sum: number = 0;

  Ins_Shr_Value_Sum: number = 0;
  Ins_Disc_Shr_Value_Sum: number = 0;
  Mkt_Shr_Value_Sum: number = 0;
  Mkt_Disc_Shr_Value_Sum: number = 0;

  Mkt_Value_Acre: number = 0;
  Disc_Mkt_Value_Acre: number = 0;

  FC_MPCIvalue_Acre: number = 0;
  FC_Disc_MPCI_value_Acre: number;

  FC_Hmaxvalue_Acre: number = 0;
  FC_Disc_Hmaxvalue_Acre: number = 0;

  FC_Scovalue_Acre: number = 0;
  FC_Disc_Scovalue_Acre: number = 0;

  FC_Staxvalue_Acre: number = 0;
  FC_Disc_Staxvalue_Acre: number = 0;

  FC_Rampvalue_Acre: number = 0;
  FC_Disc_Rampvalue_Acre: number = 0;

  FC_Icevalue_Acre: number = 0;
  FC_Disc_Icevalue_Acre: number = 0;

  FC_Abcvalue_Acre: number = 0;
  FC_Disc_Abcvalue_Acre: number = 0;

  FC_Pcivalue_Acre: number = 0;
  FC_Disc_Pcivalue_Acre: number = 0;

  FC_Crophailvalue_Acre: number = 0;
  FC_Disc_Crophailvalue_Acre: number = 0;

  Ins_Value_Acre: number = 0;
  Disc_Ins_value_Acre: number = 0;

  CEI_Value_Acre: number = 0;
  Disc_CEI_Value_Acre: number = 0;

  FC_Total_Used_Acres: number;
  FC_Irr_Acres: number;
  FC_NI_Acres: number;
  FC_Total_Used_Irr_Acres: number;
  FC_Total_Used_NI_Acres: number;

  FC_Rent_Percent: number;
  FC_Disabled: boolean;

  FC_State_Name: string;
  FC_County_Name: string;
}

export class Crop_Unit_Policy {
  @JsonProperty('Loan_Plan_Detail_ID', IntConverter, false)
  Loan_Plan_Detail_ID: number = undefined;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string = '';

  @JsonProperty('Loan_Crop_Unit_ID', IntConverter, false)
  Loan_Crop_Unit_ID: number = 0;

  @JsonProperty('Loan_Policy_ID', IntConverter, false)
  Loan_Policy_ID: number = 0;

  @JsonProperty('Ins_Plan_Code', StringConverter, false)
  Ins_Plan_Code: string = '';

  @JsonProperty('Ins_Plan_Type_Code', StringConverter, false)
  Ins_Plan_Type_Code: string = '';

  @JsonProperty('Ins_Qty', IntConverter, false)
  Ins_Qty: number = 0;

  @JsonProperty('Ins_Value', IntConverter, false)
  Ins_Value: number = 0;

  @JsonProperty('Disc_Ins_Value', IntConverter, false)
  Disc_Ins_Value: number = 0;

  @JsonProperty('Ins_Value_Acre', IntConverter, false)
  Ins_Value_Acre: number = 0;

  @JsonProperty('Disc_Ins_Value_Acre', IntConverter, false)
  Disc_Ins_Value_Acre: number = 0;

  @JsonProperty('Premium', IntConverter, false)
  Premium: number = 0;

  @JsonProperty('Status', IntConverter, false)
  Status: number = 0;

  @JsonProperty('Status', IntConverter, false)
  Custom1: number = 0;
}
export class APH_Units {
  @JsonProperty('Loan_CU_ID', IntConverter, false)
  Loan_CU_ID: number = undefined;

  @JsonProperty('State_ID', IntConverter, false)
  State_ID: number = 0;

  @JsonProperty('County_ID', IntConverter, false)
  County_ID: number = 0;

  @JsonProperty('Crop_Code', StringConverter, false)
  Crop_Code: string = '';

  @JsonProperty('Ins_APH', IntConverter, false)
  Ins_APH: number = 0;

  @JsonProperty('Crop_Practice_Type_Code', StringConverter, false)
  Crop_Practice_Type_Code: string = '';

  @JsonProperty('Prod_Perc', IntConverter, false)
  Prod_Perc: number = 0;

  @JsonProperty('LandLord', StringConverter, false)
  LandLord: string = '';

  @JsonProperty('FSN', StringConverter, false)
  FSN: string = '';

  @JsonProperty('Section', StringConverter, false)
  Section: string = '';

  @JsonProperty('Rating', StringConverter, false)
  Rating: string = '';

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number = 0;

  @JsonProperty('Added_Land', Boolean, false)
  Added_Land: boolean;

  @JsonProperty('Rate_Yield', IntConverter, false)
  Rate_Yield: number = 0;

  @JsonProperty('Uom', StringConverter, false)
  Uom: string = '';

  Verf_Ins_APH: number = 0;
  Verf_Ins_APH_Status: boolean = false;
  Verf_Ins_APH_touched: boolean = false;
  Fc_Custom: boolean = false;
  FC_StateName:String="";
  FC_CountyName: string;
}
export class Loan_Crop_Unit_FC {
  FC_TotalRevenue: number = 0;
  FC_TotalBudget: number = 0;
  FC_EstimatedInterest: number = 0;
  FC_TotalCashFlow: number = 0;
  FC_SubtotalCropRevenue: number = 0;
  FC_SubTotalAcres: number = 0;
}

export class Loan_Crop_History_FC {
  FC_Crop_Type_Code: string;
  FC_Crop_Yield: number = 0;
  FC_Crop_APH: number = 0;
}

@JsonObject
export class V_Crop_Price_Details {
  @JsonProperty('Crop_And_Practice_ID', IntConverter, false)
  Crop_And_Practice_ID: number = undefined;

  @JsonProperty('Crop_Code', StringConverter, false)
  Crop_Code: string = undefined;

  @JsonProperty('Crop_Name', StringConverter, false)
  Crop_Name: string = undefined;

  @JsonProperty('Crop_Type_Code', StringConverter, false)
  Crop_Type_Code: String = undefined;

  @JsonProperty('Irr_Prac_Code', StringConverter, false)
  Irr_Prac_Code: String = undefined;

  @JsonProperty('Crop_Prac_Code', StringConverter, false)
  Crop_Prac_Code: String = undefined;

  @JsonProperty('Irr_NI_Ind', IntConverter, false)
  Irr_NI_Ind: number = undefined;

  @JsonProperty('RMA_Ref_Yield', IntConverter, false)
  RMA_Ref_Yield: number = undefined;

  @JsonProperty('Std_Crop_Type_Practice_Type_Ind', IntConverter, false)
  Std_Crop_Type_Practice_Type_Ind: number = undefined;

  @JsonProperty('Final_Planting_Date', StringConverter, false)
  Final_Planting_Date: String = undefined;

  @JsonProperty('Crop_Full_Key', StringConverter, false)
  Crop_Full_Key: String = undefined;

  @JsonProperty('State_ID', StringConverter, false)
  State_ID: String = undefined;

  @JsonProperty('Region_ID', StringConverter, false)
  Region_ID: String = undefined;

  @JsonProperty('Office_ID', StringConverter, false)
  Office_ID: String = undefined;

  @JsonProperty('Crop_Year', IntConverter, false)
  Crop_Year: number = undefined;

  @JsonProperty('MPCI_UOM', StringConverter, false)
  MPCI_UOM: String = undefined;

  @JsonProperty('Price', IntConverter, false)
  Price: number = undefined;

  @JsonProperty('Basis', IntConverter, false)
  Basis: number = undefined;

  @JsonProperty('Rebate', IntConverter, false)
  Rebate: number = undefined;

  @JsonProperty('Adj_Price', IntConverter, false)
  Adj_Price: number = undefined;

  @JsonProperty('Status', IntConverter, false)
  Status: number = undefined;
}

export class Loan_Crop_Type_Practice_Type_Yield_EditModel {
  CropId: number | null;
  LoanFullID: string | null;
  PropertyName: string;
  PropertyValue: string;
  CropYear: number | null;
  YieldLine: number | null;
  IsPropertyYear: boolean;
}

export class Loan_Crop_Type_Practice_Type_Yield_AddModel {
  Crop: number;
  CropType: number;
  Crop_ID: string;
  Loan_ID: number;
  IrNI: string | null;
  Practice: number | null;
  CropYield: number | null;
  APH: number;
  InsUOM: string;
}
