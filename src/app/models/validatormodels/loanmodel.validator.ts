import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsPositive, IsNotEmpty} from "class-validator";
export class Loan_Master_validator
{
    Loan_Master_ID: number;
    Loan_ID: number ;
    @IsNotEmpty()
    Loan_Seq_num: number ;
    @IsNotEmpty()
    Loan_Full_ID: string;
    Crop_Year: number ;
    Region_ID: number ;
    Office_ID: number ;
    User_ID: number ;
    Farmer_ID: number ;
    Borrower_ID: number ;
    Loan_Type_Code: number ;
    @IsDate()
    Application_Date: Date | string ;
    Borrower_3yr_Tax_Returns: number ;
    Borrower_ID_Type: number ;
    Borrower_SSN_Hash: string;
    Borrower_Entity_Type_Code: string;
    Borrower_Last_Name: string;
    Borrower_First_Name: string;
    Borrower_MI: string;
    Borrower_Address: string;
    Borrower_City: string;
    Borrower_State_Abbrev: string;
    Borrower_Zip: number ;
    Borrower_Phone: string;
    Borrower_email: string;
    Borrower_DL_State: string;
    Borrower_Dl_Num: string;
    Borrower_DOB: Date | string ;
    Borrower_Preferred_Contact_Ind: number ;
    Borrower_County_ID: number ;
    Borrower_Lat: number ;
    Borrower_Long: number ;
    Co_Borrower_ID: string;
    Co_Borrower_ID_Type: number ;
    Co_Borrower_SSN_Hash: string;
    Co_Borrower_Entity_Type_Code: number ;
    Co_Borrower_Last_Name: string;
    Co_Borrower_First_Name: string;
    Co_Borrower_MI: string;
    Co_Borrower_Address: string;
    Co_Borrower_City: string;
    Co_Borrower_State: string;
    Co_Borrower_Zip: number ;
    Co_Borrower_Phone: string;
    Co_Borrower_email: string;
    Co_Borrower_DL_State: string;
    Co_Borrower_DL_Num: string;
    Co_Borrower_DOB: Date | string ;
    Co_Borrower_Preferred_Contact_ind: number ;
    Co_Borrower_Count: number ;
    Spouse_SSN_Hash: string;
    Spouse_Last_name: string;
    Spouse_First_Name: string;
    Spouse_MI: string;
    Spouse_Address: string;
    Spouse_City: string;
    Spouse_State: string;
    Spouse_Zip: number ;
    Spouse_Phone: string;
    Spouse_Email: string;
    Spouse_Preffered_Contact_Ind: number ;
    Farmer_ID_Type: number ;
    Farmer_SSN_Hash: string;
    Farmer_Entity_Type: number ;
    Farmer_Last_Name: string;
    Farmer_First_Name: string;
    Farmer_MI: string;
    Farmer_Address: string;
    Farmer_City: string;
    Farmer_State: string;
    Farmer_Zip: number ;
    Farmer_Phone: string;
    Farmer_Email: string;
    Farmer_DL_State: string;
    Farmer_DL_Num: string;
    Farmer_DOB: Date | string ;
    Farmer_Preferred_Contact_Ind: number ;
    Year_Begin_Farming: number ;
    Year_Begin_Client: number ;
    Refferal_Type_Code: number ;
    Distributor_ID: number ;
    Agency_Primary: number ;
    Current_Bankruptcy_Status: number ;
    Previously_Bankrupt: number ;
    Judgement: number ;
    Local_Ind: number ;
    Credit_Score: number ;
    Credit_Score_Date: Date | string ;
    CPA_Prepared_Financials: number ;
    Financials_Date: Date | string ;
    Current_Assets: number ;
    Current_Assets_Disc_Percent: number;
    Current_Liabilities: number ;
    Current_Disc_Net_Worth: number ;

    Inter_Assets: number ;
    Inter_Assets_Disc_Percent: number ;
    Inter_Liabilities: number ;
    Inter_Disc_Net_Worth: number ;

    Fixed_Assets: number ;
    Fixed_Assets_Disc_Percent: number ;
    Fixed_Liabilities: number ;
    Fixed_Disc_Net_Worth: number ;

    Total_Assets: number ;
    @IsInt()
    @IsPositive()
    Total_Liabilities: number ;
    @IsInt()
    @IsPositive()
    Total_Disc_Net_Worth: number ;
    Borrower_Farm_Financial_Rating: number ;
    Borrower_Rating: number ;
    Request_Credit: number ;
    Maturity_Date: Date | string ;
    Estimated_Days: number ;
    Orgination_Fee_Percent: number ;
    Service_Fee_Percent: number ;
    Rate_Percent: number ;
    Orgination_Fee_Amount: number ;
    Service_Fee_Amount: number ;
    Rate_Fee_Amount: number ;
    @IsInt()
    @IsPositive()
    Total_Crop_Acres: number ;
    @IsInt()
    @IsPositive()
    Total_Revenue: number ;
    @IsInt()
    @IsPositive()
    ARM_Commitment: number ;
    @IsInt()
    @IsPositive()
    Dist_Commitment: number ;
    Third_Party_Credit: number ;
    @IsInt()
    @IsPositive()
    Total_Commitment: number ;
    @IsInt()
    @IsPositive()
    Cash_Flow_Amount: number ;
    Break_Even_Percent: number ;
    @IsInt()
    @IsPositive()
    Ag_Pro_Requested_Credit: number ;
    @IsInt()
    @IsPositive()
    Risk_Cushion_Amount: number ;
    Risk_Cushion_Percent: number ;
    Return_Percent: number ;
    Delta_Crop_Acres: number ;
    Delta_Total_Revenue: number ;
    Delta_ARM_Commitment: number ;
    Delta_Dist_Commitment: number ;
    Delta_Third_Party_Credit: number ;
    Delta_Total_Commitment: number ;
    Delta_Orgination_Fee_Amount: number ;
    Delta_Service_Fee_Amount: number ;
    Delta_Cash_Flow_Amount: number ;
    Delta_Risk_Cushion_Amount: number ;
    Delta_Risk_Cushion_Percent: number ;
    Delta_Return_Percent: number ;
    @IsInt()
    @IsPositive()
    Net_Market_Value_Crops: number ;
    @IsInt()
    @IsPositive()
    Net_Market_Value_Insurance: number ;
    @IsInt()
    @IsPositive()
    Net_Market_Value_FSA: number = 0;
    @IsInt()
    @IsPositive()
    Net_Market_Value_Livestock: number ;
    @IsInt()
    @IsPositive()
    Net_Market_Value_Stored_Crops: number ;
    @IsInt()
    @IsPositive()
    Net_Market_Value_Equipment: number ;
    @IsInt()
    @IsPositive()
    Net_Market_Value_Real_Estate: number ;
    @IsInt()
    @IsPositive()
    Net_Market_Value_Other: number ;
    @IsInt()
    @IsPositive()
    Disc_value_Crops: number ;
    @IsInt()
    @IsPositive()
    Disc_value_Insurance: number ;
    @IsInt()
    @IsPositive()
    Disc_value_FSA: number ;
    @IsInt()
    @IsPositive()
    Disc_value_Livestock: number ;
    @IsInt()
    @IsPositive()
    Disc_value_Stored_Crops: number ;
    @IsInt()
    @IsPositive()
    Disc_value_Equipment: number ;
    @IsInt()
    @IsPositive()
    Disc_value_Real_Estate: number ;
    @IsInt()
    @IsPositive()
    Disc_value_Other: number ;
    @IsInt()
    @IsPositive()
    Disc_CEI_Value: number ;
    Prev_Addendum_Percent: number ;
    Current_Addendum_Percent: number ;
    Rate_Yeild_Ref_Yeild_Percent: number ;
    Original_Application_Date: Date | string ;
    Original_Maturity_Date: Date | string ;
    Original_Bankruptcy_Status: number ;
    Decision_Date: Date | string ;
    Close_Date: Date | string ;
    Cross_Collateral_ID: number ;
    Controlled_Disbursement_Ind: number ;
    Watch_List_ind: number ;
    Loan_Pending_Action_Type_Code: number ;
    Loan_Pending_Action_Level: number ;
    Loan_Status: string;
    Prev_Loan_Status: string;
    Nortridge_API_In_ID: number ;
    IsDelete: boolean;
}
