/**
 * Pages in the app used by publish service for displaying
 * whether or not publish is required
 */
export enum Page {
  schematic = 'schematic',
  reports = 'reports',
  borrower = 'borrower',
  crop = 'crop',
  farm = 'farm',
  insurance = 'insurance',
  budget = 'budget',
  optimizer = 'optimizer',
  collateral = 'collateral',
  committee = 'committee',
  closing = 'closing',
  reconciliation = 'reconciliation',
  myPreferences = 'my preferences',
  favorites = 'favorites',
  workInProgress = 'work in progress',
  customEntry = 'custom entry',
  checkout = 'checkout',
  otherqueue = 'otherqueue',
  customentry = 'customentry',
  debug = 'debug',
  flowchart = 'flowchart',
  loandetails = 'loandetails',
  loanlist = 'loanlist',
  createloan = 'createloan',
  summary = 'summary',
  disburse = 'disburse',
  loanHistory = 'loanHistory',
  viewBalances = 'viewBalances',
  loanobjectpreview = 'loanobjectpreview'
}
