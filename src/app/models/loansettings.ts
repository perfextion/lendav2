import { ReportsDisplaySettings } from "@lenda/preferences/models/loan-settings/reports-display.model";
import { DashboardItemSettings } from "@lenda/preferences/models/user-settings/dashboard.model";
import { ViewArea } from "@lenda/preferences/models/base-entities/view-area.enum";
import { ViewMode } from "@lenda/preferences/models/base-entities/view-modes.enum";

export class Loansettings{
    //insurance Settings
    /**
     *
     */
    constructor() {

       this.insurance_policy_Settings=new Insurance_Policy_Settings();
       this.HrInclusion=true;

    }
   public insurance_policy_Settings:Insurance_Policy_Settings;
   public HrInclusion:boolean;
   public validation_errors: ValidationError = {};
   public Validations: any = {};
   public Loan_key_Settings:Loan_Key_Visibilty=new Loan_Key_Visibilty();
   public committeeLevel: number = 1;
   public Export_Excel_Settings: any;

   public dashboardSettings: DashboardAllSettings = new DashboardAllSettings();
   public reportsSettings: ReportsSettings = new ReportsSettings();
}

export class DashboardAllSettings {
  viewMode: string = ViewMode.standard;
}

export class ReportsSettings {
  isArmDetailEnabled: boolean = true;
  isCropDetailEnabled: boolean = true;
  isConditionDetailEnabled: boolean = true;
  isDiscValueEnabled: boolean = true;
  isShowCompletedEnabled: boolean = true;
}

//SubModels
export class Insurance_Policy_Settings {
  constructor() {
    this.policy_visibility_state = new Insurance_Policy_Tabs();
  }
  public policy_visibility_state: Insurance_Policy_Tabs;
}

export class Insurance_Policy_Tabs {
  /**
   *
   */
  constructor() {}

  public MPCI: boolean=true;
  public STAX: boolean=false;
  public SCO: boolean=false;
  public HMAX: boolean=false;
  public PCI: boolean=false;
  public ABC: boolean=false;
  public RAMP: boolean=false;
  public ICE: boolean=false;
  public CROPHAIL: boolean=false;
  public WFRP: boolean=false;
  public HR: boolean=false;
}


// Added for Saving validationError in Local storage

export type ValidationError = { [key: string]: Validation };

export class Validation {
  /**
   * Exceptions
   */
  exceptions: {
    /**
     * Level 1 Exceptions
     */
    level1: number;

    /**
     * Level 2 Exceptions
     */
    level2: number
  }

  /**
   * Validations
   */
  validations: {
    /**
     * Level 1 Validations
     */
    level1: number;

    /**
     * Level 2 Validations
     */
    level2: number;
  }
}

export class ValidationModel {
  /**
   * Exception Level 1
   */
  level1: number;

  /**
   * Exception Level 2
   */
  level2: number;

  /**
   * Validation Level 1
   */
  level3: number;

  /**
   * Validation Level 2
   */
  level4: number;

  constructor() {
    this.level1 = 0;
    this.level2 = 0;
    this.level3 = 0;
    this.level4 = 0;
  }
}

//This Model is intented for keys hide and unhide on tables

export class Loan_Key_Visibilty{

  constructor() {
    this.Section = true;
    this.Rated=true;
    this.Crop_Type = true;
    this.Crop_Practice=true;
    this.Irr_Practice = false;
    this.RateYield=false;
    this.APH=false;

  }
  Section:boolean;
  Rated:boolean;
  Crop_Type:boolean;
  Irr_Practice:boolean;
  Crop_Practice:boolean;
  RateYield:boolean;
  APH:boolean;

}
