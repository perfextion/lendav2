export enum DiscountKeys {
  // TERMS
  OriginationFeePercent = 'ORG_PCT',
  ExtensionFeePercent = 'EXT_PCT',
  ServiceFeePercent = 'SERVICE_PCT',
  InterestFeePercent = 'INTEREST_PCT',
  MaturityDate = 'MATURITY_DATE',
  EstimatedDays = 'EST_DAYS',

  // COST_TO_CARRY
  COST_TO_CARRY = 'COST_TO_CARRY',

  // RISK SCALE
  RISK_SCALE = 'RISK_SCALE',

  // RETURN SCALE
  RETURN_SCALE = 'RETURN_SCALE'
}
