import { JsonObject, JsonProperty } from 'json2typescript';

import {
  StringConverter,
  IntConverter
} from '@lenda/Workers/utility/jsonconvertors';

import { RefQuestions } from './loan-response.model';
import { V_Crop_Price_Details } from './cropmodel';
import { LoanStatus } from './loanmodel';

export class RefDataModel {
  CountyList: RefCountyList[];
  StateList: RefStateList[];
  OfficeList: RefOfficeList[];
  RegionList: RefRegionModel[];
  BudgetDefault: RefBudgetDefault[];
  BudgetExpenseType: RefBudgetExpenseType[];
  CropList: V_Crop_Price_Details[];
  RefQuestions: RefQuestions[];
  Crops: RefCrop[];
  Ref_Crop_Basis_List: Ref_Crop_Basis[];
  Discounts: RefDiscount[];
  RefAssociations: RefAssociation[];
  Databasename: string;
  RefTabs: RefTab[];
  RefChevrons: RefChevron[];
  RefPendingActions: RefPendingAction[];
  ContractType: RefContractType[];
  CollateralCategory: RefCollateralCategory[];
  CollateralMktDisc: RefCollateralMktDisc[];
  CollateralInsDisc: RefCollateralInsDisc[];
  CollateralActionDiscAdj: RefCollateralActionDiscAdj[];
  MeasurementType: RefMeasurementType[];
  CommitteeLevels: RefCommitteeLevel[];
  LoanOfficers: RefLoanOfficer[];
  InsValidation: RefInsValidation[];
  RMA_Insurance_Offers: Array<RMA_Insurance_Offer>;
  Ref_Association_Defaults: Array<Ref_Association_Default>;
  RefExceptions: Array<Ref_Exception>;
  RefValidations: Array<RefValidation>;
  Ref_Other_Incomes: Array<Ref_Other_Income>;

  Ref_Document_Details: Array<Ref_Document_Detail>;
  Ref_Document_Templates: Array<Ref_Document_Template>;
  Ref_Document_Types: Array<Ref_Document_Type>;

  Ref_VCA_Fields: Array<Ref_VCA_Field>;
  ARM_Defaults: Array<ARM_Defaults_Model>;
  Ref_Option_Adj_Lookups: Array<Ref_Option_Adj_Lookup>;

  Ref_List_Items: Array<Ref_List_Item>;

  Ref_Hyperfield_Validations: RefHyperfieldValidation[];

  Ref_Ins_Validation_Rules: Array<Ref_Ins_Validation_Rule>;
  Ref_Ins_Eligibility_Rules: Array<Ref_Ins_Eligibility_Rule>;

  Ref_Association_Types: Array<Ref_Association_Type>;

  Ref_Comment_Emojies: Array<Ref_Comment_Emoji>;
  Ref_Comment_Types: Array<Ref_Comment_Type>;

  Ref_Ins_Policy_Plans: Array<Ref_Ins_Policy_Plan>;

  Ref_Farm_Financial_Ratings: Array<Ref_Farm_Financial_Rating>;
  Ref_Borrower_Ratings: Array<Ref_Borrower_Rating>;

  Ref_Loan_Types: Array<Ref_Loan_Type>;
  Ref_Ins_Plan_Actuarials: Array<Ref_Ins_Plan_Actuarial>;
}

export class Ref_Assoc_Type {
  Assoc_Type_ID: number;
  Assoc_Type_Code: string;
  Assoc_Type_Description: string;
  Editable_Ind: number;
  Status: number;
}

export class Ref_Borrower_Entity_Type {
  Entity_Type_ID: number;
  Entity_Type_Code: string;
  Entity_Type_Name: string;
  Status: number;
}

export class Ref_Crop_Basis {
  Ref_Basis_ID: number;
  Crop_Code: string;
  Crop_Name: string;
  Crop_Type_Code: string;
  Basis_Adj: number;
  Z_Office_ID: number;
  Z_Region_ID: number;
  Status: number;

  constructor() {
    this.Crop_Code = '';
    this.Basis_Adj = 0;
    this.Z_Office_ID = 0;
    this.Z_Region_ID = 0;
  }
}

export class Exception_Detail {
  Level_1_Value: boolean;
  Level_2_Value: boolean;
  Id: number;
  Support: boolean;
  Pending_Action: boolean;
  Mitigation: boolean;

  Documents: Array<number> = [];
  Exisiting_Documents: Array<number> = [];
  Deleted_Documents: Array<number> = [];

  constructor() {
    this.Documents = [];
    this.Exisiting_Documents = [];
    this.Deleted_Documents = [];
    this.Level_1_Value = false;
    this.Level_2_Value = false;
    this.Support = false;
    this.Pending_Action = false;
    this.Mitigation = false;
  }
}

export class RefValidation {
  Validation_ID: number;
  Validation_ID_Text: string;
  Tab_ID: number;
  Sort_Order: number;
  Level_1_Hyper_Code: string;
  Level_2_Hyper_Code: string;
  Pending_Action_Required_Ind: number;
  Pending_Action_Code: string;
  Status: number;
}

@JsonObject
export class Ref_Exception {
  @JsonProperty('Exception_ID', IntConverter, false)
  Exception_ID: number;

  @JsonProperty('Exception_ID_Text', StringConverter, false)
  Exception_ID_Text: string;

  @JsonProperty('Tab_ID', IntConverter, false)
  Tab_ID: number;

  @JsonProperty('Chevron_ID', IntConverter, false)
  Chevron_ID: number;

  @JsonProperty('Sort_Order', IntConverter, false)
  Sort_Order: number;

  @JsonProperty('Level_1_Hyper_Code', StringConverter, false)
  Level_1_Hyper_Code: string;

  @JsonProperty('Level_2_Hyper_Code', StringConverter, false)
  Level_2_Hyper_Code: string;

  @JsonProperty('Mitigation_Text_Required_Ind', IntConverter, false)
  Mitigation_Text_Required_Ind: number;

  @JsonProperty('Support_Required_Ind', IntConverter, false)
  Support_Required_Ind: number;

  @JsonProperty('Support_Role_Type_Code_1', StringConverter, false)
  Support_Role_Type_Code_1: string;

  @JsonProperty('Support_Role_Type_Code_2', StringConverter, false)
  Support_Role_Type_Code_2: string;

  @JsonProperty('Support_Role_Type_Code_3', StringConverter, false)
  Support_Role_Type_Code_3: string;

  @JsonProperty('Support_Role_Type_Code_4', StringConverter, false)
  Support_Role_Type_Code_4: string;

  @JsonProperty('Document_Type_ID', IntConverter, false)
  Document_Type_ID: number;

  @JsonProperty('Document_Required_Ind', IntConverter, false)
  Document_Required_Ind: number;

  @JsonProperty('Pending_Action_Required_Ind', IntConverter, false)
  Pending_Action_Required_Ind: number;

  @JsonProperty('Pending_Action_Code', StringConverter, false)
  Pending_Action_Code: string;

  @JsonProperty('Custom_Ind', IntConverter, false)
  Custom_Ind: number;

  @JsonProperty('Status', IntConverter, false)
  Status: number;

  Exception_Details: Exception_Detail;
}

//#region Document

export class Ref_Document_Detail {
  Document_Detail_ID: number;
  Document_Type_ID: number;
  Field_ID: number;
  Table_Name: string;
  Field_Name: string;
  Critical_Ind: number;
  Status: number;
}

export class Ref_Document_Template {
  Document_Template_ID: number;
  State_ID: number;
  Docuument_Type_ID: number;
  Document_Template_Name: string;
  Document_Content: string;
  Status: number;
}

export class Ref_Document_Type {
  Document_Type_ID: number;
  Document_Type_Name: string;
  Document_Type_Level: number;
  Sort_Order: number;
  Standard_Condition_Ind: number;
  State_Variation_Ind: number;
  Replication_Association_Type_Code: string;
  Assoc_Replication_Code: string;
  Document_Name_Override: string;
  Status: number;
  Table_Name: string;
}

export enum Ref_Document_Type_Level {
  Underwriting = 1,
  Prerequisite = 2,
  Closing = 3,
  Crop_Monitoring = 4
}

//#endregion Document

export class ARM_Defaults_Model {
  Default_ID: number;
  ARM_Default_Value: string;
  Default_Value_Type: string;
  Status: string;
}

export class Ref_VCA_Field {
  Ref_VCA_ID: number;
  CA_No: string;
  Name: string;
  Hyper_Code: string;
  Status: number;
}

export class Ref_Other_Income {
  Other_Income_ID: number;
  Other_Income_Name: string;
  Sort_Order: number;
  Status: number;
}
export class Ref_Association_Default {
  Assoc_Def_ID: number;
  Assoc_Name: string;
  Assoc_Type_Code: string;
  Office_ID: number;
  User_ID: number;
  Contact: string;
  Location: string;
  Phone: string;
  Email: string;
  Preferred_Contact_Ind: number;
  Status: number;

  Assoc_ID: number;
  Validation_ID: number;
  Exception_ID: number;

  IsOther: boolean;
}

export class RMA_Insurance_Offer {
  RMA_Price_ID: number;
  Crop_Year: number;
  RMA_Price_Key: string;
  Ins_Plan_Code?: any;
  Ins_Plan_Type_Code?: any;
  Crop_Code?: any;
  Crop_Type?: any;
  Irr_Prac?: any;
  Crop_Prac?: any;
  State_ID?: any;
  County_ID?: any;
  MPCI_Base_Price: number;
  MPCI_Harvest_Price: number;
  Price_Established: number;
}

export class RMA_Insurance_Offer_Model {
  RMA_Price_Key: string;
  MPCI_Base_Price: number;
  MPCI_Harvest_Price: number;
  Price_Established: number;

  constructor(key: string) {
    this.RMA_Price_Key = key;
    this.MPCI_Base_Price = 0;
    this.MPCI_Harvest_Price = 0;
    this.Price_Established = 0;
  }
}

export interface RefInsValidation {
  Ref_InsValid_ID: number;
  Ins_V_Key: string;
  Ins_V_Value: string;
  IV_Detail?: any;
  IV_Group?: any;
  CreatedOn?: any;
}
export class RefLoanOfficer {
  Job_Title_ID: number;
  FirstName: string;
  MI: string;
  LastName: string;
  UserID: number;

  constructor() {
    this.FirstName = '';
    this.LastName = '';
  }
}

export class RefMeasurementType {
  Measurement_Type_Code: string;
  Measurement_Type_Name: string;
  Collateral_Category: string;
  Disc_Percent: number;
  Exception_Ind: string;
  Status: number;
}
export class RefCollateralInsDisc {
  Ins_Plan_Code?: string;
  Ins_Type_Code?: string;
  Collateral_Category_Code: string;
  Collateral_Code?: string;
  Collateral_Type_Code?: string;
  Irr_Practice_Type_Code?: string;
  Crop_Practice_Type_Code?: string;
  Disc_Percent: number;
  Exception_Ind: string;
  Validation_Exception_Ind: number;
  Validation_ID: number;
  Exception_ID: number;
  Status: number;
}
export class RefCollateralMktDisc {
  Collateral_Category_Code: string;
  Collateral_Code?: string;
  Collateral_Type_Code?: string;
  Irr_Practice_Type_Code?: string;
  Crop_Practice_Type_Code?: string;
  Disc_Percent: number;
  Exception_Ind: string;
  Validation_Exception_Ind: number;
  Validation_ID: number;
  Exception_ID: number;
  Status: number;

  constructor(code: string) {
    this.Collateral_Category_Code = code;
    this.Disc_Percent = 100;
    this.Validation_Exception_Ind = 0;
    this.Exception_ID = 0;
    this.Validation_ID = 0;
  }
}

export class RefCollateralActionDiscAdj {
  Collateral_Action_Type_Code: string;
  Collateral_Action_Name?: string;
  Collateral_Mkt_Disc_Adj_Percent?: number;
  Collateral_Ins_Disc_Adj_Percent?: number;
  Exception_Ind: string;
  Validation_Exception_Ind: number;
  Validation_ID: number;
  Exception_ID: number;
  Status: number;

  constructor(code: string) {
    this.Collateral_Action_Type_Code = code;
    this.Collateral_Ins_Disc_Adj_Percent = 0;
    this.Collateral_Mkt_Disc_Adj_Percent = 0;
    this.Validation_ID = 0;
    this.Exception_ID = 0;
    this.Validation_Exception_Ind = 0;
  }
}

export class RefCollateralCategory {
  Collateral_Category_Code: string;
  Collateral_Category_Name: string;
  Sort_Order: number;
  Prior_Lien_Max_Disc_Ind: string;
  Protect_Insured_Ind: string;
  Default_Insured_Ind: string;
  Status: number;
}

export class RefCommitteeLevel {
  CM_Level_ID: number;
  Level_Code: number;
  Level_Name: string;
  Approval: number;
  Status: number;
}

export class RefTab {
  Tab_ID: number;
  Tab_Name: string;
  Tab_Code: string;
  Sort_Order: number;
  Status: number;
}

export class RefChevron {
  Chevron_ID: number;
  Chevron_Name: string;
  Chevron_Code: string;
  Chevron_Sort_Order: number;
  Standard_Chevron_Ind?: number;
  Tab_ID: number;
  Tab_Name: string;
  Tab_Code: string;
  Tab_Sort_Order: number;
  Status: number;
}

export class RefAssociation {
  Ref_Association_ID: number;
  ROQ_ID?: number;
  Assoc_Type_Code: string;
  Assoc_Name?: string;
  Contact?: string;
  Location?: string;
  Phone?: string;
  Email?: string;
  Referred_Type?: string;
  Response?: string;
  Preferred_Contact_Ind: number;
  Assoc_Status: number;
  IsOther?: boolean;
  Status: number;
  IsDelete: boolean;

  Validation_ID: number;
  Exception_ID: number;
}

export class RefDiscount {
  Discount_Id: number;
  Discount_Key: string;
  Discount_Value: number;
  Disc_Group_Code: string;
  Disc_Group: string;
  Disc_Detail: string;
}

export class RefCrop {
  Crop_ID: number;
  Crop_Code: string;
  Crop_Name: string;
  Crop_Price: number;
  Region_ID: number;
  Sort_Order: number;
  Status: number;
  Crop_Type: string;

  constructor() {
    this.Crop_Code = '';
    this.Crop_Price = 0;
    this.Region_ID = 0;
  }
}

export class RefBudgetExpenseType {
  Budget_Expense_Type_ID: number;
  Budget_Expense_Name: string;
  Variable_Ind?: string;
  Fixed_Only: number;
  Standard_Ind: number;
  Crop_Input_Ind?: string;
  Sort_order?: number;
  Documentation_Trigger_Ind?: string;
  Documentation_ID?: number;
  Financial_Budget_Code?: number;
  Validation_Exception_Ind: number;
  Validation_ID: number;
  Exception_ID: number;
  Status?: number;
}

export class RefBudgetDefault {
  Budget_Default_ID: number;
  Crop_Practice_Id: number;
  Crop_Full_Key?: string;
  Budget_Expense_Type_ID: number;
  Z_Practice_Type_Code?: string;
  Z_Region_ID?: any;
  Z_Office_ID?: any;
  ARM_Budget: number;
  Distributor_Budget: number;
  Third_Party_Budget: number;
  Status?: any;
}

export class RefOfficeList {
  Office_ID: number;
  Office_Abbrev: string;
  Office_Name: string;
  Address: string;
  City: string;
  State: string;
  Zip: string;
  Phone: string;
  Region_ID: number;
  Lat?: number;
  Long?: number;
  Status?: number;

  constructor() {
    this.Office_Name = '';
  }
}

export class RefRegionModel {
  Region_ID: number;
  Region_Name: string;
}

export class RefStateList {
  State_ID: number;
  State_Abbrev: string;
  State_Name: string;
}

export class RefCountyList {
  County_ID: number;
  County_Name: string;
  State_ID: number;
  State_Abbrev?: any;
  Contains_Rated_Land?: number;
  Status?: number;
}

export class LoanMaster {
  Loan_Master_ID: number;
  Loan_ID: number;
  Loan_Seq_num: number;
  Loan_Full_ID: string;
  Crop_Year: number;
  Prev_Yr_Loan_ID: number;

  Region_ID: number;
  Region_Name: string;

  Office_ID: number;
  Office_Name: string;

  Loan_Officer_ID: number;
  Loan_Officer_Name: string;
  User_FirstName: string;
  User_LastName: string;

  Farmer_ID: number;
  Borrower_ID: number;

  Loan_Type_Code: number;
  Loan_Type_Name: string;

  Application_Date: string;
  Original_Application_Date: string;

  Borrower_ID_Type: number;
  Borrower_SSN_Hash: string;
  Borrower_Entity_Type_Code: string;
  Borrower_Last_Name: string;
  Borrower_First_Name: string;
  Borrower_MI: string;
  Borrower_Address: string;
  Borrower_City: string;
  Borrower_State_Abbrev: string;
  Borrower_State: string;
  Borrower_Zip: number;
  Borrower_Phone: string;
  Borrower_Email: string;
  Borrower_DL_State: string;
  Borrower_DL_State_Abbrev: string;
  Borrower_DL_Num: string;
  Borrower_DOB: string;
  Nortridge_UploadDate: string;
  Borrower_Preferred_Contact_Ind: number;
  Borrower_County_ID: number;
  Borrower_County_Name: string;
  Borrower_Lat: number;
  Borrower_Long: number;
  Nortridge_Disburse_Text: string;
  Co_Borrower_Count: number;

  Spouse_SSN_Hash: string;
  Spouse_Last_name: string;
  Spouse_First_Name: string;
  Spouse_MI: string;
  Spouse_Address: string;
  Spouse_City: string;
  Spouse_State: string;
  Spouse_State_Abbrev: string;
  Spouse_Zip: number;
  Spouse_Phone: string;
  Spouse_Email: string;
  Spouse_Preferred_Contact_Ind: number;

  Farmer_ID_Type: number;
  Farmer_SSN_Hash: string;
  Farmer_Entity_Type: number;
  Farmer_Last_Name: string;
  Farmer_First_Name: string;
  Farmer_MI: string;
  Farmer_Address: string;
  Farmer_City: string;
  Farmer_State: string;
  Farmer_State_Abbrev: string;
  Farmer_Zip: number;
  Farmer_Phone: string;
  Farmer_Email: string;
  Farmer_DL_State: string;
  Farmer_DL_State_Abbrev: string;
  Farmer_DL_Num: string;
  Farmer_DOB: string;
  Farmer_Preferred_Contact_Ind: number;

  Year_Begin_Farming: number;
  Year_Begin_Client: number;

  Referral_Type_Code: number;

  Distributor_ID: number;
  Distributor_Name: string;

  Primary_AIP_ID: number;
  Primary_AIP_Name: string;

  Primary_Agency_ID: number;
  Primary_Agency_Name: string;

  Current_Bankruptcy_Status: number;
  Original_Bankruptcy_Status: number;
  Previous_Bankruptcy_Status: number;

  Judgement_Ind: number;
  Local_Ind: number;
  Controlled_Disbursement_Ind: number;
  Watch_List_Ind: number;
  Rate_Yield_Ref_Yield_Percent: number;

  Credit_Score: number;
  Credit_Score_Date: string;

  CPA_Prepared_Financials: number;
  Financials_Date: string;

  Total_Acres: number;

  Current_Assets: number;
  Inter_Assets: number;
  Fixed_Assets: number;
  Total_Assets: number;

  Current_Assets_Disc_Percent: number;
  Inter_Assets_Disc_Percent: number;
  Fixed_Assets_Disc_Percent: number;

  Current_Liabilities: number;
  Inter_Liabilities: number;
  Fixed_Liabilities: number;
  Total_Liabilities: number;

  Current_Net_Worth: number;
  Inter_Net_Worth: number;
  Fixed_Net_Worth: number;
  Total_Net_Worth: number;

  Current_Disc_Net_Worth: number;
  Inter_Disc_Net_Worth: number;
  Fixed_Disc_Net_Worth: number;
  Total_Disc_Net_Worth: number;

  Income_Detail_Ind: number;

  Borrower_Farm_Financial_Rating: number;
  Borrower_Rating: number;

  Requested_Credit: number;

  Maturity_Date: string;
  Original_Maturity_Date: string;

  Estimated_Days: number;

  Origination_Fee_Percent: number;
  Service_Fee_Percent: number;
  Extension_Fee_Percent: number;
  Interest_Percent: number;

  Origination_Fee_Amount: number;
  Service_Fee_Amount: number;
  Extension_Fee_Amount: number;
  Interest_Est_Amount: number;

  Delta_Origination_Fee_Amount: number;
  Delta_Service_Fee_Amount: number;
  Delta_Extension_Fee_Amount: number;
  Delta_Interest_Est_Amount: number;

  Total_Crop_Acres: number;
  Total_Revenue: number;
  Delta_Total_Revenue: number;

  ARM_Commitment: number;
  Dist_Commitment: number;
  Third_Party_Credit: number;
  Total_Commitment: number;

  Cash_Flow_Amount: number;
  Break_Even_Percent: number;
  Ag_Pro_Requested_Credit: number;
  Risk_Cushion_Amount: number;
  Risk_Cushion_Percent: number;
  Return_Percent: number;

  Delta_Crop_Acres: number;
  Delta_ARM_Commitment: number;
  Delta_Dist_Commitment: number;
  Delta_Third_Party_Credit: number;
  Delta_Total_Commitment: number;
  Delta_Total_Return_Fee: number;

  Delta_Cash_Flow_Amount: number;
  Delta_Cash_Flow_Percent: number;

  Delta_Risk_Cushion_Amount: number;
  Delta_Risk_Cushion_Percent: number;

  Delta_Return_Percent: number;
  Delta_Current_Addendum_Percent: number;

  ARM_Margin_Amount: number;
  ARM_Margin_Percent: number;
  Total_Margin_Percent: number;
  Total_margin_Amount: number;

  LTV_Percent: number;

  Net_Market_Value_Crops: number;
  Net_Market_Value_FSA: number;
  Net_Market_Value_Livestock: number;
  Net_Market_Value_Stored_Crops: number;
  Net_Market_Value_Equipment: number;
  Net_Market_Value_Real_Estate: number;
  Net_Market_Value_Other: number;
  Net_Market_Value_Total: number;

  Disc_value_Crops: number;
  Disc_value_FSA: number;
  Disc_value_Livestock: number;
  Disc_value_Stored_Crops: number;
  Disc_value_Equipment: number;
  Disc_value_Real_Estate: number;
  Disc_value_Other: number;
  Disc_value_Total: number;

  Ins_Value_Crops: number;
  Ins_Value_FSA: number;
  Ins_Value_Livestock: number;
  Ins_Value_Stored_Crops: number;
  Ins_Value_Equipment: number;
  Ins_Value_Real_Estate: number;
  Ins_Value_Other: number;
  Net_Market_Value_Insurance: number;
  Ins_Value_Total: number;

  Disc_Ins_Value_Crops: number;
  Disc_Ins_Value_FSA: number;
  Disc_Ins_Value_Livestock: number;
  Disc_Ins_Value_Stored_Crops: number;
  Disc_Ins_Value_Equipment: number;
  Disc_Ins_Value_Real_Estate: number;
  Disc_Ins_Value_Other: number;
  Disc_value_Insurance: number;
  Disc_Ins_Value_Total: number;

  CEI_Value: number;

  Prev_Addendum_Percent: number;
  Current_Addendum_Percent: number;

  Primary_Col: string;

  Third_Party_Total_Budget: number;
  Dist_Total_Budget: number;
  Arm_Total_Budget: number;
  Loan_Total_Budget: number;

  ARM_Extension_Fee: number;
  Dist_Rate_Fee: number;
  ARM_Rate_Fee: number;
  Total_ARM_Fees_And_Interest: number;
  Total_Return_Fee: number;

  Decision_Date: Date;
  Close_Date: Date | string;

  Aff_ID: number;
  Cross_Col_ID: number;

  Loan_Pending_Action_Type_Code: number;
  Loan_Pending_Action_Level: number;

  Loan_Status: LoanStatus;
  Prev_Loan_Status: string;

  Nortridge_API_In_ID: number;
  Outstanding_Balance?: number;

  Past_Due_Amount?: number;
  Last_Nortridge_Sync_Date?: string;

  Loan_Comment: string;
  Loan_Settings: string;

  IsDelete: boolean;
  Is_Latest: boolean;

  Loan_Tolerance_Level_Ind: number;
  Prev_Year_Loan_Full_ID?: string;

  Verf_AIP_Data?: any;
  Verf_FSA_Data?: any;

  // Verification
  Acres_Mapped: boolean;
  AIP_Verified: boolean;

  AIP_Acres_Verified: boolean;
  FSA_Acres_Verified: boolean;

  /**
   * Old Lenda Loan ID
   */
  Legacy_Loan_ID: number;

  /**
   * Old Lenda Value Adgustement - Farmula Difference Or Value Differences
   */
  Migrated_Loan_Adjustments: string;

  // Credit Agreement Fields

  /**
   * Credit Agreement Field 1
   */
  CAF1: number;

  /**
   * Credit Agreement Field 2
   */
  CAF2: number;
  Rev_Ins_Col: number;

  /**
   * Credit Agreement Field 3
   */
  CAF3: number;
  Non_Rev_Ins_Col: number;

  /**
   * Credit Agreement Field 4
   */
  CAF4: number;

  /**
   * Total Other Income Amount
   */
  Net_Other_Income: number;

  ActionStatus: number;
  FC_Current_Adjvalue: number;
  FC_Inter_Adjvalue: number;
  FC_Fixed_Adjvalue: number;
  FC_Total_AdjValue: number;

  FC_Total_Addt_Collateral_Value: number;
  Return_Amount: number;
  FC_Net_Market_Value_Insurance: number;
  Borrower_3yr_Tax_Returns: number;

  FC_FSA_Prior_Lien_Amount: number;
  FC_Market_Value_FSA: number;

  FC_Equip_Prior_Lien_Amount: number;
  FC_Market_Value_Equip: number;

  FC_Market_Value_lst: number;
  FC_Lst_Prior_Lien_Amount: number;
  FC_total_Qty_lst: number;
  FC_total_Price_lst: number;

  FC_Market_Value_other: number;
  FC_other_Prior_Lien_Amount: number;

  FC_Market_Value_realstate: number;
  FC_realstate_Prior_Lien_Amount: number;
  FC_total_Qty_Real_Estate: number;

  FC_Market_Value_storedcrop: number;
  FC_storedcrop_Prior_Lien_Amount: number;
  FC_total_Qty_storedcrop: number;
  FC_total_Price_storedcrop: number;
  FC_Total_Disc_Collateral_Value: number;
  FC_Ins_Disc_Collateral_Value: any;
  FC_Return_Scale: number;
  FC_Borrower_FICO: number;
  FC_Owned: boolean;
  Cash_Flow_Percent: number;
  FC_Disc_value_Insurance: number;
  Disc_CEI_Value_Acre: number;

  Last_Sync_Date: string;
  Active_User: number;
  Active_User_Name: string;

  Loan_Officer_Role: string;

  FC_Invalid_Origination: boolean;
  FC_Invalid_Service: boolean;
  FC_Invalid_Extension: boolean;
  FC_Invalid_ARM_Fee: boolean;
  FC_Invalid_Dist_Fee: boolean;
}

export class RefPendingAction {
  Ref_PA_ID: number;
  PA_Code: string;
  PA_Name: string;
  PA_Group: string;
  PA_Level: number;
  PA_Level_Name: string;
  Sort_Order: number;
  Icon_Code: string;
  Icon_Color_Code: string;
  Status: number;
}

export class RefContractType {
  Contract_Type_Code: string;
  Contract_Type_Name: string;
  Disc_Adj: string;
  Exception_Ind: string;
  Status: number;
}

export class RefHyperfieldValidation {
  Ref_Validation_ID: number;
  Table_Name: string;
  Field: string;
  Operator: string;
  Compare_Fn: string;
  Action_Ind: string;
  Tab_Id: number;
  Tab_Name: string;
  Chevron_Id: number;
  Chevron_Code: string;
  Status: number;
}

export class IUser {
  UserID: number;
  Username: string;
  Password_Hash?: any;
  User_Type_ID: number;
  FirstName: string;
  LastName: string;
  MI: string;
  Phone: string;
  Email: string;
  Address: string;
  City?: any;
  State: string;
  Zip: string;
  Job_Title_ID: number;
  Access_Failed_Count: number;
  Lockout_Enabled: Date;
  Lockout_End_Date: Date;
  Office_ID: number;
  Report_To_ID: number;
  Hire_Date: Date;
  Last_Committe_Assignment: Date;
  Vacation_Start: Date;
  Vacation_End: Date;
  Status: number;

  IsAdmin: number;
  SysAdmin: number;
  RiskAdmin: number;
  Processor: number;
}

export enum Pending_Action_Required {
  Level1AndLevel2 = 1,
  Level2Only = 2
}

export class Ref_Option_Adj_Lookup {
  Id: number;
  Option_Adj_Key: string;
  Option_Adj_Price: number;
  Option_Adj_Percent: number;
}

export class Ref_List_Item {
  List_Item_ID: number;
  List_Item_Code: string;
  List_Item_Name: string;
  List_Item_Value: string;
  List_Group_Code: string;
  Status: number;
}

export const LIST_GROUPS = {
  BORROWER_ENTITY_TYPE: 'BORROWER_ENTITY_TYPE',
  Assoc_Referral_Type: 'ASSOC_REFERRAL_TYPE',
  Preferred_Contacts: 'PFC',
  Entity_Type_ID: 'ID',
  Lienholder_Documents: 'LEI',
  Response: 'RSP',
  Scale: 'SCALE'
};

export class Ref_Admin_Default {
  Admin_Default_ID: number;
  Admin_Default_Code: string;
  Admin_Default_Name: string;
  Default_Text: string;
  Admin_Ind: string;
  Status: number;
}

export class Ref_Association_Type {
  Ref_Assoc_Type_ID: number;
  Assoc_Type_Code: string;
  Assoc_Type_Name: string;
  Contact_Req_Ind: number;
  Location_Req_Ind: number;
  Email_Req_Ind: number;
  Preffered_Contact_Req_Ind: number;
  Value_Req_Ind: number;
  Status: number;
}

export class Ref_Borrower_Rating {
  Borrower_Rating_ID: number;
  Borrower_Rating_Code: string;
  Borrower_Rating_Name: string;
  Value_1: string;
  Value_2: string;
  Value_3: string;
  Value_4: string;
  Value_5: string;
  Constant_Ind: number;
  Status: number;
}

export class Ref_Comment_Emoji {
  Comment_Emoji_ID: number;
  Comment_Emoji_Text: string;
  Emoji_Reference: string;
  Vote_Ind: number;
  Status: number;
}

export class Ref_Comment_Type {
  Comment_Type_ID: number;
  Comment_Type_Code: string;
  Comment_Type_Name: string;
  Notification_Ind: number;
  Read_Required_Ind: number;
  Pending_Action_Code: string;
  Status: number;
}

export class Ref_Farm_Financial_Rating {
  Farm_Financial_Rating_ID: number;
  Farm_Financial_Code: string;
  Farm_Financial_Name: string;
  Owned_Strong: number;
  Owned_Stable: number;
  Leased_Strong: number;
  Leased_Stable: number;
  Weight: number;
  Status: number;
}

export class Ref_Field_Id_Mapping {
  Field_ID: number;
  Field_Name: string;
  Table_Name: string;
  Status: number;
}

export class Ref_Ins_Policy_Plan {
  Ref_Ins_Plan_ID: number;
  Ins_Plan: string;
  Ins_Plan_Name: string;
  Federal_Ind: number;
  Unique_AIP_Ind: number;
  Unique_AIP_ID: number;
  Status: number;
}

export class Ref_Ins_Eligibility_Rule {
  Ins_Eligibility_Rule_ID: number;
  Ins_Plan: string;
  Ins_Plan_Type: string;
  Eligible_Crop_Codes: Array<string>;
  Eligible_States: Array<string>;
  Ineligible_Crop_Practices: Array<string>;
  Status: number;

  constructor() {
    this.Eligible_Crop_Codes = [];
    this.Eligible_States = [];
    this.Ineligible_Crop_Practices = [];
  }
}

export class Ref_Ins_Plan_Actuarial {
  Ins_Plan_Actuarial_ID: number;
  State_ID: string;
  County_ID: string;
  Hail_Actuarial_Percent: number;
  Hail_Wind_Acturial_Percent: number;
  Status: number;
}

export class Ref_Ins_Validation_Rule {
  Ins_Validation_Rule_ID: number;
  Ins_Plan: string;
  Ins_Plan_Type: string;
  Unit: string;
  Option_Type: string;
  Upper_Pct: string;
  Lower_Pct: string;
  Range_Max: number;
  Area_Yield: number;
  Yield_Pct: string;
  Price_Pct: string;
  Liability_Max_Upper_Pct_75: string;
  Liability_Max_Upper_Pct_85: string;
  Deduct_Percent_Max: string;
  Deduct_Unit_CRN: number;
  Deduct_Unit_SOY: number;
  Deduct_Unit_RIC: number;
  Deduct_Unit_WHT: number;
  Late_Deduct_Unit: number;
  FCMC: number;
  AIP: string;
  Status: number;

  constructor() {
    this.Unit = '';
    this.Option_Type = '';
    this.Upper_Pct = '';
    this.Lower_Pct = '';
    this.Range_Max = 0;
    this.Area_Yield = 0;
    this.Yield_Pct = '';
    this.Price_Pct = '';
    this.Liability_Max_Upper_Pct_75 = '';
    this.Liability_Max_Upper_Pct_85 = '';
    this.Deduct_Percent_Max = '';
    this.Deduct_Unit_CRN = 0;
    this.Deduct_Unit_RIC = 0;
    this.Deduct_Unit_SOY = 0;
    this.Deduct_Unit_WHT = 0;
    this.Late_Deduct_Unit = 0;
    this.FCMC = 0;
    this.AIP = '';
  }
}

export class Ref_Loan_Type {
  Loan_Type_ID: number;
  Loan_Type_Code: number;
  Loan_Type_Name: string;
  Origination_Fee_Percent: number;
  Service_Fee_Percent: number;
  Extension_Fee_Percent: number;
  Rate_Percent: number;
  Status: number;

  constructor() {
    this.Origination_Fee_Percent = 1.5;
    this.Service_Fee_Percent = 1.0;
    this.Extension_Fee_Percent = 0;
    this.Rate_Percent = 9.0;
  }
}

export class Ref_User_Type_Access {
  User_Type_Access_ID: number;
  User_Type_Code: string;
  User_Type_Name: string;
  System_Area: string;
  Access_Level: string;
  Status: number;
}

export class Migrated_Loan_Adjustments {
  ARM_Commitment: number;
  Dist_Commitment: number;
  Net_Market_Value_Crops: number;
  Disc_value_Crops: number;
  Ins_Value_Crops: number;
  Disc_Ins_Value_Crops: number;
  Cash_Flow_Amount: number;
  Risk_Cushion_Amount: number;
  Origination_Fee_Amount: number;
  Service_Fee_Amount: number;
  ARM_Rate_Fee: number;
  Dist_Rate_Fee: number;
  Cash_Rent_Budget: number;
}

export enum MigratedLoanAdjustmentKeys {
  ARM_Commitment = 'ARM_Commitment',
  Dist_Commitment = 'Dist_Commitment',
  Net_Market_Value_Crops = 'Net_Market_Value_Crops',
  Disc_value_Crops = 'Disc_value_Crops',
  Ins_Value_Crops = 'Ins_Value_Crops',
  Disc_Ins_Value_Crops = 'Disc_Ins_Value_Crops',
  Cash_Flow_Amount = 'Cash_Flow_Amount',
  Risk_Cushion_Amount = 'Risk_Cushion_Amount',
  Origination_Fee_Amount = 'Origination_Fee_Amount',
  Service_Fee_Amount = 'Service_Fee_Amount',
  ARM_Rate_Fee = 'ARM_Rate_Fee',
  Dist_Rate_Fee = 'Dist_Rate_Fee',
  Cash_Rent_Budget = 'Cash_Rent_Budget'
}
