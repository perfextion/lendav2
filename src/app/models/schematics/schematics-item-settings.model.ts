import { SchematicStatus } from "@lenda/models/schematics/base-entities/schematic-status.enum";
import { CriticalItem } from "@lenda/models/schematics/base-entities/critical-item.model";

export class SchematicsItemSettings {
  iconStatus: SchematicStatus = SchematicStatus.info;
  isStepCompleted: boolean = false;
  warnings: CriticalItem[];
  errors: CriticalItem[];
  infos: CriticalItem[];
  details: string[];

  hoverInfos: CriticalItem[] = [];
  /**
   *
   * @param iconStatus Status for the object - success, error, etc.
   * @param isStepCompleted is the step completed successfully
   * @param infos array of informations
   * @param warnings array of warnings
   * @param errors array of errors
   */
  constructor(
    iconStatus: SchematicStatus = SchematicStatus.info,
    isStepCompleted: boolean = false,
    infos: CriticalItem[] = [], // green info bubbles
    warnings: CriticalItem[] = [], // orange exceptions
    errors: CriticalItem[] = [], // red exceptions
    details = []) {
    this.iconStatus = iconStatus;
    this.isStepCompleted = isStepCompleted;
    this.infos = infos;
    this.warnings = warnings;
    this.errors = errors;
    this.details = details;
  }
}
