import { SchematicsItemSettings } from "@lenda/models/schematics/schematics-item-settings.model";
import { SchematicStatus } from "@lenda/models/schematics/base-entities/schematic-status.enum";
import { CriticalItem } from "@lenda/models/schematics/base-entities/critical-item.model";

export class SchematicsSettings {
  // Schematic items
  crossCollateral = new SchematicsItemSettings(SchematicStatus.success, true);
  farmer: SchematicsItemSettings =  new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  borrower: SchematicsItemSettings =  new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  spouse: SchematicsItemSettings = new SchematicsItemSettings (
    SchematicStatus.success
  );
  crop: SchematicsItemSettings =  new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  farm: SchematicsItemSettings =  new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  budget: SchematicsItemSettings =  new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  irr: SchematicsItemSettings =  new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  ni: SchematicsItemSettings =  new SchematicsItemSettings(
    SchematicStatus.success, false
  );
  practicesIrr: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.info, true
  );
  practicesNi: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.info, false
  );
  insurance: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  optimizer: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  collateral: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  riskReturn: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  exceptions: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  mitigations: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, false
  );
  underwriting: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  summary: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  committee: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.info, true
  );
  decide: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.warning, false
  );
  conditionalApproval: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, false
  );
  prerequisiteConditions: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, false
  );
  checkout: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, false
  );
  closing: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, false
  );
  loanInventory: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, false
  );
  insuranceVerification: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.error, false
  );
  reconciliation: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, false
  );
  apply: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, true
  );
  nortridge: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, false
  );
  lostats: SchematicsItemSettings = new SchematicsItemSettings(
    SchematicStatus.success, false
  );
}

