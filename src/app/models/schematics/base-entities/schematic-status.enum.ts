export enum SchematicStatus {
  disabled = 'disabled',
  success = 'success',
  error = 'error',
  warning = 'warning',
  info = 'info'
}
