import { SchematicStatus } from "./schematic-status.enum";

export class CriticalItem {
  infoText: string;

  // Added for Committee
  isActionRequired: CriticalItemAction;

  // Added for Validation and Exception
  level: CriticalItemLevel = CriticalItemLevel.Level2;

  constructor(info: string, isActionRequired = CriticalItemAction.Required) {
    this.infoText = info;
    this.isActionRequired = isActionRequired;
  }
}

/**
 *@title Action Status
 */
export enum CriticalItemAction {
  Required = 0,
  Success = 1,
  Rejected = 2
}


/**
 *Critical Item Level
 */
export enum CriticalItemLevel {
  /**
   * Validation
   */
  Level1,

  /**
   * Exception
   */
  Level2
}

/**
 * Critical Info Model
 */
export class CriticalInfo {
  constructor(
    public level: CriticalItemLevel,
    public type: SchematicStatus,
    public items: Array<CriticalItem> = [],
    public source: string
  ) {}
}
