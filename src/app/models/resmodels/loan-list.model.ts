export class LoanListResModel {
  public Id: number;
  public ApplicantName: string;
  public FarmerName: string;
  public LoanTypeName: string;
  public CropYear: string;
  public ApplicationDate: string;
}

export class UpdateWatchListIndModel {
  LoanFullId: string;
  WatchListInd: number;
  Comment: string;
  System_Comment: string;
}
