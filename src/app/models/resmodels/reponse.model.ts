export class ResponseModel<T = any> {
  Data: T;
  Message: string;
  ResCode: number;
}
