import { JsonProperty } from 'json2typescript';
import {
  IntConverter,
  StringConverter,
  ArrayConverter
} from '../Workers/utility/jsonconvertors';

export enum ExceptionOperation {
  Greater = '>',
  Equal = '=',
  GreaterEqual = '>=',
  Lesser = '<',
  LesserEqual = '<='
}

export enum Chevron {
  Borrower = 1,
  Crop = 2,
  Farm = 3,
  Insurance = 4,
  Budget = 5,
  Optimizer = 6,
  Collateral = 7,
  Committee = 8,
  Insurance_MPCI = 9,
  Insurance_HMAX = 10,
  Insurance_RAMP = 11,
  Insurance_ICE = 12,
  Insurance_ABC = 13,
  Insurance_CROPHAIL = 14,
  Insurance_WFRP = 15,
  Credit_Score = 56,
  Balance_Sheet = 52,
  Terms = 41,
  Borrower_Rating = 64
}

export enum Exception_ID {
  Credit_Score = 6,
}

export class LoanQResponse {
  @JsonProperty('Loan_Q_response_ID', IntConverter, false)
  Loan_Q_response_ID: number;

  @JsonProperty('Loan_ID', IntConverter, false)
  Loan_ID: number;

  @JsonProperty('Loan_Seq_Num', IntConverter, false)
  Loan_Seq_Num: number;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string;

  @JsonProperty('Chevron_ID', IntConverter, false)
  Chevron_ID: number;

  @JsonProperty('Question_ID', IntConverter, false)
  Question_ID: number;

  @JsonProperty('Question_Category_Code', IntConverter, false)
  Question_Category_Code: number;

  @JsonProperty('Response_Detail', StringConverter, false)
  Response_Detail: string;

  @JsonProperty('Response_Detail_Field_ID', IntConverter, false)
  Response_Detail_Field_ID: number;

  @JsonProperty('Status', IntConverter, false)
  Status: number;

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number;

  // FC_Question_ID_Text : string;
  // FC_Choice1 :string;
  // FC_Choice2 : string;
  // FC_Subsidiary_Question_ID_Ind : number;
  // FC_Parent_Question_ID : number;
  // FC_Sort_Order : number;
  // FC_Exception_Msg: string;
  // FC_Operation: Operation;
  // FC_Level1_Val: string;
  // FC_Level2_Val: string;
}

export class RefQuestions {
  @JsonProperty('Question_ID', IntConverter, false)
  Question_ID: number;

  @JsonProperty('Question_ID_Text', StringConverter, false)
  Question_ID_Text: string;

  @JsonProperty('Chevron_ID', IntConverter, false)
  Chevron_ID: number;

  @JsonProperty('Sort_Order', IntConverter, false)
  Sort_Order: number;

  @JsonProperty('Questions_Cat_Code', IntConverter, false)
  Questions_Cat_Code: number;

  @JsonProperty('Response_Detail_Reqd_Ind', IntConverter, false)
  Response_Detail_Reqd_Ind: number;

  @JsonProperty('Subsidiary_Question_ID_Ind', IntConverter, false)
  Subsidiary_Question_ID_Ind: number;

  @JsonProperty('Parent_Question_ID', IntConverter, false)
  Parent_Question_ID: number;

  @JsonProperty('Status', IntConverter, false)
  Status: number;

  @JsonProperty('Choice1', StringConverter, false)
  Choice1: string;

  @JsonProperty('Choice2', StringConverter, false)
  Choice2: string;

  @JsonProperty('Exception_ID_Text', StringConverter, false)
  Exception_ID_Text: string;

  @JsonProperty('Exception_Sort_Order', IntConverter, false)
  Exception_Sort_Order: number;

  @JsonProperty('Is_Justification_Required', IntConverter, false)
  Is_Justification_Required: number;

  @JsonProperty('Tab_ID', IntConverter, false)
  Tab_ID: number;

  @JsonProperty('ExceptionLevels', ArrayConverter, true)
  ExceptionLevels: Array<ExceptionLevel> = [];

  @JsonProperty('Validation_Exception_Ind', IntConverter, false)
  Validation_Exception_Ind: number;

  @JsonProperty('Validation_ID', IntConverter, false)
  Validation_ID: number;

  @JsonProperty('Exception_ID', IntConverter, false)
  Exception_ID: number;

  FC_Response_Detail: string;
  isQuestionVisible: boolean;
  Subsidiary_Q_Visible: boolean;
}

export class RefHyperField {
  @JsonProperty('HyperField_ID', IntConverter, false)
  HyperField_ID: number;

  @JsonProperty('HyperField_TableName', StringConverter, false)
  HyperField_TableName: string;

  @JsonProperty('HyperField_Name', StringConverter, false)
  HyperField_Name: string;

  @JsonProperty('HyperField_Type', IntConverter, false)
  HyperField_Type: number;

  @JsonProperty('Array_Item_ID_Exp', StringConverter, false)
  Array_Item_ID_Exp: string;

  @JsonProperty('HyperField_Expression', StringConverter, false)
  HyperField_Expression: string;

  @JsonProperty('Chevron_ID', IntConverter, false)
  Chevron_ID: number;

  @JsonProperty('Sort_Order', IntConverter, false)
  Sort_Order: number;

  @JsonProperty('Status', IntConverter, false)
  Status: number;

  @JsonProperty('Exception_ID', IntConverter, false)
  Exception_ID: number;

  @JsonProperty('Exception_ID_Text', StringConverter, false)
  Exception_ID_Text: string;

  @JsonProperty('Exception_Sort_Order', IntConverter, false)
  Exception_Sort_Order: number;

  @JsonProperty('Is_Justification_Required', IntConverter, false)
  Is_Justification_Required: number;

  @JsonProperty('Tab_ID', IntConverter, false)
  Tab_ID: number;

  @JsonProperty('ExceptionLevels', ArrayConverter, true)
  ExceptionLevels: Array<ExceptionLevel> = [];
}

export class ExceptionLevel {
  @JsonProperty('Exception_Level_ID', IntConverter, false)
  Exception_Level_ID: number;

  @JsonProperty('Exception_ID_Level', IntConverter, false)
  Exception_ID_Level: number;

  // @JsonProperty("Threshold_Operative", StringConverter,false)
  Threshold_Operative: ExceptionOperation;

  @JsonProperty('Threshold_Value', StringConverter, false)
  Threshold_Value: string;

  @JsonProperty('Threshold_Hyper_Code', StringConverter, false)
  Threshold_Hyper_Code: string;
}

export class RefCondition {
  Condition_ID: number;
  Condition_Name: string;
  Condition_Level: number;
  Sort_Order: number;
  Standard_Condition_Ind: number;
  Document_ID: number;
  Association_Type?: any;
  Status: number;
  Multiple: number;
}
