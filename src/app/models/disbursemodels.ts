    export class Loan_Disburse
    {
        constructor(){
            this.RentDetails=[];
            this.History=[];
            this.Details=[];
        }
        Loan_Disburse_ID: any;
        Loan_ID: number;
        Total_Disburse_Amount: number | null;
        Special_ACH_Ind: boolean | null;
        Special_CRP_Ind: boolean | null;
        Special_Comment: string;
        Special_Harvest_Comment:string;
        Special_COC_Ind: boolean | null;
        Status: number | null;
        Details: Loan_Disburse_Detail[];
        History:Loan_Disburse_Approval_Hist[];
        RentDetails:Loan_Disburse_Rent_Detail[]
        ActionStatus:number;
        FC_PopupStatus:number=0;
    }

    export class Loan_Disburse_Detail
    {
        Loan_Disburse_Detail_ID: number;
        Loan_ID: number;
        Loan_Disburse_ID: number | null;
        Budget_Expense_ID: number | null;
        Disburse_Detail_Commit_Amount: number | null;
        Disburse_Detail_Used_Amount: number | null;
        Disburse_Detail_Requested_Amount: number | null;
        Status: number | null;
        ActionStatus:number;
    }

    export class Loan_Disburse_Approval_Hist
    {
        Loan_Disburse_Approval_Hist_ID: number;
        Loan_Disburse_ID: number;
        Loan_ID: number;
        User_ID: number | null;
        Username:string;
        Approval_Status: number | null;
        Action_Code :number|null;
        Approval_Date_Time: Date | string | null;
        User_Role: string;
        Comments: string;
    }

export class Loan_Disburse_Rent_Detail {
    Loan_Disburse_Rent_Detail_ID: number;
    Loan_ID: number;
    Loan_Disburse_ID?: number;
    Landowner_ID: string;
    Farm_ID: string;
    Cash_Rent_Amount?: number;
    status?: number;
    Reimbusement:boolean;
    ActionStatus:number;
}