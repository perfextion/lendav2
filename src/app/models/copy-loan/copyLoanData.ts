import { JsonProperty, JsonObject, JsonConverter, JsonCustomConvert } from "json2typescript";
import { copy_loan_input } from './copyLoanInput';

@JsonObject
export class copy_loan_data{

     loanInputs : copy_loan_input;
     //loanActions : CopyLoanAction;

}