import { JsonObject } from 'json2typescript';
import { copy_loan_data } from './copyLoanData';

export enum CopyLoanTabs {
  borrowers = 'borrowers',
  crops = 'crops',
  farms = 'farms',
  insurances = 'insurances',
  budgets = 'budgets',
  optimizers = 'optimizers',
  collaterals = 'collaterals',
  committees = 'committees'
}

export enum CopyLoanBorrowerTypes {
  ReferredFrom = 'ReferredFrom',
  CreditReference = 'CreditReference',
  Questions = 'Questions',
  Borrower = 'Borrower',
  Farmer = 'Farmer',
  CreditScore = 'CreditScore',
  BalanceSheet = 'BalanceSheet',
  IncomeHistory = 'IncomeHistory',
  CoBorrower = 'CoBorrower'
}

export enum CopyLoanCropTypes {
  Buyer = 'Buyer',
  Rebator = 'Rebator',
  Questions = 'Questions',
  Yield = 'Yield',
  Price = 'Price',
  OtherIncome = 'OtherIncome',
  Contracts = 'Contracts'
}

export enum CopyLoanFarmTypes {
  Landlord = 'Landlord',
  Questions = 'Questions',
  Farm = 'Farm'
}

export enum CopyLoanInsuranceTypes {
  Agent = 'Agent',
  Agency = 'Agency',
  AIP = 'AIP',
  Questions = 'Questions',
  Policy = 'Policy',
  APH = 'APH'
}

export enum CopyLoanBudgetTypes {
  Distributer = 'Distributer',
  ThirdParty = 'ThirdParty',
  Harvester = 'Harvester',
  EquipmentProvider = 'EquipmentProvider',
  Questions = 'Questions',
  CropBudgets = 'CropBudgets',
  CreditNotes = 'CreditNotes'
}

export enum CopyLoanOptimizerTypes {
  IrrAcres = 'IrrAcres',
  NIAcres = 'NIAcres'
}

export enum CopyLoanCollateralTypes {
  Lienholder = 'Lienholder',
  Buyer = 'Buyer',
  Guarantor = 'Guarantor',
  Questions = 'Questions',
  AdditionalCollateral = 'AdditionalCollateral'
}

export enum CopyLoanCommitteeTypes {
  Terms = 'Terms',
  LoanComment = 'LoanComment'
}

@JsonObject
export class copy_loan_model {
  FromLoanFullID: string;
  ToLoanFullID: string;
  IsNew: boolean = false;
  IsFull: boolean;

  Data: copy_loan_data;
}

export enum LoanStatusChange {
  NewLoan,
  WorkingChange,
  Withdrawn,
  Recommended,
  Approved,
  Declined,
  Uploaded,
  Paid,
  WrittenOff,
  ReviveOrCopy,
  Rescind
}

export class ChangeStatusModel {
  FromLoanFullID: string;
  ToStatus: LoanStatusChange;
}
