import { JsonObject } from 'json2typescript';

@JsonObject
export class copy_loan_input {
  borrowers: loan_input_borrowers = new loan_input_borrowers();
  crops: loan_input_crops = new loan_input_crops();
  farms: loan_input_farms = new loan_input_farms();
  insurances: loan_input_insurances = new loan_input_insurances();
  budgets: loan_input_budgets = new loan_input_budgets();
  optimizers: loan_input_optimizers = new loan_input_optimizers();
  collaterals: loan_input_collaterals = new loan_input_collaterals();
  committees: loan_input_committees = new loan_input_committees();
}

@JsonObject
export class loan_input_borrowers {
  Borrower: boolean = false;
  ReferredFrom: boolean = false;
  CreditReference: boolean = false;
  Questions: boolean = false;
  BorrowerName: boolean = false;
  FarmerName: boolean = false;
  CreditScore: boolean = false;
  BalanceSheet: boolean = false;
  IncomeHistory: boolean = false;
}

@JsonObject
export class loan_input_crops {
  Buyer: boolean = false;
  Rebator: boolean = false;
  Questions: boolean = false;
  Yield: boolean = false;
  OtherIncome: boolean = false;
  Contracts: boolean = false;
}

@JsonObject
export class loan_input_farms {
  Landlord: boolean = false;
  Questions: boolean = false;
  Farm: boolean = false;
}

@JsonObject
export class loan_input_insurances {
  Agent: boolean = false;
  Agency: boolean = false;
  AIP: boolean = false;
  Questions: boolean = false;
  Policy: boolean = false;
  APH: boolean = false;
}

@JsonObject
export class loan_input_budgets {
  Distributer: boolean = false;
  ThirdParty: boolean = false;
  Harvester: boolean = false;
  EquipmentProvider: boolean = false;
  Questions: boolean = false;
  CropBudgets: boolean = false;
  CreditNotes: boolean = false;
}

@JsonObject
export class loan_input_optimizers {
  IrrAcres: boolean = false;
  NIAcres: boolean = false;
}

@JsonObject
export class loan_input_collaterals {
  Lienholder: boolean = false;
  Buyer: boolean = false;
  Guarantor: boolean = false;
  Questions: boolean = false;
  AdditionalCollateral: boolean = false;
}

@JsonObject
export class loan_input_committees {
  Terms: boolean = false;
  LoanComment: boolean = false;
}
