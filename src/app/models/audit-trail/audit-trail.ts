export class LoanAuditTrail {
  Loan_Audit_Trail_ID: number;
  Loan_Full_ID: string;
  Loan_ID: string;
  Loan_Seq_Num: string;
  User_ID: number;
  Date_Time: Date;
  Loan_Status: string;
  View_Update_Ind: number;
  Audit_Trail_Type: string;
  Audit_Field_ID: string;
  Audit_Trail_Text: string;
}

export class Loan_Audit_Trail_Collection {
  Loan_ID: string;
  Loan_Full_ID: string;
  Loan_Seq_Num: string;
  User_ID: number;
  Date_Time: Date;
  Loan_Status: string;
  Audit_Trails: Array<Audit_Trail_Item>;
}

export class Audit_Trail_Item {
  View_Update_Ind?: number;
  Audit_Field_ID?: string;
  Audit_Trail_Text: string;
  Audit_Trail_Type: string;
}

export type Audit_Item = { id: string; text: string, type?: string };
