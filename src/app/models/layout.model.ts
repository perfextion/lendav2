export class LayoutModel {
    isFilter: boolean;
    id: number;
    commentReadType: number;

    // added property to sort by watch_list_ind
    watchListInd: number;
  }
