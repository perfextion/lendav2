import { JsonProperty, JsonObject } from 'json2typescript';
import {
  Loan_Crop_Unit,
  Loan_Crop_Unit_FC,
  APH_Units,
  Crop_Unit_Policy
} from './cropmodel';
import {
  IntConverter,
  StringConverter,
  ArrayConverter
} from '../Workers/utility/jsonconvertors';
import { Loan_Farm } from './farmmodel.';
import { LoanQResponse } from './loan-response.model';
import { ModelStatus, Sync_Status } from './syncstatusmodel';
import { Insurance_Policy, Loan_Policy } from './insurancemodel';
import { Loan_Disburse } from './disbursemodels';
import { Loan_Exception } from './loan/loan_exceptions';
import { Loan_Validation } from './loan/loan_validation';
import { Loan_Document, Loan_Document_Detail } from './loan/loan_document';
import { LoanMaster } from './ref-data-model';

export enum AssociationTypeCode {
  Agency = 'AGY',
  AIP = 'AIP',
  Buyer = 'BUY',
  Distributor = 'DIS',
  Harvester = 'HAR',
  Rebator = 'REB',
  ThirdParty = 'THR',
  LienHolder = 'LEI',
  Guarantor = 'GUA',
  ReferredFrom = 'REF',
  Landlord = 'LLD',
  CreditRererrence = 'CRF',
  SBI = 'SBI',
  Borrower = 'BOR',
  EquipmentProvider = 'EQP'
}

export const AssociationCodeNameMapping = {
  AGY: 'Agency',
  AIP: 'AIP',
  BUY: 'Buyer',
  DIS: 'Distributor',
  HAR: 'Harvester',
  REB: 'Rebator',
  THR: 'Third Party',
  LEI: 'Lienholder',
  GUA: 'Guarantor',
  REF: 'Referred From',
  LLD: 'Landlord',
  CRF: 'Credit Reference',
  BOR: 'Borrower',
  SBI: 'SBI',
  EQP: 'Equipemt Provider'
};

export enum Collateral_Category_Code {
  FSA = 'FSA',
  Livestock = 'LSK',
  StoredCrop = 'STO',
  Equipemt = 'EQP',
  RealEState = 'REA',
  Other = 'OTH'
}

export enum CollateralCategoryMapping {
  FSA = 'FSA',
  LSK = 'Livestock',
  STO = 'Stored Crop',
  EQP = 'Equipment',
  REA = 'Real EState',
  OTH = 'Other'
}

export enum CropPracticeCode {
  NIR = 'NI',
  IRR = 'IRR'
}

export enum ExceptionSource {
  Question = 1,
  HyperField = 2
}

export enum BorrowerEntityType {
  Individual = 'IND',
  IndividualWithSpouse = 'INS',
  Partner = 'PRP',
  Joint = 'JNT',
  Corporation = 'CRP',
  LLC = 'LLC',
  Trust = 'TRS',
  Limited_Partnership = 'LPA',
  Limited_Liability_Partnership = 'LLP'
}

export enum LoanStatus {
  Working = 'W',
  Recommended = 'R',
  Approved = 'A',
  Uploaded = 'U',
  Void = 'V',
  Withdrawn = 'Y',
  Declined = 'Z',
  Historical = 'H',
  PaidOff = 'P',
  WrittenOff = 'X'
}

export enum LoanStatusMapping {
  W = 'Working',
  R = 'Recommended',
  A = 'Approved',
  U = 'Uploaded',
  V = 'Void',
  Y = 'Withdrawn',
  Z = 'Declined',
  H = 'Historical',
  P = 'PaidOff',
  X = 'Written Off'
}

@JsonObject
export class borrower_model {
  @JsonProperty('CoBorrower_ID', IntConverter, false)
  CoBorrower_ID: number = undefined;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string = '';

  @JsonProperty('Borrower_ID_Type', IntConverter, false)
  Borrower_ID_Type: number = undefined;

  @JsonProperty('Borrower_SSN_Hash', StringConverter, false)
  Borrower_SSN_Hash: string = '';

  @JsonProperty('Borrower_Entity_Type_Code', StringConverter, false)
  Borrower_Entity_Type_Code: string = '';

  @JsonProperty('Borrower_Last_Name', StringConverter, false)
  Borrower_Last_Name: string = '';

  @JsonProperty('Borrower_First_Name', StringConverter, false)
  Borrower_First_Name: string = '';

  @JsonProperty('Borrower_MI', StringConverter, false)
  Borrower_MI: string = '';

  @JsonProperty('Borrower_Address', StringConverter, false)
  Borrower_Address: string = '';

  @JsonProperty('Borrower_City', StringConverter, false)
  Borrower_City: string = '';

  @JsonProperty('Borrower_State_ID', StringConverter, false)
  Borrower_State_ID: string = '';

  @JsonProperty('Borrower_Zip', IntConverter, false)
  Borrower_Zip: number = undefined;

  @JsonProperty('Borrower_Phone', StringConverter, false)
  Borrower_Phone: string = '';

  @JsonProperty('Borrower_email', StringConverter, false)
  Borrower_email: string = '';

  @JsonProperty('Borrower_DL_state', StringConverter, false)
  Borrower_DL_state: string = '';

  @JsonProperty('Borrower_DL_Num', StringConverter, false)
  Borrower_Dl_Num: string = '';

  @JsonProperty('Borrower_DOB', StringConverter, false)
  Borrower_DOB: string = '';

  @JsonProperty('Borrower_Preferred_Contact_Ind', IntConverter, false)
  Borrower_Preferred_Contact_Ind: number = undefined;

  @JsonProperty('Borrower_County_ID', IntConverter, false)
  Borrower_County_ID: number = undefined;

  @JsonProperty('Borrower_Lat', IntConverter, false)
  Borrower_Lat: number = undefined;

  @JsonProperty('Borrower_Long', IntConverter, false)
  Borrower_Long: number = undefined;

  @JsonProperty('Spouse_SSN_Hash', StringConverter, false)
  Spouse_SSN_Hash: string = '';

  @JsonProperty('Spouse_Last_name', StringConverter, false)
  Spouse_Last_name: string = '';

  @JsonProperty('Spouse_First_Name', StringConverter, false)
  Spouse_First_Name: string = '';

  @JsonProperty('Spouse_MI', StringConverter, false)
  Spouse_MI: string = '';

  @JsonProperty('Spouse_Address', StringConverter, false)
  Spouse_Address: string = '';

  @JsonProperty('Spouse_City', StringConverter, false)
  Spouse_City: string = '';

  @JsonProperty('Spouse_State', StringConverter, false)
  Spouse_State: string = '';

  @JsonProperty('Spouse_Zip', IntConverter, false)
  Spouse_Zip: number = undefined;

  @JsonProperty('Spouse_Phone', StringConverter, false)
  Spouse_Phone: string = '';

  @JsonProperty('Spouse_Email', StringConverter, false)
  Spouse_Email: string = '';

  @JsonProperty('Spouse_Preffered_Contact_Ind', IntConverter, false)
  Spouse_Preffered_Contact_Ind: number = undefined;

  @JsonProperty('IsDelete', IntConverter, false)
  IsDelete: number = undefined;

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number = undefined;

  @JsonProperty('Borrower_3yr_Tax_Returns', IntConverter, false)
  Borrower_3yr_Tax_Returns: number = undefined;

  //extra to handle to existing code: should be removed
  Borrower_CPA_Financials: string;
  CPA_Prepared_Financials: boolean;
  FC_Borrower_FICO: number;

  // create loan new
  Master_Borrower_ID: number;
}

@JsonObject
export class Loan_Association {
  @JsonProperty('Assoc_ID', IntConverter, false)
  Assoc_ID: number = 0;

  @JsonProperty('Loan_ID', IntConverter, false)
  Loan_ID: number = 0;

  @JsonProperty('Loan_Seq_Num', IntConverter, false)
  Loan_Seq_Num: number = 0;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string = '';

  @JsonProperty('Ref_Assoc_ID', IntConverter, false)
  Ref_Assoc_ID: number = 0;

  @JsonProperty('Assoc_Type_Code', StringConverter, false)
  Assoc_Type_Code: AssociationTypeCode;

  @JsonProperty('Assoc_Name', StringConverter, false)
  Assoc_Name: string = '';

  @JsonProperty('Principal_Ind', IntConverter, false)
  Principal_Ind: number = 0;

  @JsonProperty('Contact', StringConverter, false)
  Contact: string = '';

  @JsonProperty('Location', StringConverter, false)
  Location: string = '';

  @JsonProperty('Phone', StringConverter, false)
  Phone: string = '';

  @JsonProperty('Email', StringConverter, false)
  Email: string = '';

  @JsonProperty('Amount', IntConverter, false)
  Amount: number = 0;

  @JsonProperty('Referred_Type', StringConverter, false)
  Referred_Type: string = '';

  @JsonProperty('Response', StringConverter, false)
  Response: string = '';

  @JsonProperty('Documentation', StringConverter, false)
  Documentation: string = '';

  @JsonProperty('Preferred_Contact_Ind', IntConverter, false)
  Preferred_Contact_Ind: number = 0;

  @JsonProperty('Is_CoBorrower', IntConverter, false)
  Is_CoBorrower: number = 0;

  @JsonProperty('Assoc_Status', IntConverter, false)
  Assoc_Status: number = undefined;

  @JsonProperty('Status', IntConverter, false)
  Status: number = 0;

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number = 0;

  ActionDefault: number;
  FC_INVALID_THR: boolean;
  Office_ID: number;
  Duplicate_Assoc: boolean;

  @JsonProperty('Validation_ID', IntConverter, false)
  Validation_ID: number;

  @JsonProperty('Exception_ID', IntConverter, false)
  Exception_ID: number;
}

@JsonObject
export class loan_model {
  @JsonProperty('CoBorrower', ArrayConverter, true)
  CoBorrower: Array<borrower_model> = [];

  @JsonProperty('LoanCropUnits', ArrayConverter, true)
  LoanCropUnits: Loan_Crop_Unit[] = [];

  @JsonProperty('CropUnitPolicies', ArrayConverter, true)
  CropUnitPolicies: Crop_Unit_Policy[] = [];

  @JsonProperty('AphUnits', ArrayConverter, true)
  AphUnits: APH_Units[] = [];

  @JsonProperty('CropYield', [])
  CropYield: any = null;

  @JsonProperty('Farms', ArrayConverter, true)
  Farms: Loan_Farm[] = [];

  @JsonProperty('LoanQResponse', ArrayConverter, true)
  LoanQResponse: Array<LoanQResponse> = [];

  @JsonProperty('LoanBudget', ArrayConverter, true)
  LoanBudget: Array<Loan_Budget> = [];

  @JsonProperty('DisburseBudget', ArrayConverter, true)
  DisburseBudget: Array<Loan_Budget> = [];

  @JsonProperty('Loan_Full_ID', StringConverter)
  Loan_Full_ID: string = undefined;

  @JsonProperty('Association', ArrayConverter, true)
  Association: Loan_Association[] = new Array<Loan_Association>();

  @JsonProperty('LoanCollateral', ArrayConverter, true)
  LoanCollateral: Loan_Collateral[] = [];

  @JsonProperty('LoanMaster')
  LoanMaster: LoanMaster = undefined;

  @JsonProperty('DashboardStats', [])
  DashboardStats: any = undefined;

  @JsonProperty('LoanCropPractices', ArrayConverter, true)
  LoanCropPractices: Array<Loan_Crop_Practice> = [];

  LoanCropUnitFCvalues: Loan_Crop_Unit_FC = new Loan_Crop_Unit_FC();

  @JsonProperty('InsurancePolicies', ArrayConverter, true)
  InsurancePolicies: Array<Insurance_Policy> = undefined;

  @JsonProperty('LoanMarketingContracts', ArrayConverter, true)
  LoanMarketingContracts: Array<Loan_Marketing_Contract> = [];

  @JsonProperty('LoanCrops', ArrayConverter, true)
  LoanCrops: Array<Loan_Crop> = [];

  @JsonProperty('LoanDetail', ArrayConverter, true)
  LoanDetail: Array<any> = [];

  @JsonProperty('BorrowerIncomeHistory', ArrayConverter, true)
  BorrowerIncomeHistory: Borrower_Income_History[] = [];

  @JsonProperty('Loan_Exceptions', ArrayConverter, true)
  Loan_Exceptions: Array<Loan_Exception> = [];

  @JsonProperty('Loan_Validations', ArrayConverter, true)
  Loan_Validations: Array<Loan_Validation> = [];

  @JsonProperty('Loan_Documents', ArrayConverter, true)
  Loan_Documents: Array<Loan_Document> = [];

  @JsonProperty('Loan_Document_Details', ArrayConverter, true)
  Loan_Document_Details: Array<Loan_Document_Detail> = [];

  @JsonProperty('LoanCommittees', ArrayConverter, true)
  LoanCommittees: Array<Loan_Committee> = [];

  @JsonProperty('Meetings', ArrayConverter, true)
  Meetings: Array<Committee_Meetings> = [];

  @JsonProperty('LoanConditions', ArrayConverter, true)
  LoanConditions: Array<Loan_Condition> = [];

  @JsonProperty('LoanComments', ArrayConverter, true)
  LoanComments: Array<Loan_Comments> = [];

  @JsonProperty('RiskOtherQueues', ArrayConverter, true)
  RiskOtherQueues: Array<Risk_Other_Queue> = [];

  @JsonProperty('Loanwfrp', [])
  Loanwfrp: Loan_Wfrp = null;

  @JsonProperty('Disbursements', ArrayConverter, true)
  Disbursements: Array<Loan_Disburse> = null;

  @JsonProperty('User_Pending_Actions', ArrayConverter, true)
  User_Pending_Actions: Array<User_Pending_Action> = [];

  @JsonProperty('LoanPolicies', ArrayConverter, true)
  LoanPolicies: Array<Loan_Policy> = [];

  @JsonProperty('FarmCropOverride', ArrayConverter, true)
  FarmCropOverride: Array<FarmOverrideModel> = [];

  @JsonProperty('Cross_Collateralized_Loans', ArrayConverter, true)
  Cross_Collateralized_Loans: Array<Cross_Collateralized_Loans> = [];

  @JsonProperty('Prev_Yr_Loan_ID', StringConverter)
  Prev_Yr_Loan_ID: string;

  @JsonProperty('LoanOtherIncomes', ArrayConverter, true)
  LoanOtherIncomes: Array<Loan_Other_Income> = [];

  lasteditrowindex: number;
  srccomponentedit: string;

  CalculatedVariables: CalculatedVariables = new CalculatedVariables();

  SyncStatus: ModelStatus = {
    Status_Farm: Sync_Status.NOCHANGE,
    Status_Crop_Practice: Sync_Status.NOCHANGE,
    Status_Insurance_Policies: Sync_Status.NOCHANGE,
    Status_Borrower: Sync_Status.NOCHANGE,
    Status_Farmer: Sync_Status.NOCHANGE
  };
  //to be changed later
  Hrinclusion: boolean = true;

  FC_Ins_Disc_Collateral_Value: number = 0;

  @JsonProperty('Update_Budget_Ind', IntConverter, true)
  Update_Budget_Ind: number;

  @JsonProperty('Update_Budget_Defaults', ArrayConverter, true)
  Update_Budget_Defaults: Update_Budget_Default[];

  @JsonProperty('Borrower_Signers', ArrayConverter, true)
  Borrower_Signers: Array<Borrower_Signer> = [];
}

export class Borrower_Signer {
  Signer_ID: number;
  Borrower_ID: number;
  Loan_Full_ID: string;
  Signer_Name: string;
  Location: string;
  Phone: string;
  Email: string;
  Preferred_Contact_Ind: number;
  Title: string;
  Percent_Owned: number;
  SBI_Ind: number;
  Signer_Ind: number;
  Guarantor_Ind: number;
  Status: number;
  ActionStatus: number;
}

export class User_Pending_Action {
  User_PA_ID: number;
  User_ID: number;
  Loan_Full_ID: string;
  Ref_PA_ID: number;
  PA_Level: number;
  PA_Name: string;
  PA_Group_Code: string;
  PA_Code: string;
  Reminder_Ind: number;
  Status: number;
  From_User_ID: number;
  Trigger_ID: number;
  Trigger_Role: string;
  Support_Roles: Array<string>;
  Custom_Txt: string;
  Added_On: string;
  Modified_On: string;

  ActionStatus: number;

  constructor() {
    this.Support_Roles = [];
  }
}

export class UpdateMigrationAdjustmentModel {
  LoanFullID: string;
  PayLoad_Json: string;
}
export class LoanGroup {
  LoanJSON: loan_model;
  Loan_Full_ID: string;
  IsAffiliated: boolean;
  IsCrossCollateralized: boolean;
  IsCurrentYearLoan: boolean;
  IsPreviousYearLoan: boolean;
  Loan_ID: number;
  Loan_Seq_Number: number;
}

/**
 * The Cross Collateralized Loans.
 */
export class Cross_Collateralized_Loans {
  Cross_Collateral_Group_ID: number;
  Loan_ID: number;
  Status: number;
  ActionStatus: number;
  Loan_Full_ID: string;
  Application_Date: Date;
  Crop_Year: number;
  Loan_Type_Code: number;

  //For UI View Only
  Farmer: string;
  Borrower: string;
  View: string;
}

/**
 * The List of Distinct Loans for Collateralizing.
 */
export class Loan_Cross_Collateral {
  Borrower_First_Name: string;
  Borrower_Last_Name: string;
  Borrower_MI: string;
  Crop_Year: number;
  Cross_Collateral_Group_ID: number;
  Farmer_First_Name: string;
  Farmer_ID: number;
  Farmer_Last_Name: string;
  Farmer_MI: string;
  Loan_ID: number;
  Loan_Full_ID: string;
  Application_Date: Date;
  Status: number;

  //UI VIEW ONLY
  Farmer: string;
  Borrower: string;
  View: string;
}

export class Risk_Other_Queue {
  ROQ_ID: number;
  Loan_ID: number;
  Loan_Seq_ID: number;
  Loan_Full_ID: string;
  Field_ID: string;
  Ref_Database_ID: number;
  Ref_Column_ID: number;
  User_ID: number;
  User_First_Name: string;
  User_Last_Name: string;
  Data_Time_Entered: string;
  Date_Time_Action: string;
  Other_Text: string;
  Risk_Admin_ID: number;
  Suggested_Ref_ID: number;
  Response_Ind: number;
  Status: number;
}

export class Loan_Condition {
  Loan_Condition_ID: number;
  Loan_Full_ID: string;
  Loan_Seq_Num: number;
  Condition_Type_ID: number;
  Condition_Name: string;
  Condition_Level: number;
  Association_ID: number;
  Suggestion_Only_Ind: number;
  Date_Time: string;
  Other_Description: string;
  Status: number;
  Action_Status: number;
  Condition_Key: string;
  Document_ID: number;
  Sort_Order: number;
  Multiple: number;
  IsCustomCondition: number;

  User_ID: number;
  Requested_Date_Time: string;
  KL_Document_ID: number;
  Upload_User_ID: number;
  Uploaded_Date_Time: string;
}

export class Loan_Committee {
  CM_ID: number;
  Loan_Full_ID: string;
  User_ID: number;
  Role: number;
  Pool_Set_Name: string;
  Job_Title: string;
  Added_Date: string;
  Voted_Date: string;
  Vote: number;
  Vote_Emoji_Type: number;
  CM_Role: number;
  Status: number;
  ActionStatus: number;
  FirstName: string;
  LastName: string;
  Username: string;
  Email: string;
}

export class Loan_Comments {
  Loan_Comment_ID: number;
  Loan_Full_ID: string;
  Loan_ID: string;
  Loan_Seq_Num: number;
  Parent_Comment_ID: number;
  User_ID: number;
  Comment_Time: string;
  Comment_Type_Code: number;
  Comment_Type_Level_Code: number;
  Comment_Text: string;
  Parent_Emoji_ID: number;
  Comment_Emoji_ID: number;
  Comment_Read_Ind: number;
  Status: number;
  FirstName: string;
  LastName: string;
  IsRead: number;
}

export class Loan_List_Comment extends Loan_Comments {
  Crop_Year: number;
  Borrower_Name: string;
  Loan_Type_Code: number;
  Watch_List_Ind: number;
}

export class Add_Loan_Comments {
  Loan_Full_ID: string;
  User_ID: number;
  Comment_Type_Code: number;
  Comment_Type_Level_Code: number;
  Comment_Text: string;
  Parent_Emoji_ID: number;
  Comment_Emoji_ID: number;
  Comment_Read_Ind: number;
}

export class Decide_Action extends Add_Loan_Comments {
  // cross collaterized
  LoanIds: Array<string> = [];

  // pending actions
  ToUserIds: Array<number> = [];

  // rescind vote
  LoggedId_User_ID: number;
  CM_ID: number;
}

export class CalculatedVariables {
  FC_Total_CreditRef: number;
  FC_Total_CRF_Response: number;
  FC_Total_CRF_Neg_Response: number;
  FC_hasborrowerinc: boolean;
  //Contract Type
  FC_MKT_Total_Contract_Type_Exception_Ind: number;
  FC_ADCOL_Total_Contract_Type_Exception_Ind: number;

  //Measure Code
  FC_ADCOL_Total_Measure_Code_Exception_Ind: number;

  // new fields added for credit agreement calculation
  Total_Acres: number;
  Total_Farmed_Acres: number;

  // Farming Years History
  FC_Farming_Years: number;

  // Farms Related
  FC_Total_Farm_Without_Rent: number = 0;
  FC_Total_Farm_With_Rent_Waiver: number = 0;
  FC_Total_Farm_With_Permission_To_Insure: number = 0;

  // Loan Office Role
  FC_Valid_LO_Role: boolean;
  Loan_Officer_Role: string;
}

export class Loan_Budget {
  @JsonProperty('Loan_Budget_ID', IntConverter, false)
  Loan_Budget_ID: number;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string = '';

  @JsonProperty('Crop_Practice_ID', IntConverter, false)
  Crop_Practice_ID: number;

  @JsonProperty('Expense_Type_ID', IntConverter, false)
  Expense_Type_ID: number = 0;

  @JsonProperty('Expense_Type_Name', StringConverter, false)
  Expense_Type_Name: string = '';

  @JsonProperty('ARM_Budget_Acre', IntConverter, false)
  ARM_Budget_Acre: number = 0;

  @JsonProperty('Distributor_Budget_Acre', IntConverter, false)
  Distributor_Budget_Acre: number = 0;

  @JsonProperty('Third_Party_Budget_Acre', IntConverter, false)
  Third_Party_Budget_Acre: number = 0;

  @JsonProperty('Total_Budget_Acre', IntConverter, false)
  Total_Budget_Acre: number = 0;

  @JsonProperty('ARM_Budget_Crop', IntConverter, false)
  ARM_Budget_Crop: number = 0;

  @JsonProperty('Distributor_Budget_Crop', IntConverter, false)
  Distributor_Budget_Crop: number = 0;

  @JsonProperty('Third_Party_Budget_Crop', IntConverter, false)
  Third_Party_Budget_Crop: number = 0;

  @JsonProperty('Total_Budget_Crop_ET', IntConverter, false)
  Total_Budget_Crop_ET: number = 0;

  @JsonProperty('ARM_Used', IntConverter, false)
  ARM_Used: number = 0;

  @JsonProperty('Notes', StringConverter, false)
  Notes: string = '';

  @JsonProperty('Other_Description_Text', StringConverter, false)
  Other_Description_Text: string = '';

  @JsonProperty('IsCustom', IntConverter, false)
  IsCustom: number = 0;

  @JsonProperty('Status', IntConverter, false)
  Status: number = 0;

  @JsonProperty('Budget_Type', StringConverter, false)
  Budget_Type: string = ''; // '' = Crop, 'F' = Fixed, 'V' = Total Rows

  @JsonProperty('Z_Loan_ID', IntConverter, false)
  Z_Loan_ID: number = 0;

  @JsonProperty('Z_Loan_Seq_num', IntConverter, false)
  Z_Loan_Seq_num: number = 0;

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number = 0;

  @JsonProperty('Sort_order', IntConverter, false)
  Sort_order: number;

  @JsonProperty('Crop_Input_Ind', StringConverter, false)
  Crop_Input_Ind: string;

  @JsonProperty('Variable_Ind', StringConverter, false)
  Variable_Ind: string;

  @JsonProperty('Standard_Ind', IntConverter, false)
  Standard_Ind: number;

  @JsonProperty('Validation_Exception_Ind', StringConverter, false)
  Validation_Exception_Ind: number;

  @JsonProperty('Validation_ID', StringConverter, false)
  Validation_ID: number;

  @JsonProperty('Exception_ID', StringConverter, false)
  Exception_ID: number;

  row_id?: number;
  Needed_Ind: number;
}

export class Borrower_Income_History {
  @JsonProperty('BIH_ID', IntConverter, false)
  BIH_ID: number = 0;

  @JsonProperty('Borrower_ID', IntConverter, false)
  Borrower_ID: number = 0;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string = '';

  @JsonProperty('Borrower_Year', IntConverter, false)
  Borrower_Year: number = 0;

  @JsonProperty('Borrower_Expense', IntConverter, false)
  Borrower_Expense: number = 0;

  @JsonProperty('Borrower_Revenue', IntConverter, false)
  Borrower_Revenue: number = 0;

  @JsonProperty('FC_Borrower_Income', IntConverter, false)
  FC_Borrower_Income: number = 0;

  @JsonProperty('Status', IntConverter, false)
  Status: number = 0;

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number = 0;

  FC_Index: number;
}

@JsonObject
export class Loan_Crop_Practice {
  @JsonProperty('Loan_Crop_Practice_ID', IntConverter, false)
  Loan_Crop_Practice_ID: number = 0;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string = '';

  @JsonProperty('Crop_Practice_ID', IntConverter, false)
  Crop_Practice_ID: number = 0;

  @JsonProperty('LCP_APH', IntConverter, false)
  LCP_APH: number = 0;

  @JsonProperty('LCP_Acres', IntConverter, false)
  LCP_Acres: number = 0;

  @JsonProperty('LCP_ARM_Budget', IntConverter, false)
  LCP_ARM_Budget: number = 0;

  @JsonProperty('LCP_Distributer_Budget', IntConverter, false)
  LCP_Distributer_Budget: number = 0;

  @JsonProperty('LCP_Third_Party_Budget', IntConverter, false)
  LCP_Third_Party_Budget: number = 0;

  @JsonProperty('Market_Value', IntConverter, false)
  Market_Value: number = 0;

  @JsonProperty('Disc_Market_Value', IntConverter, false)
  Disc_Market_Value: number = 0;

  @JsonProperty('LCP_Notes', StringConverter, false)
  LCP_Notes: string = '';

  @JsonProperty('LCP_Status', IntConverter, false)
  LCP_Status: number = 0;

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number = 0;

  FC_CropName?: string = '';
  FC_PracticeType?: string = '';

  FC_CropYield: number = 0;
  FC_Share: number = 0;
  FC_RateYieldAVG: number = 0;

  //Values on top for crop pratice

  // FC_Agg_Mkt_Value & FC_Agg_Disc_Mkt_Value not needed as already storing their values in Market_Value & Disc_Market_Value
  // FC_Agg_Mkt_Value? :number=0;
  // FC_Agg_Disc_Mkt_Value? :number=0;

  FC_LCP_Ins_Value_Acre?: number = 0;
  FC_LCP_Ins_Value?: number = 0;

  FC_LCP_Disc_Ins_Value_Acre?: number = 0;
  FC_LCP_Disc_Ins_Value?: number = 0;

  FC_LCP_Disc_Cei_Value_Acre?: number = 0;
  FC_LCP_Disc_Cei_Value?: number = 0;

  FC_LCP_Cei_Value_Acre?: number = 0;
  FC_LCP_Cei_Value?: number = 0;

  // values according to insurance types
  FC_LCP_Ins_Value_Hmax?: number = 0;
  FC_LCP_Disc_Ins_Value_Hmax: number = 0;

  FC_LCP_Ins_Value_Mpci?: number = 0;
  FC_LCP_Disc_Ins_Value_Mpci: number = 0;

  FC_LCP_Ins_Value_Ramp?: number = 0;
  FC_LCP_Disc_Ins_Value_Ramp: number = 0;

  FC_LCP_Ins_Value_Sco?: number = 0;
  FC_LCP_Disc_Ins_Value_Sco: number = 0;

  FC_LCP_Ins_Value_Stax?: number = 0;
  FC_LCP_Disc_Ins_Value_Stax: number = 0;

  FC_LCP_Ins_Value_Ice?: number = 0;
  FC_LCP_Disc_Ins_Value_Ice: number = 0;

  FC_LCP_Ins_Value_Abc?: number = 0;
  FC_LCP_Disc_Ins_Value_Abc: number = 0;

  FC_LCP_Ins_Value_Pci?: number = 0;
  FC_LCP_Disc_Ins_Value_Pci: number = 0;

  FC_LCP_Ins_Value_Crophail?: number = 0;
  FC_LCP_Disc_Ins_Value_Crophail: number = 0;
  //

  // per acre values
  FC_LCP_Ins_Value_Mpci_Acre?: number = 0;
  FC_LCP_Disc_Ins_Value_Mpci_Acre?: number = 0;

  FC_LCP_Ins_Value_Hmax_Acre?: number = 0;
  FC_LCP_Disc_Ins_Value_Hmax_Acre?: number = 0;

  FC_LCP_Ins_Value_Ramp_Acre?: number = 0;
  FC_LCP_Disc_Ins_Value_Ramp_Acre?: number = 0;

  FC_LCP_Ins_Value_Ice_Acre?: number = 0;
  FC_LCP_Disc_Ins_Value_Ice_Acre?: number = 0;

  FC_LCP_Ins_Value_Abc_Acre?: number = 0;
  FC_LCP_Disc_Ins_Value_Abc_Acre?: number = 0;

  FC_LCP_Ins_Value_Pci_Acre?: number = 0;
  FC_LCP_Disc_Ins_Value_Pci_Acre?: number = 0;

  FC_LCP_Ins_Value_Crophail_Acre?: number = 0;
  FC_LCP_Disc_Ins_Value_Crophail_Acre?: number = 0;

  FC_LCP_Ins_Value_Stax_Acre?: number = 0;
  FC_LCP_Disc_Ins_Value_Stax_Acre?: number = 0;

  FC_LCP_Ins_Value_Sco_Acre?: number = 0;
  FC_LCP_Disc_Ins_Value_Sco_Acre?: number = 0;

  FC_Market_Value_Acre?: number = 0;
  FC_Disc_Market_Value_Acre?: number = 0;

  FC_Crop_Type: String;
  FC_Crop_Practice_Type: String;
  Fc_Crop_Code: string;
  Update_Budget_Ind: any;
}

@JsonObject
export class FarmersList {
  @JsonProperty('Farmer_ID', IntConverter, false)
  Farmer_ID: number;

  @JsonProperty('Farmer_Last_Name', StringConverter, false)
  Farmer_Last_Name: string;

  @JsonProperty('Farmer_First_Name', StringConverter, false)
  Farmer_First_Name: string;

  @JsonProperty('Farmer_SSN_Hash', StringConverter, false)
  Farmer_SSN_Hash: string;
}

@JsonObject
export class Loan_Collateral {
  @JsonProperty('Collateral_ID', IntConverter, false)
  Collateral_ID: number = 0;

  @JsonProperty('Loan_ID', IntConverter, false)
  Loan_ID: number = 0;

  @JsonProperty('Loan_Seq_Num', IntConverter, false)
  Loan_Seq_Num: number = 0;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string = '';

  @JsonProperty('Collateral_Category_Code', StringConverter, false)
  Collateral_Category_Code: string = '';

  @JsonProperty('Collateral_Type_Code', IntConverter, false)
  Collateral_Type_Code: number = 0;

  @JsonProperty('Collateral_Sub_Type_Code', IntConverter, false)
  Collateral_Sub_Type_Code: number = 0;

  @JsonProperty('Crop_Detail', StringConverter, false)
  Crop_Detail: string = '';

  @JsonProperty('Crop_Type', StringConverter, false)
  Crop_Type: string = '';

  @JsonProperty('Collateral_Description', StringConverter, false)
  Collateral_Description: string = '';

  @JsonProperty('Measure_Code', StringConverter, false)
  Measure_Code: string = '';

  @JsonProperty('Insurance_Category_Code', IntConverter, false)
  Insurance_Category_Code: number = 0;

  @JsonProperty('Insurance_Type_Code', IntConverter, false)
  Insurance_Type_Code: number = 0;

  @JsonProperty('Insurance_Sub_Type_Code', IntConverter, false)
  Insurance_Sub_Type_Code: number = 0;

  @JsonProperty('Market_Value', IntConverter, false)
  Market_Value: number = 0;

  @JsonProperty('Prior_Lien_Amount', IntConverter, false)
  Prior_Lien_Amount: number = 0;

  @JsonProperty('Net_Market_Value', IntConverter, false)
  Net_Market_Value: number = 0;

  @JsonProperty('Disc_Pct', IntConverter, false)
  Disc_Pct: number = 0;

  @JsonProperty('Insurance_Value', IntConverter, false)
  Insurance_Value: number = 0;

  @JsonProperty('Disc_Ins_Value', IntConverter, false)
  Disc_Ins_Value: number = 0;

  @JsonProperty('Ins_Value', IntConverter, false)
  Ins_Value: number = 0;

  @JsonProperty('Insurance_Disc_Value', IntConverter, false)
  Insurance_Disc_Value: number = 0;

  @JsonProperty('Disc_Mkt_Value', IntConverter, false)
  Disc_Mkt_Value: number = 0;

  @JsonProperty('Lien_Holder', StringConverter, false)
  Lien_Holder: string = '';

  @JsonProperty('Status', IntConverter, false)
  Status: number = 0;

  @JsonProperty('IsDelete', IntConverter, false)
  IsDelete: number = 0;

  @JsonProperty('Insured_Flag', IntConverter, false)
  Insured_Flag: number = 0;

  @JsonProperty('Qty', IntConverter, false)
  Qty: number = 0;

  @JsonProperty('Price', IntConverter, false)
  Price: number = 0;

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number = 0;

  @JsonProperty('Assoc_ID', IntConverter, false)
  Assoc_ID: number;

  @JsonProperty('Contract', StringConverter, false)
  Contract: string = '';

  @JsonProperty('Contract_Type_Code', StringConverter, false)
  Contract_Type_Code: string = '';

  @JsonProperty('Measure_Description', StringConverter, false)
  Measure_Description: string = '';

  @JsonProperty('Income_Ind', IntConverter, false)
  Income_Ind: number = 0;

  @JsonProperty('Loan_Other_Income_ID', IntConverter, false)
  Loan_Other_Income_ID: number = 0;

  Exception_Ind: string;

  Disc_Value: number = 0;
  collateralInsDisc: any = [];
  FC_Crop_Name: string;
}

export class Loan_Marketing_Contract {
  Contract_ID: number;
  Z_Loan_ID: number;
  Z_Loan_Seq_Num: string;
  Loan_Full_ID: string;
  Category: number;
  Crop_Code: string;
  FC_Crop_Name: string;
  Crop_Type_Code: string;
  Assoc_Type_Code: string;
  Assoc_ID: number;
  Quantity: number;
  Price: number;
  UoM: string;
  Description_Text: string;
  ActionStatus: number;
  Market_Value: number;
  Contract_Per: number;
  rowid: number;
  Contract: string;
  Contract_Type_Code: string;
}

export class Loan_Crop {
  Loan_Crop_ID: number;
  Loan_Full_ID: string;
  Crop_Code: string;
  Crop_ID: number;
  Crop_Type_Code: string;
  Crop_Price: number;
  Basic_Adj: number;
  Marketing_Adj: number;
  Rebate_Adj: number;
  Adj_Price: number;
  Contract_Qty: number;
  Contract_Price: number;
  Percent_booked: number;
  Acres: number;
  Revenue: number;
  W_Crop_Yield: number;
  LC_Share: number;
  ActionStatus: number;
  InsUOM: string;
  Irr_Acres: number;
  NI_Acres: number;
  Update_Basis_Ind: number;
  Z_Office_ID: number;
  Z_Region_ID: number;

  Needed_Ind: number = 0;
  Crop_Price_Percent: number = 0;
  Crop_Price_Percent_Value: number = 0;
}

export class loan_farmer {
  Farmer_SSN_Hash: string;
  Farmer_Last_Name: string;
  Farmer_First_Name: string;
  Farmer_MI: string;
  Farmer_DL_State: string;
  Farmer_DL_Num: string;
  Farmer_Address: string;
  Farmer_City: string;
  Farmer_State: string;
  Farmer_Zip: number;
  Farmer_Phone: string;
  Farmer_Email: string;
  Farmer_DOB: string;
  Farmer_Preferred_Contact_Ind: number;
  Year_Begin_Farming: number;
  Year_Begin_Client: number;
}

export class borrower_income_history_model {
  BIH_ID: number = undefined;
  Loan_ID: number = undefined;
  Borrower_Year: number = undefined;
  Borrower_Expense: number = undefined;
  Borrower_Revenue: number = undefined;
  Borrower_Income: number = undefined;
}

export class LoanException {
  @JsonProperty('Loan_Exception_ID', IntConverter, false)
  Loan_Exception_ID: number;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string;

  @JsonProperty('Loan_Seq_Num', IntConverter, false)
  Loan_Seq_Num: number;

  @JsonProperty('Exception_ID', IntConverter, false)
  Exception_ID: number;

  @JsonProperty('Array_Item_ID', StringConverter, false)
  Array_Item_ID: string;

  @JsonProperty('Exception_Level', IntConverter, false)
  Exception_ID_Level: number;

  @JsonProperty('Chevron_ID', IntConverter, false)
  Chevron_ID: number;

  @JsonProperty('Field_Ind', IntConverter, false)
  Field_Ind: ExceptionSource;

  @JsonProperty('Exception_Mitigation_Text', StringConverter, false)
  Exception_Mitigation_Text: string;

  @JsonProperty('Exception_Justification', StringConverter, false)
  Justification: string;

  @JsonProperty('Exception_Sort_Order', IntConverter, false)
  Sort_Order: number;

  @JsonProperty('Action_Status', IntConverter, false)
  Action_Status: number;

  @JsonProperty('Is_Justification_Required', IntConverter, false)
  Is_Justification_Required: number;

  @JsonProperty('Tab_ID', IntConverter, false)
  Tab_ID: number;

  @JsonProperty('Tab_Name', StringConverter, false)
  Tab_Name: string;
}

export enum Logpriority {
  None = 0, // nothing at all
  Low = 1, //Logs only events
  High = 2 // all of them,
}

export enum Loan_Type_Codes {
  AgPro = 0, // nothing at all
  AgInput = 1, //Logs only events
  AllIn = 2, // all of them,
  NA = 3
}

export function Loantype_dsctext(lc: number) {
  if (lc == Loan_Type_Codes.AgPro) {
    return 'Ag-Pro';
  }

  if (lc == Loan_Type_Codes.AllIn) {
    return 'All-In';
  }

  if (lc == Loan_Type_Codes.AgInput) {
    return 'Ag-Input';
  }

  if (lc == Loan_Type_Codes.NA) {
    return 'N/A';
  }
}

export class Logs {
  Log_Id: number;
  Log_Section: string;
  Log_Message: string;
  Sql_Error: string;
  Loan_Full_ID: string;
  SourceName: string;
  SourceDetail: string;
  userID: number;
  Severity: SeverityEnum;
}

export class Loan_Wfrp {
  Wfrv_ID: number;
  Proposed_AIP: number;
  Approved_Revenue: number;
  Level: number;
  Premium: number;
  Agent_ID: number;
  Agency_ID: number;
  FC_WfrpEnabled: boolean;
  //this goes too backend when checkbox is checked
  Wfrp_Enabled: boolean;
  FC_Wfrp_MaxLevelPct: number;
  FC_Wfrp_MaxAllowedRevenue: number;
  Wfrp_Ins_Value: number;
  Wfrp_Disc_Ins_Value: number;
  Wfrp_Add_Coverage: number;
  Wfrp_Value: number;
  Wfrp_Disc_Value: number;
  FC_Wfrp_CEI: number;
}

export enum SeverityEnum {
  Success = 1,
  SqlError = 2,
  NonSQLError = 3,
  FrontEndError = 4
}

@JsonObject
export class FarmerDataModel {
  @JsonProperty('Farmer_ID', IntConverter, false)
  Farmer_ID: number;

  @JsonProperty('Farmer_ID_Type', IntConverter, false)
  Farmer_ID_Type: number;

  @JsonProperty('Farmer_SSN_Hash', StringConverter, false)
  Farmer_SSN_Hash: string;

  @JsonProperty('Farmer_Entity_Type', IntConverter, false)
  Farmer_Entity_Type: number;

  @JsonProperty('Farmer_Entity_Notes', StringConverter, false)
  Farmer_Entity_Notes: string;

  @JsonProperty('Farmer_Last_Name', StringConverter, false)
  Farmer_Last_Name: string;

  @JsonProperty('Farmer_First_Name', StringConverter, false)
  Farmer_First_Name: string;

  @JsonProperty('Farmer_MI', StringConverter, false)
  Farmer_MI: string;

  @JsonProperty('Farmer_Address', StringConverter, false)
  Farmer_Address: string;

  @JsonProperty('Farmer_City', StringConverter, false)
  Farmer_City: string;

  @JsonProperty('Farmer_State_ID', StringConverter, false)
  Farmer_State_ID: string;

  @JsonProperty('Farmer_Zip', IntConverter, false)
  Farmer_Zip: number;

  @JsonProperty('Farmer_Phone', StringConverter, false)
  Farmer_Phone: string;

  @JsonProperty('Farmer_Email', StringConverter, false)
  Farmer_Email: string;

  @JsonProperty('Farmer_Pref_Contact_Ind', IntConverter, false)
  Farmer_Pref_Contact_Ind: number;

  @JsonProperty('Farmer_DL_State', StringConverter, false)
  Farmer_DL_State: string;

  @JsonProperty('Farmer_DL_Num', StringConverter, false)
  Farmer_DL_Num: string;

  @JsonProperty('Farmer_DOB', StringConverter, false)
  Farmer_DOB: string;

  @JsonProperty('Year_Begin_farming', IntConverter, false)
  Year_Begin_farming: number;

  @JsonProperty('Year_Begin_Client', IntConverter, false)
  Year_Begin_Client: number;
}

@JsonObject
export class FarmOverrideModel {
  @JsonProperty('Farm_ID', IntConverter, false)
  Farm_ID: number;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string;

  @JsonProperty('Crop_Code', StringConverter, false)
  Crop_Code: string;

  @JsonProperty('Share_Rent_Override', false)
  Share_Rent_Override: boolean;

  @JsonProperty('Share_Rent_Percent', IntConverter, false)
  Share_Rent_Percent: number;

  @JsonProperty('Status', IntConverter, false)
  Status: number;

  crop: string;
  crop_type: string;
  crop_prac: string;
  irr_prac?: string;
}

@JsonObject
export class Loan_Other_Income {
  @JsonProperty('Loan_Other_Income_ID', IntConverter, false)
  Loan_Other_Income_ID: number;

  @JsonProperty('Other_Income_ID', IntConverter, false)
  Other_Income_ID: number;

  @JsonProperty('Other_Income_Name', StringConverter, false)
  Other_Income_Name: string;

  @JsonProperty('Crop_Code', StringConverter, false)
  Crop_Code: string;

  @JsonProperty('Loan_Collateral_ID', IntConverter, false)
  Loan_Collateral_ID: number;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string;

  @JsonProperty('Other_Description_Text', StringConverter, false)
  Other_Description_Text: string;

  @JsonProperty('Amount', IntConverter, false)
  Amount: number;

  @JsonProperty('Collateral_Ind', IntConverter, false)
  Collateral_Ind: number;

  @JsonProperty('Status', IntConverter, false)
  Status: number;

  @JsonProperty('Sort_Order', IntConverter, false)
  Sort_Order: number;

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number;
}

export class Update_Budget_Default {
  Crop_Full_Key: string;
  Crop_Practice_ID: number;
  Z_Office_ID: number;
  Z_Region_ID: number;
}

export class Committee_Meetings {
  emailAddress: EmailAddressModel;
  type: string;
}

export class EmailAddressModel {
  address: string;
  name: string;
}

export class AdminUser {
  User_ID: number;
  Username: string;
  Name: string;
  Office_ID: string;
}
