import { exceptionlevel } from '../commonmodels';

export class Loan_Document {
  public Loan_Document_ID: number;
  public Prev_Loan_Document_ID: number;

  public Loan_Full_ID: string;
  public Loan_ID: number;

  public Document_Name: string;

  public Document_Type_ID: number;
  public Document_Type_Level: number;

  public Document_Template_ID: number;
  public Document_State_Code: string;
  public Sort_Order: number;

  public Replication_Association_Type: string;
  public Replication_Association_ID: number;

  public Critical_Change_Ind: number;
  public Standard_Document_Ind: number;

  public Request_User_ID: number;
  public Request_User_Name: string;
  public Request_Date_Time: string;

  public KL_Document_ID: number;
  public Upload_User_ID: number;
  public Upload_User_Name: string;
  public Upload_User_Email: string;
  public Upload_Date_Time: string;

  public Document_URL: string;
  public Document_URL_Unexecuted: string;
  public Document_URL_Executed: string;

  public Status: number;

  public ActionStatus: number;

  public Action_Ind: Loan_Document_Action;
  public Document_Details: string;
  public Document_Key: string;

  public Doc_Needed_Ind: number;
  public Is_Custom: number;
  Exception_Level: exceptionlevel;
}

export enum Loan_Document_Action {
  Archived = 0,
  RequireUpload = 1,
  RequireReupload = 2,
  Uploaded = 3
}


export enum Loan_Document_Downloan_Type {
  Unexecuted,
  Executed
}


export class Loan_Document_Detail {
  Loan_Document_Detail_ID: number;
  Loan_Document_ID: number;
  Loan_Full_ID: string;
  Field_ID: number;
  Field_ID_Value: string;
  Critical_Change_Detail_Ind: number;
  Status: number;
  ActionStatus: number;

  constructor(value: string) {
    this.Field_ID_Value = value;
  }
}
