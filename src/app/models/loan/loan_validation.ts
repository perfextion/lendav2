import { JsonObject, JsonProperty } from 'json2typescript';
import {
  IntConverter,
  StringConverter
} from '@lenda/Workers/utility/jsonconvertors';

export class Loan_Validation {
  @JsonProperty('Loan_Validation_ID', IntConverter, false)
  Loan_Validation_ID: Number;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string;

  @JsonProperty('Validation_ID', IntConverter, false)
  Validation_ID: number;

  @JsonProperty('Validation_ID_Text', StringConverter, false)
  Validation_ID_Text: string;

  @JsonProperty('Validation_ID_Level', IntConverter, false)
  Validation_ID_Level: number;

  @JsonProperty('Validation_Key', StringConverter, false)
  Validation_Key: string;

  @JsonProperty('Validation_Details', StringConverter, false)
  Validation_Details: string;

  @JsonProperty('Tab_ID', StringConverter, false)
  Tab_ID: string;

  @JsonProperty('Chevron_ID', StringConverter, false)
  Chevron_ID: string;

  @JsonProperty('Validation_Action_Ind', IntConverter, false)
  Validation_Action_Ind: number;

  @JsonProperty('Status', IntConverter, false)
  Status: number;

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number;

  @JsonProperty('RowID', StringConverter, false)
  RowID: string;

  @JsonProperty('ColID', StringConverter, false)
  ColID: string;

  @JsonProperty('FormDetails', StringConverter, false)
  FormDetails: string;

  @JsonProperty('HoverText', StringConverter, false)
  HoverText: string;

  @JsonProperty('Ignore_Ind', IntConverter, false)
  Ignore_Ind: number;

  @JsonProperty('Validation_Json', StringConverter, false)
  Validation_Json: string;

  Details: Array<string>;
}
