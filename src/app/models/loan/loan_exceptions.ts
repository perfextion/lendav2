import { JsonObject, JsonProperty } from 'json2typescript';
import {
  IntConverter,
  StringConverter
} from '@lenda/Workers/utility/jsonconvertors';

@JsonObject
export class Loan_Exception {
  @JsonProperty('Loan_Exception_ID', IntConverter, false)
  Loan_Exception_ID: number;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string;

  @JsonProperty('Exception_ID', IntConverter, false)
  Exception_ID: number;

  @JsonProperty('Exception_ID_Text', StringConverter, false)
  Exception_ID_Text: string;

  @JsonProperty('Exception_Key', StringConverter, false)
  Exception_Key: string;

  @JsonProperty('Exception_ID_Level', IntConverter, false)
  Exception_ID_Level: number;

  @JsonProperty('Tab_ID', IntConverter, false)
  Tab_ID: number;

  @JsonProperty('Chevron_ID', IntConverter, false)
  Chevron_ID: number;

  @JsonProperty('Mitigation_Ind', IntConverter, false)
  Mitigation_Ind: number;

  @JsonProperty('Mitigation_Text', StringConverter, false)
  Mitigation_Text: string;

  @JsonProperty('Sort_Order', IntConverter, false)
  Sort_Order: number;

  @JsonProperty('Support_Ind', IntConverter, false)
  Support_Ind: number;

  /**
   * Role_Type_Code_1
   */
  @JsonProperty('Support_Role_Type_Code_1', StringConverter, false)
  Support_Role_Type_Code_1: string;

  @JsonProperty('Support_User_ID_1', IntConverter, false)
  Support_User_ID_1: number;

  @JsonProperty('Support_User_Name_1', StringConverter, false)
  Support_User_Name_1: string;

  @JsonProperty('Support_Date_Time_1', StringConverter, false)
  Support_Date_Time_1: Date | string;

  /**
   * Role_Type_Code_2
   */
  @JsonProperty('Support_Role_Type_Code_2', StringConverter, false)
  Support_Role_Type_Code_2: string;

  @JsonProperty('Support_User_ID_2', IntConverter, false)
  Support_User_ID_2: number;

  @JsonProperty('Support_User_Name_2', StringConverter, false)
  Support_User_Name_2: string;

  @JsonProperty('Support_Date_Time_2', StringConverter, false)
  Support_Date_Time_2: Date | string;

  /**
   * Role_Type_Code_3
   */
  @JsonProperty('Support_Role_Type_Code_3', StringConverter, false)
  Support_Role_Type_Code_3: string;

  @JsonProperty('Support_User_ID_3', IntConverter, false)
  Support_User_ID_3: number;

  @JsonProperty('Support_User_Name_3', StringConverter, false)
  Support_User_Name_3: string;

  @JsonProperty('Support_Date_Time_3', StringConverter, false)
  Support_Date_Time_3: string | Date;

  /**
   * Role_Type_Code_4
   */
  @JsonProperty('Support_Role_Type_Code_4', StringConverter, false)
  Support_Role_Type_Code_4: string;

  @JsonProperty('Support_User_ID_4', IntConverter, false)
  Support_User_ID_4: number;

  @JsonProperty('Support_User_Name_4', StringConverter, false)
  Support_User_Name_4: string;

  @JsonProperty('Support_Date_Time_4', StringConverter, false)
  Support_Date_Time_4: string | Date;

  @JsonProperty('Status', IntConverter, false)
  Status: number;

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number;

  @JsonProperty('Custom_Ind', IntConverter, false)
  Custom_Ind: number;

  @JsonProperty('Document_ID', IntConverter, false)
  Document_Type_ID: number;

  @JsonProperty('Complete_Ind', IntConverter, false)
  Complete_Ind: number;

  /**
   * Only for UI - To check Exception is needed or not
   */
  Needed_Ind: number;
}
