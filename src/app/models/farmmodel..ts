import { JsonProperty, JsonObject } from 'json2typescript';
import {
  IntConverter,
  StringConverter
} from '../Workers/utility/jsonconvertors';

@JsonObject
export class Loan_Farm {
  @JsonProperty('Farm_ID', IntConverter, false)
  Farm_ID: number = undefined;

  @JsonProperty('Loan_Full_ID', StringConverter, false)
  Loan_Full_ID: string = '';

  @JsonProperty('Farm_State_ID', IntConverter, false)
  Farm_State_ID: number = 0;

  @JsonProperty('Farm_County_ID', IntConverter, false)
  Farm_County_ID: number = 0;

  @JsonProperty('Percent_Prod', IntConverter, false)
  Percent_Prod: number = 0;

  @JsonProperty('Rent_UOM', IntConverter, false)
  Rent_UOM: number = 1;

  @JsonProperty('FSN', StringConverter, false)
  FSN: string = '';

  @JsonProperty('Section', StringConverter, false)
  Section: string = '';

  @JsonProperty('Rated', StringConverter, false)
  Rated: string = '';

  @JsonProperty('Owned', IntConverter, false)
  Owned: number = 0;

  @JsonProperty('Cash_Rent_Used', IntConverter, false)
  Cash_Rent_Used: number = 0;


  @JsonProperty('Landowner', StringConverter, false)
  Landowner: string = '';

  @JsonProperty('Share_Rent', IntConverter, false)
  Share_Rent: number = 0;

  @JsonProperty('Cash_Rent_Total', IntConverter, false)
  Cash_Rent_Total: number = 0;

  @JsonProperty('Cash_Rent_Per_Acre', IntConverter, false)
  Cash_Rent_Per_Acre: number = 0;

  @JsonProperty('Cash_Rent_Due_Date', StringConverter, false)
  Cash_Rent_Due_Date: string = '';

  @JsonProperty('Cash_Rent_Paid', IntConverter, false)
  Cash_Rent_Paid: number = 0;

  @JsonProperty('Permission_To_Insure', IntConverter, false)
  Permission_To_Insure: number = 0;

  /**
   * Cash Rent Waived Per Acre
   */
  @JsonProperty('Cash_Rent_Waived', IntConverter, false)
  Cash_Rent_Waived: number = 0;

  /**
   * Cash_Rent_Waived_Total_Amount
   */
  @JsonProperty('Cash_Rent_Waived_Amount', IntConverter, false)
  Cash_Rent_Waived_Amount: number = 0;

  @JsonProperty('Irr_Acres', IntConverter, false)
  Irr_Acres: number = 0;

  @JsonProperty('NI_Acres', IntConverter, false)
  NI_Acres: number = 0;

  @JsonProperty('Crop_share_Detail_Indicator', IntConverter, false)
  Crop_share_Detail_Indicator: number = 0;

  @JsonProperty('Status', IntConverter, false)
  Status: number = 0;

  @JsonProperty('ActionStatus', IntConverter, false)
  ActionStatus: number = undefined;

  index: number = 0;

  FC_Total_Acres: number;
  FC_State_Name: string;
  FC_County_Name: string;

  //Added for the schedule land report on borrower summary
  FC_Rent_Total: number;
  FC_Waived_Total: number;

  /**
   * Use only for Display Purpose - not stored in db - Waived
   */
  Waived_Rent: number = 0;

  /**
   * Use only for Display Purpose - not stored in db - Rent
   */
  Display_Rent: number = 0;

  FC_Net_Rent_Acre: number;
  FC_Total_Used_Acres: number;
  FC_State_Code: string;
  FC_Total_Used_Irr_Acres: number;
  FC_Total_Used_NI_Acres: number;

  /**
   * Invalid Farm Key
   */
  Duplicate_Farm_Key: boolean;
  FC_Perm_To_Ins: boolean;
}
