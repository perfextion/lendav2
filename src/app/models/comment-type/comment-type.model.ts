export class CommentType {
  id: number;
  title: string;
  fontType: string;
  icon: string;
  color: string;
  order?: number;
  code?: string;
}

export class Priority {
  id: number;
  title: string;
}

export class CommentReadType {
  id: number;
  title: string;
  fontType: string;
  icon: string;
  color: string;
}

export enum CommentTypeEnum {
  PassiveComment,
  CommitteeComment,
  Note,
  ConfCall,
  WatchList
}

export enum FontType {
  Material = 'mat',
  Flaticon = 'flaticon'
}
