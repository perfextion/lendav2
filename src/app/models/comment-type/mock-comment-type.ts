import { CommentType, Priority, CommentReadType, CommentTypeEnum } from './comment-type.model';

export const CommentTypeMock: CommentType[] = [
  {
    id: -1,
    title: 'All',
    fontType: 'flaticon',
    icon: 'flaticon-multiple-users-silhouette',
    color: '#ded8d8'
  },
  {
    id: 0,
    title: 'Passive Comment',
    fontType: 'flaticon',
    icon: 'flaticon-multiple-users-silhouette',
    color: '#23b4ad',
    order: 1,
    code: 'PAS'
  },
  {
    id: 1,
    title: 'Committee Comment',
    fontType: 'flaticon',
    icon: 'flaticon-multiple-users-silhouette',
    color: '#FFBB19',
    order: 0,
    code: 'CMT'
  },
  {
    id: 2,
    title: 'User Note',
    fontType: 'mat',
    icon: 'mode_edit',
    color: '#FFBB19',
    order: 2,
    code: 'NOTE'
  },
  {
    id: 3,
    title: 'Conferrence Call',
    fontType: 'flaticon',
    icon: 'flaticon-old-typical-phone',
    color: '#d2162a',
    order: 3,
    code: 'CNF'
  },
  {
    id: 4,
    title: 'Watch List',
    fontType: 'mat',
    icon: 'remove_red_eye',
    color: '#d2162a',
    order: 4
  }
];

export const PriorityMock: Priority[] = [
  {
    id: 0,
    title: 'Normal'
  },
  {
    id: 1,
    title: 'Urgent'
  }
];

export const CommentReadTypeMock: CommentReadType[] = [
  {
    id: -1,
    title: 'All',
    fontType: 'mat',
    icon: 'done',
    color: '#ded8d8'
  },
  {
    id: 0,
    title: 'UnRead',
    fontType: 'mat',
    icon: 'done',
    color: '#23b4ad'
  },
  {
    id: 1,
    title: 'Read',
    fontType: 'mat',
    icon: 'done_all',
    color: '#164c7b'
  }
];
