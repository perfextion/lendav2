import { BorrowerEntityType } from "./loanmodel";

export class Master_Borrower {
  Master_Borrower_ID: number;
  Borrower_ID_Type: number;
  Borrower_SSN_Hash: number;
  Borrower_Entity_Type_Code: string;
  Borrower_Last_Name: string;
  Borrower_First_Name: string;
  Borrower_MI: string;
  Borrower_Address: string;
  Borrower_City: string;
  Borrower_State_ID: string;
  Borrower_Zip: number;
  Borrower_Phone: string;
  Borrower_email: string;
  Borrower_Preferred_Contact_ind: number;
  Borrower_DL_state: string;
  Borrower_Dl_Num: string;
  Borrower_DOB: Date;
  Borrower_County_ID: number;
  Borrower_Lat: number;
  Borrower_Long: number;
  Status: number;
  IsDelete: number;

  constructor() {
    this.Borrower_ID_Type = 1;
    this.Borrower_Entity_Type_Code = BorrowerEntityType.Individual;
    this.Borrower_Preferred_Contact_ind = 1;
  }
}
