import { JsonObject, JsonProperty } from 'json2typescript';
import {
  IntConverter,
  StringConverter
} from '../Workers/utility/jsonconvertors';

@JsonObject
export class Insurance_Policy {
  @JsonProperty('Policy_id', IntConverter)
  Policy_id: number = undefined;

  @JsonProperty('Agent_Id', IntConverter)
  Agent_Id: number = undefined;

  @JsonProperty('Agency_Id', IntConverter)
  Agency_Id: number = undefined;

  @JsonProperty('State_Id', IntConverter)
  State_Id: number = undefined;

  @JsonProperty('Crop_Practice_Id', IntConverter)
  Crop_Practice_Id: number = undefined;

  @JsonProperty('ProposedAIP', IntConverter)
  ProposedAIP: number = undefined;

  @JsonProperty('County_Id', IntConverter)
  County_Id: number = undefined;

  @JsonProperty('Rated', StringConverter)
  Rated: string = undefined;

  @JsonProperty('MPCI_Subplan', StringConverter)
  MPCI_Subplan: string = undefined;

  @JsonProperty('Option', StringConverter)
  Option: string = undefined;

  @JsonProperty('Unit', StringConverter)
  Unit: string = undefined;

  @JsonProperty('Level', IntConverter)
  Level: number = undefined;

  @JsonProperty('Price', IntConverter)
  Price: number = undefined;

  @JsonProperty('Premium', IntConverter)
  Premium: number = undefined;

  @JsonProperty('HasSecondaryPlans', true)
  HasSecondaryPlans: boolean = undefined;

  @JsonProperty('Custom', true)
  Custom: boolean = undefined;

  @JsonProperty('HighlyRated', true)
  HighlyRated: number = undefined;

  @JsonProperty('YieldProt_Pct', IntConverter)
  YieldProt_Pct: number = undefined;

  @JsonProperty('PriceProt_Pct', IntConverter)
  PriceProt_Pct: number = undefined;

  MPCI_Enabled: Boolean;

  @JsonProperty('Subpolicies', [])
  Subpolicies: Insurance_Subpolicy[] = [];

  ActionStatus: number = 0;

  Practice_Type: string = '';

  FC_StateAbbr: string;
  FC_CountyName: string;
  FC_CropName: string;
  FC_PracticeType: string;
}

@JsonObject
export class Insurance_Subpolicy {
  @JsonProperty('SubPolicy_Id', IntConverter)
  SubPolicy_Id: number = undefined;

  @JsonProperty('FK_Policy_Id', IntConverter)
  FK_Policy_Id: number = undefined;

  @JsonProperty('Ins_Type', StringConverter)
  Ins_Type: string = undefined;

  @JsonProperty('Ins_SubType', StringConverter)
  Ins_SubType: string = undefined;

  @JsonProperty('Upper_Limit', IntConverter)
  Upper_Limit: number = undefined;

  @JsonProperty('Agent_Id', IntConverter)
  Agent_Id: number = undefined;

  @JsonProperty('Agency_Id', IntConverter)
  Agency_Id: number = undefined;

  @JsonProperty('Aip', IntConverter)
  Aip: number = undefined;

  @JsonProperty('Lower_Limit', IntConverter)
  Lower_Limit: number = undefined;

  @JsonProperty('Price_Pct', IntConverter)
  Price_Pct: number = undefined;

  @JsonProperty('Yield_Pct', IntConverter)
  Yield_Pct: number = undefined;

  @JsonProperty('Premium', IntConverter)
  Premium: number = undefined;

  @JsonProperty('Prot_Factor', IntConverter)
  Prot_Factor: number = undefined;

  @JsonProperty('Wind', IntConverter)
  Wind: number = undefined;

  @JsonProperty('Option', StringConverter)
  Options: string = undefined;

  @JsonProperty('Unit', StringConverter)
  Unit: string = undefined;

  @JsonProperty('Yield', IntConverter)
  Yield: number = undefined;

  @JsonProperty('Liability', IntConverter)
  Liability: number = undefined;

  @JsonProperty('FCMC', IntConverter)
  FCMC: number = undefined;

  @JsonProperty('Deduct', IntConverter)
  Deduct: number = undefined;

  Deduct_amt: number = undefined;
  Deduct_pct: number = undefined;
  Icc: number = 0;
  ActionStatus: number = 0;
}

export class InsuranceReportModel {
  State_Id: number = undefined;
  Crop_Practice_Id: number = undefined;
  County_Id: number = undefined;
  Rated: string = undefined;
  Plan: string = undefined;
  Option: number = undefined;
  Unit: string = undefined;
  Price: number = undefined;
  Premium: number = undefined;
  Ins_Type: string = undefined;
  Upper_Level: number = undefined;
  Lower_Level: number = undefined;
  Price_Percent: number = undefined;
  Yield_Percent: number = undefined;
  FCMC: number = undefined;
  Crop_Type_Code: string = undefined;
  Irr_Practice_Type_Code: string = undefined;
  Crop_Practice_Type_Code: string = undefined;
  HR_Exclusion_Ind: boolean = undefined;
  Ins_Plan: string = undefined;
  Ins_Plan_Type: string = undefined;
  Ins_Unit_Type_Code: string = undefined;
  Crop_Code: string = undefined;
  Custom1: number = undefined;
  FC_StateAbbr: string;
  FC_CountyName: string;
  FC_CropName: string;
  FC_PracticeType: string;
}

export class InsuranceValidationData {
  //MPCI
  MPCI_CAT_UNIT: [];

  //MPCI_CAT_AIP
  MPCI_YP_UNIT: [];
  MPCI_YP_OPTION: [];
  MPCI_YP_UPPERPER: [];
  //MPC_YP_AIP
  MPCI_RP_HPE_UNIT: [];
  MPCI_RP_HPE_OPTION: [];
  MPCI_RP_HPE_UPPERPER: [];
  //MPC_RP-HPE_AIP
  MPCI_ARH_UNIT: [];
  MPCI_ARH_OPTION: [];
  MPCI_ARH_UPPERPER: [];
  MPCI_ARH_YIELDPER: [];
  MPCI_ARH_PRICEPER: [];

  MPCI_RP_UNIT: [];
  MPCI_RP_OPTION: [];
  MPCI_RP_UPPERPER: [];

  //MPC_RP_AIP
  //WFRP
  WFRP_UPPERPER: [];
  //WFRP_AIP
  //STAX
  STAX_UNIT: [];
  STAX_UPPERPER: [];
  STAX_LOWERPER: [];
  STAX_RANGEMAX: string | number = undefined;
  STAX_YIELD: [];
  //STAX_AIP
  //SCO
  SCO_UNIT: [];
  //SCO_AIP
  //HMAX
  HMAX_STANDARD_UPPERPER: [];
  HMAX_STANDARD_LOWERPER: [];
  HMAX_STANDARD_PRICE: [];
  HMAX_X1_UPPERPER: [];
  HMAX_X1_LOWERPER: [];
  HMAX_X1_PRICE: [];
  HMAX_MAXRP_UPPERPER: [];
  HMAX_MAXRP_LOWERPER: [];
  HMAX_MAXRP_PRICE: [];

  //RAMP
  RAMP_RY_UNIT: [];
  RAMP_RY_UPPERPER: [];
  RAMP_RY_LOWERPER: [];
  RAMP_RY_PRICE: [];
  RAMP_RY_LIABILITYMAX: [];
  RAMP_RY_LIABILITYMAX85: [];

  RAMP_RR_UNIT: [];
  RAMP_RR_UPPERPER: [];
  RAMP_RR_LOWERPER: [];
  RAMP_RR_PRICE: [];
  RAMP_RR_LIABILITYMAX: [];
  RAMP_RR_LIABILITYMAX85: [];
  RAMP_Liability_Max_Range: number = undefined;

  //ICE
  ICE_BY_UNIT: [];
  ICE_BY_YIELD: [];
  ICE_BR_UNIT: [];
  ICE_BR_YIELD: [];
  ICE_CY_UNIT: [];
  ICE_CY_YIELD: [];
  ICE_CR_UNIT: [];
  ICE_CR_YIELD: [];

  //ABC
  SELECT_REVENUE_UNIT: [];
  SELECT_REVENUE_UPPER_PER: [];
  SELECT_REVENUE_LOWER_PER: [];

  SELECT_YIELD_UNIT: [];
  SELECT_YIELD_UPPER_PER: [];
  SELECT_YIELD_LOWER_PER: [];

  //PCI
  PCI_FCMC: [];

  //PCI_AIP

  //CROPHAIL
  CROPHAIL_BASIC_DEDUCTMAX: [];
  //CROPHAIL_BASIC_AIP

  CROPHAIL_COMPANION_YIELD: [];
  CROPHAIL_COMPANION_PRICE: [];
  CROPHAIL_COMPANION_DEDUCTMAX: [];
  //CROPHAIL_COMPANION_AIP

  HMAX_STANDARD_ELLIGIBLECROPS: Array<string>;
  HMAX_STANDARD_ELLIGIBLESTATES: Array<string>;
  HMAX_STANDARD_INELLIGIBLEPRACTICES: Array<string>;

  HMAX_X1_ELLIGIBLECROPS: Array<string>;
  HMAX_X1_ELLIGIBLESTATES: Array<string>;
  HMAX_X1_INELLIGIBLEPRACTICES: Array<string>;

  HMAX_MAXRP_ELLIGIBLESTATES: Array<string>;
  HMAX_MAXRP_ELLIGIBLECROPS: Array<string>;
  HMAX_MAXRP_INELLIGIBLEPRACTICES: Array<string>;

  RAMP_RY_ELLIGIBLECROPS: Array<string>;
  RAMP_RY_ELLIGIBLESTATES: Array<string>;
  RAMP_RY_INELLIGIBLEPRACTICES: Array<string>;

  RAMP_RR_ELLIGIBLECROPS: Array<string>;
  RAMP_RR_ELLIGIBLESTATES: Array<string>;
  RAMP_RR_INELLIGIBLEPRACTICES: Array<string>;

  ICE_BY_ELLIGIBLECROPS: Array<string>;
  ICE_BY_ELLIGIBLESTATES: Array<string>;
  ICE_BY_INELLIGIBLEPRACTICES: Array<string>;

  ICE_BR_ELLIGIBLESTATES: Array<string>;
  ICE_BR_ELLIGIBLECROPS: Array<string>;
  ICE_BR_INELLIGIBLEPRACTICES: Array<string>;

  ICE_CY_ELLIGIBLECROPS: Array<string>;
  ICE_CY_ELLIGIBLESTATES: Array<string>;
  ICE_CY_INELLIGIBLEPRACTICES: Array<string>;

  ICE_CR_ELLIGIBLECROPS: Array<string>;
  ICE_CR_ELLIGIBLESTATES: Array<string>;
  ICE_CR_INELLIGIBLEPRACTICES: Array<string>;

  SELECT_Yield_ELLIGIBLECROPS: Array<string>;
  SELECT_Yield_ELIGIBLE_STATES: Array<string>;
  SELECT_Yield_INELLIGIBLEPRACTICES: Array<string>;

  SELECT_REVENUE_ELLIGIBLECROPS: Array<string>;
  SELECT_REVENUE_ELIGIBLE_STATES: Array<string>;
  SELECT_REVENUE_INELLIGIBLEPRACTICES: Array<string>;

  PCI_ELLIGIBLESTATES: Array<string>;
  PCI_ELLIGIBLECROPS: Array<string>;
  PCI_INELLIGIBLEPRACTICES: Array<string>;

  CROPHAIL_BASIC_ELLIGIBLECROPS: Array<string>;
  CROPHAIL_BASIC_ELLIGIBLESTATES: Array<string>;
  CROPHAIL_BASIC_INELLIGIBLEPRACTICES: Array<string>;

  CROPHAIL_COMPANION_ELLIGIBLECROPS: Array<string>;
  CROPHAIL_COMPANION_ELLIGIBLESTATES: Array<string>;
  CROPHAIL_COMPANION_INELLIGIBLEPRACTICES: Array<string>;
}

//Newer models on 3 Dec 2018

export class Loan_Policy {
  /**
   *
   */
  constructor() {
    this.Ins_Plan_Type = '';
  }

  @JsonProperty('Loan_Policy_ID', IntConverter)
  Loan_Policy_ID: number = undefined;

  @JsonProperty('Loan_Full_ID', StringConverter)
  Loan_Full_ID: string = '';

  @JsonProperty('State_ID', IntConverter)
  State_ID: number = undefined;

  @JsonProperty('County_ID', IntConverter)
  County_ID: number = undefined;

  @JsonProperty('Agent_ID', IntConverter)
  Agent_ID: number = undefined;

  @JsonProperty('Agency_ID', IntConverter)
  Agency_ID: number = undefined;

  @JsonProperty('AIP_ID', IntConverter)
  AIP_ID: number = undefined;

  @JsonProperty('Crop_Code', StringConverter)
  Crop_Code: string = '';

  @JsonProperty('Crop_Type_Code', StringConverter)
  Crop_Type_Code: string = '';

  @JsonProperty('Irr_Practice_Type_Code', StringConverter)
  Irr_Practice_Type_Code: string = '';

  @JsonProperty('Crop_Practice_Type_Code', StringConverter)
  Crop_Practice_Type_Code: string = '';

  @JsonProperty('HR_Exclusion_Ind', Boolean)
  HR_Exclusion_Ind: boolean = undefined;

  @JsonProperty('Ins_Plan', StringConverter)
  Ins_Plan: string = '';

  @JsonProperty('Ins_Plan_Type', StringConverter)
  Ins_Plan_Type: string = '';

  @JsonProperty('Ins_Unit_Type_Code', StringConverter)
  Ins_Unit_Type_Code: string = '';

  @JsonProperty('Eligible_Ind', Boolean)
  Eligible_Ind: boolean;

  @JsonProperty('Select_Ind', Boolean)
  Select_Ind: boolean;

  @JsonProperty('Area_Yield', IntConverter)
  Area_Yield: number = undefined;

  @JsonProperty('Upper_Level', IntConverter)
  Upper_Level: number = undefined;

  @JsonProperty('Lower_Level', IntConverter)
  Lower_Level: number = undefined;

  @JsonProperty('Liability_Percent', IntConverter)
  Liability_Percent: number = undefined;

  @JsonProperty('Price_Percent', IntConverter)
  Price_Percent: number = undefined;

  @JsonProperty('Liability_Amount', IntConverter)
  Liability_Amount: number = undefined;

  @JsonProperty('Deductible_Units', IntConverter)
  Deductible_Units: number = undefined;

  @JsonProperty('Deductible_Percent', IntConverter)
  Deductible_Percent: number = undefined;

  @JsonProperty('Late_Deductible', IntConverter)
  Late_Deductible: number = undefined;

  @JsonProperty('Acturaial_Hail', IntConverter)
  Acturaial_Hail: number = undefined;

  @JsonProperty('Actuarial_Wind', IntConverter)
  Actuarial_Wind: number = undefined;

  @JsonProperty('Premium', IntConverter)
  Premium: number = undefined;

  @JsonProperty('Yield_Percent', IntConverter)
  Yield_Percent: number = undefined;

  @JsonProperty('Custom1', IntConverter)
  Custom1: number = undefined; //for Fcmc and prot factor

  @JsonProperty('Custom2', IntConverter)
  Custom2: number = undefined;

  @JsonProperty('Ins_Value', IntConverter)
  Ins_Value: number = undefined;

  @JsonProperty('Price', IntConverter)
  Price: number = undefined;

  @JsonProperty('Option', IntConverter)
  Option: number = undefined;

  @JsonProperty('Policy_Verification_Ind', IntConverter)
  Policy_Verification_Ind: number = undefined;

  @JsonProperty('Policy_Verification_Doc_ID', IntConverter)
  Policy_Verification_Doc_ID: number = undefined;

  @JsonProperty('Policy_Verification_User_ID', IntConverter)
  Policy_Verification_User_ID: number = undefined;

  @JsonProperty('Policy_Verification_Date_Time', Date)
  Policy_Verification_Date_Time: Date = undefined;

  @JsonProperty('IsCustom', Boolean)
  IsCustom: boolean;

  @JsonProperty('Other_Description_Text', StringConverter)
  Other_Description_Text: string = '';

  @JsonProperty('Status', IntConverter)
  Status: number = undefined;

  @JsonProperty('Policy_Key', StringConverter)
  Policy_Key: string = undefined;

  ActionStatus: number;
  isVerified: boolean;
  FC_CanCopy: boolean;
  FC_StateCode: string;
  FC_County: string;
}

export class CropUnitPolicies {
  @JsonProperty('Disc_Ins_Value', IntConverter)
  Disc_Ins_Value: number = undefined;

  @JsonProperty('Disc_Ins_Value_Acre', IntConverter)
  Disc_Ins_Value_Acre: number = undefined;

  @JsonProperty('Ins_Plan_Code', StringConverter)
  Ins_Plan_Code: string = undefined;

  @JsonProperty('Ins_Plan_Type_Code', StringConverter)
  Ins_Plan_Type_Code: string = undefined;

  @JsonProperty('Ins_Qty', IntConverter)
  Ins_Qty: number = undefined;

  @JsonProperty('Ins_Value', IntConverter)
  Ins_Value: number = undefined;

  @JsonProperty('Ins_Value_Acre', IntConverter)
  Ins_Value_Acre: number = undefined;

  @JsonProperty('Loan_Crop_Unit_ID', IntConverter)
  Loan_Crop_Unit_ID: number = undefined;

  @JsonProperty('Loan_Full_ID', StringConverter)
  Loan_Full_ID: string = undefined;

  @JsonProperty('Loan_Plan_Detail_ID', IntConverter)
  Loan_Plan_Detail_ID: number = undefined;

  @JsonProperty('Loan_Policy_ID', IntConverter)
  Loan_Policy_ID: number = undefined;

  @JsonProperty('Premium', IntConverter)
  Premium: number = undefined;

  @JsonProperty('Status', IntConverter)
  Status: number = undefined;
}
