/**
 * Keeps Track of Sync Status
 */
export enum Sync_Status {
  NOCHANGE = 0,
  EDITED = 1,
  ADDORDELETE = 2
}
export interface ModelStatus {
  Status_Farm: Sync_Status;
  Status_Crop_Practice: Sync_Status;
  Status_Insurance_Policies: Sync_Status;
  Status_Borrower: Sync_Status;
  Status_Farmer: Sync_Status;
}
