export class LoggedInUserModel {
  UserID: number;
  Username: string;
  Password_Hash?: any;
  User_Type_ID: number;
  FirstName: string;
  LastName: string;
  MI: string;
  Phone: string;
  Email: string;
  Address: string;
  City: string;
  State: string;
  Zip: string;
  Office_ID: number;
  Region_ID: number;
  Job_Title_Code: string;
  Job_Title_ID: number;
  Access_Failed_Count: number;
  Lockout_Enabled: Date;
  Lockout_End_Date: Date;
  Report_To_ID: number;
  Hire_Date: Date;
  Last_Committe_Assignment: Date;
  Vacation_Start: Date;
  Vacation_End: Date;
  Status: number;
  SysAdmin: number;
  RiskAdmin: number;
  Processor: number;
  IsAdmin: number;
}

export class errormodel {
  tab: string;
  cellid?: string;
  rowId?: string;
  colId?: string;
  details: string[];
  chevron: string;
  ToolTipText?: string;
  level: validationlevel;
  formDetails?: {
    component: string;
    formGroup: string;
    formControlName: string;
  };
  quetionId?: number; // validatio
  Validation_ID?: number;
  Validation_ID_Text?: string;
  hoverText?: string;
  ignore?: boolean;
}

/**
 * Validation levels enum
 */
export enum validationlevel {
  /**
   * Validation -   Warning
   *
   */
  level1 = 1,
  level1blank = 3,

  /**
   * Validation -   Hard Stop
   */
  level2 = 2,
  level2blank = 4
}

/**
 * Exception levels enum
 */
export enum exceptionlevel {
  /**
   * Exception  -   Non Critical
   */
  level1 = 1,

  /**
   * Exception   -   Critical
   */
  level2 = 2
}

export enum Role {
  user = 1,
  admin = 2
}

export enum Chevronkeys {
  InsurancePolices = 'InsPolicies',
  Farm = 'Farm',
  APH = 'Aph',
  AGT = 'AGT',
  AGY = 'AGY',
  AIP = 'AIP',
  BUY = 'BUY',
  DIS = 'DIS',
  HAR = 'HAR',
  REB = 'REB',
  THR = 'THR',
  LEI = 'LEI',
  GUA = 'GUA',
  REF = 'REF',
  LLD = 'LLD',
  CRF = 'CRF'
}

export enum Tabs {
  Farm = 'Farm',
  Insurance = 'Insurance',
  APH = 'Aph',
  AGT = 'AGT',
  AGY = 'AGY',
  AIP = 'AIP',
  BUY = 'BUY',
  DIS = 'DIS',
  HAR = 'HAR',
  REB = 'REB',
  THR = 'THR',
  LEI = 'LEI',
  GUA = 'GUA',
  REF = 'REF',
  LLD = 'LLD',
  CRF = 'CRF'
}

export enum TableId {
  Farm = 'Frm_',
  Insurance = 'Ins_',
  APH = 'Aph_',
  AGT = 'AGT_',
  AGY = 'AGY_',
  AIP = 'AIP_',
  BUY = 'BUY_',
  DIS = 'DIS_',
  HAR = 'HAR_',
  REB = 'REB_',
  THR = 'THR_',
  LEI = 'LEI_',
  GUA = 'GUA_',
  REF = 'REF_',
  LLD = 'LLD_',
  CRF = 'CRF_',
  OPT = 'OPT'
}

export enum SourceComponent {
  FarmComponent = 'FarmComponent',
  APH = 'APH',
  APH_Excel_Upload = 'APH_Excel_Upload'
}

export enum VerificationStages {
  Verify = 'Verify',
  Check = 'Verify ',
  Check2 = 'Verify  '
}

export class LoanCalculationParams {
  Recalculate: boolean;
  Documents: boolean;
  Exception: boolean;
  Tab_ID: number;
}
