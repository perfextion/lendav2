import {Component} from "@angular/core";
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
    selector: 'child-cell',
    template: '<input type="checkbox" [disabled]="edit" [checked]="params.value" (change)="invokeParentMethod($event)">'
})
export class CheckboxCellComponent implements ICellRendererAngularComp {
    public params: any;
    public edit=true;
    agInit(params: any): void {
        this.params = params;
         this.edit=(!params.edit);
    }

    public invokeParentMethod(event) {
        
        if(!this.edit)
        {
            let obj;
         if(this.params.colDef.field!="")
         obj={value:event.srcElement.checked,col:this.params.colDef.field,id:this.params.data.mainpolicyId}
        else
        obj={value:event.srcElement.checked,col:this.params.colDef.pickfield,id:this.params.data.mainpolicyId}
        if(this.params.colDef.field=="Reimbursement")
        {
            obj={value:event.srcElement.checked,col:this.params.colDef.field,id:this.params.rowIndex}
            this.params.context.componentParent.methodFromcheckbox(obj);
        }
        else
        this.params.context.componentParent.methodFromcheckbox(obj)
        }
    }

    refresh(): boolean {
        return false;
    }
}